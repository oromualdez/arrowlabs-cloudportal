﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Mims.Audit.Models
{
    public class UserAnswerSurveyAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int SurveyId { get; set; }
        public int UserId { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CustomerUName { get; set; }
        public int Answer { get; set; }

        public static int InsertUserAnswerSurveyAudit(UserAnswerSurveyAudit notification, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(notification.BaseAudit, dbConnection);

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertUserAnswerSurveyAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = notification.UserId;

                    cmd.Parameters.Add(new SqlParameter("@SurveyId", SqlDbType.Int));
                    cmd.Parameters["@SurveyId"].Value = notification.SurveyId;
                      
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate; 

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy; 

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@Answer", SqlDbType.Int));
                    cmd.Parameters["@Answer"].Value = notification.Answer;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertUserAnswerSurveyAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return 0;
        }
    }
}
