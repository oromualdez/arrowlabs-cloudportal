﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Customers.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.Customers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
         <style>
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <script src="https://testportalcdn.azureedge.net/scripts/jquery.validate.js"></script>
      <script src="https://testportalcdn.azureedge.net/scripts/dropzone.js"></script>
    <script src="https://testportalcdn.azureedge.net/scripts/fileinput.js"></script>
    <!--Add script to update the page and send messages.-->
    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        function saveInfo() {
            try {
                //isEmptyOrSpaces
                var isErr = false;
                if (isEmptyOrSpaces(document.getElementById('addUserNameInput').value)) {
                    isErr = true;
                    showAlert('Provide your first name');
                }
                if (isEmptyOrSpaces(document.getElementById("addLastNameInput").value)) {
                    isErr = true;
                    showAlert('Provide your last name');
                }
                if (!isErr) {
                    if (isEmptyOrSpaces(document.getElementById('addAddressInput').value)) {
                        isErr = true;
                        showAlert('Provide your address');
                    }
                }
                if (!isErr) {
                    if (!isEmptyOrSpaces(document.getElementById('addEmailAddInput').value)) {
                        if (isInvalidEmail(document.getElementById('addEmailAddInput').value)) {
                            isErr = true;
                        }
                    }
                    else {
                        isErr = true;
                        showAlert('Provide emaill address');
                    }
                }
                if (!isErr) {
                    if (!isEmptyOrSpaces(document.getElementById('addPhoneNoInput').value)) {
                        if (!isNumeric(document.getElementById('addPhoneNoInput').value)) {
                            isErr = true;
                            showAlert('Provide numeric phone number');
                        }
                    }
                    else {
                        isErr = true;
                        showAlert('Provide phone number');
                    }
                }
                if (!isErr) {
                    if (!isEmptyOrSpaces(document.getElementById('addTotalInput').value)) {
                        if (!isNumeric(document.getElementById('addTotalInput').value)) {
                            isErr = true;
                            showAlert('Provide numeric total user');
                        }
                    }
                    else {
                        isErr = true;
                        showAlert('Provide total user');
                    }
                }
                var scountr = $("#countrySelect option:selected").text();
                if (!isErr) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Customers.aspx/saveInfo",
                        data: "{'name':'" + document.getElementById('addUserNameInput').value + "','address':'" + document.getElementById('addAddressInput').value
                            + "','country':'" + scountr + "','email':'" + document.getElementById('addEmailAddInput').value
                            + "','phoneno':'" + document.getElementById('addPhoneNoInput').value + "','total':'" + document.getElementById('addTotalInput').value
                            + "','notimodule':'" + document.getElementById("notificationModule").checked + "','incimodule':'" + document.getElementById("incidentModule").checked
                            + "','taskmodule':'" + document.getElementById("taskModule").checked + "','ticketmodule':'" + document.getElementById("ticketModule").checked
                            + "','postmodule':'" + document.getElementById("postOrderModule").checked + "','verimodule':'" + document.getElementById("postOrderModule").checked + "','chatmodule':'" + document.getElementById("chatModule").checked + "','drmodule':'" + document.getElementById("DRModule").checked
                            + "','lfmodule':'" + document.getElementById("LostFoundModule").checked + "','survmodule':'" + document.getElementById("postOrderModule").checked + "','reqmodule':'" + document.getElementById("requestModule").checked + "','collabmodule':'" + document.getElementById("collabModule").checked
                            + "','dispatchmodule':'" + document.getElementById("dispatchModule").checked + "','locationmodule':'" + document.getElementById("locationModule").checked
                            + "','waremodule':'" + document.getElementById("warehouseModule").checked + "','lastname':'" + document.getElementById("addLastNameInput").value
                            + "','id':'" + document.getElementById("rowidChoice").text + "','activity':'" + document.getElementById("activityModule").checked + "','uname':'" + loggedinUsername
                            + "','id':'" + document.getElementById("rowidChoice").text + "','activity':'" + document.getElementById("activityModule").checked + "','uname':'" + loggedinUsername
                            + "','mbCountInput':'" + document.getElementById("mbCountInput").value
                            + "','incidentCountInput':'" + document.getElementById("incidentCountInput").value
                            + "','taskCountInput':'" + document.getElementById("taskCountInput").value
                            + "','ticketCountInput':'" + document.getElementById("ticketCountInput").value
                            + "','poCountInput':'" + document.getElementById("poCountInput").value
                            + "','wareCountInput':'" + document.getElementById("wareCountInput").value
                            + "','chatCountInput':'" + document.getElementById("chatCountInput").value
                            + "','drCountInput':'" + document.getElementById("drCountInput").value
                            + "','lfCountInput':'" + document.getElementById("lfCountInput").value
                            + "','conCountInput':'" + document.getElementById("conCountInput").value
                            + "','reqCountInput':'" + document.getElementById("reqCountInput").value
                            + "','surCountInput':'" + document.getElementById("surCountInput").value
                            + "','disCountInput':'" + document.getElementById("disCountInput").value
                            + "','actCountInput':'" + document.getElementById("actCountInput").value
                            + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else if (data.d == "SUCCESS") {
                                jQuery('#newUser').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Successfully updated client info.";
                                jQuery('#successfulDispatch').modal('show');
                            }
                            else
                                showError('Problem occured trying to register. Feel free to contact us for support');
                        },
                        error: function (err) {
                            console.log(err);
                           // showError("Session timeout. Kindly login again.");
                           // setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            catch (err) {
                //alert(err)
            }
        }
        var loggedinUsername = '<%=senderName2%>';
        //User-profile
        function changePassword() {
            try {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;
                var isErr = false;
                if (!isErr) { 
                    if (!letterGood) {
                        showAlert('Password does not contain letter');
                        isErr = true;
                    }
                    if (!isErr) {
                        if (!capitalGood) {
                            showAlert('Password does not contain capital letter');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!numGood) {
                            showAlert('Password does not contain number');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!lengthGood) {
                            showAlert('Password length not enough');
                            isErr = true;
                        }
                    }
                }
                if (!isErr) {
                    if (newPw == confPw && newPw != "" && confPw != "") {
                        jQuery.ajax({
                            type: "POST",
                            url: "Customers.aspx/changePW",
                            data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                } else {
                                    jQuery('#changePasswordModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                    jQuery('#successfulDispatch').modal('show');
                                    document.getElementById("newPwInput").value = "";
                                    document.getElementById("confirmPwInput").value = "";
                                    document.getElementById("oldPwInput").value = confPw;
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        showAlert("Kindly match new password with confirm password.")
                    }
                }
            }
            catch (ex)
            { showAlert('Error 60: Problem loading page element.-' + ex) }
        }
        function editUnlock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
            document.getElementById("editProfileA").style.display = "none";
            document.getElementById("saveProfileA").style.display = "block";
            document.getElementById("profileEmailAddDIV").style.display = "none";
            document.getElementById("profileEmailAddEditDIV").style.display = "block";
            document.getElementById("userFullnameSpanDIV").style.display = "none";
            document.getElementById("userFullnameSpanEditDIV").style.display = "block";
            if (document.getElementById('profileRoleName').innerHTML == "User") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "block";
                document.getElementById("dirInfoDIV").style.display = "none";
            }
            else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "none";
                document.getElementById("dirInfoDIV").style.display = "block";
            }
        }
        function editJustLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        function editLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
            var role = document.getElementById('UserRoleSelector').value;
            var roleid = 0;
            var supervisor = 0;
            var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
            if (retVal > 0) {
                assignUserProfileData();
            }
            else {
                showError('Error 62: Problem occured while trying to get user profile.');
            }

        }
        function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
            var output = 0;
            jQuery.ajax({
                type: "POST",
                url: "Customers.aspx/addUserProfile",
                data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                }
            });
            return output;
        }
        function assignUserProfileData() {
            try {
                jQuery.ajax({
                    type: "POST",
                    url: "Customers.aspx/getUserProfileData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            try {
                                document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                document.getElementById('profileRoleName').innerHTML = data.d[5];
                                document.getElementById('profileManagerName').innerHTML = data.d[6];
                                document.getElementById('userStatusSpan').innerHTML = data.d[8];
                                var el = document.getElementById('userStatusIconSpan');
                                if (el) {
                                    el.className = data.d[9];
                                }
                                document.getElementById('userprofileImgSrc').src = data.d[10];
                                document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                document.getElementById('userFirstnameSpan').value = data.d[13];
                                document.getElementById('userLastnameSpan').value = data.d[14];
                                document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                document.getElementById('profileEmailAddEdit').value = data.d[3];

                                document.getElementById('oldPwInput').value = data.d[16];

                                document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                document.getElementById('profileGender').innerHTML = data.d[20];
                            }
                            catch (err) {
                                //alert(err)
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            catch (err)
            {
                showError('Error 60: Problem loading page element.-' + err)
            }
        }
        function forceLogout() {
            document.getElementById('<%= closingbtn.ClientID %>').click();
        }
        function userChangeStatus(id, state) {
            jQuery.ajax({
                type: "POST",
                url: "Customers.aspx/changeCustomerState",
                data: "{'id':'" + id + "','status':'" + state + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById(data.d[0]).innerHTML = data.d[1];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }
        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
        jQuery(document).ready(function () {
            localStorage.removeItem("activeTabDev");
            localStorage.removeItem("activeTabInci");
            localStorage.removeItem("activeTabMessage");
            localStorage.removeItem("activeTabTask");
            localStorage.removeItem("activeTabTick");
            localStorage.removeItem("activeTabUB");
            localStorage.removeItem("activeTabVer");
            localStorage.removeItem("activeTabLost");
            addrowtoTable();
            $('input[type=password]').keyup(function () {
                // keyup event code here
                var pswd = $(this).val();
                if (pswd.length < 8) {
                    $('#length').removeClass('valid').addClass('invalid');
                    lengthGood = false;
                } else {
                    $('#length').removeClass('invalid').addClass('valid');
                    lengthGood = true;
                }
                //validate letter
                if (pswd.match(/[A-z]/)) {
                    $('#letter').removeClass('invalid').addClass('valid');
                    letterGood = true;

                } else {
                    $('#letter').removeClass('valid').addClass('invalid');
                    letterGood = false;
                }

                //validate capital letter
                if (pswd.match(/[A-Z]/)) {
                    $('#capital').removeClass('invalid').addClass('valid');
                    capitalGood = true;
                } else {
                    $('#capital').removeClass('valid').addClass('invalid');

                    capitalGood = false;
                }

                //validate number
                if (pswd.match(/\d/)) {
                    $('#number').removeClass('invalid').addClass('valid');
                    numGood = true;

                } else {
                    $('#number').removeClass('valid').addClass('invalid');
                    numGood = false;
                }
            });
            $('input[type=password]').focus(function () {
                // focus code here
                $('#pswd_info').show();
            });
            $('input[type=password]').blur(function () {
                // blur code here
                $('#pswd_info').hide();
            });
        });
        function rowchoice(id) {
            document.getElementById("rowidChoice").text = id;

            jQuery.ajax({
                type: "POST",
                url: "Customers.aspx/getCustomerInfo",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "SUCCESS") {
                        document.getElementById("addAddressInput").value = data.d[1];
                        document.getElementById("countrySelect").value = data.d[2];
                        jQuery('#countrySelect').selectpicker('val', data.d[2]);
                        document.getElementById("addEmailAddInput").value = data.d[3];

                        document.getElementById("headersitename").innerHTML = "Are you sure you want to delete this client " + data.d[3] + "?";

                        document.getElementById("addUserNameInput").value = data.d[4];
                        document.getElementById("addPhoneNoInput").value = data.d[5];
                        document.getElementById("addTotalInput").value = data.d[6];

                        document.getElementById('notificationModule').checked = false;
                        document.getElementById('incidentModule').checked = false;
                        document.getElementById('taskModule').checked = false;
                        document.getElementById('ticketModule').checked = false;
                        document.getElementById('postOrderModule').checked = false;
                        //document.getElementById('verifierModule').checked = false;
                        document.getElementById('chatModule').checked = false;
                        document.getElementById('DRModule').checked = false;
                        document.getElementById('LostFoundModule').checked = false;
                        document.getElementById('surveilModule').checked = false;
                        document.getElementById('requestModule').checked = false;
                        document.getElementById('collabModule').checked = false;
                        document.getElementById('dispatchModule').checked = false;
                        document.getElementById('locationModule').checked = false;
                        document.getElementById('warehouseModule').checked = false;
                        document.getElementById('activityModule').checked = false;
                        
                        if (data.d[7] == "true") {
                            document.getElementById('notificationModule').checked = true;
                        }
                        if (data.d[8] == "true") {
                            document.getElementById('incidentModule').checked = true;
                        }
                        if (data.d[9] == "true") {
                            document.getElementById('taskModule').checked = true;
                        }
                        if (data.d[10] == "true") {
                            document.getElementById('ticketModule').checked = true;
                        }
                        if (data.d[11] == "true") {
                            document.getElementById('postOrderModule').checked = true;
                        }
                        if (data.d[12] == "true") {
                            //document.getElementById('verifierModule').checked = true;
                        }
                        if (data.d[13] == "true") {
                            document.getElementById('chatModule').checked = true;
                        }
                        if (data.d[14] == "true") {
                            document.getElementById('DRModule').checked = true;
                        }
                        if (data.d[15] == "true") {
                            document.getElementById('LostFoundModule').checked = true;
                        }
                        if (data.d[16] == "true") {
                            document.getElementById('surveilModule').checked = true;
                        }
                        if (data.d[17] == "true") {
                            document.getElementById('requestModule').checked = true;
                        }
                        if (data.d[18] == "true") {
                            document.getElementById('collabModule').checked = true;
                        }
                        if (data.d[19] == "true") {
                            document.getElementById('dispatchModule').checked = true;
                        }
                        if (data.d[20] == "true") {
                            document.getElementById('locationModule').checked = true;
                        }
                        if (data.d[21] == "true") {
                            document.getElementById('warehouseModule').checked = true;
                        }
                        if (data.d[22] == "true") {
                            document.getElementById('activityModule').checked = true;
                        }
                        document.getElementById("addcreatedDInput").value = data.d[23];

                        document.getElementById("addLastNameInput").value = data.d[24];

                        document.getElementById("mbCountInput").value = data.d[25];

                        document.getElementById("incidentCountInput").value = data.d[26];
                        
                        document.getElementById("taskCountInput").value = data.d[27];

                        document.getElementById("ticketCountInput").value = data.d[28];
                        
                        document.getElementById("poCountInput").value = data.d[29];

                        document.getElementById("wareCountInput").value = data.d[30];
                        
                        document.getElementById("chatCountInput").value = data.d[31];
                        
                        document.getElementById("drCountInput").value = data.d[32];
                        
                        document.getElementById("lfCountInput").value = data.d[33];

                        document.getElementById("conCountInput").value = data.d[34];
                        
                        document.getElementById("reqCountInput").value = data.d[35];
                        
                        document.getElementById("surCountInput").value = data.d[36];
                        
                        document.getElementById("disCountInput").value = data.d[37];
                        
                        document.getElementById("actCountInput").value = data.d[38];
                        
                        
                    }
                    else if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert(data.d[0]);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function deleteChoice() {
            jQuery.ajax({
                type: "POST",
                url: "Customers.aspx/deleteSiteData",
                data: "{'id':'" + document.getElementById("rowidChoice").text + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        jQuery('#deleteSite').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = "Successfully deleted client info!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    function addrowtoTable() {
        jQuery("#verifierTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Customers.aspx/getTableData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#verifierTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    </script>
    <!-- ============================================
    MAIN CONTENT SECTION
    =============================================== -->
        <section class="content-wrapper" role="main">
            <div class="content">
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">Clients</span></h3>
                                </div>
                                <div class="col-md-8">
                                    <div class="panel-control">
                                        <ul id="demo3-tabs" class="nav nav-tabs nav-main">
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div role="group" class="pull-right">
                                       <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                    <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
							<div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                  
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-sites">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">CLIENTS</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="verifierTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="EMAIL">EMAIL<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="LICENSE">LICENSE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="MODULES">MODULES<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </div>
                <!-- /panel-body -->
            </div>
            <!-- /.panel -->
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="../Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            		                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="deleteSite" role="dialog" tabindex="-1" id="deleteSite" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="headersitename">Are you sure you want to delete this customer?</h4>
                        </div>
                         <div class="row">
                            <h4 class="text-center" id="todeleteVer"></h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: Please take a backup of database.This action cannot be undone!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li > <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li ><a href="#"  onclick="deleteChoice()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="newUser" role="dialog" tabindex="-1" id="newUser" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="clearFields()"><i class="icon_close fa-lg"></i></button>
                      <h4 class="modal-title">REGISTRATION FORM</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-7 border-right">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Created Date:</p>
                                        <input disabled placeholder="Name" id="addcreatedDInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">First Name:</p>
                                        <input placeholder="Name" id="addUserNameInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Last Name:</p>
                                        <input placeholder="Name" id="addLastNameInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Address:</p>
                                        <input placeholder="Address" id="addAddressInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                       <p style="margin-left:5px;" class="red-color">Country:</p>
                                         										<label class="select select-o" >
                                           <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:30)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:30)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:30)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:30)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:30)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+05:45 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:30)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Email Address:</p>
                                        <input placeholder="Email Address" id="addEmailAddInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Phone Number:</p>
                                        <input placeholder="Phone Number"  id="addPhoneNoInput" class="form-control">
                                    </div>
                                </div>    
                                  <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Total User License:</p>
                                        <input placeholder="Total User License"  id="addTotalInput" class="form-control"> 
                                    </div>
                                </div>                                                                               
                            </div>
                            <div class="col-md-5 ">
                                <div class="row" id="mobilemodulesDIV">
                                    <div class="col-md-12" >
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                          <h3 class="capitalize-text no-margin">MODULES</h3>
                                                </div>
                                                </div>
                                             <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:550px">												
                                                    
                                                                            
                                            <div class="row">
                                                   <div class="col-md-8">
                                                           <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="notificationModule" name="niceCheck">
                                            <label for="notificationModule">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                                       </div>
                                                <div class="col-md-3">                                              
                                      <input placeholder="0"  id="mbCountInput" class="form-control"> 
                                          </div>
                                                 
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8">
                                                                                              <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="incidentModule" name="niceCheck">
                                            <label for="incidentModule">Incidents</label>
                                          </div><!--/nice-checkbox-->
                                                </div> 
                                                                                          <div class="col-md-3">    
<input placeholder="0"  id="incidentCountInput" class="form-control"> 
                                          </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8">
                                           <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="taskModule" name="niceCheck">
                                            <label for="taskModule">Task</label>
                                          </div><!--/nice-checkbox-->
                                                </div> 
                                                                                          <div class="col-md-3">    
   <input placeholder="0"  id="taskCountInput" class="form-control"> 
                                          </div>
                                            </div>


                                             <div class="row">
                                                <div class="col-md-8">
 <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="ticketModule" name="niceCheck">
                                                                            <label for="ticketModule">Ticketing</label>
                                                                            </div><!--/nice-checkbox-->  
                                                </div> 
                                                                                          <div class="col-md-3">    
    <input placeholder="0"  id="ticketCountInput" class="form-control"> 
                                          </div>
                                            </div>


                                             <div class="row">
                                                <div class="col-md-8">
                                           <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="postOrderModule" name="niceCheck">
                                            <label for="postOrderModule">Post Order</label>
                                          </div><!--/nice-checkbox--> 
                                                </div> 
                                                                                          <div class="col-md-3">    
    <input placeholder="0"  id="poCountInput" class="form-control"> 
                                          </div>
                                            </div>

                                            <div class="row">
 <div class="col-md-8">
  <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="warehouseModule" name="niceCheck">
                                                                            <label for="warehouseModule">Warehouse</label>
                                                                            </div><!--/nice-checkbox-->  
                                                </div> 
 
                             <div class="col-md-3 ">        
                                     <input placeholder="0"  id="wareCountInput" class="form-control">                              
                                                                            </div>
                                          </div>

                                            <div class="row">
                                                <div class="col-md-8">
 <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="chatModule" name="niceCheck">
                                            <label for="chatModule">Chat</label>
                                          </div><!--/nice-checkbox-->   
                                                </div> 
                                                                                          <div class="col-md-3">    
	                         <input placeholder="0"  id="chatCountInput" class="form-control">  
                                          </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-8">
  <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="DRModule" name="niceCheck">
                                            <label for="DRModule">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                                </div> 
                                                                                          <div class="col-md-3">    
	                         <input placeholder="0"  id="drCountInput" class="form-control">  
                                          </div>
                                            </div>

                                            <div class="row">
                                           <div class="col-md-8">
    <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="LostFoundModule" name="niceCheck">
                                            <label for="LostFoundModule">Lost&Found</label>
                                          </div><!--/nice-checkbox-->   
                                                </div> 
                                        <div class="col-md-3" >                                              
                                                 <input placeholder="0"  id="lfCountInput" class="form-control">  
                                        </div>
                                        </div>
                                                  <div class="row">
                                                <div class="col-md-8">
 <div  class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox"  id="locationModule" name="niceCheck">
                                                                            <label for="locationModule">Contract</label>
                                                                            </div><!--/nice-checkbox-->  
                                                </div> 
                                                                                          <div class="col-md-3">    
	                         <input placeholder="0"  id="conCountInput" class="form-control">  
                                          </div>
                                            </div>

                                                  
                                                                          

                                                 <div class="row">
                                                <div class="col-md-8">
      <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="requestModule" name="niceCheck">
                                                                            <label for="requestModule">Request</label>
                                                                            </div><!--/nice-checkbox-->   
                                                </div> 
                                                                                          <div class="col-md-3">    
	                         <input placeholder="0"  id="reqCountInput" class="form-control">  
                                          </div>
                                            </div>

                                            <div class="row">
                                                            <div class="col-md-8">   
                                                                 <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="surveilModule" name="niceCheck">
                                                                            <label for="surveilModule">Surveillance</label>
                                                                            </div><!--/nice-checkbox-->                                  
                                             </div> 

                                                                                                                        
 
                                                                            <div class="col-md-3">
                                                          <input placeholder="0"  id="surCountInput" class="form-control">  
                                                       </div>  
                                                 <div class="col-md-4 " style="display:none;"> 
                                                                                                                      <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="collabModule" name="niceCheck">
                                                                            <label for="collabModule">Collaboration</label>
                                                                            </div><!--/nice-checkbox-->   
                                                                            </div>    
                                            </div>

                                                  <div class="row">
                                                <div class="col-md-8">
   <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="dispatchModule" name="niceCheck">
                                            <label for="dispatchModule">Dispatch</label>
                                          </div><!--/nice-checkbox-->   
                                                </div> 
                                                                                          <div class="col-md-3">    
	                         <input placeholder="0"  id="disCountInput" class="form-control">  
                                          </div>
                                            </div>

                                            <div class="row">
                                                          <div class="col-md-8">   
                                                               <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="activityModule" name="niceCheck">
                                            <label for="activityModule">Activity</label>
                                          </div><!--/nice-checkbox-->  
                                                                  </div>                             
                                                                                               <div class="col-md-3" >    
                                           <input placeholder="0"  id="actCountInput" class="form-control">  
                                          </div>                                                                 
                                                                   
                                                </div>
                                                 </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                        </div>
                                    </div>
                                </div>                                                           
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs">
                                    <li ><a href="#" data-dismiss="modal" onclick="clearFields()">Close</a>
                                    </li>  
                                    <li ><a href="#" onclick="saveInfo()">Update</a>
                                    </li>                                   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>	
            <input style="display:none;" id="rowIncidentName" type="text"/>
            <input style="display:none;" id="verCaseID1" type="text"/>
            <input style="display:none;" id="verCaseID2" type="text"/>
            <input style="display:none;" id="verCaseID3" type="text"/>
            <input style="display:none;" id="verCaseID4" type="text"/>
            <input style="display:none;" id="verCaseID5" type="text"/>
            <input style="display:none;" id="verCaseName1" type="text"/>
            <input style="display:none;" id="verCaseName2" type="text"/>
            <input style="display:none;" id="verCaseName3" type="text"/>
            <input style="display:none;" id="verCaseName4" type="text"/>
            <input style="display:none;" id="verCaseName5" type="text"/>
            
            <input style="display:none;" id="rowidChoice" type="text"/>
            <input style="display:none;" id="rowidChoiceANPR" type="text"/>
            <input style="display:none;" id="imagePath" type="text"/>
             </section>
        <!-- /MAIN -->
</asp:Content>
