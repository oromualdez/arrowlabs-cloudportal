﻿// -----------------------------------------------------------------------
// <copyright file="MobileHotEvent.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Data.SqlClient;
    using System.Data;
    using Arrowlabs.Mims.Audit.Models;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    /// 
    [Serializable]
    [DataContract]
    public class MobileHotEvent
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int EventType { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public float SubmittedFromLatitude { get; set; }
        [DataMember]
        public float SubmittedFromLongitude { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public Guid Identifier { get; set; }
        [DataMember]
        public string EventTypeName { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public string Notes { get; set; }
        [DataMember]
        public int WebPort { get; set; }
        public string ServerNotes { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string SessionId { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int AssignId { get; set; }

        [DataMember]
        public int CustomerId { get; set; }
         

        public enum HotEventTypes
        {
            None = 0,
            Traffic = 1,
            Accident = 2,
            Damage = 3,
            Theft = 4,
            Vandalism = 5
        }

        public static List<string> GetHotEventTypeList()
        {
            return Enum.GetNames(typeof(HotEventTypes)).ToList();
        }
        private static MobileHotEvent GetMobileHotEventFromSqlReader(SqlDataReader reader, string dbConnection)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var hotEvent = new MobileHotEvent();

            if (reader["Id"] != DBNull.Value)
                hotEvent.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["EventType"] != DBNull.Value)
            {
                hotEvent.EventType = Convert.ToInt32(reader["EventType"].ToString());
                //var hotEventCategories = MobileHotEventCategory.GetAllMobileHotEventCategories(dbConnection);
                //var hotEventCategory = hotEventCategories.Where(x => x.CategoryId == hotEvent.EventType).FirstOrDefault();
                //if (hotEventCategory != null)
                //    hotEvent.EventTypeName = hotEventCategory.Description;
                //else
                //    hotEvent.EventTypeName = "None";                
            }

            if (columns.Contains( "EventTypeName") && reader["EventTypeName"] != DBNull.Value)
                hotEvent.EventTypeName = reader["EventTypeName"].ToString();

            if (reader["CreatedBy"] != DBNull.Value)
                hotEvent.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                hotEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (reader["UpdatedBy"] != DBNull.Value)
                hotEvent.UpdatedBy = reader["UpdatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                hotEvent.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
            if (reader["Comments"] != DBNull.Value)
                hotEvent.Comments = reader["Comments"].ToString();
            if (reader["IsDeleted"] != DBNull.Value)
                hotEvent.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (reader["SubmittedFromLatitude"] != DBNull.Value)
                hotEvent.SubmittedFromLatitude = Convert.ToSingle(reader["SubmittedFromLatitude"].ToString());
            if (reader["SubmittedFromLongitude"] != DBNull.Value)
                hotEvent.SubmittedFromLongitude = Convert.ToSingle(reader["SubmittedFromLongitude"].ToString());
            if (reader["Identifier"] != DBNull.Value)
                hotEvent.Identifier = new Guid(reader["Identifier"].ToString());
            if (reader["WebPort"] != DBNull.Value)
                hotEvent.WebPort = Convert.ToInt32(reader["WebPort"].ToString());
            if (columns.Contains("Notes") && reader["Notes"] != DBNull.Value)
                hotEvent.Notes = reader["Notes"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains("ServerNotes") && reader["ServerNotes"] != DBNull.Value)
                hotEvent.ServerNotes = reader["ServerNotes"].ToString();
            try
            {
                if (columns.Contains("Location") && reader["Location"] != DBNull.Value)
                    hotEvent.Location = reader["Location"].ToString();
            }
            catch { }
            return hotEvent;
        }

        public static List<MobileHotEvent> GetAllMobileHotEvents(string dbConnection)
        {
            var hotEvents = new List<MobileHotEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllMobileHotEvents";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventFromSqlReader(reader, dbConnection);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }


        public static MobileHotEvent GetMobileHotEventById(int id, string dbConnection)
        {
            MobileHotEvent hotEvent = new  MobileHotEvent();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetMobileHotEventById";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    hotEvent = GetMobileHotEventFromSqlReader(reader, dbConnection);
                    
                }
                connection.Close();
                reader.Close();
            }
            return hotEvent;
        }


        public static MobileHotEvent GetMobileHotEventByGuid(Guid Identifier, string dbConnection)
        {
            MobileHotEvent hotEvent = new MobileHotEvent();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetMobileHotEventByGuid";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@Identifier"].Value = Identifier;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    hotEvent = GetMobileHotEventFromSqlReader(reader, dbConnection);

                }
                connection.Close();
                reader.Close();
            }
            return hotEvent;
        }

        public static int InsertOrUpdateMobileHotEvent(MobileHotEvent hotEvent, string dbConnection
            , string auditDbConnection= "",  bool isAuditEnabled = true)
        {
            int id = hotEvent.Id;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


            if (hotEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMobileHotEvent", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = hotEvent.Id;

                    cmd.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                    cmd.Parameters["@EventType"].Value = hotEvent.EventType;

                    cmd.Parameters.Add(new SqlParameter("@Comments", SqlDbType.NVarChar));
                    cmd.Parameters["@Comments"].Value = hotEvent.Comments;

                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = hotEvent.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@SubmittedFromLatitude", SqlDbType.Float));
                    cmd.Parameters["@SubmittedFromLatitude"].Value = hotEvent.SubmittedFromLatitude;

                    cmd.Parameters.Add(new SqlParameter("@SubmittedFromLongitude", SqlDbType.Float));
                    cmd.Parameters["@SubmittedFromLongitude"].Value = hotEvent.SubmittedFromLongitude;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = hotEvent.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    if (hotEvent.Id == 0)
                    {
                        cmd.Parameters["@CreatedDate"].Value = DateTime.Now;
                    }
                    else
                    {
                        cmd.Parameters["@CreatedDate"].Value = hotEvent.CreatedDate;
                    }

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = hotEvent.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = hotEvent.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@WebPort", SqlDbType.Int));
                    cmd.Parameters["@WebPort"].Value = hotEvent.WebPort;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = hotEvent.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = hotEvent.CustomerId;
                    

                    if (!string.IsNullOrEmpty(hotEvent.Notes))
                    {
                        cmd.Parameters.Add(new SqlParameter("@Notes", SqlDbType.NVarChar));
                        cmd.Parameters["@Notes"].Value = hotEvent.Notes;
                    }

                    if (!string.IsNullOrEmpty(hotEvent.ServerNotes))
                    {
                        cmd.Parameters.Add(new SqlParameter("@ServerNotes", SqlDbType.NVarChar));
                        cmd.Parameters["@ServerNotes"].Value = hotEvent.ServerNotes;
                    }
                    try
                    {
                        cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.NVarChar));
                        cmd.Parameters["@Location"].Value = hotEvent.Location;
                    }
                    catch { }
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            hotEvent.Id = id;
                            var audit = new MobileHotEventAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, hotEvent.UpdatedBy);
                            Reflection.CopyProperties(hotEvent, audit);
                            MobileHotEventAudit.InsertMobileHotEventAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateMobileHotEvent", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static IList<MobileHotEvent> GetAllMobileHotEventsByTime(DateTime frmTime, DateTime toTime, string dbConnection)
        {
            var hotEvents = new List<MobileHotEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllMobileHotEventsByTime";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = frmTime;
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = toTime;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventFromSqlReader(reader, dbConnection);
                    hotEvents.Add(hotEvent);

                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }

    }
}
