﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ErrorMessages
    {
        public int Id { get; set; }
        public string MsgIdentifier { get; set; }
        public string ErrorMsg { get; set; }

        public enum ErrMsgs :int
        {
            GenericError = 1,
            ErrDBConnection= 2,
            IncorrectInput = 3,
            NoLiveVideo = 4,
            ErrActivationKey = 5,
            ErrKeyMismatch = 6,
            ErrInvalidLicense = 7,
            ErrLicenseUpload = 8,
            ErrVideoFeed = 9,
            AlarmHandled = 10,
            ErrAccessDenied = 11,
            ErrWebConnection = 13,
            ErrService = 14,
            ErrWrongServerIP = 15,
            ErrLicenseAlreadyUploaded = 16,
            ErrMacAddressNotFound = 17,
            ErrMissingFields = 18,
            ErrUsername = 19,
            ErrSignalR = 20,
            ErrNoDispatchTo = 21,
            ErrVerifierService = 22,
            ErrEditingRow = 23,
            ErrAddingRow = 24,
            ErrDeletingRow = 25,
            ScsTaskTemplateCreated = 26,
            ScsTemplateCreated = 27,
            ScsDispatch = 28,
            ScsNotification = 29,
            DcsTaskTemplate = 30,
            ErrNoData = 31, ScsUserCreated = 32, ScsGroupCreated = 33, ErrNoSearchResults=34
        }
        public static string getErrMsg(ErrMsgs err)
        {
            var errmsg = string.Empty;
            if(ErrMsgs.GenericError == err)
            {
                errmsg = "GenericError";
            }
            return errmsg;
        }
        public static string GetErrorMsgByMsgIdentifier(ErrMsgs err, string dbConnection)
        {
            var errmsg = string.Empty;

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetErrorMsgByMsgIdentifier", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@errmsg", SqlDbType.NVarChar));
                sqlCommand.Parameters["@errmsg"].Value = err.ToString();//getErrMsg(err);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetErrorMsgByMsgIdentifier";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    errmsg = reader["ErrorMsg"].ToString();
                }
                connection.Close();
                reader.Close();
            }
            return errmsg;
        }
    }
}
