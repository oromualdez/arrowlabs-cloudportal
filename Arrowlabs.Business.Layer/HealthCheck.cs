﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{
    public class HealthCheck
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public string MacAddress { get; set; }
        public string PCName { get; set; }
        public string StatusInfo { get; set; }
        public string UpdatedBy { get; set; }
        public string LastLoginDate { get; set; }
        public string imageURL { get; set; }

        public int SiteId { get; set; }
        public enum DeviceStatus : int
        {
            Unknown = -1,
            Offline = 0,
            Online = 1,
            Error = 2,// Yellow
           
        }
        public  bool isOffline { get; set; }
        public bool isOnline { get; set; }
        public bool isError { get; set; }

        public static bool InsertOrUpdateHealthCheck(HealthCheck healthCheck, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (healthCheck != null)
            {

                int id = healthCheck.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateHealthCheck", connection);

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = healthCheck.Status;

                    cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MacAddress"].Value = healthCheck.MacAddress;

                    cmd.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                    cmd.Parameters["@PCName"].Value = healthCheck.PCName;
                   
                    cmd.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                    cmd.Parameters["@LastLoginDate"].Value = healthCheck.LastLoginDate;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = healthCheck.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateHealthCheck";

                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            healthCheck.Id = id;
                            var audit = new Arrowlabs.Mims.Audit.Models.HealthCheckAudit();
                            audit.BaseAudit = Arrowlabs.Mims.Audit.Models.BaseAudit.FillBaseAudit(AppConstants.Account, action, healthCheck.UpdatedBy);
                            Reflection.CopyProperties(healthCheck, audit);
                            Arrowlabs.Mims.Audit.Models.HealthCheckAudit.InsertHealthCheckAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateHealthCheck", ex, dbConnection);
                    }
                }
            }
            return true;
        }


        public static bool ResetConfigSettings(ConfigSettings conClass, string dbConnection)
        {
            if (conClass != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("ResetConfigSettings", connection);

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = conClass.Port;

                    cmd.Parameters.Add(new SqlParameter("@Interval", SqlDbType.Int));
                    cmd.Parameters["@Interval"].Value = conClass.Interval;

                    cmd.Parameters.Add(new SqlParameter("@ServerIP", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerIP"].Value = conClass.ServerIP;

                    cmd.Parameters.Add(new SqlParameter("@ServerConnection", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerConnection"].Value = conClass.ServerConnection;

                    cmd.Parameters.Add(new SqlParameter("@ThirdPartyPort", SqlDbType.Int));
                    cmd.Parameters["@ThirdPartyPort"].Value = conClass.ThirdPartyPort;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "ResetConfigSettings";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static bool DeleteHealthCheckDeviceByMacAddress(string macAddress, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteHealthCheckDeviceByMacAddress", connection);

                cmd.Parameters.Add(new SqlParameter("@macAddress", SqlDbType.NVarChar));
                cmd.Parameters["@macAddress"].Value = macAddress;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteHealthCheckDeviceByMacAddress";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static List<HealthCheck> GetAllDevicesHealthCheck(string dbConnection)
        {
            var healthChecks = new List<HealthCheck>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDevicesHealthCheck‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var healthCheck = new HealthCheck();

                    healthCheck.MacAddress = reader["MacAddress"].ToString();
                    healthCheck.PCName = reader["PCName"].ToString();
                    healthCheck.Status = Convert.ToInt32(reader["Status"].ToString());
                    healthCheck.LastLoginDate = reader["LastLoginDate"].ToString();
                    if (healthCheck.Status == 0)
                    {
                        healthCheck.isOffline = true;
                        healthCheck.StatusInfo = "Offline";
                        healthCheck.imageURL = @"../Images/red.png";
                    }
                    else if (healthCheck.Status == 1)
                    {
                        healthCheck.isOnline = true;
                        healthCheck.StatusInfo = "Online";
                        healthCheck.imageURL = @"../Images/green.png";
                    }
                    else if (healthCheck.Status == 2)
                    {
                        healthCheck.isError = true;
                        healthCheck.StatusInfo = "Warning";
                        healthCheck.imageURL = @"../Images/yellow1.png";
                    }
                    healthChecks.Add(healthCheck);
                }
                connection.Close();
                reader.Close();
            }
            return healthChecks;
        }

        public static HealthCheck GetDeviceHealthCheckbyMacAddress(string macAddress, string dbConnection)
        {
            HealthCheck healthCheck = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetDeviceHealthCheckbyMacAddress‏", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@macAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@macAddress"].Value = macAddress;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    healthCheck = new HealthCheck();

                    healthCheck.MacAddress = reader["MacAddress"].ToString();
                    healthCheck.PCName = reader["PCName"].ToString();
                    healthCheck.Status = Convert.ToInt32(reader["Status"].ToString());
                    healthCheck.LastLoginDate = reader["LastLoginDate"].ToString();
                    if (healthCheck.Status == 0)
                    {
                        healthCheck.isOffline = true;
                        healthCheck.StatusInfo = "Offline";
                        healthCheck.imageURL = @"../Images/red.png";
                    }
                    else if (healthCheck.Status == 1)
                    {
                        healthCheck.isOnline = true;
                        healthCheck.StatusInfo = "Online";
                        healthCheck.imageURL = @"../Images/green.png";
                    }
                    else if (healthCheck.Status == 2)
                    {
                        healthCheck.isError = true;
                        healthCheck.StatusInfo = "Warning";
                        healthCheck.imageURL = @"../Images/yellow1.png";
                    }

                }
                connection.Close();
                reader.Close();
            }
            return healthCheck;
        }
        public static bool UpdateHealthCheckByMacAddress(string pcName, int status, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateHealthCheckByMacAddress", connection);

                cmd.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                cmd.Parameters["@PCName"].Value = pcName;

                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                cmd.Parameters["@Status"].Value = status;

                cmd.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                cmd.Parameters["@LastLoginDate"].Value = DateTime.Now.ToString();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool UpdateHealthByMacAddress(string macAddress, int status, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateHealthByMacAddress", connection);

                cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                cmd.Parameters["@MacAddress"].Value = macAddress;

                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                cmd.Parameters["@Status"].Value = status;

                cmd.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                cmd.Parameters["@LastLoginDate"].Value = DateTime.Now.ToString();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
