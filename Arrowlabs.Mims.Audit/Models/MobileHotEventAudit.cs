﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class MobileHotEventAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }         
        public int EventType { get; set; }
        public string Comments { get; set; }         
        public bool IsDeleted { get; set; }         
        public float SubmittedFromLatitude { get; set; }         
        public float SubmittedFromLongitude { get; set; }         
        public string CreatedBy { get; set; }         
        public DateTime? CreatedDate { get; set; }                  
        public Guid Identifier { get; set; }         
        public string EventTypeName { get; set; }         
        public string AccountName { get; set; }         
        public int WebPort { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        
        public enum HotEventTypes
        {
            None = 0,
            Traffic = 1,
            Accident = 2,
            Damage = 3,
            Theft = 4,
            Vandalism = 5
        }

        public static List<string> GetHotEventTypeList()
        {
            return Enum.GetNames(typeof(HotEventTypes)).ToList();
        }
        private static MobileHotEventAudit GetMobileHotEventAuditFromSqlReader(SqlDataReader reader)
        {
            var hotEvent = new MobileHotEventAudit();
            hotEvent.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                hotEvent.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("EventType") && reader["EventType"] != DBNull.Value)
                hotEvent.EventType = Convert.ToInt32(reader["EventType"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                hotEvent.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                hotEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (columns.Contains("Comments") && reader["Comments"] != DBNull.Value)
                hotEvent.Comments = reader["Comments"].ToString();
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                hotEvent.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (columns.Contains("SubmittedFromLatitude") && reader["SubmittedFromLatitude"] != DBNull.Value)
                hotEvent.SubmittedFromLatitude = Convert.ToSingle(reader["SubmittedFromLatitude"].ToString());
            if (columns.Contains("SubmittedFromLongitude") && reader["SubmittedFromLongitude"] != DBNull.Value)
                hotEvent.SubmittedFromLongitude = Convert.ToSingle(reader["SubmittedFromLongitude"].ToString());
            if (columns.Contains("Identifier") && reader["Identifier"] != DBNull.Value)
                hotEvent.Identifier = new Guid(reader["Identifier"].ToString());
            if (columns.Contains("WebPort") && reader["WebPort"] != DBNull.Value)
                hotEvent.WebPort = Convert.ToInt32(reader["WebPort"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return hotEvent;
        }
        public static List<MobileHotEventAudit> GetAllMobileHotEventAudit(string dbConnection)
        {
            var hotEvents = new List<MobileHotEventAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMobileHotEventAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventAuditFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }
        public static int InsertMobileHotEventAudit(MobileHotEventAudit hotEvent, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(hotEvent.BaseAudit, dbConnection);

            int id = 0;
            if (hotEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMobileHotEventAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = hotEvent.Id;

                    cmd.Parameters.Add("@EventType", SqlDbType.Int).Value = hotEvent.EventType;

                    cmd.Parameters.Add("@Comments", SqlDbType.NVarChar).Value = hotEvent.Comments;

                    cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = hotEvent.IsDeleted;

                    cmd.Parameters.Add("@SubmittedFromLatitude", SqlDbType.Float).Value = hotEvent.SubmittedFromLatitude;

                    cmd.Parameters.Add("@SubmittedFromLongitude", SqlDbType.Float).Value = hotEvent.SubmittedFromLongitude;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = hotEvent.CreatedBy;

                  
                    if (hotEvent.Id == 0)
                    {
                        cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    }
                    else
                    {
                        cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = hotEvent.CreatedDate;
                    }

                    cmd.Parameters.Add("@Identifier", SqlDbType.UniqueIdentifier).Value = hotEvent.Identifier;

                    cmd.Parameters.Add("@WebPort", SqlDbType.Int).Value = hotEvent.WebPort;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = hotEvent.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = hotEvent.CustomerId;
                    

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
