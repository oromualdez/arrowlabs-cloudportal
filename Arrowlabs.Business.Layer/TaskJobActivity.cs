﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public enum TaskJobAction
    {
        OnRoute = 0,
        InProgress = 1,
        Pause = 2,
        Resume = 3,
        Complete = 4
    }
    public class TaskJobActivity
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int JobAction { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string Latitude { get; set; }
        public string Longtitude { get; set; }

        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public string CustomerUName { get; set; }
        public string CustomerFullName { get; set; }

        private static TaskJobActivity GetTaskJobActivityFromSqlReader(SqlDataReader reader)
        {
            var taskEventHistory = new TaskJobActivity();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (reader["Id"] != DBNull.Value)
                taskEventHistory.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["TaskId"] != DBNull.Value)
                taskEventHistory.TaskId = Convert.ToInt32(reader["TaskId"].ToString());

            if (reader["JobAction"] != DBNull.Value)
                taskEventHistory.JobAction = Convert.ToInt32(reader["JobAction"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                taskEventHistory.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                taskEventHistory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                taskEventHistory.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                taskEventHistory.CustomerId = Convert.ToInt32(reader["CustomerId"]);

            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                taskEventHistory.Latitude = reader["Latitude"].ToString();

            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                taskEventHistory.Longtitude = reader["Longitude"].ToString();

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                taskEventHistory.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("CustomerFullName") && reader["CustomerFullName"] != DBNull.Value)
                taskEventHistory.CustomerFullName = reader["CustomerFullName"].ToString();


            return taskEventHistory;
        }

        public static double GetTotalActivityTimebyTaskId(UserTask taskid, string dbcon)
        {
            var total = Arrowlabs.Business.Layer.TaskJobActivity.GetTaskJobActivityByTaskId(taskid.Id, dbcon);
            DateTime start = DateTime.MinValue;
            DateTime end = DateTime.MinValue;
            double tot = 0;
            var first = false;
            var hasOnroute = false;
            foreach (var t in total)
            {
                if (!first)
                {
                    if (t.JobAction == 0)
                    {
                        hasOnroute = true;
                        if (t.JobAction == 2) //Pause
                            end = t.CreatedDate.Value;
                        else if (t.JobAction == 0)//OnRoute
                            start = t.CreatedDate.Value;
                        else if (t.JobAction == 3) //Resume
                            start = t.CreatedDate.Value;
                        else if (t.JobAction == 4) //Complete
                            end = t.CreatedDate.Value;
                    }
                    else
                    {
                        hasOnroute = false;
                        if (t.JobAction == 2) //Pause
                            end = t.CreatedDate.Value;
                        else if (t.JobAction == 1)//InPro
                            start = t.CreatedDate.Value;
                        else if (t.JobAction == 3) //Resume
                            start = t.CreatedDate.Value;
                        else if (t.JobAction == 4) //Complete
                            end = t.CreatedDate.Value;
                    }
                    first = true;
                }
                else
                {
                    if (hasOnroute)
                    {
                        if (t.JobAction == 2) //Pause
                            end = t.CreatedDate.Value;
                        else if (t.JobAction == 0)//OnRoute
                            start = t.CreatedDate.Value;
                        else if (t.JobAction == 3) //Resume
                            start = t.CreatedDate.Value;
                        else if (t.JobAction == 4) //Complete
                            end = t.CreatedDate.Value;
                    }
                    else
                    {
                        if (t.JobAction == 2) //Pause
                            end = t.CreatedDate.Value;
                        else if (t.JobAction == 1)//InPro
                            start = t.CreatedDate.Value;
                        else if (t.JobAction == 3) //Resume
                            start = t.CreatedDate.Value;
                        else if (t.JobAction == 4) //Complete
                            end = t.CreatedDate.Value;
                    }
                }
                if (start != DateTime.MinValue && end != DateTime.MinValue)
                {
                    var tot1 = end.Subtract(start).TotalMinutes;
                    tot = tot + tot1;
                    start = DateTime.MinValue;
                    end = DateTime.MinValue;

                }
            }
            return tot;
        }

        public static bool InsertTaskJobActivity(TaskJobActivity eventHistory, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (eventHistory.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (eventHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskJobActivity", connection);

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = eventHistory.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@JobAction", SqlDbType.Int));
                    cmd.Parameters["@JobAction"].Value = eventHistory.JobAction;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;
                     
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = eventHistory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = eventHistory.CustomerId;


                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = eventHistory.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = eventHistory.Latitude;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new Arrowlabs.Mims.Audit.Models.TaskJobActivityAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, eventHistory.CreatedBy);
                            Reflection.CopyProperties(eventHistory, audit);
                            Arrowlabs.Mims.Audit.Models.TaskJobActivityAudit.InsertTaskJobActivityAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertTaskJobActivityAudit", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static List<TaskJobActivity> GetTaskJobActivityByTaskId(int eventId, string dbConnection)
        {
            var eventHistories = new List<TaskJobActivity>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTaskJobActivityByTaskId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = eventId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTaskJobActivityByTaskId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetTaskJobActivityFromSqlReader(reader);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

    }
}
