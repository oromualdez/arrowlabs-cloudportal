﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public enum PushNotificationDeviceType
    {
        None = 0,
        Apple = 1,
        Android = 2
    }
    public class PushNotificationDevice
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string UpdatedBy { get; set; }
        public string DeviceToken { get; set; }
        public int DeviceType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool IsServerPortal { get; set; }

        public bool IsWatch { get; set; }

        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public string CustomerUName { get; set; }

        public enum PushNotificationType
        {
            ChatMessage = 0,
            IncidentNotification = 1,
            TaskNotification = 2,
            AppUpdatenotification = 3,
            VoiceMessage = 4
        }

        private static PushNotificationDevice GetPushNotificationDeviceFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var pushNotificationDevice = new PushNotificationDevice();

            if (columns.Contains("Username") && reader["Username"] != DBNull.Value)
                pushNotificationDevice.Username = reader["Username"].ToString();

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                pushNotificationDevice.CustomerUName = reader["CustomerUName"].ToString();


            if (columns.Contains("DeviceToken") && reader["DeviceToken"] != DBNull.Value)
                pushNotificationDevice.DeviceToken = reader["DeviceToken"].ToString();

            if (columns.Contains("DeviceType") && reader["DeviceType"] != DBNull.Value)
                pushNotificationDevice.DeviceType = Convert.ToInt16(reader["DeviceType"].ToString());

            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                pushNotificationDevice.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("UpdatedDate") && reader["UpdatedDate"] != DBNull.Value)
                pushNotificationDevice.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (columns.Contains("IsServerPortal") && reader["IsServerPortal"] != DBNull.Value)
                pushNotificationDevice.IsServerPortal = Convert.ToBoolean(reader["IsServerPortal"]);
            else
                pushNotificationDevice.IsServerPortal = false;

            if (columns.Contains("IsWatch") && reader["IsWatch"] != DBNull.Value)
                pushNotificationDevice.IsWatch = Convert.ToBoolean(reader["IsWatch"]);
            else
                pushNotificationDevice.IsWatch = false;


            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                pushNotificationDevice.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                pushNotificationDevice.CustomerId = Convert.ToInt32(reader["CustomerId"]);

            return pushNotificationDevice;
        }

        public static bool InsertorUpdatePushNotificationDevice(PushNotificationDevice pushNotificationDevice, 
            string dbConnection, string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (pushNotificationDevice != null)
            {
                if (!string.IsNullOrEmpty(pushNotificationDevice.Username))
                {
                    int id = pushNotificationDevice.Id;
                    var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdatePushNotificationDevice", connection);
                        cmd.CommandType = CommandType.StoredProcedure;


                        cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                        cmd.Parameters["@Username"].Value = pushNotificationDevice.Username;

                        cmd.Parameters.Add(new SqlParameter("@DeviceToken", SqlDbType.NVarChar));
                        cmd.Parameters["@DeviceToken"].Value = pushNotificationDevice.DeviceToken;

                        cmd.Parameters.Add(new SqlParameter("@DeviceType", SqlDbType.Int));
                        cmd.Parameters["@DeviceType"].Value = pushNotificationDevice.DeviceType;

                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreatedDate"].Value = pushNotificationDevice.CreatedDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = pushNotificationDevice.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@IsServerPortal", SqlDbType.Bit));
                        cmd.Parameters["@IsServerPortal"].Value = pushNotificationDevice.IsServerPortal;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = pushNotificationDevice.SiteId;

                        var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;


                        cmd.ExecuteNonQuery();

                        if (id == 0)
                            id = (int)returnParameter.Value;

                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {
                                pushNotificationDevice.Id = id;
                                var audit = new PushNotificationDeviceAudit();
                                audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, pushNotificationDevice.UpdatedBy);
                                Reflection.CopyProperties(pushNotificationDevice, audit);
                                PushNotificationDeviceAudit.InsertPushNotificationDeviceAudit(audit, auditDbConnection);
                            }
                        }
                        catch (Exception ex)
                        {
                            MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdatePushNotificationDevice", ex, auditDbConnection);
                        }

                    }
                }
            }
            return true;
        }

        public static List<PushNotificationDevice> GetAllPushNotificationDevices(string dbConnection)
        {
            var pushNotificationDevices = new List<PushNotificationDevice>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPushNotificationDevices", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    pushNotificationDevices.Add(GetPushNotificationDeviceFromSqlReader(reader));
                }
                connection.Close();
                reader.Close();
            }
            return pushNotificationDevices;
        }

        public static List<PushNotificationDevice> GetAllPushNotificationDevicesByDeviceType(int deviceType,
            string dbConnection)
        {
            var pushNotificationDevices = new List<PushNotificationDevice>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPushNotificationDevicesByDeviceType", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@DeviceType", SqlDbType.Int)).Value = deviceType;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    pushNotificationDevices.Add(GetPushNotificationDeviceFromSqlReader(reader));
                }
                connection.Close();
                reader.Close();
            }
            return pushNotificationDevices;
        }

        public static PushNotificationDevice GetPushNotificationDeviceByUsername(string username,
            string dbConnection)
        {
            var pushNotificationDevice = new PushNotificationDevice();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetPushNotificationDeviceByUsername", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar)).Value = username;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    pushNotificationDevice = GetPushNotificationDeviceFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return pushNotificationDevice;
        }

        public static List<PushNotificationDevice> GetAllPushNotificationDevicesByGroupId(int groupId,
            string dbConnection)
        {
            var pushNotificationDevices = new List<PushNotificationDevice>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPushNotificationDevicesByGroupId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int)).Value = groupId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    pushNotificationDevices.Add(GetPushNotificationDeviceFromSqlReader(reader));
                }
                connection.Close();
                reader.Close();
            }
            return pushNotificationDevices;
        }

        public static void UpdatePushNotificationDeviceByUsername(string userName, int siteId, string dbConnection, bool isServer = true)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdatePushNotificationDeviceByUsername", connection);
                cmd.CommandType = CommandType.StoredProcedure;


                cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                cmd.Parameters["@Username"].Value = userName;

                cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                cmd.Parameters["@SiteId"].Value = siteId;

                cmd.Parameters.Add(new SqlParameter("@IsServer", SqlDbType.Bit));
                cmd.Parameters["@IsServer"].Value = isServer;
                cmd.ExecuteNonQuery();
            }
        }
    }
}
