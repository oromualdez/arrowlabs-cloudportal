﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class NotificationAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }        
        public string Description { get; set; }   
        public bool IsDeleted { get; set; }        
        public DateTime? CreatedDate { get; set; }        
        public string CreatedBy { get; set; }        
        public string LocationName { get; set; }        
        public string SubLocationName { get; set; }                
        public int AssigneeType { get; set; }        
        public int AssigneeId { get; set; }        
        public int CreatorId { get; set; }        
        public string AssigneeName { get; set; }
        public string Instruction { get; set; }
        public string Topic { get; set; }
        public bool IsRead { get; set; }
        public int LookUpUserId { get; set; }
        public int SiteId { get; set; }
        public NotificationTypes NotificationType { get; set; }
        public string Prefix { get; set; }

        public int CustomerId { get; set; }
        
        public enum NotificationAssigneeType
        {
            User = 0,
            Location = 1,
            Group = 2,
            Device = 3,
            All = 4,
            AdminAll = 5
        }
        public enum NotificationTypes
        {
            None = -1,
            Alarm = 0,
            Notification = 1,
            Chat = 2
        }

        public static List<string> GetNotificationAssigneeTypes()
        {
            return Enum.GetNames(typeof(NotificationAssigneeType)).ToList();
        }
        private static NotificationAudit GetNotificationAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            
            var notification = new NotificationAudit();
            notification.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Description") && reader["Description"] != DBNull.Value)
                notification.Description = reader["Description"].ToString();
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                notification.IsDeleted = (String.IsNullOrEmpty(reader["IsDeleted"].ToString())) ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("AssigneeType") && reader["AssigneeType"] != DBNull.Value)
                notification.AssigneeType = Convert.ToInt32(reader["AssigneeType"].ToString());
            if (columns.Contains("AssigneeId") && reader["AssigneeId"] != DBNull.Value)
                notification.AssigneeId = Convert.ToInt32(reader["AssigneeId"].ToString());
            if (columns.Contains("AssigneeName") && reader["AssigneeName"] != DBNull.Value)
                notification.AssigneeName = reader["AssigneeName"].ToString();
            if (columns.Contains("IsRead") && reader["IsRead"] != DBNull.Value)
                notification.IsRead = Convert.ToBoolean(reader["IsRead"].ToString());
            if (columns.Contains("LookUpUserId") && reader["LookUpUserId"] != DBNull.Value)
                notification.LookUpUserId = Convert.ToInt32(reader["LookUpUserId"].ToString());
            if (columns.Contains("NotificationType") && reader["NotificationType"] != DBNull.Value)
                notification.NotificationType = (NotificationTypes)Convert.ToInt32(reader["NotificationType"].ToString());
            if (columns.Contains("Topic") && reader["Topic"] != DBNull.Value)
                notification.Topic = reader["Topic"].ToString();
            if (columns.Contains("Prefix") && reader["Prefix"] != DBNull.Value)
                notification.Prefix = reader["Prefix"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return notification;
        }
        public static List<NotificationAudit> GetAllNotificationAudit(string dbConnection)
        {
            var notifications = new List<NotificationAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationAuditFromSqlReader(reader);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        public static bool InsertNotificationAudit(NotificationAudit notification, string dbConnection)
        {
            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertNotificationAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(notification.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = notification.Id;

                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = notification.Description;

                    cmd.Parameters.Add("@AssigneeType", SqlDbType.Int).Value = notification.AssigneeType;

                    cmd.Parameters.Add("@AssigneeId", SqlDbType.Int).Value = notification.AssigneeId;

                    cmd.Parameters.Add("@AssigneeName", SqlDbType.NVarChar).Value = notification.AssigneeName;

                    cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = notification.IsDeleted;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = notification.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = notification.CreatedBy;

                    cmd.Parameters.Add("@NotificationType", SqlDbType.Int).Value = notification.NotificationType;

                    cmd.Parameters.Add("@Topic", SqlDbType.NVarChar).Value = notification.Topic;

                    cmd.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = notification.Prefix;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = notification.CustomerId;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = notification.SiteId;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
