﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewTask.ascx.cs" Inherits="ArrowLabs.Licence.Portal.Controls.Modals.NewTask" %>
			<div aria-hidden="true" aria-labelledby="newDocument" role="dialog" tabindex="-1" id="newDocument" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button id="backTaskClose" aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <button id="backTicketCardClose" style="display:none;" aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="$('#ticketingViewCard').modal('show')"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text" id="newtaskHeader">CREATE NEW TASK</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5">
                                   <div class="row" style="display:<%=customersDisplay%>;">
									<div class="col-md-12">
                                        <label class="select select-o">
                                           <select id="customerslst" onchange="customersOnChange(this);">
											</select>
										 </label>

									</div>
								</div>
                                <div class="row" style="display:<%=customersDisplay%>;">
									<div class="col-md-12">
                                        <label class="select select-o">
                                           <select id="contractslst" >
                                               <option value="0" ><%=selectContractPlaceholder%></option>
											</select>
										 </label>
									</div>

								</div>
                                    <div class="row">
									<div class="col-md-12">
                                        <label class="select select-o">
    <select id="projectlst" onchange="projectlstOnChange(this);"> 

                                               <option value="0" ><%=selectProjectPlaceholder%></option>
											</select>
										 </label>

									</div>
                                        
								</div>
								<div class="row">
									<div class="col-md-7">
										<input placeholder="<%=namePlaceholder%>" id="tbNewTaskName" class="form-control">
									</div>
                                    <div class="col-md-5">
                                        <label class="select select-o">
                                           <select id="taskTypeSelect" runat="server">
											</select>
										 </label>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-12">
                                        <textarea placeholder="<%=descPlaceholder%>" id="tbNewDescription" class="form-control" rows="5"></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="locationSelect" onchange="locationOnChange(this);" runat="server">
											</select>
										 </label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<input id="tbNewLongitude" placeholder="<%=longiPlaceholder%>"  class="form-control">
									</div>
									<div class="col-md-6">
										<input id="tbNewLatitude" placeholder="<%=latiPlaceholder%>"  class="form-control">
									</div>									
								</div>		
                                <div class="row">
									<div class="col-md-6">
										<div class="form-group">
                                          <div class="input-group input-group-in">

                                            <input id="newtaskCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
									</div>
                                     <div class="col-md-3">
                                                                                    <label class="select select-o">
                                         <select id="newtaskTime" >
                                              <option>00</option>
                                             <option>01</option>
                                             <option>02</option>
                                             <option>03</option>
                                             <option>04</option>
                                             <option>05</option>
                                             <option>06</option>
                                             <option>07</option>
                                             <option>08</option>
                                             <option>09</option>
                                             <option>10</option>
                                             <option>11</option>
                                             <option>12</option>
                                             <option>13</option>
                                             <option>14</option>
                                             <option>15</option>
                                             <option>16</option>
                                             <option>17</option>
                                             <option>18</option>
                                             <option>19</option>
                                             <option>20</option>
                                             <option>21</option>
                                             <option>22</option>
                                             <option>23</option>
                                            
                                         </select>
                                        </label>
                                        </div> 
                                        <div class="col-md-3">
                                                                                    <label class="select select-o">
                                         <select id="newtaskTimeMinute" >
                                                                                        <option>00</option>
                                             <option>01</option>
                                             <option>02</option>
                                             <option>03</option>
                                             <option>04</option>
                                             <option>05</option>
                                             <option>06</option>
                                             <option>07</option>
                                             <option>08</option>
                                             <option>09</option>
                                             <option>10</option>
                                             <option>11</option>
                                             <option>12</option>
                                             <option>13</option>
                                             <option>14</option>
                                             <option>15</option>
                                             <option>16</option>
                                             <option>17</option>
                                             <option>18</option>
                                             <option>19</option>
                                             <option>20</option>
                                             <option>21</option>
                                             <option>22</option>
                                             <option>23</option>
                                             <option>24</option>
                                             <option>25</option>
                                             <option>26</option>
                                             <option>27</option>
                                             <option>28</option>
                                             <option>29</option>
                                             <option>30</option>
                                             <option>31</option>
                                             <option>32</option>
                                             <option>33</option>
                                             <option>34</option>
                                             <option>35</option>
                                             <option>36</option>
                                             <option>37</option>
                                             <option>38</option>
                                             <option>39</option>
                                             <option>40</option>
                                             <option>41</option>
                                             <option>42</option>
                                             <option>43</option>
                                             <option>44</option>
                                             <option>45</option>
                                             <option>46</option>
                                             <option>47</option>
                                             <option>48</option>
                                             <option>49</option>
                                             <option>50</option>
                                             <option>51</option>
                                             <option>52</option>
                                             <option>53</option>
                                             <option>54</option>
                                             <option>55</option>
                                             <option>56</option>
                                             <option>57</option>
                                             <option>58</option>
                                             <option>59</option>
                                         </select>
                                        </label>
                                        </div>
										<div class="col-md-6" style="display:none;"><!--/recurringSelectDIV-->
									    <label class="select select-o">
                                           <select id="newrecurringSelect">
											  <option>Recurring</option>
											  <option>Daily</option>
											  <option>Weekly</option>
                                              <option>Monthly</option>
											</select>
										 </label>
									</div>									
								</div>		
								<div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="checklistSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>		
                                <div class="row">
                                <div class='row' style="padding-top:0px">
                                                                                                      <div class="nice-checkbox inline-block no-vmargin">
                                                                    <input onclick="assignMySelf()" type="checkbox" id="myselfCheck" name="niceCheck">
                                                                    <label for="myselfCheck"><%=assigntomyselfPlaceholder%></label> 
                                                                                                          </div>
                                           </div>
                                </div>	
                                <div class="row" id="assigneeDIV">
									<div class="col-md-4">
										<label class="select select-o">
                                           <select id="taskSelectAssigneeType" onchange="taskselectAssigneeTypeChange()">
											  <option><%=userPlaceholder%></option>
											  <option style="display:<%=grpOptionView%>"><%=groupPlaceholder%></option>
                                               <option><%=multiplePlaceholder%></option>
											</select>
										 </label>
									</div>
									<div id="usersearchSelectDIV" class="col-md-8">
									<select id="usersearchSelect" class="selectpicker form-control"  data-live-search="true" runat="server">
                                    </select>
									</div>	

                                    <div id="groupsearchSelectDIV" style="display:none;" class="col-md-8">
                                        <select id="groupsearchSelect"  class="selectpicker form-control"  data-live-search="true" runat="server">
                                        </select>	
                                    </div>						
								</div>		
                                <div class="row">
									<div class="col-md-12">
										<label class="select select-o">
                                           <select id="prioritySelect">
                                              <option><%=severePlaceholder%></option>
											  <option><%=highPlaceholder%></option>
											  <option><%=mediumPlaceholder%></option>
											  <option><%=lowPlaceholder%></option>
											</select>
										 </label>
									</div>
								</div>	
                                                                <div class="row">
                                <div class='row' style="padding-top:0px">
                                                                                                      <div class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="requiresignatureCheck" name="niceCheck">
                                                                    <label for="requiresignatureCheck"><%=requiresignaturePlaceholder%></label> 
                                                                                                          </div>
                                           </div>
                                </div>						
							</div>
							<div class="col-md-7" id="map_canvasLocationDIV">
                                <input id="pac-input" class="controls" type="text" placeholder="Search Location">
                                <div id="map_canvasLocation" style="width:100%;height:442px;"></div>
						    </div>
                            <div id="devicesearchSelectDIV" style="display:none;" class="col-md-7">
                                                                <div class="row">
                                    <div class="horizontal-navigation">
                                        <div class="panel-control">
                                                <ul class="nav nav-tabs" id="UsersToDispatchList">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                    </div>
                                </div>
                                <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MY USERS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="assignUsersTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">USER
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
<%--                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">LOCATION<i class="fa fa-sort ml-2x"></i>
                                                                </th>--%>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">ACTION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                            </div>	
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li id="backTaskLi" style="display:none;"><a href="#" class="capitalize-text" data-dismiss="modal" onclick="$('#taskDocument').modal('show')">BACK</a>
									</li> 
                                    <li id="backTicketCardLi" style="display:none;"><a href="#" class="capitalize-text" data-dismiss="modal" onclick="$('#ticketingViewCard').modal('show')">BACK</a>
									</li>
                                    <li ><a href="#" class="capitalize-text" onclick="sendTask();"><%=sendPlaceholder%></a>
									</li>
                                    <li id="saveAsTemplateLi"><a href="#" class="capitalize-text" onclick="saveTemplateTask()"><%=saveastemplatePlaceholder%></a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>		