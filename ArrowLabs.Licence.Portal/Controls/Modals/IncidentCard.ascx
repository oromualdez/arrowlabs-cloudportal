﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncidentCard.ascx.cs" Inherits="ArrowLabs.Licence.Portal.Controls.Modals.IncidentCard" %>
<script>
    function nextbackInci() {
        document.getElementById("incirotationDIV1").style.display = "block";
        document.getElementById("incirotationDIV2").style.display = "block";
    }
</script>            
<div aria-hidden="true" aria-labelledby="viewDocument1" role="dialog" tabindex="-1" id="viewDocument1" class="modal fade videoModal" style="display: none;">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <div class="row">
                        <div class="col-md-11">
                            <span class="circle-point-container pull-left mt-2x mr-1x"><span id="headerImageClass" class="circle-point circle-point-orange"></span></span>
                            <h4 class="modal-title capitalize-text" id="incidentNameHeader"></h4>
                        </div>
                        <div class="col-md-1">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>                      
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <p>Created by: <span id="usernameSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Time: <span id="timeSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Type: <span id="typeSpan"></span></p>
                        </div>                          
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <p>Status: <span id="statusSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Location: <span id="locSpan"></span></p>
                        </div>                          
                      </div>                      
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5" style="border-right: 1px #bbbbbb solid;">
                                <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" id="demo3-tabs">
                                            <li class="active" id="liInfo"><a href="#info-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="liActi"><a href="#activity-tab" onclick="tracebackOn('incident')" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="liAtta"><a href="#attachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>                                           
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tab-content">
                                        <div class="tab-pane fade active in" id="info-tab">
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Received by:</b> <span class="red-color" id="receivedBySpan"></span></p>                                              
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Phone number:</b> <span class="red-color" id="phonenumberSpan"></span></p>                                                
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Email:</b> <span class="red-color" id="emailSpan"></span></p>                                             
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Description:</b></p>
                                                    <p id="descriptionSpan"></p>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Instruction:</b></p>
                                                    <p id="instructionSpan"></p>
                                                </div>
                                            </div>  
                                                <div class="row">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Notes:</b></p>
                                                    <p id="notesSpan"></p>
                                                </div>
                                            </div> 
                                            <div id="escalateionSpanDiv" class="row" style="display:none">  
                                                <div class="col-md-12">
                                                    <p id="escalationHead" class="red-color"><b>Escalation Notes:</b></p>
                                                    <p id="escalatedSpan"></p>
                                                </div>
                                            </div>  
                                                <div class="row" id="checklistDIV" style="display:none;">
                                                  <div class="col-md-12">
													<p class="red-color"><b>Dispatched Task : </b><b id="taskItemsnameSpan"></b></p>
                                                      <div class="row horizontal-navigation">
                                                        <div class="panel-control">
                                                                <ul class="nav nav-tabs" id="taskItemsList">

                                                                </ul>
                                                            <!-- /.nav -->
                                                        </div>
                                                    </div>
												</div>
                                                </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                        </div>
                                        </div>
                                        <div class="tab-pane fade" id="activity-tab">
                                                    <div id="divIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                                    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                      
                                        </div>
                                        <div class="tab-pane fade" id="attachments-tab" onclick="startRot();nextbackInci();">        
                                             <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="attachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                            </div>                      
                                        </div>      
                                            </div>                                
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1"  style="width:40px;">

                                <i id="incirotationDIV1" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb" class="fa fa-angle-double-left  fa-2x" onclick="incibackImg()"></i>

                            </div>

                            <div class="col-md-5" id="divAttachmentHolder" style="width:440px;margin-top:4px;border-right: 1px #bbbbbb solid;border-left: 1px #bbbbbb solid;">
                                <div class="row" id="UsersToDispatchListDIV" style="display:none;">
                                    <div class="horizontal-navigation">
                                        <div class="panel-control">
                                                <ul class="nav nav-tabs" id="UsersToDispatchList">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade active in" id="location-tab">
                                    <div id="map_canvasIncidentLocation" style="width:100%;height:378px;"></div>
                                    <div id="divAttachment" onclick="startRot()" class="overlapping-map-image">
                                    </div>
<%--                                    <div class="overlapping-map-image" style="top:30px;left:150px;height:5px;">
 
                                    </div>--%>
                                </div>
                                <div class="tab-pane fade " id="incidentAssign-user-tab">
                                        <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MY USERS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    
                                                        <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="assignUsersTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">USER
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
<%--                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">LOCATION<i class="fa fa-sort ml-2x"></i>
                                                                </th>--%>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">ACTION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="tab-pane fade " id="incidentNext-user-tab">
                                     <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:378px;">
                                    <p class="red-color"><b>Dispatch Options:</b></p>
                                    <div class="row"  style="display:none;">
                                       <div class="col-md-4">
                                           <div class='row nice-checkbox'>
                                               <input onclick="showViewCamDuration()" id='cameraCheck' type='checkbox' name='niceCheck'>
                                               <label for='cameraCheck'>Camera</label>
                                           </div>

                                       </div>
                                       <div class="col-md-8">
                                           <div class="row mb-1x" id="cameraselectDiv">
                                         <label class="select select-o">
                                            <select id="cameraSelect" runat="server">
                                            </select>
                                         </label>
                                               </div>

                                    </div>
                                    </div>
                                                                                                            <div id="camViewDurationDiv" style="display:none;" class="row">
                                       <div class="col-md-4">
                                           Access Duration
                                       </div>
                                       <div class="col-md-8">
                                     <div  style="margin-top:0px;margin-bottom:7px;" class="row horizontal-navigation">
                                                <a style="font-size:14px;" href="#" onclick="viewdurationselect(15)"><i id="camviewfilter1" class="fa fa-square-o"></i>15 MIN</a>
                                                |<a style="font-size:14px;" href="#" onclick="viewdurationselect(30)"><i id="camviewfilter2" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:14px;" href="#" onclick="viewdurationselect(45)"><i id="camviewfilter3" class="fa fa-square-o"></i>45 MIN</a>
									            |<a style="font-size:14px;" href="#" onclick="viewdurationselect(60)"><i id="camviewfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                |<a style="font-size:14px;" href="#" onclick="viewdurationselect(120)"><i id="camviewfilter5" class="fa fa-square-o"></i>2 HOUR</a>
                                </div>
                                    </div>
                                    </div>

                                                                        <div class="row" <%=taskDisplay%>>
                                       <div class="col-md-4">
                                                                                      <div class='row nice-checkbox' >
                                               <input id='TaskCheck' type='checkbox' name='niceCheck'>
                                               <label for='TaskCheck'>Task</label>
                                           </div>
                                       </div>
                                       <div class="col-md-8">
                                                                                      <div class="row">
                                        <label class="select select-o" >
                                            <select id="selectTaskTemplate" runat="server">
                                            </select>
                                         </label>
                                               </div>
                                    </div>
                                    </div>
                                                                    <div class="row"  style="display:none;"> 

                                               <div class="text-center">Attachment Photo</div>

                                    </div>
                                <div class="row">
                                                      <div style="height:50px;" enctype="multipart/form-data" id="dz-upload" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                                        <div class="dz-message">
                                                          <h1>BROWSE</h1>
                                                        </div>
                                                      </div>

                                    
                                         </div> 
 <div style="height:50px;display:none;" enctype="multipart/form-data" id="dz-post" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message"> 
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </div> 
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="Instructions" id="selectinstructionTextarea" class="form-control" rows="6"></textarea>
                                    </div>
                                </div>       
                                                                                                                                 <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                        </div>                       
                                </div>
                        </div>

                            <div class="col-md-1"  style="width:40px;">
                                 <i id="incirotationDIV2" class="fa fa-angle-double-right  fa-2x" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb;"  onclick="incinextImg()"></i>

                             </div>

                    </div>
                        <div class="row">
                                                        <div class="col-md-5">

                            </div>
                                    <div class="col-md-7" style="display:none;border-left:0px;" id="incidentAudioDIV"> 
                                        <audio id="incidentAudio" style="width: 100%;" width="100%" controls><source id="incidentAudioSrc" type="audio/mpeg"/></audio>
                                    </div>
                        </div>

                      <div class="row" id="pdfloadingAccept" style="padding-bottom:75px;display:none;">
                            <div class="col-md-7">

                            </div>
                            <div class="col-md-5" style="margin-bottom:-50px">
                                <p class="red-color text-center" style="font-size:12px"><b id="gnNote">GENERATING REPORT</b></p>
                                                                            <div id="myProgress">
                                              <div id="myBar"></div>
                                            </div>
                            </div>
                        </div>
                  </div><!-- /.modal-content -->
                                          <div class="modal-footer">
                                                                      <div class="row" id="rowtracebackUser" style="display:none;">
                        <div class="col-md-5">
                            </div>
                               <div class="col-md-7" style="text-align:left;margin-top:-8px;">
 <a style="font-size:16px;" href="#" id="incidentplaypausefilter" onclick="incidentplaypauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1,'incident')"><i id="filter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5,'incident')"><i id="filter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30,'incident')"><i id="filter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60,'incident')"><i id="filter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                                    User:
							        <div style="margin-top:10px;margin-bottom:10px;" class="panel-control" id="tracebackUserFilter">
							        </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                 <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs" id="initialOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">UNHANDLE</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="handledClick()">HANDLE</a>
                                    </li>
                                                                        <li>
<a href="#" id="initialAttach" onclick="document.getElementById('dz-post').click()">ATTACH</a>

                                    </li>	
                                    <li <%=escalatedView%>><a href="#" class="capitalize-text" onclick="escalateOptionClick()">ESCALATE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="escalateOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="escalateClick()">ESCALATE</a>
                                    </li>
                                    <li>

                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="dispatchOptionsDiv" style="display:none;">
                                    <li>
                                        <a href="#" id="dispatchAttach" onclick="document.getElementById('dz-post').click()">ATTACH</a>
                                    </li>	
                                    <li><a href="#"   onclick="IncidentStateChange('Release')">RELEASE</a>
                                    </li>
                                    <li id="disResolveLi"><a href="#" class="capitalize-text" onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="handleOptionsDiv" style="display:none;"> 
                                    <li id="parkLi"><a href="#"  onclick="IncidentStateChange('Park')" >PARK</a>
                                    </li>
                                    <li style="display:<%=dispatchDisplay%>;"><a  class="capitalize-text" data-target="#incidentAssign-user-tab" data-toggle="tab" onclick="nextLiClick()">DISPATCH FROM USERS</a>
                                    </li>
                                    <li style="display:<%=dispatchDisplay%>;"><a  class="capitalize-text" data-target="#location-tab" data-toggle="tab" onclick="nextMapLiClick()">DISPATCH FROM MAP</a>
                                    </li>
                                    <li id="nextLi" style="display:none"><a  class="capitalize-text" data-target="#incidentNext-user-tab" onclick="finishLiClick()" data-toggle="tab" >NEXT</a>
                                    </li>
                                    <li id="finishLi" style="display:none"><a  class="capitalize-text" onclick="dispatchIncident()">DISPATCH</a>
                                    </li>
                                    <li id="resolveLi"><a onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                    <li>
                                        <a href="#" id="handleOptionsAttach" onclick="document.getElementById('dz-post').click()">ATTACH</a>
                                    </li>	
                                </ul>
                                <ul class="nav nav-tabs" id="completedOptionsDiv2" style="display:none;">
                                    <li>
                                        <a href="#" id="completedAttach" onclick="document.getElementById('dz-post').click()">ATTACH</a>
                                    </li>	
                                    <li><a href="#"   onclick="IncidentStateChange('Reject')">REJECT</a>
                                    </li>
                                    <li id="compResolveLi2"><a href="#" class="capitalize-text"  onclick="IncidentStateChange('Resolve')">RESOLVE</a>
                                    </li>
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="completedOptionsDiv" style="display:none;">
                                    <li>
                                        <a href="#" id="completed2Attach" onclick="document.getElementById('dz-post').click()">ATTACH</a>
                                    </li>	
                                    <li><a href="#"    onclick="IncidentStateChange('Reject')">REJECT</a>
                                    </li>
                                    <li id="compResolveLi"><a href="#" class="capitalize-text"  onclick="IncidentStateChange('Resolve')">RESOLVE</a>
                                    </li>
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="rejectOptionsDiv" style="display:none;">
                                    <li style="display:none;"><a href="#"  onclick="IncidentStateChange('Reject')">SAVE</a>
                                    </li>
                                    <li>
                                        <a href="#" id="rejectAttach" onclick="document.getElementById('dz-post').click()">ATTACH</a>
                                    </li>	
                                    <li style="display:<%=dispatchDisplay%>;"><a href="#" onclick="handledClick()">DISPATCH</a>
                                    </li>
                                    <li style="display:<%=dispatchDisplay%>;"><a href="#" onclick="redispatch()">RE-DISPATCH</a>
                                    </li>
                                    <li style="display:none;" id="rejResolveLi"><a href="#" class="capitalize-text" onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="resolvedDiv" style="display:none;">
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="escalatedDIV" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>
                                    <li style="display:<%=dispatchDisplay%>;"><a href="#" onclick="handledClick()">DISPATCH</a>
                                    </li>
                                </ul> 
                                <textarea placeholder="Rejection Notes" style="display:none;" id="rejectionTextarea" class="form-control" rows="3"></textarea>
                                <textarea placeholder="Notes" style="display:none;" id="resolutionTextarea" class="form-control" rows="3"></textarea>
                                 <textarea placeholder="Escalation Notes" style="display:none;" id="escalateTextarea" class="form-control" rows="3"></textarea>
                                <!-- /.nav -->
                                
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-dialog -->
             </div> 
            </div>    