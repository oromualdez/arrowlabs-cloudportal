﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace ArrowLabs.Licence.Portal.Helpers
{
    public class PushNotificationAndroid
    {

        public static void SendNotification(string uname, string applicationID, string senderId, string deviceId, string title, string body, string dbConnection, int pushnotificationtype)
        {
            try
            {

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    data = new
                    {
                        data = new
                        {
                            title = title,
                            message = body,
                            notificationtype = pushnotificationtype,
                            is_background = true,
                            timestamp = DateTime.UtcNow.Ticks,
                            imageurl = "",
                        }
                    }
                    //,
                    //notification = new
                    //{
                    //    body = body,
                    //    title = title,
                    //    notificationtype = pushnotificationtype,
                    //    sound = "Enabled",
                    //    content_available = true
                    //}
                };



                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                var retstr = string.Empty;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                retstr = sResponseFromServer;
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(retstr))
                {
                    var retObj = JObject.Parse(retstr);
                    var returnVal = retObj.GetValue("success").ToString();
                    if (returnVal == "1")
                    {
                        Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("PushNotificationAndroid-" + uname, "(Android Notification Sent!) - " + DateTime.Now.ToString(), new Exception(), dbConnection);
                    }
                    else
                    {
                        Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("PushNotificationAndroid-" + uname, "(Android Notification Failed to send!) - " + DateTime.Now.ToString(), new Exception(), dbConnection);
                    }
                }
                else
                {
                    Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("PushNotificationAndroid-" + uname, "(Android Notification connection failed to send!) - " + DateTime.Now.ToString(), new Exception(), dbConnection);
                }

            }
            catch (Exception ex)
            {
                string str = ex.Message;
                Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("PushNotificationAndroid-" + uname, "SendMessage", ex, dbConnection);
            }
        }

        public static void SendNotificationGroup(string applicationID, string senderId, string deviceId, string body, string title, string dbConnection, int groupid)
        {
            try
            {
                var devicelist = Arrowlabs.Business.Layer.PushNotificationDevice.GetAllPushNotificationDevicesByGroupId(groupid, dbConnection);
                foreach (var dev in devicelist)
                {
                    if (!string.IsNullOrEmpty(dev.DeviceToken) && dev.DeviceType == (int)Arrowlabs.Business.Layer.PushNotificationDeviceType.Apple)
                    {
                        SendNotification(groupid.ToString(), applicationID, senderId, deviceId, body, title, dbConnection, (int)Arrowlabs.Business.Layer.PushNotificationDevice.PushNotificationType.ChatMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("PushNotificationAndroid", "SendNotificationGroup", ex, dbConnection);
            }
        }
    }
}