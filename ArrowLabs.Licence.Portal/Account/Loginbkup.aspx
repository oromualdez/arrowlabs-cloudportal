﻿<%@ Page Title="Log In" EnableEventValidation="false" Language="C#" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Account.Login" %>
<head>
     <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,width=device-width,height=device-height,target-densitydpi=device-dpi,user-scalable=yes">

  <title>Signin Page</title>


  <!-- favicon.ico and apple-touch-icon.png -->
  <link rel="apple-touch-icon" sizes="57x57" href="<%= Page.ResolveClientUrl("~/Images/favicons/apple-touch-icon-57x57.png") %>">
    <link rel="apple-touch-icon" sizes="60x60" href="<%= Page.ResolveClientUrl("~/images/favicons/apple-touch-icon-60x60.png") %>">
    <link rel="apple-touch-icon" sizes="72x72" href="<%= Page.ResolveClientUrl("~/images/favicons/apple-touch-icon-72x72.png") %>">
    <link rel="apple-touch-icon" sizes="76x76" href="<%= Page.ResolveClientUrl("~/images/favicons/apple-touch-icon-76x76.png") %>">
    <link rel="apple-touch-icon" sizes="114x114" href="<%= Page.ResolveClientUrl("~/images/favicons/apple-touch-icon-114x114.png") %>">
    <link rel="apple-touch-icon" sizes="120x120" href="<%= Page.ResolveClientUrl("~/images/favicons/apple-touch-icon-120x120.png") %>">
    <link rel="apple-touch-icon" sizes="144x144" href="<%= Page.ResolveClientUrl("~/images/favicons/apple-touch-icon-144x144.png") %>">
    <link rel="apple-touch-icon" sizes="152x152" href="<%= Page.ResolveClientUrl("~/images/favicons/apple-touch-icon-152x152.png") %>">
    <link rel="icon" type="image/png" href="<%= Page.ResolveClientUrl("~/images/favicons/favicon-32x32.png") %>" sizes="32x32">
    <link rel="icon" type="image/png" href="<%= Page.ResolveClientUrl("~/images/favicons/favicon-96x96.png") %>" sizes="96x96">
    <link rel="icon" type="image/png" href="<%= Page.ResolveClientUrl("~/images/favicons/favicon-16x16.png") %>" sizes="16x16">
    <link rel="manifest" href="<%= Page.ResolveClientUrl("~/images/favicons/manifest.json") %>">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-TileImage" content="<%= Page.ResolveClientUrl("~/images/favicons/mstile-144x144.png") %>">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/styles/bootstrap-select.css") %>">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/styles/bootstrap.css") %>">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/styles/dependencies.css") %>">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/styles/wrapkit.css") %>">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/styles/components.css") %>">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/styles/styles.css") %>">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/styles/styles-media.css") %>">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/styles/sweetalert.css") %>">
          <style>
    .modal {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba( 255, 255, 255, .8 ) url('/FhHRx.gif') 50% 50% no-repeat;
    }

    body.loading {
        overflow: hidden;
    }

        body.loading .modal {
            display: block;
        }
</style>
</head>
<script type="text/javascript">
<%--    function ddl_changed(ddl) {
        //alert(ddl.value);
        var myHidden = document.getElementById('<%= languageHidden.ClientID %>');

        if (myHidden)//checking whether it is found on DOM, but not necessary
        {
            myHidden.value = ddl.value;
        }
    }--%>
    </script>
<body class="login">
    <form runat="server">
  <!--[if lt IE 9]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <main class="signin-wrapper">
    <div class="tab-content">
      <p class="text-center p-4x">
        <img src="<%= Page.ResolveClientUrl("~/Images/custom-images/site-login-logo.png") %>" alt="" height="71px">
      </p>
      <div class="tab-pane fade in active" id="signin">
            <asp:Login ID="LoginUser" runat="server" EnableViewState="false" 
        RenderOuterTable="false" onauthenticate="LoginUser_Authenticate" DisplayRememberMe="true"
         >
        <LayoutTemplate>
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" 
                 ValidationGroup="LoginUserValidationGroup"/>
<%--          <p class="login-header">SIGN INTO MIMS</p>--%>
                                  <div class="form-group">
            <div class="input-group input-group-in mb-2x">
              <span class="input-group-addon"><i class="icon-user"></i></span>
                        <asp:TextBox style="border:none;width:95%;height:40px;" ID="UserName" runat="server" CssClass="textEntry" AutoCompleteType="Disabled" placeholder="Username"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                             CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
            </div>
          </div><!-- /.form-group -->
          <div class="form-group">
            <div class="input-group input-group-in mb-2x">
              <span class="input-group-addon"><i class="icon-lock"></i></span>
                        <asp:TextBox ID="Password" style="border:none;width:95%;height:40px;" runat="server" CssClass="passwordEntry" placeholder="Password" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                             CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
          </div>
          </div><!-- /.form-group -->
                      <div class="form-group" style="display:none;">
            <div class="input-group input-group-in mb-2x">
              <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                     <asp:DropDownList style="border:none;width:98%;height:40px;"  onchange="ddl_changed(this)" id="languageSelect" runat="server">
                     </asp:DropDownList>
          </div>
          </div><!-- /.form-group -->

            <div class="text-center"><asp:CheckBox ID="RememberMe"  runat="server" Text="Remember me" /></div>
                <div style="margin-top:10px;" class="form-group clearfix">
                    <div class="animated-hue text-center">
                    <asp:Button class="btn btn-primary" ID="LoginButton" runat="server" CommandName="Login" Text="SIGN IN" 
                        ValidationGroup="LoginUserValidationGroup"/>
                    </div>
                <%--<p class="pull-right"><a href="#recoverAccount" data-toggle="modal">Can't Sign In?</a></p>--%>
             </div><!-- /.form-group -->
        </LayoutTemplate>
    </asp:Login>
          <asp:HiddenField ID="languageHidden" Value="Please Select Language" runat="server"/>
      </div><!-- /.tab-pane -->

    </div><!-- /.tab-content -->
<%--    <p class="slogin">Magic is something <span class="red-color">you make</span></p>--%>
  </main><!--/#wrapper-->

<div class="signin-cr text-light">
<%--  <ul>
  <li>
    <a href="#">
        Home
    </a>
  </li>
  <li>
    <a href="#">
        Support
    </a>
  </li>
  <li>
    <a href="#">
        Security
    </a>
  </li>
  <li>
    <a href="#">
        Privacy
    </a>
  </li>

  <li>
    <a href="#">
        Terms
    </a>
  </li>

  <li>
    <a href="#">
        Contact
    </a>
  </li>


  </ul>--%>
<p >Copyright &copy; 2016 Arrow Labs Security Solutions LLC. All Rights Reserved</p>
</div>
  <!-- Modal Recover -->
<%--  <div class="modal fade" id="recoverAccount" tabindex="-1" role="dialog" aria-labelledby="recoverAccountLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="recoverForm" action="index.html">
          <div class="modal-header">
            <h4 class="modal-title" id="recoverAccountLabel">Reset Password</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div class="input-group input-group-in">
                <span class="input-group-addon"><i class="fa fa-envelope-o text-muted"></i></span>
                <input type="email" name="recoverEmail" id="recoverEmail" class="form-control" placeholder="Enter your email address">
              </div>
            </div><!-- /.form-group -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Send reset link</button>
          </div>
        </form><!-- /#recoverForm -->
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /#recoverAccount -->--%>


  <!-- VENDORS : jQuery & Bootstrap -->
    <script src="<%= Page.ResolveClientUrl("~/scripts/vendor.js") %>"></script>
  <!-- END VENDORS -->

  <!-- DEPENDENCIES : Required plugins -->
    <script src="<%= Page.ResolveClientUrl("~/scripts/dependencies.js") %>"></script>
  <!-- END DEPENDENCIES -->


  <!-- PLUGIN SETUPS: vendors & dependencies setups -->
    <script src="<%= Page.ResolveClientUrl("~/scripts/plugin-setups.js") %>"></script>
  <!-- END PLUGIN SETUPS -->
        	<script src="<%= Page.ResolveClientUrl("~/scripts/script.js") %>"></script>
        </form>
    </body>