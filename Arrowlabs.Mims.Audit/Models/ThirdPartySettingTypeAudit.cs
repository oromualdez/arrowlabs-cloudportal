﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ThirdPartySettingTypeAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

        public static int InsertOrUpdateThirdPartySettingType(ThirdPartySettingTypeAudit thirdPartySeting, string dbConnection)
        {
            var id = 0;
            if (thirdPartySeting != null)
            {
                var baseAuditId = BaseAudit.InsertBaseAudit(thirdPartySeting.BaseAudit, dbConnection);

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateThirdPartySettingType", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = thirdPartySeting.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = thirdPartySeting.Name;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
