﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class VerifierServiceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }         
        public string Url { get; set; }         
        public int Type { get; set; }         
        public string CreatedBy { get; set; }          
        public DateTime CreatedDate { get; set; }         
        public string TypeName { get; set; }
        public int SiteId { get; set; }
        public enum VerifierServiceTypes
        {
            FRS = 0,
            ANPR = 1
        }
        private static VerifierServiceAudit GetVerifierServiceAuditFromSqlReader(SqlDataReader reader)
        {
            var verifier = new VerifierServiceAudit();
            verifier.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                verifier.Id = Convert.ToInt16(reader["Id"].ToString());
            if (columns.Contains("Url") && reader["Url"] != DBNull.Value)
                verifier.Url = reader["Url"].ToString();
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                verifier.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                verifier.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                verifier.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (columns.Contains("Type") && reader["Type"] != DBNull.Value)
            {
                verifier.Type = Convert.ToInt16(reader["Type"].ToString());
                if (verifier.Type == (int)VerifierServiceTypes.ANPR)
                    verifier.TypeName = "ANPR";
                else
                    verifier.TypeName = "FRS";
            }

            return verifier;
        }
        public static bool InsertVerifierServiceAudit(VerifierServiceAudit verifierServiceAudit, string dbConnection)
        {
            if (verifierServiceAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertVerifierServiceAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(verifierServiceAudit.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = verifierServiceAudit.Id;
                    cmd.Parameters.Add("@Url", SqlDbType.NVarChar).Value = verifierServiceAudit.Url;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = verifierServiceAudit.CreatedBy;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = verifierServiceAudit.CreatedDate;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = verifierServiceAudit.Type;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = verifierServiceAudit.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static List<VerifierServiceAudit> GetAllVerifierServiceAudit(string dbConnection)
        {
            var VerifierServiceAudits = new List<VerifierServiceAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllVerifierServiceAudit";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var VerifierServiceAudit = GetVerifierServiceAuditFromSqlReader(reader);
                    VerifierServiceAudits.Add(VerifierServiceAudit);
                }
                connection.Close();
                reader.Close();
            }
            return VerifierServiceAudits;
        }
    }
}
