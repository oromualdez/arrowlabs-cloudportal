﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Site.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.Site" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
         <style>
             
             *{
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	position: relative;
}

.depbox{
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	position: relative;
}

.cf:before,
.cf:after {
    content: " "; /* 1 */
    display: table; /* 2 */
}

.cf:after {
    clear: both;
}

/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
.cf {
    *zoom: 1;
}

/* Generic styling */

.depcontent{
	width: 100%;
	max-width: 1142px;
	margin: 0 auto;
	padding: 0 20px;
    height:1000px;
}
 

@media all and (max-width: 767px){
	.depcontent{
		padding: 0 20px;
	}	
}

.depul{
	padding: 0;
	margin: 0;
	list-style: none;		
}

.depul a{
	display: block;
	background: #ccc;
	border: 4px solid gray;
	text-align: center;
	overflow: hidden;
	font-size: .7em;
	text-decoration: none;
	font-weight: bold;
	color: #333;
	height: 70px;
	margin-bottom: -26px;
	box-shadow: 4px 4px 9px -4px rgba(0,0,0,0.4);
	-webkit-transition: all linear .1s;
	-moz-transition: all linear .1s;
	transition: all linear .1s;
}


@media all and (max-width: 767px){
	.depul a{
		font-size: 1em;
	}
}


.depul a span{
	top: 50%;
	margin-top: -0.7em;
	display: block;
}

/*
 
 */

.administration > li > a{
	margin-bottom: 25px;
}

.director > li > a{
	width: 50%;
	margin: 0 auto 0px auto;
}

.subdirector:after{
	content: "";
	display: block;
	width: 0;
	height: 130px;
	background: red;
	border-left: 4px solid gray;
	left: 45.45%;
	position: relative;
}

.subdirector,
.departments{
	position: absolute;
	width: 100%;
}

.subdirector > li:first-child,
.departments > li:first-child{	
	width: 18.59894921190893%;
	height: 64px;
	margin: 0 auto 92px auto;		
	padding-top: 25px;
	border-bottom: 4px solid gray;
	z-index: 1;	
}

.subdirector > li:first-child{
	float: right;
	right: 27.2%;
	border-left: 4px solid gray;
}

.departments > li:first-child{	
	float: left;
	left: 27.2%;
	border-right: 4px solid gray;	
}

.subdirector > li:first-child a,
.departments > li:first-child a{
	width: 100%;
}

.subdirector > li:first-child a{	
	left: 25px;
}

@media all and (max-width: 767px){
	.subdirector > li:first-child,
	.departments > li:first-child{
		width: 40%;	
	}

	.subdirector > li:first-child{
		right: 10%;
		margin-right: 2px;
	}

	.subdirector:after{
		left: 49.8%;
	}

	.departments > li:first-child{
		left: 10%;
		margin-left: 2px;
	}
}


.departments > li:first-child a{
	right: 25px;
}

.department:first-child,
.departments li:nth-child(2){
	margin-left: 0;
	clear: left;	
}

.departments:after{
	content: "";
	display: block;
	position: absolute;
	width: 81.1%;
	height: 22px;	
	border-top: 4px solid gray;
	border-right: 4px solid gray;
	border-left: 4px solid gray;
	margin: 0 auto;
	top: 130px;
	left: 9.1%
}

@media all and (max-width: 767px){
	.departments:after{
		border-right: none;
		left: 0;
		width: 49.8%;
	}  
}

@media all and (min-width: 768px){
	.department:first-child:before,
   .department:last-child:before{
    border:none;
  }
}

.department:before{
	content: "";
	display: block;
	position: absolute;
	width: 0;
	height: 22px;
	border-left: 4px solid gray;
	z-index: 1;
	top: -22px;
	left: 50%;
	margin-left: -4px;
}

.department{
	border-left: 4px solid gray;
	width: 18.59894921190893%;
	float: left;
	margin-left: 1.751313485113835%;
	margin-bottom: 60px;
}

.lt-ie8 .department{
	width: 18.25%;
}

@media all and (max-width: 767px){
	.department{
		float: none;
		width: 100%;
		margin-left: 0;
	}

	.department:before{
		content: "";
		display: block;
		position: absolute;
		width: 0;
		height: 60px;
		border-left: 4px solid gray;
		z-index: 1;
		top: -60px;
		left: 0%;
		margin-left: -4px;
	}

	.department:nth-child(2):before{
		display: none;
	}
}

.department > a{
	margin: 0 0 -26px -4px;
	z-index: 1;
}

.department > a:hover{	
	height: 80px;
}

.department > ul{
	margin-top: 0px;
	margin-bottom: 0px;
}

.department li{	
	padding-left: 25px;
	border-bottom: 4px solid gray;
	height: 80px;	
}

.department li a{
	background: gray;
	top: 48px;	
	position: absolute;
	z-index: 1;
	width: 90%;
	height: 60px;
	vertical-align: middle;
	right: -1px;
	background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMTAwJSI+CiAgICA8c3RvcCBvZmZzZXQ9IjAlIiBzdG9wLWNvbG9yPSIjMDAwMDAwIiBzdG9wLW9wYWNpdHk9IjAuMjUiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background-image: -moz-linear-gradient(-45deg,  rgba(0,0,0,0.25) 0%, rgba(0,0,0,0) 100%) !important;
	background-image: -webkit-gradient(linear, left top, right bottom, color-stop(0%,rgba(0,0,0,0.25)), color-stop(100%,rgba(0,0,0,0)))!important;
	background-image: -webkit-linear-gradient(-45deg,  rgba(0,0,0,0.25) 0%,rgba(0,0,0,0) 100%)!important;
	background-image: -o-linear-gradient(-45deg,  rgba(0,0,0,0.25) 0%,rgba(0,0,0,0) 100%)!important;
	background-image: -ms-linear-gradient(-45deg,  rgba(0,0,0,0.25) 0%,rgba(0,0,0,0) 100%)!important;
	background-image: linear-gradient(135deg,  rgba(0,0,0,0.25) 0%,rgba(0,0,0,0) 100%)!important;
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#40000000', endColorstr='#00000000',GradientType=1 );
}

.department li a:hover{
	box-shadow: 8px 8px 9px -4px rgba(0,0,0,0.1);
	height: 80px;
	width: 95%;
	top: 39px;
	background-image: none!important;
}

/* Department/ section colors */
.department.dep-a a{ background: #FFD600; }
.department.dep-b a{ background: #AAD4E7; }
.department.dep-c a{ background: #FDB0FD; }
.department.dep-d a{ background: #A3A2A2; }
.department.dep-e a{ background: #f0f0f0; }
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <script src="https://testportalcdn.azureedge.net/scripts/jquery.validate.js"></script>
      <script src="https://testportalcdn.azureedge.net/scripts/dropzone.js"></script>
    <script src="https://testportalcdn.azureedge.net/scripts/fileinput.js"></script>
    <!--Add script to update the page and send messages.-->
      <script type="text/javascript">
          $j = jQuery.noConflict();
          var chat;
          $j(function () {
              try {
                  var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        var loggedinUsername = '<%=senderName2%>';
          //User-profile
          function changePassword() {
              try {
                  var newPw = document.getElementById("newPwInput").value;
                  var confPw = document.getElementById("confirmPwInput").value;
                  var isErr = false;
                  if (!isErr) { 
                      if (!letterGood) {
                          showAlert('Password does not contain letter');
                          isErr = true;
                      }
                      if (!isErr) {
                          if (!capitalGood) {
                              showAlert('Password does not contain capital letter');
                              isErr = true;
                          }
                      }
                      if (!isErr) {
                          if (!numGood) {
                              showAlert('Password does not contain number');
                              isErr = true;
                          }
                      }
                      if (!isErr) {
                          if (!lengthGood) {
                              showAlert('Password length not enough');
                              isErr = true;
                          }
                      }
                  }
                  if (!isErr) {
                      if (newPw == confPw && newPw != "" && confPw != "") {
                          jQuery.ajax({
                              type: "POST",
                              url: "Site.aspx/changePW",
                              data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                              async: false,
                              dataType: "json",
                              contentType: "application/json; charset=utf-8",
                              success: function (data) {

                                  if (data.d == "LOGOUT") {
                                      showError("Session has expired. Kindly login again.");
                                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                  } else {

                                      jQuery('#changePasswordModal').modal('hide');
                                      document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                      jQuery('#successfulDispatch').modal('show');
                                      document.getElementById("newPwInput").value = "";
                                      document.getElementById("confirmPwInput").value = "";
                                      document.getElementById("oldPwInput").value = confPw;

                                  }
                              },
                              error: function () {
                                  showError("Session timeout. Kindly login again.");
                                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                              }
                          });
                      }
                      else {
                          showAlert("Kindly match new password with confirm password.")
                      }
                  }
              }
              catch (ex)
              { showAlert('Error 60: Problem loading page element.-' + ex) }
          }
          function editUnlock() {
              document.getElementById("profilePhoneNumberDIV").style.display = "none";
              document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
              document.getElementById("editProfileA").style.display = "none";
              document.getElementById("saveProfileA").style.display = "block";
              document.getElementById("profileEmailAddDIV").style.display = "none";
              document.getElementById("profileEmailAddEditDIV").style.display = "block";
              document.getElementById("userFullnameSpanDIV").style.display = "none";
              document.getElementById("userFullnameSpanEditDIV").style.display = "block";
              if (document.getElementById('profileRoleName').innerHTML == "User") {
                  document.getElementById("superviserInfoDIV").style.display = "none";
                  document.getElementById("managerInfoDIV").style.display = "block";
                  document.getElementById("dirInfoDIV").style.display = "none";
              }
              else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
                  document.getElementById("superviserInfoDIV").style.display = "none";
                  document.getElementById("managerInfoDIV").style.display = "none";
                  document.getElementById("dirInfoDIV").style.display = "block";
              }
          }
          function editJustLock() {
              document.getElementById("profilePhoneNumberDIV").style.display = "block";
              document.getElementById("userFullnameSpanEditDIV").style.display = "none";
              document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
              document.getElementById("editProfileA").style.display = "block";
              document.getElementById("saveProfileA").style.display = "none";
              document.getElementById("profileEmailAddDIV").style.display = "block";
              document.getElementById("profileEmailAddEditDIV").style.display = "none";
              document.getElementById("userFullnameSpanDIV").style.display = "block";
              document.getElementById("superviserInfoDIV").style.display = "block";
              document.getElementById("managerInfoDIV").style.display = "none";
              document.getElementById("dirInfoDIV").style.display = "none";
          }
          function editLock() {
              document.getElementById("profilePhoneNumberDIV").style.display = "block";
              document.getElementById("userFullnameSpanEditDIV").style.display = "none";
              document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
              document.getElementById("editProfileA").style.display = "block";
              document.getElementById("saveProfileA").style.display = "none";
              document.getElementById("profileEmailAddDIV").style.display = "block";
              document.getElementById("profileEmailAddEditDIV").style.display = "none";
              document.getElementById("userFullnameSpanDIV").style.display = "block";
              document.getElementById("superviserInfoDIV").style.display = "block";
              document.getElementById("managerInfoDIV").style.display = "none";
              document.getElementById("dirInfoDIV").style.display = "none";
              var role = document.getElementById('UserRoleSelector').value;
              var roleid = 0;
              var supervisor = 0;
              var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
              if (retVal > 0) {
                  assignUserProfileData();
              }
              else {
                  alert('Error 62: Problem occured while trying to get user profile.');
              }

          }
          function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
              var output = 0;
              jQuery.ajax({
                  type: "POST",
                  url: "Site.aspx/addUserProfile",
                  data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
                  async: false,
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function (data) {
                      output = data.d;
                  },
                  error: function () {
                      showError("Session timeout. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }
              });
              return output;
          }
          function useLocation(lat, long) {

              document.getElementById("tbLatitude").value = lat;
              document.getElementById("tbLongitude").value = long;
          }
          function editSite(id, name, lat, long) {
              document.getElementById("rowidChoice").value = id;
              document.getElementById('newsitename').value = name;
              document.getElementById("tbLatitude").value = lat;
              document.getElementById("tbLongitude").value = long;
              document.getElementById("headersitename").innerHTML = "Are you sure you want to delete this site " + name + "?"; 
          }
          function rowchoice(id) {
              document.getElementById("rowidChoice").value = id;
          }
          var sourceLat = 43.58039085560783;
          var sourceLon = 17.578125;
          var mapLocation;
          function getLocationMarkers(obj) {

              try {
                  locationAllowed = true;
                  //setTimeout(function () {
                  google.maps.visualRefresh = true;
                  var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                  // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                  var mapOptions = {
                      zoom: 1,
                      center: Liverpool,
                      mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                  };

                  // This makes the div with id "map_canvas" a google map
                  mapLocation = new google.maps.Map(document.getElementById("map_canvasLocation"), mapOptions);
                  google.maps.event.addListener(mapLocation, 'click', function (event) {
                      alldeletemarker();
                      var location = event.latLng;
                      //Create a marker and placed it on the map.
                      var marker2 = new google.maps.Marker({
                          position: location,
                          map: mapLocation
                      });
                      marker2.id = uniqueId;
                      marker2.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                      uniqueId++;
                      markers2.push(marker2);
                      //Attach click event handler to the marker.
                      google.maps.event.addListener(marker2, "click", function (e) {
                          var infoWindow = new google.maps.InfoWindow({
                              content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                          });
                          infoWindow.open(mapLocation, marker2);
                      });
                  });

                  for (var i = 0; i < obj.length; i++) {

                      var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="locationChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                      var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                      var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                      marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');
                      myMarkersLocation["MARKER"] = marker;
                      createInfoWindowLocation(marker, contentString);
                  }
              }
              catch (err) {
                  alert(err)
              }
          }
          function getLocationNoMarkers() {

              try {
                  locationAllowed = true;
                  //setTimeout(function () {
                  google.maps.visualRefresh = true;
                  var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                  // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                  var mapOptions = {
                      zoom: 1,
                      center: Liverpool,
                      mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                  };

                  // This makes the div with id "map_canvas" a google map
                  mapLocation = new google.maps.Map(document.getElementById("map_canvasLocation"), mapOptions);
                  google.maps.event.addListener(mapLocation, 'click', function (event) {
                      alldeletemarker();
                      var location = event.latLng;
                      //Create a marker and placed it on the map.
                      var marker2 = new google.maps.Marker({
                          position: location,
                          map: mapLocation
                      });
                      marker2.id = uniqueId;
                      marker2.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                      uniqueId++;
                      markers2.push(marker2);
                      //Attach click event handler to the marker.
                      google.maps.event.addListener(marker2, "click", function (e) {
                          var infoWindow = new google.maps.InfoWindow({
                              content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                          });
                          infoWindow.open(mapLocation, marker2);
                      });
                  });
              }
              catch (err) {
              }
          }
          var uniqueId = 1;
          function alldeletemarker() {
              try {
                  for (var i = 0; i < markers2.length; i++) {
                      markers2[i].setMap(null);
                      markers2[i] = null;
                  }
                  markers2 = [];
              }
              catch (err) {

              }
          }
          var markers2 = [];
          var myMarkersLocation = new Array();
          function createInfoWindowLocation(marker, popupContent) {
              google.maps.event.addListener(marker, 'click', function () {
                  infoWindowLocation.setContent(popupContent);
                  infoWindowLocation.open(mapLocation, this);
              });
          }
          function saveTZ() {
              var scountr = $("#countrySelect option:selected").text();
              jQuery.ajax({
                  type: "POST",
                  url: "Site.aspx/saveTZ",
                  data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
                  async: false,
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function (data) {
                      if (data.d[0] == "LOGOUT") {
                          showError("Session has expired. Kindly login again.");
                          setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                      } else {
                          document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                          jQuery('#successfulDispatch').modal('show');
                      }
                  },
                  error: function () {
                      showError("Session timeout. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }
              });
          }
          function getserverInfo() {
              jQuery.ajax({
                  type: "POST",
                  url: "Site.aspx/getServerData",
                  data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                  async: false,
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function (data) {
                      if (data.d[0] == "LOGOUT") {
                          showError("Session has expired. Kindly login again.");
                          setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                      } else {
                          document.getElementById('mobileRemaining').value = data.d[9];
                          document.getElementById('mobileTotal').value = data.d[10];
                          document.getElementById('mobileUsed').value = data.d[11];

                          document.getElementById("countrySelect").value = data.d[28];
                          jQuery('#countrySelect').selectpicker('val', data.d[28]);

                          document.getElementById('surveillanceCheck').checked = false;
                          document.getElementById('notificationCheck').checked = false;
                          document.getElementById('locationCheck').checked = false;
                          document.getElementById('ticketingCheck').checked = false;
                          document.getElementById('taskCheck').checked = false;
                          document.getElementById('incidentCheck').checked = false;
                          document.getElementById('warehouseCheck').checked = false;
                          document.getElementById('chatCheck').checked = false;
                          document.getElementById('collaborationCheck').checked = false;
                          document.getElementById('lfCheck').checked = false;
                          document.getElementById('dutyrosterCheck').checked = false;
                          document.getElementById('postorderCheck').checked = false;
                          document.getElementById('verificationCheck').checked = false;
                          document.getElementById('requestCheck').checked = false;
                          document.getElementById('dispatchCheck').checked = false;
                          document.getElementById('activityCheck').checked = false;

                          if (data.d[12] == "true")
                              document.getElementById('surveillanceCheck').checked = true;
                          if (data.d[13] == "true")
                              document.getElementById('notificationCheck').checked = true;
                          if (data.d[14] == "true")
                              document.getElementById('locationCheck').checked = true;
                          if (data.d[15] == "true")
                              document.getElementById('ticketingCheck').checked = true;
                          if (data.d[16] == "true")
                              document.getElementById('taskCheck').checked = true;
                          if (data.d[17] == "true")
                              document.getElementById('incidentCheck').checked = true;
                          if (data.d[18] == "true")
                              document.getElementById('warehouseCheck').checked = true;
                          if (data.d[19] == "true")
                              document.getElementById('chatCheck').checked = true;
                          if (data.d[20] == "true")
                              document.getElementById('collaborationCheck').checked = true;
                          if (data.d[21] == "true")
                              document.getElementById('lfCheck').checked = true;
                          if (data.d[22] == "true")
                              document.getElementById('dutyrosterCheck').checked = true;
                          if (data.d[23] == "true")
                              document.getElementById('postorderCheck').checked = true;
                          if (data.d[24] == "true")
                              document.getElementById('verificationCheck').checked = true;
                          if (data.d[25] == "true")
                              document.getElementById('requestCheck').checked = true;
                          if (data.d[26] == "true")
                              document.getElementById('dispatchCheck').checked = true;
                          if (data.d[27] == "true")
                              document.getElementById('activityCheck').checked = true;
                      }
                  },
                  error: function () {
                      showError("Session timeout. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }
              });
          }
          function assignUserProfileData() {
              try {
                  jQuery.ajax({
                      type: "POST",
                      url: "Site.aspx/getUserProfileData",
                      data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                      async: false,
                      dataType: "json",
                      contentType: "application/json; charset=utf-8",
                      success: function (data) {
                          if (data.d[0] == "LOGOUT") {
                              showError("Session has expired. Kindly login again.");
                              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                          } else {
                              try {
                                  document.getElementById('containerDiv2').style.display = "none";
                                  document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                  document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                  document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                  document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                  document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                  document.getElementById('profileRoleName').innerHTML = data.d[5];
                                  document.getElementById('profileManagerName').innerHTML = data.d[6];
                                  document.getElementById('userStatusSpan').innerHTML = data.d[8];

                                  if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                      document.getElementById('containerDiv2').style.display = "block";
                                      document.getElementById('defaultGenderDiv').style.display = "none";
                                      getserverInfo();
                                  }

                                  var el = document.getElementById('userStatusIconSpan');
                                  if (el) {
                                      el.className = data.d[9];
                                  }
                                  document.getElementById('userprofileImgSrc').src = data.d[10];
                                  document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                  document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                  document.getElementById('userFirstnameSpan').value = data.d[13];
                                  document.getElementById('userLastnameSpan').value = data.d[14];
                                  document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                  document.getElementById('profileEmailAddEdit').value = data.d[3];

                                  document.getElementById('oldPwInput').value = data.d[16];

                                  document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                  document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                  document.getElementById('profileGender').innerHTML = data.d[20];
                              }
                              catch (err) { alert(err) }
                          }
                      },
                      error: function () {
                          showError("Session timeout. Kindly login again.");
                          setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                      }
                  });
              }
              catch (err)
              { alert('Error 60: Problem loading page element.-' + err) }
          }
          function forceLogout() {
              document.getElementById('<%= closingbtn.ClientID %>').click();
          }
          function clearPWBox() {
              document.getElementById("confirmPwInput").value = "";
              document.getElementById("newPwInput").value = "";
          }
          var lengthGood = false;
          var letterGood = false;
          var capitalGood = false;
          var numGood = false;
        jQuery(document).ready(function () {
            localStorage.removeItem("activeTabActivity");
            localStorage.removeItem("activeTabDev");
            localStorage.removeItem("activeTabInci");
            localStorage.removeItem("activeTabMessage");
            localStorage.removeItem("activeTabTask");
            localStorage.removeItem("activeTabTick");
            localStorage.removeItem("activeTabUB");
            localStorage.removeItem("activeTabVer");
            localStorage.removeItem("activeTabLost");
            $('input[type=password]').keyup(function () {
                // keyup event code here
                var pswd = $(this).val();
                if (pswd.length < 8) {
                    $('#length').removeClass('valid').addClass('invalid');
                    lengthGood = false;
                } else {
                    $('#length').removeClass('invalid').addClass('valid');
                    lengthGood = true;
                }
                //validate letter
                if (pswd.match(/[A-z]/)) {
                    $('#letter').removeClass('invalid').addClass('valid');
                    letterGood = true;

                } else {
                    $('#letter').removeClass('valid').addClass('invalid');
                    letterGood = false;
                }

                //validate capital letter
                if (pswd.match(/[A-Z]/)) {
                    $('#capital').removeClass('invalid').addClass('valid');
                    capitalGood = true;
                } else {
                    $('#capital').removeClass('valid').addClass('invalid');

                    capitalGood = false;
                }

                //validate number
                if (pswd.match(/\d/)) {
                    $('#number').removeClass('invalid').addClass('valid');
                    numGood = true;

                } else {
                    $('#number').removeClass('valid').addClass('invalid');
                    numGood = false;
                }
            });
            $('input[type=password]').focus(function () {
                // focus code here
                $('#pswd_info').show();
            });
            $('input[type=password]').blur(function () {
                // blur code here
                $('#pswd_info').hide();
            });
            addrowtoTable();
            document.getElementById("rowidChoice").value = "0";
            jQuery('#newSite').on('shown.bs.modal', function () {
                if (typeof myMarkersLocation["MARKER"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersLocation["MARKER"].setMap(null);
                }
                alldeletemarker();
                if (firstOpenNewTask == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Site.aspx/getLocationData",
                        data: "{'id':'" + document.getElementById("rowidChoice").value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else if (data.d != "]") {
                                var obj = jQuery.parseJSON(data.d)
                                getLocationMarkers(obj);
                            }
                            else {
                                getLocationNoMarkers();
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                    firstOpenNewTask = true;
                }
                else {
                    jQuery.ajax({
                        type: "POST",
                        url: "Site.aspx/getLocationData",
                        data: "{'id':'" + document.getElementById("rowidChoice").value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else if (data.d != "]") {
                                var obj = jQuery.parseJSON(data.d)
                                updateMarker(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            });
        });
        function updateMarker(obj) {
            try {
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="locationChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                    marker.setIcon('../Images/markerOnline.png')
                    myMarkersLocation["MARKER"] = marker;
                    createInfoWindowLocation(marker, contentString);
                }
            }
            catch (err) {
                //showAlert('Error 43: Problem faced during getting markers-' + err);
            }
        }
        var firstOpenNewTask = false;
        function clearsite() {
            document.getElementById("rowidChoice").value = "0";
            document.getElementById("tbLongitude").value = "";
            document.getElementById("tbLatitude").value = "";
            document.getElementById("newsitename").value = "";
            alldeletemarker();
        }
        function saveSite() {
            var siteName = document.getElementById("newsitename").value;
            var isErr = false;
            if (isEmptyOrSpaces(document.getElementById('newsitename').value)) {
                isErr = true;
                showAlert("Kindly provide site name")
            }
            else {
                if (isSpecialChar(document.getElementById('newsitename').value)) {
                    isErr = true;
                }
            }
            var longi = document.getElementById("tbLongitude").value;
            var lati = document.getElementById("tbLatitude").value;
            if (!isEmptyOrSpaces(longi)) {
                if (isInvalidDouble(longi)) {
                    isErr = true;
                }
            }

            if (!isEmptyOrSpaces(longi)) {
                if (isInvalidDouble(lati)) {
                    isErr = true;
                }
            }
            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Site.aspx/insertSiteData",
                    data: "{'id':'" + document.getElementById("rowidChoice").value + "','lat':'" + document.getElementById('tbLatitude').value + "','longi':'" + document.getElementById('tbLongitude').value + "','name':'" + siteName + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newSite').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Successfully!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showAlert(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
        }
    }
          function deleteChoice() {
              jQuery.ajax({
                  type: "POST",
                  url: "Site.aspx/deleteSiteData",
                  data: "{'id':'" + document.getElementById("rowidChoice").value + "','uname':'" + loggedinUsername + "'}",
                  async: false,
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function (data) {
                      if (data.d == "SUCCESS") {
                          jQuery('#deleteSite').modal('hide');
                          document.getElementById('successincidentScenario').innerHTML = "Successfully deleted site!";
                          jQuery('#successfulDispatch').modal('show');
                      }
                      else if (data.d == "LOGOUT") {
                          showError("Session has expired. Kindly login again.");
                          setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                      }
                      else {
                          showAlert(data.d);
                      }
                  },
                  error: function () {
                      showError("Session timeout. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }
              });
          }
    function addrowtoTable() {
        jQuery("#verifierTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Site.aspx/getTableData",
            data: "{'id':'1','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#verifierTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    </script>
    <!-- ============================================
    MAIN CONTENT SECTION
    =============================================== -->
        <section class="content-wrapper" role="main">
            <div class="content">
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">Sites</span></h3>
                                </div>
                                <div class="col-md-8">
                                    <div class="panel-control">
                                        <ul id="demo3-tabs" class="nav nav-tabs nav-main">
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div role="group" class="pull-right">
                                       <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                    <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <asp:Button ID="logoutbtn" runat="server"  OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
							<div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li ><a href="#"  data-target="#newSite" data-toggle="modal" class="capitalize-text" onclick="clearsite()">+ NEW SITE</a>
                                                    </li>                                                  
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-sites">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">SITES</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="verifierTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div  class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                  			<label class="select select-o" >
                                                                                  <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>         
                                                            <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                                      <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Contract</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                   <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                         <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                        
                                            </div>
                                                         <div class="row" style="display:none;">
                                            <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </div>
                <!-- /panel-body -->
            </div>
            <!-- /.panel -->
            <div aria-hidden="true" aria-labelledby="newSite" role="dialog" tabindex="-1" id="newSite" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text" style="width:400px;">SITE</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Site Name" id="newsitename"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-6">
                                 <input class="form-control" placeholder="Latitude" id="tbLatitude"/>
                              </div>
                                                                                        <div class="col-md-6">
                                 <input class="form-control" placeholder="Longitude" id="tbLongitude"/>
                              </div>
                           </div>
                             
                            <div class="row">
                                <div id="map_canvasLocation" style="width:100%;height:390px;"></div>
                            </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li><a href="#"  onclick="saveSite()">SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>


            

             <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            		                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="deleteSite" role="dialog" tabindex="-1" id="deleteSite" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="headersitename">Are you sure you want to delete this entry?</h4>
                        </div>
                         <div class="row">
                            <h4 class="text-center" id="todeleteVer"></h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: This action cannot be undone!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li > <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li ><a href="#"  onclick="deleteChoice()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                                                 <div aria-hidden="true" aria-labelledby="newOrgChart" role="dialog" tabindex="-1" id="newOrgChart" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-lgx">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">ORGANIZATIONAL CHART</h4>
                     </div>
                     <div class="modal-body">
 <div class="depcontent">
  <h1>Site Name</h1>
  <figure class="org-chart cf">
    <ul class="depul administration">
      <li class="depbox">					
        <ul class="depul director">
          <li class="depbox">
            <a href="#"><span>Director</span></a>
            <ul class="depul subdirector">
              <li class="depbox"><a href="#"><span>Assistante Director</span></a></li>
            </ul>
            <ul class="depul departments cf">								
              <li class="depbox"><a href="#"><span>Administration</span></a></li>
              
              <li class="depbox department dep-a">
                <a href="#"><span>Department A</span></a>
                <ul class="depul sections">
                  <li class="section depbox"><a href="#"><span>Section A1</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section A2</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section A3</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section A4</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section A5</span></a></li>
                </ul>
              </li>
              <li class="depbox department dep-b">
                <a href="#"><span>Department B</span></a>
                <ul class="depul sections">
                  <li class="section depbox"><a href="#"><span>Section B1</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section B2</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section B3</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section B4</span></a></li>
                </ul>
              </li>
              <li class="depbox department dep-c">
                <a href="#"><span>Department C</span></a>
                <ul class="depul sections">
                  <li class="section depbox"><a href="#"><span>Section C1</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section C2</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section C3</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section C4</span></a></li>
                </ul>
              </li>
              <li class="depbox department dep-d">
                <a href="#"><span>Department D</span></a>
                <ul class="depul sections">
                  <li class="section depbox"><a href="#"><span>Section D1</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section D2</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section D3</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section D4</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section D5</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section D6</span></a></li>
                </ul>
              </li>
              <li class="depbox department dep-e">
                <a href="#"><span>Department E</span></a>
                <ul class="depul sections">
                  <li class="section depbox"><a href="#"><span>Section E1</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section E2</span></a></li>
                  <li class="section depbox"><a href="#"><span>Section E3</span></a></li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>	
      </li>
    </ul>			
  </figure>
</div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#"  onclick="saveSite()">SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <input style="display:none;" id="rowIncidentName" type="text"/>
            <input style="display:none;" id="verCaseID1" type="text"/>
            <input style="display:none;" id="verCaseID2" type="text"/>
            <input style="display:none;" id="verCaseID3" type="text"/>
            <input style="display:none;" id="verCaseID4" type="text"/>
            <input style="display:none;" id="verCaseID5" type="text"/>
            <input style="display:none;" id="verCaseName1" type="text"/>
            <input style="display:none;" id="verCaseName2" type="text"/>
            <input style="display:none;" id="verCaseName3" type="text"/>
            <input style="display:none;" id="verCaseName4" type="text"/>
            <input style="display:none;" id="verCaseName5" type="text"/>
            
            <input style="display:none;" id="rowidChoice" type="text"/>
            <input style="display:none;" id="rowidChoiceANPR" type="text"/>
            <input style="display:none;" id="imagePath" type="text"/>
             </section>
        <!-- /MAIN -->
</asp:Content>
