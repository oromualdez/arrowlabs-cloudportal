﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;


    namespace Arrowlabs.Mims.Audit.Models
    {

        /// <summary>
        /// TODO: Update summary.
        /// </summary>
        /// 
        [Serializable]
        public class ItemLostAttachment
        {
            public BaseAudit BaseAudit { get; set; }
            public int Id { get; set; }

            public int ItemLostId { get; set; }


            public byte[] Attachment { get; set; }

            public string AttachmentPath { get; set; }

            public string CreatedBy { get; set; }

            public string AccountName { get; set; }

            public DateTime? CreatedDate { get; set; }

            public string UpdatedBy { get; set; }

            public DateTime? UpdatedDate { get; set; }

            public string AttachmentFileName { get; set; }

            public string AttachmentFolderName { get; set; }

            public int SiteId { get; set; }

            public int CustomerId { get; set; }

            private static ItemLostAttachment GetItemLostAttachmentFromSqlReader(SqlDataReader reader)
            {
                var itemLost = new ItemLostAttachment();

                if (reader["Id"] != DBNull.Value)
                    itemLost.Id = Convert.ToInt32(reader["Id"].ToString());
                if (reader["ItemLostId"] != DBNull.Value)
                    itemLost.ItemLostId = Convert.ToInt32(reader["ItemLostId"].ToString());

                if (reader["CustomerId"] != DBNull.Value)
                    itemLost.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                 
                if (reader["CreatedBy"] != DBNull.Value)
                    itemLost.CreatedBy = reader["CreatedBy"].ToString();
                if (reader["CreatedDate"] != DBNull.Value)
                    itemLost.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                if (reader["UpdatedBy"] != DBNull.Value)
                    itemLost.UpdatedBy = reader["UpdatedBy"].ToString();
                if (reader["UpdatedDate"] != DBNull.Value)
                    itemLost.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
                if (reader["Attachment"] != DBNull.Value)
                    itemLost.Attachment = (byte[])reader["Attachment"];
                if (reader["AttachmentPath"] != DBNull.Value)
                    itemLost.AttachmentPath = reader["AttachmentPath"].ToString();

                //C:\SVNServer\trunk\MIMS_Mobile_2.2_LostAndFound\Mims_Mobile\WebPortalClient\Uploads\Lost\ee73e7ec-aedf-4049-9434-1cb389d7b8d5\f34d122e-91e5-436c-ae9c-b120268fde30.jpg
                var splits = itemLost.AttachmentPath.Split('\\');
                var foundLost = false;
                var count = 0;
                foreach (var spl in splits)
                {
                    count++;
                    if (!foundLost)
                    {
                        if (spl == "Lost")
                            foundLost = true;
                    }
                    else
                    {
                        itemLost.AttachmentFolderName = spl;
                        itemLost.AttachmentFileName = splits[count];
                        break;
                    }
                }
                return itemLost;
            }

            public static List<ItemLostAttachment> GetAllItemLostAttachments(string dbConnection)
            {
                var itemLostAttachments = new List<ItemLostAttachment>();
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand();
                    sqlCommand.Connection = connection;
                    sqlCommand.CommandText = "GetAllItemLostAttachments";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemLost = GetItemLostAttachmentFromSqlReader(reader);
                        itemLostAttachments.Add(itemLost);
                    }
                    connection.Close();
                    reader.Close();
                }
                return itemLostAttachments;
            }

            public static List<ItemLostAttachment> GetItemLostAttachmentById(int id, string dbConnection)
            {
                var itemLostAttachments = new List<ItemLostAttachment>();
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand();
                    sqlCommand.Connection = connection;
                    sqlCommand.CommandText = "GetItemLostAttachmentById";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemLost = GetItemLostAttachmentFromSqlReader(reader);
                        itemLostAttachments.Add(itemLost);
                    }
                    connection.Close();
                    reader.Close();
                }
                return itemLostAttachments;
            }

            public static List<ItemLostAttachment> GetItemLostAttachmentByItemLostId(int itemLostId, string dbConnection)
            {
                var itemLostAttachments = new List<ItemLostAttachment>();
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand();
                    sqlCommand.Connection = connection;
                    sqlCommand.CommandText = "GetItemLostAttachmentByItemLostId";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = itemLostId;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemLost = GetItemLostAttachmentFromSqlReader(reader);
                        itemLostAttachments.Add(itemLost);
                    }
                    connection.Close();
                    reader.Close();
                }
                return itemLostAttachments;
            }

            public static bool InsertOrUpdateItemLostAttachment(ItemLostAttachment itemLostAttachment, string dbConnection)
            {
                int id = itemLostAttachment.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                if (itemLostAttachment != null)
                {
                    var baseAuditId = BaseAudit.InsertBaseAudit(itemLostAttachment.BaseAudit, dbConnection);
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateItemLostAttachmentAudit", connection);
                        cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = itemLostAttachment.Id;

                        cmd.Parameters.Add(new SqlParameter("@ItemLostId", SqlDbType.Int));
                        cmd.Parameters["@ItemLostId"].Value = itemLostAttachment.ItemLostId;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = itemLostAttachment.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = itemLostAttachment.CustomerId;
                         
                        cmd.Parameters.Add(new SqlParameter("@Attachment", SqlDbType.VarBinary));
                        cmd.Parameters["@Attachment"].Value = itemLostAttachment.Attachment;

                        cmd.Parameters.Add(new SqlParameter("@AttachmentPath", SqlDbType.NVarChar));
                        cmd.Parameters["@AttachmentPath"].Value = itemLostAttachment.AttachmentPath;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = itemLostAttachment.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        if (itemLostAttachment.Id == 0)
                        {
                            cmd.Parameters["@CreatedDate"].Value = DateTime.Now;
                        }
                        else
                        {
                            cmd.Parameters["@CreatedDate"].Value = itemLostAttachment.CreatedDate;
                        }

                        cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@UpdatedBy"].Value = itemLostAttachment.UpdatedBy;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = DateTime.Now;

                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();

                        if (id == 0)
                            id = (int)returnParameter.Value;

                    }
                }
                return true;
            }
        }
    }

