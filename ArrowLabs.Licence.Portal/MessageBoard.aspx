﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MessageBoard.aspx.cs" Inherits="ArrowLabs.Licence.Portal.MessageBoard" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
        <style>
            .inner{
    position:relative;
    top:0px;
}
.outer{
    overflow:hidden;
}
            .inner2{
    position:relative;
    top:0px;
}
.outer2{
    overflow:hidden;
}
            .inner3{
    position:relative;
    top:0px;
}
.outer3{
    overflow:hidden;
}

                             #taskDocument {
    overflow-y:scroll;
} 
            #viewDocument1 {
    overflow-y:scroll;
}      
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
     <script src="Scripts/jquery.rss.min.js"></script>
    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name="+name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                
               
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        function camStart(cam,name)
        {
            document.getElementById("camFrame").src = "https://mimscloud.net:8082/DemoApp_Portal/index.html#?"+cam;
            document.getElementById("carNameSpan").innerHTML = name;
        }
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }
        function saveTZ() {
            var scountr = $("#countrySelect option:selected").text();
            jQuery.ajax({
                type: "POST",
                url: "Default.aspx/saveTZ",
                data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                        jQuery('#successfulDispatch').modal('show');
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getserverInfo() {
            jQuery.ajax({
                type: "POST",
                url: "Default.aspx/getServerData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        document.getElementById('mobileRemaining').value = data.d[9];
                        document.getElementById('mobileTotal').value = data.d[10];
                        document.getElementById('mobileUsed').value = data.d[11];
                        document.getElementById("countrySelect").value = data.d[28];
                        jQuery('#countrySelect').selectpicker('val', data.d[28]);

                        document.getElementById('surveillanceCheck').checked = false;
                        document.getElementById('notificationCheck').checked = false;
                        document.getElementById('locationCheck').checked = false;
                        document.getElementById('ticketingCheck').checked = false;
                        document.getElementById('taskCheck').checked = false;
                        document.getElementById('incidentCheck').checked = false;
                        document.getElementById('warehouseCheck').checked = false;
                        document.getElementById('chatCheck').checked = false;
                        document.getElementById('collaborationCheck').checked = false;
                        document.getElementById('lfCheck').checked = false;
                        document.getElementById('dutyrosterCheck').checked = false;
                        document.getElementById('postorderCheck').checked = false;
                        document.getElementById('verificationCheck').checked = false;
                        document.getElementById('requestCheck').checked = false;
                        document.getElementById('dispatchCheck').checked = false;
                        document.getElementById('activityCheck').checked = false;
                    
                        if (data.d[12] == "true")
                            document.getElementById('surveillanceCheck').checked = true;
                        if (data.d[13] == "true")
                            document.getElementById('notificationCheck').checked = true;
                        if (data.d[14] == "true")
                            document.getElementById('locationCheck').checked = true;
                        if (data.d[15] == "true")
                            document.getElementById('ticketingCheck').checked = true;
                        if (data.d[16] == "true")
                            document.getElementById('taskCheck').checked = true;
                        if (data.d[17] == "true")
                            document.getElementById('incidentCheck').checked = true;
                        if (data.d[18] == "true")
                            document.getElementById('warehouseCheck').checked = true;
                        if (data.d[19] == "true")
                            document.getElementById('chatCheck').checked = true;
                        if (data.d[20] == "true")
                            document.getElementById('collaborationCheck').checked = true;
                        if (data.d[21] == "true")
                            document.getElementById('lfCheck').checked = true;
                        if (data.d[22] == "true")
                            document.getElementById('dutyrosterCheck').checked = true;
                        if (data.d[23] == "true")
                            document.getElementById('postorderCheck').checked = true;
                        if (data.d[24] == "true")
                            document.getElementById('verificationCheck').checked = true;
                        if (data.d[25] == "true")
                            document.getElementById('requestCheck').checked = true;
                        if (data.d[26] == "true")
                            document.getElementById('dispatchCheck').checked = true;
                        if (data.d[27] == "true")
                            document.getElementById('activityCheck').checked = true;
                    }
                }
            });
        }

        function camClear()
        {
            document.getElementById("camFrame").src = "https://google.com";
        }
        var loggedinUsername = '<%=senderName2%>';
        var vehcounter = 1;
        var currentlocation = '<%=currentlocation%>';
        var sourceLat = '<%=sourceLat%>';
        var sourceLon = '<%=sourceLon%>';
        var locationAllowed = false;
        var mapUrl;
        var mapUrlLocation;
        var map;
        var mapLocation;
        var mapLocationTicket;
        var mapTaskLocation;
        var myMarkersTasksLocation = new Array();
        
        var infoWindowTaskLocation;
        var myMarkers = new Array();
        var myMarkersLocation = new Array();
        var divArray = new Array();

        var rotate_factor = 0;
        var rotated = false;
        function startRot() {
            rotated = false;
            rotate_factor = 0;
        }
        function loadMe(e) {
            var newImageWidth = e.width;
            var newImageHeight = e.height;
            var canvasWidth = 400;
            var canvasHeight = 400;
            var imageAspectRatio = e.width / e.height;
            var canvasAspectRatio = canvasWidth / canvasHeight;
            if (imageAspectRatio < canvasAspectRatio) {
                newImageHeight = canvasHeight;
                newImageWidth = e.width * (newImageHeight / e.height);
            }
            else if (imageAspectRatio > canvasAspectRatio) {
                newImageWidth = canvasWidth
                newImageHeight = e.height * (newImageWidth / e.width);
            }
            var margins;
            if (newImageWidth == 400) {
                jQuery(e).css({
                    'width': newImageWidth,
                    'height': newImageHeight,
                    'margin-top': '10%'
                });
            }
            else
            {
                jQuery(e).css({
                    'width': newImageWidth,
                    'height': newImageHeight
                });
            }
        }
        function rotateMe(e) {
            try {
                rotate_factor += 1;
                var rotate_angle = (90 * rotate_factor) % 360;
                if (rotated) {

                    if (e.width == 400) {
                        jQuery(e).css({
                            'margin-top': '10%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else {
                        jQuery(e).css({
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    rotated = false;
                }
                else {
                    if (e.width == 400 && e.height == 300) {
                        jQuery(e).css({
                            'margin-top': '10%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else if (e.width == 400 && e.height < 300) {
                        jQuery(e).css({
                            'margin-top': '20%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else {
                        jQuery(e).css({
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    rotated = true;
                }
            }
            catch (err) {
                alert(err);
            }
        }

        var infoWindow;
        var infoWindowLocation;
        var rtime = new Date(1, 1, 2000, 12, 00, 00);
        var timeout = false;
        var delta = 200;
        function searchMapFunction (){
            try{
                var foundMatch= false;
                var text = document.getElementById("tbMapSearch").value; 
                if(!isSpecialChar(text)){
                    var options = document.getElementById('usermapListBox').options; 
                    for (var i = 0; i < options.length; i++) {
                        var option = options[i]; 
                        var optionText = option.text; 
                        var lowerOptionText = optionText.toLowerCase();
                        var lowerText = text.toLowerCase(); 
                        var regex = new RegExp("^" + text, "i");
                        var match = optionText.match(regex); 
                        var contains = lowerOptionText.indexOf(lowerText) != -1;
                        if(lowerOptionText == lowerText)
                        {
                            var res = option.value.split("-");
                            map.setCenter(new google.maps.LatLng(res[0], res[1]));
                            map.setZoom(16);
                            foundMatch = true;
                            return;
                        }
                    }
                    if(!foundMatch){
                        showAlert('Couldnt find user.');
                    }
                }
            }catch(ex)
            {
                alert(ex)
            }
        }
        //DEMO EDI 

        var infoVerWindowLocation;
        var myVerMarkersLocation = new Array();
        var mapVerLocation;
        function getVerLocationMarkers(obj) {
            navigator.geolocation.getCurrentPosition(function (position) {
                sourceLat = position.coords.latitude;
                sourceLon = position.coords.longitude;

                locationAllowed = true;
                google.maps.visualRefresh = true;

                var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 12,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                mapVerLocation = new google.maps.Map(document.getElementById("map_verLocation"), mapOptions);
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div id="content">' + obj[i].Username +
                    '<br/></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    var marker = new google.maps.Marker({ position: myLatlng, map: mapVerLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    myVerMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                    createverInfoWindowLocation(marker, contentString);
                }
            });
        }
        function createverInfoWindowLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoVerWindowLocation.setContent(popupContent);
                infoVerWindowLocation.open(mapVerLocation, this);
            });
        }
        function assignVerifierId(id)
        {
            document.getElementById('verifierID').value = id;
            assignVerifierRequestData();
        }
        function assignVerifierRequestData() {
            jQuery('#taskDocument').modal('hide');
            var id = document.getElementById('verifierID').value;
            $.ajax({
                type: "POST",
                url: "Default.aspx/getVerifyLocationData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var obj = $.parseJSON(data.d)
                    getVerLocationMarkers(obj);
                }
            });
            jQuery.ajax({
                type: "POST",
                url: "Default.aspx/getVerifierRequestData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById("verifyUserSpan").innerHTML = data.d[0];
                    document.getElementById("verifyTypeSpan").innerHTML = data.d[1];
                    document.getElementById("verifyCaseSpan").innerHTML = data.d[2];
                    document.getElementById("verifyTimeSpan").innerHTML = data.d[3];
                    document.getElementById("verifyLocSpan").innerHTML = data.d[4];
                    document.getElementById("verifyReasonSpan").innerHTML = data.d[5];
                    document.getElementById("verifyBYearSpan").innerHTML = data.d[6];
                    document.getElementById("verifyEthniSpan").innerHTML = data.d[7];
                    document.getElementById("verifyPIDSpan").innerHTML = data.d[8];
                    document.getElementById("verifyGenderSpan").innerHTML = data.d[9];
                    document.getElementById("verifyListTypeSpan").innerHTML = data.d[10];
                    document.getElementById("veriResultIMG").src = data.d[12];
                    document.getElementById("veriSentIMG").src = data.d[11];
                    if (data.d.length > 14) {
                        document.getElementById("verifyHeaderSpan").innerHTML = data.d[23];
                        document.getElementById("veriResultIMG").src = data.d[13];
                        document.getElementById("veriResult1IMG").src = data.d[13];
                        document.getElementById("veriResult2IMG").src = data.d[15];
                        document.getElementById("veriResult3IMG").src = data.d[17];
                        document.getElementById("veriResult4IMG").src = data.d[19];
                        document.getElementById("veriResult5IMG").src = data.d[21];

                        document.getElementById("resultPercent1").innerHTML = data.d[14] + "%";
                        document.getElementById("resultPercent2").innerHTML = data.d[16] + "%";
                        document.getElementById("resultPercent3").innerHTML = data.d[18] + "%";
                        document.getElementById("resultPercent4").innerHTML = data.d[20] + "%";
                        document.getElementById("resultPercent5").innerHTML = data.d[22] + "%";
                        document.getElementById("resultPercentMain").innerHTML = data.d[14] + "%";
                        document.getElementById('progressbarMainDisplay').setAttribute("style", "width:" + data.d[14] + "%");

                        document.getElementById("verCaseName1").text = data.d[24];
                        document.getElementById("verCaseName2").text = data.d[26];
                        document.getElementById("verCaseName3").text = data.d[28];
                        document.getElementById("verCaseName4").text = data.d[30];
                        document.getElementById("verCaseName5").text = data.d[32];

                        document.getElementById("verifyCaseSpan").innerHTML = data.d[25];
                        document.getElementById("verCaseID1").text = data.d[25];
                        document.getElementById("verCaseID2").text = data.d[27];
                        document.getElementById("verCaseID3").text = data.d[29];
                        document.getElementById("verCaseID4").text = data.d[31];
                        document.getElementById("verCaseID5").text = data.d[33];

                    }
                    else {
                        jQuery('#verificationFail').modal('show');
                        document.getElementById("veriResult1IMG").src = "";
                        document.getElementById("veriResult2IMG").src = "";
                        document.getElementById("veriResult3IMG").src = "";
                        document.getElementById("veriResult4IMG").src = "";
                        document.getElementById("veriResult5IMG").src = "";

                        document.getElementById("resultPercent1").innerHTML = "";
                        document.getElementById("resultPercent2").innerHTML = "";
                        document.getElementById("resultPercent3").innerHTML = "";
                        document.getElementById("resultPercent4").innerHTML = "";
                        document.getElementById("resultPercent5").innerHTML = "";
                        document.getElementById("resultPercentMain").innerHTML = "";
                        document.getElementById('progressbarMainDisplay').setAttribute("style", "width:1%");
                    }
                }
            });
        }

        var firstcharttask = false;
        var firstother = false;

        function showTaskingCharts(dash)
        {
            try{
                if(dash == "inci")
                {
                    document.getElementById("divActReport").style.display = "block";
                    document.getElementById("divEventStatus").style.display = "block";
                    document.getElementById("divTopChart").style.display = "block";
                    document.getElementById("divTopOffence").style.display = "block";

                    document.getElementById("incieventStatusLastHeader").style.display = "block";
                    document.getElementById("incidentsDivContainer").style.display = "block";

                    document.getElementById("divrecentActivityDIV").style.display = "block";

                    jQuery.ajax({
                        type: "POST",
                        url: "Default.aspx/getGPSData",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if(data.d == "LOGOUT"){
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else if (data.d == "]") {
                                //getLocationNoOnline();
                            }
                            else {
                                var obj = jQuery.parseJSON(data.d)
                                updateGPSMapMarker(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                        }
                    }); 
                    document.getElementById("maptoggle").style.display = "block";
                    document.getElementById("usertoggle").style.display = "none";

                    //togglemapDIV('INCIDENTS');
                }
            }
            catch(er)
            {
                showAlert("Error 35: Problem loading charts. - "+er);
            }
        }
        //User-Profile
        function changePassword() {
            try
            {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;

                var isErr = false;
                if (!isErr) { 
                    if (!letterGood) {
                        showAlert('Password does not contain letter');
                        isErr = true;
                    }
                    if (!isErr) {
                        if (!capitalGood) {
                            showAlert('Password does not contain capital letter');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!numGood) {
                            showAlert('Password does not contain number');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!lengthGood) {
                            showAlert('Password length not enough');
                            isErr = true;
                        }
                    }
                }
                if(!isErr){
                    if (newPw == confPw && newPw != "" && confPw != "") {
                        $.ajax({
                            type: "POST",
                            url: "Default.aspx/changePW",
                            data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
  
                                    jQuery('#changePasswordModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                    jQuery('#successfulDispatch').modal('show');
                                    document.getElementById("newPwInput").value = "";
                                    document.getElementById("confirmPwInput").value = "";
                                    document.getElementById("oldPwInput").value = confPw;
                                
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                            } 
                        });
                    }
                    else {
                        showAlert("Kindly match new password with confirm password.")
                    }
                }
            }
            catch(ex)
            {
                showAlert("Error 60: Problem loading page element. - "+ ex)
            }
        }
        function editUnlock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
            document.getElementById("editProfileA").style.display = "none";
            document.getElementById("saveProfileA").style.display = "block";
            document.getElementById("profileEmailAddDIV").style.display = "none";
            document.getElementById("profileEmailAddEditDIV").style.display = "block";
            document.getElementById("userFullnameSpanDIV").style.display = "none";
            document.getElementById("userFullnameSpanEditDIV").style.display = "block";
            if (document.getElementById('profileRoleName').innerHTML == "User")
            {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "block";
                document.getElementById("dirInfoDIV").style.display = "none";
            
            
            }
            else if (document.getElementById('profileRoleName').innerHTML == "Manager")
            {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "none";
                document.getElementById("dirInfoDIV").style.display = "block";
            }
        }
        function editJustLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        function editLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
            var uID = document.getElementById('rowidChoice').text;
            var role = document.getElementById('UserRoleSelector').value;
            var roleid = 0;
            var supervisor = 0;
            if (role == "User") {
                supervisor = document.getElementById('MainContent_editmanagerpickerSelect').value;
                if (supervisor > 0) {
                    roleid = 3;
                }
                else {
                    roleid = 5;
                }
            }
            else if (role == "Manager") {
                supervisor = document.getElementById('MainContent_editdirpickerSelect').value;
                roleid = 2;
            }
            else if (role == "Director") {
                roleid = 1;
            }

            var retVal = saveUserProfile(uID, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
            if (retVal > 0) {
                assignUserProfileData(retVal);  
                showAlert('Successfully updated user info.');
            }
            else {
                showAlert("Error 61: Problem occured while trying to update user profile.")
            }

        }
        function getGroupnsOfUser(id) {
            document.getElementById("userGroupList").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "Default.aspx/getGroupDataFromUser",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else{
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'help-block round-border padding-2x mb-2x relative drop-element';

                            div.innerHTML = data.d[i];

                            div.id = data.d[i + 1];

                            document.getElementById('userGroupList').appendChild(div);
                            i++;
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role,img) {
            var output = 0;
            $.ajax({
                type: "POST",
                url: "Default.aspx/addUserProfile",
                data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
            return output;
        }
        var firstuserMap = false;
        function assignManagerUserProfileData(id) {
            try
            {
                getGroupnsOfUser(id)
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/getUserProfileData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            try{
                                document.getElementById('containerDiv').style.display="none";
                                document.getElementById('containerDiv2').style.display="none";


                                document.getElementById('changePWDIV').style.display = "block";
                                document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                document.getElementById('profileRoleName').innerHTML = data.d[5];
                                document.getElementById('profileManagerName').innerHTML = data.d[6];
                                document.getElementById('userStatusSpan').innerHTML = data.d[8];

                                if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                    document.getElementById('containerDiv2').style.display = "block";
                                    document.getElementById('defaultGenderDiv').style.display = "none";
                                    getserverInfo();
                                }

                                var el = document.getElementById('userStatusIconSpan');
                                if (el) {
                                    el.className = data.d[9];
                                }
                                document.getElementById('userprofileImgSrc').src = data.d[10];
                                document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                document.getElementById('userFirstnameSpan').value = data.d[13];
                                document.getElementById('userLastnameSpan').value = data.d[14];
                                document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                document.getElementById('profileEmailAddEdit').value = data.d[3];

                                document.getElementById('oldPwInput').value = data.d[16];
                                userrecentActivity(id);
                                if (!firstuserMap) {
                                    //getUserLocation(data.d[18], data.d[17], data.d[0]);
                                    locationAllowed = true;
                                    setTimeout(function () {
                                        google.maps.visualRefresh = true;
                                        var Liverpool = new google.maps.LatLng(data.d[17], data.d[18]);

                                        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                                        var mapOptions = {
                                            zoom: 8,
                                            center: Liverpool,
                                            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                                        };

                                        // This makes the div with id "map_canvas" a google map
                                        usermap = new google.maps.Map(document.getElementById("usermap_canvas"), mapOptions);
                                        usermarker = new google.maps.Marker({ position: Liverpool, map: usermap, title: data.d[0] });
                                        usermarker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                                    }, 1000);
                                    firstuserMap = true;
                                }
                                else
                                    updateUserLocation(data.d[18], data.d[17], data.d[0]);

                            
                                document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                document.getElementById('profileGender').innerHTML = data.d[20];
                                 
                                    

                                

                                if (document.getElementById('profileRoleName').innerHTML != "Level 7") {
                                    document.getElementById('deviceTypesDiv').innerHTML = "<i class='fa fa-mobile fa-2x mr-2x'></i><i style='color:lime;' class='fa fa-laptop fa-2x mr-2x'></i>";//data.d[11];

                                }
                            }
                            catch(err){}
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            catch(ex)
            {
                showAlert("Error 62: Problem occured while trying to get user profile. - " + ex)
            }
        }
        function assignUserProfileData(id) {
            try
            {
                getGroupnsOfUser(id)
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/getUserProfileData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            try{
                                document.getElementById('containerDiv').style.display="block";
                                document.getElementById('containerDiv2').style.display="none";
                                document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                document.getElementById('profileRoleName').innerHTML = data.d[5];
                                document.getElementById('profileManagerName').innerHTML = data.d[6];
                                document.getElementById('userStatusSpan').innerHTML = data.d[8];
                                var el = document.getElementById('userStatusIconSpan');
                                if (el) {
                                    el.className = data.d[9];
                                }

                                document.getElementById('changePWDIV').style.display = "none";

                                document.getElementById('userprofileImgSrc').src = data.d[10];
                                document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                document.getElementById('userFirstnameSpan').value = data.d[13];
                                document.getElementById('userLastnameSpan').value = data.d[14];
                                document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                document.getElementById('profileEmailAddEdit').value = data.d[3];

                                document.getElementById('oldPwInput').value = data.d[16];
                                userrecentActivity(id);
                                if (!firstuserMap) {
                                    //getUserLocation(data.d[18], data.d[17], data.d[0]);
                                    locationAllowed = true;
                                    setTimeout(function () {
                                        google.maps.visualRefresh = true;
                                        var Liverpool = new google.maps.LatLng(data.d[17], data.d[18]);

                                        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                                        var mapOptions = {
                                            zoom: 8,
                                            center: Liverpool,
                                            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                                        };

                                        // This makes the div with id "map_canvas" a google map
                                        usermap = new google.maps.Map(document.getElementById("usermap_canvas"), mapOptions);
                                        usermarker = new google.maps.Marker({ position: Liverpool, map: usermap, title: data.d[0] });
                                        usermarker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                                    }, 1000);
                                    firstuserMap = true;
                                }
                                else
                                    updateUserLocation(data.d[18], data.d[17], data.d[0]);

                            
                                document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                document.getElementById('profileGender').innerHTML = data.d[20];
                            }
                            catch(err){}
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            catch(ex)
            {
                showAlert("Error 62: Problem occured while trying to get user profile. - " + ex)
            }
        }
        function userrecentActivity(id) {
            document.getElementById('divrecentUserActivity').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Default.aspx/getUserRecentActivity",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else{
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row';

                            div.innerHTML = data.d[i];

                            document.getElementById('divrecentUserActivity').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        var usermap;
        var usermarker;
        function getUserLocation(longi,lati,uname) {

            locationAllowed = true;

            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(lati, longi);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 15,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            usermap = new google.maps.Map(document.getElementById("usermap_canvas"), mapOptions);
            usermarker = new google.maps.Marker({ position: Liverpool, map: usermap, title: uname });
            usermarker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
        }
        function updateUserLocation(longi, lati, uname) {
            usermarker.setMap(null);
            var Liverpool = new google.maps.LatLng(lati, longi);
            usermarker = new google.maps.Marker({ position: Liverpool, map: usermap, title: uname });
            usermarker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
        }
        function forceLogout()
        {
            document.getElementById('<%= closingbtn.ClientID %>').click();
        }
        //Reminders
        function newReminderInsert()
        {
            var topic  = document.getElementById("tbReminderName").value;
            var reminder  = document.getElementById("tbReminderNotes").value;
            var remdate  = document.getElementById("tbReminderDate").value;
            var todate = document.getElementById("toReminderDate").value;    
            var fromdate = document.getElementById("fromReminderDate2").value;
            var colorcode = 0;
            if (document.getElementById('cbRemBlue').checked) {
                colorcode = 1;
            }
            else if (document.getElementById('cbRemGreen').checked) {
                colorcode = 2;
            }
            else if (document.getElementById('cbRemYellow').checked) {
                colorcode = 3;
            }
            else if (document.getElementById('cbRemRed').checked) {
                colorcode = 0;
            }
            if(!isEmptyOrSpaces(topic))
            {
                if(!isSpecialChar(topic)){
                    var validReminder = true;
                    if(isSpecialChar(reminder))
                    {
                        validReminder = false;
                    }
                    if(validReminder){
                        jQuery.ajax({
                            type: "POST",
                            url: "Default.aspx/insertNewReminder",
                            data: "{'name':'" + topic + "','date':'" + remdate + "','reminder':'" + reminder+ "','colorcode':'" + colorcode + "','todate':'" + todate + "','fromdate':'" + fromdate + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if(data.d == "SUCCESS")
                                {
                                    jQuery('#newReminder').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Reminder created.";
                                    jQuery('#successfulDispatch').modal('show');
                                }
                                else if(data.d == "LOGOUT"){
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                                }
                                else
                                {
                                    showError("Error 63: Problem occured while trying to insert reminder. - " + data.d );
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                            }
                        });
                }
            }
        }
        else
        {
            showAlert("Kindly provide reminder topic.");
        }
    }
    //Dashboard Map
    function clearTasksFromDashMap(markers)
    {

    }
    function clearIncidentsFromDashMap(markers)
    {
        jQuery.ajax({
            type: "POST",
            url: "Default.aspx/getGPSData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (data.d == "]") {
                    //getLocationNoOnline();
                }
                else {
                    var obj = jQuery.parseJSON(data.d)
                    setMapOnAll(obj,markers);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    function clearOthersFromDashMap(markers)
    {

    }
    function clearUsersFromDashMap(markers)
    {
        jQuery.ajax({
            type: "POST",
            url: "Default.aspx/getGPSDataUsers",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (data.d == "]") {
                    //getLocationNoOnline();
                }
                else {
                    var obj = jQuery.parseJSON(data.d)
                    setMapOnAll(obj,markers);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    var polylinesArray = new Array();
    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    function updateGPSMapMarker(obj) {
        try {
            var poligonCoords = [];
            var subpoligonCoords = [];
            var first = true;
            var secondfirst = true;
            var previousColor = "";
            var currentSubLocation;
            var polyCounter = 0;

            for (var i = 0; i < Object.size(polylinesArray) ; i++) {
                if (polylinesArray[i] != null) {
                    polylinesArray[i].setMap(null);
                }
            }

            for (var i = 0; i < obj.length; i++) {

                if(obj[i].Logs == "User")
                {
                    if (obj[i].State == "BLUE" || obj[i].State == "OFFCLIENT" || obj[i].State == "10" || obj[i].State == "11" || obj[i].State == "12" || obj[i].State == "13" || obj[i].State == "14" || obj[i].State == "15")
                    {
                        if(obj[i].Cams == "NO CAM")
                        {
                            var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-user pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"></div>';
                        }
                        else{
                            var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-user pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#cameraDocument"  data-toggle="modal" onclick="camStart(&apos;' + obj[i].Cams + '&apos;,&apos;' + obj[i].Username + '&apos;)" ><i class="fa fa-eye red-color"></i>CAMERAS</a></div>';
                        }
                    }
                    else
                    {
                        var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-user pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Uname + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#user-profile-tab"  data-toggle="tab" onclick="assignUserProfileData(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                    }
                }
                else if (obj[i].Logs == "Task")
                {
                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#taskDocument"  data-toggle="modal" onclick="showTaskDocument(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                }
                else if (obj[i].Logs == "Verify")
                {
                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#verificationDocument"  data-toggle="modal" onclick="assignVerifierId(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                }
                else
                {
                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#viewDocument1"  data-toggle="modal" onclick="rowchoice(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                }
                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                //testsearchmap
                var myOption;
                myOption = document.createElement("Option");
                myOption.text = obj[i].Username; //Textbox's value
                myOption.value = obj[i].Lat+"-"+obj[i].Long; 
                usermapListBox.add(myOption);

                var isuser = false;
                if (obj[i].hasOwnProperty('Uname')) {
                    isuser = true;
                }

                if (obj[i].State == "YELLOW") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                    myMarkers[obj[i].Username+obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);
                }
                else if (obj[i].State == "GREEN") {
                    if(isuser){
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Uname + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                    else{
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                }
                else if (obj[i].State == "BLUE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                    myMarkers[obj[i].Username+obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);
                }
                else if (obj[i].State == "OFFUSER") {
                    if(isuser){
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Uname + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                    else{
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                }
                else if (obj[i].State == "OFFCLIENT") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                    myMarkers[obj[i].Username+obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);
                }
                else if (obj[i].State == "PURPLE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                    myMarkers[obj[i].Username+obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);
                }
                else {
                    if (secondfirst) {
                        currentSubLocation = obj[i].State;
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        if (obj[i].Logs == '#17ff33') {
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');
                        }
                        else {
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                        }
                        previousColor = obj[i].Logs;
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                        secondfirst = false;
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        subpoligonCoords.push(point);
                    }
                    else {
                        if (currentSubLocation == obj[i].State) {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                        else {
                            if (subpoligonCoords.length > 0) {
                                var subpoligon = new google.maps.Polyline({
                                    path: subpoligonCoords,
                                    geodesic: true,
                                    strokeColor: previousColor,
                                    strokeOpacity: 1.0,
                                    strokeWeight: 2
                                });
                                subpoligon.setMap(map);
                                polylinesArray[polyCounter] = subpoligon;
                                polyCounter++;
                            }
                            subpoligonCoords = [];
                            currentSubLocation = obj[i].State;
                            var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                            if (obj[i].Logs == '#17ff33') {
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');
                            }
                            else {
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                            }
                            previousColor = obj[i].Logs;
                            myMarkers[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindow(marker, contentString);
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                    }
                }
            }
            if (subpoligonCoords.length > 0) {
                var subpoligon = new google.maps.Polyline({
                    path: subpoligonCoords,
                    geodesic: true,
                    strokeColor: previousColor,
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                subpoligon.setMap(map);
                polylinesArray[polyCounter]= subpoligon;
                polyCounter++;
            }
        }
        catch (err) {
            showAlert("Error 64: Problem occured while trying to get gps data. - " + err );
        }
    }
    var firsttimeload = false;
    function togglemapDIV(mapview)
    {
        try
        {
            if (mapview == "USERS") {
                //document.getElementById("userSearchDIV").style.display = "block";
                document.getElementById("userSearchDIV").style.display = "none";
                clearIncidentsFromDashMap(myMarkers);
               // clearTasksFromDashMap(myMarkers);
              //  clearOthersFromDashMap(myMarkers);
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getGPSDataUsers",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d == "]") {
                            //getLocationNoOnline();
                        }
                        else {
                            var obj = jQuery.parseJSON(data.d)
                            updateGPSMapMarker(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                }); 
                    
                document.getElementById("usertoggle").style.display = "block";
                document.getElementById("maptoggle").style.display = "none";

                    
            }
            else if(mapview == "INCIDENTS"){
                document.getElementById("userSearchDIV").style.display = "none";
                if(firsttimeload){
                    clearUsersFromDashMap(myMarkers);
                   // clearTasksFromDashMap(myMarkers);
                   // clearOthersFromDashMap(myMarkers);
                    jQuery.ajax({
                        type: "POST",
                        url: "Default.aspx/getGPSData",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if(data.d == "LOGOUT"){
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else if (data.d == "]") {
                                //getLocationNoOnline();
                            }
                            else {
                                var obj = jQuery.parseJSON(data.d)
                                updateGPSMapMarker(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                        }
                    }); 
                    document.getElementById("maptoggle").style.display = "block";
                    document.getElementById("usertoggle").style.display = "none";
                }
                else{
                    firsttimeload = true;
                }
            }
        }
        catch(err)
        {
            showAlert("Error 66: Problem occured while trying to get toggle map data. - " + err );
        }
    }
    function sendpushMultipleNotification(selectedUserIds, messages) {
        jQuery.ajax({
            type: "POST",
            url: "Default.aspx/sendpushMultipleNotification",
            data: JSON.stringify({ userIds: selectedUserIds, msg: messages ,uname: loggedinUsername}),
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d =="LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    function getDispatchUserList(id, type) {
        document.getElementById("rowtracebackUser").style.display = "block";
        jQuery('#tracebackUserFilter div').html('');

        jQuery.ajax({
            type: "POST",
            url: "Default.aspx/getDispatchUserList",
            data: "{'id':'" + id + "','ttype':'" + type + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else
                    document.getElementById("tracebackUserFilter").innerHTML = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    function tracebackOn(type) {
        var id = 0;
        if (type == "task") {
            id = document.getElementById('rowChoiceTasks').value;
        }
        else
            id = document.getElementById('rowidChoice').text;
           
        dduration = "0";

        $.ajax({
            type: "POST",
            url: "Default.aspx/getTracebackLocationData",
            data: "{'id':'" + id + "','ttype':'" + type + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }else{
                    var obj = $.parseJSON(data.d)
                    
                    if (obj.length > 1) {
                        if (type == "task") {
                            updateTaskMarker(obj);
                            document.getElementById("rowtasktracebackUser").style.display = "block";
                        }
                        else {
                            updateIncidentMarker(obj);
                            getDispatchUserList(id, type);
                        }
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    var sselectedUserIds = [];
    function tracebackOnUser(id,type,uid)
    {
        var ell = document.getElementById(id + uid);
        if (ell) {
            if (ell.className == 'fa fa-square-o') {
                ell.className = 'fa fa-check-square-o';

                var index = sselectedUserIds.indexOf(uid);
                if (index > -1) {
                    sselectedUserIds.splice(index, 1);
                }
            }
            else {
                ell.className = 'fa fa-square-o';
                var a = sselectedUserIds.indexOf(uid);

                if (a < 0)
                    sselectedUserIds.push(uid);

            }
            $.ajax({
                type: "POST",
                url: "Default.aspx/getTracebackLocationDataByUser",
                data: JSON.stringify({ id: id, duration: dduration, ttype: type, userIds: sselectedUserIds ,uname: loggedinUsername}),
                //data: "{'id':'" + id + "','duration':'" + dduration + "','ttype':'" + type + "','userIds':'" + uid + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        var obj = $.parseJSON(data.d)

                        if (type == "task")
                            updateTaskMarker(obj);
                        else
                            updateIncidentMarker(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
    }
    var dduration = "";
    function changeFilterMarkers(id,type, duration) {

        if (type == "task") {
            if (duration == "1") {
                var el = document.getElementById('taskfilter1');
                if (el) {
                    if (el.className == 'fa fa-check-square-o'){
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('taskfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('taskfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('taskfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "5") {
                var el2 = document.getElementById('taskfilter2');
                if (el2) {
                    if (el2.className == 'fa fa-check-square-o'){
                        el2.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el2.className = 'fa fa-check-square-o';
                }
                var ell = document.getElementById('taskfilter1');
                if (ell) {
                    ell.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('taskfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('taskfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "30") {
                var el3 = document.getElementById('taskfilter3');
                if (el3) {
                    if (el3.className == 'fa fa-check-square-o'){
                        el3.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el3.className = 'fa fa-check-square-o';
                }
                var ell = document.getElementById('taskfilter1');
                if (ell) {
                    ell.className = 'fa fa-square-o';
                }
                var ell2 = document.getElementById('taskfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('taskfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "60") {
                var el4 = document.getElementById('taskfilter4');
                if (el4) {
                    if (el4.className == 'fa fa-check-square-o'){
                        el4.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el4.className = 'fa fa-check-square-o';
                }
                var ell = document.getElementById('taskfilter1');
                if (ell) {
                    ell.className = 'fa fa-square-o';
                }
                var ell2 = document.getElementById('taskfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('taskfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
            }
        }
        else {
            if (duration == "1") {
                var el = document.getElementById('filter1');
                if (el) {
                    if (el.className == 'fa fa-check-square-o'){
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var elll2 = document.getElementById('filter2');
                if (elll2) {
                    elll2.className = 'fa fa-square-o';
                }
                var elll3 = document.getElementById('filter3');
                if (elll3) {
                    elll3.className = 'fa fa-square-o';
                }
                var elll4 = document.getElementById('filter4');
                if (elll4) {
                    elll4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "5") {
                var el2 = document.getElementById('filter2');
                if (el2) {
                    if (el2.className == 'fa fa-check-square-o'){
                        el2.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el2.className = 'fa fa-check-square-o';
                }
                var elll = document.getElementById('filter1');
                if (elll) {
                    elll.className = 'fa fa-square-o';
                }
                var elll3 = document.getElementById('filter3');
                if (elll3) {
                    elll3.className = 'fa fa-square-o';
                }
                var elll4 = document.getElementById('filter4');
                if (elll4) {
                    elll4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "30") {
                var el3 = document.getElementById('filter3');
                if (el3) {
                    if (el3.className == 'fa fa-check-square-o') {
                        el3.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el3.className = 'fa fa-check-square-o';
                }
                var elll = document.getElementById('filter1');
                if (elll) {
                    elll.className = 'fa fa-square-o';
                }
                var elll2 = document.getElementById('filter2');
                if (elll2) {
                    elll2.className = 'fa fa-square-o';
                }
                var elll4 = document.getElementById('filter4');
                if (elll4) {
                    elll4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "60") {
                var el4 = document.getElementById('filter4');
                if (el4) {
                    if (el4.className == 'fa fa-check-square-o') {
                        el4.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el4.className = 'fa fa-check-square-o';
                }
                var elll = document.getElementById('filter1');
                if (elll) {
                    elll.className = 'fa fa-square-o';
                }
                var elll2 = document.getElementById('filter2');
                if (elll2) {
                    elll2.className = 'fa fa-square-o';
                }
                var elll3 = document.getElementById('filter3');
                if (elll3) {
                    elll3.className = 'fa fa-square-o';
                }
            }
        }
        dduration = duration;
        $.ajax({
            type: "POST",
            url: "Default.aspx/getTracebackLocationDataByUser",
            data: JSON.stringify({ id: id, duration: dduration, ttype: type, userIds: sselectedUserIds,uname: loggedinUsername }),
            //data: "{'id':'" + id + "','duration':'" + dduration + "','ttype':'" + type + "','userIds':'" + uid + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }else{
                    var obj = $.parseJSON(data.d)

                    if (type == "task")
                        updateTaskMarker(obj);
                    else
                        updateIncidentMarker(obj);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    function tracebackOnFilter(duration, type) {

        var id = 0;
        if (type == "task")
            id = document.getElementById('rowChoiceTasks').value;
        else
            id = document.getElementById('rowidChoice').text;

        changeFilterMarkers(id,type, duration);
    }
    function getGeofenceSuccess(stringVal) {
        var output = "";
        $.ajax({
            type: "POST",
            url: "Default.aspx/geofenceSuccess",
            data: "{'returnV':'" + stringVal + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else
                    output = data.d;
            }
        });
        return output;
    }

    function reminderChoice(id, time, description, name, color) {
        document.getElementById('rowidReminderChoice').text = id;
        document.getElementById(color + 'RemName').innerHTML = name;
        document.getElementById(color + 'RemDate').innerHTML = time;
        document.getElementById(color + 'RemDesc').innerHTML = description;
    }
    function reminderRead() {
        var id = document.getElementById('rowidReminderChoice').text;
        jQuery.ajax({
            type: "POST",
            url: "Default.aspx/markReminderAsRead",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (data.d == "SUCCESS") {
                    showLoader();
                    location.reload();
                }
                else {
                    showAlert("Error 71: Problem occured while trying to mark reminder as read. - "+data.d);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    function reminderDelete() {
        var id = document.getElementById('rowidReminderChoice').text;
        jQuery.ajax({
            type: "POST",
            url: "Default.aspx/deleteReminder",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (data.d == "SUCCESS") {
                    showLoader();
                    location.reload();
                }
                else {
                    showAlert("Error 72: Problem occured while trying to delete reminder. - "+data.d);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    function checkLiveVideo(camera) {
        var output = "";
        $.ajax({
            type: "POST",
            url: "Default.aspx/checkLiveVideo",
            data: "{'camera':'" + camera + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else
                    output = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
        return output;
    }
    var isViewCamDuration = false;
    function showViewCamDuration() {
        if (!isViewCamDuration) {
            document.getElementById("camViewDurationDiv").style.display = "block";
            isViewCamDuration = true;
        }
        else {
            document.getElementById("camViewDurationDiv").style.display = "none";
            isViewCamDuration = false;
        }
    }
    function viewdurationselect(dur) {
        try{
            //camfilter1
            document.getElementById('cameraDuration').text = dur;
            if (dur == 15) {
                var el = document.getElementById('camviewfilter1');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }

            }
            else if (dur == 30) {
                var el = document.getElementById('camviewfilter2');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 45) {
                var el = document.getElementById('camviewfilter3');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 60) {
                var el = document.getElementById('camviewfilter4');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter3');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 120) {
                var el = document.getElementById('camviewfilter5');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter3');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
        }
        catch(err)
        {showAlert("Error 65: Problem occured while trying to set camera duration. - " + err );}
    }
    function deleteAttachmentChoiceAsset(id) {
        jQuery('#deleteAttachModal').modal('show');
        document.getElementById('rowidChoiceAttachment').value = id;
        jQuery('#viewDocument1').modal('hide');
    }
    function deleteAttachment() {
        jQuery.ajax({
            type: "POST",
            url: "Default.aspx/deleteAttachmentData",
            data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','incidentid':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d != "LOGOUT") {
                    showAlert(data.d);
                    rowchoice(document.getElementById('rowidChoice').text);
                    jQuery('#viewDocument1').modal('show');
                }
                else {
                    document.getElementById('<%= logoutbtn.ClientID %>').click();
                }
            }
        });
    }
    function uploadattachment(id,imgpath)
    {
        if (typeof imgpath === 'undefined') {
                
        }
        else {
            var res = imgpath.replace(/\\/g, "|")
            try {
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/uploadMobileAttachment",
                    data: "{'id':'" + id + "','imgpath':'" + res + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            catch (err) {
                alert(err);
            }
        }
    }
        
    function displayIncident(){
        jQuery('#viewDocument1').modal('show');
    }
    var width = 1;
    function move() {
        var elem = document.getElementById("myBar");
        width = 1;
        var id = setInterval(frame, 100);
        function frame() {
            if (width >= 100) {
                clearInterval(id);
            } else {
                width++;
                elem.style.width = width + '%';
            }
        }
    }
    function generateIncidentPDF() {
        width = 1;
        move();
        document.getElementById("pdfloadingAccept").style.display = "block";
        jQuery.ajax({
            type: "POST",
            url: "Default.aspx/CreatePDFIncident",
            data: "{'id':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    document.getElementById('<%= logoutbtn.ClientID %>').click();
                }
                else if (data.d != "Failed to generate report!") {
                    jQuery('#viewDocument1').modal('hide');
                    window.open(data.d);
                    document.getElementById('successfulReportScenario').innerHTML = "Report successfully created!";
                    jQuery('#successfulReport').modal('show');
                    document.getElementById("pdfloadingAccept").style.display = "none";
                }
                else {
                    showError(data.d);
                    document.getElementById("pdfloadingAccept").style.display = "none";
                }
            }
        });
    }

        var firstticketpress = false;
        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
        //function autoScrollDown(){
        //    jQuery(".inner").css({top:-jQuery('.inner').height()}) // jump back
        //               .animate({top:0},8000,"linear", autoScrollDown); // and animate
        //}
        //function autoScrollDown2(){
        //    jQuery(".inner2").css({top:-jQuery('.inner2').height()}) // jump back
        //               .animate({top:0},8000,"linear", autoScrollDown); // and animate
        //}
        function autoScrollUp(){
            try{
                jQuery(".inner").css({top:0}) // jump back
                           .animate({top:-jQuery('.inner').height()},jQuery('.inner').height()*200,"linear", autoScrollUp); // and animate
            }
            catch(err){

            }
        }
        function autoScrollUp2(){
            try{
                jQuery(".inner2").css({top:0}) // jump back
                           .animate({top:-jQuery('.inner2').height()},jQuery('.inner2').height()*200,"linear", autoScrollUp2); // and animate
            }
            catch(err){

            }
        }
        function autoScrollUp3() {
            try {
                jQuery(".inner3").css({ top: 0 }) // jump back
                           .animate({ top: -jQuery('.inner3').height() }, jQuery('.inner3').height() * 200, "linear", autoScrollUp3); // and animate
            }
            catch (err) {

            }
        }
            function myFunction(xml) {
                var xmlDoc = xml.responseXML;
                var x = xmlDoc.getElementsByTagName('item')[0].getElementsByTagName('description')[0];
                var y = x.childNodes[0];
                var weathersplit = y.nodeValue.split(',');
                jQuery("#weatherfeedDIV").append("<div class='row' style='padding-top:10px;'><div class='col-md-12'><div class='col-md-12'><p><i class='wi wi-thermometer pr-1x'></i>"+weathersplit[0]+"</p></div><div class='col-md-12'><p><i class='wi wi-wind-direction pr-1x'></i>"+weathersplit[1]+"</p></div><div class='col-md-12'><p><i class='wi wi-strong-wind pr-1x'></i>"+weathersplit[2]+"</p></div><div class='col-md-12'><p><i class='wi wi-humidity pr-1x'></i>"+weathersplit[3]+"</p></div><div class='col-md-12'><p><i class='wi wi-barometer pr-1x'></i>"+weathersplit[4]+"</p></div></div></div>");
 
            }   
            jQuery(function ($) {
                $("#newsfeedDIV").rss("<%=rssfeedurl%>",
                {
                    entryTemplate: "<li><p class='red-color' style='font-size:12px;'><b>{date}</b></p><p style='cursor:pointer;' onclick='window.open(&apos;{url}&apos;);'><b>{title}</b></p><p>{shortBodyPlain}</p></li>"

                    //entryTemplate: "<li><div class='row' style='margin-top:-15px;'><div class='col-md-12'><div class='col-md-12'><p class='red-color' style='font-size:12px;'><b>{date}</b></p></div><div class='col-md-12'><p><b>{title}</b></p></div><div class='col-md-12'><p>{shortBodyPlain}</p></div></div></div></li>"
                    //'<li><a href="{url}">[{author}@{date}] {title}</a><br/>{teaserImage}{shortBodyPlain}</li>'
                })
                //setTimeout(function () { jQuery('.outer3').css({ maxHeight: 300 }); autoScrollUp3(); }, 1000);
               
                
            })

            jQuery(document).ready(function () {
                try { 
                    var count = 0;
                    var nowDate = new Date(); 
                    var date = nowDate.getDate() + '/' + (nowDate.getMonth() + 1) + '/' + nowDate.getFullYear();
                    document.getElementById('dttimenow').innerHTML = date;
                    
                    
          //          jQuery.getJSON('https://newsapi.org/v2/top-headlines?' +
          //'sources=bbc-news&' +
          //'apiKey=e7f665050f01485da085cf0d86792b67', function (json) {
          //    var html = "";
          //    jQuery(json.articles).each(function (index, value) {
          //        if(count < 3)
          //        {     
          //            var s = value.publishedAt.split('T')[0] + " "+value.publishedAt.split('T')[1].split('Z')[0];
                 
          //            jQuery("#newsfeedDIV").append("<div class='row' style='padding-top:10px;'><div class='col-md-12'><div class='col-md-12'><p class='red-color' style='font-size:12px;'><b>"+s +"</b></p></div><div class='col-md-12'><p><b>"+ value.title +"</b></p></div><div class='col-md-12'><p>"+ value.description +"</p></div></div></div>");

          //            count++;
          //        }
          //    });
          //});
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            myFunction(this);
                        }
                    };
                    xhttp.open("GET", "https://weather-broker-cdn.api.bbci.co.uk/en/observation/rss/292223", true);
                    xhttp.send();
                   
                    localStorage.removeItem("activeTabDev");
                    localStorage.removeItem("activeTabInci");
                    localStorage.removeItem("activeTabMessage");
                    localStorage.removeItem("activeTabTask");
                    localStorage.removeItem("activeTabTick");
                    localStorage.removeItem("activeTabUB");
                    localStorage.removeItem("activeTabVer");
                    localStorage.removeItem("activeTabLost"); 
        
                    $('input[type=password]').keyup(function () {
                        // keyup event code here
                        var pswd = $(this).val();
                        if (pswd.length < 8) {
                            $('#length').removeClass('valid').addClass('invalid');
                            lengthGood = false;
                        } else {
                            $('#length').removeClass('invalid').addClass('valid');
                            lengthGood = true;
                        }
                        //validate letter
                        if (pswd.match(/[A-z]/)) {
                            $('#letter').removeClass('invalid').addClass('valid');
                            letterGood = true;
                    
                        } else {
                            $('#letter').removeClass('valid').addClass('invalid');
                            letterGood = false;
                        }

                        //validate capital letter
                        if (pswd.match(/[A-Z]/)) {
                            $('#capital').removeClass('invalid').addClass('valid');
                            capitalGood = true;
                        } else {
                            $('#capital').removeClass('valid').addClass('invalid');
                    
                            capitalGood = false;
                        }

                        //validate number
                        if (pswd.match(/\d/)) {
                            $('#number').removeClass('invalid').addClass('valid');
                            numGood = true;
                   
                        } else {
                            $('#number').removeClass('valid').addClass('invalid');
                            numGood = false;
                        }
                    });
                    $('input[type=password]').focus(function () {
                        // focus code here
                        $('#pswd_info').show();
                    });
                    $('input[type=password]').blur(function () {
                        // blur code here
                        $('#pswd_info').hide();
                    });
                    getMessageBoards();


                    
                    
                }catch(err){
                    console.log(err)
                }   
            });
        function savehappyCount(type){
            jQuery.ajax({
                type: "POST",
                url: "MessageBoard.aspx/saveHappinessCount",
                data: "{'id':'"+type+"','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) { 
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        showAlert("Thank you for answering the survey");
                        document.getElementById("smileydiv").style.display = "none";
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getMessageBoards() { 
            jQuery.ajax({
                type: "POST",
                url: "MessageBoard.aspx/getTableDataFlashAlert",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
 
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
 
                        $("#inner").append(data.d.MessagePost);
 
                        $("#inner2").append(data.d.FlashAlerts); 
                        
                        
                        // fix hight of outer:
                        jQuery('.outer').css({maxHeight: 170});
                        // duplicate content of inner:
                        //jQuery('.inner').html(jQuery('.inner').html() + jQuery('.inner').html());
                        autoScrollUp();
                        //jQuery('.outer2').css({maxHeight: 170});
                        //autoScrollUp2();

                        // duplicate content of inner:
                        //jQuery('.inner2').html(jQuery('.inner2').html() + jQuery('.inner2').html());
                      
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
 

            function getTaskLocationMarkers(obj) {

                locationAllowed = true;
                //setTimeout(function () {
                google.maps.visualRefresh = true;

                var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 15,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                mapTaskLocation = new google.maps.Map(document.getElementById("taskmap_canvasIncidentLocation"), mapOptions);
                var first = true;
                var poligonCoords = [];
                var tbcounter = 0;
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div id="content">' + obj[i].Username +
                    '<br/></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    
                    if (obj[i].Username == "Pending") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "InProgress") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "Completed") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "Accepted") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].State == "RED") {
                        if (first) {
                            first = false;
                            document.getElementById('previousTaskUser').text = obj[i].Username;
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);

                        }
                        else {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                        }
                    }
                    else {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        myMarkersTasksLocation[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                }
            }

            function createInfoWindowTaskLocation(marker, popupContent) {
                google.maps.event.addListener(marker, 'click', function () {
                    infoWindowTaskLocation.setContent(popupContent);
                    infoWindowTaskLocation.open(mapTaskLocation, this);
                });
            }
            function fromReminderDateChange()
            {
                var select = document.getElementById("toReminderDate");
                document.getElementById("toReminderDate").options.length = 0;
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getDateRangeReminder",
                    data: "{'date':'" + document.getElementById('fromReminderDate2').value+ "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                var opt = document.createElement('option');
                                opt.value = data.d[i];
                                opt.innerHTML = data.d[i];
                                select.appendChild(opt);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function getReminderSelectorDates()
            {
                var select1 = document.getElementById('fromReminderDate2');
                var select2 = document.getElementById('toReminderDate');
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getReminderSelectorDates",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {                
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                var opt = document.createElement('option');
                                opt.value = data.d[i];
                                opt.innerHTML = data.d[i];
                                select1.appendChild(opt);
                                var opt2 = document.createElement('option');
                                opt2.value = data.d[i];
                                opt2.innerHTML = data.d[i];
                                select2.appendChild(opt2);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function getActivityReport()
            {
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getActivityReportData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            var bar4Ctx = document.getElementById('chartjs-barActivity').getContext('2d'),
                            bar4Data = {
                                labels: ['24h', '18h', '12h','6h'],
                                datasets: [
                                  { label: 'Clothes', fillColor: 'rgba(237, 85, 101, 1)', highlightFill: 'rgba(237, 85, 101, 0.8)', data: [data.d[0],data.d[2],data.d[4],data.d[6]] },
                                  { label: 'Trousers', fillColor: 'rgba(93, 156, 236, 1)', highlightFill: 'rgba(93, 156, 236, 0.8)', data: [data.d[1],data.d[3],data.d[5],data.d[7]] }
                                ]
                            },  
                            chartjsBar4 = new Chart(bar4Ctx).Bar(bar4Data, {
                                scaleShowVerticalLines: false,
                                barShowStroke : false,
                                barValueSpacing : 16,
                                responsive: false,
                            });
                        }

                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function topOffencesChart()
            {
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getOffenceChart",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            var lineCtx = document.getElementById('chartjs-linez').getContext('2d'),
                            lineData = {
                                labels: [data.d[0], data.d[1], data.d[2], data.d[3], data.d[4]],
                                datasets: [
                                  {
                                      label: 'User1',
                                      fillColor: 'transparent',
                                      strokeColor: '#1b93c0',
                                      pointColor: '#1b93c0',
                                      pointStrokeColor: '#ffffff',
                                      pointHighlightFill: '#ffffff',
                                      pointHighlightStroke: '#1b93c0',
                                      data: [0, data.d[5], data.d[10], data.d[15], data.d[20]]
                                  },
                                  {
                                      label: 'User2',
                                      fillColor: 'transparent',
                                      strokeColor: '#3ebb64',
                                      pointColor: '#3ebb64',
                                      pointStrokeColor: '#ffffff',
                                      pointHighlightFill: '#ffffff',
                                      pointHighlightStroke: '#3ebb64',
                                      data: [0, data.d[6], data.d[11], data.d[16], data.d[21]]
                                  },
                                  {
                                      label: 'User3',
                                      fillColor: 'transparent',
                                      strokeColor: '#f2c400',
                                      pointColor: '#f2c400',
                                      pointStrokeColor: '#ffffff',
                                      pointHighlightFill: '#ffffff',
                                      pointHighlightStroke: '#f2c400',
                                      data: [0, data.d[7], data.d[12], data.d[17], data.d[22]]
                                  },
                                  {
                                      label: 'User4',
                                      fillColor: 'transparent',
                                      strokeColor: '#f44e4b',
                                      pointColor: '#f44e4b',
                                      pointStrokeColor: '#ffffff',
                                      pointHighlightFill: '#ffffff',
                                      pointHighlightStroke: '#f44e4b',
                                      data: [0, data.d[8], data.d[13], data.d[18], data.d[23]]
                                  },
                                  {
                                      label: 'User5',
                                      fillColor: 'transparent',
                                      strokeColor: '#b2163b',
                                      pointColor: '#b2163b',
                                      pointStrokeColor: '#ffffff',
                                      pointHighlightFill: '#ffffff',
                                      pointHighlightStroke: '#b2163b',
                                      data: [0, data.d[9], data.d[14], data.d[19], data.d[24]]
                                  }
                                ]
                            },
                            chartjsLine = new Chart(lineCtx).Line(lineData, {
                                scaleBeginAtZero: true,
                                scaleShowVerticalLines: false,
                                responsive: false,
                            });
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function RejectionHandle() {
                document.getElementById("rejectionTextarea").style.display = "block";
                document.getElementById("resolutionTextarea").style.display = "none";
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "none";
                document.getElementById("completedOptionsDiv").style.display = "none";
                document.getElementById("completedOptionsDiv2").style.display = "none";
                document.getElementById("dispatchOptionsDiv").style.display = "none";
                document.getElementById("rejectOptionsDiv").style.display = "block";
            }
            function dispatchIncident() {
                try {
                    var inciName = document.getElementById("incidentNameHeader").innerHTML;
                    var id = document.getElementById("rowidChoice").text;
                    var list = document.getElementById("sendToListBox");
                    var isCameraAttached = document.getElementById("cameraCheck").checked;
                    var isTaskAttached = document.getElementById("TaskCheck").checked;
                    var longi = document.getElementById("rowLongitude").text;
                    var lati = document.getElementById("rowLatitude").text;
                    var instruction = document.getElementById("selectinstructionTextarea").value;
                    var camDetails = "";
                    var cameraValue;
                    var taskValue;
                    var isErr = false;
                    if(isSpecialChar(instruction)){
                        isErr = true;
                    }
                    if (isTaskAttached) {
                        var msgtemplate2 = document.getElementById("<%=selectTaskTemplate.ClientID%>").options[document.getElementById("<%=selectTaskTemplate.ClientID%>").selectedIndex].value;
                        if (msgtemplate2 > 0) {
                            taskValue = msgtemplate2;
                        }
                        else {
                            isErr = true;
                            showAlert("Please provide a task you wish to attach");
                        }
                    }
                    if (isCameraAttached) {
                        var msgtemplate = document.getElementById("<%=cameraSelect.ClientID%>").options[document.getElementById("<%=cameraSelect.ClientID%>").selectedIndex].value; 
                        if (msgtemplate == "Select Camera") {
                            isErr = true;
                            showAlert("Please provide a camera you wish to attach");
                        }
                        else {
                            var camName = document.getElementById("<%=cameraSelect.ClientID%>").options[document.getElementById("<%=cameraSelect.ClientID%>").selectedIndex].text; 
                            var checkVal = checkLiveVideo(camName);
                            if (checkVal == "SUCCESS") {
                                cameraValue = msgtemplate;
                                camDetails = cameraValue;
                                if (typeof document.getElementById("cameraDuration").text === 'undefined') {
                                    isErr = true;
                                    showAlert("Please select the camera duration");
                                }
                                else {
                                    instruction = instruction + "|" + document.getElementById('cameraDuration').text;
                                }
                            }
                            else {
                                isErr = true;
                                showAlert(checkVal);
                            }
                        }
                    }
                    document.getElementById("<%=tbincidentName.ClientID%>").value = inciName;
                    if (!isErr) {
                        var selected = [];
                        if (list.length > 0) {
                            IncidentStateChange('Dispatch');
                            var i;
                            for (i = 0; i < list.length; i++) {
                                if (isTaskAttached) {
                                    insertNewTask(isTaskAttached, getAssigneeType(list.options[i].value), list.options[i].text, list.options[i].value, taskValue, longi, lati, id);
                                }
                        
                                if (getAssigneeType(list.options[i].value) == "User") {
                            
                                    chat.server.sendIncidenttoUser(btoa(list.options[i].text), "ARL┴" + inciName + "┴" + instruction + "┴" + lati + "┴" + longi+ "≈" + camDetails, list.options[i].value, id);
                                    selected.push(list.options[i].text);
                                }
                                else {
                                    chat.server.sendIncidenttoDevice(btoa(list.options[i].text), "ARL┴" + inciName + "┴" + instruction + "┴" + lati + "┴" + longi+ "≈" + camDetails, id);
                                }
                            }
                            if (selected.length > 0)
                                sendpushMultipleNotification(selected, inciName);
                        }
                        else
                        {
                            isErr = true;
                            showAlert("Please provide assignees");
                        }
                    }
                    if (!isErr) {
                        jQuery('#viewDocument1').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = inciName + " has been successfully Dispatch";
                        jQuery('#successfulDispatch').modal('show');
                    }
                }
                catch(err)
                {
                    showAlert("Error 67: Problem occured while trying to dispatch incident. - " + err );
                }
            }
            function getAssigneeType(stringVal)
            {
                var output = "";
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getAssigneeType",
                    data: "{'id':'" + stringVal + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            output =  data.d;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
                return output;
            }
            function IncidentStateChange(state)
            {
                document.getElementById("mobilehotEvSearch").value = "";


                var id = document.getElementById("rowidChoice").text;
                var ins = document.getElementById("selectinstructionTextarea").value;
                var isErr = false;
                if (state == "Reject") {
                    ins = document.getElementById("rejectionTextarea").value;

                    if (isEmptyOrSpaces(ins)) {
                        ins = document.getElementById("resolutionTextarea").value;
                        if (isEmptyOrSpaces(ins)) {
                            isErr = true;
                            showAlert("Kindly add the rejected notes");
                        }
                    }
                }
                if (state == "Resolve") {
                    ins = document.getElementById("resolutionTextarea").value;
                }
                if (!isEmptyOrSpaces(ins)) {
                    if (isSpecialChar(ins)) {
                        isErr = true;
                        showAlert("Kindly remove special character from text area");
                    }
                }
                if (!isErr) {

                    var imgPath = document.getElementById("mobimagePath2").text;
                    uploadattachment(id, imgPath);
                    jQuery.ajax({
                        type: "POST",
                        url: "Default.aspx/changeIncidentState",
                        data: "{'id':'" + id + "','state':'" + state + "','ins':'" + ins + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            //addrowtoTable();
                            if (data.d == "") {
                                showAlert("Error changing incident state");
                            }
                            else if (data.d == "LOGOUT") {
                                document.getElementById('<%= logoutbtn.ClientID %>').click();
                            }
                            else if (state != "Dispatch") {
                                jQuery('#viewDocument1').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = data.d + " status has been changed to " + state;
                                jQuery('#successfulDispatch').modal('show');
                            }
                     
                            //togglemapDIV('INCIDENTS');
                        }
                    });
                }
            }
            function insertNewTask(id, assigneetype, assigneename, assigneeid, templatename, longi, lati, incidentId) {
                var output = "";
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/inserttask",
                    data: "{'id':'" + id + "','assigneetype':'" + assigneetype + "','assigneename':'" + assigneename + "','assigneeid':'" + assigneeid + "','templatename':'" + templatename + "','longi':'" + longi + "','lati':'" + lati + "','incidentId':'" + incidentId + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else{
                            output = data.d;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
                return output;
            }
            function insertNewIncident(name, desc, locationid, incidenttype, notificationid, taskid, receivedby, longi, lati, status, ins, msgtask, incidentId, isrej, remarks) {
                var output = "";
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/insertNewIncident",
                    data: "{'name':'" + name + "','desc':'" + desc + "','locationid':'" + locationid + "','incidenttype':'" + incidenttype + "','notificationid':'" + notificationid + "','taskid':'" + taskid + "','receivedby':'" + receivedby + "','longi':'" + longi + "','lati':'" + lati + "','status':'" + status + "','instructions':'" + ins + "','msgtask':'" + msgtask + "','incidentId':'" + incidentId + "','isrej':'" + isrej + "','remarks':'" + remarks + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else{
                            output = data.d;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
                return output;
            }
            function getLocationMarkers(obj) {
                try
                {
                    locationAllowed = true;
                    //setTimeout(function () {
                    google.maps.visualRefresh = true;
                    var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                    for (var i = 0; i < obj.length; i++) {

                        if (obj[i].State == "PURPLE") {
                            Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        }
                    }

                    // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                    var mapOptions = {
                        zoom: 8,
                        center: Liverpool,
                        mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                    };

                    // This makes the div with id "map_canvas" a google map
                    mapLocation = new google.maps.Map(document.getElementById("map_canvasIncidentLocation"), mapOptions);
                    var poligonCoords = [];
                    var first = true;
                    for (var i = 0; i < obj.length; i++) {

                        var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Uname + '</p></div><div class="help-block text-center"><a id="' + obj[i].Id + obj[i].Username + '" href="#" class="red-borders light-button red-color" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;,&apos;' + obj[i].Uname + '&apos;)"  style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color"><i class="fa fa-plus  red-color"></i>ADD</a></div>';
                        //'<div class="map-container inline-block"><div class="map-popup"><div class="help-block text-center pt-2x"><i class="fa fa-mobile"></i><p class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"><a id="' + obj[i].Id + obj[i].Username + '" href="#" class="red-borders light-button red-color" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div></div><div class="map-selector"></div></div> ';
                        var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    
                        if (obj[i].State == "YELLOW") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "GREEN") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "BLUE") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "OFFUSER") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "OFFCLIENT") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "PURPLE") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png')
                            contentString = '<div class="help-block text-center pt-2x"><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"></div>';
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                            document.getElementById("rowIncidentName").text = obj[i].Username+obj[i].Id;  
                        }
                        else if (obj[i].State == "RED") {
                            if (first) {
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png');
                                contentString = '<div class="help-block text-center pt-2x"><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"></div>';
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocation(marker, contentString);
                                document.getElementById("rowIncidentName").text = obj[i].Username+obj[i].Id;  
                                first = false;
                            }
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);
                        }

                    }
                    if (poligonCoords.length > 0) {
                        updatepoligon = new google.maps.Polyline({
                            path: poligonCoords,
                            geodesic: true,
                            strokeColor: '#FF0000',
                            strokeOpacity: 1.0,
                            strokeWeight: 2
                        });
                        updatepoligon.setMap(mapLocation);
                    }
                }catch(err)
                {showAlert("Error 68: Problem occured while trying to load map. - " + err );}
            }

            function createInfoWindowLocation(marker, popupContent) {
                google.maps.event.addListener(marker, 'click', function () {
                    infoWindowLocation.setContent(popupContent);
                    infoWindowLocation.open(mapLocation, this);
                });
            }

            function play(i) {
                try {
                    var player = document.getElementById('Video' + (i + 1));
                    player.play();
                } catch (error) {
                    //showAlert("Error 69: Problem occured while trying to play video. - " + error );
                }
            }
            var tracebackpoligon;
            function updateIncidentMarker(obj)
            {
                try
                {
                    var poligonCoords = [];
                    var tracepoligonCoords = [];
                    var first = true;
                    var first2 = true;
                    var currentSubLocation;
                    var secondfirst = true;
                    var previousColor = "";
                    var subpoligonCoords = [];
                    var first = true;
                    var previousMarker = document.getElementById("rowIncidentName").text;
                    for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                        if (myTraceBackMarkers[i] != null) {
                            myTraceBackMarkers[i].setMap(null);
                        }
                    }
                    if (typeof tracebackpoligon === 'undefined') {
                        // your code here.
                    }
                    else {
                        tracebackpoligon.setMap(null);
                    }
                    if(typeof previousMarker === 'undefined'){
                        // your code here.
                    }
                    else
                    {
                        myMarkersLocation[previousMarker].setMap(null);
                        if(typeof updatepoligon === 'undefined'){
                            // your code here.
                        }
                        else
                        {
                            updatepoligon.setMap(null);
                        }
                    }
                    var tbcounter = 0;
                
                    for (var i = 0; i < obj.length; i++) {

                        var isuser = false;
                        if (obj[i].hasOwnProperty('Uname')) {
                            isuser = true;
                        }


                        var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Uname + '</p></div><div class="help-block text-center"><a id="' + obj[i].Id + obj[i].Username + '" href="#" class="red-borders light-button red-color" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;,&apos;' + obj[i].Uname + '&apos;)" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color"><i class="fa fa-plus  red-color"></i>ADD</a></div>';
                        //'<div class="map-container inline-block"><div class="map-popup"><div class="help-block text-center pt-2x"><i class="fa fa-mobile"></i><p class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"><a id="' + obj[i].Id + obj[i].Username + '" href="#" class="red-borders light-button red-color" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div></div><div class="map-selector"></div></div> ';
                        var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        mapLocation.setCenter(new google.maps.LatLng(obj[i].Lat, obj[i].Long));
                        mapLocation.setZoom(9);
                 
                        if (obj[i].State == "YELLOW") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "GREEN") {
                            if (isuser) {
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Uname });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocation(marker, contentString);
                            }
                            else {
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocation(marker, contentString);
                            }

                        }
                        else if (obj[i].State == "BLUE") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "OFFUSER") {
                            if (isuser) {
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Uname });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocation(marker, contentString);
                            }
                            else {
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocation(marker, contentString);
                            }

                        }
                        else if (obj[i].State == "OFFCLIENT") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "PURPLE") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png')
                            contentString = '<div class="help-block text-center pt-2x"><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"></div>';
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocation(marker, contentString);
                            document.getElementById("rowIncidentName").text = obj[i].Username+obj[i].Id;  
                        }
                        else if (obj[i].State == "ENG") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                            contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                            createInfoWindowLocation(marker, contentString);
                        }
                        else if (obj[i].State == "COM") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png')
                            contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                            createInfoWindowLocation(marker, contentString);
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                            if (subpoligonCoords.length > 0) {
                                var subpoligon = new google.maps.Polyline({
                                    path: subpoligonCoords,
                                    geodesic: true,
                                    strokeColor: previousColor,
                                    strokeOpacity: 1.0,
                                    strokeWeight: 7,
                                    icons: [{
                                        icon: iconsetngs,
                                        offset: '100%'
                                    }]
                                });
                                subpoligon.setMap(mapLocation);
                                animateCircle(subpoligon);
                                myTraceBackMarkers[tbcounter] = subpoligon;
                                tbcounter++;
                            }
                        }
                        else if (obj[i].State == "RED") {
                            if (first) {
                                contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png');
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocation(marker, contentString);
                                document.getElementById("rowIncidentName").text = obj[i].Username+obj[i].Id;
                                first = false;
                            }
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);
                        }
                        else {
                            if (secondfirst) {
                                currentSubLocation = obj[i].State;
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].LastLog });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                                myTraceBackMarkers[tbcounter] = marker;
                                tbcounter++;
                                previousColor = obj[i].Logs;
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocation(marker, contentString);
                                secondfirst = false;
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                subpoligonCoords.push(point);

                            }
                            else {
                                if (currentSubLocation == obj[i].State) {
                                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                    subpoligonCoords.push(point);
                                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].LastLog });
                                    marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                                    myTraceBackMarkers[tbcounter] = marker;
                                    tbcounter++;
                                }
                                else {
                                    if (subpoligonCoords.length > 0) {
                                        var subpoligon = new google.maps.Polyline({
                                            path: subpoligonCoords,
                                            geodesic: true,
                                            strokeColor: previousColor,
                                            strokeOpacity: 1.0,
                                            strokeWeight: 7,
                                            icons: [{
                                                icon: iconsetngs,
                                                offset: '100%'
                                            }]
                                        });
                                        subpoligon.setMap(mapLocation);
                                        animateCircle(subpoligon);
                                        myTraceBackMarkers[tbcounter] = subpoligon;
                                        tbcounter++;
                                    }
                                    subpoligonCoords = [];
                                    currentSubLocation = obj[i].State;
                                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].LastLog });
                                    marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                                    myTraceBackMarkers[tbcounter] = marker;
                                    tbcounter++;
                                    previousColor = obj[i].Logs;
                                    myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                    createInfoWindowLocation(marker, contentString);
                                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                    subpoligonCoords.push(point);
                                }
                            }
                        }
                    }
                    if (poligonCoords.length > 0) {
                        updatepoligon = new google.maps.Polyline({
                            path: poligonCoords,
                            geodesic: true,
                            strokeColor: '#FF0000',
                            strokeOpacity: 1.0,
                            strokeWeight: 2
                        });
                        updatepoligon.setMap(mapLocation);
                    }
                    if (tracepoligonCoords.length > 0) {
                        tracebackpoligon = new google.maps.Polyline({
                            path: tracepoligonCoords,
                            geodesic: true,
                            strokeColor: '#1b93c0',
                            strokeOpacity: 1.0,
                            strokeWeight: 7,
                            icons: [{
                                icon: iconsetngs,
                                offset: '100%'
                            }]
                        });
                        tracebackpoligon.setMap(mapLocation);
                        animateCircle(tracebackpoligon);
                    }
                    if (subpoligonCoords.length > 0) {
                        var subpoligon = new google.maps.Polyline({
                            path: subpoligonCoords,
                            geodesic: true,
                            strokeColor: previousColor,
                            strokeOpacity: 1.0,
                            strokeWeight: 7,
                            icons: [{
                                icon: iconsetngs,
                                offset: '100%'
                            }]
                        });
                        subpoligon.setMap(mapLocation);
                        animateCircle(subpoligon);
                        myTraceBackMarkers[tbcounter] = subpoligon;
                        tbcounter++;
                    }
                }
                catch (err)
                { 
                    //showAlert("Error 64: Problem occured while trying to get gps data. - " + err ); 
                }
            }

            function updateIncidentMarkerTicket(obj)
            {
                try
                {
                    var poligonCoords = [];
                    var tracepoligonCoords = [];
                    var first = true;
                    var first2 = true;
                    var currentSubLocation;
                    var secondfirst = true;
                    var previousColor = "";
                    var subpoligonCoords = [];
                    var first = true;
                    var previousMarker = document.getElementById("rowIncidentName").text;
                    for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                        if (myTraceBackMarkers[i] != null) {
                            myTraceBackMarkers[i].setMap(null);
                        }
                    }
                    if (typeof tracebackpoligon === 'undefined') {
                        // your code here.
                    }
                    else {
                        tracebackpoligon.setMap(null);
                    }
                    if(typeof previousMarker === 'undefined'){
                        // your code here.
                    }
                    else
                    {
                        myMarkersLocation[previousMarker].setMap(null);
                        if(typeof updatepoligon === 'undefined'){
                            // your code here.
                        }
                        else
                        {
                            updatepoligon.setMap(null);
                        }
                    }
                    var tbcounter = 0;
                
                    for (var i = 0; i < obj.length; i++) {
                        var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Uname + '</p></div><div class="help-block text-center"><a id="' + obj[i].Id + obj[i].Username + '" href="#" class="red-borders light-button red-color" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;,&apos;' + obj[i].Uname + '&apos;)" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color"><i class="fa fa-plus  red-color"></i>ADD</a></div>';
                        //'<div class="map-container inline-block"><div class="map-popup"><div class="help-block text-center pt-2x"><i class="fa fa-mobile"></i><p class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"><a id="' + obj[i].Id + obj[i].Username + '" href="#" class="red-borders light-button red-color" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div></div><div class="map-selector"></div></div> ';
                        var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        mapLocationTicket.setCenter(new google.maps.LatLng(obj[i].Lat, obj[i].Long));
                        mapLocationTicket.setZoom(9);
                        if (obj[i].State == "YELLOW") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocationTicket(marker, contentString);
                        }
                        else if (obj[i].State == "GREEN") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocationTicket(marker, contentString);
                        }
                        else if (obj[i].State == "BLUE") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocationTicket(marker, contentString);
                        }
                        else if (obj[i].State == "OFFUSER") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocationTicket(marker, contentString);
                        }
                        else if (obj[i].State == "OFFCLIENT") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocationTicket(marker, contentString);
                        }
                        else if (obj[i].State == "PURPLE") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                            contentString = '<div class="help-block text-center pt-2x"><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"></div>';
                            myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowLocationTicket(marker, contentString);
                            document.getElementById("rowIncidentName").text = obj[i].Username+obj[i].Id;  
                        }
                        else if (obj[i].State == "ENG") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username + "\n" + obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                            contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                            createInfoWindowLocationTicket(marker, contentString);
                        }
                        else if (obj[i].State == "COM") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username + "\n" + obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                            contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                            createInfoWindowLocationTicket(marker, contentString);
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                            if (subpoligonCoords.length > 0) {
                                var subpoligon = new google.maps.Polyline({
                                    path: subpoligonCoords,
                                    geodesic: true,
                                    strokeColor: previousColor,
                                    strokeOpacity: 1.0,
                                    strokeWeight: 7,
                                    icons: [{
                                        icon: iconsetngs,
                                        offset: '100%'
                                    }]
                                });
                                subpoligon.setMap(mapLocationTicket);
                                animateCircle(subpoligon);
                                myTraceBackMarkers[tbcounter] = subpoligon;
                                tbcounter++;
                            }
                        }
                        else if (obj[i].State == "RED") {
                            if (first) {
                                contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].Username });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocationTicket(marker, contentString);
                                document.getElementById("rowIncidentName").text = obj[i].Username+obj[i].Id;
                                first = false;
                            }
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);
                        }
                        else {
                            if (secondfirst) {
                                currentSubLocation = obj[i].State;
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].LastLog });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                                myTraceBackMarkers[tbcounter] = marker;
                                tbcounter++;
                                previousColor = obj[i].Logs;
                                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindowLocationTicket(marker, contentString);
                                secondfirst = false;
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                subpoligonCoords.push(point);

                            }
                            else {
                                if (currentSubLocation == obj[i].State) {
                                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                    subpoligonCoords.push(point);
                                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].LastLog });
                                    marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                                    myTraceBackMarkers[tbcounter] = marker;
                                    tbcounter++;
                                }
                                else {
                                    if (subpoligonCoords.length > 0) {
                                        var subpoligon = new google.maps.Polyline({
                                            path: subpoligonCoords,
                                            geodesic: true,
                                            strokeColor: previousColor,
                                            strokeOpacity: 1.0,
                                            strokeWeight: 7,
                                            icons: [{
                                                icon: iconsetngs,
                                                offset: '100%'
                                            }]
                                        });
                                        subpoligon.setMap(mapLocation);
                                        animateCircle(subpoligon);
                                        myTraceBackMarkers[tbcounter] = subpoligon;
                                        tbcounter++;
                                    }
                                    subpoligonCoords = [];
                                    currentSubLocation = obj[i].State;
                                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocationTicket, title: obj[i].LastLog });
                                    marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                                    myTraceBackMarkers[tbcounter] = marker;
                                    tbcounter++;
                                    previousColor = obj[i].Logs;
                                    myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                                    createInfoWindowLocationTicket(marker, contentString);
                                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                    subpoligonCoords.push(point);
                                }
                            }
                        }
                    }
                    if (poligonCoords.length > 0) {
                        updatepoligon = new google.maps.Polyline({
                            path: poligonCoords,
                            geodesic: true,
                            strokeColor: '#FF0000',
                            strokeOpacity: 1.0,
                            strokeWeight: 2
                        });
                        updatepoligon.setMap(mapLocationTicket);
                    }
                    if (tracepoligonCoords.length > 0) {
                        tracebackpoligon = new google.maps.Polyline({
                            path: tracepoligonCoords,
                            geodesic: true,
                            strokeColor: '#1b93c0',
                            strokeOpacity: 1.0,
                            strokeWeight: 7,
                            icons: [{
                                icon: iconsetngs, 
                                offset: '100%'
                            }]
                        });
                        tracebackpoligon.setMap(mapLocationTicket);
                        animateCircle(tracebackpoligon);
                    }
                    if (subpoligonCoords.length > 0) {
                        var subpoligon = new google.maps.Polyline({
                            path: subpoligonCoords,
                            geodesic: true,
                            strokeColor: previousColor,
                            strokeOpacity: 1.0,
                            strokeWeight: 7,
                            icons: [{
                                icon: iconsetngs, 
                                offset: '100%'
                            }]
                        });
                        subpoligon.setMap(mapLocationTicket);
                        animateCircle(subpoligon);
                        myTraceBackMarkers[tbcounter] = subpoligon;
                        tbcounter++;
                    }
                }
                catch (err)
                { 
                    //showAlert("Error 64: Problem occured while trying to get gps data. - " + err ); 
                }
            }
            function nextMapLiClick() {
                document.getElementById("nextLi").style.display = "block";
                document.getElementById("finishLi").style.display = "none";
                var name = document.getElementById("rowidChoice").text;
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getGPSDataUsers",
                    data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            var obj = jQuery.parseJSON(data.d)
                            updateIncidentMarker(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function setMapOnAll(obj,Markers) {
                try {
                    for (var i = 0; i < obj.length; i++) {
                        if (Markers[obj[i].Username+obj[i].Id] != null) {
                            Markers[obj[i].Username+obj[i].Id].setMap(null);
                        }
                    }
                }
                catch (err) {
                    //showAlert(err);
                }
            }
            var firstincidentpress = false;
            function rowchoice(name) {
                startRot();
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getGPSDataUsers",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            var obj = jQuery.parseJSON(data.d)
                            setMapOnAll(obj,myMarkersLocation);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
                var elSel = document.getElementById('liveListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {
                    if (elSel.options[i].value == name) {
                        elSel.remove(i);
                    }
                }
                $("#Inc┴"+name).removeClass("circle-point-red");
                $("#Inc┴"+name).addClass("circle-point-white");
                document.getElementById("rowtasktracebackUser").style.display = "none";
                document.getElementById("rowtracebackUser").style.display = "none";
                document.getElementById('checklistDIV').style.display = 'none';
                document.getElementById("cameraCheck").checked = false;
                document.getElementById("TaskCheck").checked = false;
                document.getElementById("finishLi").style.display = "none";
                document.getElementById("nextLi").style.display = "none";
                document.getElementById("rowidChoice").text = name;
                oldDivContainers();
                var retval = assignrowData(name);
                var splitRetval = retval.split('-');
                if (splitRetval.length > 0) {
                    retval = splitRetval[0];
                }
                if (retval == "Complete") {
                    document.getElementById("initialOptionsDiv").style.display = "none";
                    document.getElementById("handleOptionsDiv").style.display = "none";
                    document.getElementById("completedOptionsDiv").style.display = "block";
                    document.getElementById("dispatchOptionsDiv").style.display = "none";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("resolutionTextarea").style.display = "block";
                    document.getElementById("completedOptionsDiv2").style.display = "none";

                    document.getElementById("rejectOptionsDiv").style.display = "none";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "none";
                }
                else if (retval == "Dispatch") {
                    document.getElementById("initialOptionsDiv").style.display = "none";
                    document.getElementById("handleOptionsDiv").style.display = "none";
                    document.getElementById("completedOptionsDiv").style.display = "none";
                    document.getElementById("dispatchOptionsDiv").style.display = "block";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("resolutionTextarea").style.display = "none";
                    document.getElementById("completedOptionsDiv2").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "none";
                    document.getElementById("rejectOptionsDiv").style.display = "none";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "none";
                }
                else if (retval == "Pending") {
                    document.getElementById("initialOptionsDiv").style.display = "block";
                    document.getElementById("parkLi").style.display = "block";
                    document.getElementById("handleOptionsDiv").style.display = "none";
                    document.getElementById("completedOptionsDiv").style.display = "none";
                    document.getElementById("dispatchOptionsDiv").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "none";
                    document.getElementById("resolutionTextarea").style.display = "none";
                    document.getElementById("completedOptionsDiv2").style.display = "none";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("rejectOptionsDiv").style.display = "none";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "none";
                }
                else if (retval == "Release") {
                    document.getElementById("initialOptionsDiv").style.display = "none";
                    document.getElementById("handleOptionsDiv").style.display = "block";
                    document.getElementById("parkLi").style.display = "block";
                    document.getElementById("completedOptionsDiv").style.display = "none";
                    document.getElementById("dispatchOptionsDiv").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "none";
                    document.getElementById("resolutionTextarea").style.display = "none";
                    document.getElementById("completedOptionsDiv2").style.display = "none";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("rejectOptionsDiv").style.display = "none";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "none";
                }
                else if (retval == "Engage") {
                    document.getElementById("initialOptionsDiv").style.display = "none";
                    document.getElementById("handleOptionsDiv").style.display = "none";
                    document.getElementById("completedOptionsDiv").style.display = "none";
                    document.getElementById("dispatchOptionsDiv").style.display = "block";
                    document.getElementById("resolvedDiv").style.display = "none";
                    document.getElementById("resolutionTextarea").style.display = "none";
                    document.getElementById("completedOptionsDiv2").style.display = "none";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("rejectOptionsDiv").style.display = "none";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "none";
                }
                else if (retval == "Park") {
                    document.getElementById("initialOptionsDiv").style.display = "none";
                    document.getElementById("handleOptionsDiv").style.display = "block";
                    document.getElementById("parkLi").style.display = "none";
                    document.getElementById("completedOptionsDiv").style.display = "none";
                    document.getElementById("dispatchOptionsDiv").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "none";
                    document.getElementById("resolutionTextarea").style.display = "none";
                    document.getElementById("completedOptionsDiv2").style.display = "none";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("rejectOptionsDiv").style.display = "none";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "none";
                }
                else if (retval == "Reject") {
                    document.getElementById("resolutionTextarea").style.display = "none";
                    document.getElementById("completedOptionsDiv2").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "none";
                    document.getElementById("initialOptionsDiv").style.display = "none";
                    document.getElementById("parkLi").style.display = "none";
                    document.getElementById("resolveLi").style.display = "none";
                    document.getElementById("handleOptionsDiv").style.display = "none";
                    document.getElementById("completedOptionsDiv").style.display = "none";
                    document.getElementById("dispatchOptionsDiv").style.display = "none";
                    document.getElementById("rejectOptionsDiv").style.display = "block";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "none";
                }
                else if (retval == "Resolve") {
                    document.getElementById("resolutionTextarea").style.display = "none";
                    document.getElementById("completedOptionsDiv2").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "block";
                    document.getElementById("initialOptionsDiv").style.display = "none";
                    document.getElementById("parkLi").style.display = "none";
                    document.getElementById("handleOptionsDiv").style.display = "none";
                    document.getElementById("completedOptionsDiv").style.display = "none";
                    document.getElementById("dispatchOptionsDiv").style.display = "none";
                    document.getElementById("rejectOptionsDiv").style.display = "none";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "none";
                }
                else if (retval == "Escalated") {
                    document.getElementById("resolutionTextarea").style.display = "none";
                    document.getElementById("completedOptionsDiv2").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "none";
                    document.getElementById("initialOptionsDiv").style.display = "none";
                    document.getElementById("parkLi").style.display = "none";
                    document.getElementById("handleOptionsDiv").style.display = "none";
                    document.getElementById("completedOptionsDiv").style.display = "none";
                    document.getElementById("dispatchOptionsDiv").style.display = "none";
                    document.getElementById("rejectOptionsDiv").style.display = "none";
                    document.getElementById("rejectionTextarea").style.display = "none";
                    document.getElementById("escalateOptionsDiv").style.display = "none";
                    document.getElementById("escalateTextarea").style.display = "none";
                    document.getElementById("escalatedDIV").style.display = "block";
                
                }
                if (splitRetval.length > 0) {
                
                    if (splitRetval[1] == "Resolve") {
                        document.getElementById("resolutionTextarea").style.display = "none";
                        document.getElementById("completedOptionsDiv2").style.display = "none";
                        document.getElementById("resolvedDiv").style.display = "block";
                        document.getElementById("initialOptionsDiv").style.display = "none";
                        document.getElementById("parkLi").style.display = "none";
                        document.getElementById("handleOptionsDiv").style.display = "none";
                        document.getElementById("completedOptionsDiv").style.display = "none";
                        document.getElementById("dispatchOptionsDiv").style.display = "none";
                        document.getElementById("rejectOptionsDiv").style.display = "none";
                        document.getElementById("rejectionTextarea").style.display = "none";
                        document.getElementById("escalateOptionsDiv").style.display = "none";
                        document.getElementById("escalateTextarea").style.display = "none";
                        document.getElementById("escalatedDIV").style.display = "none";
                    }
                    else if (splitRetval[1] == "MyIncident") {
                        document.getElementById("resolutionTextarea").style.display = "none";
                        document.getElementById("completedOptionsDiv2").style.display = "none";
                        document.getElementById("resolvedDiv").style.display = "none";
                        document.getElementById("initialOptionsDiv").style.display = "none";
                        document.getElementById("parkLi").style.display = "none";
                        document.getElementById("handleOptionsDiv").style.display = "none";
                        document.getElementById("completedOptionsDiv").style.display = "none";
                        document.getElementById("dispatchOptionsDiv").style.display = "none";
                        document.getElementById("rejectOptionsDiv").style.display = "none";
                        document.getElementById("rejectionTextarea").style.display = "none";
                        document.getElementById("escalateOptionsDiv").style.display = "none";
                        document.getElementById("escalateTextarea").style.display = "none";
                        document.getElementById("escalatedDIV").style.display = "block";
                        if (retval == "Resolve") {
                            document.getElementById("escalatedDIV").style.display = "none";
                            document.getElementById("resolvedDiv").style.display = "block";
                        }
                        if (retval == "Complete") {
                            document.getElementById("resolutionTextarea").style.display = "block";
                            document.getElementById("completedOptionsDiv").style.display = "block";
                            document.getElementById("escalatedDIV").style.display = "none";
                            document.getElementById("compResolveLi").style.display = "none";
                    
                        }
                    }
                    else if (splitRetval[1] == "Hold") {
                        document.getElementById("resolutionTextarea").style.display = "none";
                        document.getElementById("completedOptionsDiv2").style.display = "none";
                        document.getElementById("resolvedDiv").style.display = "block";
                        document.getElementById("initialOptionsDiv").style.display = "none";
                        document.getElementById("parkLi").style.display = "none";
                        document.getElementById("handleOptionsDiv").style.display = "none";
                        document.getElementById("completedOptionsDiv").style.display = "none";
                        document.getElementById("dispatchOptionsDiv").style.display = "none";
                        document.getElementById("rejectOptionsDiv").style.display = "none";
                        document.getElementById("rejectionTextarea").style.display = "none";
                        document.getElementById("escalateOptionsDiv").style.display = "none";
                        document.getElementById("escalateTextarea").style.display = "none";
                        document.getElementById("escalatedDIV").style.display = "none";
                    }
                }
                incidentHistoryData(name);
                insertAttachmentIcons(name);
                insertAttachmentTabData(name);
                insertAttachmentData(name);
                dispatchAssignMapTab();
                cleardispatchList();
                infotabDefault();
            }
            function addnametoUserDispatchList(id,name,displayname) {
                var ul = document.getElementById("UsersToDispatchList");
                var li = document.createElement("li");
                li.setAttribute("id", "li-" + name);
                li.innerHTML = '<a href="#" class="capitalize-text">' + displayname + '<i class="fa fa-close"  onclick="usersliOnclickRemove(&apos;' + id + '&apos;,&apos;' + name + '&apos;)"></i></a>';
                ul.appendChild(li);
            }
            function dispatchUserchoiceTable(id, name,displayname) {
                var element = document.getElementById(id + name);
                var result = element.innerHTML.indexOf("ADDED");
                if (result < 0) {

                    var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                    if (exists == false) {
                        var myOption;
                        myOption = document.createElement("Option");
                        myOption.text = name; //Textbox's value
                        myOption.value = id; //Textbox's value
                        sendToListBox.add(myOption);
                        document.getElementById("<%=tbUserID.ClientID%>").value = document.getElementById("<%=tbUserID.ClientID%>").value + '-' + id;
                        document.getElementById("<%=tbUserName.ClientID%>").value = document.getElementById("<%=tbUserName.ClientID%>").value + '-' + name;
                        addnametoUserDispatchList(id,name,displayname);
                    }
                    element.style.color = "#3ebb64";
                    element.className = "green-color";
                    element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
                }
                else {
                    var elSel = document.getElementById('sendToListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) {
                        if (elSel.options[i].value == id) {
                            var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                            var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                            document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                            document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                            removenameFromDispatchList(elSel.options[i].text);
                            elSel.remove(i);
                        }
                    }
                
                    element.style.color = "#b2163b";
                    element.className = "red-color";
                    element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                }
            }
            function usersliOnclickRemove(id,name) {
                userchoice2(id,name)
                removeFromList(name);
                removenameFromDispatchList(name);
                var thismarker = myMarkersLocation[name];
                thismarker.infoWindowLocation.close();
            }
            function userchoice2(id, name) {
             
                var element = document.getElementById(id +name);
                var result = element.innerHTML.indexOf("ADDED");
                if (result < 0) {

                }
                else {
                    var elSel = document.getElementById('sendToListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) {
                        if (elSel.options[i].value == id) {
                            removenameFromDispatchList(elSel.options[i].text);
                            elSel.remove(i);
                        }
                    }

                    element.style.color = "#b2163b";
                    element.className = "red-color";
                    element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                }
            }
            function removenameFromDispatchList(name) {
                var element = document.getElementById("li-" + name);
                element.parentNode.removeChild(element);
            }
            function removeFromList(name) {
                var elSel = document.getElementById('sendToListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {
                    if (elSel.options[i].text == name) {
                        var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                        var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                        document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                        document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                        elSel.remove(i);
                    }
                }
            }
            function userdispatchChoice(id, name,displayname) {
                var element = document.getElementById(id + name);
                var result = element.innerHTML.indexOf("ADDED");
                if (result < 0) {

                    var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                    if (exists == false) {
                        var myOption;
                        myOption = document.createElement("Option");
                        myOption.text = name; //Textbox's value
                        myOption.value = id; //Textbox's value
                        sendToListBox.add(myOption);
                        document.getElementById("<%=tbUserID.ClientID%>").value = document.getElementById("<%=tbUserID.ClientID%>").value + '-' + id;
                        document.getElementById("<%=tbUserName.ClientID%>").value = document.getElementById("<%=tbUserName.ClientID%>").value + '-' + name;
                        addnametoUserDispatchList(id,name,displayname);
                    }
                    element.style.color = "#3ebb64";
                    element.className = "green-borders light-button green-color";
                    element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
                }
                else {
                    var elSel = document.getElementById('sendToListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) {
                        if (elSel.options[i].value == id) {
                            var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                            var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                            document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                            document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                            removenameFromDispatchList(elSel.options[i].text);
                            elSel.remove(i);
                        }
                    }
                
                    element.style.color = "#b2163b";
                    element.className = "red-borders light-button red-color";
                    element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                }
            }
            function assignUserTableData() {

                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getAssignUserTableData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                jQuery("#assignUsersTable tbody").append(data.d[i]);

                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function infotabDefault() {
                var el = document.getElementById('activity-tab');
                if (el) {
                    el.className = 'tab-pane fade ';
                }
                var el3 = document.getElementById('info-tab');
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
                var el4 = document.getElementById('attachments-tab');
                if (el4) {
                    el4.className = 'tab-pane fade';
                }
                var el2 = document.getElementById('liInfo');
                if (el2) {
                    el2.className = 'active';
                }
                var el5 = document.getElementById('liActi');
                if (el5) {
                    el5.className = ' ';
                }
                var el6 = document.getElementById('liAtta');
                if (el6) {
                    el6.className = ' ';
                }
            }
            function cleardispatchList() {
                try {
                    var elSel = document.getElementById('sendToListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) { 
                        var element = document.getElementById("li-" + elSel.options[i].text);
                        var element2 = document.getElementById(elSel.options[i].value + "-" + elSel.options[i].text);
                        if (element2) {
                            element2.style.color = "#b2163b";
                            element2.className = "red-color";
                            element2.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                        }
                        if (element) {
                            element.parentNode.removeChild(element);
                            elSel.remove(i);
                        }
                    }
                }
                catch (err) {
                    showAlert("Error 70: Problem occured while trying to clear dispatch list. - " + err );
                }
            }
            function dispatchAssignMapTab() {
                var el = document.getElementById('incidentAssign-user-tab');
                if (el) {
                    el.className = 'tab-pane fade ';
                }
                var el3 = document.getElementById('location-tab');
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
                var el4 = document.getElementById('incidentNext-user-tab');
                if (el4) {
                    el4.className = 'tab-pane fade';
                }
            }
            function insertAttachmentIcons(id) {
                jQuery('#divAttachment div').html('');
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getAttachmentDataIcons",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            document.getElementById("divAttachment").innerHTML = data.d;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function insertAttachmentTabData(id)
            {
                jQuery('#attachments-info-tab div').html('');
            
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getAttachmentDataTab",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            document.getElementById("attachments-info-tab").innerHTML = data.d;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function insertAttachmentData(id) {
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getAttachmentData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                if (data.d[i].indexOf("video") >= 0) {
                                    var div = document.createElement('div');
                                    div.className = 'tab-pane fade';
                                    div.innerHTML = data.d[i];
                                    div.id = 'video-' + (i + 1) + '-tab';
                                    document.getElementById('divAttachmentHolder').appendChild(div);
                                    divArray[i] = 'video-' + (i + 1) + '-tab';
                                }
                                else {
                                    var div = document.createElement('div');
                                    div.className = 'tab-pane fade';
                                    div.align = 'center';
                                    div.style.height = '420px';
                                    div.innerHTML = data.d[i];
                                    div.id = 'image-' + (i + 1) + '-tab';
                                    document.getElementById('divAttachmentHolder').appendChild(div);
                                    divArray[i] = 'image-' + (i + 1) + '-tab';
                                }
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function oldDivContainers() {
                try
                {
                    for (var i = 0; i < divArray.length; i++) {
                        var el = document.getElementById(divArray[i]);
                        el.parentNode.removeChild(el);
                    }
                    divArray = new Array();
                }
                catch(ex)
                {
                    //alert(ex);
                }
            }
            function incidentHistoryData(id)
            {
                jQuery('#divIncidentHistoryActivity div').html('');
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getEventHistoryData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                var div = document.createElement('div');

                                div.className = 'row activity-block-container';

                                div.innerHTML = data.d[i];

                                document.getElementById('divIncidentHistoryActivity').appendChild(div);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function resolveClick() {
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "none";
                document.getElementById("completedOptionsDiv").style.display = "none";
                document.getElementById("dispatchOptionsDiv").style.display = "none";
                document.getElementById("rejectOptionsDiv").style.display = "none";
                document.getElementById("rejectionTextarea").style.display = "none";

                document.getElementById("completedOptionsDiv2").style.display = "block";
                document.getElementById("resolutionTextarea").style.display = "block";
            }
            function escalateOptionClick() {
                document.getElementById("resolutionTextarea").style.display = "none";
                document.getElementById("completedOptionsDiv2").style.display = "none";
                document.getElementById("resolvedDiv").style.display = "none";
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("parkLi").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "none";
                document.getElementById("completedOptionsDiv").style.display = "none";
                document.getElementById("dispatchOptionsDiv").style.display = "none";
                document.getElementById("rejectOptionsDiv").style.display = "none";
                document.getElementById("rejectionTextarea").style.display = "none";
                document.getElementById("escalateTextarea").style.display = "block";
                document.getElementById("escalateOptionsDiv").style.display = "block";
            }
            function escalateClick() {
                var id = document.getElementById("rowidChoice").text;
                var notes = document.getElementById("escalateTextarea").value;
                var isErr = false;
                if (!isEmptyOrSpaces(document.getElementById('escalateTextarea').value)) {
                    if (isSpecialChar(document.getElementById('escalateTextarea').value)) {
                        isErr = true;
                        showAlert("Kindly remove special character from text area");
                    }
                }
                if (!isErr) {
                    $.ajax({
                        type: "POST",
                        url: "Default.aspx/escalateIncident",
                        data: "{'id':'" + id + "','ins':'" + notes + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d.length == 0) {
                                showAlert("Error 55: Problem face when trying to escalate incident.");
                            }
                            else if (data.d == "LOGOUT") {
                                document.getElementById('<%= logoutbtn.ClientID %>').click();
                            }
                            else {
                                jQuery('#viewDocument1').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Incident has been escalated!";
                                jQuery('#successfulDispatch').modal('show');
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                        }
                    });
                }
            }
            function redispatch() {
                var id = document.getElementById("rowidChoice").text;
                var ins = document.getElementById("rejectionTextarea").value;
                var isErr = false;
                if (!isEmptyOrSpaces(document.getElementById('rejectionTextarea').value)) {
                    if (isSpecialChar(document.getElementById('rejectionTextarea').value)) {
                        isErr = true;
                        showAlert("Kindly remove special character from text area");
                    }
                }
                if (!isErr) {
                    $.ajax({
                        type: "POST",
                        url: "Default.aspx/rejectReDispatch",
                        data: "{'id':'" + id + "','ins':'" + ins + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d.length == 0) {
                                showAlert("Error 55: Problem face when trying to reject and dispatching incident.");
                            }
                            else if (data.d[0] == "LOGOUT") {
                                document.getElementById('<%= logoutbtn.ClientID %>').click();
                            }
                            else {
                                var selected = [];
                                for (var i = 1; i < data.d.length; i++) {
                                    chat.server.sendIncidenttoUser(btoa(data.d[i]), "ARL┴" + document.getElementById("incidentNameHeader").innerHTML + "┴" + document.getElementById("descriptionSpan").innerHTML + "┴0┴0", data.d[i + 1], id);

                                    selected.push(data.d[i]);
                                    i++;
                                }
                                if (selected.length > 0)
                                    sendpushMultipleNotification(selected, data.d[0]);



                                jQuery('#viewDocument1').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = data.d[0] + " has been redispatched!";
                                jQuery('#successfulDispatch').modal('show');
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                        }
                    });
                }
            }
            function handledClick() {
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "block";
                document.getElementById("completedOptionsDiv").style.display = "none";
                document.getElementById("dispatchOptionsDiv").style.display = "none";
                document.getElementById("rejectOptionsDiv").style.display = "none";
                document.getElementById("rejectionTextarea").style.display = "none";

                document.getElementById("resolutionTextarea").style.display = "none";
                document.getElementById("completedOptionsDiv2").style.display = "none";
            }
            function nextLiClick() {
                document.getElementById("nextLi").style.display = "block";
                document.getElementById("finishLi").style.display = "none";
            }
            function finishLiClick() {
                document.getElementById("finishLi").style.display = "block";
                document.getElementById("nextLi").style.display = "none";
            }
            function updateCustomEventViewed(id){
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/UpdateCustomEventView",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function assignrowData(id) {
                var output = "";
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getTableRowData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            document.getElementById('escalateionSpanDiv').style.display = 'none';
                            document.getElementById("usernameSpan").innerHTML = data.d[0];
                            document.getElementById("timeSpan").innerHTML = data.d[1];
                            document.getElementById("typeSpan").innerHTML = data.d[2];
                    
                            var ret = data.d[3];
                            var splitRetval = ret.split('-');
                            if (splitRetval.length > 0) {
                                ret = splitRetval[0];
                            }
                            document.getElementById("statusSpan").innerHTML = ret;
                            document.getElementById("locSpan").innerHTML = data.d[4];
                            document.getElementById("receivedBySpan").innerHTML = data.d[5];
                            document.getElementById("phonenumberSpan").innerHTML = data.d[6];
                            document.getElementById("emailSpan").innerHTML = data.d[7];
                            document.getElementById("descriptionSpan").innerHTML = data.d[8];
                            document.getElementById("instructionSpan").innerHTML = data.d[9];
                            document.getElementById("incidentNameHeader").innerHTML = data.d[10];
                            var el = document.getElementById('headerImageClass');
                            if (el) {
                                el.className = data.d[11];
                            }
                            document.getElementById("rowLongitude").text = data.d[12];
                            document.getElementById("rowLatitude").text = data.d[13];
                            output = data.d[3];
                            if (data.d[14] == "TASK") {
                                document.getElementById('checklistDIV').style.display = 'block';
                                getTasklistItems(id);

                                if (data.d[3] == "Escalated" || data.d[3] == "Reject" || data.d[3] == "Resolve" || data.d[3] == "Resolve-MyIncident") {
                                    document.getElementById('escalateionSpanDiv').style.display = 'block';
                                    document.getElementById("escalationHead").innerHTML = data.d[3] + " Notes";

                                    if (data.d[3] == "Resolve-MyIncident")
                                        document.getElementById("escalationHead").innerHTML = "Resolve Notes";

                                    document.getElementById("escalatedSpan").innerHTML = data.d[15];

                                }
                                document.getElementById('resolveLi').style.display = data.d[16];
                                document.getElementById('disResolveLi').style.display = data.d[16];
                                document.getElementById('compResolveLi').style.display = data.d[16];
                                document.getElementById('compResolveLi2').style.display = data.d[16];
                                document.getElementById("notesSpan").innerHTML = data.d[17];


                                //document.getElementById('rejResolveLi').style.display = data.d[16];
                            }
                            else {
                                if (data.d[3] == "Escalated" || data.d[3] == "Reject" || data.d[3] == "Resolve" || data.d[3] == "Resolve-MyIncident") {
                                    document.getElementById('escalateionSpanDiv').style.display = 'block';
                                    document.getElementById("escalationHead").innerHTML = data.d[3]+" Notes";
                                    document.getElementById("escalatedSpan").innerHTML = data.d[14];

                                    if (data.d[3] == "Resolve-MyIncident")
                                        document.getElementById("escalationHead").innerHTML = "Resolve Notes";

                                }
                                document.getElementById('resolveLi').style.display = data.d[15];
                                document.getElementById('disResolveLi').style.display = data.d[15];
                                document.getElementById('compResolveLi').style.display = data.d[15];
                                document.getElementById('compResolveLi2').style.display = data.d[15];
                                document.getElementById("notesSpan").innerHTML = data.d[16];
                                //document.getElementById('rejResolveLi').style.display = data.d[15];
                            }
                            updateCustomEventViewed(id);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
                return output;
            } 
            function getTasklistItems(id) {
                document.getElementById("taskItemsList").innerHTML = "";
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/getTaskListData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            document.getElementById("taskItemsnameSpan").innerHTML = data.d[0];
                            for (var i = 1; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("taskItemsList");
                                var li = document.createElement("li");
                                li.innerHTML = '<a href="#taskDocument"  data-toggle="modal"   class="capitalize-text" onclick="showTaskDocument(&apos;' + res[1] + '&apos;)">' + res[0] + '</a>';
                                //li.appendChild(document.createTextNode(data.d[i]));
                                ul.appendChild(li);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            var firstpresstask = false;
            var updatepoligon;
            var myTraceBackMarkers = new Array();
            Object.size = function (obj) {
                var size = 0, key;
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) size++;
                }
                return size;
            };
            function updateTaskMarker(obj)
            {
                try
                {
                    var poligonCoords = [];
                    var first = true;
                    var tbcounter = 0;
                    for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                        if (myTraceBackMarkers[i] != null) {
                            myTraceBackMarkers[i].setMap(null);
                        }
                    }
                    if (typeof updatepoligon === 'undefined') {
                        // your code here.
                    }
                    else {
                        updatepoligon.setMap(null);
                    }
                    if(typeof myMarkersTasksLocation["Pending"] === 'undefined'){
                        // your code here.
                    }
                    else
                    {  
                        myMarkersTasksLocation["Pending"].setMap(null);
                    }
                    if(typeof myMarkersTasksLocation["Completed"] === 'undefined'){
                        // your code here.
                    }
                    else
                    {  
                        myMarkersTasksLocation["Completed"].setMap(null);
                    }
                    if (typeof myMarkersTasksLocation["Rejected"] === 'undefined') {
                        // your code here.
                    }
                    else {
                        myMarkersTasksLocation["Rejected"].setMap(null);
                    }
                    if (typeof myMarkersTasksLocation["Accepted"] === 'undefined') {
                        // your code here.
                    }
                    else {
                        myMarkersTasksLocation["Accepted"].setMap(null);
                    }
                    if(typeof myMarkersTasksLocation["InProgress"] === 'undefined'){
                        // your code here.
                    }
                    else
                    {  
                        myMarkersTasksLocation["InProgress"].setMap(null);
                    }
                    if(typeof myMarkersTasksLocation["Cancelled"] === 'undefined'){
                        // your code here.
                    }
                    else
                    {  
                        myMarkersTasksLocation["Cancelled"].setMap(null);
                    }
                    for (var i = 0; i < obj.length; i++) {
                        var contentString = '<div id="content">' + obj[i].Username + '<br/></div>';

                        var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                        if (obj[i].Username == "Pending") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                            myMarkersTasksLocation[obj[i].Username] = marker;
                            createInfoWindowTaskLocation(marker, contentString);
                        }
                        else if (obj[i].Username == "InProgress") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                            myMarkersTasksLocation[obj[i].Username] = marker;
                            createInfoWindowTaskLocation(marker, contentString);
                        }
                        else if (obj[i].Username == "Completed") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                            myMarkersTasksLocation[obj[i].Username] = marker;
                            createInfoWindowTaskLocation(marker, contentString);
                        }
                        else if (obj[i].Username == "Accepted") {
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                            myMarkersTasksLocation[obj[i].Username] = marker;
                            createInfoWindowTaskLocation(marker, contentString);
                        }
                        else if (obj[i].State == "RED") {
                            if (first) {
                                first = false;
                                document.getElementById('previousTaskUser').text = obj[i].Username;
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                poligonCoords.push(point);
                            }
                            else {
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                poligonCoords.push(point);
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].LastLog });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                                myTraceBackMarkers[tbcounter] = marker;
                                tbcounter++;
                            }
                        }
                        else {
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                            myMarkersTasksLocation[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindowTaskLocation(marker, contentString);
                        }
                    }
                    if (poligonCoords.length > 0) {
                        updatepoligon = new google.maps.Polyline({
                            path: poligonCoords,
                            geodesic: true,
                            strokeColor: '#1b93c0',
                            strokeOpacity: 1.0,
                            strokeWeight: 7,
                            icons: [{
                                icon: iconsetngs,
                                offset: '100%'
                            }]
                        });
                        updatepoligon.setMap(mapTaskLocation);
                        animateCircle(updatepoligon);
                    }
                }
                catch(err)
                {
                    //showAlert("Error 64: Problem occured while trying to get gps data. - " + err );
                }
            }
            function showTaskDocument(id) {
                jQuery('#viewDocument1').modal('hide');

                document.getElementById('rowChoiceTasks').value = id;
                document.getElementById("taskinitialOptionsDiv").style.display = "block";
                document.getElementById("taskhandleOptionsDiv").style.display = "none";
                document.getElementById("taskrejectOptionsDiv").style.display = "none";
                var el = document.getElementById('tasklocation-tab');
                if (el) {
                    el.className = 'tab-pane fade active in';
                }
                var el2 = document.getElementById('taskrejection-tab');
                if (el2) {
                    el2.className = 'tab-pane fade';
                }

                oldDivContainers();

                var retVal = assignrowDataTask(id);
                if (retVal == "Completed")
                    TaskIsCompleted();

                taskHistoryData(id);
                taskinsertAttachmentIcons(id);
                taskinsertAttachmentTabData(id);
                taskinsertAttachmentData(id);
                getChecklistItems(id);
                getChecklistItemsNotes(id);
                getCanvasNotes(id);
                infotabDefault();
            }
            function getChecklistItemsNotes(id) {
                document.getElementById("checklistItemsListNotes").innerHTML = "";
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/getChecklistNotesData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            if (data.d.length > 0) {
                                document.getElementById("pchecklistItemsListNotes").style.display = "block";
                                for (var i = 0; i < data.d.length; i++) {
                                    var res = data.d[i].split("|");
                                    var ul = document.getElementById("checklistItemsListNotes");
                                    var li = document.createElement("li");
                                    li.innerHTML = '<i style="margin-left:-15px;" class="fa fa-square-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                                    ul.appendChild(li);
                                    if (res.length > 1) {
                                        if (res[1] != '') {
                                            var li2 = document.createElement("li");
                                            li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                            ul.appendChild(li2);
                                        }
                                    }
                                }
                            }
                            else {
                                document.getElementById("pchecklistItemsListNotes").style.display = "none";
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });

            }
            function getCanvasNotes(id) {
                document.getElementById("canvasItemsListNotes").innerHTML = "";
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/getCanvasNotesData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            if (data.d.length > 0) {
                                document.getElementById("pCanvasNotes").style.display = "block";
                                for (var i = 0; i < data.d.length; i++) {
                                    var res = data.d[i].split("|");
                                    var ul = document.getElementById("canvasItemsListNotes");
                                    var li = document.createElement("li");
                                    li.innerHTML = '<i style="margin-left:-15px;" ></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                                    ul.appendChild(li);
                                    if (res.length > 1) {
                                        if (res[1] != '') {
                                            var li2 = document.createElement("li");
                                            li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                            ul.appendChild(li2);
                                        }
                                    }
                                }
                            }
                            else {
                                document.getElementById("pCanvasNotes").style.display = "none";
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });

            }
            function TaskIsCompleted() {
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "block";
                document.getElementById("rejectOptionsDiv").style.display = "none";
            }
            function taskinsertAttachmentData(id) {
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/taskgetAttachmentData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                if (data.d[i].indexOf("video") >= 0) {
                                    var div = document.createElement('div');
                                    div.className = 'tab-pane fade';
                                    div.innerHTML = data.d[i];
                                    div.id = 'video-' + (i + 1) + '-tab';
                                    document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                    divArray[i] = 'video-' + (i + 1) + '-tab';
                                }
                                else {
                                    var div = document.createElement('div');
                                    div.className = 'tab-pane fade';
                                    div.align = 'center';
                                    div.style.height = '420px';
                                    div.innerHTML = data.d[i];
                                    div.id = 'image-' + (i + 1) + '-tab';
                                    document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                    divArray[i] = 'image-' + (i + 1) + '-tab';
                                }
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function taskinsertAttachmentTabData(id) {
                jQuery('#taskattachments-info-tab div').html('');

                $.ajax({
                    type: "POST",
                    url: "Default.aspx/taskgetAttachmentDataTab",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            document.getElementById("taskattachments-info-tab").innerHTML = data.d;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function taskinsertAttachmentIcons(id) {
                jQuery('#taskdivAttachment div').html('');
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/taskgetAttachmentDataIcons",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            document.getElementById("taskdivAttachment").innerHTML = data.d;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function taskHistoryData(id) {
                jQuery('#taskdivIncidentHistoryActivity div').html('');
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/getTaskHistoryData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                var div = document.createElement('div');

                                div.className = 'row activity-block-container';

                                div.innerHTML = data.d[i];

                                document.getElementById('taskdivIncidentHistoryActivity').appendChild(div);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function getChecklistItems(id) {
                document.getElementById("checklistItemsList").innerHTML = "";
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/getChecklistData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else{
                            if (data.d[0] == "False") {
                                var el = document.getElementById('checklistnamespanFA');
                                if (el) {
                                    el.className = "fa fa-square-o";
                                }
                            }
                            else {
                                var el = document.getElementById('checklistnamespanFA');
                                if (el) {
                                    el.className = "fa fa-check-square-o";
                                }
                            }
                            for (var i = 1; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("checklistItemsList");
                                var li = document.createElement("li");
                                var marginLeft = '';
                                if (res[2] == 'True') {
                                    marginLeft = 'style = "margin-left:-15px;"';
                                }
                                if (res[1] == "Checked")
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                                else
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';

                                ul.appendChild(li);
                                if (res[2] == '3') {
                                    var li2 = document.createElement("li");
                                    li2.innerHTML = '<a style="margin-left:5px;" href="#"  class="capitalize-text" >Notes: ' + res[3] + '</a>';
                                    ul.appendChild(li2);
                                }
                                //li.appendChild(document.createTextNode(data.d[i]));

                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });

            }
            function assignrowDataTask(id) {
                var output = "";
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/getTableRowDataTask",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            document.getElementById("taskusernameSpan").innerHTML = data.d[0];
                            document.getElementById("tasktimeSpan").innerHTML = data.d[1];
                            document.getElementById("tasktypeSpan").innerHTML = data.d[2];
                            document.getElementById("taskstatusSpan").innerHTML = data.d[3];
                            document.getElementById("tasklocSpan").innerHTML = data.d[4];
                            document.getElementById("taskdescriptionSpan").innerHTML = data.d[8];
                            document.getElementById("taskinstructionSpan").innerHTML = data.d[9];
                            document.getElementById("taskincidentNameHeader").innerHTML = data.d[10];
                            document.getElementById("assignedTimeSpan").innerHTML = data.d[11];
                            document.getElementById("checklistNotesSpan").innerHTML = data.d[12];
                            document.getElementById("checklistnameSpan").innerHTML = data.d[14];

                            var el = document.getElementById('headerImageClass');
                            if (el) {
                                el.className = data.d[13];
                            }
                            output = data.d[3];

                            document.getElementById('ttypeSpan').innerHTML = data.d[15];

                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
                return output;
            }
            function infotabDefault() {
                var el = document.getElementById('activity-tab');
                if (el) {
                    el.className = 'tab-pane fade ';
                }
                var el3 = document.getElementById('info-tab');
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
                var el4 = document.getElementById('attachments-tab');
                if (el4) {
                    el4.className = 'tab-pane fade';
                }
                var el2 = document.getElementById('taskliInfo');
                if (el2) {
                    el2.className = 'active';
                }
                var el5 = document.getElementById('taskliActi');
                if (el5) {
                    el5.className = ' ';
                }
                var el6 = document.getElementById('taskliAtta');
                if (el6) {
                    el6.className = ' ';
                }
            }
            function addrowtoTable() {
                jQuery("#mobilehoteventTable tbody").empty();
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getTableData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",                
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                jQuery("#mobilehoteventTable tbody").append(data.d[i]);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            function userchoice(id, name) {
                var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                if (exists == false) {
                    var myOption;
                    myOption = document.createElement("Option");
                    myOption.text = name; //Textbox's value
                    myOption.value = id; //Textbox's value
                    sendToListBox.add(myOption);
                    document.getElementById("<%=tbUserID.ClientID%>").value = document.getElementById("<%=tbUserID.ClientID%>").value + '-' + id;
                    document.getElementById("<%=tbUserName.ClientID%>").value = document.getElementById("<%=tbUserName.ClientID%>").value + '-' + name;
                }
            }
            function addreminders() {

                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getReminders",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                var div = document.createElement('div');

                                div.className = 'help-block mb-4x';

                                div.innerHTML = data.d[i];

                                document.getElementById('divReminder').appendChild(div);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });

            }
            function recentActivity() {

                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getRecentActivity",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            for (var i = 0; i < data.d.length; i++) {
                                var div = document.createElement('div');

                                div.className = 'row';

                                div.innerHTML = data.d[i];

                                document.getElementById('divrecentActivity').appendChild(div);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }     
 
            function getonlineuserspercentage() {
                var output = "";
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getusershealtcheck",
                    data: "{'id':'0','onlinecount':'" + <%=onlinecount%> + "','offlinecount':'" + <%=offlinecount%> + "','idlecount':'" + <%=idlecount%> + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d.onlineuserspercentage[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {

                            // getonlineuserspercentage
                            document.getElementById('divOnlineCount').style.width = data.d.onlineuserspercentage[0];
                            document.getElementById('divOfflineCount').style.width = data.d.onlineuserspercentage[1];
                            document.getElementById('divIdleCount').style.width = data.d.onlineuserspercentage[2];

                            // recentActivity
                            for (var i = 0; i < data.d.recentActivity.length; i++) {
                                var div = document.createElement('div');

                                div.className = 'row';

                                div.innerHTML = data.d.recentActivity[i];

                                document.getElementById('divrecentActivity').appendChild(div);
                            }

                            //addreminders				
                            for (var i = 0; i < data.d.reminders.length; i++) {
                                var div = document.createElement('div');

                                div.className = 'help-block mb-4x';

                                div.innerHTML = data.d.reminders[i];

                                document.getElementById('divReminder').appendChild(div);
                            }

                            //addrowtoTable
                            for (var i = 0; i < data.d.rowtoTable.length; i++) {
                                jQuery("#mobilehoteventTable tbody").append(data.d.rowtoTable[i]);
                            }
                            // assignUserTableData
                            for (var i = 0; i < data.d.assignUserTable.length; i++) {
                                jQuery("#assignUsersTable tbody").append(data.d.assignUserTable[i]);
                            }
                            //topOffencesChart			
                            var lineCtx = document.getElementById('chartjs-linez').getContext('2d'),
                                lineData = {
                                    labels: [data.d.topOffences[0], data.d.topOffences[1], data.d.topOffences[2], data.d.topOffences[3], data.d.topOffences[4]],
                                    datasets: [
                                        {
                                            label: 'User1',
                                            fillColor: 'transparent',
                                            strokeColor: '#1b93c0',
                                            pointColor: '#1b93c0',
                                            pointStrokeColor: '#ffffff',
                                            pointHighlightFill: '#ffffff',
                                            pointHighlightStroke: '#1b93c0',
                                            data: [0, data.d.topOffences[5], data.d.topOffences[10], data.d.topOffences[15], data.d.topOffences[20]]
                                        },
                                        {
                                            label: 'User2',
                                            fillColor: 'transparent',
                                            strokeColor: '#3ebb64',
                                            pointColor: '#3ebb64',
                                            pointStrokeColor: '#ffffff',
                                            pointHighlightFill: '#ffffff',
                                            pointHighlightStroke: '#3ebb64',
                                            data: [0, data.d.topOffences[6], data.d.topOffences[11], data.d.topOffences[16], data.d.topOffences[21]]
                                        },
                                        {
                                            label: 'User3',
                                            fillColor: 'transparent',
                                            strokeColor: '#f2c400',
                                            pointColor: '#f2c400',
                                            pointStrokeColor: '#ffffff',
                                            pointHighlightFill: '#ffffff',
                                            pointHighlightStroke: '#f2c400',
                                            data: [0, data.d.topOffences[7], data.d.topOffences[12], data.d.topOffences[17], data.d.topOffences[22]]
                                        },
                                        {
                                            label: 'User4',
                                            fillColor: 'transparent',
                                            strokeColor: '#f44e4b',
                                            pointColor: '#f44e4b',
                                            pointStrokeColor: '#ffffff',
                                            pointHighlightFill: '#ffffff',
                                            pointHighlightStroke: '#f44e4b',
                                            data: [0, data.d.topOffences[8], data.d.topOffences[13], data.d.topOffences[18], data.d.topOffences[23]]
                                        },
                                        {
                                            label: 'User5',
                                            fillColor: 'transparent',
                                            strokeColor: '#b2163b',
                                            pointColor: '#b2163b',
                                            pointStrokeColor: '#ffffff',
                                            pointHighlightFill: '#ffffff',
                                            pointHighlightStroke: '#b2163b',
                                            data: [0, data.d.topOffences[9], data.d.topOffences[14], data.d.topOffences[19], data.d.topOffences[24]]
                                        }
                                    ]
                                },
                                chartjsLine = new Chart(lineCtx).Line(lineData, {
                                    scaleBeginAtZero: true,
                                    scaleShowVerticalLines: false,
                                    responsive: false,
                                });

                            //getActivityReport		

                            var bar4Ctx = document.getElementById('chartjs-barActivity').getContext('2d'),
                                bar4Data = {
                                    labels: ['24h', '18h', '12h', '6h'],
                                    datasets: [
                                        { label: 'Clothes', fillColor: 'rgba(237, 85, 101, 1)', highlightFill: 'rgba(237, 85, 101, 0.8)', data: [data.d.ActivityReport[0], data.d.ActivityReport[2], data.d.ActivityReport[4], data.d.ActivityReport[6]] },
                                        { label: 'Trousers', fillColor: 'rgba(93, 156, 236, 1)', highlightFill: 'rgba(93, 156, 236, 0.8)', data: [data.d.ActivityReport[1], data.d.ActivityReport[3], data.d.ActivityReport[5], data.d.ActivityReport[7]] }
                                    ]
                                },
                                chartjsBar4 = new Chart(bar4Ctx).Bar(bar4Data, {
                                    scaleShowVerticalLines: false,
                                    barShowStroke: false,
                                    barValueSpacing: 16,
                                    responsive: false,
                                });

                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
                return output;
            }
            function isUser(name) {
                var output = "";
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/isUser",
                    data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            output = data.d;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
                return output;
            }
            function createInfoWindow(marker, popupContent) {
                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.setContent(popupContent);
                    infoWindow.open(map, this);
                });
            }
            function getLocation(obj) {

                locationAllowed = true;
                //setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat,sourceLon);//(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
                var poligonCoords = [];
                var subpoligonCoords = [];
                var first = true;
                var secondfirst = true;
                var previousColor = "";
                var polyCounter = 0;
					
			
                for (var i = 0; i < obj.length; i++) {
                    
                    if(obj[i].Logs == "User")
                    {
                        var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-user pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#user-profile-tab"  data-toggle="tab" onclick="assignUserProfileData(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                    }
                    else
                    {
                        var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#viewDocument1"  data-toggle="modal" onclick="rowchoice(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                    }
                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    if (obj[i].State == "YELLOW") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                    else if (obj[i].State == "GREEN") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                    else if (obj[i].State == "BLUE") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                    else if (obj[i].State == "OFFUSER") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                    else if (obj[i].State == "OFFCLIENT") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                        myMarkers[obj[i].Username] = marker;
                        createInfoWindow(marker, contentString);
                    }
                    else if (obj[i].State == "PURPLE") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                        myMarkers[obj[i].Username+obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                    }
                    else {
                        if (secondfirst) {
                            currentSubLocation = obj[i].State;
                            var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                            if (obj[i].Logs == '#17ff33') {
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');
                            }
                            else {
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                            }
                            previousColor = obj[i].Logs;
                            myMarkers[obj[i].Username+obj[i].Id] = marker;
                            createInfoWindow(marker, contentString);
                            secondfirst = false;
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                        else {
                            if (currentSubLocation == obj[i].State) {
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                subpoligonCoords.push(point);
                            }
                            else {
                                if (subpoligonCoords.length > 0) {
                                    var subpoligon = new google.maps.Polyline({
                                        path: subpoligonCoords,
                                        geodesic: true,
                                        strokeColor: previousColor,
                                        strokeOpacity: 1.0,
                                        strokeWeight: 2
                                    });
                                    subpoligon.setMap(map);
                                    polylinesArray[polyCounter]= subpoligon;
                                    polyCounter++;
                                }
                                subpoligonCoords = [];
                                currentSubLocation = obj[i].State;
                                var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                                if (obj[i].Logs == '#17ff33') {
                                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');
                                }
                                else {
                                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                                }
                                previousColor = obj[i].Logs;
                                myMarkers[obj[i].Username+obj[i].Id] = marker;
                                createInfoWindow(marker, contentString);
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                subpoligonCoords.push(point);
                            }
                        }
                    }
                }

                if (subpoligonCoords.length > 0) {
                    var subpoligon = new google.maps.Polyline({
                        path: subpoligonCoords,
                        geodesic: true,
                        strokeColor: '#0000FF',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });
                    subpoligon.setMap(map);
                    polylinesArray[polyCounter]= subpoligon;
                    polyCounter++;
                }
            }
            function getLocationNoOnline() {
                locationAllowed = true;
                setTimeout(function () {
                    google.maps.visualRefresh = true;
                    var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                    // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                    var mapOptions = {
                        zoom: 8,
                        center: Liverpool,
                        mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                    };

                    // This makes the div with id "map_canvas" a google map
                    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

                }, 1000);
            }
            function Initialize() {
                jQuery.ajax({
                    type: "POST",
                    url: "Default.aspx/getGPSData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d == "]") {
                            getLocationNoOnline();
                        }
                        else {
                            var obj = jQuery.parseJSON(data.d)
                            getLocation(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
        

        </script>
        <!-- ============================================
    MAIN CONTENT SECTION
    =============================================== -->
        <section class="content-wrapper" role="main" >
            <div class="content" >
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2 col-sm-3">
                                    <h3 class="panel-title"><span class="hidden-xs">Message Boards</span></h3>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <div class="panel-control" style="display:<%=incidentDisplay%>">
                                        <ul class="nav nav-tabs nav-main">
                                            <li  id="dash1Li" style="display:<%=incidentDisplay%>"><a href="Default.aspx"  onclick="showLoader();">Incidents</a>
                                            </li>
                                            <li id="dash2Li" style="display:<%=taskDisplay%>"><a href="TaskDash.aspx" onclick="showLoader();">Tasking</a>
                                            </li>
                                            <li id="dash3Li" style="display:<%=otDisplay%>"><a href="OthersDash.aspx"  onclick="showLoader();">Ticketing</a>
                                            </li>
                                               <li id="dash4Li" class="active" ><a href="MessageBoard.aspx"  onclick="showLoader();">Message Board</a>
                                            </li> 
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-2">
                                    <div role="group" class="pull-right">
                                         <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" id="dttimenow"></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                         <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <%--<%=siteName%>
                                        <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignManagerUserProfileData(<%=loggedInId%>)'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                        <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>--%>
                                    </div>
                                </div>								
                            </div>
                        </div>
                        <div class="panel-body">
							<div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x" style="margin-bottom:0px;">
  
                                    <div class="col-md-9" style="padding-left:0px;"> 
                                        <div id="flashalertDIV" class="row">
                                            <p class="red-color"><i class="fa fa-warning pr-1x"></i><b>FLASH ALERTS</b></p> 
                                             <div class="outer2" style="border-top:1px solid #d2d2d2;">
    <div class="inner2" id="inner2">
 
                                          </div>
                                           </div>
                                               </div>
                                        <div id="storiesDIV" class="row" style="padding-top:10px;">
                                            <p class="red-color" ><b>STORIES & ACKNOWLEDGEMENTS</b></p>
                                <div class="outer" style="border-top:1px solid #d2d2d2;">
    <div class="inner" id="inner">
 
        </div>
                                    </div>
                                      
                                            </div>
                                        <div class="row" style="border-top:1px solid #d2d2d2;padding-top:10px;">
                                            <div class="col-md-4">
                                  
 
                                                                        
                                            </div>
                                            <div class="col-md-8">
                                                 <p class="red-color" ><b>VIDEO OF THE WEEK</b></p>
                                               <%-- <video id="messageboardVideo" style="margin-top:5px;" width="100%" height="315px" controls="controls" autoplay="autoplay" loop="loop"><source id="messageboardVideoSrc" src=""></video>--%>
                                           <iframe  width="100%" height="315px"
src="<%=mbvideosource%>">
</iframe>
                                                 </div>
                                        </div>

                                   	 </div>							
                                    <div class="col-md-3" style="border-left:1px solid #d2d2d2" >
                                        <div style="border-bottom:1px solid #d2d2d2" class="row text-center">
                                                  <p class="red-color"><i class="fa fa-cloud pr-1x"></i><b>LOCAL WEATHER</b></p>
 							            </div>
                                        <div  class="row" id="weatherfeedDIV">
     
                                        </div>
 							            <div style="border-bottom:1px solid #d2d2d2" class="row text-center">
                                                  <p class="red-color"><b>UAE NEWS FEED</b></p>
 							            </div>
                                        <div  class="row" id="newsfeedDIV">
     
                                        </div>
                                    </div>
                                </div>	
                                   <div class="row mb-4x" id="smileydiv" style="padding-top:50px;border-top:1px solid #d2d2d2;border-bottom:1px solid #d2d2d2;<%=mbsurveyquestiondisplay%>">
                                       <div class="row" style="padding-bottom:30px;">
                                       <div class="col-md-12"><p style="font-size:40px;" class="red-color"><b><%=mbsurveyquestion%></b></p></div>
                                       </div>
                                       <div class="row" style="border-left:1px solid #d2d2d2;border-right:1px solid #d2d2d2;border-top:1px solid #d2d2d2;padding-top:20px;padding-bottom:20px;">
                                           <div class="col-md-1">&nbsp</div>
                                       <div class="col-md-3">
                                           <img onmousedown="this.style.opacity=0.2" onmouseup="this.style.opacity=1" onclick="savehappyCount('red')" class="text-center" style="width:182px;height:182px;" src="https://testportalcdn.azureedge.net/Images/frown.png"/>
                                       </div>
                                        <div class="col-md-1" style="border-left:1px solid #d2d2d2;height: 180px;"></div>
                                       <div class="col-md-3" >
                                             <img onmousedown="this.style.opacity=0.2" onmouseup="this.style.opacity=1" onclick="savehappyCount('yellow')" class="text-center" style="width:182px;height:182px;"  src="https://testportalcdn.azureedge.net/Images/meh.png"/>
                                       </div>
                                        <div class="col-md-1" style="border-left:1px solid #d2d2d2;height: 180px;"></div>
                                       <div class="col-md-3" >
                                               <img onmousedown="this.style.opacity=0.2" onmouseup="this.style.opacity=1" onclick="savehappyCount('green')" class="text-center"  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                                       </div>
                                           </div>
                                   </div>	
                            </div>
							</div>
                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div id="changePWDIV" class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>    
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                                          <label class="select select-o">
                                                                                                   <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
                                                          </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>     
                                                 <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                     <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">Notification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Location</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                                         <div class="row" style="display:none;">
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                        
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                            <!-- /tab-content -->
                        </div>
                        <!-- /panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.content-body -->
            </div>
            <!-- /.content -->
            <div aria-hidden="true" aria-labelledby="newReminder" role="dialog" tabindex="-1" id="newReminder" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">NEW REMINDER</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Reminder Name" id="tbReminderName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
                                          <div class="input-group input-group-in">
                                            <input id="tbReminderDate" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">
                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                    </div>
								</div>
                                                                <div class="row">
									<div class="col-md-6">
                                      <p class="font-bold red-color no-vmargin" >From:</p>
                                        <label class="select select-o">
                                         <select id="fromReminderDate2" onchange="fromReminderDateChange()" >
                                         </select>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                      <p class="font-bold red-color no-vmargin" >To:</p>
                                        <label class="select select-o">
                                             <select id="toReminderDate" >
                                             </select>
                                         <//label>
                                    </div>
								</div>
		                           <div class="row">
                              <div class="col-md-4">
                                 <p>Reminder Color</p>
                              </div>
                              <div class="col-md-8">
                                 <div class="nice-radio filled-color blue-radio nice-radio-inline inline-block no-vmargin">
                                    <input type="radio" name="niceRadioAlt" id="cbRemBlue" checked="checked" class="hidden radio-o"> 
                                    <label for="cbRemBlue" class="vertical-align-top"></label>
                                 </div>
                                 <div class="nice-radio filled-color green-radio nice-radio-inline inline-block">
                                    <input type="radio" name="niceRadioAlt" id="cbRemGreen" class="hidden radio-o">
                                    <label for="cbRemGreen" class="vertical-align-top"></label>
                                 </div>
                                 <div class="nice-radio filled-color yellow-radio nice-radio-inline inline-block">
                                    <input type="radio" name="niceRadioAlt" id="cbRemYellow" class="hidden radio-o">
                                    <label for="cbRemYellow" class="vertical-align-top"></label>
                                 </div>
                                 <div class="nice-radio filled-color lightred-radio nice-radio-inline inline-block">
                                    <input type="radio" name="niceRadioAlt" id="cbRemRed" class="hidden radio-o">
                                    <label for="cbRemRed" class="vertical-align-top"></label>
                                 </div>
                              </div>
                           </div>
								<div class="row">
									<div class="col-md-12">
										<textarea placeholder="Notes" id="tbReminderNotes" class="form-control" rows="3"></textarea>
									</div>
								</div>								
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
									<li><a href="#" data-dismiss="modal" class="capitalize-text">CANCEL</a>
									</li>
                                    <li class="active"><a href="#" class="capitalize-text" onclick="newReminderInsert()">CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="viewDocument1" role="dialog" tabindex="-1" id="viewDocument1" class="modal fade videoModal" style="display: none;">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <div class="row">
                        <div class="col-md-11">
                            <span class="circle-point-container pull-left mt-2x mr-1x"><span id="headerImageClass" class="circle-point circle-point-orange"></span></span>
                            <h4 class="modal-title capitalize-text" id="incidentNameHeader"></h4>
                        </div>
                        <div class="col-md-1">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>                      
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <p>Created by: <span id="usernameSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Time: <span id="timeSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Type: <span id="typeSpan"></span></p>
                        </div>                          
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <p>Status: <span id="statusSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Location: <span id="locSpan"></span></p>
                        </div>                          
                      </div>                      
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" id="demo3-tabs">
                                            <li class="active" id="liInfo"><a href="#info-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="liActi"><a href="#activity-tab" onclick="tracebackOn('incident')" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="liAtta"><a href="#attachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>                                           
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tab-pane fade active in" id="info-tab">
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Received by:</b> <span class="red-color" id="receivedBySpan"></span></p>                                              
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Phone number:</b> <span class="red-color" id="phonenumberSpan"></span></p>                                                
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Email:</b> <span class="red-color" id="emailSpan"></span></p>                                             
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Description:</b></p>
                                                    <p id="descriptionSpan"></p>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Instruction:</b></p>
                                                    <p id="instructionSpan"></p>
                                                </div>
                                            </div>  
                                                <div class="row">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Notes:</b></p>
                                                    <p id="notesSpan"></p>
                                                </div>
                                            </div>  
                                            <div id="escalateionSpanDiv" class="row" style="display:none">  
                                                <div class="col-md-12">
                                                    <p id="escalationHead" class="red-color"><b>Escalation Notes:</b></p>
                                                    <p id="escalatedSpan"></p>
                                                </div>
                                            </div>  
                                                <div class="row" id="checklistDIV" style="display:none;">
                                                  <div class="col-md-12">
													<p class="red-color"><b>Dispatched Task : </b><b id="taskItemsnameSpan"></b></p>
                                                      <div class="row horizontal-navigation">
                                                        <div class="panel-control">
                                                                <ul class="nav nav-tabs" id="taskItemsList">

                                                                </ul>
                                                            <!-- /.nav -->
                                                        </div>
                                                    </div>
												</div>
                                                </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                        </div>
                                        </div>
                                        <div class="tab-pane fade" id="activity-tab">
                                                    <div id="divIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                                    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                      
                                        </div>
                                        <div class="tab-pane fade" id="attachments-tab" onclick="startRot()">        
                                             <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="attachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                            </div>                      
                                        </div>                                      
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8" id="divAttachmentHolder">
                                <div class="row">
                                    <div class="horizontal-navigation">
                                        <div class="panel-control">
                                                <ul class="nav nav-tabs" id="UsersToDispatchList">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade active in" id="location-tab">
                                    <div id="map_canvasIncidentLocation" style="width:100%;height:378px;"></div>
                                    <div id="divAttachment" onclick="startRot()" class="overlapping-map-image">
                                    </div>
<%--                                    <div class="overlapping-map-image" style="top:30px;left:150px;height:5px;">
 
                                    </div>--%>
                                </div>
                                <div class="tab-pane fade " id="incidentAssign-user-tab">
                                        <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MY USERS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="assignUsersTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">USER
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
<%--                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">LOCATION<i class="fa fa-sort ml-2x"></i>
                                                                </th>--%>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">ACTION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="tab-pane fade " id="incidentNext-user-tab">
                                     <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:378px;">
                                    <p class="red-color"><b>Dispatch Options:</b></p>
                                    <div class="row" style="display:none;">
                                       <div class="col-md-4">
                                           <div class='row nice-checkbox'>
                                               <input onclick="showViewCamDuration()" id='cameraCheck' type='checkbox' name='niceCheck'>
                                               <label for='cameraCheck'>Camera</label>
                                           </div>

                                       </div>
                                       <div class="col-md-8">
                                           <div class="row mb-1x" id="cameraselectDiv">
                                         <label class="select select-o">
                                            <select id="cameraSelect" runat="server">
                                            </select>
                                         </label>
                                               </div>

                                    </div>
                                    </div>
                                                                                                            <div id="camViewDurationDiv" style="display:none;" class="row">
                                       <div class="col-md-4">
                                           Access Duration
                                       </div>
                                       <div class="col-md-8">
                                     <div  style="margin-top:0px;margin-bottom:7px;" class="row horizontal-navigation">
                                                <a style="font-size:14px;" href="#" onclick="viewdurationselect(15)"><i id="camviewfilter1" class="fa fa-square-o"></i>15 MIN</a>
                                                |<a style="font-size:14px;" href="#" onclick="viewdurationselect(30)"><i id="camviewfilter2" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:14px;" href="#" onclick="viewdurationselect(45)"><i id="camviewfilter3" class="fa fa-square-o"></i>45 MIN</a>
									            |<a style="font-size:14px;" href="#" onclick="viewdurationselect(60)"><i id="camviewfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                |<a style="font-size:14px;" href="#" onclick="viewdurationselect(120)"><i id="camviewfilter5" class="fa fa-square-o"></i>2 HOUR</a>
                                </div>
                                    </div>
                                    </div>

                                                                        <div class="row" style="display:<%=taskDisplay%>;">
                                       <div class="col-md-4">
                                                                                      <div class='row nice-checkbox' >
                                               <input id='TaskCheck' type='checkbox' name='niceCheck'>
                                               <label for='TaskCheck'>Task</label>
                                           </div>
                                       </div>
                                       <div class="col-md-8">
                                                                                      <div class="row">
                                        <label class="select select-o" >
                                            <select id="selectTaskTemplate" runat="server">
                                            </select>
                                         </label>
                                               </div>
                                    </div>
                                    </div>
                                                                    <div class="row"  style="display:none;"> 

                                               <div class="text-center">Attachment Photo</div>

                                    </div>
                                <div class="row">
                                                      <div style="height:50px;" enctype="multipart/form-data" id="dz-upload" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                                        <div class="dz-message">
                                                          <h1>BROWSE</h1>
                                                        </div>
                                                      </div>
                                         </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="Instructions" id="selectinstructionTextarea" class="form-control" rows="6"></textarea>
                                    </div>
                                </div>       
                                                                                                                                 <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                        </div>                       
                                </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row" id="rowtracebackUser" style="display:none;">
                        <div class="col-md-4">
                            </div>
                               <div class="col-md-8" style="text-align:left;margin-top:-8px;">
 <a style="font-size:16px;" href="#" id="incidentplaypausefilter" onclick="incidentplaypauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1,'incident')"><i id="filter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5,'incident')"><i id="filter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30,'incident')"><i id="filter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60,'incident')"><i id="filter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                                    User:
							        <div style="margin-top:10px;" class="panel-control" id="tracebackUserFilter">
							        </div>
                            </div>
                        </div>
                        <div class="row" id="pdfloadingAccept" style="padding-bottom:10px;display:none;">
                            <div class="col-md-7">

                            </div>
                            <div class="col-md-5" style="margin-bottom:-50px">
                                <p class="red-color text-center" style="font-size:12px"><b>GENERATING REPORT</b></p>
                                                                            <div id="myProgress">
                                              <div id="myBar"></div>
                                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                 <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs" id="initialOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">UNHANDLE</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="handledClick()">HANDLE</a>
                                    </li>
                                    <li <%=escalatedView%>><a href="#" class="capitalize-text" onclick="escalateOptionClick()">ESCALATE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="escalateOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>
                                    <li <%=escalatedView%>><a href="#" class="capitalize-text" onclick="escalateClick()">ESCALATE</a>
                                    </li>
                                    <li>

                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="dispatchOptionsDiv" style="display:none;">
                                    <li><a href="#"   onclick="IncidentStateChange('Release')">RELEASE</a>
                                    </li>
                                    <li id="disResolveLi"><a href="#" class="capitalize-text" onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="handleOptionsDiv" style="display:none;">
                                    <li id="parkLi"><a href="#"  onclick="IncidentStateChange('Park')" >PARK</a>
                                    </li>
                                    <li style="display:<%=dispatchDisplay%>;"><a  class="capitalize-text" data-target="#incidentAssign-user-tab" data-toggle="tab" onclick="nextLiClick()">DISPATCH FROM USERS</a>
                                    </li>
                                    <li style="display:<%=dispatchDisplay%>;"> <a  class="capitalize-text" data-target="#location-tab" data-toggle="tab" onclick="nextMapLiClick()">DISPATCH FROM MAP</a>
                                    </li>
                                    <li id="nextLi" style="display:none"><a  class="capitalize-text" data-target="#incidentNext-user-tab" onclick="finishLiClick()" data-toggle="tab" >NEXT</a>
                                    </li>
                                    <li id="finishLi" style="display:none"><a  class="capitalize-text" onclick="dispatchIncident()">DISPATCH</a>
                                    </li>
                                    <li id="resolveLi"><a onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="completedOptionsDiv2" style="display:none;">
                                    <li><a href="#"   onclick="IncidentStateChange('Reject')">REJECT</a>
                                    </li>
                                    <li id="compResolveLi2"><a href="#" class="capitalize-text"  onclick="IncidentStateChange('Resolve')">RESOLVE</a>
                                    </li>
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="completedOptionsDiv" style="display:none;">
                                    <li><a href="#"    onclick="IncidentStateChange('Reject')">REJECT</a>
                                    </li>
                                    <li id="compResolveLi"><a href="#" class="capitalize-text"  onclick="IncidentStateChange('Resolve')">RESOLVE</a>
                                    </li>
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="rejectOptionsDiv" style="display:none;">
                                    <li style="display:none;"><a href="#"  onclick="IncidentStateChange('Reject')">SAVE</a>
                                    </li>
                                    <li><a href="#" onclick="handledClick()">DISPATCH</a>
                                    </li>
                                    <li><a href="#" onclick="redispatch()">RE-DISPATCH</a>
                                    </li>
                                    <li style="display:none;" id="rejResolveLi"><a href="#" class="capitalize-text" onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="resolvedDiv" style="display:none;">
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="escalatedDIV" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>
                                    <li><a href="#" onclick="handledClick()">DISPATCH</a>
                                    </li>
                                </ul> 
                                <textarea placeholder="Rejection Notes" style="display:none;" id="rejectionTextarea" class="form-control" rows="3"></textarea>
                                <textarea placeholder="Notes" style="display:none;" id="resolutionTextarea" class="form-control" rows="3"></textarea>
                                 <textarea placeholder="Escalation Notes" style="display:none;" id="escalateTextarea" class="form-control" rows="3"></textarea>
                                <!-- /.nav -->
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            </div>   
            <div aria-hidden="true" aria-labelledby="errorMessageModal" role="dialog" tabindex="-1" id="errorMessageModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
<%--					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					</div>--%>
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h4 class="red-color text-center" id="errormsgTXT">Please provide all needed information</h4>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CLOSE</a>
									    </li>		
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li class="active"><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="taskDocument" role="dialog" tabindex="-1" id="taskDocument" class="modal fade videoModal" style="display: none;z-index:100000">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="taskheaderImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="taskincidentNameHeader"></h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" data-target="#viewDocument1" onclick="rowchoice(document.getElementById('rowidChoice').text)" data-toggle="modal" class="close" type="button" ><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
				 <div class="row">
						<div class="col-md-3">
							<p>Status: <span id="taskstatusSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created by: <span id="taskusernameSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Assigned to: <span id="tasktypeSpan"></span></p>
						</div>		
                                   <div class="col-md-3">
							<p>Task Type: <span id="ttypeSpan"></span></p>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Location: <span id="tasklocSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created on: <span id="tasktimeSpan"></span></p>
						</div>	
                         <div class="col-md-3">
							<p>Assigned on: <span id="assignedTimeSpan"></span></p>
						</div>
                          <div class="col-md-3">
							<p id="incidentItemsList"></p>
						</div>									
					  </div>			
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li class="active" id="taskliInfo"><a href="#taskinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="taskliNotes"><a href="#notes-tab" data-toggle="tab" class="capitalize-text">NOTES</a>
                                            </li>
                                            <li id="taskliActi"><a href="#taskactivity-tab" data-toggle="tab" onclick="tracebackOn('task')" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="taskliAtta"><a href="#taskattachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
										<div class="tab-pane fade active in" id="taskinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Description:</b></p>
													<p id="taskdescriptionSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Checklist Name : </b><b id="checklistnameSpan"></b><i id="checklistnamespanFA" style="margin-left:5px;" class="fa fa-check-square-o"></i></p>
                                                    <ul id="checklistItemsList" style="list-style-type: none;">

                                                    </ul>
												</div>
                                                <div class="col-md-12" id="verificationDIV" style="display:none">
                                                    <p class="red-color"><b>Verification:</b></p>
                                                       <div class="row">
                                                         <div class="col-md-6">
                                                            <div class="help-block text-center">
                                                               <img id="veriResultIMG2" style="height:135px;width:135px;border-radius:8px;" src="">
                                                               <h4 class="gray-color">Comparison Image</h4>
                                                               </div>                                    
                                                         </div>
                                                         <div class="col-md-6">
                                                            <div class="help-block text-center">
                                                               <img id="veriSentIMG2" style="height:135px;width:135px;border-radius:8px;" src="">
                                                               <h4 class="gray-color">Sent Image</h4>
                                                               </div>
                                                         </div>                                 
                                                      </div>
                                                    <div class="text-center">
                                                    <a href="#" style="text-decoration:underline" onclick="assignVerifierRequestData()">View All</a>
					                                </div>
                                                </div>
											</div>		
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
                                        <div class="tab-pane fade" id="notes-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
													<p id="taskinstructionSpan"></p>
												</div>
											</div>	
											<div class="row">
												<div class="col-md-12">
													<p class="red-color"><b>Checklist Notes:</b></p>
													<p id="checklistNotesSpan"></p>
												</div>
											</div>	
                                            <div id="pchecklistItemsListNotes" class="row">
												<div class="col-md-12">
													<p class="red-color"><b>Unchecked Items:</b></p>
													 <ul id="checklistItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
                                                                                            <div id="pCanvasNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Canvas Notes:</b></p>
													 <ul id="canvasItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
										<div class="tab-pane fade" id="taskactivity-tab">
                                                    <div id="taskdivIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div>
										<div class="tab-pane fade" id="taskattachments-tab">	
                                              <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="taskattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>								
										</div>
							
									</div>
								</div>
							</div>
							<div class="col-md-7" id="taskdivAttachmentHolder">
								<div class="tab-pane fade active in" id="tasklocation-tab">
                                    <div id="taskmap_canvasIncidentLocation" style="width:100%;height:440px;"></div>
									<div id="taskdivAttachment" class="overlapping-map-image">
									</div>
								</div>		
                                <div class="tab-pane fade" id="taskrejection-tab">
	                                      <p class="red-color text-center"><b>Rejection Notes:</b></p>
									        <div class="col-md-12" style="height:410px;">
										        <textarea placeholder="Rejection Notes" id="taskrejectionTextarea" class="form-control" rows="12"></textarea>
									        </div>
                                </div>									
							</div>
						</div>
					</div>
					<div class="modal-footer">
                       <div class="row" id="rowtasktracebackUser" style="display:none;">
                        <div class="col-md-5">
                            </div>
                               <div class="col-md-7" style="text-align:left;margin-top:-8px;">
                                    <a style="font-size:16px;" href="#" id="playpausefilter" onclick="playpauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1,'task')"><i id="taskfilter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5,'task')"><i id="taskfilter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30,'task')"><i id="taskfilter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60,'task')"><i id="taskfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                            </div>
                        </div>
						<div class="row horizontal-navigation">
							<div class="panel-control">
                                <ul class="nav nav-tabs" id="taskinitialOptionsDiv">
									<li><a href="#" data-dismiss="modal" data-target="#viewDocument1" onclick="rowchoice(document.getElementById('rowidChoice').text)"  data-toggle="modal">CLOSE</a>
									</li>
								</ul>
								<ul class="nav nav-tabs" id="taskhandleOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal" data-target="#viewDocument1" onclick="rowchoice(document.getElementById('rowidChoice').text)"  data-toggle="modal">CLOSE</a>
									</li>		
                                    <li><a href="#">ACCEPT</a>
									</li>
									<li><a href="#" data-target="#rejection-tab" data-toggle="tab"  onclick="rejectionSelect();">REJECT</a>
									</li>							
								</ul>
                                <ul class="nav nav-tabs" id="taskrejectOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal" data-target="#viewDocument1" onclick="rowchoice(document.getElementById('rowidChoice').text)"  data-toggle="modal">CLOSE</a>
									</li>
                                    <li><a href="#">ASSIGN</a>
									</li>
									<li><a href="#">REASSIGN</a>
									</li>
									<li><a href="#">SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="cameraDocument" role="dialog" tabindex="-1" id="cameraDocument" class="modal fade videoModal" style="display: none;z-index:100000">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
                      <div class="modal-header">
                          					  <div class="row">
						<div class="col-md-11">
							<h4 class="modal-title capitalize-text" id="carNameSpan"></h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="camClear()" ><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
                      </div>
					<div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
		                        <iframe id="camFrame"  style="width: 100%;height: 700px;" frameborder="0"></iframe>
					        </div>
                            <div style="display:none;" class="col-md-3">
                                	<div class="row">
									<div class="col-md-12">
                                        <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p style="font-size:20px" class="red-color"><b>Temperature:</b></p>
													<p style="font-size:20px" id="carTemperaturespan"></p>
												</div>
											</div>	
                                                                                        <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p style="font-size:20px" class="red-color"><b>RPM:</b></p>
													<p style="font-size:20px" id="carRPMspan"></p>                                            
                                                </div>
                                            </div>  
                                                                                        <div class="row mb-2x">
                                                <div class="col-md-12">   
                                                    <p style="font-size:20px" class="red-color"><b>Speed:</b></p>
													<p style="font-size:20px" id="carSpeedspan"></p>                                               
                                                </div>
                                            </div>  	
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
                                        </div>
                                        </div>
                            </div>
                        </div>
                    </div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="verificationDocument" role="dialog" tabindex="-1" id="verificationDocument" class="modal fade videoModal" style="display: none;">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <div class="row">
                           <div class="col-md-11">
                              <h4 class="modal-title capitalize-text">VERIFICATION</h4>
                           </div>
                           <div class="col-md-1">
                              <button aria-hidden="true" data-dismiss="modal" class="close"  type="button"><i class="icon_close fa-lg"></i></button>
                           </div>
                        </div>

                     </div>
                     <div class="modal-body pt-4x">
                        <div class="row border-bottom pb-4x selected-user container-match-user-1">
                           <div class="col-md-15 text-center border-right match-user match-user-1" onclick="moveImageToComparison('1')"> 
                              <div class="help-block">
                                 <img id="veriResult1IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font green-color" id="resultPercent1">
                                    
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-2" onclick="moveImageToComparison('2')"> 
                              <div class="help-block">
                                 <img id="veriResult2IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font yellow-color" id="resultPercent2">

                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-3" onclick="moveImageToComparison('3')"> 
                              <div class="help-block">
                                 <img id="veriResult3IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent3">
                                
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-4" onclick="moveImageToComparison('4')"> 
                              <div class="help-block">
                                 <img id="veriResult4IMG"  style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent4">
                                   
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center match-user match-user-5" onclick="moveImageToComparison('5')"> 
                              <div class="help-block">
                                 <img id="veriResult5IMG" style="height:61px;width:61px;"  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent5">
                                    
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>                                                                                                            
                        </div>
                        <div class="row">
                           <div class="col-md-17 border-right mt-2x mb-2x">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="help-block text-center">
                                       <img id="veriResultIMG" style="height:135px;width:135px;border-radius:8px;" src="">
                                       <h4 class="gray-color">Comparison Image</h4>
                                       <a href="#enlargeImageModal" class="red-color" data-dismiss="modal" data-toggle="modal" onclick="enlargeDisplayImage('Comparison')">Enlarge Image</a>
                                    </div>                                    
                                 </div>
                                 <div class="col-md-6">
                                    <div class="help-block text-center">
                                       <img id="veriSentIMG" style="height:135px;width:135px;border-radius:8px;" src="">
                                       <h4 class="gray-color">Sent Image</h4>
                                       <a href="#enlargeImageModal" class="red-color" data-dismiss="modal" data-toggle="modal" onclick="enlargeDisplayImage('Sent')">Enlarge Image</a>
                                    </div>
                                 </div>                                 
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                     <div id="map_verLocation" style="width:100%;height:174px;"></div>
                                 </div>                                 
                              </div>                              
                           </div>
                           <div class="col-md-18">
                              <div class="row">
                                 <div class="col-md-12">
                                    <h4 class="main-header" id="verifyHeaderSpan" style="color:#b2163b;"></h4>
                                 </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Created by:
                                       </p>
                                       <span class="red-color" id="verifyUserSpan">
                                             
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Time:
                                       </p>
                                       <span class="red-color" id="verifyTimeSpan">
                                       </span>                                       
                                    </div>
                              </div>
   
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Type:
                                       </p>
                                       <span class="red-color" id="verifyTypeSpan">
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Location:
                                       </p>
                                       <span class="red-color" id="verifyLocSpan">
                                       </span>                                       
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Case ID:
                                       </p>
                                       <span class="red-color" id="verifyCaseSpan">
                                             
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             PID
                                       </p>
                                       <span class="red-color" id="verifyPIDSpan">
                                       </span>                                       
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Birth Year:
                                       </p>
                                       <span class="red-color" id="verifyBYearSpan">
                                       </span>                                       
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Gender
                                       </p>
                                       <span class="red-color" id="verifyGenderSpan">
                                       </span>
                                    </div>                                    
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Ethnicity: 
                                       </p>
                                       <span class="red-color" id="verifyEthniSpan">
                                       </span>                                       
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           List:
                                       </p>
                                       <span class="red-color" id="verifyListTypeSpan">
                                       </span>
                                    </div>                                    
                              </div>
                              <div class="row">

                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Reason:
                                       </p>
                                       <span class="red-color" id="verifyReasonSpan">
                                       </span>                                       
                                    </div>

                                    <div class="col-md-6">
                                       <p class="inline-block">

                                       </p>
                                       <span class="red-color">

                                       </span>                                       
                                    </div>                                    
                              </div>  


                              <div class="row mt-4x">
                                 <div class="col-md-6">
                                    <p class="no-vmargin">
                                       Match Score
                                    </p>
                                 </div>

                                 <div class="col-md-6 text-right">
                                    <span class="green-color" id="resultPercentMain">
                                          
                                    </span>
                                 </div>
                                 
                              </div>
                              <div class="row">
                                    <div class="col-md-12">
                                                <div class="progress rounded-edge full-proogress-bar">
                                                   <div id="progressbarMainDisplay" class="progress-bar progress-bar-green rounded-edge full-proogress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                                   </div>
                                                </div>                                        
                                    </div>                                    
                              </div>                                                                                                                        
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs" style="display:none;" id="verifyDIV">
                                 <li class="active" style="display:none;"><a href="#" onclick="runVerification()">VERIFY</a>
                                 </li>
                                 <li class="active"><a href="#" data-dismiss="modal">CLOSE</a>
                                 </li>
                              </ul>
                              <ul class="nav nav-tabs" id="nextVerifyDIV" style="display:none;">
                                 <li class="active"><a href="#" onclick="runVerification()">VERIFY</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="enlargeImageModal" role="dialog" tabindex="-1" id="enlargeImageModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
                       <div class="modal-header">
                            <div class="row"></div>
                        </div>
					<div class="modal-body" >
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                            <img id="enlargeIMG" class="user-profile-image" src=""> 
                                </div>
                            <div class="col-md-1"></div>
                        </div>
					</div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs text-center">
                                    <li><a href="#" data-dismiss="modal" onclick="jQuery('#verificationDocument').modal('show');" class="capitalize-text">Close</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="deleteUserModal" role="dialog" tabindex="-1" id="deleteUserModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                         <div class="row">
                            <h4 class="text-center" id="todeleteUser"></h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: This action cannot be undone!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a data-dismiss="modal" onclick="deleteUser()" ><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="greenreminderboxs" role="dialog" tabindex="-1" id="greenreminderboxs" class="modal fade reminder-boxs greenModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="greenRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color " id="greenRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                          
                           </div>
                           <div class="row border-top">
                           </div>
                         
                           <div class="row mb-2x">
                              <p id="greenRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                                                          <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="bluereminderboxs" role="dialog" tabindex="-1" id="bluereminderboxs" class="modal fade reminder-boxs blueModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="blueRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="blueRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                            
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="blueRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                                                          <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="redreminderboxs" role="dialog" tabindex="-1" id="redreminderboxs" class="modal fade reminder-boxs redModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="redRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="redRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="redRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                                                          <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="yellowreminderboxs" role="dialog" tabindex="-1" id="yellowreminderboxs" class="modal fade reminder-boxs yellowModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="yellowRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="yellowRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                            
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="yellowRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                                                          <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="ticketDocument" role="dialog" tabindex="-1" id="ticketDocument" class="modal fade videoModal" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="ticketheaderImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="ticketNameHeader">TICKET</h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-4">
							<p>Created by: <span id="ticketusernameSpan"></span></p>
						</div>		
                        <div class="col-md-4">
							<p>Location: <span id="ticketlocSpan"></span></p>
						</div>	
						<div class="col-md-4">
							<p>Created on: <span id="tickettimeSpan"></span></p>
						</div>				
					  </div>				
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                            <li class="active" id="ticketliInfo"><a href="#ticketinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="ticketliAtta"><a href="#ticketattachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
										<div class="tab-pane fade active in" id="ticketinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
													<div class="col-md-12">
													<p class="red-color"><b>Plate Number:</b></p>
													<p id="platenumberSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate Source:</b></p>
													<p id="platesourceSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate Code:</b></p>
													<p id="plateCodeSpan"></p>
												</div>
                                                <div class="col-md-12">
                                            	<p class="red-color"><b>Vehicle Make:</b></p>
													<p id="vehicleMakeSpan"></p>
												</div>
                                                <div class="col-md-12">
                                        		<p class="red-color"><b>Vehicle Color:</b></p>
													<p id="vehicleColorSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Offences: </b></p>
                                                    <ul id="offencesItemsList">

                                                    </ul>
												</div>
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div>
										<div class="tab-pane fade" id="ticketattachments-tab" onclick="startRot()">	
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="ticketattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>							
										</div>										
									</div>
								</div>
							</div>
							<div class="col-md-8" id="ticketdivAttachmentHolder">
								<div class="tab-pane fade active in" id="ticketlocation-tab">
                                    <div id="map_ticketIncidentLocation" style="width:100%;height:378px;"></div>
									<div id="ticketdivAttachment" onclick="startRot()" class="overlapping-map-image">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
									<li><a href="#" data-dismiss="modal">UNHANDLE</a>
									</li>
									<li><a href="#" data-dismiss="modal" onclick="handleTicket()">HANDLE</a>
									</li>	
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div> 
            <div aria-hidden="true" aria-labelledby="deleteAttachModal" role="dialog" tabindex="-1" id="deleteAttachModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#viewDocument1').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#viewDocument1').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachment()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="successfulReport" role="dialog" tabindex="-1" id="successfulReport" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="../Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" id="successfulReportScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>
            <input style="display:none;" id="rowidReminderChoice" type="text"/>
            <input style="display:none;" id="verCaseID1" type="text"/>
            <input style="display:none;" id="verCaseID2" type="text"/>
            <input style="display:none;" id="verCaseID3" type="text"/>
            <input style="display:none;" id="verCaseID4" type="text"/>
            <input style="display:none;" id="verCaseID5" type="text"/>
            <input style="display:none;" id="verCaseName1" type="text"/>
            <input style="display:none;" id="verCaseName2" type="text"/>
            <input style="display:none;" id="verCaseName3" type="text"/>
            <input style="display:none;" id="verCaseName4" type="text"/>
            <input style="display:none;" id="verCaseName5" type="text"/>
            <input style="display:none;" id="previousTaskUser" type="text"/>   
            <input style="display:none;" id="rowidChoice" type="text"/>  
            <input style="display:none;" id="rowLongitude" type="text"/>  
            <input style="display:none;" id="rowLatitude" type="text"/>  
            <input style="display:none;" id="cameraDuration" type="text"/>
            <input style="display:none;" id="rowIncidentName" type="text"/>  
            <input style="display:none;" id="rowTaskName" type="text"/> 
            <input style="display:none;" id="rowChoiceTasks" type="text"/>
              <select style=" display:none;" multiple="multiple" id="sendToListBox" ></select>	
             <select style=" display:none;" multiple="multiple" id="usermapListBox" ></select>	 
			 <select style=" display:none;" multiple="multiple" id="liveListBox" ></select>				  			  
           <asp:HiddenField runat="server" ID="tbUserID" />
              <asp:HiddenField runat="server" ID="tbUserName" />
            <asp:HiddenField runat="server" ID="tbincidentName" />
             <input style="display:none;" id="imagePath" type="text"/>
            <input id="verifierID" style="display:none;">  
             <input style="display:none;" id="mobimagePath2" type="text"/>	 
            <input style="display:none;" id="rowidChoiceAttachment" type="text"/> 
        </section>
        <!-- /MAIN -->
</asp:Content>
