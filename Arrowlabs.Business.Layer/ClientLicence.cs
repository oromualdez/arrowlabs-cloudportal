﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class ClientLicence
    {
        public string ClientId { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        public string TotalDevices { get; set; }
        public string Curfew { get; set; }
        public string RemainingDevices { get; set; }
        public string UsedDevices { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string SerialNo { get; set; }
        public string UpdatedDate { get; set; }
        public string ExpiryDate { get; set; }
        public bool LicenseInUse { get; set; }
        public bool DatabaseStatus { get; set; }
        public string ActivationKey { get; set; }
        public string TotalServerClients { get; set; }
        public string ActiveServerClients { get; set; }
        public int Port { get; set; }
        public string TotalMobileUsers { get; set; }
        public string UsedMobileUsers { get; set; }
        public string RemainingMobileUsers { get; set; }
        public string TotalMobileDevices { get; set; }
        public string TotalTabletDevices { get; set; }
        public string Modules { get; set; }
        public bool isSurveillance { get; set; }
        public bool isNotification { get; set; }
        public bool isLocation { get; set; }
        public bool isTicketing { get; set; }
        public bool isTask { get; set; }
        public bool isIncident { get; set; }
        public bool isDispatch { get; set; }
        public bool isWarehouse { get; set; }
        public bool isChat { get; set; }
        public bool isCollaboration { get; set; }
        public bool isRequest { get; set; }
        public bool isLostandFound { get; set; }
        public bool isDutyRoster { get; set; }
        public bool isPostOrder { get; set; }
        public bool isVerification { get; set; }
        public bool isCustomer { get; set; }
      
        public enum LicenseModuleTypes : int
        {
            Unknown = -1,
            None = 0,
            Surveillance = 1,
            Notification = 2,
            Location = 3,
            Ticketing =4,
            Task = 5,
            Incident =6,
            Dispatch = 7,
            Warehouse = 8,
            Chat = 9,
            Collaboration =10,
            Request = 11,
            LostandFound = 12,
            DutyRoster =13,
            PostOrder = 14,
            Verification = 15,
            Activity = 16,
            Customer = 17,
            MessageBoard = 18,
            Contract = 19
        }
        public static ClientLicence GetClientSerialNo(string clientId, string dbConnection)
        {
            ClientLicence result = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("ClientDeviceValid", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = clientId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "ClientDeviceValid";

                    var reader = sqlCommand.ExecuteReader();
                    var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                    while (reader.Read())
                    {

                        result = new ClientLicence();
                        result.ClientId = reader["ClientId"].ToString();

                        result.SerialNo = reader["SerialNo"].ToString();

                    }
                    connection.Close();
                    reader.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "GetLicenseByClientID", ex, dbConnection);
            }
            return readLicenseModules(result, dbConnection);
        }
        public static bool InsertClientLicenceModuleByClientID(ClientLicence clientLicence, string dbConnection)
        {
            var returnVal = true;
            try
            {
                //if (!string.IsNullOrEmpty(clientLicence.ClientId) && !string.IsNullOrEmpty(clientLicence.Modules))
                //{
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();

                        var sqlCommand = new SqlCommand("InsertClientLicenceModule", connection);

                        sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                        sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientLicence.ClientId, true, dbConnection);

                        sqlCommand.Parameters.Add(new SqlParameter("@Modules", SqlDbType.NVarChar));
                        sqlCommand.Parameters["@Modules"].Value = Encrypt.EncryptData(clientLicence.Modules, true, dbConnection);

                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.CommandText = "InsertClientLicenceModule";

                        sqlCommand.ExecuteNonQuery();

                        connection.Close();
                    }
                //}
                //else
                //{
                //    returnVal = false;
                //}
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "InsertClientLicenceModule", ex, dbConnection);
                returnVal = false;
            }
            return returnVal;
        }
        
        public static void InsertClientLicence(ClientLicence clientLicence, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("InsertOrUpdateLicenceClientDevice", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientLicence.ClientId, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = Encrypt.EncryptData(clientLicence.Name, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@PhoneNo", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@PhoneNo"].Value = Encrypt.EncryptData(clientLicence.PhoneNo, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Address", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Address"].Value = Encrypt.EncryptData(clientLicence.Address, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(clientLicence.CreatedDate, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@CreatedBy"].Value = Encrypt.EncryptData(clientLicence.CreatedBy, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UpdatedBy"].Value = Encrypt.EncryptData(clientLicence.UpdatedBy, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SerialNo", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SerialNo"].Value = Encrypt.EncryptData(clientLicence.SerialNo, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UpdatedDate"].Value = Encrypt.EncryptData(clientLicence.UpdatedDate, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ExpiryDate"].Value = Encrypt.EncryptData(clientLicence.ExpiryDate, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Curfew", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Curfew"].Value = Encrypt.EncryptData(clientLicence.Curfew, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TotalDevices", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@TotalDevices"].Value = Encrypt.EncryptData(clientLicence.TotalDevices, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@RemainingDevices", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@RemainingDevices"].Value = Encrypt.EncryptData(clientLicence.RemainingDevices, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UsedDevices", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UsedDevices"].Value = Encrypt.EncryptData(clientLicence.UsedDevices, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TotalMobileUsers", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@TotalMobileUsers"].Value = Encrypt.EncryptData(clientLicence.TotalMobileUsers, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@RemainingMobileUsers", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@RemainingMobileUsers"].Value = Encrypt.EncryptData(clientLicence.RemainingMobileUsers, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UsedMobileUsers", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UsedMobileUsers"].Value = Encrypt.EncryptData(clientLicence.UsedMobileUsers, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ActivationKey", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ActivationKey"].Value = clientLicence.ActivationKey;

                    sqlCommand.Parameters.Add(new SqlParameter("@TotalServerClients", SqlDbType.NVarChar));
                    if (!string.IsNullOrEmpty(clientLicence.TotalServerClients))
                        sqlCommand.Parameters["@TotalServerClients"].Value = Encrypt.EncryptData(clientLicence.TotalServerClients, true, dbConnection);
                    else
                        sqlCommand.Parameters["@TotalServerClients"].Value = clientLicence.TotalServerClients;

                    sqlCommand.Parameters.Add(new SqlParameter("@ActiveServerClients", SqlDbType.NVarChar));
                    if (!string.IsNullOrEmpty(clientLicence.ActiveServerClients))
                        sqlCommand.Parameters["@ActiveServerClients"].Value = Encrypt.EncryptData(clientLicence.ActiveServerClients, true, dbConnection);
                    else
                        sqlCommand.Parameters["@ActiveServerClients"].Value = clientLicence.ActiveServerClients;

                    sqlCommand.Parameters.Add(new SqlParameter("@TotalTabletDevices", SqlDbType.NVarChar));
                    if (!string.IsNullOrEmpty(clientLicence.TotalTabletDevices))
                        sqlCommand.Parameters["@TotalTabletDevices"].Value = Encrypt.EncryptData(clientLicence.TotalTabletDevices, true, dbConnection);
                    else
                        sqlCommand.Parameters["@TotalTabletDevices"].Value = clientLicence.TotalTabletDevices;

                    sqlCommand.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    sqlCommand.Parameters["@Port"].Value = clientLicence.Port;


                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "InsertOrUpdateLicenceClientDevice";

                    sqlCommand.ExecuteNonQuery();

                    connection.Close();

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new ClientLicenceAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.Create, clientLicence.UpdatedBy);
                            Reflection.CopyProperties(clientLicence, audit);
                            ClientLicenceAudit.InsertClientLicenceAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateLicenceClientDevice", ex, dbConnection);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "InsertClientLicenceModule", ex, dbConnection);
            }
        }

        public static void InsertClientLicence(ClientLicence clientLicence, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("InsertOrUpdateLicenceClientDevice", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientLicence.ClientId, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = Encrypt.EncryptData(clientLicence.Name, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@PhoneNo", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@PhoneNo"].Value = Encrypt.EncryptData(clientLicence.PhoneNo, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Address", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Address"].Value = Encrypt.EncryptData(clientLicence.Address, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(clientLicence.CreatedDate, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@CreatedBy"].Value = Encrypt.EncryptData(clientLicence.CreatedBy, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UpdatedBy"].Value = Encrypt.EncryptData(clientLicence.UpdatedBy, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SerialNo", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SerialNo"].Value = Encrypt.EncryptData(clientLicence.SerialNo, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UpdatedDate"].Value = Encrypt.EncryptData(clientLicence.UpdatedDate, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ExpiryDate"].Value = Encrypt.EncryptData(clientLicence.ExpiryDate, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Curfew", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Curfew"].Value = Encrypt.EncryptData(clientLicence.Curfew, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TotalDevices", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@TotalDevices"].Value = Encrypt.EncryptData(clientLicence.TotalDevices, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@RemainingDevices", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@RemainingDevices"].Value = Encrypt.EncryptData(clientLicence.RemainingDevices, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UsedDevices", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UsedDevices"].Value = Encrypt.EncryptData(clientLicence.UsedDevices, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TotalMobileUsers", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@TotalMobileUsers"].Value = Encrypt.EncryptData(clientLicence.TotalMobileUsers, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@RemainingMobileUsers", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@RemainingMobileUsers"].Value = Encrypt.EncryptData(clientLicence.RemainingMobileUsers, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UsedMobileUsers", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UsedMobileUsers"].Value = Encrypt.EncryptData(clientLicence.UsedMobileUsers, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ActivationKey", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ActivationKey"].Value = clientLicence.ActivationKey;

                    sqlCommand.Parameters.Add(new SqlParameter("@TotalServerClients", SqlDbType.NVarChar));
                    if (!string.IsNullOrEmpty(clientLicence.TotalServerClients))
                        sqlCommand.Parameters["@TotalServerClients"].Value = Encrypt.EncryptData(clientLicence.TotalServerClients, true, dbConnection);
                    else
                        sqlCommand.Parameters["@TotalServerClients"].Value = clientLicence.TotalServerClients;

                    sqlCommand.Parameters.Add(new SqlParameter("@ActiveServerClients", SqlDbType.NVarChar));
                    if (!string.IsNullOrEmpty(clientLicence.ActiveServerClients))
                        sqlCommand.Parameters["@ActiveServerClients"].Value = Encrypt.EncryptData(clientLicence.ActiveServerClients, true, dbConnection);
                    else
                        sqlCommand.Parameters["@ActiveServerClients"].Value = clientLicence.ActiveServerClients;

                    sqlCommand.Parameters.Add(new SqlParameter("@TotalTabletDevices", SqlDbType.NVarChar));
                    if (!string.IsNullOrEmpty(clientLicence.TotalTabletDevices))
                        sqlCommand.Parameters["@TotalTabletDevices"].Value = Encrypt.EncryptData(clientLicence.TotalTabletDevices, true, dbConnection);
                    else
                        sqlCommand.Parameters["@TotalTabletDevices"].Value = clientLicence.TotalTabletDevices;

                    sqlCommand.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    sqlCommand.Parameters["@Port"].Value = clientLicence.Port;

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "InsertOrUpdateLicenceClientDevice";

                    sqlCommand.ExecuteNonQuery();

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "InsertClientLicence", ex, dbConnection);
            }
        }

        public static int IsDeviceInWhiteList(string macAddress, string dbConnection)
        {
            var result = 0;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("DeviceWhiteListValid", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MACAddress", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@MACAddress"].Value = macAddress;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "DeviceWhiteListValid";

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader[0].ToString() != "0")
                            result = 1;

                    }

                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "IsDeviceInWhiteList", ex, dbConnection);
            }
            return result;
        }

        public static bool TabletStateChange(string state, string clientId,string dbConnection)
        {
            var result = false;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("ClientDeviceValid", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "ClientDeviceValid";

                    var used = 0;
                    var users = Users.GetAllUsers(dbConnection);
                    foreach (var user in users)
                    {
                        if (user.DeviceType == (int)Users.DeviceTypes.Mobile)
                        {
                            used++;
                        }
                    }

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        if (state == "Enable")
                        {

                            if (used <
                                Convert.ToInt32(Decrypt.DecryptData(reader["TotalTabletDevices"].ToString(), true, dbConnection)))
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            if (used != 0)
                            {
                                result = true;
                            }
                        }

                    }
                    connection.Close();
                    reader.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "TabletStateChange", ex, dbConnection);
            }
            return result;
        }

        public static bool UserStateChange(string state, string clientId, string dbConnection)
        {
            var result = false;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("ClientDeviceValid", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "ClientDeviceValid";

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        if (state == "Enable")
                        {
                            if (Convert.ToInt32(Decrypt.DecryptData(reader["UsedMobileUsers"].ToString(), true, dbConnection)) <
                                Convert.ToInt32(Decrypt.DecryptData(reader["TotalMobileUsers"].ToString(), true, dbConnection)))
                            {
                                var clientlic = new ClientLicence();
                                clientlic.ClientId = Decrypt.DecryptData(reader["ClientId"].ToString(), true, dbConnection);
                                clientlic.PhoneNo = Decrypt.DecryptData(reader["PhoneNo"].ToString(), true, dbConnection);
                                clientlic.Address = Decrypt.DecryptData(reader["Address"].ToString(), true, dbConnection);
                                clientlic.Name = Decrypt.DecryptData(reader["Name"].ToString(), true, dbConnection);
                                clientlic.SerialNo = Decrypt.DecryptData(reader["SerialNo"].ToString().Split('}')[0], true, dbConnection);
                                clientlic.CreatedBy = Decrypt.DecryptData(reader["CreatedBy"].ToString(), true, dbConnection);
                                clientlic.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                                clientlic.UpdatedBy = Decrypt.DecryptData(reader["UpdatedBy"].ToString(), true, dbConnection);
                                clientlic.UpdatedDate = Decrypt.DecryptData(reader["UpdatedDate"].ToString(), true, dbConnection);
                                clientlic.DatabaseStatus = Convert.ToBoolean(reader["DatabaseStatus"]);
                                clientlic.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                                clientlic.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                                clientlic.UsedDevices = Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection);
                                clientlic.TotalDevices = Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection);
                                clientlic.RemainingDevices = Decrypt.DecryptData(reader["RemainingDevices"].ToString(), true, dbConnection);
                                clientlic.UsedMobileUsers = Decrypt.DecryptData(reader["UsedMobileUsers"].ToString(), true, dbConnection);
                                clientlic.TotalMobileUsers = Decrypt.DecryptData(reader["TotalMobileUsers"].ToString(), true, dbConnection);
                                clientlic.RemainingMobileUsers = Decrypt.DecryptData(reader["RemainingMobileUsers"].ToString(), true, dbConnection);
                                clientlic.LicenseInUse = Convert.ToBoolean(reader["LicenseInUse"]);
                                clientlic.ActivationKey = reader["ActivationKey"].ToString();
                                if (!string.IsNullOrEmpty(reader["TotalServerClients"].ToString()))
                                    clientlic.TotalServerClients = Decrypt.DecryptData(reader["TotalServerClients"].ToString(), true, dbConnection);

                                if (!string.IsNullOrEmpty(reader["ActiveServerClients"].ToString()))
                                    clientlic.ActiveServerClients = Decrypt.DecryptData(reader["ActiveServerClients"].ToString(), true, dbConnection);
                                if (!string.IsNullOrEmpty(reader["Port"].ToString()))
                                    clientlic.Port = Convert.ToInt32(reader["Port"].ToString());
                                clientlic.TotalTabletDevices = Decrypt.DecryptData(reader["TotalTabletDevices"].ToString(), true, dbConnection);
                                clientlic.UsedMobileUsers = (Convert.ToInt32(clientlic.UsedMobileUsers) + 1).ToString();
                                clientlic.RemainingMobileUsers = (Convert.ToInt32(clientlic.RemainingMobileUsers) - 1).ToString();
                                InsertClientLicence(clientlic, dbConnection);
                                result = true;
                            }
                        }
                        else
                        {
                            var mobileUsers = (string.IsNullOrEmpty(Decrypt.DecryptData(reader["UsedMobileUsers"].ToString(), true, dbConnection))) ? 0 : Convert.ToInt32(Decrypt.DecryptData(reader["UsedMobileUsers"].ToString(), true, dbConnection));

                            if (mobileUsers != 0)
                            {
                                {
                                    var clientlic = new ClientLicence();
                                    clientlic.ClientId = Decrypt.DecryptData(reader["ClientId"].ToString(), true, dbConnection);
                                    clientlic.PhoneNo = Decrypt.DecryptData(reader["PhoneNo"].ToString(), true, dbConnection);
                                    clientlic.Address = Decrypt.DecryptData(reader["Address"].ToString(), true, dbConnection);
                                    clientlic.Name = Decrypt.DecryptData(reader["Name"].ToString(), true, dbConnection);
                                    clientlic.SerialNo = Decrypt.DecryptData(reader["SerialNo"].ToString().Split('}')[0], true, dbConnection);
                                    clientlic.CreatedBy = Decrypt.DecryptData(reader["CreatedBy"].ToString(), true, dbConnection);
                                    clientlic.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                                    clientlic.UpdatedBy = Decrypt.DecryptData(reader["UpdatedBy"].ToString(), true, dbConnection);
                                    clientlic.UpdatedDate = Decrypt.DecryptData(reader["UpdatedDate"].ToString(), true, dbConnection);
                                    clientlic.DatabaseStatus = Convert.ToBoolean(reader["DatabaseStatus"]);
                                    clientlic.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                                    clientlic.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                                    clientlic.UsedDevices = Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection);
                                    clientlic.TotalDevices = Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection);
                                    clientlic.RemainingDevices = Decrypt.DecryptData(reader["RemainingDevices"].ToString(), true, dbConnection);
                                    clientlic.UsedMobileUsers = Decrypt.DecryptData(reader["UsedMobileUsers"].ToString(), true, dbConnection);
                                    clientlic.TotalMobileUsers = Decrypt.DecryptData(reader["TotalMobileUsers"].ToString(), true, dbConnection);
                                    clientlic.RemainingMobileUsers = Decrypt.DecryptData(reader["RemainingMobileUsers"].ToString(), true, dbConnection);
                                    clientlic.LicenseInUse = Convert.ToBoolean(reader["LicenseInUse"]);
                                    clientlic.ActivationKey = reader["ActivationKey"].ToString();
                                    if (!string.IsNullOrEmpty(reader["TotalServerClients"].ToString()))
                                        clientlic.TotalServerClients = Decrypt.DecryptData(reader["TotalServerClients"].ToString(), true, dbConnection);

                                    if (!string.IsNullOrEmpty(reader["ActiveServerClients"].ToString()))
                                        clientlic.ActiveServerClients = Decrypt.DecryptData(reader["ActiveServerClients"].ToString(), true, dbConnection);
                                    if (!string.IsNullOrEmpty(reader["Port"].ToString()))
                                        clientlic.Port = Convert.ToInt32(reader["Port"].ToString());
                                    clientlic.TotalTabletDevices = Decrypt.DecryptData(reader["TotalTabletDevices"].ToString(), true, dbConnection);
                                    clientlic.UsedMobileUsers = (Convert.ToInt32(clientlic.UsedMobileUsers) - 1).ToString();
                                    clientlic.RemainingMobileUsers = (Convert.ToInt32(clientlic.RemainingMobileUsers) + 1).ToString();
                                    InsertClientLicence(clientlic, dbConnection);
                                    result = true;
                                }
                            }
                        }

                    }
                    connection.Close();
                    reader.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "UserStateChange", ex, dbConnection);
            }
            return result;
        }

        public static bool DeviceStateChange(string state, string clientId, string dbConnection)
        {
            var result = false;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("ClientDeviceValid", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "ClientDeviceValid";

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        if (state == "Enable")
                        {
                            if (Convert.ToInt32(Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection)) <
                                Convert.ToInt32(Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection)))
                            {
                                var clientlic = new ClientLicence();
                                clientlic.ClientId = Decrypt.DecryptData(reader["ClientId"].ToString(), true, dbConnection);
                                clientlic.PhoneNo = Decrypt.DecryptData(reader["PhoneNo"].ToString(), true, dbConnection);
                                clientlic.Address = Decrypt.DecryptData(reader["Address"].ToString(), true, dbConnection);
                                clientlic.Name = Decrypt.DecryptData(reader["Name"].ToString(), true, dbConnection);
                                clientlic.SerialNo = Decrypt.DecryptData(reader["SerialNo"].ToString().Split('}')[0], true, dbConnection);
                                clientlic.CreatedBy = Decrypt.DecryptData(reader["CreatedBy"].ToString(), true, dbConnection);
                                clientlic.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                                clientlic.UpdatedBy = Decrypt.DecryptData(reader["UpdatedBy"].ToString(), true, dbConnection);
                                clientlic.UpdatedDate = Decrypt.DecryptData(reader["UpdatedDate"].ToString(), true, dbConnection);
                                clientlic.DatabaseStatus = Convert.ToBoolean(reader["DatabaseStatus"]);
                                clientlic.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                                clientlic.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                                clientlic.UsedDevices = Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection);
                                clientlic.TotalDevices = Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection);
                                clientlic.RemainingDevices = Decrypt.DecryptData(reader["RemainingDevices"].ToString(), true, dbConnection);
                                clientlic.UsedMobileUsers = Decrypt.DecryptData(reader["UsedMobileUsers"].ToString(), true, dbConnection);
                                clientlic.TotalMobileUsers = Decrypt.DecryptData(reader["TotalMobileUsers"].ToString(), true, dbConnection);
                                clientlic.RemainingMobileUsers = Decrypt.DecryptData(reader["RemainingMobileUsers"].ToString(), true, dbConnection);
                                clientlic.LicenseInUse = Convert.ToBoolean(reader["LicenseInUse"]);
                                clientlic.ActivationKey = reader["ActivationKey"].ToString();
                                if (!string.IsNullOrEmpty(reader["TotalServerClients"].ToString()))
                                    clientlic.TotalServerClients = Decrypt.DecryptData(reader["TotalServerClients"].ToString(), true, dbConnection);

                                if (!string.IsNullOrEmpty(reader["ActiveServerClients"].ToString()))
                                    clientlic.ActiveServerClients = Decrypt.DecryptData(reader["ActiveServerClients"].ToString(), true, dbConnection);
                                if (!string.IsNullOrEmpty(reader["Port"].ToString()))
                                    clientlic.Port = Convert.ToInt32(reader["Port"].ToString());

                                clientlic.TotalTabletDevices = Decrypt.DecryptData(reader["TotalTabletDevices"].ToString(), true, dbConnection);
                                clientlic.UsedDevices = (Convert.ToInt32(clientlic.UsedDevices) + 1).ToString();
                                clientlic.RemainingDevices = (Convert.ToInt32(clientlic.RemainingDevices) - 1).ToString();
                                InsertClientLicence(clientlic, dbConnection);
                                result = true;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection)) != 0)
                            {
                                var clientlic = new ClientLicence();
                                clientlic.ClientId = Decrypt.DecryptData(reader["ClientId"].ToString(), true, dbConnection);
                                clientlic.PhoneNo = Decrypt.DecryptData(reader["PhoneNo"].ToString(), true, dbConnection);
                                clientlic.Address = Decrypt.DecryptData(reader["Address"].ToString(), true, dbConnection);
                                clientlic.Name = Decrypt.DecryptData(reader["Name"].ToString(), true, dbConnection);
                                clientlic.SerialNo = Decrypt.DecryptData(reader["SerialNo"].ToString().Split('}')[0], true, dbConnection);
                                clientlic.CreatedBy = Decrypt.DecryptData(reader["CreatedBy"].ToString(), true, dbConnection);
                                clientlic.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                                clientlic.UpdatedBy = Decrypt.DecryptData(reader["UpdatedBy"].ToString(), true, dbConnection);
                                clientlic.UpdatedDate = Decrypt.DecryptData(reader["UpdatedDate"].ToString(), true, dbConnection);
                                clientlic.DatabaseStatus = Convert.ToBoolean(reader["DatabaseStatus"]);
                                clientlic.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                                clientlic.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                                clientlic.UsedDevices = Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection);
                                clientlic.TotalDevices = Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection);
                                clientlic.RemainingDevices = Decrypt.DecryptData(reader["RemainingDevices"].ToString(), true, dbConnection);
                                clientlic.UsedMobileUsers = Decrypt.DecryptData(reader["UsedMobileUsers"].ToString(), true, dbConnection);
                                clientlic.TotalMobileUsers = Decrypt.DecryptData(reader["TotalMobileUsers"].ToString(), true, dbConnection);
                                clientlic.RemainingMobileUsers = Decrypt.DecryptData(reader["RemainingMobileUsers"].ToString(), true, dbConnection);
                                clientlic.LicenseInUse = Convert.ToBoolean(reader["LicenseInUse"]);
                                clientlic.ActivationKey = reader["ActivationKey"].ToString();
                                if (!string.IsNullOrEmpty(reader["TotalServerClients"].ToString()))
                                    clientlic.TotalServerClients = Decrypt.DecryptData(reader["TotalServerClients"].ToString(), true, dbConnection);

                                if (!string.IsNullOrEmpty(reader["ActiveServerClients"].ToString()))
                                    clientlic.ActiveServerClients = Decrypt.DecryptData(reader["ActiveServerClients"].ToString(), true, dbConnection);
                                if (!string.IsNullOrEmpty(reader["Port"].ToString()))
                                    clientlic.Port = Convert.ToInt32(reader["Port"].ToString());
                                clientlic.TotalTabletDevices = Decrypt.DecryptData(reader["TotalTabletDevices"].ToString(), true, dbConnection);
                                clientlic.UsedDevices = (Convert.ToInt32(clientlic.UsedDevices) - 1).ToString();
                                clientlic.RemainingDevices = (Convert.ToInt32(clientlic.RemainingDevices) + 1).ToString();
                                InsertClientLicence(clientlic, dbConnection);
                                result = true;
                            }
                        }

                    }
                    connection.Close();
                    reader.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "DeviceStateChange", ex, dbConnection);
            }
            return result;
        }

        public static ClientLicence IsClientLicenceValid(string clientId, string macAddress, string dbConnection)
        {
            ClientLicence result = null;
            try
            {
                if (IsDeviceInWhiteList(macAddress, dbConnection) > 0)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var sqlCommand = new SqlCommand("ClientDeviceValid", connection);

                        sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                        sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection);
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.CommandText = "ClientDeviceValid";

                        var reader = sqlCommand.ExecuteReader();
                        while (reader.Read())
                        {

                            if (Convert.ToDateTime(Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection)) > DateTime.Now)
                            {
                                if (Convert.ToInt32(Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection)) <=
                                    Convert.ToInt32(Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection)))
                                {
                                    result = new ClientLicence();
                                    result.ClientId = Decrypt.DecryptData(reader["ClientId"].ToString(), true, dbConnection);
                                    result.PhoneNo = Decrypt.DecryptData(reader["PhoneNo"].ToString(), true, dbConnection);
                                    result.Address = Decrypt.DecryptData(reader["Address"].ToString(), true, dbConnection);
                                    result.Name = Decrypt.DecryptData(reader["Name"].ToString(), true, dbConnection);
                                    result.SerialNo = Decrypt.DecryptData(reader["SerialNo"].ToString().Split('}')[0], true, dbConnection);
                                    result.CreatedBy = Decrypt.DecryptData(reader["CreatedBy"].ToString(), true, dbConnection);
                                    result.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                                    result.UpdatedBy = Decrypt.DecryptData(reader["UpdatedBy"].ToString(), true, dbConnection);
                                    result.UpdatedDate = Decrypt.DecryptData(reader["UpdatedDate"].ToString(), true, dbConnection);
                                    result.DatabaseStatus = Convert.ToBoolean(reader["DatabaseStatus"]);
                                    result.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                                    result.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                                    result.UsedDevices = Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection);
                                    result.TotalDevices = Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection);
                                    result.RemainingDevices = Decrypt.DecryptData(reader["RemainingDevices"].ToString(), true, dbConnection);
                                    result.LicenseInUse = Convert.ToBoolean(reader["LicenseInUse"]);
                                    result.Port = Convert.ToInt32(reader["Port"].ToString());
                                    result.TotalTabletDevices = Decrypt.DecryptData(reader["TotalTabletDevices"].ToString(), true, dbConnection);
                                }
                            }
                        }
                        connection.Close();
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "IsClientLicenceValid", ex, dbConnection);
            }
            return result;
        }

        public static ClientLicence GetLicenseByClientID(string clientId, string dbConnection)
        {
            ClientLicence result = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("ClientDeviceValid", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "ClientDeviceValid";

                    var reader = sqlCommand.ExecuteReader();
                    var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                    while (reader.Read())
                    {

                        //    if (HotEvent.DateFormater((Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true))) > DateTime.Now)
                        {
                            //if (Convert.ToInt32(Decrypt.DecryptData(reader["UsedDevices"].ToString(), true)) <
                            //    Convert.ToInt32(Decrypt.DecryptData(reader["TotalDevices"].ToString(), true)))
                            {
                                result = new ClientLicence();
                                result.ClientId = Decrypt.DecryptData(reader["ClientId"].ToString(), true, dbConnection);
                                result.PhoneNo = Decrypt.DecryptData(reader["PhoneNo"].ToString(), true, dbConnection);
                                result.Address = Decrypt.DecryptData(reader["Address"].ToString(), true, dbConnection);
                                result.Name = Decrypt.DecryptData(reader["Name"].ToString(), true, dbConnection);
                                result.SerialNo = Decrypt.DecryptData(reader["SerialNo"].ToString().Split('}')[0], true, dbConnection);
                                result.CreatedBy = Decrypt.DecryptData(reader["CreatedBy"].ToString(), true, dbConnection);
                                result.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                                result.UpdatedBy = Decrypt.DecryptData(reader["UpdatedBy"].ToString(), true, dbConnection);
                                result.UpdatedDate = Decrypt.DecryptData(reader["UpdatedDate"].ToString(), true, dbConnection);
                                result.DatabaseStatus = Convert.ToBoolean(reader["DatabaseStatus"]);
                                result.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                                result.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                                result.UsedDevices = Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection);
                                result.TotalDevices = Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection);
                                result.RemainingDevices = Decrypt.DecryptData(reader["RemainingDevices"].ToString(), true, dbConnection);
                                result.UsedMobileUsers = Decrypt.DecryptData(reader["UsedMobileUsers"].ToString(), true, dbConnection);
                                result.TotalMobileUsers = Decrypt.DecryptData(reader["TotalMobileUsers"].ToString(), true, dbConnection);
                                result.RemainingMobileUsers = Decrypt.DecryptData(reader["RemainingMobileUsers"].ToString(), true, dbConnection);
                                result.LicenseInUse = Convert.ToBoolean(reader["LicenseInUse"]);
                                result.ActivationKey = reader["ActivationKey"].ToString();
                                if (columns.Contains( "TotalServerClients"))
                                {
                                    if (!string.IsNullOrEmpty(reader["TotalServerClients"].ToString()))
                                        result.TotalServerClients = Decrypt.DecryptData(reader["TotalServerClients"].ToString(), true, dbConnection);
                                }
                                if (columns.Contains( "ActiveServerClients"))
                                {
                                    if (!string.IsNullOrEmpty(reader["ActiveServerClients"].ToString()))
                                        result.ActiveServerClients = Decrypt.DecryptData(reader["ActiveServerClients"].ToString(), true, dbConnection);
                                }
                                if (columns.Contains( "Port"))
                                {
                                    if (!string.IsNullOrEmpty(reader["Port"].ToString()))
                                        result.Port = Convert.ToInt32(reader["Port"].ToString());
                                }
                                if (columns.Contains( "TotalMobileDevices"))
                                {
                                    if (!string.IsNullOrEmpty(reader["TotalMobileDevices"].ToString()))
                                        result.TotalMobileDevices = Decrypt.DecryptData(reader["TotalMobileDevices"].ToString(), true, dbConnection);
                                }
                                if (columns.Contains( "TotalTabletDevices"))
                                {
                                    if (!string.IsNullOrEmpty(reader["TotalTabletDevices"].ToString()))
                                        result.TotalTabletDevices = Decrypt.DecryptData(reader["TotalTabletDevices"].ToString(), true, dbConnection);
                                }
                                if (columns.Contains( "Modules"))
                                {
                                    if (!string.IsNullOrEmpty(reader["Modules"].ToString()))
                                        result.Modules = Decrypt.DecryptData(reader["Modules"].ToString(), true, dbConnection);
                                }

                            }
                        }
                    }
                    connection.Close();
                    reader.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "GetLicenseByClientID", ex, dbConnection);
            }
            return readLicenseModules(result, dbConnection);
        }
        public static ClientLicence readLicenseModules(ClientLicence client,string dbConnection)
        {
            try
            {
                if (client != null)
                {
                    if (!string.IsNullOrEmpty(client.Modules))
                    {
                        var modules = client.Modules.Split('?');
                        foreach (var mods in modules)
                        {
                            if (!string.IsNullOrEmpty(mods))
                            {
                                if (mods == ((int)LicenseModuleTypes.Chat).ToString())
                                {
                                    client.isChat = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Collaboration).ToString())
                                {
                                    client.isCollaboration = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Dispatch).ToString())
                                {
                                    client.isDispatch = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.DutyRoster).ToString())
                                {
                                    client.isDutyRoster = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Incident).ToString())
                                {
                                    client.isIncident = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Location).ToString())
                                {
                                    client.isLocation = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.LostandFound).ToString())
                                {
                                    client.isLostandFound = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Notification).ToString())
                                {
                                    client.isNotification = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.PostOrder).ToString())
                                {
                                    client.isPostOrder = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Request).ToString())
                                {
                                    client.isRequest = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Surveillance).ToString())
                                {
                                    client.isSurveillance = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Task).ToString())
                                {
                                    client.isTask = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Ticketing).ToString())
                                {
                                    client.isTicketing = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Verification).ToString())
                                {
                                    client.isVerification = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Warehouse).ToString())
                                {
                                    client.isWarehouse = true;
                                }
                                if (mods == ((int)LicenseModuleTypes.Customer).ToString())
                                {
                                    client.isCustomer = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "readLicenseModules", ex, dbConnection);
            }
            return client;
        }
        public static void UpdateClientLicence(string clientId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("ClientDeviceValid", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "ClientDeviceValid";

                    var reader = sqlCommand.ExecuteReader();

                    int remainingDevices = 0;
                    int usedDevices = 0;

                    while (reader.Read())
                    {
                        if (Convert.ToInt32(Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection)) <
                                    Convert.ToInt32(Decrypt.DecryptData(reader["TotalDevices"].ToString(), true, dbConnection)))
                        {
                            usedDevices = Convert.ToInt32(Decrypt.DecryptData(reader["UsedDevices"].ToString(), true, dbConnection)) + 1;
                            remainingDevices = Convert.ToInt32(Decrypt.DecryptData(reader["RemainingDevices"].ToString(), true, dbConnection)) - 1;
                        }
                    }
                    reader.Close();
                    if (remainingDevices >= 0 && usedDevices != 0)
                    {
                        sqlCommand = new SqlCommand("UpdateClientLicence", connection);

                        sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                        sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection); ;

                        sqlCommand.Parameters.Add(new SqlParameter("@RemainingDevices", SqlDbType.NVarChar));
                        sqlCommand.Parameters["@RemainingDevices"].Value = Encrypt.EncryptData(remainingDevices.ToString(), true, dbConnection);

                        sqlCommand.Parameters.Add(new SqlParameter("@UsedDevices", SqlDbType.NVarChar));
                        sqlCommand.Parameters["@UsedDevices"].Value = Encrypt.EncryptData(usedDevices.ToString(), true, dbConnection);

                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.CommandText = "UpdateClientLicence";

                        sqlCommand.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "UpdateClientLicence", ex, dbConnection);
            }
        }

        public static void UpdateLicenceInfo(string clientId, string usedDevice, string remainingDevice, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("UpdateLicenceInfo", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UsedDevices", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UsedDevices"].Value = Encrypt.EncryptData(usedDevice, true, dbConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@RemainingDevices", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@RemainingDevices"].Value = Encrypt.EncryptData(remainingDevice, true, dbConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "UpdateLicenceInfo";

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "UpdateLicenceInfo", ex, dbConnection);
            }
        }

        public static void UpdateLicenseClientDevice(string clientId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("UpdateLicenseClientDevice", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientId, true, dbConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "UpdateLicenseClientDevice";

                    sqlCommand.ExecuteNonQuery();

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "UpdateLicenseClientDevice", ex, dbConnection);
            }
        }
    }
}
