﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ClientManagerAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string MacAddress { get; set; }
        public int UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }
        public static bool InsertorUpdateClientManager(ClientManagerAudit clientManager, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(clientManager.BaseAudit, dbConnection);

            if (clientManager != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateClientManager", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = clientManager.Id;

                    cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MacAddress"].Value = clientManager.MacAddress;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = clientManager.UserId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = clientManager.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = clientManager.UpdatedDate;


                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = clientManager.CreatedBy;
                 
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.NVarChar));
                    cmd.Parameters["@SiteId"].Value = clientManager.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
