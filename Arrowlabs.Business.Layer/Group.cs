﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int ColorCode { get; set; }
        public string UpdatedBy { get; set; }

        public string SiteName { get; set; }
        public string View { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public enum SearchTypes
        {
            Username = 0,
            FirstName = 1,
            LastName = 2
        }
        public static List<string> GetSearchTypes()
        {
            return Enum.GetNames(typeof(SearchTypes)).ToList();
        }

        private static Group GetGroupFromSqlReader(SqlDataReader reader)
        {
            var group = new Group();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                group.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                group.Name = reader["Name"].ToString();
            if (reader["CreateDate"] != DBNull.Value)
                group.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                group.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                group.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains( "AccountId"))
                group.AccountId = Convert.ToInt32(reader["AccountId"]);
            if (columns.Contains( "AccountName"))
                group.AccountName = reader["AccountName"].ToString();

            if (columns.Contains( "SiteName") && reader["SiteName"] != DBNull.Value)
                group.SiteName = reader["SiteName"].ToString();

            if (columns.Contains( "ColorCode"))
                group.ColorCode = Convert.ToInt32(reader["ColorCode"]);

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                group.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                group.CustomerId = Convert.ToInt32(reader["CustomerId"]);

            group.View = "View";

            return group;
        }

        public static List<Group> GetAllGroup(string dbConnection)
        {
            var groups = new List<Group>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllGroup", connection);
                sqlCommand.CommandText = "GetAllGroup";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var group = GetGroupFromSqlReader(reader);
                    groups.Add(group);
                }
                connection.Close();
                reader.Close();
            }
            return groups;
        }

        public static List<Group> GetAllGroupBySiteId(int siteid, string dbConnection)
        {
            var groups = new List<Group>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllGroupBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.CommandText = "GetAllGroupBySiteId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var group = GetGroupFromSqlReader(reader);
                    groups.Add(group);
                }
                connection.Close();
                reader.Close();
            }
            return groups;
        }

        public static List<Group> GetAllGroupByCreator(string name ,string dbConnection,bool ismobile=false)
        {
            var modules = UserModules.GetAllModulesByUsername(name, dbConnection);
            var isgroup = false;
            foreach (var mods in modules)
            {
                if (ismobile)
                {
                    if (mods.ModuleId == (int)Accounts.ModuleTypes.MobileCollaboration)
                    {
                        isgroup = true;
                    }
                }
                else
                {
                    if (mods.ModuleId == (int)Accounts.ModuleTypes.Collaboration)
                    {
                        isgroup = true;
                    }
                }
            }

            var groups = new List<Group>();
            if (isgroup)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllGroupByCreator", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = name;
                    sqlCommand.CommandText = "GetAllGroupByCreator";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var group = GetGroupFromSqlReader(reader);
                        groups.Add(group);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            return groups;
        }

        public static Group GetGroupById(int Id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                Group group = null;
                connection.Open();
                var sqlCommand = new SqlCommand("GetGroupById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = Id;
                sqlCommand.CommandText = "GetGroupById";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    group = GetGroupFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
                return group;
            }

        }

        public static Group GetGroupByName(string name, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                Group group = null;
                connection.Open();
                var sqlCommand = new SqlCommand("GetGroupByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@name"].Value = name;
                sqlCommand.CommandText = "GetGroupByName";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    group = GetGroupFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
                return group;
            }

        }
        public static Group GetGroupByNameAndCId(string name,int id ,string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                Group group = null;
                connection.Open();
                var sqlCommand = new SqlCommand("GetGroupByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandText = "GetGroupByNameAndCId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    group = GetGroupFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
                return group;
            }

        }
        public static int InsertorUpdateGroup(Group group, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)

        {
            int groupId = group.Id;
            var action = (groupId == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


            var id = 0;
            var isNew = false;
            if(group.Id == 0)
            {
                isNew = true;
            }
            if (group != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateGroup", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = group.Id;

                    cmd.Parameters.Add(new SqlParameter("@ColorCode", SqlDbType.Int));
                    cmd.Parameters["@ColorCode"].Value = group.ColorCode;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = group.Name;

                    cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreateDate"].Value = group.CreateDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = group.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = group.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = group.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = group.CustomerId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateGroup";

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    if (groupId == 0)
                        groupId = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            group.Id = id;
                            var audit = new GroupAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, group.UpdatedBy);
                            Reflection.CopyProperties(group, audit);
                            GroupAudit.InsertGroupAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateGroup", ex, dbConnection);
                    }


                    //var grpAudit = new GroupAudit();
                    //grpAudit.BaseAudit = new BaseAudit();
                    //grpAudit.BaseAudit.Account = group.AccountName;
                    //if (isNew)
                    //{
                    //    grpAudit.BaseAudit.Action = (int)Arrowlabs.Mims.Audit.Enums.Action.Create;
                    //    grpAudit.Id = id;
                    //}
                    //else
                    //{
                    //    grpAudit.BaseAudit.Action = (int)Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;
                    //    grpAudit.Id = group.Id;
                    //}
                    //grpAudit.BaseAudit.AuditBy = group.CreatedBy;
                    //grpAudit.BaseAudit.AuditDate = DateTime.Now;
                    //grpAudit.BaseAudit.Description = "N/A";
                    //grpAudit.CreateDate = group.CreateDate.Value;
                    //grpAudit.CreatedBy = group.CreatedBy;
                    //grpAudit.Name = group.Name;
                    //GroupAudit.InsertGroupAudit(grpAudit, dbAudit);


                }


            }

            return id;
        }

        public static bool DeleteGroupByGroupId(int groupId, string dbConnection)
        {
            if (DeviceGroup.DeleteDeviceGroupByGroupId(groupId, dbConnection))
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteGroupByGroupId", connection);

                    cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                    cmd.Parameters["@GroupId"].Value = groupId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteGroupByGroupId";

                    cmd.ExecuteNonQuery();
                }
                return true;
            }

            return false ;
        }

        public static List<Group> GetAllGroupBySearchName(string search, string dbConnection)
        {
            var groups = new List<Group>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllGroupBySearchName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Search", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Search"].Value = search;
                sqlCommand.CommandText = "GetAllGroupBySearchName";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var group = GetGroupFromSqlReader(reader);
                    groups.Add(group);
                }
                connection.Close();
                reader.Close();
            }
            return groups;
        }

        public static List<Group> GetAllGroupByCreatorAndName(string name, string search, string dbConnection)
        {
            var groups = new List<Group>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllGroupByCreatorAndName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Search", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Search"].Value = search;
                sqlCommand.CommandText = "GetAllGroupByCreatorAndName";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var group = GetGroupFromSqlReader(reader);
                    groups.Add(group);
                }
                connection.Close();
                reader.Close();
            }
            return groups;
        }
    }
}
