﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
 public class ClientManager
 {
     public int Id { get; set; }
     public string MacAddress { get; set; }
     public int UserId { get; set; }
     public DateTime? CreatedDate { get; set; }
     public DateTime? UpdatedDate { get; set; }
     public string CreatedBy { get; set; }
     public int SiteId { get; set; }
     private static ClientManager GetClientManagerFromSqlReader(SqlDataReader reader)
     {
         var clientManager = new ClientManager();
         var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
         if (reader["Id"] != DBNull.Value)
             clientManager.Id = Convert.ToInt32(reader["Id"].ToString());
         
         if (reader["MacAddress"] != DBNull.Value)
             clientManager.MacAddress = reader["MacAddress"].ToString();
        
         if (reader["UserId"] != DBNull.Value)
             clientManager.UserId = Convert.ToInt32(reader["UserId"].ToString());
        
         if (reader["CreatedDate"] != DBNull.Value)
             clientManager.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
        
         if (reader["UpdatedDate"] != DBNull.Value)
             clientManager.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
        
         if (reader["CreatedBy"] != DBNull.Value)
             clientManager.CreatedBy = reader["CreatedBy"].ToString();

         if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
             clientManager.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

         return clientManager;
     }
     public static bool InsertorUpdateClientManager(ClientManager clientManager, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
     {
         int id = 0;

         var action = (clientManager.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

         if (clientManager != null)
         {
             using (var connection = new SqlConnection(dbConnection))
             {
                 connection.Open();
                 var cmd = new SqlCommand("InsertorUpdateClientManager", connection);

                 cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                 cmd.Parameters["@Id"].Value = clientManager.Id;

                 cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                 cmd.Parameters["@MacAddress"].Value = clientManager.MacAddress;

                 cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                 cmd.Parameters["@UserId"].Value = clientManager.UserId;

                 cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                 cmd.Parameters["@CreatedDate"].Value = clientManager.CreatedDate;

                 cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                 cmd.Parameters["@UpdatedDate"].Value = clientManager.UpdatedDate;


                 cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                 cmd.Parameters["@CreatedBy"].Value = clientManager.CreatedBy;

                 cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                 cmd.Parameters["@SiteId"].Value = clientManager.SiteId;

                 SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                 returnParameter.Direction = ParameterDirection.ReturnValue;

                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.ExecuteNonQuery();

                 id = (int)returnParameter.Value;

                 try
                 {
                     if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                     {
                         var audit = new ClientManagerAudit();
                         audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, clientManager.CreatedBy);
                         Reflection.CopyProperties(clientManager, audit);
                         ClientManagerAudit.InsertorUpdateClientManager(audit, auditDbConnection);
                     }
                 }
                 catch (Exception ex)
                 {
                     MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateClientManager", ex, dbConnection);
                 }
             }
         }
         return true;
     }
     public static List<ClientManager> GetAllClientManager(string dbConnection)
     {
         List<ClientManager> allClientManager = new List<ClientManager>();
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();

             var cmd = new SqlCommand("GetAllClientManager", connection);

             cmd.CommandType = CommandType.StoredProcedure;

             var resultSet = cmd.ExecuteReader();
             while (resultSet.Read())
             {
                 var groupRole = GetClientManagerFromSqlReader(resultSet);
                 allClientManager.Add(groupRole);
             }
             connection.Close();
             resultSet.Close();
             return allClientManager;
         }
     }

     public static List<ClientManager> GetAllClientManagerByMacAddress(string macAddress, string dbConnection)
     {
         List<ClientManager> allClientCamera = new List<ClientManager>();
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();

             var cmd = new SqlCommand("GetAllClientManagerByMacAddress", connection);

             cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
             cmd.Parameters["@MacAddress"].Value = macAddress;

             cmd.CommandType = CommandType.StoredProcedure;

             var resultSet = cmd.ExecuteReader();
             while (resultSet.Read())
             {
                 var groupRole = GetClientManagerFromSqlReader(resultSet);
                 allClientCamera.Add(groupRole);
             }
             connection.Close();
             resultSet.Close();
             return allClientCamera;
         }
     }
     public static bool DeleteClientManagerByMacAddress(string macAddress, string dbConnection)
     {
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var cmd = new SqlCommand("DeleteClientManagerByMacAddress", connection);

             cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
             cmd.Parameters["@MacAddress"].Value = macAddress;

             cmd.CommandType = CommandType.StoredProcedure;

             cmd.ExecuteNonQuery();
         }
         return true;
     }
    }
}
