﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class DriverOffenceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public Guid Identifier { get; set; }
        public string OffenceDate { get; set; }
        public string OffenceTime { get; set; }
        public string PlateNumber { get; set; }
        public string PlateSource { get; set; }
        public string PlateCode { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleColor { get; set; }
        public string VehicleType { get; set; }
        public string LocationType { get; set; }
        public string Location { get; set; }
        public string OffenceType { get; set; }
        public string Offence1 { get; set; }
        public string Offence2 { get; set; }
        public string Offence3 { get; set; }
        public string OffenceCategory { get; set; }
        public string Offence4 { get; set; }
        
        public string UserName { get; set; }
        
        public byte[] VideoPath { get; set; }
        
        public byte[] ImagePath1 { get; set; }
        
        public byte[] ImagePath2 { get; set; }
        
        public byte[] ImagePath3 { get; set; }
        
        public byte[] ImagePath4 { get; set; }
        
        public byte[] ImagePath5 { get; set; }
        
        public byte[] ImagePath6 { get; set; }
        
        public byte[] OffenceImagePath { get; set; }
        
        public string VideoPath1 { get; set; }
        
        public string Image1Path { get; set; }
        
        public string Image2Path { get; set; }
        
        public string Image3Path { get; set; }
        
        public string Image4Path { get; set; }
        
        public string Image5Path { get; set; }
        
        public string Image6Path { get; set; }
        
        public string OffenceImagePath1 { get; set; }
        
        public double Longitude { get; set; }
        
        public double Latitude { get; set; }
        
                public DateTime StartDate { get; set; }
                public DateTime EndDate { get; set; }

                public string StartBy { get; set; }
                        public string EndBy { get; set; }
         
        public DateTime CreatedDate { get; set; }
        
        public int Status { get; set; }

                public int ContractId { get; set; }

                public int CusId { get; set; }
          
        
        public string Comments { get; set; }

        public int TicketStatus { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public enum StatusType : int
        {
            Unknown = -1,
            NotProcessed = 0,
            Processed = 1,
            Deleted = 2,
            Sent = 3
        }

        private static DriverOffenceAudit GetDriverOffenceAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var driverOffence = new DriverOffenceAudit();
            driverOffence.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Identifier") && reader["Identifier"] != DBNull.Value)
                driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));

            if (columns.Contains("OffenceDate") && reader["OffenceDate"] != DBNull.Value)
                driverOffence.OffenceDate = reader["OffenceDate"].ToString();

            if (columns.Contains("OffenceTime") && reader["OffenceTime"] != DBNull.Value)
                driverOffence.OffenceTime = reader["OffenceTime"].ToString();

            if (columns.Contains("PlateNumber") && reader["PlateNumber"] != DBNull.Value)
                driverOffence.PlateNumber = reader["PlateNumber"].ToString();

            if (columns.Contains("PlateSource") && reader["PlateSource"] != DBNull.Value)
                driverOffence.PlateSource = reader["PlateSource"].ToString();

            if (columns.Contains("PlateCode") && reader["PlateCode"] != DBNull.Value)
                driverOffence.PlateCode = reader["PlateCode"].ToString();

            if (columns.Contains("VehicleMake") && reader["VehicleMake"] != DBNull.Value)
                driverOffence.VehicleMake = reader["VehicleMake"].ToString();

            if (columns.Contains("VehicleColor") && reader["VehicleColor"] != DBNull.Value)
                driverOffence.VehicleColor = reader["VehicleColor"].ToString();

            if (columns.Contains("VehicleType") && reader["VehicleType"] != DBNull.Value)
                driverOffence.VehicleType = reader["VehicleType"].ToString();

            if (columns.Contains("LocationType") && reader["LocationType"] != DBNull.Value)
                driverOffence.LocationType = reader["LocationType"].ToString();

            if (columns.Contains("Location") && reader["Location"] != DBNull.Value)
                driverOffence.Location = reader["Location"].ToString();

            if (columns.Contains("OffenceType") && reader["OffenceType"] != DBNull.Value)
                driverOffence.OffenceType = reader["OffenceType"].ToString();

            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());

            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());

            if (columns.Contains("Image1Path") && reader["Image1Path"] != DBNull.Value)
                driverOffence.Image1Path = reader["Image1Path"].ToString();

            if (columns.Contains("Image2Path") && reader["Image2Path"] != DBNull.Value)
                driverOffence.Image2Path = reader["Image2Path"].ToString();

            if (columns.Contains("Image3Path") && reader["Image3Path"] != DBNull.Value)
                driverOffence.Image3Path = reader["Image3Path"].ToString();

            if (columns.Contains("Image4Path") && reader["Image4Path"] != DBNull.Value)
                driverOffence.Image4Path = reader["Image4Path"].ToString();

            if (columns.Contains("Image5Path") && reader["Image5Path"] != DBNull.Value)
                driverOffence.Image5Path = reader["Image5Path"].ToString();

            if (columns.Contains("Image6Path") && reader["Image6Path"] != DBNull.Value)
                driverOffence.Image6Path = reader["Image6Path"].ToString();

            if (columns.Contains("OffenceImagePath1") && reader["OffenceImagePath1"] != DBNull.Value)
                driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();

            if (columns.Contains("VideoPath1") && reader["VideoPath1"] != DBNull.Value)
                driverOffence.VideoPath1 = reader["VideoPath1"].ToString();

            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("Username") && reader["Username"] != DBNull.Value)
                driverOffence.UserName = reader["UserName"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
           

            return driverOffence;
        }

        public static List<DriverOffenceAudit> GetAllDriverOffenceAudit(string dbConnection)
        {

            var driverOffences = new List<DriverOffenceAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var driverOffence = GetDriverOffenceAuditFromSqlReader(reader);                    
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static bool InsertDriverOffenceAudit(DriverOffenceAudit driverOffence, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(driverOffence.BaseAudit, dbConnection);

            if (driverOffence != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertDriverOffenceAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Identifier", SqlDbType.UniqueIdentifier).Value = driverOffence.Identifier;

                    cmd.Parameters.Add("@OffenceDate", SqlDbType.NVarChar).Value = driverOffence.OffenceDate;

                    cmd.Parameters.Add("@OffenceTime", SqlDbType.NVarChar).Value = driverOffence.OffenceTime;

                    cmd.Parameters.Add("@PlateNumber", SqlDbType.NVarChar).Value = driverOffence.PlateNumber;

                    cmd.Parameters.Add("@PlateSource", SqlDbType.NVarChar).Value = driverOffence.PlateSource;

                    cmd.Parameters.Add("@PlateCode", SqlDbType.NVarChar).Value = driverOffence.PlateCode;

                    cmd.Parameters.Add("@VehicleMake", SqlDbType.NVarChar).Value = driverOffence.VehicleMake;

                    cmd.Parameters.Add("@VehicleColor", SqlDbType.NVarChar).Value = driverOffence.VehicleColor;

                    cmd.Parameters.Add("@VehicleType", SqlDbType.VarChar).Value = driverOffence.VehicleType;

                    cmd.Parameters.Add("@LocationType", SqlDbType.NVarChar).Value = driverOffence.LocationType;

                    cmd.Parameters.Add("@Location", SqlDbType.NVarChar).Value = driverOffence.Location;

                    cmd.Parameters.Add("@OffenceType", SqlDbType.NVarChar).Value = driverOffence.OffenceType;

                    cmd.Parameters.Add("@Offence1", SqlDbType.NVarChar).Value = driverOffence.Offence1;

                    cmd.Parameters.Add("@Offence2", SqlDbType.NVarChar).Value = driverOffence.Offence2;

                    cmd.Parameters.Add("@Offence3", SqlDbType.NVarChar).Value = driverOffence.Offence3;

                    cmd.Parameters.Add("@Offence4", SqlDbType.NVarChar).Value = driverOffence.Offence4;

                    cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = driverOffence.UserName;

                    cmd.Parameters.Add("@Longitude", SqlDbType.Float).Value = driverOffence.Longitude;

                    cmd.Parameters.Add("@Latitude", SqlDbType.Float).Value = driverOffence.Latitude;

                    cmd.Parameters.Add("@Image1Path", SqlDbType.NVarChar).Value = driverOffence.Image1Path;

                    cmd.Parameters.Add("@Image2Path", SqlDbType.NVarChar).Value = driverOffence.Image2Path;

                    cmd.Parameters.Add("@Image3Path", SqlDbType.NVarChar).Value = driverOffence.Image3Path;

                    cmd.Parameters.Add("@Image4Path", SqlDbType.NVarChar).Value = driverOffence.Image4Path;

                    cmd.Parameters.Add("@Image5Path", SqlDbType.NVarChar).Value = driverOffence.Image5Path;

                    cmd.Parameters.Add("@Image6Path", SqlDbType.NVarChar).Value = driverOffence.Image6Path;

                    cmd.Parameters.Add("@OffenceImagePath1", SqlDbType.NVarChar).Value = driverOffence.OffenceImagePath1;

                    cmd.Parameters.Add("@VideoPath1", SqlDbType.NVarChar).Value = driverOffence.VideoPath1;

                    cmd.Parameters.Add("@ContractId", SqlDbType.Int).Value = driverOffence.ContractId;
                    cmd.Parameters.Add("@CusId", SqlDbType.Int).Value = driverOffence.CusId;

                     


                    cmd.Parameters.Add(new SqlParameter("@OffenceCategory", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceCategory"].Value = driverOffence.OffenceCategory;
                    

                    if (!string.IsNullOrEmpty(driverOffence.Comments))
                    {
                        cmd.Parameters.Add("@Comments", SqlDbType.NVarChar).Value = driverOffence.Comments;
                    }
                    if (driverOffence.ImagePath1 != null)
                    {
                        cmd.Parameters.Add("@ImagePath1", SqlDbType.VarBinary).Value = driverOffence.ImagePath1;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add("@ImagePath1", SqlDbType.VarBinary).Value = ImageBytes(driverOffence.Image1Path);
                    }
                    if (driverOffence.ImagePath2 != null)
                    {
                        cmd.Parameters.Add("@ImagePath2", SqlDbType.VarBinary).Value = driverOffence.ImagePath2;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add("@ImagePath2", SqlDbType.VarBinary).Value = driverOffence.ImagePath2;
                    }
                    if (driverOffence.ImagePath3 != null)
                    {
                        cmd.Parameters.Add("@ImagePath3", SqlDbType.VarBinary).Value = driverOffence.ImagePath3;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add("@ImagePath3", SqlDbType.VarBinary).Value = ImageBytes(driverOffence.Image3Path);
                    }
                    if (driverOffence.ImagePath4 != null)
                    {
                        cmd.Parameters.Add("@ImagePath4", SqlDbType.VarBinary).Value = driverOffence.ImagePath4;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add("@ImagePath4", SqlDbType.VarBinary).Value = ImageBytes(driverOffence.Image4Path);
                    }
                    if (driverOffence.ImagePath5 != null)
                    {
                        cmd.Parameters.Add("@ImagePath5", SqlDbType.VarBinary).Value = driverOffence.ImagePath5;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add("@ImagePath5", SqlDbType.VarBinary).Value = ImageBytes(driverOffence.Image5Path);
                    }
                    if (driverOffence.ImagePath6 != null)
                    {
                        cmd.Parameters.Add("@ImagePath6", SqlDbType.VarBinary).Value = driverOffence.ImagePath6;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add("@ImagePath6", SqlDbType.VarBinary).Value = ImageBytes(driverOffence.Image6Path);
                    }
                    if (driverOffence.OffenceImagePath != null)
                    {
                        cmd.Parameters.Add("@OffenceImagePath", SqlDbType.VarBinary).Value = driverOffence.OffenceImagePath;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add("@OffenceImagePath", SqlDbType.VarBinary).Value = ImageBytes(driverOffence.OffenceImagePath1);
                    }
                    if (driverOffence.VideoPath != null)
                    {
                        cmd.Parameters.Add("@VideoPath", SqlDbType.VarBinary).Value = driverOffence.VideoPath;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add("@VideoPath", SqlDbType.VarBinary).Value = ImageBytes(driverOffence.VideoPath1);
                    }
                   
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = driverOffence.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = driverOffence.CustomerId;
                    
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = driverOffence.CreatedDate;

                    cmd.Parameters.Add("@Status", SqlDbType.Int).Value = driverOffence.Status;

                    cmd.Parameters.Add("@TicketStatus", SqlDbType.Int).Value = driverOffence.TicketStatus;

                    cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = driverOffence.StartDate;
                    cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = driverOffence.EndDate;

                    cmd.Parameters.Add("@EndBy", SqlDbType.NVarChar).Value = driverOffence.EndBy;

                    cmd.Parameters.Add("@StartBy", SqlDbType.NVarChar).Value = driverOffence.StartBy;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static byte[] ImageBytes(string imagePath)
        {
            if (string.IsNullOrEmpty(imagePath))
                return null;
            using (FileStream FS = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
            {
                byte[] img = new byte[FS.Length];
                FS.Read(img, 0, Convert.ToInt32(FS.Length));
                return img;
            }
        }
    }
}
