﻿using Arrowlabs.Business.Layer;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure;


namespace ArrowLabs.Licence.Portal.Helpers
{
    public class ITextEvents : PdfPageEventHelper
    {

        // This is the contentbyte object of the writer
        PdfContentByte cb;

        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate, autoTemplate,rectTemplate;

        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;

        // This keeps track of the creation time
        DateTime PrintTime = DateTime.Now;

        public int cid { get; set; }

        #region Fields
        private string _header;
        #endregion

        #region Properties
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        #endregion


        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                
                cb = writer.DirectContent;

                rectTemplate = cb.CreateTemplate(document.PageSize.Width, 24);
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(100, 100);
                autoTemplate = cb.CreateTemplate(180, 120);
 

            }
            catch (DocumentException de)
            {

            }
            catch (System.IO.IOException ioe)
            {

            }
        }

        //public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        //{
        //    base.OnEndPage(writer, document);

        //    //iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

        //    //iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

        //    BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        //    iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(f_cn, 16, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.LIGHT_GRAY);
        //    iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(f_cn, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.GRAY);
        //    Phrase p1Header = new Phrase("CONFIDENTIAL", baseFontNormal);

        //    //Create PdfTable object
        //    PdfPTable pdfTab = new PdfPTable(3);

        //    //We will have to create separate cells to include image logo and 2 separate strings
        //    //Row 1
        //    PdfPCell pdfCell1 = new PdfPCell();
        //    PdfPCell pdfCell2 = new PdfPCell(p1Header);
        //    PdfPCell pdfCell3 = new PdfPCell();
        //    //String text = "Page " + writer.PageNumber + " of ";


        //    //Add paging to header
        //    {
        //        //cb.BeginText();
        //        //cb.SetFontAndSize(bf, 12);
        //        //cb.SetTextMatrix(document.PageSize.GetRight(200), document.PageSize.GetTop(45));
        //        //cb.ShowText(text);
        //        //cb.EndText();
        //        //float len = bf.GetWidthPoint(text, 12);
        //        ////Adds "12" in Page 1 of 12
        //        //cb.AddTemplate(headerTemplate, document.PageSize.GetRight(200) + len, document.PageSize.GetTop(45));
        //    }
        //    //Add paging to footer
        //    {
        //        //cb.BeginText();
        //        //cb.SetFontAndSize(f_cn, 16);
        //        //cb.SetTextMatrix(0, 0);
        //        ////cb.ShowText("CONFIDENTIAL");
        //        //cb.SetColorFill(iTextSharp.text.Color.LIGHT_GRAY);
        //        //cb.EndText();
        //        //float len = bf.GetWidthPoint("CONFIDENTIAL", 12);
        //        //cb.AddTemplate(footerTemplate, 260, document.PageSize.GetBottom(30));

        //        int pageN = writer.PageNumber;
        //        String text = "PAGE " + pageN + " OF ";
        //        float len = f_cn.GetWidthPoint(text, 8);
        //        //cb.SetRGBColorFill(100, 100, 100);

        //        cb.BeginText();
        //        cb.SetFontAndSize(f_cn, 8);
        //        cb.SetColorFill(iTextSharp.text.Color.LIGHT_GRAY);
        //        cb.SetTextMatrix(document.PageSize.GetRight(60), document.PageSize.GetBottom(10));
        //        cb.ShowText(text);
        //        cb.EndText();

        //        cb.AddTemplate(headerTemplate, document.PageSize.GetRight(60) + len, document.PageSize.GetBottom(10));
        //        cb.AddTemplate(footerTemplate, 300, document.PageSize.GetBottom(10));
        //        cb.AddTemplate(autoTemplate, document.PageSize.GetLeft(10), document.PageSize.GetBottom(10));
        //    }
        //    //Row 2
        //    //PdfPCell pdfCell4 = new PdfPCell(new Phrase("Sub Header Description", baseFontNormal));
        //    //Row 3


        //    //PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
        //    //PdfPCell pdfCell6 = new PdfPCell();
        //    //PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));


        //    //set the alignment of all three cells and set border to 0
        //    pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
        //    pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
        //    pdfCell3.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;


        //    //pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
        //    //pdfCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    //pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
        //    //pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    //pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    //pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;


        //    //pdfCell4.Colspan = 3;



        //    pdfCell1.Border = 0;
        //    pdfCell2.Border = 0;
        //    pdfCell3.Border = 0;
        //    //pdfCell4.Border = 0;
        //    //pdfCell5.Border = 0;
        //    //pdfCell6.Border = 0;
        //    //pdfCell7.Border = 0;


        //    //add all three cells into PdfTable
        //    pdfTab.AddCell(pdfCell1);
        //    pdfTab.AddCell(pdfCell2);
        //    pdfTab.AddCell(pdfCell3);
        //    //pdfTab.AddCell(pdfCell4);
        //    //pdfTab.AddCell(pdfCell5);
        //    //pdfTab.AddCell(pdfCell6);
        //    //pdfTab.AddCell(pdfCell7);

        //    pdfTab.TotalWidth = document.PageSize.Width - 80f;
        //    pdfTab.WidthPercentage = 70;
        //    //pdfTab.HorizontalAlignment = Element.ALIGN_CENTER;


        //    //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
        //    //first param is start row. -1 indicates there is no end row and all the rows to be included to write
        //    //Third and fourth param is x and y position to start writing
        //    pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
        //    //set pdfContent value

        //    //Move the pointer and draw line to separate header section from rest of page
        //    //cb.MoveTo(40, document.PageSize.Height - 100);
        //    //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
        //    //cb.Stroke();

        //    //Move the pointer and draw line to separate footer section from rest of page
        //    //cb.MoveTo(40, document.PageSize.GetBottom(50));
        //    //cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
        //    //cb.Stroke();
        //}


        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnEndPage(writer, document);

            //iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

            //iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

            iTextSharp.text.Color color = new iTextSharp.text.Color(162, 0, 46);
            BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(f_cn, 16, iTextSharp.text.Font.ITALIC, color);
            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(f_cn, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.GRAY);
            Phrase p1Header = new Phrase("The Workforce Platform", baseFontNormal);

            string strLogoPath2 = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";

            if (cid == 87 || cid == 45)
            {
                strLogoPath2 = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\g4s.png";
            }

            iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(strLogoPath2);
            //img1.SetAbsolutePosition(doc.GetLeft(10), doc.GetTop(10));
            img1.ScalePercent(50f);
            img1.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            //Create PdfTable object
            PdfPTable pdfTab = new PdfPTable(3);
            //We will have to create separate cells to include image logo and 2 separate strings
            //Row 1
            PdfPCell pdfCell1 = new PdfPCell(img1);
            PdfPCell pdfCell2 = new PdfPCell();
            PdfPCell pdfCell3 = new PdfPCell(p1Header);

            //String text = "Page " + writer.PageNumber + " of ";


            //Add paging to header
            {
                //cb.BeginText();
                //cb.SetFontAndSize(bf, 12);
                //cb.SetTextMatrix(document.PageSize.GetRight(200), document.PageSize.GetTop(45));
                //cb.ShowText(text);
                //cb.EndText();
                //float len = bf.GetWidthPoint(text, 12);
                ////Adds "12" in Page 1 of 12
                //cb.AddTemplate(headerTemplate, document.PageSize.GetRight(200) + len, document.PageSize.GetTop(45));
            }
            //Add paging to footer
            {
                //cb.BeginText();
                //cb.SetFontAndSize(f_cn, 16);
                //cb.SetTextMatrix(0, 0);
                ////cb.ShowText("CONFIDENTIAL");
                //cb.SetColorFill(iTextSharp.text.Color.LIGHT_GRAY);
                //cb.EndText();
                //float len = bf.GetWidthPoint("CONFIDENTIAL", 12);
                //cb.AddTemplate(footerTemplate, 260, document.PageSize.GetBottom(30));

                cb.AddTemplate(rectTemplate, document.PageSize.GetLeft(0), document.PageSize.GetBottom(0));

                int pageN = writer.PageNumber;
                String text = "PAGE " + pageN + " OF ";
                float len = f_cn.GetWidthPoint(text, 8);
                //cb.SetRGBColorFill(100, 100, 100);

                cb.BeginText();
                cb.SetFontAndSize(f_cn, 8);
                cb.SetColorFill(iTextSharp.text.Color.WHITE);
                cb.SetTextMatrix(document.PageSize.GetRight(60), document.PageSize.GetBottom(10));

                //cb.Rectangle(document.PageSize.GetLeft(0), document.PageSize.GetBottom(0), document.PageSize.Width, 24);
                //cb.SetColorFill(color);
                //cb.EoFill();  

                cb.ShowText(text);
                cb.EndText();

     
                cb.AddTemplate(headerTemplate, document.PageSize.GetRight(60) + len, document.PageSize.GetBottom(10));
                cb.AddTemplate(footerTemplate, 300, document.PageSize.GetBottom(10));
                cb.AddTemplate(autoTemplate, document.PageSize.GetLeft(10), document.PageSize.GetBottom(10));
            }
            //Row 2
            //PdfPCell pdfCell4 = new PdfPCell(new Phrase("Sub Header Description", baseFontNormal));
            //Row 3


            //PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
            //PdfPCell pdfCell6 = new PdfPCell();
            //PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));


            //set the alignment of all three cells and set border to 0
            pdfCell1.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell3.HorizontalAlignment = Element.ALIGN_RIGHT;
            //pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
            //pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
            //pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
            //pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;


            //pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
            //pdfCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
            //pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
            //pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
            //pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
            //pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;


            //pdfCell4.Colspan = 3;



            pdfCell1.Border = 0;
            pdfCell2.Border = 0;
            pdfCell3.Border = 0;
            //pdfCell4.Border = 0;
            //pdfCell5.Border = 0;
            //pdfCell6.Border = 0;
            //pdfCell7.Border = 0;


            //add all three cells into PdfTable
            pdfTab.AddCell(pdfCell1);
            pdfTab.AddCell(pdfCell2);
            pdfTab.AddCell(pdfCell3);
            //pdfTab.AddCell(pdfCell4);
            //pdfTab.AddCell(pdfCell5);
            //pdfTab.AddCell(pdfCell6);
            //pdfTab.AddCell(pdfCell7);

            pdfTab.TotalWidth = document.PageSize.Width - 80f;
            pdfTab.WidthPercentage = 70;
            //pdfTab.HorizontalAlignment = Element.ALIGN_CENTER;


            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
            //first param is start row. -1 indicates there is no end row and all the rows to be included to write
            //Third and fourth param is x and y position to start writing
            pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 15, writer.DirectContent);
            //set pdfContent value

            //Move the pointer and draw line to separate header section from rest of page
            cb.MoveTo(40, document.PageSize.Height - 55);
            cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 55);
            cb.Stroke();

            //Move the pointer and draw line to separate footer section from rest of page
            //cb.MoveTo(40, document.PageSize.GetBottom(50));
            //cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            //cb.Stroke();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            //headerTemplate.BeginText();
            //headerTemplate.SetFontAndSize(bf, 12);
            //headerTemplate.SetTextMatrix(0, 0);
            //headerTemplate.ShowText((writer.PageNumber - 1).ToString());
            //headerTemplate.EndText();
            //BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
           
            //footerTemplate.BeginText();
            //footerTemplate.SetFontAndSize(f_cn, 16);
            ////footerTemplate.SetTextMatrix(0, 0);
            //footerTemplate.ShowText("CONFIDENTIAL");//(writer.PageNumber - 1).ToString());
            //footerTemplate.EndText();

            BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

 
            //headerTemplate.SetRGBColorFill(162, 0, 46);  
            //headerTemplate.Rectangle(0, 0, 1000, 10);
            //headerTemplate.Fill();

            //headerTemplate.SetRGBColorFill(162, 0, 46); 
            //headerTemplate.Rectangle(40, document.PageSize.Height - 65, document.PageSize.Width - 80f, 10);
            //headerTemplate.Fill();

             
            rectTemplate.SetRGBColorFill(162, 0, 46);
            rectTemplate.Rectangle(document.PageSize.GetLeft(0), document.PageSize.GetBottom(0), document.PageSize.Width, 24);
            rectTemplate.Fill();

            headerTemplate.BeginText();
            headerTemplate.SetFontAndSize(f_cn, 8);
            headerTemplate.SetTextMatrix(0, 0);
            headerTemplate.SetColorFill(iTextSharp.text.Color.WHITE);
            headerTemplate.ShowText((writer.PageNumber - 1).ToString());
            headerTemplate.EndText();

           

            autoTemplate.BeginText();
            autoTemplate.SetFontAndSize(f_cn, 8);
            //footerTemplate.SetTextMatrix(0, 0);
            autoTemplate.SetColorFill(iTextSharp.text.Color.WHITE);
            autoTemplate.ShowText("AUTOMATICALLY GENERATED REPORT");//(writer.PageNumber - 1).ToString());
            //headerTemplate.SetRGBColorFill(162, 0, 46);  
            //headerTemplate.Rectangle(0, 0, 1000, 10);
            //headerTemplate.Fill();
            autoTemplate.EndText();

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(f_cn, 8);
            footerTemplate.SetColorFill(iTextSharp.text.Color.WHITE);
            //footerTemplate.SetTextMatrix(0, 0);
            footerTemplate.ShowText("CONFIDENTIAL");//(writer.PageNumber - 1).ToString());
            footerTemplate.EndText();

        }
    }
        public class ReverseGeocode
    {

        public string status { get; set; }
        public results[] results { get; set; }
        static string baseUri = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&key=" + CommonUtility.googlekeyapi;
        static string baseUri2 = "https://maps.googleapis.com/maps/api/geocode/json?address={0}&key=" + CommonUtility.googlekeyapi;
        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string RetrieveFormatedAddress(string lat, string lng, ClientLicence getClientLic)
        {
            var stringresponse = string.Empty;
            try
            {
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        string requestUri = string.Format(baseUri, lat, lng);

                        using (WebClient wc = new WebClient())
                        {
                            var result = new System.Net.WebClient().DownloadString(requestUri);
                            ReverseGeocode response = JsonConvert.DeserializeObject<ReverseGeocode>(result);
                            if (response.status.ToLower() == "ok")
                            {
                                stringresponse = RemoveSpecialCharacters(response.results[1].formatted_address);
                            }
                            else
                            {
                                MIMSLog.MIMSLogSave("RetrieveFormatedAddress", "GOOGLEMAPS API-"+response.status, new Exception(), CommonUtility.dbConnection);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "RetrieveFormatedAddress", ex, CommonUtility.dbConnection);
                return stringresponse;
               
            }
            return stringresponse;
        }
        public static List<string> RetrieveFormatedGeo(string address)
        {
            var stringresponse = new List<string>();
            try
            {
                
                string requestUri = string.Format(baseUri2, address);

                using (WebClient wc = new WebClient())
                {
                    var result = new System.Net.WebClient().DownloadString(requestUri);
                    ReverseGeocode response = JsonConvert.DeserializeObject<ReverseGeocode>(result);
                    if (response.status.ToLower() == "ok")
                    {
                        //stringresponse = RemoveSpecialCharacters(response.results[1].formatted_address);
                        stringresponse.Add(response.results[0].geometry.location.lat);
                        stringresponse.Add(response.results[0].geometry.location.lng);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("RetrieveFormatedAddress", "GOOGLEMAPS API-" + response.status, new Exception(), CommonUtility.dbConnection);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "RetrieveFormatedAddress", ex, CommonUtility.dbConnection);
                return stringresponse;

            }
            return stringresponse;
        }
    }

    public class results
    {
        public string formatted_address { get; set; }
        public geometry geometry { get; set; }
        public string[] types { get; set; }
        public address_component[] address_components { get; set; }
    }

    public class geometry
    {
        public string location_type { get; set; }
        public location location { get; set; }
    }

    public class location
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class address_component
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }
    public class ImageInfo
    {
        public string datastring;
        public int index;
    }
    public class SearchSelectItem
    {
        public string ID { get; set; }
        public string Name { get; set; }  
    }
    public class eHistoryDisplay
    {
        public string type { get; set; }
        public int id { get; set; }
        public string name { get; set; }

        public DateTime date { get; set; }

        public string actioninfo { get; set; }

        public string incidentLocation { get; set; }

        public string username { get; set; }

    }
    public class CommonUtility
    {
        public static bool IsPointInPolygon(List<Arrowlabs.Business.Layer.GeofenceLocation> poly, double lati, double longi)
        {
            int i, j;
            bool c = false;
            for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {
                if ((((poly[i].Latitude <= lati) && (lati < poly[j].Latitude))
                        || ((poly[j].Latitude <= lati) && (lati < poly[i].Latitude)))
                        && (longi < (poly[j].Longitude - poly[i].Longitude) * (lati - poly[i].Latitude)
                            / (poly[j].Latitude - poly[i].Latitude) + poly[i].Longitude))

                    c = !c;
            }

            return c;
        }

        public static List<string> FileExtensions = new List<string> { ".PDF", ".DOCX", ".DOC" };
        public static List<string> VideoExtensions = new List<string> { ".AVI", ".MP4", ".MKV", ".FLV" };
        public static int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public static string getUserDeviceType2(int dtype, string fontstyle, string monitor, bool iswatch)
        {
            var retString = string.Empty;
            if (dtype == (int)Users.DeviceTypes.Watch)
            {
                if (fontstyle == "style='color:lime;'")
                    retString = "<img style='width: 28px;height: 28px;' src='../Images/onlinesmart.png'>" + monitor;
                else
                    retString = "<img style='width: 28px;height: 28px;' src='../Images/smart.png'>" + monitor;
            }
            else if (dtype == (int)Users.DeviceTypes.Client)
            {
                retString = "<i " + fontstyle + " class='fa fa-tablet fa-2x mr-2x'></i>" + monitor;
            }
            else if (dtype == (int)Users.DeviceTypes.Mobile)
            {
                retString = "<i " + fontstyle + " class='fa fa-mobile fa-2x mr-2x'></i>" + monitor;
            }
            else if (dtype == (int)Users.DeviceTypes.MobileAndClient)
            {
                retString = "<i class='fa fa-mobile fa-2x mr-2x'></i><i class='fa fa-tablet fa-2x'></i>" + monitor;
            }
            else if (dtype == (int)Users.DeviceTypes.MobileAndWatch)
            {
                if (fontstyle == "style='color:lime;'")
                {
                    if (iswatch)
                        retString = "<i class='fa fa-mobile fa-2x mr-2x'></i><img style='width: 28px;height: 28px;margin-top: -13px;' src='/Images/onlinesmart.png'>" + monitor;
                    else
                        retString = "<i " + fontstyle + " class='fa fa-mobile fa-2x mr-2x'></i><img style='width: 28px;height: 28px;margin-top: -13px;' src='/Images/smart.png'>" + monitor;
                }
                else
                    retString = "<i class='fa fa-mobile fa-2x mr-2x'></i><img style='width: 28px;height: 28px;margin-top: -13px;' src='/Images/smart.png'>" + monitor;

            }
            else
                retString = monitor;

            return retString;
            //</i><i class='fa fa-tablet fa-2x mr-2x'></i><i class='fa fa-television fa-2x'></i>
        }



        public static bool ServiceCheckStatus()
        {
            System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("ArrowLabs MIMS Notification");

            switch (sc.Status)
            {
                case System.ServiceProcess.ServiceControllerStatus.Running:
                    return true;
                case System.ServiceProcess.ServiceControllerStatus.Stopped:
                    return false;
                case System.ServiceProcess.ServiceControllerStatus.Paused:
                    return false;
                case System.ServiceProcess.ServiceControllerStatus.StopPending:
                    return false;
                case System.ServiceProcess.ServiceControllerStatus.StartPending:
                    return false;
                default:
                    return false;
            }
        }
        public static void LogoutUser(Users customData, string sessionid, string ip)
        {
            LoginSession.LogoutUserByUsernameAndSessionId(customData.Username,System.Web.HttpContext.Current.Session.SessionID, dbConnection);

            var loginHistory = new LoginSession();
            loginHistory.IPAddress = ip;
            loginHistory.LoginDate = DateTime.Now;
            loginHistory.LogoutDate = DateTime.Now;
            loginHistory.MacAddress = customData.Username;
            loginHistory.SiteId = customData.SiteId;
            loginHistory.CustomerId = customData.CustomerInfoId;
            loginHistory.LoggedIn = false;
            loginHistory.PCName = "Successful logout";
            loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
            LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, dbConnectionAudit, true);
        }
        public static string getAttachmentDisplayName(string path,int count)
        {
            Guid guidOutput = new Guid();
            bool isValid = Guid.TryParse(System.IO.Path.GetFileNameWithoutExtension(path), out guidOutput);

            var attachmentName = "Attachment";// +count;

            if (!isValid)
            {
                var fname = System.IO.Path.GetFileNameWithoutExtension(path);
                var split = fname.Split('-');
                if (split.Length > 1)
                {
                    fname = split[1];
                }
                attachmentName = fname;
            }

            return attachmentName;
        }
        public static string GetCurrentUserSession(ClaimsIdentity identity)
        {
            IEnumerable<Claim> claims = identity.Claims;
            var sessionId = string.Empty;
            // Access claims
            foreach (Claim claim in claims)
            {
                if (claim.Type.Equals(ClaimTypes.Hash))
                {
                    sessionId = claim.Value;
                }
            }
            return sessionId;
        }
        public static bool HandleConcurrentSession(string username, string sessionId)
        {

            var loginDetails = LoginSession.GetLoginStatusByUsernameAndSessionId(username,
                sessionId, CommonUtility.dbConnection);

            if (loginDetails.LoggedIn)
            {
                var loginDetailSomeWhereElseLogin = LoginSession.GetConcurrentLoginByUsernameAndSessionId(username,
                sessionId, CommonUtility.dbConnection);

                // check to see if your user ID is being used elsewhere under a different session ID
                if (!loginDetailSomeWhereElseLogin.LoggedIn)
                {
                    //var pushNotificationDevice = PushNotificationDevice.GetPushNotificationDeviceByUsername(username,
                    //dbConnection);
                    //if (!String.IsNullOrEmpty(pushNotificationDevice.Username))
                    //{
                    //    pushNotificationDevice.IsServerPortal = false;
                    //}
                    ////PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushNotificationDevice,
                    ////AppConstant.ConnectionString,AppConstant.AuditConnectionString,true);
                    var getUser = Users.GetUserByName(username, CommonUtility.dbConnection);
                    //pushNotificationDevice.SiteId = getUser.SiteId;
                    //PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushNotificationDevice, CommonUtility.dbConnection);
                    if (getUser != null)
                        PushNotificationDevice.UpdatePushNotificationDeviceByUsername(getUser.Username, getUser.SiteId, dbConnection, false);

                    return true;
                }
                else
                {
                    var getUser = Users.GetUserByName(username, CommonUtility.dbConnection);
                    var loginHistory = new LoginSession();
                    loginHistory.IPAddress = sessionId;
                    loginHistory.LoginDate = DateTime.Now;
                    loginHistory.LogoutDate = DateTime.Now;
                    loginHistory.MacAddress = username;
                    if (getUser != null)
                    {
                        loginHistory.SiteId = getUser.SiteId;
                        loginHistory.CustomerId = getUser.CustomerInfoId;
                    }
                    loginHistory.PCName = "New access from another session";
                    LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, dbConnectionAudit, true);
                    return false;
                }
            }
            else
            {
                var getUser = Users.GetUserByName(username, CommonUtility.dbConnection);
                var loginHistory = new LoginSession();
                loginHistory.IPAddress = sessionId;
                loginHistory.LoginDate = DateTime.Now;
                loginHistory.LogoutDate = DateTime.Now;
                loginHistory.MacAddress = username;
                if (getUser != null)
                {
                    loginHistory.SiteId = getUser.SiteId;
                    loginHistory.CustomerId = getUser.CustomerInfoId;
                }
                loginHistory.PCName = "New access from another session";
                LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, dbConnectionAudit, true);
                return false;
            }
        }
        public static string PhysicalPath(string path)
        {
            var mappedPath = HttpContext.Current.Server.MapPath(path);
            return mappedPath;
        }

        public static bool sendEmail(string emailaddress,string name,string title,string message,string subject,string emailtemp)
        {
            try
            {
                var body = System.Text.Encoding.Default.GetString(File.ReadAllBytes(System.IO.Path.Combine(System.Web.HttpRuntime.BinDirectory, emailtemp + ".html")));
                body = body.Replace("{UserName}", name); //replacing the required things  

                body = body.Replace("{Title}", title);

                body = body.Replace("{message}", message);

                System.Net.Mail.MailMessage objeto_mail = new System.Net.Mail.MailMessage();
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                client.Port = emailPort;//587;//webconfig
                client.EnableSsl = true;
                client.Host = emailHost;//"smtp.gmail.com";//webconfig
                client.Timeout = 100000;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = true;
                client.Credentials = new System.Net.NetworkCredential(emailUsr, emailPw);//webcongi
                objeto_mail.From = new System.Net.Mail.MailAddress(emailUsr);//webconfig 
                objeto_mail.CC.Add(new System.Net.Mail.MailAddress(emailUsr));
                objeto_mail.To.Add(new System.Net.Mail.MailAddress(emailaddress));
                objeto_mail.IsBodyHtml = true;
                objeto_mail.Subject = subject;
                objeto_mail.Body = body;

                client.Send(objeto_mail);

               // SystemLogger.SaveSystemLog(CommonUtility.dbConnection, "sendEmail", emailaddress, title, user, "Email send to " + emailaddress);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "sendEmail", ex, CommonUtility.dbConnection, 0);
                return false;
            }
            return true;
        }

        public static bool sendEmailNotification(string emailaddress, string uname, string type
            , string assignee, string compdate, string name, string number, string startdate
            , string status,Users user ,string startlocation = "", string endlocation = "")
        {
            try
            {
                var body = System.Text.Encoding.Default.GetString(File.ReadAllBytes(System.IO.Path.Combine(System.Web.HttpRuntime.BinDirectory, "Notification.html")));
                body = body.Replace("{UserName}", uname); //replacing the required things  

                body = body.Replace("{type}", type);

                body = body.Replace("{assignee}", assignee);

                body = body.Replace("{compdate}", compdate);

                body = body.Replace("{Name}", name);

                body = body.Replace("{Number}", number);

                body = body.Replace("{starttime}", startdate);

                body = body.Replace("{endtime}", compdate);

                body = body.Replace("{startlocation}", startlocation);

                body = body.Replace("{endlocation}", endlocation);

                body = body.Replace("{status}", status);
                
                System.Net.Mail.MailMessage objeto_mail = new System.Net.Mail.MailMessage();
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                client.Port = emailPort;//587;//webconfig
                client.EnableSsl = true;
                client.Host = emailHost;//"smtp.gmail.com";//webconfig
                client.Timeout = 100000;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = true;
                client.Credentials = new System.Net.NetworkCredential(emailNotUsr, emailNotPw);//webcongi
                objeto_mail.From = new System.Net.Mail.MailAddress(emailNotUsr);//webconfig 
                //objeto_mail.CC.Add(new System.Net.Mail.MailAddress(emailUsr));
                objeto_mail.To.Add(new System.Net.Mail.MailAddress(emailaddress));
                objeto_mail.IsBodyHtml = true;
                objeto_mail.Subject = "MIMS Notification";
                objeto_mail.Body = body;

                client.Send(objeto_mail);

                SystemLogger.SaveSystemLog(CommonUtility.dbConnectionAudit, "sendEmailNotification", emailaddress, type + "-" + number, user, "Email send to " + emailaddress);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "sendEmailNotification", ex, CommonUtility.dbConnection, 0);
                return false;
            }
            return true;
        }

        public static bool sendEmailConsumption(string emailaddress, string uname, string accountname, string cNumber
            , string thours, string cHours, string cDate, string cConsumption,Users user)
        {
            try
            {
                var body = System.Text.Encoding.Default.GetString(File.ReadAllBytes(System.IO.Path.Combine(System.Web.HttpRuntime.BinDirectory, "Consumption.html")));
                body = body.Replace("{UserName}", uname); //replacing the required things  

                body = body.Replace("{accountname}", accountname);

                body = body.Replace("{cNumber}", cNumber);

                body = body.Replace("{thours}", thours);

                body = body.Replace("{cHours}", cHours);

                body = body.Replace("{cDate}", cDate);

                body = body.Replace("{consumption}", cConsumption);

                System.Net.Mail.MailMessage objeto_mail = new System.Net.Mail.MailMessage();
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                client.Port = emailPort;//587;//webconfig
                client.EnableSsl = true;
                client.Host = emailHost;//"smtp.gmail.com";//webconfig
                client.Timeout = 100000;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = true;
                client.Credentials = new System.Net.NetworkCredential(emailNotUsr, emailNotPw);//webcongi
                objeto_mail.From = new System.Net.Mail.MailAddress(emailNotUsr);//webconfig 
                //objeto_mail.CC.Add(new System.Net.Mail.MailAddress(emailUsr));
                objeto_mail.To.Add(new System.Net.Mail.MailAddress(emailaddress));
                objeto_mail.IsBodyHtml = true;
                objeto_mail.Subject = "MIMS Contract Consumption Notification";
                objeto_mail.Body = body;

                client.Send(objeto_mail);

                SystemLogger.SaveSystemLog(CommonUtility.dbConnectionAudit, "sendEmailConsumption", emailaddress, accountname, user, "Email send to " + emailaddress);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "sendEmailNotification", ex, CommonUtility.dbConnection, 0);
                return false;
            }
            return true;
        }


        public static List<int> getContractConsumption(List<UserTask> gtasklist, ContractInfo customers)
        {
            var consumption = new List<int>();

            var contracthours = 0;
            if (customers.ContractType == (int)ContractInfo.ContractTypes.PPM)
            {
                contracthours = customers.TotalPPMHour;

                if (customers.SRTotal > 0)
                    contracthours = customers.SRTotal;
            }
            else
            {
                contracthours = customers.TotalPPMHour;
            }

            if (gtasklist.Count > 0)
            {
                gtasklist = gtasklist.Where(i => i.Status == (int)TaskStatus.Completed || i.Status == (int)TaskStatus.Accepted).ToList();

                if (gtasklist.Count > 0)
                {
                    var totaltime = 0.0;

                    foreach (var gtask in gtasklist)
                    {
                        if (gtask.ActualOnRouteDate != null && gtask.ActualEndDate != null)
                        {
                            var span = gtask.ActualEndDate.Value.Subtract(gtask.ActualOnRouteDate.Value);
                            totaltime = totaltime + span.TotalMinutes;
                        }
                        else if (gtask.ActualStartDate != null && gtask.ActualEndDate != null)
                        {
                            var span = gtask.ActualEndDate.Value.Subtract(gtask.ActualStartDate.Value);
                            totaltime = totaltime + span.TotalMinutes;
                        } 
                    }

                    var gConsumed = (int)Math.Round((float)totaltime / (float)60);
                    var r = (int)Math.Round((float)gConsumed / (float)contracthours * (float)100);
                    consumption.Add(r);
                    consumption.Add(gConsumed);
                    consumption.Add(contracthours);
                }

            }
            return consumption;
        }

        public static void sendPushNotificationAlarm(string[] userIds, string msg, string dbConnection)
        {
            foreach (var username in userIds)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    var isMobile = UserModules.GetAllModulesByUsername(username, dbConnection);
                    if (isMobile.Count > 0)
                    {
                        var isMobList = isMobile.Where(x => x.ModuleId == (int)Accounts.ModuleTypes.MobileAlarms);
                        if (isMobList != null)
                        {
                            var pushDevInfo = PushNotificationDevice.GetPushNotificationDeviceByUsername(username, dbConnection);
                            if (pushDevInfo.IsWatch)
                            {
                                Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(username, CommonUtility.watchpushApplicationID, CommonUtility.watchpushSenderId, pushDevInfo.DeviceToken, "Alarm", msg, dbConnection, (int)PushNotificationDevice.PushNotificationType.IncidentNotification));

                                thread.Start();
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Apple)
                                {
                                    Thread thread = new Thread(() => PushNotificationClient.SendtoAppleUserNotification("Alarm: " + msg, username, dbConnection));

                                    thread.Start();
                                }
                                else if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Android)
                                {
                                    Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(username, CommonUtility.androidpushApplicationID, CommonUtility.androidpushSenderId, pushDevInfo.DeviceToken, "Alarm", msg, dbConnection, (int)PushNotificationDevice.PushNotificationType.IncidentNotification));

                                    thread.Start();
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void sendPushNotificationAlarmMessage(string[] userIds, string msg, string dbConnection)
        {
            foreach (var username in userIds)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    var isMobile = UserModules.GetAllModulesByUsername(username, dbConnection);
                    if (isMobile.Count > 0)
                    {
                        var isMobList = isMobile.Where(x => x.ModuleId == (int)Accounts.ModuleTypes.MobileAlarms);
                        if (isMobList != null)
                        {
                            var pushDevInfo = PushNotificationDevice.GetPushNotificationDeviceByUsername(username, dbConnection);
                            if (pushDevInfo.IsWatch)
                            {
                                Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(username, CommonUtility.watchpushApplicationID, CommonUtility.watchpushSenderId, pushDevInfo.DeviceToken, "Notification", msg, dbConnection, (int)PushNotificationDevice.PushNotificationType.ChatMessage));

                                thread.Start();
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Apple)
                                {
                                    Thread thread = new Thread(() => PushNotificationClient.SendtoAppleUserNotification("Alarm: " + msg, username, dbConnection));

                                    thread.Start();
                                }
                                else if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Android)
                                {
                                    Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(username, CommonUtility.androidpushApplicationID, CommonUtility.androidpushSenderId, pushDevInfo.DeviceToken, "Alarm", msg, dbConnection, (int)PushNotificationDevice.PushNotificationType.IncidentNotification));

                                    thread.Start();
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void sendPushNotificationGroup(int groupid, string msg, string dbConnection,int pushtype = (int)PushNotificationDevice.PushNotificationType.TaskNotification)
        {
            var devicelist = PushNotificationDevice.GetAllPushNotificationDevicesByGroupId(groupid, dbConnection);
            foreach (var pushDevInfo in devicelist)
            {
                var sendPush = false;
                var isMobile = UserModules.GetAllModulesByUsername(pushDevInfo.Username, dbConnection);
                if (isMobile.Count > 0)
                {
                    if (msg.Contains("Task"))
                    {
                        var isMobList = isMobile.Where(x => x.ModuleId == (int)Accounts.ModuleTypes.MobileTasks);
                        if (isMobList != null)
                        {
                            sendPush = true;
                        }
                    }
                    else
                    {
                        var isMobList = isMobile.Where(x => x.ModuleId == (int)Accounts.ModuleTypes.MobileAlarms);
                        if (isMobList != null)
                        {
                            sendPush = true;
                        }
                    }
                    if (sendPush = true)
                    {
                        if (pushDevInfo.IsWatch)
                        {
                            Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(pushDevInfo.Username, CommonUtility.watchpushApplicationID, CommonUtility.watchpushSenderId, pushDevInfo.DeviceToken, "Task", msg, dbConnection, pushtype));

                            thread.Start();
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Apple)
                            {
                                Thread thread = new Thread(() => PushNotificationClient.SendtoAppleUserNotification(msg, pushDevInfo.Username, dbConnection));

                                thread.Start();
                            }
                            else if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Android)
                            {
                                Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(pushDevInfo.Username, CommonUtility.androidpushApplicationID, CommonUtility.androidpushSenderId, pushDevInfo.DeviceToken, "Task", msg, dbConnection, (int)PushNotificationDevice.PushNotificationType.TaskNotification));

                                thread.Start();
                            }
                        }
                    }
                }
            }
        }

        public static void sendPushNotificationMessage(string[] userIds, string msg, string dbConnection)
        {
            foreach (var username in userIds)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    var isMobile = UserModules.GetAllModulesByUsername(username, dbConnection);
                    if (isMobile.Count > 0)
                    {
                        var isMobList = isMobile.Where(x => x.ModuleId == (int)Accounts.ModuleTypes.MobileNotification);
                        if (isMobList != null)
                        {
                            var pushDevInfo = PushNotificationDevice.GetPushNotificationDeviceByUsername(username, dbConnection);
                            if (pushDevInfo.IsWatch)
                            {
                                Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(username, CommonUtility.watchpushApplicationID, CommonUtility.watchpushSenderId, pushDevInfo.DeviceToken, "Notification", msg, dbConnection, (int)PushNotificationDevice.PushNotificationType.ChatMessage));

                                thread.Start();
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Apple)
                                {
                                    Thread thread = new Thread(() => PushNotificationClient.SendtoAppleUserNotification("Notification: " + msg, username, dbConnection));

                                    thread.Start();
                                }
                                else if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Android)
                                {
                                    Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(username, CommonUtility.androidpushApplicationID, CommonUtility.androidpushSenderId, pushDevInfo.DeviceToken, "Notification", msg, dbConnection, (int)PushNotificationDevice.PushNotificationType.ChatMessage));

                                    thread.Start();
                                }
                            }
                        }
                    }
                }
            }
        }


        public static string CloudUploadFile(int sitename, string filename, Stream fileStream)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container.

                var containerName = sitename.ToString() + "blob";

                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(
        new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                // Retrieve reference to a blob named "myblob".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
                var type = string.Empty;
                string extension = Path.GetExtension(filename).ToUpper();
                if (FileExtensions.Contains(extension))
                {
                    
                }
                else if (VideoExtensions.Contains(extension))
                {
                    type = "video/mp4";
                }
                else if (extension.ToLower() == ".mp3")
                {
                    type = "audio/mp3";
                }
                else
                {
                    type = "image/jpeg";
                }
                blockBlob.Properties.ContentType = type;
                //blockBlob.UploadFromStream(fileStream);

                System.Threading.Tasks.Task.WaitAll(blockBlob.UploadFromStreamAsync(fileStream));

                return blockBlob.Uri.ToString();
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "CloudUploadFile", ex, dbConnection);
                return "FAIL";
            }
        }
        public static bool BlobExistsOnCloud(
   string containerName, string key)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            return blobClient.GetContainerReference(containerName)
                         .GetBlockBlobReference(key)
                         .Exists();
        }


        public static string CloudDeleteFile(string filename)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container.

                var splits = filename.Split('/');

                var fileExt = filename.Split('/').Last();

                var containerName = splits[splits.Length - 2];

                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();

                container.SetPermissions(
        new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileExt);

                blockBlob.Delete();

                return "TRUE";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "CloudDeleteFile", ex, dbConnection);
                return "FAIL";
            }
        }

        public static string CloudDeleteContainer(string containerName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container.
                CloudBlobContainer container =
                             blobClient.GetContainerReference(containerName);
                try
                {
                    //Fetches attributes of container
                    container.FetchAttributes();
                    //Console.WriteLine("Container exists..");
                    container.Delete();
                }
                catch (Exception ex)
                {

                    MIMSLog.MIMSLogSave("stroge contain", "Container does not exist", ex, dbConnection);
                //    return "FAIL";
                }

                return "TRUE";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "CloudDeleteFile", ex, dbConnection);
                return "FAIL";
            }
        }

        public static string watchpushApplicationID = ArrowLabs.Licence.Portal.Properties.Settings.Default.watchpushApplicationID;
        public static string watchpushSenderId = ArrowLabs.Licence.Portal.Properties.Settings.Default.watchpushSenderId;

        public static string androidpushApplicationID = ArrowLabs.Licence.Portal.Properties.Settings.Default.androidpushApplicationID;
        public static string androidpushSenderId = ArrowLabs.Licence.Portal.Properties.Settings.Default.androidpushSenderId;

        public static string googlekeyapi = ArrowLabs.Licence.Portal.Properties.Settings.Default.GoogleAPI;
        public static string milestonemobile = ArrowLabs.Licence.Portal.Properties.Settings.Default.MilestoneMobile;
        public static string milestoneport = ArrowLabs.Licence.Portal.Properties.Settings.Default.MilestonePort;
        public static string storageConnectionString = ArrowLabs.Licence.Portal.Properties.Settings.Default.StorageConnectionString;
        public static string dbConnection = WebConfigurationManager.ConnectionStrings["ArrowLabsPortal"].ConnectionString;
        public static string dbConnectionAudit = WebConfigurationManager.ConnectionStrings["ArrowLabsPortalAudit"].ConnectionString;

        public static string emailUsr = ArrowLabs.Licence.Portal.Properties.Settings.Default.EmailCredentialsUSR;
        public static string emailPw = ArrowLabs.Licence.Portal.Properties.Settings.Default.EmailCredentialsPW;

        public static string emailNotUsr = ArrowLabs.Licence.Portal.Properties.Settings.Default.EmailNotificationCredUSR;
        public static string emailNotPw = ArrowLabs.Licence.Portal.Properties.Settings.Default.EmailNotificationCredPW;

        public static int emailPort = ArrowLabs.Licence.Portal.Properties.Settings.Default.EmailPort;
        public static string emailHost = ArrowLabs.Licence.Portal.Properties.Settings.Default.EmailHost;

        public static string arrowlabsKey = "ARL";
        public static int tracebackreportmaxcount = 220;
        public static string sessionID { get; set; }
        public static List<string> GetDatesForRecurringTask(string selectedRecurrence, DateTime selectedTaskStartDateTime)
        {
            List<string> calculatedDates = null;


            if (selectedRecurrence.Equals("Daily"))
            {
                calculatedDates = new List<string>(AllDatesBetween(selectedTaskStartDateTime, new DateTime(DateTime.Now.Year, 12, 31)).Select(d => d.ToString("yyyy-MM-dd")));
            }
            else if (selectedRecurrence.Equals("Weekly"))
            {
                calculatedDates = new List<string>(AllWeekDatesBetween(selectedTaskStartDateTime, new DateTime(DateTime.Now.Year, 12, 31)).Select(d => d.ToString("yyyy-MM-dd")));
            }
            else if (selectedRecurrence.Equals("Monthly"))
            {
                calculatedDates = new List<string>(AllMonthDatesBetween(selectedTaskStartDateTime, new DateTime(DateTime.Now.Year, 12, 31)).Select(d => d.ToString("yyyy-MM-dd")));
            }
            return calculatedDates;
        }

        public static List<string> GetDatesForRecurringTaskSchedule(string selectedRecurrence, DateTime selectedTaskStartDateTime,DateTime endDate)
        {
            List<string> calculatedDates = null;

            if (selectedRecurrence.Equals("Daily"))
            {
                calculatedDates = new List<string>(AllDatesBetween(selectedTaskStartDateTime, endDate).Select(d => d.ToString("yyyy-MM-dd")));
            }
            else if (selectedRecurrence.Equals("Weekly"))
            {
                calculatedDates = new List<string>(AllWeekDatesBetween(selectedTaskStartDateTime, endDate).Select(d => d.ToString("yyyy-MM-dd")));
            }
            else if (selectedRecurrence.Equals("Monthly"))
            {
                calculatedDates = new List<string>(AllMonthDatesBetween(selectedTaskStartDateTime, endDate).Select(d => d.ToString("yyyy-MM-dd")));
            }

            //if (selectedRecurrence.Equals("Daily"))
            //{
            //    calculatedDates = new List<string>(AllDatesBetween(selectedTaskStartDateTime, new DateTime(DateTime.Now.Year, 12, 31)).Select(d => d.ToString("yyyy-MM-dd")));
            //}
            //else if (selectedRecurrence.Equals("Weekly"))
            //{
            //    calculatedDates = new List<string>(AllWeekDatesBetween(selectedTaskStartDateTime, new DateTime(DateTime.Now.Year, 12, 31)).Select(d => d.ToString("yyyy-MM-dd")));
            //}
            //else if (selectedRecurrence.Equals("Monthly"))
            //{
            //    calculatedDates = new List<string>(AllMonthDatesBetween(selectedTaskStartDateTime, new DateTime(DateTime.Now.Year, 12, 31)).Select(d => d.ToString("yyyy-MM-dd")));
            //}
            return calculatedDates;
        }
        public static IEnumerable<DateTime> AllDatesBetween(DateTime start, DateTime end)
        {
            for (var day = start.Date; day <= end; day = day.AddDays(1))
                yield return day;
        }
        public static IEnumerable<DateTime> AllWeekDatesBetween(DateTime start, DateTime end)
        {
            for (var day = start.Date; day <= end; day = day.AddDays(7))
                yield return day;
        }
        public static IEnumerable<DateTime> AllMonthDatesBetween(DateTime start, DateTime end)
        {
            for (var day = start.Date; day <= end; day = day.AddDays(30))
                yield return day;
        }
        public static List<eHistoryDisplay> getUserHistory(List<EventHistory> cusEvs, List<UserTask> taskLists,List<Verifier> verifier,List<LoginSession> loginhistory,ClientLicence getClientLic)
        {
            var eeList = new List<eHistoryDisplay>();

            var eCount = 5;
            var cusCount = 0;
            var taskCount = 0;
            var veriCount = 0;
            var loginCount = 0;
            foreach (var login in loginhistory)
            {
                if (loginCount == eCount)
                    break;
                var newEDisplay = new eHistoryDisplay();
                if (login.SessionId.ToLower() == "successful login" || login.SessionId.ToLower() == "successful logout")
                {
                    if (login.LoggedIn)
                    {

                        newEDisplay.date = login.LoginDate.Value;
                        newEDisplay.type = "Log";
                        newEDisplay.actioninfo = "Logged In";
                        newEDisplay.id = 0;
                        newEDisplay.username = login.CustomerUName;
                        newEDisplay.name = "";
                    }
                    else
                    {
                        newEDisplay.date = login.LoginDate.Value;
                        newEDisplay.type = "Log";
                        newEDisplay.actioninfo = "Logged Off";
                        newEDisplay.id = 0;
                        newEDisplay.username = login.CustomerUName;
                        newEDisplay.name = "";
                    }
                    eeList.Add(newEDisplay);
                    loginCount++;
                }
            }
            foreach(var cus in cusEvs)
            {
                if (cusCount == eCount)
                    break;
                var gethotevent = CustomEvent.GetCustomEventById(cus.EventId, dbConnection);
                if (gethotevent != null)
                {
                    if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                        if (mobEv != null)
                        {
                            if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                gethotevent.Name = "None";
                            else
                                gethotevent.Name = mobEv.EventTypeName;

                        }
                    }
                    else if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                    {
                        var reqEv = HotEvent.GetHotEventById(gethotevent.Identifier, dbConnection);
                        var splitName = reqEv.Name.Split('^');
                        if (splitName.Length > 1)
                        {
                            gethotevent.Name = splitName[1];
                        }
                    }
                    var newEDisplay = new eHistoryDisplay();
                    newEDisplay.date = cus.CreatedDate.Value;
                    if (gethotevent.EventType == CustomEvent.EventTypes.DriverOffence)
                    {
                        newEDisplay.type = "Tic";
                        newEDisplay.name = "Ticket";
                    }
                    else
                    {
                        newEDisplay.type = "Cus";
                        newEDisplay.name = gethotevent.Name;
                    }
                    newEDisplay.id = cus.EventId;
                    newEDisplay.username = cus.ACustomerUName;
                    if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        if (!string.IsNullOrEmpty(gethotevent.Latitude) && !string.IsNullOrEmpty(gethotevent.Longtitude))
                            newEDisplay.incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(gethotevent.Latitude.ToString(), gethotevent.Longtitude.ToString(), getClientLic);

                        newEDisplay.actioninfo = "created";

                        if (cus.CustomerUName != cus.ACustomerUName)
                            newEDisplay.type = "";

                    }
                    else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        newEDisplay.actioninfo = "completed";

                        if (!string.IsNullOrEmpty(cus.Latitude) && !string.IsNullOrEmpty(cus.Longtitude))
                            newEDisplay.incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(cus.Latitude.ToString(), cus.Longtitude.ToString(), getClientLic);

                    }
                    else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        newEDisplay.actioninfo = "dispatched";

                        newEDisplay.username = cus.CustomerUName;

                        newEDisplay.incidentLocation = "to " + cus.ACustomerUName;

                    }
                    else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        newEDisplay.actioninfo = "resolved";
                    }
                    else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        newEDisplay.actioninfo = "rejected";

                    }
                    else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        newEDisplay.actioninfo = "parked";

                    }
                    else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {

                        if (!string.IsNullOrEmpty(cus.Latitude) && !string.IsNullOrEmpty(cus.Longtitude))
                            newEDisplay.incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(cus.Latitude.ToString(), cus.Longtitude.ToString(), getClientLic);

                        newEDisplay.actioninfo = "engaged";

                    }
                    else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        newEDisplay.actioninfo = "released";


                    }
                    else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        newEDisplay.actioninfo = "arrived to incident";

                        if (!string.IsNullOrEmpty(cus.Latitude) && !string.IsNullOrEmpty(cus.Longtitude))
                            newEDisplay.incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(cus.Latitude.ToString(), cus.Longtitude.ToString(), getClientLic);

                    }

                    eeList.Add(newEDisplay);
                    cusCount++;
                }
                
            }
            foreach (var veri in verifier)
            {
                if (veriCount == eCount)
                    break;


                if (veri.Request == (int)Verifier.RequestTypes.Verify)
                {
                    var newEDisplay = new eHistoryDisplay();
                    newEDisplay.date = veri.CreatedDate;
                    newEDisplay.type = "Ver";
                    newEDisplay.name = veri.TypeName;
                    newEDisplay.id = veri.Id;
                    newEDisplay.username = veri.CreatedBy;

                    newEDisplay.actioninfo = "requested verification";

                    if (veri.Latitude > 0 && veri.Longitude > 0)
                        newEDisplay.incidentLocation = " at " + ReverseGeocode.RetrieveFormatedAddress(veri.Latitude.ToString(), veri.Longitude.ToString(), getClientLic);

                    eeList.Add(newEDisplay);
                    veriCount++;
                }
            }
            foreach (var tsk in taskLists)
            {
                if (taskCount == eCount)
                    break;

                if (tsk.Status == (int)TaskStatus.Pending)
                {
                    var newEDisplay = new eHistoryDisplay();
                    newEDisplay.date = tsk.CreateDate.Value;

                    if (tsk.Latitude > 0 && tsk.Longitude > 0)
                        newEDisplay.incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.Latitude.ToString(), tsk.Longitude.ToString(), getClientLic);

                    newEDisplay.actioninfo = "got assigned task";
                    newEDisplay.type = "Tsk";
                    newEDisplay.name = tsk.Name;
                    newEDisplay.id = tsk.Id;
                    newEDisplay.username = tsk.ACustomerUName;
                    eeList.Add(newEDisplay);
                }
                else if (tsk.Status == (int)TaskStatus.OnRoute)
                {
                    var newEDisplay = new eHistoryDisplay();
                    newEDisplay.date = tsk.CreateDate.Value;

                    if (tsk.OnRouteLatitude > 0 && tsk.OnRouteLongitude > 0)
                        newEDisplay.incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.OnRouteLatitude.ToString(), tsk.OnRouteLongitude.ToString(), getClientLic);

                    newEDisplay.actioninfo = "on route";
                    newEDisplay.type = "Tsk";
                    newEDisplay.name = tsk.Name;
                    newEDisplay.id = tsk.Id;
                    newEDisplay.username = tsk.ACustomerUName;
                    eeList.Add(newEDisplay);
                }
                else if (tsk.Status == (int)TaskStatus.InProgress && tsk.JobStatus == (int)TaskJobAction.Pause)
                {
                    var ghistory = TaskEventHistory.GetTaskEventHistoryByTaskId(tsk.Id, dbConnection);

                    ghistory = ghistory.Where(i => i.Action == (int)TaskAction.Pause).ToList();

                    if (ghistory.Count > 0)
                    {
                        var newEDisplay = new eHistoryDisplay();
                        newEDisplay.date = ghistory[0].CreatedDate.Value;

                        if (!string.IsNullOrEmpty(ghistory[0].Latitude) && !string.IsNullOrEmpty(ghistory[0].Longtitude))
                            newEDisplay.incidentLocation = "on " + ReverseGeocode.RetrieveFormatedAddress(ghistory[0].Latitude.ToString(), ghistory[0].Longtitude.ToString(), getClientLic);

                        newEDisplay.actioninfo = "paused ";
                        newEDisplay.type = "Tsk";
                        newEDisplay.name = tsk.Name;
                        newEDisplay.id = tsk.Id;
                        newEDisplay.username = tsk.ACustomerUName;
                        eeList.Add(newEDisplay);
                    }
                } 
                else if (tsk.Status == (int)TaskStatus.InProgress)
                {
                    if (tsk.ActualStartDate != null)
                    {
                        var newEDisplay = new eHistoryDisplay();
                        newEDisplay.date = tsk.ActualStartDate.Value;

                        if (tsk.Latitude > 0 && tsk.Longitude > 0)
                            newEDisplay.incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.StartLatitude.ToString(), tsk.StartLongitude.ToString(), getClientLic);

                        newEDisplay.actioninfo = "started progress on";
                        newEDisplay.type = "Tsk";
                        newEDisplay.name = tsk.Name;
                        newEDisplay.id = tsk.Id;
                        newEDisplay.username = tsk.ACustomerUName;
                        eeList.Add(newEDisplay);
                    }
                    //var newEDisplay2 = new eHistoryDisplay();
                    //newEDisplay2.date = tsk.CreateDate.Value;

                    //if (tsk.Latitude > 0 && tsk.Longitude > 0)
                    //    newEDisplay2.incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.Latitude.ToString(), tsk.Longitude.ToString(), getClientLic);

                    //newEDisplay2.actioninfo = "got assigned task";
                    //newEDisplay2.type = "Tsk";
                    //newEDisplay2.name = tsk.Name;
                    //newEDisplay2.id = tsk.Id;
                    //newEDisplay2.username = tsk.ACustomerUName;
                    //eeList.Add(newEDisplay2);
                }
                else if (tsk.Status == (int)TaskStatus.Completed || tsk.Status == (int)TaskStatus.Accepted)
                {
                    if (tsk.ActualEndDate != null)
                    {
                        var newEDisplay = new eHistoryDisplay();

                        newEDisplay.date = tsk.ActualEndDate.Value;

                        if (tsk.Latitude > 0 && tsk.Longitude > 0)
                            newEDisplay.incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.EndLatitude.ToString(), tsk.EndLongitude.ToString(), getClientLic);

                        newEDisplay.actioninfo = "completed";
                        newEDisplay.type = "Tsk";
                        newEDisplay.name = tsk.Name;
                        newEDisplay.id = tsk.Id;
                        newEDisplay.username = tsk.ACustomerUName;
                        eeList.Add(newEDisplay);
                    }
                    //if (tsk.ActualStartDate != null)
                    //{
                    //    var newEDisplay2 = new eHistoryDisplay();
                    //    newEDisplay2.date = tsk.ActualStartDate.Value;

                    //    if (tsk.StartLatitude > 0 && tsk.StartLongitude > 0)
                    //        newEDisplay2.incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.StartLatitude.ToString(), tsk.StartLongitude.ToString(), getClientLic);

                    //    newEDisplay2.actioninfo = "started progress on";
                    //    newEDisplay2.type = "Tsk";
                    //    newEDisplay2.name = tsk.Name;
                    //    newEDisplay2.id = tsk.Id;
                    //    newEDisplay2.username = tsk.ACustomerUName;
                    //    eeList.Add(newEDisplay2);
                    //}
                    //var newEDisplay3 = new eHistoryDisplay();
                    //newEDisplay3.date = tsk.CreateDate.Value;

                    //if (tsk.Latitude > 0 && tsk.Longitude > 0)
                    //    newEDisplay3.incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.Latitude.ToString(), tsk.Longitude.ToString(), getClientLic);

                    //newEDisplay3.actioninfo = "got assigned task";
                    //newEDisplay3.type = "Tsk";
                    //newEDisplay3.name = tsk.Name;
                    //newEDisplay3.id = tsk.Id;
                    //newEDisplay3.username = tsk.ACustomerUName;
                    //eeList.Add(newEDisplay3);
                }
 
                taskCount++;
            }
            return eeList.OrderByDescending(o => o.date).ToList();
        }

        public static List<eHistoryDisplay> getUserHistoryDutyRoster(List<EventHistory> cusEvs, List<UserTask> taskLists, ClientLicence getClientLic)
        {
            var eeList = new List<eHistoryDisplay>();

            var eCount = 5;
            var cusCount = 0;
            var taskCount = 0;
            var eEventKeys = new List<int>();
            foreach (var cus in cusEvs)
            {
                if (!eEventKeys.Contains(cus.EventId))
                {
                    eEventKeys.Add(cus.EventId);
                    if (cusCount == eCount)
                        break;

                    var gethotevent = CustomEvent.GetCustomEventById(cus.EventId, dbConnection);
                    if (gethotevent != null)
                    {
                        if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            gethotevent.Name = gethotevent.EventTypeName;
                            //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                            //if (mobEv != null)
                            //{
                            //    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                            //        gethotevent.Name = "None";
                            //    else
                            //        gethotevent.Name = mobEv.EventTypeName;

                            //}
                        }
                        else if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                        {
                           // var reqEv = HotEvent.GetHotEventById(gethotevent.Identifier, dbConnection);
                            var splitName = gethotevent.RequestName.Split('^');
                            if (splitName.Length > 1)
                            {
                                gethotevent.Name = splitName[1];
                            }
                        }
                        var newEDisplay = new eHistoryDisplay();
                        newEDisplay.date = cus.CreatedDate.Value;
                        if (gethotevent.EventType == CustomEvent.EventTypes.DriverOffence)
                        {
                            newEDisplay.type = "Tic";
                            newEDisplay.name = "Ticket";
                        }
                        else
                        {
                            newEDisplay.type = "Cus";
                            newEDisplay.name = gethotevent.Name;
                        }
                        newEDisplay.id = cus.EventId;
                        newEDisplay.username = cus.UserName;
                        if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                        {
                            newEDisplay.actioninfo = "got dispatched";

                            newEDisplay.username = cus.UserName;

                            eeList.Add(newEDisplay);
                            cusCount++;

                        }
                        else if (cus.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                        {
                            newEDisplay.actioninfo = "engaged";

                            newEDisplay.username = cus.UserName;

                            eeList.Add(newEDisplay);
                            cusCount++;

                        }
                    }
                }
            }
            foreach (var tsk in taskLists)
            {
                if (taskCount == eCount)
                    break;

                if (tsk.Status == (int)TaskStatus.Pending)
                {
                    var newEDisplay = new eHistoryDisplay();
                    newEDisplay.date = tsk.CreateDate.Value;

                    if (tsk.Latitude > 0 && tsk.Longitude > 0)
                        newEDisplay.incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.Latitude.ToString(), tsk.Longitude.ToString(), getClientLic);

                    newEDisplay.actioninfo = "got assigned";
                    newEDisplay.type = "Tsk";
                    newEDisplay.name = tsk.Name;
                    newEDisplay.id = tsk.Id;
                    newEDisplay.username = tsk.AssigneeName;
                    eeList.Add(newEDisplay);

                    taskCount++;
                }
                else if (tsk.Status == (int)TaskStatus.InProgress)
                {
                    if (tsk.ActualStartDate != null)
                    {
                        var newEDisplay = new eHistoryDisplay();
                        newEDisplay.date = tsk.ActualStartDate.Value;

                        if (tsk.Latitude > 0 && tsk.Longitude > 0)
                            newEDisplay.incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.StartLatitude.ToString(), tsk.StartLongitude.ToString(), getClientLic);

                        newEDisplay.actioninfo = "is in progress";
                        newEDisplay.type = "Tsk";
                        newEDisplay.name = tsk.Name;
                        newEDisplay.id = tsk.Id;
                        newEDisplay.username = tsk.AssigneeName;
                        eeList.Add(newEDisplay);
                    }
                    taskCount++;
                }
                
            }
            return eeList.OrderByDescending(o => o.date).ToList();
        }

        public static string getUserPhotoUrl(int id , string dbcon)
        {
            var imgSrc = "https://testportalcdn.azureedge.net/Images/icon-user-default.png";
            //"../images/icon-user-default.png";

            var userImg = UserImage.GetUserImageByUserId(id, dbConnection);
            if (userImg != null)
            {
                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
               // int index = userImg.ImagePath.IndexOf("UserPhoto");
                var requiredString = userImg.ImagePath;//.Substring(index, (userImg.ImagePath.Length - index));
              //  requiredString = requiredString.Replace("\\", "/");
              //  imgSrc = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                imgSrc = requiredString;
            }

            return imgSrc;
        }

        public static string getOrgRoleColor(int id)
        {
            var rv = string.Empty;
            if (id == (int)Role.Operator || id == (int)Role.UnassignedOperator)
            {
                rv = "#ed7fff";
            }
            else if (id == (int)Role.Manager)
            {
                rv = "#7f8aff";
            }
            else if (id == (int)Role.Admin)
            {
                rv = "#4fffde";
            }
            else if (id == (int)Role.Director)
            {
                rv = "#6cff4f";
            }
            else if (id == (int)Role.Regional)
            {
                rv = "#ffff4f";
            }
            else if (id == (int)Role.CustomerUser)
            {
                rv = "#ff9b4f";
            }
            else if (id == (int)Role.CustomerSuperadmin)
            {
                rv = "#ff7272";
            }
            return rv;
        }

        public static DateTime getDTNow()
        {
            return DateTime.UtcNow;
        }
        public static string getMobileHoteventCategoryUrl(string img)
        {
            var val = string.Empty;
            if (img == "img1")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_1.png";
            }
            else if (img == "img2")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_2.png";
            }
            else if (img == "img3")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_3.png";
            }
            else if (img == "img4")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_4.png";
            }
            else if (img == "img5")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_5.png";
            }
            else if (img == "img6")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_6.png";
            }
            else if (img == "img7")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_7.png";
            }
            else if (img == "img8")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_8.png";
            }
            else if (img == "eimg1")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_1.png";
            }
            else if (img == "eimg2")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_2.png";
            }
            else if (img == "eimg3")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_3.png";
            }
            else if (img == "eimg4")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_4.png";
            }
            else if (img == "eimg5")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_5.png";
            }
            else if (img == "eimg6")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_6.png";
            }
            else if (img == "eimg7")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_7.png";
            }
            else if (img == "eimg8")
            {
                val = "https://livemimslob.blob.core.windows.net/mobileevent/incident_type_8.png";
            } 
            return val;
        }
        public static string getMobileHoteventCategoryUrlId(string img)
        {
            var val = string.Empty;
            if (!string.IsNullOrEmpty(img))
            {
                var split = img.Split('_');
                val = split[2].Split('.')[0];
            } 
            return val;
        }

        public static string randomColor()
        {
            var random = new Random();
            return String.Format("#{0:X6}", random.Next(0x1000000)); // = "#A197B9"
        }
        public static string getHexColor(int value)
        {

            switch (value)
            {
                case 0:
                    return "#1b93c0";
                case 1:
                    return "#f44e4b";
                case 2:
                    return "#f2c400";
                case 3:
                    return "#3ebb64";
                default:
                    return randomColor();
            }
                
            
            return string.Empty;
        }
        public static string getColorName(int value)
        {

            switch (value)
            {
                case 0:
                    return "Blue";
                case 1:
                    return "Yellow";
                case 2:
                    return "Green";
                case 3:
                    return "Black";
                default:
                    return "Red";
            }


            return string.Empty;
        }
        public static string getTemplateChecklistNameById(int value)
        {

            var checklist = TemplateCheckList.GetAllTemplateCheckListById(value.ToString(),dbConnection);
            if (checklist != null)
                return checklist.Name;


            return "Unknown";
        }
        public static string getPCName(HotEvent hotEV)
        {
            if (hotEV.CamType == 5)
            {
                var splits = hotEV.Name.Split('^');
                return splits[2];
            }
            if (hotEV.CamType == 2)
            {
                var splits = hotEV.Name.Split('-');
                var pcname = splits[splits.Length-1].Split('_');
                return pcname[0];
            }
            if (!string.IsNullOrEmpty(hotEV.Detail1))
            {

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(hotEV.Detail1);
                XmlNodeList nodes = doc.SelectNodes("/MilestoneHotClass");
                int i = nodes.Count;
                if (i > 0)
                {
                    foreach (XmlNode n in nodes)
                    {
                        if (n["PcName"] != null)
                            return n["PcName"].InnerText;
                    }
                }
                else
                {
                    XmlNodeList nodes2 = doc.SelectNodes("/HotEvents/HotEvent");
                    int x = nodes.Count;
                    foreach (XmlNode node in nodes2)
                    {
                        if (node["PcName"] != null)
                            return node["PcName"].InnerText;
                    }
                }

            }
            return string.Empty;
        }
        public static byte[] ImageToByte2(System.Drawing.Bitmap img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }
        public static string CreateIncident(Arrowlabs.Business.Layer.CustomEvent hotEvent)
        {
            string hotEventId = "0";
            WebServiceUtility webServiceUtility = new WebServiceUtility();
            var uploadServiceUrl = Arrowlabs.Business.Layer.MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection).UploadServiceUrl;//Arrowlabs.Business.Layer.ConfigSettings.GetConfigSettings(AppConstant.ConnectionString).ServerIP + "/lupload/";
            hotEventId = webServiceUtility.MakePostStreamWebServiceRequest(uploadServiceUrl, "ReceiveIncident", hotEvent);
            if (!String.IsNullOrEmpty(hotEventId))
            {
                string substring = hotEventId.Substring(1, hotEventId.Length - 2);
                hotEventId = substring;
            }
            return hotEventId;
        }
        public static bool CreateTicket(Arrowlabs.Business.Layer.DriverOffence hotEvent)
        { 
            WebServiceUtility webServiceUtility = new WebServiceUtility();
            var uploadServiceUrl = Arrowlabs.Business.Layer.MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection).UploadServiceUrl;//Arrowlabs.Business.Layer.ConfigSettings.GetConfigSettings(AppConstant.ConnectionString).ServerIP + "/lupload/";
            var driverOffenceResult = webServiceUtility.MakePostStreamWebServiceRequest(uploadServiceUrl, "ReceiveDriverOffenceStream", hotEvent);
             
            var xmldoc = new XmlDocument();
            xmldoc.LoadXml(driverOffenceResult);
            var nodeList = xmldoc.GetElementsByTagName("string");
            var result = false;
            foreach (XmlNode node in nodeList)
            {
                result = (Convert.ToInt32(node.InnerText) == 1) ? true : false;
            }
            return result;
        }
        public static string ClearUser(string username, string password, string sessionid, string id, int siteid)
        {
            var testclass = new { username = username, password = password, sessionId = sessionid, id = id, siteId = siteid };
            string hotEventId = "0";
            var jsonMsg = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(testclass);
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(jsonMsg);
            var request = WebRequest.Create(GetServiceURL(Arrowlabs.Business.Layer.MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection).UploadServiceUrl, "ChangeSiteByUserId")) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            using (MemoryStream ms = new MemoryStream())
            {
                request.ContentLength = buffer.Length;
                request.GetRequestStream().Write(buffer, 0, buffer.Length);
                var webResponse = request.GetResponse() as HttpWebResponse;
                if (webResponse != null)
                {
                    var stream = webResponse.GetResponseStream();
                    if (stream != null)
                    {
                        var reader = new StreamReader(stream);
                        var test2 = reader.ReadToEnd();
                    }
                }
            }

            //if (!String.IsNullOrEmpty(hotEventId))
            //{
            //    string substring = hotEventId.Substring(1, hotEventId.Length - 2);
            //    hotEventId = substring;
            //}
            return hotEventId;
        }
        public static string CreateTask(Arrowlabs.Business.Layer.UserTask task)
        {
            string hotEventId = "0";
            WebServiceUtility webServiceUtility = new WebServiceUtility();
            var uploadServiceUrl = Arrowlabs.Business.Layer.MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection).UploadServiceUrl;//Arrowlabs.Business.Layer.ConfigSettings.GetConfigSettings(CommonUtility.dbConnection).ServerIP + "/lupload/";//"http://192.168.1.120/lupload/";//
            hotEventId = webServiceUtility.MakePostStreamWebServiceRequest(uploadServiceUrl, "ReceiveIncident", task);
            if (!String.IsNullOrEmpty(hotEventId))
            {
                string substring = hotEventId.Substring(1, hotEventId.Length - 2);
                hotEventId = substring;
            }
            return hotEventId;
        }
        public static string getUserDeviceType(int dtype,string fontstyle,string monitor)
        {
            var retString = string.Empty;
            if (dtype == (int)Users.DeviceTypes.Mobile)
                retString = "<i " + fontstyle + " class='fa fa-mobile fa-2x mr-2x'></i>" + monitor;
            else if (dtype == (int)Users.DeviceTypes.Client)
                retString = "<i " + fontstyle + " class='fa fa-tablet fa-2x mr-2x'></i>" + monitor;
            else if (dtype == (int)Users.DeviceTypes.Watch)
            {
                retString = "<img style='width: 28px;height: 28px;margin-top: -13px;' src='../Images/smart.png'>" + monitor;
            }
            else if (dtype == (int)Users.DeviceTypes.MobileAndClient)
            {
                retString = "<i class='fa fa-mobile fa-2x mr-2x'></i><i class='fa fa-tablet fa-2x'></i>" + monitor;
            }
            else if (dtype == (int)Users.DeviceTypes.MobileAndWatch)
            {
                if (fontstyle == "style='color:lime;'")
                {
                    retString = "<i " + fontstyle + " class='fa fa-mobile fa-2x mr-2x'></i><img style='width: 28px;height: 28px;margin-top: -13px;' src='/Images/smart.png'>" + monitor;
                }
                else
                    retString = "<i class='fa fa-mobile fa-2x mr-2x'></i><img style='width: 28px;height: 28px;margin-top: -13px;' src='/Images/smart.png'>" + monitor;
            }
            else
                retString = monitor;

            return retString;
                        //</i><i class='fa fa-tablet fa-2x mr-2x'></i><i class='fa fa-television fa-2x'></i>
        }
        public static bool getCheckboxValue(string val)
        {
            if (val == "on")
                return true;
            return false;
            //</i><i class='fa fa-tablet fa-2x mr-2x'></i><i class='fa fa-television fa-2x'></i>
        }
        public static string getRoleSupervisor(int role)
        {
            if (role == (int)Role.Manager)
            {
                return  "Manager";
            }
            else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
            {
                return  "Supervisor";
            }
            else
            {
                return string.Empty;
            }
        }
        public static string getUserRoleName(int usertype)
        {
            var retString = string.Empty;
            if (usertype == (int)Role.Admin)
            {
                retString = "Level 3";
            }
            else if (usertype == (int)Role.Manager)
            {
                retString = "Level 2";
            }
            else if (usertype == (int)Role.Operator || usertype == (int)Role.UnassignedOperator)
            {
                retString = "Level 1";
            }
            else if (usertype == (int)Role.SuperAdmin)
                retString = "Level 7";
            else if (usertype == (int)Role.CustomerSuperadmin)
                retString = "Customer";
            else if (usertype == (int)Role.CustomerUser)
                retString = "Account User";
            else if (usertype == (int)Role.Director)
                retString = "Level 4";
            else if (usertype == (int)Role.Regional)
                retString = "Level 5";
            else if (usertype == (int)Role.ChiefOfficer)
                retString = "Level 6";
            return retString;
        }
        public static string getUserStatus(int active)
        {
            var status = string.Empty;
            if (active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
            {
                status = Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online.ToString();
            }
            else if (active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
            {
                status = Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error.ToString();
            }
            else if (active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
            {
                status = Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline.ToString();
            }
            return status;
        }
        public static string getImgStatus(string desc)
        {
            if (desc == "Pending")
            {
                return "circle-point-red";
            }
            else if (desc == "InProgress")
            {
                return "circle-point-yellow";
            }
            else if (desc == "Completed")
            {
                return "circle-point-green";
            }
            else if (desc == "Accepted")
            {
                return "circle-point-green";
            }
            else if (desc == "Rejected")
            {
                return "circle-point-red";
            }
            else
            {
                return "circle-point-red";
            }
        }
        public static string getImgStatusPriority(int desc)
        {
            if (desc == (int)TaskPiority.Severe)
            {
                return "circle-point-orange";
            }
            else if (desc == (int)TaskPiority.High)
            {
                return "circle-point-red";
            }
            else if (desc == (int)TaskPiority.Medium)
            {
                return "circle-point-yellow";
            }
            else
            {
                return "circle-point-blue";
            }
        }
        public static string getImgIncidentPriority(string desc)
        {
            if (desc == "DriverOffence")
            {
                return "circle-point-yellow";
            }
            else if (desc == "HotEvent" || desc == "MobileHotEvent")
            {
                return "circle-point-red";
            }
            else if (desc == "Request")
            {
                return "circle-point-blue";
            }
            else
            {
                return "circle-point-yellow";
            }
        }
        public static string getImgUserStatus(string desc)
        {
            if (desc == "Online")
            {
                return "circle-point-green";
            }
            else if (desc == "Offline")
            {
                return "circle-point-red";
            }
            else if (desc == "Error")
            {
                return "circle-point-yellow";
            }
            else
            {
                return "circle-point-red";
            }
        }
        public static int getIncidentStatusValue(string desc)
        {
            if (desc == Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Dispatch.ToString().ToLower())
                return (int)Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Dispatch;
            else if (desc == Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Pending.ToString().ToLower())
                return (int)Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Pending;
            else if (desc == Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Park.ToString().ToLower())
                return (int)Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Park;
            else if (desc == Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Engage.ToString().ToLower())
                return (int)Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Engage;
            else if (desc == Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Release.ToString().ToLower())
                return (int)Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Release;
            else if (desc == Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Complete.ToString().ToLower())
                return (int)Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Complete;
            else if (desc == Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Reject.ToString().ToLower())
                return (int)Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Reject;
            else if (desc == Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Resolve.ToString().ToLower())
                return (int)Arrowlabs.Business.Layer.CustomEvent.IncidentActionStatus.Resolve;

            return 0;
        }
        public static string getDeviceType(int DeviceType)
        {
            var devtype = string.Empty;
            if (DeviceType == (int)Arrowlabs.Business.Layer.Users.DeviceTypes.Mobile)
            {
                devtype = "Mobile";
            }
            else if (DeviceType == (int)Arrowlabs.Business.Layer.Users.DeviceTypes.Client)
            {
                devtype = "Client";
            }
            else if (DeviceType == (int)Arrowlabs.Business.Layer.Users.DeviceTypes.MobileAndClient)
            {
                devtype = "MobileAndClient";
            }
            else
            {
                devtype = "None";
            }
            return devtype;
        }
        public static string getGPSMapURL(string lbLatitude1, string lbLongtitude1)
        {
            if (!string.IsNullOrEmpty(lbLatitude1) || !string.IsNullOrEmpty(lbLongtitude1))
            {

                String mapURL = "https://maps.googleapis.com/maps/api/staticmap?" +
            "center=" + lbLatitude1 + "," + lbLongtitude1 + "&" +
            "size=532x334&markers=color:blue%7Clabel:A%7C" + lbLatitude1 + "," + lbLongtitude1 + "&zoom=14" + "&maptype=png" + "&sensor=true";
                //Image1.ImageUrl = mapURL;
                return mapURL;
            }
            return string.Empty;
        }
        public static string IsEnglish(string text)
        {
            return (Regex.IsMatch(text, "^[0-9A-Za-z ]+$")) ? "true" : "false";
        }
        public static string HasArabicGlyphs(string text)
        {
            try
            {
                char[] glyphs = text.ToCharArray();
                foreach (char glyph in glyphs)
                {
                    if (glyph >= 0x600 && glyph <= 0x6ff) return "true";
                    if (glyph >= 0x750 && glyph <= 0x77f) return "true";
                    if (glyph >= 0xfb50 && glyph <= 0xfc3f) return "true";
                    if (glyph >= 0xfe70 && glyph <= 0xfefc) return "true";
                }
            }
            catch (Exception EX)
            {
                var logid = MIMSLog.MIMSLogSave("CommonUtility", "HasArabicGlyphs", EX, dbConnection);
                //  ScriptManager.RegisterStartupScript(this, this.GetType(), "err_msg", String.Format("alert('{0}');", ErrorMessages.GetErrorMsgByMsgIdentifier(ErrorMessages.ErrMsgs.GenericError, dbConnection) + "(" + logid + ")"), true);
            }
            return "false";
        }
        public static string IsSuperAdmin(string manager, string dbConnection)
        {
            var mang = Users.GetSuperAdmin(dbConnection);
            return (mang.Username == manager) ? "true" : "false";
        }
        public static bool isNumeric(string text)
        {
            decimal n;
            bool isDecimal = false;
            try
            {
                isDecimal = decimal.TryParse(text, out n);
            }
            catch (Exception EX)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "isNumeric", EX, dbConnection);
            }
            return isDecimal;
        }

        public static double GetUserOTHours(int user, DateTime start, DateTime end)
        {
            var otduration = 0.0;
            try
            {
                var getOtHours = JobOTHours.GetJobOTHoursByUserId(user, start, end, dbConnection);
                //Get first ot start marker
                getOtHours = getOtHours.OrderBy(i => i.OTStart).ToList();
                if (getOtHours.Count > 0)
                {
                    var otStart = getOtHours[0].OTStart;

                    var changingStart = getOtHours[0].OTStart;
                    var changingEnd = getOtHours[0].OTEnd;
                    foreach (var ot in getOtHours)
                    {
                        if (otStart.Value == ot.OTStart.Value)
                        {
                            //save first ot duration
                            TimeSpan span = ot.OTEnd.Value.Subtract(ot.OTStart.Value);
                            otduration = span.TotalMinutes;
                        }
                        else
                        {
                            //following ot durations

                            if (changingStart.Value > ot.OTStart.Value)
                            {
                                //should never come here since arranged by otstart means lowest value is first one.
                            }
                            else if (changingStart.Value <= ot.OTStart.Value)
                            {
                                if (isBetweenDates(changingStart.Value, changingEnd.Value, ot.OTStart.Value))
                                {
                                    //ignore because start is inside first block.
                                    if (ot.OTEnd.Value >= changingEnd.Value)
                                    {
                                        TimeSpan span = ot.OTEnd.Value.Subtract(changingEnd.Value);
                                        otduration = span.TotalMinutes + otduration;
                                    }
                                    else if (ot.OTEnd.Value <= changingEnd.Value)
                                    {
                                        //doest nothing cause means it was inside the previous ot block.
                                    }
                                }
                                else if (ot.OTStart.Value <= changingEnd.Value)
                                {
                                    TimeSpan span = changingEnd.Value.Subtract(ot.OTStart.Value);
                                    otduration = span.TotalMinutes + otduration;
                                    if (ot.OTEnd.Value > changingEnd.Value)
                                    {
                                        span = ot.OTEnd.Value.Subtract(changingEnd.Value);
                                        otduration = span.TotalMinutes + otduration;
                                    }
                                    else if (ot.OTEnd.Value <= changingEnd.Value)
                                    {
                                        //doest nothing cause means it was inside the previous ot block.
                                    }
                                }
                                else if (ot.OTStart.Value >= changingEnd.Value) //new segment means that the next ot started after the end time of the last ot block.
                                {
                                    TimeSpan span = ot.OTEnd.Value.Subtract(ot.OTStart.Value);
                                    otduration = span.TotalMinutes + otduration;
                                }
                            }
                            changingStart = ot.OTStart;
                            changingEnd = ot.OTEnd;
                        }
                    }
                    //var OverlappingEvents = getOtHours.Where(e1 => getOtHours.Where(e2 => e2 != e1).Any(e2 => e1.OTStart <= e2.OTEnd && e1.OTEnd >= e2.OTStart));
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "GetUserOTHoursForEmployeeReport", ex, dbConnection);
            }
            if (otduration > 0)
            {
                var retV = MinutesToHoursAndMinutes(Math.Round(otduration));
                return retV;
            }
            else
            {
                return 0.0;
            }
        }

        public static double GetUserOTHoursPerDay(DateTime start, DateTime end, List<JobOTHours> oggetOtHours)
        {
            var otduration = 0.0;
            try
            {
                var startoggetOtHours = oggetOtHours.Where(i => isBetweenDates(start, end, i.OTStart.Value)).ToList();

                var endoggetOtHours = oggetOtHours.Where(i => isBetweenDates(start, end, i.OTEnd.Value)).ToList();

                var getOtHours = new List<JobOTHours>();

                foreach (var ot in startoggetOtHours)
                {
                    var newot = new JobOTHours();
                    newot.OTStart = ot.OTStart;
                    if (ot.OTEnd.Value.Date == end.Date)
                    {
                        newot.OTEnd = ot.OTEnd;
                    }
                    else
                    {
                        newot.OTEnd = ot.OTStart.Value.Date.AddHours(24);
                    }
                    getOtHours.Add(newot);
                }
                foreach (var ot in endoggetOtHours)
                {
                    var newot = new JobOTHours();
                    newot.OTEnd = ot.OTEnd;
                    if (ot.OTStart.Value.Date == start.Date)
                    {
                        newot.OTStart = ot.OTStart;
                    }
                    else
                    {
                        newot.OTStart = ot.OTEnd.Value.Date;
                    }
                    getOtHours.Add(newot);
                }
                //Get first ot start marker
                getOtHours = getOtHours.OrderBy(i => i.OTStart).ToList();
                if (getOtHours.Count > 0)
                {
                    var otStart = getOtHours[0].OTStart;

                    var changingStart = getOtHours[0].OTStart;
                    var changingEnd = getOtHours[0].OTEnd;
                    foreach (var ot in getOtHours)
                    {
                        if (otStart.Value == ot.OTStart.Value)
                        {
                            //save first ot duration
                            TimeSpan span = ot.OTEnd.Value.Subtract(ot.OTStart.Value);
                            otduration = span.TotalMinutes;
                        }
                        else
                        {
                            //following ot durations

                            if (changingStart.Value > ot.OTStart.Value)
                            {
                                //should never come here since arranged by otstart means lowest value is first one.
                            }
                            else if (changingStart.Value <= ot.OTStart.Value)
                            {
                                if (isBetweenDates(changingStart.Value, changingEnd.Value, ot.OTStart.Value))
                                {
                                    //ignore because start is inside first block.
                                    if (ot.OTEnd.Value >= changingEnd.Value)
                                    {
                                        TimeSpan span = ot.OTEnd.Value.Subtract(changingEnd.Value);
                                        otduration = span.TotalMinutes + otduration;
                                    }
                                    else if (ot.OTEnd.Value <= changingEnd.Value)
                                    {
                                        //doest nothing cause means it was inside the previous ot block.
                                    }
                                }
                                else if (ot.OTStart.Value <= changingEnd.Value)
                                {
                                    TimeSpan span = changingEnd.Value.Subtract(ot.OTStart.Value);
                                    otduration = span.TotalMinutes + otduration;
                                    if (ot.OTEnd.Value > changingEnd.Value)
                                    {
                                        span = ot.OTEnd.Value.Subtract(changingEnd.Value);
                                        otduration = span.TotalMinutes + otduration;
                                    }
                                    else if (ot.OTEnd.Value <= changingEnd.Value)
                                    {
                                        //doest nothing cause means it was inside the previous ot block.
                                    }
                                }
                                else if (ot.OTStart.Value >= changingEnd.Value) //new segment means that the next ot started after the end time of the last ot block.
                                {
                                    TimeSpan span = ot.OTEnd.Value.Subtract(ot.OTStart.Value);
                                    otduration = span.TotalMinutes + otduration;
                                }
                            }
                            changingStart = ot.OTStart;
                            changingEnd = ot.OTEnd;
                        }
                    }
                    //var OverlappingEvents = getOtHours.Where(e1 => getOtHours.Where(e2 => e2 != e1).Any(e2 => e1.OTStart <= e2.OTEnd && e1.OTEnd >= e2.OTStart));
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "GetUserOTHoursForEmployeeReport", ex, dbConnection);
            }
            var retV = Math.Round(otduration);//MinutesToHoursAndMinutes(Math.Round(otduration));

            return retV;
        }
        public static double GetUserOTHoursForEmployeeReport(int user, DateTime start, DateTime end)
        {
            var otduration = 0.0;
            try
            {
                var getOtHours = JobOTHours.GetJobOTHoursByUserIdForReport(user, start, end, dbConnection);
                //Get first ot start marker
                getOtHours = getOtHours.OrderBy(i => i.OTStart).ToList();
                if (getOtHours.Count > 0)
                {
                    var otStart = getOtHours[0].OTStart;

                    var changingStart = getOtHours[0].OTStart;
                    var changingEnd = getOtHours[0].OTEnd;
                    foreach (var ot in getOtHours)
                    {
                        if (otStart.Value == ot.OTStart.Value)
                        {
                            //save first ot duration
                            TimeSpan span = ot.OTEnd.Value.Subtract(ot.OTStart.Value);
                            otduration = span.TotalMinutes;
                        }
                        else
                        {
                            //following ot durations

                            if (changingStart.Value > ot.OTStart.Value)
                            {
                                //should never come here since arranged by otstart means lowest value is first one.
                            }
                            else if (changingStart.Value <= ot.OTStart.Value)
                            {
                                if (isBetweenDates(changingStart.Value, changingEnd.Value, ot.OTStart.Value))
                                {
                                    //ignore because start is inside first block.
                                    if (ot.OTEnd.Value >= changingEnd.Value)
                                    {
                                        TimeSpan span = ot.OTEnd.Value.Subtract(changingEnd.Value);
                                        otduration = span.TotalMinutes + otduration;
                                    }
                                    else if (ot.OTEnd.Value <= changingEnd.Value)
                                    {
                                        //doest nothing cause means it was inside the previous ot block.
                                    }
                                }
                                else if (ot.OTStart.Value <= changingEnd.Value)
                                {
                                    TimeSpan span = changingEnd.Value.Subtract(ot.OTStart.Value);
                                    otduration = span.TotalMinutes + otduration;
                                    if (ot.OTEnd.Value > changingEnd.Value)
                                    {
                                        span = ot.OTEnd.Value.Subtract(changingEnd.Value);
                                        otduration = span.TotalMinutes + otduration;
                                    }
                                    else if (ot.OTEnd.Value <= changingEnd.Value)
                                    {
                                        //doest nothing cause means it was inside the previous ot block.
                                    }
                                }
                                else if (ot.OTStart.Value >= changingEnd.Value) //new segment means that the next ot started after the end time of the last ot block.
                                {
                                    TimeSpan span = ot.OTEnd.Value.Subtract(ot.OTStart.Value);
                                    otduration = span.TotalMinutes + otduration;
                                }
                            }
                            changingStart = ot.OTStart;
                            changingEnd = ot.OTEnd;
                        }
                    }
                    //var OverlappingEvents = getOtHours.Where(e1 => getOtHours.Where(e2 => e2 != e1).Any(e2 => e1.OTStart <= e2.OTEnd && e1.OTEnd >= e2.OTStart));
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "GetUserOTHoursForEmployeeReport", ex, dbConnection);
            }
            return Math.Round(otduration);
        }

        public static double GetTravelTimeByTask(UserTask ttask)
        {
            var otduration = 0.0;
            try
            {
                if (ttask != null)
                {
                    if (ttask.ActualOnRouteDate != null && ttask.ActualStartDate != null)
                    {
                        TimeSpan span = ttask.ActualStartDate.Value.Subtract(ttask.ActualOnRouteDate.Value);
                        otduration = span.TotalMinutes;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "GetTravelTimeByTask", ex, dbConnection);
            }
            return Math.Round(otduration);
        }

        public static double GetTravelTimeByTaskByDuration(List<TaskEventHistory> tevents)
        {
            var otduration = 0.0;
            try
            { 
                var inptevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                var onrtevents = tevents.Where(i => i.Action == (int)TaskAction.OnRoute).ToList();

                var onrtlist = new List<int>();
                foreach(var o in onrtevents)
                {
                    onrtlist.Add(o.TaskId);
                }


                inptevents = inptevents.Where(i => onrtlist.Contains(i.TaskId)).ToList();
                var inptlist = new List<int>();
                foreach (var ix in inptevents) {
                    inptlist.Add(ix.TaskId);
                }
                onrtevents = onrtevents.Where(i => inptlist.Contains(i.TaskId)).ToList();
                var c = 0;
                if (inptevents.Count > 0 && onrtevents.Count > 0)
                {
                    foreach (var travel in inptevents)
                    {
                        if (onrtevents.Count > c)
                        {
                            if (onrtevents[c] != null && travel.CreatedDate != null)
                            {
                                if (onrtevents[c].CreatedDate.Value < travel.CreatedDate.Value)
                                {
                                    TimeSpan span = travel.CreatedDate.Value.Subtract(onrtevents[c].CreatedDate.Value);
                                    //otduration = span.TotalMinutes;
                                    otduration = otduration + span.TotalMinutes;
                                }
                            }
                            c++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "GetTravelTimeByTaskByDuration", ex, dbConnection);
            }
            return Math.Round(otduration);
        }

        public static double GetTravelTimeByTaskByDay(UserTask ttask, DateTime dttocheck)
        {
            var otduration = 0.0;
            try
            {
                var tevents = TaskEventHistory.GetTaskEventHistoryByTaskId(ttask.Id, dbConnection);

                var xtevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                var inptevents = xtevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                var onrtevents = tevents.Where(i => i.Action == (int)TaskAction.OnRoute).ToList();

                var c = 0;
                if (inptevents.Count > 0 && onrtevents.Count > 0)
                {
                    foreach (var travel in inptevents)
                    {
                        if (onrtevents.Count > c)
                        {
                            if (onrtevents[c] != null && travel.CreatedDate != null)
                            {
                                if (onrtevents[c].CreatedDate.Value < travel.CreatedDate.Value)
                                {
                                    TimeSpan span = travel.CreatedDate.Value.Subtract(onrtevents[c].CreatedDate.Value);
                                    //otduration = span.TotalMinutes;
                                    otduration = otduration + span.TotalMinutes;
                                }
                            }
                            c++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "GetTravelTimeByTaskByDay", ex, dbConnection);
            }
            return Math.Round(otduration);
        }

        public static double MinutesToHoursAndMinutes(double otduration)
        {
            var retV = 0.0;
            if (otduration > 0)
            {
                //if (otduration > 1440)
                //{
                //    var days = Math.Round((otduration/1440));
                //    var excess = (otduration - (days * 1440));

                //    TimeSpan ts = TimeSpan.FromMinutes(Math.Round(excess));

                //    var cHours = ts.Hours;
                //    var cMinutes = ts.Minutes;

                //    cHours = cHours + (int)(days * 24);

                //    retV = Convert.ToDouble(cHours + "." + cMinutes);
                //    if (cMinutes < 10)
                //    {
                //        retV = Convert.ToDouble(cHours + ".0" + ts.Minutes);
                //    }
                //}
                //else
                //{ 
                    TimeSpan ts = TimeSpan.FromMinutes(Math.Round(otduration));
                   
                    var cHours = ts.Hours;
                    if (ts.Days > 0)
                    {
                        cHours = (ts.Days * 24) + cHours;
                    }
                    retV = Convert.ToDouble(cHours + "." + ts.Minutes);
                    if (ts.Minutes < 10)
                    {
                        retV = Convert.ToDouble(cHours + ".0" + ts.Minutes);
                    }
                //}
            }
            return retV;
        }

        public static bool isBetweenDates(DateTime d1, DateTime d2, DateTime targetDt)
        {
            if (targetDt.Ticks > d1.Ticks && targetDt.Ticks < d2.Ticks)
            {
                // targetDt is in between d1 and d2
                return true;
            }
            else if (targetDt.Ticks >= d1.Ticks && targetDt.Ticks <= d2.Ticks)
            {
                return true;
            }
            return false;
        }
        public static string GetServiceURL(string webServiceIP, string webServiceMethodName)
        {
            string targetURL = "";
            try
            {
                var confSettings = ConfigSettings.GetConfigSettings(CommonUtility.dbConnection);

                if (!webServiceIP.Contains("http://") && !webServiceIP.Contains("https://"))
                {
                    if (confSettings != null)
                    {
                        if (confSettings.Protocol)
                            targetURL += "https://";
                        else
                            targetURL += "http://";
                    }
                }
                targetURL += webServiceIP + webServiceMethodName;
            }
            catch (Exception exception)
            {
                MIMSLog.MIMSLogSave("GetTargetURL", targetURL, exception, CommonUtility.dbConnection);
            }
            return targetURL;
        }

         //var ttest = Convert.FromBase64String("c3VwZXJhZG1pbg==");
         // Actual String Encoding.Default.GetString(ttest)

        public static List<string> getUsersOfManagerDirector(Users userinfo)
        {
            var usersList = new List<string>();
            try
            {
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                    foreach (var alluser in allusers)
                    {
                        usersList.Add(alluser.Username.ToLower());
                    }
                    //var vehs = ClientManager.GetAllClientManager(dbConnection);
                    //foreach (var veh in vehs)
                    //{
                    //    if (veh.UserId == userinfo.ID)
                    //    {
                    //        var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                    //        usersList.Add(health.PCName.ToLower());
                    //    }
                    //}
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var dirList = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in dirList)
                    {
                        usersList.Add(usr.Username);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            usersList.Add(subsubuser.Username.ToLower());
                        }
                        //var vehs = ClientManager.GetAllClientManager(dbConnection);
                        //foreach (var veh in vehs)
                        //{
                        //    if (veh.UserId == usr.ID)
                        //    {
                        //        var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                        //        usersList.Add(health.PCName.ToLower());
                        //    }
                        //}
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        usersList.Add(subsubuser.Username.ToLower());
                    } 
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CommonUtility", "getUsersOfManagerDirector", ex, dbConnection);
            }
            return usersList;
        }
    }
    public class WebServiceUtility
    {
        #region Webservice calling and getting response
        /// <summary>
        /// Use it to call webservice
        /// </summary>
        /// <param name="webServiceIP"></param>
        /// <param name="webServiceMethodName"></param>
        /// <param name="webRequestMethod"></param>
        /// <returns></returns>
        public string MakeGetWebServiceRequest(string webServiceIP, string webServiceMethodName, string webRequestMethod = "GET"
           )
        {
            string responseToReturn = String.Empty;
            Uri targetURL = null;
            try
            {
                targetURL = new Uri(GetTargetURL(webServiceIP, webServiceMethodName));
                responseToReturn = new WebClient().DownloadString(targetURL);

            }
            catch (Exception exception)
            {
                //MIMSLog.MIMSLogSave("MakeGetWebServiceRequest", targetURL.AbsoluteUri.ToString(), exception, AppConstant.ConnectionString);
                //throw new Exception(exception.Message, exception);
            }
            return responseToReturn;
        }
        public static MemoryStream SerializeToStream(object objectType)
        {
            MemoryStream stream = new MemoryStream();
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, objectType);
            return stream;
        }
        /// <summary>
        /// Use it to call webservice
        /// </summary>
        /// <param name="webServiceIP"></param>
        /// <param name="webServiceMethodName"></param>
        /// <param name="webRequestMethod"></param>
        /// <returns></returns>
        public string MakePostStreamWebServiceRequest(string webServiceIP, string webServiceMethodName, object dataToPost,
            string webRequestMethod = "POST")
        {
            string responseToReturn = String.Empty;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(GetTargetURL(webServiceIP, webServiceMethodName));
            try
            {

                req.Timeout = 20 * 1000;
                req.AllowWriteStreamBuffering = false;
                req.SendChunked = true;
                req.Method = "POST";
                var stream = SerializeToStream(dataToPost);
                using (Stream st = req.GetRequestStream())
                {
                    st.Write(stream.ToArray(), 0, stream.ToArray().Length);
                }
                var httpResponse = (HttpWebResponse)req.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    return result;
                }
            }
            catch (Exception exception)
            {
                //MIMSLog.MIMSLogSave("MakePostStreamWebServiceRequest", req.Address.ToString(), exception, AppConstant.ConnectionString);
            }
            return responseToReturn;
        }

        /// <summary>
        /// Get webServiceIP + webServiceMethodName combined URL
        /// </summary>
        /// <param name="webServiceIP"></param>
        /// <param name="webServiceMethodName"></param>
        /// <returns></returns>
        private string GetTargetURL(string webServiceIP, string webServiceMethodName)
        {
            string targetURL = "";
            try
            {
                var confSettings = ConfigSettings.GetConfigSettings(CommonUtility.dbConnection);

                if (!webServiceIP.Contains("http://") && !webServiceIP.Contains("https://"))
                {
                    if (confSettings != null)
                    {
                        if (confSettings.Protocol)
                            targetURL += "https://";
                        else
                            targetURL += "http://";
                    }
                }
                targetURL += webServiceIP + webServiceMethodName;
            }
            catch (Exception exception)
            {
                MIMSLog.MIMSLogSave("GetTargetURL", targetURL, exception, CommonUtility.dbConnection);
            }
            return targetURL;
        }

        #endregion

        #region JSON Deserialization
        public T GetDeserializedJsonObject<T>(string jsonInput)
        {
            try
            {
                T resultantObject = (JsonConvert.DeserializeObject<T>(jsonInput));
                return resultantObject;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message, exception);
                //return default(T);
            }

        }
        public string GetSserializedJsonObject(object input)
        {
            try
            {
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue;
                return javaScriptSerializer.Serialize(input);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message, exception);
                //return default(T);
            }

        }

        #endregion

        public string MakePostObjectWebServiceRequest(string url, object dataToPost,
            string webRequestMethod = "POST")
        {
            string responseToReturn = String.Empty;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(new Uri(url));
            try
            {
                using (MemoryStream stream = SerializeToStream(dataToPost))
                {

                    req.Timeout = 360000;
                    req.AllowWriteStreamBuffering = false;
                    req.SendChunked = true;
                    req.Method = webRequestMethod;
                    using (Stream st = req.GetRequestStream())
                    {
                        st.Write(stream.ToArray(), 0, stream.ToArray().Length);
                    }
                    using (HttpWebResponse response = req.GetResponse() as HttpWebResponse)
                    {
                        int result = (int)response.StatusCode;
                        if (result.ToString() == "200")
                        {
                            using (var reader = new StreamReader(response.GetResponseStream()))
                            {
                                responseToReturn = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }

            catch (Exception exception)
            {
                //MIMSLog.MIMSLogSave("MakePostObjectWebServiceRequest", req.Address.ToString(), exception, AppConstant.ConnectionString);
            }
            return responseToReturn;
        }
    }
}