﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class DutyRoaster
    {
        [DataMember]
        public int Id { get; set; }

        public string UpdatedBy { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int LocationId { get; set; }
        [DataMember]
        public int SubLocationId { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public DateTime? ShiftStartTime { get; set; }
        [DataMember]
        public DateTime? ShiftEndTime { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }

        public string LocationName { get; set; }

        public string SubLocationName { get; set; }
        
        public string Username { get; set; }

        private static DutyRoaster GetDutyRoasterFromSqlReader(SqlDataReader reader)
        {
            var dutyRoaster = new DutyRoaster();

            if (reader["Id"] != DBNull.Value)
                dutyRoaster.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Description"] != DBNull.Value)
                dutyRoaster.Description = reader["Description"].ToString();
            if (reader["UserId"] != DBNull.Value)
                dutyRoaster.UserId = (String.IsNullOrEmpty(reader["UserId"].ToString())) ? 0 : Convert.ToInt32(reader["UserId"].ToString());
            if (reader["LocationId"] != DBNull.Value)
                dutyRoaster.LocationId = Convert.ToInt32(reader["LocationId"].ToString());
            if (reader["SubLocationId"] != DBNull.Value)
                dutyRoaster.SubLocationId = Convert.ToInt32(reader["SubLocationId"].ToString());
            if (reader["IsDeleted"] != DBNull.Value)
                dutyRoaster.IsDeleted = (String.IsNullOrEmpty(reader["IsDeleted"].ToString())) ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                dutyRoaster.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                dutyRoaster.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["ShiftStartTime"] != DBNull.Value)
                dutyRoaster.ShiftStartTime = Convert.ToDateTime(reader["ShiftStartTime"].ToString());
            if (reader["ShiftEndTime"] != DBNull.Value)
                dutyRoaster.ShiftEndTime = Convert.ToDateTime(reader["ShiftEndTime"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                dutyRoaster.CreatedBy = reader["CreatedBy"].ToString();

            dutyRoaster.SubLocationName = "All";
            if (reader["LocationName"] != DBNull.Value)
                dutyRoaster.LocationName = (String.IsNullOrEmpty(reader["LocationName"].ToString())) ? "All" : reader["LocationName"].ToString();
            if (reader["SubLocationName"] != DBNull.Value)
                dutyRoaster.SubLocationName = (String.IsNullOrEmpty(reader["SubLocationName"].ToString())) ? "All" : reader["SubLocationName"].ToString();
            if (reader["Username"] != DBNull.Value)
                dutyRoaster.Username = (String.IsNullOrEmpty(reader["Username"].ToString())) ? "All" : reader["Username"].ToString();

            return dutyRoaster;
        }

        /// <summary>
        /// It returns all duty roasters
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<DutyRoaster> GetAllDutyRoasters(string dbConnection)
        {
            var dutyRoasters = new List<DutyRoaster>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllDutyRoasters", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var dutyRoaster = GetDutyRoasterFromSqlReader(reader);
                    dutyRoasters.Add(dutyRoaster);
                }
                connection.Close();
                reader.Close();
            }
            return dutyRoasters;
        }

        /// <summary>
        /// It returns all duty roasters for particular location
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<DutyRoaster> GetAllDutyRoastersByMainLocationIdAndSubLocationId(int mainLocationId, int subLocationId,
            string dbConnection)
        {

            var allDutyRoasters = GetAllDutyRoasters(dbConnection);
            var filteredDutyRoasters = new List<DutyRoaster>();

            foreach (var dutyRoaster in allDutyRoasters)
            {
                if (dutyRoaster.LocationId == mainLocationId && dutyRoaster.SubLocationId == subLocationId)
                {
                    filteredDutyRoasters.Add(dutyRoaster);
                }
                else if (dutyRoaster.LocationId == mainLocationId && dutyRoaster.SubLocationId == 0)
                {
                    filteredDutyRoasters.Add(dutyRoaster);
                }
                else if (dutyRoaster.LocationId == 0 && dutyRoaster.SubLocationId == 0)
                {
                    filteredDutyRoasters.Add(dutyRoaster);
                }
            }

            return filteredDutyRoasters;
        }

        /// <summary>
        /// It inserts or updates duty roaster
        /// </summary>
        /// <param name="dutyRoaster"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static bool InsertorUpdateDutyRoaster(DutyRoaster dutyRoaster, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (dutyRoaster.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;


            if (dutyRoaster != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateDutyRoaster", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = dutyRoaster.Id;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = dutyRoaster.Description;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = dutyRoaster.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = dutyRoaster.UserId;

                    cmd.Parameters.Add(new SqlParameter("@SubLocationId", SqlDbType.Int));
                    cmd.Parameters["@SubLocationId"].Value = dutyRoaster.SubLocationId;

                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = dutyRoaster.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = dutyRoaster.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = dutyRoaster.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@ShiftStartTime", SqlDbType.NVarChar));
                    cmd.Parameters["@ShiftStartTime"].Value = dutyRoaster.ShiftStartTime;

                    cmd.Parameters.Add(new SqlParameter("@ShiftEndTime", SqlDbType.DateTime));
                    cmd.Parameters["@ShiftEndTime"].Value = dutyRoaster.ShiftEndTime;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = dutyRoaster.CreatedBy;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new DutyRosterAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, dutyRoaster.UpdatedBy);
                            Reflection.CopyProperties(dutyRoaster, audit);
                            DutyRosterAudit.InsertDutyRosterAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateDutyRoaster", ex, dbConnection);
                    }
                }
            }
            return true;
        }
    }
}
