﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class CandidStatusCode
    {
        public int Id { get; set; }

        public string StatusCode { get; set; }

        public string Description { get; set; }
        public string CreatedBy { get; set; }
       public DateTime CreatedDate { get; set; }
       public DateTime UpdatedDate { get; set; }

       private static CandidStatusCode GetCandidStatusCodeListFromSqlReader(SqlDataReader reader)
        {
            var candidStatusCode = new CandidStatusCode();
            if (reader["Id"] != DBNull.Value)
                candidStatusCode.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["StatusCode"] != DBNull.Value)
                candidStatusCode.StatusCode = reader["StatusCode"].ToString();
            if (reader["Description"] != DBNull.Value)
                candidStatusCode.Description = reader["Description"].ToString();           
            if (reader["CreatedDate"] != DBNull.Value)
                candidStatusCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                candidStatusCode.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                candidStatusCode.CreatedBy = reader["CreatedBy"].ToString();
            
            return candidStatusCode;
        }

        /// <summary>
        /// It will return all assets in stock
        /// </summary>
        /// <param name="dbConnection">connection string to database</param>
        /// <returns>list of available assets</returns>

        public static List<CandidStatusCode> GetAllCandidStatusCodes(string dbConnection)
        {
            var candidStatusCodeList = new List<CandidStatusCode>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllCandidStatusCodes", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = GetCandidStatusCodeListFromSqlReader(reader);
                    candidStatusCodeList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return candidStatusCodeList;
        }
    }
}
