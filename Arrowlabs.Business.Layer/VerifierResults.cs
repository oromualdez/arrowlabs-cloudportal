﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]  
    public class VerifierResults
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int VerifierID { get; set; }
        [DataMember]
        public int CaseId { get; set; }
        [DataMember]
        public float Percent { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        private static VerifierResults GetVerifierResultFromSqlReader(SqlDataReader reader)
        {
            var verifierResult = new VerifierResults();

            if (reader["Id"] != DBNull.Value)
                verifierResult.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["VerifierID"] != DBNull.Value)
                verifierResult.VerifierID = Convert.ToInt32(reader["VerifierID"].ToString());
            if (reader["CaseId"] != DBNull.Value)
                verifierResult.CaseId = Convert.ToInt32(reader["CaseId"].ToString());
            if (reader["Percent"] != DBNull.Value)
                verifierResult.Percent = float.Parse(reader["Percent"].ToString());
            if (reader["FilePath"] != DBNull.Value)
                verifierResult.FilePath =reader["FilePath"].ToString();           
            if (reader["CreatedDate"] != DBNull.Value)
                verifierResult.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            return verifierResult;
        }
        public static bool InsertOrUpdateVerifierResult(VerifierResults verifierResult, string dbConnection)
        {
            if (verifierResult != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVerifierResult", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = verifierResult.Id;

                    cmd.Parameters.Add(new SqlParameter("@VerifierID", SqlDbType.Int));
                    cmd.Parameters["@VerifierID"].Value = verifierResult.VerifierID;

                    cmd.Parameters.Add(new SqlParameter("@CaseId", SqlDbType.Int));
                    cmd.Parameters["@CaseId"].Value = verifierResult.CaseId;

                    cmd.Parameters.Add(new SqlParameter("@Percent", SqlDbType.Float));
                    cmd.Parameters["@Percent"].Value = verifierResult.Percent;

                    cmd.Parameters.Add(new SqlParameter("@FilePath", SqlDbType.NVarChar));
                    cmd.Parameters["@FilePath"].Value = verifierResult.FilePath;
                 
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = verifierResult.CreatedDate;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static bool InsertOrUpdateVerifierResult(VerifierResults verifierResult, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (verifierResult != null)
            {
                var action = (verifierResult.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVerifierResult", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = verifierResult.Id;

                    cmd.Parameters.Add(new SqlParameter("@VerifierID", SqlDbType.Int));
                    cmd.Parameters["@VerifierID"].Value = verifierResult.VerifierID;

                    cmd.Parameters.Add(new SqlParameter("@CaseId", SqlDbType.Int));
                    cmd.Parameters["@CaseId"].Value = verifierResult.CaseId;

                    cmd.Parameters.Add(new SqlParameter("@Percent", SqlDbType.Float));
                    cmd.Parameters["@Percent"].Value = verifierResult.Percent;

                    cmd.Parameters.Add(new SqlParameter("@FilePath", SqlDbType.NVarChar));
                    cmd.Parameters["@FilePath"].Value = verifierResult.FilePath;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = verifierResult.CreatedDate;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new VerifierResultsAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, verifierResult.VerifierID.ToString());
                            Reflection.CopyProperties(verifierResult, audit);
                            VerifierResultsAudit.InsertOrUpdateVerifierResult(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer Audit", "InsertOrUpdateVerifierResult", ex, auditDbConnection);
                    }
                }
            }
            return true;
        }

        public static List<VerifierResults> GetVerifierResultByVerifierId(int verifierID, string dbConnection)
        {
            var verifierResults = new List<VerifierResults>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetVerifierResultByVerifierId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@VerifierID", SqlDbType.Int));
                sqlCommand.Parameters["@VerifierID"].Value = verifierID;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifierResult = GetVerifierResultFromSqlReader(reader);
                    verifierResults.Add(verifierResult);
                }
                connection.Close();
                reader.Close();
            }
            return verifierResults;
        }

    }
}
