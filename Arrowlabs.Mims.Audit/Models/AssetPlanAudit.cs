﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Arrowlabs.Mims.Audit.Models
{
    public class AssetPlanAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int AssetId { get; set; }
        public int AssigneeType { get; set; }
        public int AssigneeId { get; set; }
        public int TaskType { get; set; }
        public int ChecklistId { get; set; }
        public string PlanName { get; set; }
        public string Recurring { get; set; }
        public bool isRecurring { get; set; }
        public string TaskName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdateBy { get; set; }
        public string TaskDescription { get; set; }
        public string PlanTime { get; set; }
        public int Priority { get; set; }
        public bool isSignatures { get; set; }
        public bool isTemplate { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public int ManagerId { get; set; }

        public int IncidentId { get; set; }

        public int TaskLinkId { get; set; }

        public int ProjectId { get; set; }

        public int CustId { get; set; }

        public int ContractId { get; set; }

        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string AssigneeName { get; set; }

        public static int InsertorUpdateAssetPlanAudit(AssetPlanAudit userTask, string dbConnection)
        {
            int id = userTask.Id;
            var baseAuditId = BaseAudit.InsertBaseAudit(userTask.BaseAudit, dbConnection);
            try
            {
                var action = (userTask.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                if (userTask != null)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertorUpdateAssetPlanAudit", connection);
                        cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = userTask.Id;
                        cmd.Parameters.Add(new SqlParameter("@AssetId", SqlDbType.Int));
                        cmd.Parameters["@AssetId"].Value = userTask.AssetId;
                        cmd.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                        cmd.Parameters["@AssigneeType"].Value = userTask.AssigneeType;
                        cmd.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                        cmd.Parameters["@AssigneeId"].Value = userTask.AssigneeId;
                        cmd.Parameters.Add(new SqlParameter("@TaskType", SqlDbType.Int));
                        cmd.Parameters["@TaskType"].Value = userTask.TaskType;
                        cmd.Parameters.Add(new SqlParameter("@ChecklistId", SqlDbType.Int));
                        cmd.Parameters["@ChecklistId"].Value = userTask.ChecklistId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = userTask.CustomerId;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = userTask.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@Priority", SqlDbType.Int));
                        cmd.Parameters["@Priority"].Value = userTask.Priority;
                         
                        cmd.Parameters.Add(new SqlParameter("@TaskName", SqlDbType.NVarChar));
                        cmd.Parameters["@TaskName"].Value = userTask.TaskName;

                        cmd.Parameters.Add(new SqlParameter("@PlanName", SqlDbType.NVarChar));
                        cmd.Parameters["@PlanName"].Value = userTask.PlanName;

                        cmd.Parameters.Add(new SqlParameter("@Recurring", SqlDbType.NVarChar));
                        cmd.Parameters["@Recurring"].Value = userTask.Recurring;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = userTask.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@UpdateBy", SqlDbType.NVarChar));
                        cmd.Parameters["@UpdateBy"].Value = userTask.UpdateBy;

                        cmd.Parameters.Add(new SqlParameter("@TaskDescription", SqlDbType.NVarChar));
                        cmd.Parameters["@TaskDescription"].Value = userTask.TaskDescription;

                        cmd.Parameters.Add(new SqlParameter("@PlanTime", SqlDbType.NVarChar));
                        cmd.Parameters["@PlanTime"].Value = userTask.PlanTime;

                        cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                        cmd.Parameters["@StartDate"].Value = userTask.StartDate;

                        cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                        cmd.Parameters["@EndDate"].Value = userTask.EndDate;

                        cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreateDate"].Value = userTask.CreateDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = userTask.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@isRecurring", SqlDbType.TinyInt));
                        cmd.Parameters["@isRecurring"].Value = userTask.isRecurring;

                        cmd.Parameters.Add(new SqlParameter("@isSignatures", SqlDbType.TinyInt));
                        cmd.Parameters["@isSignatures"].Value = userTask.isSignatures;

                        cmd.Parameters.Add(new SqlParameter("@isTemplate", SqlDbType.TinyInt));
                        cmd.Parameters["@isTemplate"].Value = userTask.isTemplate;

                        cmd.Parameters.Add(new SqlParameter("@ContractId", SqlDbType.Int));
                        cmd.Parameters["@ContractId"].Value = userTask.ContractId;

                        cmd.Parameters.Add(new SqlParameter("@CustId", SqlDbType.Int));
                        cmd.Parameters["@CustId"].Value = userTask.CustId;

                        cmd.Parameters.Add(new SqlParameter("@ProjectId", SqlDbType.Int));
                        cmd.Parameters["@ProjectId"].Value = userTask.ProjectId;

                        cmd.Parameters.Add(new SqlParameter("@TaskLinkId", SqlDbType.Int));
                        cmd.Parameters["@TaskLinkId"].Value = userTask.TaskLinkId;

                        cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                        cmd.Parameters["@IncidentId"].Value = userTask.IncidentId;

                        cmd.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                        cmd.Parameters["@ManagerId"].Value = userTask.ManagerId;

                        cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                        cmd.Parameters["@Latitude"].Value = userTask.Latitude;

                        cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                        cmd.Parameters["@Longitude"].Value = userTask.Longitude;

                        cmd.Parameters.Add(new SqlParameter("@AssigneeName", SqlDbType.NVarChar));
                        cmd.Parameters["@AssigneeName"].Value = userTask.AssigneeName;
                        
                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.CommandText = "InsertorUpdateAssetPlanAudit";

                        cmd.ExecuteNonQuery();

                        if (id == 0)
                            id = (int)returnParameter.Value;

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return id;
        }
    }
}
