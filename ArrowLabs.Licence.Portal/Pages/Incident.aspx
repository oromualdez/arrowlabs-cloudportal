﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeBehind="Incident.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.Incident" %>
<%@ Register TagPrefix="incidentTableUC" TagName="MyIncidentTableUC" Src="~/Controls/Tables/IncidentTableUC.ascx" %>
<%@ Register TagPrefix="IncidentCard" TagName="MyIncidentCard" Src="~/Controls/Modals/IncidentCard.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
            <style>
                   .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 210px;
        margin-top:6px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 220px;
        height:23px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
                 #taskDocument {
    overflow-y:scroll;
} 
            #viewDocument1 {
    overflow-y:scroll;
}      
            .dropzone .dz-preview .dz-error-message {
    display:none!important;
}
             #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
                </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--Add script to update the page and send messages.-->
    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        var loggedinUsername = '<%=senderName2%>';
        var locationAllowed = false;
        var mapUrl;
        var mapUrlLocation;
        var mapUrlIncidentLocation;
        var map;
        var mapUsers;
        var mapLocation;
        var mapIncidentLocation;
        var mapTaskLocation;
        var myMarkersTasksLocation = new Array();
        var infoWindowTaskLocation;
        var myMarkers = new Array();
        var myMarkersUsers = new Array();
        var myMarkersLocation = new Array();

        var rotate_factor = 0;
        var rotated = false;
        function startRot() {
            rotated = false;
            rotate_factor = 0;
        }
        function loadMe(e) {
            var newImageWidth = e.width;
            var newImageHeight = e.height;
            var canvasWidth = 400;
            var canvasHeight = 400;
            var imageAspectRatio = e.width / e.height;
            var canvasAspectRatio = canvasWidth / canvasHeight;
            if (imageAspectRatio < canvasAspectRatio) {
                newImageHeight = canvasHeight;
                newImageWidth = e.width * (newImageHeight / e.height);
            }
            else if (imageAspectRatio > canvasAspectRatio) {
                newImageWidth = canvasWidth
                newImageHeight = e.height * (newImageWidth / e.width);
            }
            else {
                newImageWidth = 350;
                newImageHeight = 350;
            }
            var margins;
            if (newImageWidth == 400) {
                jQuery(e).css({
                    'width': newImageWidth,
                    'height': newImageHeight,
                    'margin-top': '10%'
                });
            }
            else {
                jQuery(e).css({
                    'width': newImageWidth,
                    'height': newImageHeight
                });
            }
        }
        function rotateMe(e) {
            try {
                rotate_factor += 1;
                var rotate_angle = (90 * rotate_factor) % 360;
                if (rotated) {

                    if (e.width == 400) {
                        jQuery(e).css({
                            'margin-top': '10%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else {
                        jQuery(e).css({
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    rotated = false;
                }
                else {
                    if (e.width == 400 && e.height == 300) {
                        jQuery(e).css({
                            'margin-top': '10%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else if (e.width == 400 && e.height < 300) {
                        jQuery(e).css({
                            'margin-top': '20%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else {
                        jQuery(e).css({
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    rotated = true;
                }
            }
            catch (err) {
               // alert(err);
            }
        }

        var myMarkersIncidentLocation = new Array();
        var infoWindow;
        var infoWindowUsers;
        var infoWindowLocation;
        var infoWindowIncidentLocation;

        var rtime = new Date(1, 1, 2000, 12, 00, 00);
        var timeout = false;
        var delta = 200;
        var divArray = new Array();
        var popupUP = false;
        var alarmpopUP = false;
        var viewpopupUP = false;
        function checkLiveVideo(camera) {
            var output = "";
            $.ajax({
                type: "POST",
                url: "Incident.aspx/checkLiveVideo",
                data: "{'camera':'" + camera + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else
                        output = data.d;
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                } 
            });
            return output;
        }
        function editIncidentTypeChoice(id, name, img) {
            document.getElementById("rowChoiceIncidentType").value = id;
            document.getElementById("editIncidentTypeinput").value = name;

            document.getElementById("eimg1").filter = "none";
            document.getElementById("eimg1").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg1").style.width = "40px";

            document.getElementById("eimg2").filter = "none";
            document.getElementById("eimg2").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg2").style.width = "40px";

            document.getElementById("eimg3").filter = "none";
            document.getElementById("eimg3").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg3").style.width = "40px";

            document.getElementById("eimg4").filter = "none";
            document.getElementById("eimg4").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg4").style.width = "40px";

            document.getElementById("eimg5").filter = "none";
            document.getElementById("eimg5").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg5").style.width = "40px";

            document.getElementById("eimg6").filter = "none";
            document.getElementById("eimg6").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg6").style.width = "40px";

            document.getElementById("eimg7").filter = "none";
            document.getElementById("eimg7").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg7").style.width = "40px";

            document.getElementById("eimg8").filter = "none";
            document.getElementById("eimg8").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg8").style.width = "40px";

            document.getElementById("eimg" + img).style.filter = "gray";
            document.getElementById("eimg" + img).setAttribute("style", "-webkit-filter:grayscale(1)");
            document.getElementById("eimg" + img).style.width = "40px";

            document.getElementById("imgnewtype").value = "img"+img; 
        }
        function saveeditIncidentType() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('editIncidentTypeinput').value)) {
                isPass = false;
                showAlert("Kindly provide incident type");
            }
            else {
                if (isSpecialChar(document.getElementById('editIncidentTypeinput').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character incident type");
                }
            }
            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "Incident.aspx/editIncidentTypeSave",
                    data: "{'id':'" + document.getElementById("rowChoiceIncidentType").value + "','type':'" + document.getElementById("editIncidentTypeinput").value + "','img':'" + document.getElementById("imgnewtype").value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successincidentScenario').innerHTML = "Incident Type successfully edited";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showAlert("Error 37: Failed to update incident type. -" + data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
        }
    }
    function deleteIncidentType() {
        $.ajax({
            type: "POST",
            url: "Incident.aspx/deleteIncidentTypeSave",
            data: "{'id':'" + document.getElementById("rowChoiceIncidentType").value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {
                    document.getElementById('successincidentScenario').innerHTML = "Incident Type successfully deleted";
                    jQuery('#successfulDispatch').modal('show');
                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showAlert('Error 40: Failed to delete incident type.-' + data.d);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
}

function savenewIncidentType() {
    var newPw = document.getElementById("newIncidentTypeinput").value;
    if (!isEmptyOrSpaces(newPw)) {
        if (!isSpecialChar(newPw)) {
            if (document.getElementById("imgnewtype").value != "0") {
                $.ajax({
                    type: "POST",
                    url: "Incident.aspx/newIncidentTypeSave",
                    data: "{'newtype':'" + newPw + "','img':'" + document.getElementById("imgnewtype").value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newIncidentType').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Incident Type successfully added";
                            jQuery('#successfulDispatch').modal('show');
                            document.getElementById("newIncidentTypeinput").value = "";
                            document.getElementById("img1").filter = "none";
                            document.getElementById("img1").setAttribute("style", "-webkit-filter:grayscale(0)");
                            document.getElementById("img1").style.width = "40px";

                            document.getElementById("img2").filter = "none";
                            document.getElementById("img2").setAttribute("style", "-webkit-filter:grayscale(0)");
                            document.getElementById("img2").style.width = "40px";

                            document.getElementById("img3").filter = "none";
                            document.getElementById("img3").setAttribute("style", "-webkit-filter:grayscale(0)");
                            document.getElementById("img3").style.width = "40px";

                            document.getElementById("img4").filter = "none";
                            document.getElementById("img4").setAttribute("style", "-webkit-filter:grayscale(0)");
                            document.getElementById("img4").style.width = "40px";

                            document.getElementById("img5").filter = "none";
                            document.getElementById("img5").setAttribute("style", "-webkit-filter:grayscale(0)");
                            document.getElementById("img5").style.width = "40px";

                            document.getElementById("img6").filter = "none";
                            document.getElementById("img6").setAttribute("style", "-webkit-filter:grayscale(0)");
                            document.getElementById("img6").style.width = "40px";

                            document.getElementById("img7").filter = "none";
                            document.getElementById("img7").setAttribute("style", "-webkit-filter:grayscale(0)");
                            document.getElementById("img7").style.width = "40px";

                            document.getElementById("img8").filter = "none";
                            document.getElementById("img8").setAttribute("style", "-webkit-filter:grayscale(0)");
                            document.getElementById("img8").style.width = "40px";
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showAlert('Error 41: Failed to save incident type.-' + data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                showAlert("Kindly select an icon");
            }
        }
    }
    else {
        showAlert("Kindly provide incident type");
    }
}
function showNewAlarmModal() {
    try {
        alarmpopUP = true;
    }
    catch (err) {
        showError('Error 60: Problem loading page element.')
    }
}
function showNewIncidentModal() {
    try {
        popupUP = true;
    }
    catch (err) {
        showError('Error 60: Problem loading page element.')
    }
}
function closeIt() {
    self.close();
}
function closeModal() {
    try {
        if (viewpopupUP == true || popupUP == true) {
            window.open('', '_self', '');
            window.close();
            var win = window.open("", "_self");
            win.close();
        }
        else {
            showLoader();
            location.reload();
        }
    }
    catch (err)
    {
        showError('Error 60: Problem loading page element.')
    }
}
function showIncidentCardModal(id) {
    try {
        viewpopupUP = true;
        rowchoice(id);
    }
    catch (err) {
        showError('Error 60: Problem loading page element.')
    }
}
//User-profile
function changePassword() {
    try {
        var newPw = document.getElementById("newPwInput").value;
        var confPw = document.getElementById("confirmPwInput").value;
        var isErr = false;
        if (!isErr) {
            if (!letterGood) {
                showAlert('Password does not contain letter');
                isErr = true;
            }
            if (!isErr) {
                if (!capitalGood) {
                    showAlert('Password does not contain capital letter');
                    isErr = true;
                }
            }
            if (!isErr) {
                if (!numGood) {
                    showAlert('Password does not contain number');
                    isErr = true;
                }
            }
            if (!isErr) {
                if (!lengthGood) {
                    showAlert('Password length not enough');
                    isErr = true;
                }
            }
        }
        if (!isErr) {
            if (newPw == confPw && newPw != "" && confPw != "") {
                $.ajax({
                    type: "POST",
                    url: "Incident.aspx/changePW",
                    data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d != "LOGOUT") {
                            jQuery('#changePasswordModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                            jQuery('#successfulDispatch').modal('show');
                            document.getElementById("newPwInput").value = "";
                            document.getElementById("confirmPwInput").value = "";
                            document.getElementById("oldPwInput").value = confPw;
                        }
                        else {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                showAlert("Kindly match new password with confirm password.")
            }
        }
    }
    catch (ex)
    { showAlert('Error 60: Problem loading page element.-' + ex) }
}
    function editUnlock() {
        document.getElementById("profilePhoneNumberDIV").style.display = "none";
        document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
        document.getElementById("editProfileA").style.display = "none";
        document.getElementById("saveProfileA").style.display = "block";
        document.getElementById("profileEmailAddDIV").style.display = "none";
        document.getElementById("profileEmailAddEditDIV").style.display = "block";
        document.getElementById("userFullnameSpanDIV").style.display = "none";
        document.getElementById("userFullnameSpanEditDIV").style.display = "block";
        if (document.getElementById('profileRoleName').innerHTML == "User") {
            document.getElementById("superviserInfoDIV").style.display = "none";
            document.getElementById("managerInfoDIV").style.display = "block";
            document.getElementById("dirInfoDIV").style.display = "none";


        }
        else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
            document.getElementById("superviserInfoDIV").style.display = "none";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "block";
        }
    }
    function editJustLock() {
        document.getElementById("profilePhoneNumberDIV").style.display = "block";
        document.getElementById("userFullnameSpanEditDIV").style.display = "none";
        document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
        document.getElementById("editProfileA").style.display = "block";
        document.getElementById("saveProfileA").style.display = "none";
        document.getElementById("profileEmailAddDIV").style.display = "block";
        document.getElementById("profileEmailAddEditDIV").style.display = "none";
        document.getElementById("userFullnameSpanDIV").style.display = "block";
        document.getElementById("superviserInfoDIV").style.display = "block";
        document.getElementById("managerInfoDIV").style.display = "none";
        document.getElementById("dirInfoDIV").style.display = "none";
    }
    function editLock() {
        document.getElementById("profilePhoneNumberDIV").style.display = "block";
        document.getElementById("userFullnameSpanEditDIV").style.display = "none";
        document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
        document.getElementById("editProfileA").style.display = "block";
        document.getElementById("saveProfileA").style.display = "none";
        document.getElementById("profileEmailAddDIV").style.display = "block";
        document.getElementById("profileEmailAddEditDIV").style.display = "none";
        document.getElementById("userFullnameSpanDIV").style.display = "block";
        document.getElementById("superviserInfoDIV").style.display = "block";
        document.getElementById("managerInfoDIV").style.display = "none";
        document.getElementById("dirInfoDIV").style.display = "none";
        var role = document.getElementById('UserRoleSelector').value;
        var roleid = 0;
        var supervisor = 0;
        var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
        if (retVal > 0) {
            assignUserProfileData();
        }
        else {
            showError('Error 62: Problem occured while trying to get user profile.');
        }

    }
    function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
        var output = 0;
        $.ajax({
            type: "POST",
            url: "Incident.aspx/addUserProfile",
            data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                output = data.d;
            }
        });
        return output;
    }
    function saveTZ() {
        var scountr = $("#countrySelect option:selected").text();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/saveTZ",
            data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                    jQuery('#successfulDispatch').modal('show');
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function getserverInfo() {
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getServerData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) { 
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {

                    document.getElementById('mobileRemaining').value = data.d[9];
                    document.getElementById('mobileTotal').value = data.d[10];
                    document.getElementById('mobileUsed').value = data.d[11];

                    document.getElementById("countrySelect").value = data.d[28];
                    jQuery('#countrySelect').selectpicker('val', data.d[28]);

                    document.getElementById('surveillanceCheck').checked = false;
                    document.getElementById('notificationCheck').checked = false;
                    document.getElementById('locationCheck').checked = false;
                    document.getElementById('ticketingCheck').checked = false;
                    document.getElementById('taskCheck').checked = false;
                    document.getElementById('incidentCheck').checked = false;
                    document.getElementById('warehouseCheck').checked = false;
                    document.getElementById('chatCheck').checked = false;
                    document.getElementById('collaborationCheck').checked = false;
                    document.getElementById('lfCheck').checked = false;
                    document.getElementById('dutyrosterCheck').checked = false;
                    document.getElementById('postorderCheck').checked = false;
                    document.getElementById('verificationCheck').checked = false;
                    document.getElementById('requestCheck').checked = false;
                    document.getElementById('dispatchCheck').checked = false;
                    document.getElementById('activityCheck').checked = false;

                    if (data.d[12] == "true")
                        document.getElementById('surveillanceCheck').checked = true;
                    if (data.d[13] == "true")
                        document.getElementById('notificationCheck').checked = true;
                    if (data.d[14] == "true")
                        document.getElementById('locationCheck').checked = true;
                    if (data.d[15] == "true")
                        document.getElementById('ticketingCheck').checked = true;
                    if (data.d[16] == "true")
                        document.getElementById('taskCheck').checked = true;
                    if (data.d[17] == "true")
                        document.getElementById('incidentCheck').checked = true;
                    if (data.d[18] == "true")
                        document.getElementById('warehouseCheck').checked = true;
                    if (data.d[19] == "true")
                        document.getElementById('chatCheck').checked = true;
                    if (data.d[20] == "true")
                        document.getElementById('collaborationCheck').checked = true;
                    if (data.d[21] == "true")
                        document.getElementById('lfCheck').checked = true;
                    if (data.d[22] == "true")
                        document.getElementById('dutyrosterCheck').checked = true;
                    if (data.d[23] == "true")
                        document.getElementById('postorderCheck').checked = true;
                    if (data.d[24] == "true")
                        document.getElementById('verificationCheck').checked = true;
                    if (data.d[25] == "true")
                        document.getElementById('requestCheck').checked = true;
                    if (data.d[26] == "true")
                        document.getElementById('dispatchCheck').checked = true;
                    if (data.d[27] == "true")
                        document.getElementById('activityCheck').checked = true;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function assignUserProfileData() {
        try {
            $.ajax({
                type: "POST",
                url: "Incident.aspx/getUserProfileData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    try {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            document.getElementById('containerDiv2').style.display = "none";
                            document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                            document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                            document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                            document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                            document.getElementById('profileLastLocation').innerHTML = data.d[4];
                            document.getElementById('profileRoleName').innerHTML = data.d[5];
                            document.getElementById('profileManagerName').innerHTML = data.d[6];
                            document.getElementById('userStatusSpan').innerHTML = data.d[8];

                            if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                document.getElementById('containerDiv2').style.display = "block";
                                document.getElementById('defaultGenderDiv').style.display = "none";
                                getserverInfo();
                            }

                            var el = document.getElementById('userStatusIconSpan');
                            if (el) {
                                el.className = data.d[9];
                            }
                            document.getElementById('userprofileImgSrc').src = data.d[10];
                            document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                            document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                            document.getElementById('userFirstnameSpan').value = data.d[13];
                            document.getElementById('userLastnameSpan').value = data.d[14];
                            document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                            document.getElementById('profileEmailAddEdit').value = data.d[3];

                            document.getElementById('oldPwInput').value = data.d[16];

                            document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                            document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                            document.getElementById('profileGender').innerHTML = data.d[20];

                            if (document.getElementById('profileRoleName').innerHTML != "Level 7") {
                                document.getElementById('deviceTypesDiv').innerHTML = "<i class='fa fa-mobile fa-2x mr-2x'></i><i style='color:lime;' class='fa fa-laptop fa-2x mr-2x'></i>";//data.d[11];

                            }
                        } 
                    }
                    catch (err) {
                        //alert(err)
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        catch (err)
        {
            showError('Error 60: Problem loading page element.-' + err)
        }
    }
    function forceLogout() {
        document.getElementById('<%= closingbtn.ClientID %>').click();
        }
        //Location
        function loadSavedLocation() {
            document.getElementById("liLoadSaved").style.display = "none";
            document.getElementById("liClearSaved").style.display = "block";
            document.getElementById("vertices").value = "";
            for (var i = 0; i < Object.size(polylinesArray) ; i++) {
                if (polylinesArray[i] != null) {
                    polylinesArray[i].setMap(null);
                }
            }
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getLocationData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d);
                        setMapOnAll(obj, myMarkersLocation);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function setMapOnAll(obj, Markers) {
            try {

                for (var i = 0; i < obj.length; i++) {
                    if (Markers[obj[i].Username] != null) {
                        Markers[obj[i].Username].setMap(null);
                    }
                    else {

                    }
                }
            }
            catch (err) {
                //showAlert('');
            }
        }
        var polylinesArray = new Array();
        Object.size = function (obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
        function placeLocationMarkersNewDocument(obj) {
            var secondfirst = true;
            var currentSubLocation;
            var subpoligonCoords = [];
            var polyCounter = 0;
            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="locationChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                if (obj[i].State == "PURPLE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                    myMarkersLocation[obj[i].Username] = marker;
                    createInfoWindowLocation(marker, contentString);
                }
                else {
                    var contentString2 = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="verticesChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                    if (secondfirst) {

                        currentSubLocation = obj[i].State;
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerEngaged.png');
                        myMarkersLocation[obj[i].Username] = marker;
                        createInfoWindowLocation(marker, contentString2);
                        secondfirst = false;
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        subpoligonCoords.push(point);
                    }
                    else {
                        if (currentSubLocation == obj[i].State) {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                        else {
                            if (subpoligonCoords.length > 0) {
                                //var subpoligon = new google.maps.Polygon({
                                //    map: mapLocation,
                                //    path: subpoligonCoords,
                                //    strokeColor: '#0000FF',
                                //    strokeOpacity: 0.8,
                                //    strokeWeight: 3,
                                //    fillColor: '#0000FF',
                                //    fillOpacity: 0.35
                                //});
                                var subpoligon = new google.maps.Polyline({
                                    path: subpoligonCoords,
                                    geodesic: true,
                                    strokeColor: '#FF0000',
                                    strokeOpacity: 1.0,
                                    strokeWeight: 2
                                });
                                subpoligon.setMap(mapLocation);
                                polylinesArray[polyCounter] = subpoligon;
                                polyCounter++;
                            }
                            subpoligonCoords = [];
                            currentSubLocation = obj[i].State;
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerEngaged.png');
                            myMarkersLocation[obj[i].Username] = marker;
                            createInfoWindowLocation(marker, contentString2);
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                    }
                }
            }
            if (subpoligonCoords.length > 0) {
                //var subpoligon = new google.maps.Polygon({
                //    map: mapLocation,
                //    paths: subpoligonCoords,
                //    strokeColor: '#0000FF',
                //    strokeOpacity: 0.8,
                //    strokeWeight: 3,
                //    fillColor: '#0000FF',
                //    fillOpacity: 0.35
                //});
                var subpoligon = new google.maps.Polyline({
                    path: subpoligonCoords,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                subpoligon.setMap(mapLocation);
                polylinesArray[polyCounter] = subpoligon;
                polyCounter++;
            }
        }
        function clearSavedLocation() {
            document.getElementById("liLoadSaved").style.display = "block";
            document.getElementById("liClearSaved").style.display = "none";
            document.getElementById("vertices").value = "";
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getLocationData",
                data: "{'id':'0 ','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d)
                        placeLocationMarkersNewDocument(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function deleteAttachmentChoiceAsset(id) {
            jQuery('#deleteAttachModal').modal('show');
            document.getElementById('rowidChoiceAttachment').value = id;
            jQuery('#viewDocument1').modal('hide');
        }
        function deleteAttachment() {
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/deleteAttachmentData",
                data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','incidentid':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d != "LOGOUT") {
                        showAlert(data.d);
                        rowchoice(document.getElementById('rowidChoice').text);
                        jQuery('#viewDocument1').modal('show');
                    }
                    else {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function uploadattachment(id, imgpath) {
            if (typeof imgpath === 'undefined') {

            }
            else {
                var res = imgpath.replace(/\\/g, "|")
                try {
                    jQuery.ajax({
                        type: "POST",
                        url: "Incident.aspx/uploadMobileAttachment",
                        data: "{'id':'" + id + "','imgpath':'" + res + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
                catch (err) {
                   // alert(err);
                }
            }
        }
        function dispatchNewIncident(status) {
            try {
                var list = document.getElementById("sendToListBox");
                var recBy = document.getElementById("tbIReceivedBy").value;
                var phoneNo = document.getElementById("tbPhone").value;
                var emailAdd = document.getElementById("tbEmail").value;
                var longi = document.getElementById("tbLongitude").value;
                var lati = document.getElementById("tbLatitude").value;
                var incidentName = document.getElementById("tbIncidentName").value;
                var descriptionText = document.getElementById("tbIDescription").value;
                var isCameraAttached = document.getElementById("newcameraCheck").checked;
                var isTaskAttached = document.getElementById("newTaskCheck").checked;
                var locid = document.getElementById('MainContent_locationSelect').value;
                var ins = document.getElementById('instructionTextarea').value;
                var vert = document.getElementById('vertices').value;
                var cameraValue;
                var camDetails = "";
                var taskValue;
                var isErr = false;
                if (isEmptyOrSpaces(recBy)) {
                    isErr = true;
                    showAlert("Kindly provide received by");
                }
                else {
                    if (isSpecialChar(recBy)) {
                        isErr = true;
                    }
                }
                if (!isEmptyOrSpaces(emailAdd)) {
                    if (isInvalidEmail(emailAdd)) {
                        isErr = true;
                    }
                }
                if (!isEmptyOrSpaces(ins)) {
                    if (isSpecialChar(ins)) {
                        isErr = true;
                    }
                }

                if (!isEmptyOrSpaces(phoneNo)) {
                    if (isSpecialChar(phoneNo)) {
                        isErr = true;
                    }
                }

                if (!isEmptyOrSpaces(descriptionText)) {
                    if (isSpecialChar(descriptionText)) {
                        isErr = true;
                    }
                }

                if (!isEmptyOrSpaces(incidentName)) {
                    if (isSpecialChar(incidentName)) {
                        isErr = true;
                    }
                }
                if (!isEmptyOrSpaces(longi)) {
                    if (!isNumeric(longi)) {
                        isErr = true;
                        showAlert("Kindly provide numeric value for the longitude field");
                    }
                }
                if (!isEmptyOrSpaces(lati)) {
                    if (!isNumeric(lati)) {
                        isErr = true;
                        showAlert("Kindly provide numeric value for the latitude field");
                    }
                }
                if (!isNumeric(phoneNo)) {
                    isErr = true;
                    showAlert("Kindly provide numeric value for the phone number field");
                }
                if (isTaskAttached) {
                    var msgtemplate2 = document.getElementById("<%=taskSelect.ClientID%>").options[document.getElementById("<%=taskSelect.ClientID%>").selectedIndex].value;
                    if (msgtemplate2 > 0) {
                        taskValue = msgtemplate2;
                    }
                    else {
                        isErr = true;
                        showAlert("Please select the task you wish to attach");
                    }
                }
                if (isCameraAttached) {
                    if (document.getElementById("<%=newCameraSelect.ClientID%>").selectedIndex >= 0) {
                        var msgtemplate = document.getElementById("<%=newCameraSelect.ClientID%>").options[document.getElementById("<%=newCameraSelect.ClientID%>").selectedIndex].value;
                        if (msgtemplate == "Select Camera") {
                            isErr = true;
                            showAlert("Please select the camera you wish to attach");
                        }
                        else {
                            var camName = document.getElementById("<%=newCameraSelect.ClientID%>").options[document.getElementById("<%=newCameraSelect.ClientID%>").selectedIndex].text;
                            ins = ins + "|" + document.getElementById('cameraDuration').text;

                            var checkVal = checkLiveVideo(camName);
                            if (checkVal == "SUCCESS") {
                                cameraValue = msgtemplate;
                                camDetails = cameraValue;
                                if (typeof document.getElementById("cameraDuration").text === 'undefined') {
                                    isErr = true;
                                    showAlert("Please select the camera duration");
                                }
                                else {
                                    ins = ins + "|" + document.getElementById('cameraDuration').text;
                                }
                            }
                            else {
                                isErr = true;
                                showAlert(checkVal);
                            }
                        }
                    }
                    else {
                        isErr = true;
                        showAlert("No Camera available kindly unselect adding camera to dispatch.");
                    }
                }
                if (!isErr) {
                    if (incidentName == "") {
                        isErr = true;
                        showAlert("Please provide incident name");
                    }
                    else {
                        var imgPath = document.getElementById("mobimagePath").text;
                        if (status == "Park") {
                            var incidentid = insertNewIncident(incidentName, descriptionText, locid, 2, 0, taskValue, recBy, longi, lati, status, ins, isTaskAttached, phoneNo, emailAdd)
                            if (incidentid == "0") {
                                isErr = true;
                                showAlert("Failed to create incident.");
                            }
                            else {
                                uploadattachment(incidentid, imgPath);
                            }
                            if (vert != "") {
                                if (getGeofenceSuccess(incidentid) == "SUCCESS")
                                    incidentGeoFence(incidentid, vert);
                                else {
                                    isErr = true;
                                    var id = incidentid;
                                    $.ajax({
                                        type: "POST",
                                        url: "Incident.aspx/deleteIncident",
                                        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                                        async: false,
                                        dataType: "json",
                                        contentType: "application/json; charset=utf-8",
                                        success: function (data) {
                                            if (data.d == "LOGOUT") {
                                                showError("Session has expired. Kindly login again.");
                                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                            }
                                        },
                                        error: function () {
                                            showError("Session timeout. Kindly login again.");
                                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                        }
                                    });
                                    showAlert("Error occured trying to setup geo fence! Please try again.");
                                }
                            }

                        }
                        else {
                            if (list.length > 0) {
                                var i;
                                var incidentid = insertNewIncident(incidentName, descriptionText, locid, 2, 0, taskValue, recBy, longi, lati, status, ins, isTaskAttached, phoneNo, emailAdd)
                                if (incidentid == "0") {
                                    isErr = true;
                                    showAlert("Failed to create incident.");
                                }
                                else {
                                    uploadattachment(incidentid, imgPath);
                                    if (vert != "") {
                                        if (getGeofenceSuccess(incidentid) == "SUCCESS")
                                            incidentGeoFence(incidentid, vert);
                                        else {
                                            isErr = true;
                                            var id = incidentid;
                                            $.ajax({
                                                type: "POST",
                                                url: "Incident.aspx/deleteIncident",
                                                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                                                async: false,
                                                dataType: "json",
                                                contentType: "application/json; charset=utf-8",
                                                success: function (data) {
                                                    if (data.d == "LOGOUT") {
                                                        showError("Session has expired. Kindly login again.");
                                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                                    }
                                                },
                                                error: function () {
                                                    showError("Session timeout. Kindly login again.");
                                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                                }
                                            });
                                            showAlert("Error occured trying to setup geo fence! Please try again");
                                        }
                                    }
                                    if (!isErr) {
                                        var selected = [];
                                        for (i = 0; i < list.length; i++) {
                                            if (dispatchMarkers[list.options[i].text] != null) {
                                                lati = dispatchMarkers[list.options[i].text].getPosition().lat();
                                                longi = dispatchMarkers[list.options[i].text].getPosition().lng();
                                            }
                                            if (isTaskAttached) {
                                                insertNewTask(isTaskAttached, getAssigneeType(list.options[i].value), list.options[i].text, list.options[i].value, taskValue, longi, lati, incidentid);
                                            }
                                            if (status == "Dispatch") {
                                                if (getAssigneeType(list.options[i].value) == "User") {
                                                    chat.server.sendIncidenttoUser(btoa(list.options[i].text), "ARL┴" + incidentName + "┴" + ins + "┴" + lati + "┴" + longi + "≈" + camDetails, list.options[i].value, incidentid);
                                                    //sendpushNotification(list.options[i].value, list.options[i].text, "User", incidentName, incidentName + " status has been changed to Dispatch");
                                                    //alert('sent-' + list.options[i].text+ "ARL┴" + incidentName + "┴" + ins + "┴" + lati + "┴" + longi + "≈" + camDetails+ list.options[i].value + incidentid)
                                                    selected.push(list.options[i].text);
                                                }
                                                else {

                                                    chat.server.sendIncidenttoDevice(btoa(list.options[i].text), "ARL┴" + incidentName + "┴" + ins + "┴" + lati + "┴" + longi + "≈" + camDetails, incidentid);
                                                }
                                            }
                                        }
                                        if (selected.length > 0)
                                            sendpushMultipleNotification(selected, incidentName);
                                    }
                                }
                            }
                            else {
                                isErr = true;
                                showAlert("Please add incident recepients");
                            }
                        }
                    }
                    if (!isErr) {
                        jQuery('#newDocument2').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = incidentName + " has been successfully " + status;
                        jQuery('#successfulDispatch').modal('show');
                    }
                }
            }
            catch (err) {
                showAlert('dispatchNewIncident-' + err);
            }
        }

        function sendAlarmNotification() {
            var msg = document.getElementById("sendAlarmMessagetxtArea").value;
            if (!isEmptyOrSpaces(msg)) {
                if (!isSpecialChar(msg)) {
                    var list = document.getElementById("sendToListBox");
                    if (list.length > 0) {
                        var selected = [];
                        for (i = 0; i < list.length; i++) {
                            if (getAssigneeType(list.options[i].value) == "User") {
                                if (document.getElementById("assigneeTypeSelectionID").text == "User" || typeof document.getElementById("assigneeTypeSelectionID").text === 'undefined') {
                                    chat.server.sendtoUser(btoa(list.options[i].text), "ARL-Alarm Notification-" + msg + "-" + "┴0┴0", list.options[i].value);
                                    selected.push(list.options[i].text);
                                }
                                else if (document.getElementById("assigneeTypeSelectionID").text == "Group") {
                                    chat.server.sendtoGroup(btoa(list.options[i].text), list.options[i].value, "ARL-Alarm Notification-" + msg + "-" + "┴0┴0");
                                    sendpushNotification(list.options[i].value, list.options[i].text, "Group", msg, "Alarm Notification has been successfully sent");
                                }
                            }
                            else {
                                chat.server.sendtoDevice(btoa(list.options[i].text), "ARL-Alarm Notification-" + msg + "-" + "┴0┴0");
                            }
                        }

                        if (selected.length > 0)
                            sendpushMultipleNotificationAlarm(selected, msg);

                        jQuery('#newAlarm').modal('hide');
                        document.getElementById("sendAlarmMessagetxtArea").value = "";
                        document.getElementById('successincidentScenario').innerHTML = "Alarm Notification has been successfully sent";
                        jQuery('#successfulDispatch').modal('show');
                        cleardispatchList();
                    }
                    else {
                        showAlert("Kindly provide to whom you want to send alarm to");
                    }
                }
            }
            else {
                showAlert("Kindly provide alarm you want to send");
            }
        }
        function getAssigneeType(stringVal) {
            var output = "";
            $.ajax({
                type: "POST",
                url: "Incident.aspx/getAssigneeType",
                data: "{'id':'" + stringVal + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                }
            });
            return output;
        }
        function getGeofenceSuccess(stringVal) {
            var output = "";
            $.ajax({
                type: "POST",
                url: "Incident.aspx/geofenceSuccess",
                data: "{'returnV':'" + stringVal + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        output = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
            return output;
        }
        function incidentGeoFence(id, vert) {
            $.ajax({
                type: "POST",
                url: "Incident.aspx/InsertVertices",
                data: "{'id':'" + id + "','vertices':'" + vert + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function RejectionHandle() {
            document.getElementById("rejectionTextarea").style.display = "block";
            document.getElementById("escalateTextarea").style.display = "none";

            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";

            document.getElementById("rejectOptionsDiv").style.display = "block";
        }
        function dispatchIncident() {
            try {
                var inciName = document.getElementById("incidentNameHeader").innerHTML;
                var id = document.getElementById("rowidChoice").text;
                var list = document.getElementById("sendToListBox");
                var isCameraAttached = document.getElementById("cameraCheck").checked;
                var isTaskAttached = document.getElementById("TaskCheck").checked;
                var longi = document.getElementById("rowLongitude").text;
                var lati = document.getElementById("rowLatitude").text;
                var instruction = document.getElementById("selectinstructionTextarea").value;
                var camDetails = "";
                var cameraValue;
                var taskValue;
                var isErr = false;
                if (isSpecialChar(instruction)) {
                    isErr = true;
                }
                if (isTaskAttached) {
                    var msgtemplate2 = document.getElementById("MainContent_IncidentCardControl_selectTaskTemplate").options[document.getElementById("MainContent_IncidentCardControl_selectTaskTemplate").selectedIndex].value;
                    if (msgtemplate2 > 0) {
                        taskValue = msgtemplate2;
                    }
                    else {
                        isErr = true;
                        showAlert("Please provide a task you wish to attach");
                    }
                }
                if (isCameraAttached) {
                    if (document.getElementById("MainContent_IncidentCardControl_cameraSelect").selectedIndex >= 0) {
                        var msgtemplate = document.getElementById("MainContent_IncidentCardControl_cameraSelect").options[document.getElementById("MainContent_IncidentCardControl_cameraSelect").selectedIndex].value;
                        if (msgtemplate == "Select Camera") {
                            isErr = true;
                            showAlert("Please provide a camera you wish to attach");
                        }
                        else {
                            var camName = document.getElementById("MainContent_IncidentCardControl_cameraSelect").options[document.getElementById("MainContent_IncidentCardControl_cameraSelect").selectedIndex].text;
                            var checkVal = checkLiveVideo(camName);
                            if (checkVal == "SUCCESS") {
                                cameraValue = msgtemplate;
                                camDetails = cameraValue;
                                if (typeof document.getElementById("cameraDuration").text === 'undefined') {
                                    isErr = true;
                                    showAlert("Please select the camera duration");
                                }
                                else {
                                    instruction = instruction + "|" + document.getElementById('cameraDuration').text;
                                }

                            }
                            else {
                                isErr = true;
                                showAlert(checkVal);
                            }
                        }
                    }
                    else {
                        isErr = true;
                        showAlert("No Camera available kindly unselect adding camera to dispatch.");
                    }
                }
                document.getElementById("<%=tbincidentName2.ClientID%>").value = inciName;
                if (!isErr) {
                    var selected = [];
                    if (list.length > 0) {
                        IncidentStateChange('Dispatch');
                        var i;
                        for (i = 0; i < list.length; i++) {

                            if (isTaskAttached) {
                                insertNewTask(isTaskAttached, getAssigneeType(list.options[i].value), list.options[i].text, list.options[i].value, taskValue, longi, lati, id);
                            }
                            if (getAssigneeType(list.options[i].value) == "User") {
                                chat.server.sendIncidenttoUser(btoa(list.options[i].text), "ARL┴" + inciName + "┴" + instruction + "┴" + lati + "┴" + longi + "≈" + camDetails, list.options[i].value, id);
                                selected.push(list.options[i].text);
                            }
                            else {
                                chat.server.sendIncidenttoDevice(btoa(list.options[i].text), "ARL┴" + inciName + "┴" + instruction + "┴" + lati + "┴" + longi + "≈" + camDetails, id);
                            }
                        }
                    }
                    else {
                        isErr = true;
                        showAlert("Please provide assignees");
                    }

                    if (selected.length > 0)
                        sendpushMultipleNotification(selected, inciName);

                    if (!isErr) {
                        jQuery('#viewDocument1').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = inciName + " has been successfully Dispatch";
                        jQuery('#successfulDispatch').modal('show');
                    }
                }
            }
            catch (err) {
                showAlert('Error 56: Problem faced when trying to dispatch incident.-' + err);
            }
        }

        function getDispatchUserList(id, type) {
            document.getElementById("rowtracebackUser").style.display = "block";
            jQuery('#tracebackUserFilter div').html('');

            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getDispatchUserList",
                data: "{'id':'" + id + "','ttype':'" + type + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else
                        document.getElementById("tracebackUserFilter").innerHTML = data.d;
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function tracebackOn(type) {
            var id = 0;
            if (type == "task") {
                id = document.getElementById('rowChoiceTasks').value;
            }
            else
                id = document.getElementById('rowidChoice').text;

            dduration = "0";

            $.ajax({
                type: "POST",
                url: "Incident.aspx/getTracebackLocationData",
                data: "{'id':'" + id + "','ttype':'" + type + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = $.parseJSON(data.d)
                        if (obj.length > 1) {
                            if (type == "task") {
                                updateTaskMarker(obj);
                                document.getElementById("rowtasktracebackUser").style.display = "block";
                            }
                            else {
                                updateIncidentMarker(obj);
                                getDispatchUserList(id, type);
                            }
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        var sselectedUserIds = [];
        function tracebackOnUser(id, type, uid) {
            var ell = document.getElementById(id + uid);
            if (ell) {
                if (ell.className == 'fa fa-square-o') {
                    ell.className = 'fa fa-check-square-o';

                    var index = sselectedUserIds.indexOf(uid);
                    if (index > -1) {
                        sselectedUserIds.splice(index, 1);
                    }
                }
                else {
                    ell.className = 'fa fa-square-o';
                    var a = sselectedUserIds.indexOf(uid);

                    if (a < 0)
                        sselectedUserIds.push(uid);

                }
                $.ajax({
                    type: "POST",
                    url: "Incident.aspx/getTracebackLocationDataByUser",
                    data: JSON.stringify({ id: id, duration: dduration, ttype: type, userIds: sselectedUserIds, uname: loggedinUsername }),
                    //data: "{'id':'" + id + "','duration':'" + dduration + "','ttype':'" + type + "','userIds':'" + uid + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var obj = $.parseJSON(data.d)

                            if (type == "task")
                                updateTaskMarker(obj);
                            else
                                updateIncidentMarker(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        var dduration = "";
        function changeFilterMarkers(id, type, duration) {

            if (type == "task") {
                if (duration == "1") {
                    var el = document.getElementById('taskfilter1');
                    if (el) {
                        if (el.className == 'fa fa-check-square-o') {
                            el.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el.className = 'fa fa-check-square-o';
                    }
                    var ell2 = document.getElementById('taskfilter2');
                    if (ell2) {
                        ell2.className = 'fa fa-square-o';
                    }
                    var ell3 = document.getElementById('taskfilter3');
                    if (ell3) {
                        ell3.className = 'fa fa-square-o';
                    }
                    var ell4 = document.getElementById('taskfilter4');
                    if (ell4) {
                        ell4.className = 'fa fa-square-o';
                    }
                }
                else if (duration == "5") {
                    var el2 = document.getElementById('taskfilter2');
                    if (el2) {
                        if (el2.className == 'fa fa-check-square-o') {
                            el2.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el2.className = 'fa fa-check-square-o';
                    }
                    var ell = document.getElementById('taskfilter1');
                    if (ell) {
                        ell.className = 'fa fa-square-o';
                    }
                    var ell3 = document.getElementById('taskfilter3');
                    if (ell3) {
                        ell3.className = 'fa fa-square-o';
                    }
                    var ell4 = document.getElementById('taskfilter4');
                    if (ell4) {
                        ell4.className = 'fa fa-square-o';
                    }
                }
                else if (duration == "30") {
                    var el3 = document.getElementById('taskfilter3');
                    if (el3) {
                        if (el3.className == 'fa fa-check-square-o') {
                            el3.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el3.className = 'fa fa-check-square-o';
                    }
                    var ell = document.getElementById('taskfilter1');
                    if (ell) {
                        ell.className = 'fa fa-square-o';
                    }
                    var ell2 = document.getElementById('taskfilter2');
                    if (ell2) {
                        ell2.className = 'fa fa-square-o';
                    }
                    var ell4 = document.getElementById('taskfilter4');
                    if (ell4) {
                        ell4.className = 'fa fa-square-o';
                    }
                }
                else if (duration == "60") {
                    var el4 = document.getElementById('taskfilter4');
                    if (el4) {
                        if (el4.className == 'fa fa-check-square-o') {
                            el4.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el4.className = 'fa fa-check-square-o';
                    }
                    var ell = document.getElementById('taskfilter1');
                    if (ell) {
                        ell.className = 'fa fa-square-o';
                    }
                    var ell2 = document.getElementById('taskfilter2');
                    if (ell2) {
                        ell2.className = 'fa fa-square-o';
                    }
                    var ell3 = document.getElementById('taskfilter3');
                    if (ell3) {
                        ell3.className = 'fa fa-square-o';
                    }
                }
            }
            else {
                if (duration == "1") {
                    var el = document.getElementById('filter1');
                    if (el) {
                        if (el.className == 'fa fa-check-square-o') {
                            el.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el.className = 'fa fa-check-square-o';
                    }
                    var elll2 = document.getElementById('filter2');
                    if (elll2) {
                        elll2.className = 'fa fa-square-o';
                    }
                    var elll3 = document.getElementById('filter3');
                    if (elll3) {
                        elll3.className = 'fa fa-square-o';
                    }
                    var elll4 = document.getElementById('filter4');
                    if (elll4) {
                        elll4.className = 'fa fa-square-o';
                    }
                }
                else if (duration == "5") {
                    var el2 = document.getElementById('filter2');
                    if (el2) {
                        if (el2.className == 'fa fa-check-square-o') {
                            el2.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el2.className = 'fa fa-check-square-o';
                    }
                    var elll = document.getElementById('filter1');
                    if (elll) {
                        elll.className = 'fa fa-square-o';
                    }
                    var elll3 = document.getElementById('filter3');
                    if (elll3) {
                        elll3.className = 'fa fa-square-o';
                    }
                    var elll4 = document.getElementById('filter4');
                    if (elll4) {
                        elll4.className = 'fa fa-square-o';
                    }
                }
                else if (duration == "30") {
                    var el3 = document.getElementById('filter3');
                    if (el3) {
                        if (el3.className == 'fa fa-check-square-o') {
                            el3.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el3.className = 'fa fa-check-square-o';
                    }
                    var elll = document.getElementById('filter1');
                    if (elll) {
                        elll.className = 'fa fa-square-o';
                    }
                    var elll2 = document.getElementById('filter2');
                    if (elll2) {
                        elll2.className = 'fa fa-square-o';
                    }
                    var elll4 = document.getElementById('filter4');
                    if (elll4) {
                        elll4.className = 'fa fa-square-o';
                    }
                }
                else if (duration == "60") {
                    var el4 = document.getElementById('filter4');
                    if (el4) {
                        if (el4.className == 'fa fa-check-square-o') {
                            el4.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el4.className = 'fa fa-check-square-o';
                    }
                    var elll = document.getElementById('filter1');
                    if (elll) {
                        elll.className = 'fa fa-square-o';
                    }
                    var elll2 = document.getElementById('filter2');
                    if (elll2) {
                        elll2.className = 'fa fa-square-o';
                    }
                    var elll3 = document.getElementById('filter3');
                    if (elll3) {
                        elll3.className = 'fa fa-square-o';
                    }
                }
            }
            dduration = duration;
            $.ajax({
                type: "POST",
                url: "Incident.aspx/getTracebackLocationDataByUser",
                data: JSON.stringify({ id: id, duration: dduration, ttype: type, userIds: sselectedUserIds, uname: loggedinUsername }),
                //data: "{'id':'" + id + "','duration':'" + dduration + "','ttype':'" + type + "','userIds':'" + uid + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = $.parseJSON(data.d)

                        if (type == "task")
                            updateTaskMarker(obj);
                        else
                            updateIncidentMarker(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function tracebackOnFilter(duration, type) {

            var id = 0;
            if (type == "task")
                id = document.getElementById('rowChoiceTasks').value;
            else
                id = document.getElementById('rowidChoice').text;

            changeFilterMarkers(id, type, duration);
        }
        function viewdurationselect(dur) {
            try {
                //camfilter1
                document.getElementById('cameraDuration').text = dur;
                if (dur == 15) {
                    var el = document.getElementById('camviewfilter1');
                    if (el) {
                        if (el.className == 'fa fa-check-square-o') {
                            el.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el.className = 'fa fa-check-square-o';
                    }
                    var ell2 = document.getElementById('camviewfilter2');
                    if (ell2) {
                        ell2.className = 'fa fa-square-o';
                    }
                    var ell3 = document.getElementById('camviewfilter3');
                    if (ell3) {
                        ell3.className = 'fa fa-square-o';
                    }
                    var ell4 = document.getElementById('camviewfilter4');
                    if (ell4) {
                        ell4.className = 'fa fa-square-o';
                    }
                    var ell5 = document.getElementById('camviewfilter5');
                    if (ell5) {
                        ell5.className = 'fa fa-square-o';
                    }

                }
                else if (dur == 30) {
                    var el = document.getElementById('camviewfilter2');
                    if (el) {
                        if (el.className == 'fa fa-check-square-o') {
                            el.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el.className = 'fa fa-check-square-o';
                    }
                    var ell2 = document.getElementById('camviewfilter1');
                    if (ell2) {
                        ell2.className = 'fa fa-square-o';
                    }
                    var ell3 = document.getElementById('camviewfilter3');
                    if (ell3) {
                        ell3.className = 'fa fa-square-o';
                    }
                    var ell4 = document.getElementById('camviewfilter4');
                    if (ell4) {
                        ell4.className = 'fa fa-square-o';
                    }
                    var ell5 = document.getElementById('camviewfilter5');
                    if (ell5) {
                        ell5.className = 'fa fa-square-o';
                    }
                }
                else if (dur == 45) {
                    var el = document.getElementById('camviewfilter3');
                    if (el) {
                        if (el.className == 'fa fa-check-square-o') {
                            el.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el.className = 'fa fa-check-square-o';
                    }
                    var ell2 = document.getElementById('camviewfilter1');
                    if (ell2) {
                        ell2.className = 'fa fa-square-o';
                    }
                    var ell3 = document.getElementById('camviewfilter2');
                    if (ell3) {
                        ell3.className = 'fa fa-square-o';
                    }
                    var ell4 = document.getElementById('camviewfilter4');
                    if (ell4) {
                        ell4.className = 'fa fa-square-o';
                    }
                    var ell5 = document.getElementById('camviewfilter5');
                    if (ell5) {
                        ell5.className = 'fa fa-square-o';
                    }
                }
                else if (dur == 60) {
                    var el = document.getElementById('camviewfilter4');
                    if (el) {
                        if (el.className == 'fa fa-check-square-o') {
                            el.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el.className = 'fa fa-check-square-o';
                    }
                    var ell2 = document.getElementById('camviewfilter1');
                    if (ell2) {
                        ell2.className = 'fa fa-square-o';
                    }
                    var ell3 = document.getElementById('camviewfilter2');
                    if (ell3) {
                        ell3.className = 'fa fa-square-o';
                    }
                    var ell4 = document.getElementById('camviewfilter3');
                    if (ell4) {
                        ell4.className = 'fa fa-square-o';
                    }
                    var ell5 = document.getElementById('camviewfilter5');
                    if (ell5) {
                        ell5.className = 'fa fa-square-o';
                    }
                }
                else if (dur == 120) {
                    var el = document.getElementById('camviewfilter5');
                    if (el) {
                        if (el.className == 'fa fa-check-square-o') {
                            el.className = 'fa fa-square-o';
                            duration = "0";
                        }
                        else
                            el.className = 'fa fa-check-square-o';
                    }
                    var ell2 = document.getElementById('camviewfilter1');
                    if (ell2) {
                        ell2.className = 'fa fa-square-o';
                    }
                    var ell3 = document.getElementById('camviewfilter2');
                    if (ell3) {
                        ell3.className = 'fa fa-square-o';
                    }
                    var ell4 = document.getElementById('camviewfilter4');
                    if (ell4) {
                        ell4.className = 'fa fa-square-o';
                    }
                    var ell5 = document.getElementById('camviewfilter3');
                    if (ell5) {
                        ell5.className = 'fa fa-square-o';
                    }
                }
            }
            catch (err)
            {
                //alert(err)
            }
        }
        function durationselect(dur) {
            //camfilter1
            document.getElementById('cameraDuration').text = dur;
            if (dur == 15) {
                var el = document.getElementById('camfilter1');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }

            }
            else if (dur == 30) {
                var el = document.getElementById('camfilter2');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 45) {
                var el = document.getElementById('camfilter3');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 60) {
                var el = document.getElementById('camfilter4');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camfilter3');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 120) {
                var el = document.getElementById('camfilter5');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camfilter3');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
        }
        var isCamDuration = false;
        function showCamDuration() {
            if (!isCamDuration) {
                document.getElementById("camDurationDiv").style.display = "block";
                isCamDuration = true;
            }
            else {
                document.getElementById("camDurationDiv").style.display = "none";
                isCamDuration = false;
            }
        }
        var isViewCamDuration = false;
        function showViewCamDuration() {
            if (!isViewCamDuration) {
                document.getElementById("camViewDurationDiv").style.display = "block";
                isViewCamDuration = true;
            }
            else {
                document.getElementById("camViewDurationDiv").style.display = "none";
                isViewCamDuration = false;
            }
        }
        var width = 1;
        function move() {
            var elem = document.getElementById("myBar");
            width = 1;
            var id = setInterval(frame, 200);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        function generateIncidentPDF() {
            width = 1;
            move();
            document.getElementById("pdfloadingAccept").style.display = "block";
            document.getElementById("gnNote").innerHTML = "GENERATING REPORT";
            
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/CreatePDFIncident",
                data: "{'id':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d != "Failed to generate report!") {
                        //jQuery('#viewDocument1').modal('hide');
                        window.open(data.d);
                        //document.getElementById('successfulReportScenario').innerHTML = "Report successfully created!";
                        //jQuery('#successfulReport').modal('show');
                        showAlert("Report successfully created!");
                        document.getElementById("pdfloadingAccept").style.display = "none";
                    }
                    else {
                        showError(data.d);
                        document.getElementById("pdfloadingAccept").style.display = "none";
                    }
                }
            });
        }
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }

        function thisImgClick(e) {
            document.getElementById("img1").filter = "none";
            document.getElementById("img1").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("img1").style.width = "40px";

            document.getElementById("img2").filter = "none";
            document.getElementById("img2").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("img2").style.width = "40px";

            document.getElementById("img3").filter = "none";
            document.getElementById("img3").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("img3").style.width = "40px";

            document.getElementById("img4").filter = "none";
            document.getElementById("img4").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("img4").style.width = "40px";

            document.getElementById("img5").filter = "none";
            document.getElementById("img5").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("img5").style.width = "40px";

            document.getElementById("img6").filter = "none";
            document.getElementById("img6").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("img6").style.width = "40px";

            document.getElementById("img7").filter = "none";
            document.getElementById("img7").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("img7").style.width = "40px";

            document.getElementById("img8").filter = "none";
            document.getElementById("img8").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("img8").style.width = "40px";
             
            document.getElementById("imgnewtype").value = "0";

            if (e.style.filter == "grayscale(1)") {
                e.style.filter = "none";
                e.setAttribute("style", "-webkit-filter:grayscale(0)");
                e.style.width = "40px";
            }
            else {
                e.style.filter = "gray";
                e.setAttribute("style", "-webkit-filter:grayscale(1)");
                e.style.width = "40px";
                document.getElementById("imgnewtype").value = e.id;
            }
        }

        function ethisImgClick(e) {
            document.getElementById("eimg1").filter = "none";
            document.getElementById("eimg1").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg1").style.width = "40px";

            document.getElementById("eimg2").filter = "none";
            document.getElementById("eimg2").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg2").style.width = "40px";

            document.getElementById("eimg3").filter = "none";
            document.getElementById("eimg3").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg3").style.width = "40px";

            document.getElementById("eimg4").filter = "none";
            document.getElementById("eimg4").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg4").style.width = "40px";

            document.getElementById("eimg5").filter = "none";
            document.getElementById("eimg5").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg5").style.width = "40px";

            document.getElementById("eimg6").filter = "none";
            document.getElementById("eimg6").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg6").style.width = "40px";

            document.getElementById("eimg7").filter = "none";
            document.getElementById("eimg7").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg7").style.width = "40px";

            document.getElementById("eimg8").filter = "none";
            document.getElementById("eimg8").setAttribute("style", "-webkit-filter:grayscale(0)");
            document.getElementById("eimg8").style.width = "40px";

            document.getElementById("imgnewtype").value = "0";

            if (e.style.filter == "grayscale(1)") {
                e.style.filter = "none";
                e.setAttribute("style", "-webkit-filter:grayscale(0)");
                e.style.width = "40px";
            }
            else {
                e.style.filter = "gray";
                e.setAttribute("style", "-webkit-filter:grayscale(1)");
                e.style.width = "40px";
                document.getElementById("imgnewtype").value = e.id;
            }
        }

        var firstNewDocOpen = false;
        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
        jQuery(document).ready(function () {
            try {
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    if ($(e.target).attr('href') == "#home-tab" || $(e.target).attr('href') == "#sytem-tab" || $(e.target).attr('href') == "#3rdParty-tab" || $(e.target).attr('href') == "#incidentType-tab")
                        localStorage.setItem('activeTabInci', $(e.target).attr('href'));
                    //alert($(e.target).attr('href')+'----------NOW Selected');
                });
                var activeTab = localStorage.getItem('activeTabInci');
                if (activeTab) {
                    //alert(activeTab +'--------Selected');
                    //$('#myTab a[href="' + activeTab + '"]').tab('show'); 
                    jQuery('a[data-toggle="tab"][href="' + activeTab + '"]').tab('show');
                }
                localStorage.removeItem("activeTabDev");
                localStorage.removeItem("activeTabMessage");
                localStorage.removeItem("activeTabTask");
                localStorage.removeItem("activeTabTick");
                localStorage.removeItem("activeTabUB");
                localStorage.removeItem("activeTabVer");
                localStorage.removeItem("activeTabLost");

                $('input[type=password]').keyup(function () {
                    // keyup event code here
                    var pswd = $(this).val();
                    if (pswd.length < 8) {
                        $('#length').removeClass('valid').addClass('invalid');
                        lengthGood = false;
                    } else {
                        $('#length').removeClass('invalid').addClass('valid');
                        lengthGood = true;
                    }
                    //validate letter
                    if (pswd.match(/[A-z]/)) {
                        $('#letter').removeClass('invalid').addClass('valid');
                        letterGood = true;

                    } else {
                        $('#letter').removeClass('valid').addClass('invalid');
                        letterGood = false;
                    }

                    //validate capital letter
                    if (pswd.match(/[A-Z]/)) {
                        $('#capital').removeClass('invalid').addClass('valid');
                        capitalGood = true;
                    } else {
                        $('#capital').removeClass('valid').addClass('invalid');

                        capitalGood = false;
                    }

                    //validate number
                    if (pswd.match(/\d/)) {
                        $('#number').removeClass('invalid').addClass('valid');
                        numGood = true;

                    } else {
                        $('#number').removeClass('valid').addClass('invalid');
                        numGood = false;
                    }
                });
                $('input[type=password]').focus(function () {
                    // focus code here
                    $('#pswd_info').show();
                });
                $('input[type=password]').blur(function () {
                    // blur code here
                    $('#pswd_info').hide();
                });
            } catch (err)
            {
                //alert(err);
            }
            try {
                infoWindowTaskLocation = new google.maps.InfoWindow();
                infoWindow = new google.maps.InfoWindow();
                infoWindowUsers = new google.maps.InfoWindow();
                infoWindowLocation = new google.maps.InfoWindow();
                infoWindowIncidentLocation = new google.maps.InfoWindow();

                // Now that the DOM is fully loaded, create the dropzone, and setup the
                // event listeners


            }
            catch (err) {

            }
            var myDropzone = new Dropzone("#dz-test");
            myDropzone.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */
                file.previewElement.addEventListener("click", function () {
                    myDropzone.removeFile(file);
                    document.getElementById("mobimagePath").text = '';
                });
                if (typeof document.getElementById("mobimagePath").text === 'undefined' || document.getElementById("mobimagePath").text == '') {
                    if (file.type != "image/jpeg" && file.type != "image/png") {
                        showAlert("Kindly provided a JPEG or PNG Image for upload");
                        this.removeFile(file);
                    }
                    else {
                        var data = new FormData();
                        data.append(file.name, file);
                        jQuery.ajax({
                            url: "../Handlers/MobileIncidentUpload.ashx",
                            type: "POST",
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                document.getElementById("mobimagePath").text = result;
                                //myDropzone.removeAllFiles(true);
                            },
                            error: function (err) {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                            }
                        });
                    }
                }
                else {
                    showAlert("You can only add one image at a time kindly delete previous image");
                    this.removeFile(file);
                }
            });
            var myDropzonePost = new Dropzone("#dz-post");
            myDropzonePost.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */
                width = 1;
                move();
                document.getElementById("pdfloadingAccept").style.display = "block";
                document.getElementById("gnNote").innerHTML = "ATTACHING FILE";
                if (file.type != "image/jpeg" && file.type != "image/png" && file.type != "application/pdf") {
                    showAlert("Kindly provided a JPEG , PNG Image or PDF for upload");
                    this.removeFile(file);
                    document.getElementById("pdfloadingAccept").style.display = "none";
                }
                else {
                    var data = new FormData();
                    data.append(file.name, file);
                    jQuery.ajax({
                        url: "../Handlers/MobileIncidentUpload.ashx",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|")
                            jQuery.ajax({
                                type: "POST",
                                url: "Incident.aspx/attachFileToTask",
                                data: "{'id':'" + document.getElementById('rowidChoice').text + "','filepath':'" + document.getElementById("imagePostAttachment").text + "','uname':'" + loggedinUsername + "'}",
                                async: false,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    if (data.d == "LOGOUT") {
                                        showError("Session has expired. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    }
                                    else {
                                        showAlert(data.d);
                                        rowchoice(document.getElementById('rowidChoice').text);
                                        document.getElementById("pdfloadingAccept").style.display = "none";
                                    }
                                },
                                error: function () {
                                    showError("Session timeout. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            });
                        },
                        error: function (err) {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000); 
                        }
                    })
                }
            });
            var myDropzone2 = new Dropzone("#dz-upload");
            myDropzone2.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */
                file.previewElement.addEventListener("click", function () {
                    myDropzone2.removeFile(file);
                    document.getElementById("mobimagePath2").text = '';
                });
                if (typeof document.getElementById("mobimagePath2").text === 'undefined' || document.getElementById("mobimagePath2").text == '') {
                    if (file.type != "image/jpeg" && file.type != "image/png") {
                        showAlert("Kindly provided a JPEG or PNG Image for upload");
                        this.removeFile(file);
                    }
                    else {
                        var data = new FormData();
                        data.append(file.name, file);
                        jQuery.ajax({
                            url: "../Handlers/MobileIncidentUpload.ashx",
                            type: "POST",
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                document.getElementById("mobimagePath2").text = result;
                                //myDropzone.removeAllFiles(true);
                            },
                            error: function (err) {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000); 
                            }
                        });
                    }
                }
                else {
                    showAlert("You can only add one image at a time kindly delete previous image");
                    this.removeFile(file);
                }
            });
            jQuery('#newDocument').on('shown.bs.modal', function () {
                if (firstNewDocOpen == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Incident.aspx/getLocationData",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                if (data.d != "]") {
                                    var obj = jQuery.parseJSON(data.d)
                                    getLocationMarkers(obj);
                                }
                                else {
                                    getLocationNoMarkers();
                                }
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                    firstNewDocOpen = true;
                }
            });
            jQuery('#taskDocument').on('shown.bs.modal', function () {
                if (firstpresstask == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Incident.aspx/getTaskLocationData",
                        data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                var obj = $.parseJSON(data.d)
                                getTaskLocationMarkers(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                    firstpresstask = true;
                }
                else {
                    jQuery.ajax({
                        type: "POST",
                        url: "Incident.aspx/getTaskLocationData",
                        data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                var obj = $.parseJSON(data.d)
                                updateTaskMarker(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            });

            jQuery('#newDocument2').on('shown.bs.modal', function () {
                Initialize();
                dispatchmapTab();
            });
            jQuery('#viewDocument1').on('shown.bs.modal', function () {
                if (firstincidentpress == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Incident.aspx/getIncidentLocationData",
                        data: "{'id':'" + document.getElementById("rowidChoice").text + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                var obj = jQuery.parseJSON(data.d)
                                getIncidentLocationMarkers(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                    firstincidentpress = true;
                }
                else {
                    jQuery.ajax({
                        type: "POST",
                        url: "Incident.aspx/getIncidentLocationData",
                        data: "{'id':'" + document.getElementById("rowidChoice").text + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                var obj = jQuery.parseJSON(data.d)
                                updateIncidentMarker(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            });
            addrowtoTable('Incident', 'getTableData', 'mobilehoteventTable', loggedinUsername);
            //addrowtoTable('Incident', 'getTableData2', 'hoteventTable', loggedinUsername);
            addrowtoTable3();
            addrowtoTable4();
            addrowtoTable5();
            addrowtoTable6();
            addrowtoAllIncident();
            addrowtoMyIncident();
            addrowtoResolved();
            userTableData();
            assignUserTableData();
            if (popupUP) {
                jQuery('#newDocument').modal('show');
            }
            if (viewpopupUP) {
                jQuery('#viewDocument1').modal('show');
            }
            if (alarmpopUP) {
                jQuery('#newAlarm').modal('show');
            }
        });
        function addrowtoAllIncident() {
            $("#allincidentTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getTableDataAllIncident",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#allincidentTable tbody").append(data.d[i]);
                    }
                }
            });
        }
        function addrowtoMyIncident() {
            $("#myincidentTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getTableDataMyIncident",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#myincidentTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoResolved() {
            $("#resolvedTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getTableDataResolved",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#resolvedTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function handledClick() {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "block";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";

            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";

        }
        function resolveClick() {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "block";
            document.getElementById("resolutionTextarea").style.display = "block";
            document.getElementById("escalateOptionsDiv").style.display = "none";
        }
        function escalateClick() {
            var id = document.getElementById("rowidChoice").text;
            var notes = document.getElementById("escalateTextarea").value;
            var isErr = false;
            if (!isEmptyOrSpaces(document.getElementById('escalateTextarea').value)) {
                if (isSpecialChar(document.getElementById('escalateTextarea').value)) {
                    isErr = true;
                    showAlert("Kindly remove special character from text area");
                }
            }
            if (!isErr) {
                $.ajax({
                    type: "POST",
                    url: "Incident.aspx/escalateIncident",
                    data: "{'id':'" + id + "','ins':'" + notes + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d.length == 0) {
                            showAlert("Error 55: Problem face when trying to escalate incident.");
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            jQuery('#viewDocument1').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Incident has been escalated!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
function redispatch() {
    var id = document.getElementById("rowidChoice").text;
    var ins = document.getElementById("rejectionTextarea").value;
    var isErr = false;
    if (!isEmptyOrSpaces(document.getElementById('rejectionTextarea').value)) {
        if (isSpecialChar(document.getElementById('rejectionTextarea').value)) {
            isErr = true;
            showAlert("Kindly remove special character from text area");
        }
    }
    if (!isErr) {
        $.ajax({
            type: "POST",
            url: "Incident.aspx/rejectReDispatch",
            data: "{'id':'" + id + "','ins':'" + ins + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d.length == 0) {
                    showAlert("Error 55: Problem face when trying to reject and dispatching incident.");
                }
                else if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    var selected = [];
                    for (var i = 1; i < data.d.length; i++) {
                        chat.server.sendIncidenttoUser(btoa(data.d[i]), "ARL┴" + document.getElementById("incidentNameHeader").innerHTML + "┴" + document.getElementById("descriptionSpan").innerHTML + "┴0┴0", data.d[i + 1], id);

                        selected.push(data.d[i]);
                        i++;
                    }
                    if (selected.length > 0)
                        sendpushMultipleNotification(selected, data.d[0]);



                    jQuery('#viewDocument1').modal('hide');
                    document.getElementById('successincidentScenario').innerHTML = data.d[0] + " has been redispatched!";
                    jQuery('#successfulDispatch').modal('show');
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
}
}
        function IncidentStateChange(state) {

            var id = document.getElementById("rowidChoice").text;
            var ins = document.getElementById("selectinstructionTextarea").value;
            var isErr = false;
            if (state == "Reject") {
                ins = document.getElementById("rejectionTextarea").value;

                if (isEmptyOrSpaces(ins)) {
                    ins = document.getElementById("resolutionTextarea").value;
                    if (isEmptyOrSpaces(ins)) {
                        isErr = true;
                        showAlert("Kindly add the rejected notes");
                    }
                }
            }
            if (state == "Resolve") {
                ins = document.getElementById("resolutionTextarea").value;
            }
            if (!isEmptyOrSpaces(ins)) {
                if (isSpecialChar(ins)) {
                    isErr = true;
                    showAlert("Kindly remove special character from text area");
                }
            }
            if (!isErr) {
                var imgPath = document.getElementById("mobimagePath2").text;
                uploadattachment(id, imgPath);
                $.ajax({
                    type: "POST",
                    url: "Incident.aspx/changeIncidentState",
                    data: "{'id':'" + id + "','state':'" + state + "','ins':'" + ins + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "") {
                            showAlert("Error changing incident state");
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (state != "Dispatch") {
                            jQuery('#viewDocument1').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = data.d + " status has been changed to " + state;
                            jQuery('#successfulDispatch').modal('show');
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
    function sendpushNotification(id, aName, aType, name, msg) {
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/pushNotificationSend",
            data: "{'id':'" + id + "','assigneename':'" + aName + "','assigneetype':'" + aType + "','name':'" + name + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function sendpushMultipleNotification(selectedUserIds, messages) {
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/sendpushMultipleNotification",
            data: JSON.stringify({ userIds: selectedUserIds, msg: messages, uname: loggedinUsername }),
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
        function sendpushMultipleNotificationAlarm(selectedUserIds, messages) {
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/sendpushMultipleNotificationAlarm",
                data: JSON.stringify({ userIds: selectedUserIds, msg: messages, uname: loggedinUsername }),
                dataType: "json",
                traditional: true,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    function nextLiClick() {
        document.getElementById("nextLi").style.display = "block";
        document.getElementById("finishLi").style.display = "none";

        document.getElementById("incirotationDIV1").style.display = "none";
        document.getElementById("incirotationDIV2").style.display = "none";

    }
    function finishLiClick() {
        document.getElementById("finishLi").style.display = "block";
        document.getElementById("nextLi").style.display = "none";

        document.getElementById("incirotationDIV1").style.display = "none";
        document.getElementById("incirotationDIV2").style.display = "none";
    }
    function getTaskLocationMarkers(obj) {
        locationAllowed = true;
        //setTimeout(function () {
        google.maps.visualRefresh = true;

        var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 15,
            center: Liverpool,
            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
        };

        // This makes the div with id "map_canvas" a google map
        mapTaskLocation = new google.maps.Map(document.getElementById("taskmap_canvasIncidentLocation"), mapOptions);

        for (var i = 0; i < obj.length; i++) {

            var contentString = '<div id="content">' + obj[i].Username +
            '<br/></div>';

            var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });

            if (obj[i].Username == "Pending") {
                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
            }
            else if (obj[i].Username == "InProgress") {
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
            }
            else if (obj[i].Username == "Completed") {
                if (obj[i].State == "GREEN") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                }
                else {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerX.png');
                }
            }
            else if (obj[i].Username == "OnRoute") {
                marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
            }
            else if (obj[i].Username == "Accepted") {
                marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
            }
            else {
                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
            }
            myMarkersTasksLocation[obj[i].Username] = marker;
            createInfoWindowTaskLocation(marker, contentString);
        }
    }
    function createInfoWindowTaskLocation(marker, popupContent) {
        google.maps.event.addListener(marker, 'click', function () {
            infoWindowTaskLocation.setContent(popupContent);
            infoWindowTaskLocation.open(mapTaskLocation, this);
        });
    }
    function getIncidentLocationMarkers(obj) {


        locationAllowed = true;
        //setTimeout(function () {
        google.maps.visualRefresh = true;
        var firstRed = false;
        var Liverpool;
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].State == "PURPLE") {
                Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
            }
            else if (obj[i].State == "RED") {
                if (!firstRed) {
                    Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    firstRed = true;
                }
            }
        }
        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 10,
            center: Liverpool,
            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
        };

        // This makes the div with id "map_canvas" a google map
        mapIncidentLocation = new google.maps.Map(document.getElementById("map_canvasIncidentLocation"), mapOptions);
        var poligonCoords = [];
        var first = true;
        for (var i = 0; i < obj.length; i++) {

            var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].DisplayName + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '"  onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;,&apos;' + obj[i].DisplayName + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>'

            var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);


            if (obj[i].State == "YELLOW") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                myMarkersIncidentLocation[obj[i].Username] = marker;
                createInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "GREEN") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                myMarkersIncidentLocation[obj[i].Username] = marker;
                createInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "BLUE") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                myMarkersIncidentLocation[obj[i].Username] = marker;
                createInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "OFFUSER") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                myMarkersIncidentLocation[obj[i].Username] = marker;
                createInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "OFFCLIENT") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                myMarkersIncidentLocation[obj[i].Username] = marker;
                createInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "PURPLE") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png')
                contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                myMarkersIncidentLocation[obj[i].Username] = marker;
                createInfoWindowIncidentLocation(marker, contentString);
                document.getElementById("rowIncidentName").text = obj[i].Username;
            }
            else if (obj[i].State == "RED") {
                if (first) {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png');
                    contentString = '<div class="help-block text-center pt-2x"><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">' + obj[i].Username + '</p></div><div class="help-block text-center"></div>';
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    createInfoWindowIncidentLocation(marker, contentString);
                    document.getElementById("rowIncidentName").text = obj[i].Username;
                    first = false;
                }
                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                poligonCoords.push(point);
            }

        }
        if (poligonCoords.length > 0) {
            //var poligon = new google.maps.Polygon({
            //    map: mapIncidentLocation,
            //    paths: poligonCoords,
            //    strokeColor: '#FF0000',
            //    strokeOpacity: 0.8,
            //    strokeWeight: 3,
            //    fillColor: '#FF0000',
            //    fillOpacity: 0.35
            //});
            updatepoligon = new google.maps.Polyline({
                path: poligonCoords,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            updatepoligon.setMap(mapIncidentLocation);
        }
    }
    function createInfoWindowIncidentLocation(marker, popupContent) {
        google.maps.event.addListener(marker, 'click', function () {
            infoWindowLocation.setContent(popupContent);
            infoWindowLocation.open(mapIncidentLocation, this);
        });
    }
    function play(i) {
        try {
            var player = document.getElementById('Video' + (i + 1));
            player.play();
        } catch (error) {
            //alert('play-' + err);
        }
    }
    function dispatchuserTab() {
        document.getElementById("liIncidentDispatchUser").style.display = "none";
        document.getElementById("liIncidentDispatchMap").style.display = "block";
        var el = document.getElementById('assign-user-tab');
        if (el) {
            el.className = 'tab-pane fade active in';
        }
        var el2 = document.getElementById('divLocTabs');
        if (el2) {
            el2.className = 'tab-pane fade';
        }
    }
    function dispatchmapTab() {
        document.getElementById("liIncidentDispatchUser").style.display = "block";
        document.getElementById("liIncidentDispatchMap").style.display = "none";
        var el = document.getElementById('assign-user-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el2 = document.getElementById('divLocTabs');
        if (el2) {
            el2.className = 'tab-pane fade active in';
        }
    }
    function dispatchassignUserTab() {

        var el = document.getElementById('incidentAssign-user-tab');
        if (el) {
            el.className = 'tab-pane fade active in';
        }
        var el3 = document.getElementById('location-tab');
        if (el3) {
            el3.className = 'tab-pane fade';
        }
    }
    function dispatchAssignMapTab() {
        var el = document.getElementById('incidentAssign-user-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('location-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('incidentNext-user-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
    }
    function assignrowDataTask(id) {
        var output = "";
        $.ajax({
            type: "POST",
            url: "Incident.aspx/getTableRowDataTask",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskusernameSpan").innerHTML = data.d[0];
                    document.getElementById("tasktimeSpan").innerHTML = data.d[1];
                    document.getElementById("tasktypeSpan").innerHTML = data.d[2];
                    document.getElementById("taskstatusSpan").innerHTML = data.d[3];
                    document.getElementById("tasklocSpan").innerHTML = data.d[4];
                    document.getElementById("taskdescriptionSpan").innerHTML = data.d[8];
                    document.getElementById("taskinstructionSpan").innerHTML = data.d[9];
                    document.getElementById("taskincidentNameHeader").innerHTML = data.d[10];
                    document.getElementById("assignedTimeSpan").innerHTML = data.d[11];
                    document.getElementById("checklistNotesSpan").innerHTML = data.d[12];
                    document.getElementById("checklistnameSpan").innerHTML = data.d[14];

                    var el = document.getElementById('headerImageClass');
                    if (el) {
                        el.className = data.d[13];
                    }
                    output = data.d[3];

                    document.getElementById('ttypeSpan').innerHTML = data.d[15];

                    document.getElementById("incidentItemsList").innerHTML = "";
                    var res = data.d[16].split("|");
                    if (res.length > 0) {
                        document.getElementById("incidentItemsList").innerHTML = 'Incident: <a style="color:#b2163b;" href="#viewDocument1"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="rowchoice(&apos;' + res[1] + '&apos;)">' + res[0] + '</a>';
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
    function infotabDefault() {
        var el = document.getElementById('activity-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('info-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('attachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
        var el2 = document.getElementById('taskliInfo');
        if (el2) {
            el2.className = 'active';
        }
        var el5 = document.getElementById('taskliActi');
        if (el5) {
            el5.className = ' ';
        }
        var el6 = document.getElementById('taskliAtta');
        if (el6) {
            el6.className = ' ';
        }
    }
    function clearIncidentTabDefault() {
        var el = document.getElementById('activity-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('info-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('attachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }

        var el2 = document.getElementById('liInfo');
        if (el2) {
            el2.className = 'active';
        }
        var el5 = document.getElementById('liActi');
        if (el5) {
            el5.className = ' ';
        }
        var el6 = document.getElementById('liAtta');
        if (el6) {
            el6.className = ' ';
        }
        var ell = document.getElementById('taskfilter1');
        if (ell) {
            ell.className = 'fa fa-square-o';
        }
        var ell2 = document.getElementById('taskfilter2');
        if (ell2) {
            ell2.className = 'fa fa-square-o';
        }
        var ell3 = document.getElementById('taskfilter3');
        if (ell3) {
            ell3.className = 'fa fa-square-o';
        }
        var ell4 = document.getElementById('taskfilter4');
        if (ell4) {
            ell4.className = 'fa fa-square-o';
        }
        var elll = document.getElementById('filter1');
        if (elll) {
            elll.className = 'fa fa-square-o';
        }
        var elll2 = document.getElementById('filter2');
        if (elll2) {
            elll2.className = 'fa fa-square-o';
        }
        var elll3 = document.getElementById('filter3');
        if (elll3) {
            elll3.className = 'fa fa-square-o';
        }
        var elll4 = document.getElementById('filter4');
        if (elll4) {
            elll4.className = 'fa fa-square-o';
        }
    }
 
    var updatepoligon;
    var tracebackpoligon;
    function updateIncidentMarker(obj) {
        try {
            var poligonCoords = [];
            var tracepoligonCoords = [];
            var first = true;
            var first2 = true;
            var currentSubLocation;
            var secondfirst = true;
            var previousColor = "";
            var subpoligonCoords = [];
            var previousMarker = document.getElementById("rowIncidentName").text;
            for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                if (myTraceBackMarkers[i] != null) {
                    myTraceBackMarkers[i].setMap(null);
                }
            }

            if (typeof tracebackpoligon === 'undefined') {
                // your code here.
            }
            else {
                tracebackpoligon.setMap(null);
            }
            if (typeof previousMarker === 'undefined') {
                // your code here.
            }
            else {
                myMarkersIncidentLocation[previousMarker].setMap(null);
                if (typeof updatepoligon === 'undefined') {
                    // your code here.
                }
                else {
                    updatepoligon.setMap(null);
                }
            }
            var tbcounter = 0;


            for (var i = 0; i < obj.length; i++) {
                var isuser = false;
                if (obj[i].hasOwnProperty('DisplayName')) {
                    isuser = true;
                }

                var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].DisplayName + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '"  onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;,&apos;' + obj[i].DisplayName + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>'

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                mapIncidentLocation.setCenter(new google.maps.LatLng(obj[i].Lat, obj[i].Long));
                mapIncidentLocation.setZoom(9);
                if (obj[i].State == "YELLOW") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "GREEN") {
                    if (isuser) {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].DisplayName });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        createInfoWindowIncidentLocation(marker, contentString);
                    }
                    else {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        createInfoWindowIncidentLocation(marker, contentString);
                    }
                }
                else if (obj[i].State == "BLUE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "OFFUSER") {
                    if (isuser) {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].DisplayName });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        createInfoWindowIncidentLocation(marker, contentString);
                    }
                    else {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        createInfoWindowIncidentLocation(marker, contentString);

                    }
                }
                else if (obj[i].State == "OFFCLIENT") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "PURPLE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png')
                    contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    createInfoWindowIncidentLocation(marker, contentString);
                    document.getElementById("rowIncidentName").text = obj[i].Username;
                }
                else if (obj[i].State == "ENG") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                    contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                    myTraceBackMarkers[tbcounter] = marker;
                    tbcounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "COM") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png')
                    contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                    myTraceBackMarkers[tbcounter] = marker;
                    tbcounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    subpoligonCoords.push(point);
                    if (subpoligonCoords.length > 0) {
                        var subpoligon = new google.maps.Polyline({
                            path: subpoligonCoords,
                            geodesic: true,
                            strokeColor: previousColor,
                            strokeOpacity: 1.0,
                            strokeWeight: 7,
                            icons: [{
                                icon: iconsetngs,
                                offset: '100%'
                            }]
                        });
                        subpoligon.setMap(mapIncidentLocation);
                        animateCircle(subpoligon);
                        myTraceBackMarkers[tbcounter] = subpoligon;
                        tbcounter++;
                    }
                }
                else if (obj[i].State == "RED") {
                    if (first) {
                        contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png');
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        createInfoWindowIncidentLocation(marker, contentString);
                        document.getElementById("rowIncidentName").text = obj[i].Username;
                        first = false;
                    }
                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    poligonCoords.push(point);
                }
                else {
                    if (secondfirst) {
                        currentSubLocation = obj[i].State;
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                        myTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                        previousColor = obj[i].Logs;
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        createInfoWindowIncidentLocation(marker, contentString);
                        secondfirst = false;
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        subpoligonCoords.push(point);

                    }
                    else {
                        if (currentSubLocation == obj[i].State) {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                        }
                        else {
                            if (subpoligonCoords.length > 0) {
                                var subpoligon = new google.maps.Polyline({
                                    path: subpoligonCoords,
                                    geodesic: true,
                                    strokeColor: previousColor,
                                    strokeOpacity: 1.0,
                                    strokeWeight: 7
                                });
                                subpoligon.setMap(mapIncidentLocation);
                                myTraceBackMarkers[tbcounter] = subpoligon;
                                tbcounter++;
                            }
                            subpoligonCoords = [];
                            currentSubLocation = obj[i].State;
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                            previousColor = obj[i].Logs;
                            myMarkersIncidentLocation[obj[i].Username] = marker;
                            createInfoWindowIncidentLocation(marker, contentString);
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                    }
                }
            }
            if (poligonCoords.length > 0) {
                updatepoligon = new google.maps.Polyline({
                    path: poligonCoords,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                updatepoligon.setMap(mapIncidentLocation);
            }
            if (tracepoligonCoords.length > 0) {
                tracebackpoligon = new google.maps.Polyline({
                    path: tracepoligonCoords,
                    geodesic: true,
                    strokeColor: '#1b93c0',
                    strokeOpacity: 1.0,
                    strokeWeight: 7,
                    icons: [{
                        icon: iconsetngs,
                        offset: '100%'
                    }]
                });
                tracebackpoligon.setMap(mapIncidentLocation);
                animateCircle(tracebackpoligon);
            }
            if (subpoligonCoords.length > 0) {
                var subpoligon = new google.maps.Polyline({
                    path: subpoligonCoords,
                    geodesic: true,
                    strokeColor: previousColor,
                    strokeOpacity: 1.0,
                    strokeWeight: 7,
                    icons: [{
                        icon: iconsetngs,
                        offset: '100%'
                    }]
                });
                subpoligon.setMap(mapIncidentLocation);
                animateCircle(subpoligon);
                myTraceBackMarkers[tbcounter] = subpoligon;
                tbcounter++;
            }
        }
        catch (err) {
            //showAlert(err);
        }
    }
         

    function nextMapLiClick() {

        document.getElementById("incirotationDIV1").style.display = "none";
        document.getElementById("incirotationDIV2").style.display = "none";

        document.getElementById("nextLi").style.display = "block";
        document.getElementById("finishLi").style.display = "none";
        var name = document.getElementById("rowidChoice").text;
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getGPSDataUsers",
            data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    var obj = jQuery.parseJSON(data.d)
                    updateIncidentMarker(obj);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    var firstincidentpress = false;
    function rowchoice(name) {
        startRot();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getGPSDataUsers",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    var obj = jQuery.parseJSON(data.d)
                    setMapOnAll(obj, myMarkersIncidentLocation);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        clearIncidentTabDefault();

        document.getElementById("escalateionSpanDiv").style.display = "none";
        document.getElementById('rejectionTextarea').value = "";
        document.getElementById('resolutionTextarea').value = "";
        document.getElementById('escalateTextarea').value = "";
        document.getElementById("rowtasktracebackUser").style.display = "none";
        document.getElementById("rowtracebackUser").style.display = "none";
        document.getElementById('checklistDIV').style.display = 'none';
        document.getElementById("finishLi").style.display = "none";
        document.getElementById("nextLi").style.display = "none";
        document.getElementById("rowidChoice").text = name;
        document.getElementById("handleOptionsDiv").style.display = "none";
        oldDivContainers();

        var retval = assignrowData(name);

        var splitRetval = retval.split('-');
        if (splitRetval.length > 0) {
            retval = splitRetval[0];
        }
        if (retval == "Complete") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "block";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "block";
            document.getElementById("completedOptionsDiv2").style.display = "none";

            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Dispatch") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "block";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Pending") {
            document.getElementById("initialOptionsDiv").style.display = "block";
            document.getElementById("parkLi").style.display = "block";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Release") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "block";
            document.getElementById("parkLi").style.display = "block";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Engage") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "block";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Park") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "block";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Reject") {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("resolveLi").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "block";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Resolve") {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "block";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Escalated") {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "block";

        }
        if (splitRetval.length > 0) {

            if (splitRetval[1] == "Resolve") {
                document.getElementById("resolutionTextarea").style.display = "none";
                document.getElementById("completedOptionsDiv2").style.display = "none";
                document.getElementById("resolvedDiv").style.display = "block";
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("parkLi").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "none";
                document.getElementById("completedOptionsDiv").style.display = "none";
                document.getElementById("dispatchOptionsDiv").style.display = "none";
                document.getElementById("rejectOptionsDiv").style.display = "none";
                document.getElementById("rejectionTextarea").style.display = "none";
                document.getElementById("escalateOptionsDiv").style.display = "none";
                document.getElementById("escalateTextarea").style.display = "none";
                document.getElementById("escalatedDIV").style.display = "none";
            }
            else if (splitRetval[1] == "MyIncident") {
                document.getElementById("resolutionTextarea").style.display = "none";
                document.getElementById("completedOptionsDiv2").style.display = "none";
                document.getElementById("resolvedDiv").style.display = "none";
                document.getElementById("resolveLi").style.display = "none";
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("parkLi").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "none";
                document.getElementById("completedOptionsDiv").style.display = "none";
                document.getElementById("dispatchOptionsDiv").style.display = "none";
                document.getElementById("rejectOptionsDiv").style.display = "none";
                document.getElementById("rejectionTextarea").style.display = "none";
                document.getElementById("escalateOptionsDiv").style.display = "none";
                document.getElementById("escalateTextarea").style.display = "none";
                document.getElementById("escalatedDIV").style.display = "block";
                if (retval == "Resolve") {
                    document.getElementById("escalatedDIV").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "block";
                }

            }
        }
        incidentHistoryData(name);
        insertAttachmentIcons(name);
        insertAttachmentTabData(name);
        insertAttachmentData(name);
        dispatchAssignMapTab();
        cleardispatchList();
        infotabDefault();
    }
    function escalateOptionClick() {
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("parkLi").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "block";
        document.getElementById("escalateOptionsDiv").style.display = "block";
    }
    var myTraceBackMarkers = new Array();
 
    function updateTaskMarker(obj) {
        try {
            var first = true;
            var poligonCoords = [];

            for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                if (myTraceBackMarkers[i] != null) {
                    myTraceBackMarkers[i].setMap(null);
                }
            }

            if (typeof tracebackpoligon === 'undefined') {
                // your code here.
            }
            else {
                tracebackpoligon.setMap(null);
            }

            if (typeof myMarkersTasksLocation["Pending"] === 'undefined') {
                // your code here.
            }
            else {
                myMarkersTasksLocation["Pending"].setMap(null);
            }
            if (typeof myMarkersTasksLocation["Completed"] === 'undefined') {
                // your code here.
            }
            else {
                myMarkersTasksLocation["Completed"].setMap(null);
            } 
            if (typeof myMarkersTasksLocation["Rejected"] === 'undefined') {
                // your code here.
            }
            else {
                myMarkersTasksLocation["Rejected"].setMap(null);
            }
            if (typeof myMarkersTasksLocation["Accepted"] === 'undefined') {
                // your code here.
            }
            else {
                myMarkersTasksLocation["Accepted"].setMap(null);
            }
            if (typeof myMarkersTasksLocation["InProgress"] === 'undefined') {
                // your code here.
            }
            else {
                myMarkersTasksLocation["InProgress"].setMap(null);
            }
            if (typeof myMarkersTasksLocation["Cancelled"] === 'undefined') {
                // your code here.
            }
            else {
                myMarkersTasksLocation["Cancelled"].setMap(null);
            }
            if (typeof myMarkersTasksLocation["OnRoute"] === 'undefined') {
                // your code here.
            }
            else {
                myMarkersTasksLocation["OnRoute"].setMap(null);
            }
            var tbcounter = 0;
            for (var i = 0; i < obj.length; i++) {
                var contentString = '<div id="content">' + obj[i].Username + '<br/></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                if (obj[i].Username == "Pending") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    myMarkersTasksLocation[obj[i].Username] = marker;
                    createInfoWindowTaskLocation(marker, contentString);
                }
                else if (obj[i].Username == "InProgress") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                    myMarkersTasksLocation[obj[i].Username] = marker;
                    createInfoWindowTaskLocation(marker, contentString);
                }
                else if (obj[i].Username == "OnRoute") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
                    myMarkersTasksLocation[obj[i].Username] = marker;
                    createInfoWindowTaskLocation(marker, contentString);
                }
                else if (obj[i].Username == "Completed") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                    if (obj[i].State == "GREEN") {
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                    }
                    else {
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerX.png');
                    }
                    myMarkersTasksLocation[obj[i].Username] = marker;
                    createInfoWindowTaskLocation(marker, contentString);
                }
                else if (obj[i].Username == "Accepted") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                    myMarkersTasksLocation[obj[i].Username] = marker;
                    createInfoWindowTaskLocation(marker, contentString);
                }
                else if (obj[i].State == "RED") {
                    if (first) {
                        first = false;
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        poligonCoords.push(point);

                    }
                    else {
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        poligonCoords.push(point);
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                        myTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                    }
                }
                else {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    myMarkersTasksLocation[obj[i].Username] = marker;
                    createInfoWindowTaskLocation(marker, contentString);
                }
            }
            if (poligonCoords.length > 0) {
                tracebackpoligon = new google.maps.Polyline({
                    path: poligonCoords,
                    geodesic: true,
                    strokeColor: '#1b93c0',
                    strokeOpacity: 1.0,
                    strokeWeight: 7,
                    icons: [{
                        icon: iconsetngs,
                        offset: '100%'
                    }]
                });
                tracebackpoligon.setMap(mapTaskLocation);
                animateCircle(tracebackpoligon);
            }
        }
        catch (err) {
            //showAlert(err);
        }
    }
    var firstpresstask = false;

    function showTaskDocument(id) {
        document.getElementById('rowChoiceTasks').value = id;
        document.getElementById("taskinitialOptionsDiv").style.display = "block";
        document.getElementById("taskhandleOptionsDiv").style.display = "none";
        document.getElementById("taskrejectOptionsDiv").style.display = "none";
        var el = document.getElementById('tasklocation-tab');
        if (el) {
            el.className = 'tab-pane fade active in';
        }
        var el2 = document.getElementById('taskrejection-tab');
        if (el2) {
            el2.className = 'tab-pane fade';
        }

        oldDivContainers();

        var retVal = assignrowDataTask(id);
        if (retVal == "Completed")
            TaskIsCompleted();

        taskHistoryData(id);
        taskinsertAttachmentIcons(id);
        taskinsertAttachmentTabData(id);
        taskinsertAttachmentData(id);
        getChecklistItems(id);
        getChecklistItemsNotes(id);
        getCanvasNotes(id);
        infotabDefault();
    }
    function getCanvasNotes(id) {
        document.getElementById("canvasItemsListNotes").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Incident.aspx/getCanvasNotesData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    if (data.d.length > 0) {
                        document.getElementById("pCanvasNotes").style.display = "block";
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("canvasItemsListNotes");
                            var li = document.createElement("li");
                            li.innerHTML = '<i style="margin-left:-15px;" ></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                            ul.appendChild(li);
                            if (res.length > 1) {
                                if (res[1] != '') {
                                    var li2 = document.createElement("li");
                                    li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                    ul.appendChild(li2);
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById("pCanvasNotes").style.display = "none";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
    function getChecklistItemsNotes(id) {
        document.getElementById("checklistItemsListNotes").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Incident.aspx/getChecklistNotesData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    if (data.d.length > 0) {
                        document.getElementById("pchecklistItemsListNotes").style.display = "block";
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("checklistItemsListNotes");
                            var li = document.createElement("li");
                            li.innerHTML = '<i style="margin-left:-15px;" class="fa fa-square-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                            ul.appendChild(li);
                            if (res.length > 1) {
                                if (res[1] != '') {
                                    var li2 = document.createElement("li");
                                    li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                    ul.appendChild(li2);
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById("pchecklistItemsListNotes").style.display = "none";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
    function infotabDefault() {
        var el = document.getElementById('taskactivity-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('taskinfo-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('taskattachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
        var el2 = document.getElementById('taskliInfo');
        if (el2) {
            el2.className = 'active';
        }
        var el5 = document.getElementById('taskliActi');
        if (el5) {
            el5.className = ' ';
        }
        var el6 = document.getElementById('taskliAtta');
        if (el6) {
            el6.className = ' ';
        }
        var el7 = document.getElementById('taskliNotes');
        if (el7) {
            el7.className = ' ';
        }
        var el8 = document.getElementById('tasknotes-tab');
        if (el8) {
            el8.className = 'tab-pane fade';
        }
    }
    function TaskIsCompleted() {
        document.getElementById("taskinitialOptionsDiv").style.display = "block";
        document.getElementById("taskhandleOptionsDiv").style.display = "none";
        document.getElementById("taskrejectOptionsDiv").style.display = "none";
    }
    function incinextImg() {
        var found = false;
        for (var i = 0; i < divArray.length; i++) {
            var el = document.getElementById(divArray[i]);
            if (el.classList.contains("active")) {
                el.className = 'tab-pane fade ';
                if (i + 1 < imgcount) {
                    var el3 = document.getElementById(divArray[i + 1]);
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                else {
                    var el3 = document.getElementById("location-tab");
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            if (divArray.length > 0) {
                var ell = document.getElementById(divArray[0]);
                if (ell) {
                    ell.className = 'tab-pane fade active in';
                }
                var ell1 = document.getElementById("location-tab");
                if (ell1) {
                    ell1.className = 'tab-pane fade';
                }
            }
        }
    }
    function incibackImg() {
        var found = false;
        for (var i = 0; i < divArray.length; i++) {
            var el = document.getElementById(divArray[i]);
            if (el.classList.contains("active")) {
                el.className = 'tab-pane fade ';
                if (i == 0) {
                    var elz = document.getElementById("location-tab");
                    if (elz) {
                        elz.className = 'tab-pane fade active in';
                    }
                }
                else if (i - imgcount < imgcount) {
                    var el3 = document.getElementById(divArray[i - 1]);
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                else {
                    var el3 = document.getElementById("location-tab");
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            if (divArray.length > 0) {
                var ell = document.getElementById(divArray[imgcount - 1]);
                if (ell) {
                    ell.className = 'tab-pane fade active in';
                }
                var ell1 = document.getElementById("location-tab");
                if (ell1) {
                    ell1.className = 'tab-pane fade';
                }
            }
        }
    }
    function tasknextImg() {
        var found = false;
        for (var i = 0; i < divArray.length; i++) {
            var el = document.getElementById(divArray[i]);
            if (el.classList.contains("active")) {
                el.className = 'tab-pane fade ';
                if (i + 1 < imgcount) {
                    var el3 = document.getElementById(divArray[i + 1]);
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                else {
                    var el3 = document.getElementById("tasklocation-tab");
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            if (divArray.length > 0) {
                var ell = document.getElementById(divArray[0]);
                if (ell) {
                    ell.className = 'tab-pane fade active in';
                }
                var ell1 = document.getElementById("tasklocation-tab");
                if (ell1) {
                    ell1.className = 'tab-pane fade';
                }
            }
        }
    }
    function taskbackImg() {
        var found = false;
        for (var i = 0; i < divArray.length; i++) {
            var el = document.getElementById(divArray[i]);
            if (el.classList.contains("active")) {
                el.className = 'tab-pane fade ';
                if (i == 0) {
                    var elz = document.getElementById("tasklocation-tab");
                    if (elz) {
                        elz.className = 'tab-pane fade active in';
                    }
                }
                else if (i - imgcount < imgcount) {
                    var el3 = document.getElementById(divArray[i - 1]);
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                else {
                    var el3 = document.getElementById("tasklocation-tab");
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            if (divArray.length > 0) {
                var ell = document.getElementById(divArray[imgcount - 1]);
                if (ell) {
                    ell.className = 'tab-pane fade active in';
                }
                var ell1 = document.getElementById("tasklocation-tab");
                if (ell1) {
                    ell1.className = 'tab-pane fade';
                }
            }
        }
    }
    var imgcount = 0;
    function taskinsertAttachmentData(id) {
        document.getElementById('taskrotationDIV1').style.display = "none";
        document.getElementById('taskrotationDIV2').style.display = "none";
        $.ajax({
            type: "POST",
            url: "Incident.aspx/taskgetAttachmentData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    imgcount = 0;
                    document.getElementById('taskAudioDIV').style.display = "none";
                    for (var i = 0; i < data.d.length; i++) {
                        if (data.d[i].indexOf("video") >= 0) {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.innerHTML = data.d[i];
                            div.id = 'video-' + (i + 1) + '-tab';
                            document.getElementById('taskdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'video-' + (i + 1) + '-tab';
                            imgcount++;
                        }
                        else {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.align = 'center';
                            div.style.height = '380px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i + 1) + '-tab';
                            document.getElementById('taskdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'image-' + (i + 1) + '-tab';
                            imgcount++;
                        }
                    }
                    if (imgcount > 0) {
                        document.getElementById('taskrotationDIV1').style.display = "block";
                        document.getElementById('taskrotationDIV2').style.display = "block";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }

    function taskinsertAttachmentTabData(id) {
        jQuery('#taskattachments-info-tab div').html('');

        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/taskgetAttachmentDataTab",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskattachments-info-tab").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function taskinsertAttachmentIcons(id) {
        jQuery('#taskdivAttachment div').html('');
        $.ajax({
            type: "POST",
            url: "Incident.aspx/taskgetAttachmentDataIcons",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else
                    document.getElementById("taskdivAttachment").innerHTML = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function taskHistoryData(id) {
        jQuery('#taskdivIncidentHistoryActivity div').html('');
        $.ajax({
            type: "POST",
            url: "Incident.aspx/getTaskHistoryData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('taskdivIncidentHistoryActivity').appendChild(div);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function getTasklistItems(id) {
        document.getElementById("taskItemsList").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Incident.aspx/getTaskListData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskItemsnameSpan").innerHTML = data.d[0];
                    for (var i = 1; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("taskItemsList");
                        var li = document.createElement("li");
                        li.innerHTML = '<a href="#taskDocument"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="showTaskDocument(&apos;' + res[1] + '&apos;)">' + res[0] + '</a>';
                        //li.appendChild(document.createTextNode(data.d[i]));
                        ul.appendChild(li);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function getChecklistItems(id) {
        document.getElementById("checklistItemsList").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Incident.aspx/getChecklistData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    if (data.d[0] == "False") {
                        var el = document.getElementById('checklistnamespanFA');
                        if (el) {
                            el.className = "fa fa-square-o";
                        }
                    }
                    else {
                        var el = document.getElementById('checklistnamespanFA');
                        if (el) {
                            el.className = "fa fa-check-square-o";
                        }
                    }
                    for (var i = 1; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("checklistItemsList");
                        var li = document.createElement("li");
                        var marginLeft = '';
                        if (res[2] == 'True') {
                            marginLeft = 'style = "margin-left:-15px;"';
                        }

                        if (res[2] == '3') {

                            if (res[1] == "Checked")
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];
                            else
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];

                            ul.appendChild(li);

                            var li2 = document.createElement("li");
                            li2.innerHTML = '<a style="margin-left:5px;" href="#"  class="capitalize-text" >Notes: ' + res[3] + '</a>';
                            ul.appendChild(li2);
                        }
                        else {
                            if (res[1] == "Checked")
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];
                            else
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];

                            ul.appendChild(li);
                        }
                        //li.appendChild(document.createTextNode(data.d[i]));

                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
    function insertAttachmentIcons(id) {
        jQuery('#divAttachment div').html('');
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getAttachmentDataIcons",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //for (var i = 0; i < data.d.length; i++) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    document.getElementById("divAttachment").innerHTML = data.d;
                }
                //}
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function insertAttachmentTabData(id) {
        jQuery('#attachments-info-tab div').html('');

        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getAttachmentDataTab",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else
                    document.getElementById("attachments-info-tab").innerHTML = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }

        function taskinsertAttachmentData(id) {
            document.getElementById('taskrotationDIV1').style.display = "none";
            document.getElementById('taskrotationDIV2').style.display = "none";
            $.ajax({
                type: "POST",
                url: "Incident.aspx/taskgetAttachmentData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        imgcount = 0;
                        document.getElementById('taskAudioDIV').style.display = "none";
                        for (var i = 0; i < data.d.length; i++) {
                            if (data.d[i].indexOf("video") >= 0) {
                                var div = document.createElement('div');
                                div.className = 'tab-pane fade';
                                div.innerHTML = data.d[i];
                                div.id = 'video-' + (i + 1) + '-tab';
                                document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                divArray[i] = 'video-' + (i + 1) + '-tab';
                                imgcount++;
                            }
                            else {
                                var div = document.createElement('div');
                                div.className = 'tab-pane fade';
                                div.align = 'center';
                                div.style.height = '380px';
                                div.innerHTML = data.d[i];
                                div.id = 'image-' + (i + 1) + '-tab';
                                document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                divArray[i] = 'image-' + (i + 1) + '-tab';
                                imgcount++;
                            }
                        }
                        if (imgcount > 0) {
                            document.getElementById('taskrotationDIV1').style.display = "block";
                            document.getElementById('taskrotationDIV2').style.display = "block";
                        }
                        
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }



        function insertAttachmentData(id) {
            document.getElementById('incirotationDIV1').style.display = "none";
            document.getElementById('incirotationDIV2').style.display = "none";
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getAttachmentData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    imgcount = 0;
                    document.getElementById('incidentAudioDIV').style.display = "none";
                    for (var i = 0; i < data.d.length; i++) {
                        if (data.d[i].indexOf("video") >= 0) {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.innerHTML = data.d[i];
                            div.id = 'video-' + (i + 1) + '-tab';
                            document.getElementById('divAttachmentHolder').appendChild(div);
                            divArray[i] = 'video-' + (i + 1) + '-tab';
                            imgcount++;
                        } 
                        else {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.align = 'center';
                            div.style.height = '380px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i + 1) + '-tab';
                            document.getElementById('divAttachmentHolder').appendChild(div);
                            divArray[i] = 'image-' + (i + 1) + '-tab';
                            imgcount++;
                        }
                    }
                    if (imgcount > 0) {
                        document.getElementById('incirotationDIV1').style.display = "block";
                        document.getElementById('incirotationDIV2').style.display = "block";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
        function audioincidentplay(sr) {
            document.getElementById('incidentAudioDIV').style.display = "block";
            document.getElementById('incidentAudioSrc').src = sr;
            document.getElementById('incidentAudio').load(); //call this to just preload the audio without playing

        }
        function hideIncidentplay() {
            document.getElementById('incidentAudioDIV').style.display = "none";
        }
        function audiotaskplay(sr) {
            document.getElementById('taskAudioDIV').style.display = "block";
            document.getElementById('taskAudioSrc').src = sr;
            document.getElementById('taskAudio').load(); //call this to just preload the audio without playing

        }
        function hideTaskplay() {
            document.getElementById('taskAudioDIV').style.display = "none";
        }
    function oldDivContainers() {
        try {
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                el.parentNode.removeChild(el);
            }
            divArray = new Array();
        }
        catch (ex) {
            //alert('oldDivContainers-' + ex);
            //alert(ex);
        }
    }
    function incidentHistoryData(id) {
        jQuery('#divIncidentHistoryActivity div').html('');
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getEventHistoryData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('divIncidentHistoryActivity').appendChild(div);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
        function clearAlarmCard() {
            cleardispatchList();
            document.getElementById("MainContent_sendAlarmSelect").value = 0;
            jQuery('#MainContent_sendAlarmSelect').selectpicker('val', 0);
            
        }
    function clearCreateIncidentCard() {
        document.getElementById('tbIReceivedBy').value = "";
        document.getElementById('tbPhone').value = "";
        document.getElementById('tbEmail').value = "";
        document.getElementById('tbIncidentName').value = "";
        document.getElementById('tbIDescription').value = "";
        document.getElementById('instructionTextarea').value = "";
        document.getElementById('tbLongitude').value = "";
        document.getElementById('tbLatitude').value = "";
        document.getElementById('divLongLat').style.display = 'block';
        document.getElementById('divLocSel').style.display = 'block';
        document.getElementById('vertices').value = "";
        cleardispatchList();
        alldeletemarker();
        deleteSelectedShape();
    }
    function updateCustomEventViewed(id) {
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/UpdateCustomEventView",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function assignrowData(id) {
        var output = "";
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getTableRowData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {

                    document.getElementById('escalateionSpanDiv').style.display = 'none';
                    document.getElementById("usernameSpan").innerHTML = data.d[0];
                    document.getElementById("timeSpan").innerHTML = data.d[1];
                    document.getElementById("typeSpan").innerHTML = data.d[2];

                    var ret = data.d[3];
                    var splitRetval = ret.split('-');
                    if (splitRetval.length > 0) {
                        ret = splitRetval[0];
                    }
                    document.getElementById("statusSpan").innerHTML = ret;
                    document.getElementById("locSpan").innerHTML = data.d[4];
                    document.getElementById("receivedBySpan").innerHTML = data.d[5];
                    document.getElementById("phonenumberSpan").innerHTML = data.d[6];
                    document.getElementById("emailSpan").innerHTML = data.d[7];
                    document.getElementById("descriptionSpan").innerHTML = data.d[8];
                    document.getElementById("instructionSpan").innerHTML = data.d[9];
                    document.getElementById("incidentNameHeader").innerHTML = data.d[10];
                    var el = document.getElementById('headerImageClass');
                    if (el) {
                        el.className = data.d[11];
                    }
                    document.getElementById("rowLongitude").text = data.d[12];
                    document.getElementById("rowLatitude").text = data.d[13];
                    output = data.d[3];
                    if (data.d[14] == "TASK") {
                        document.getElementById('checklistDIV').style.display = 'block';
                        getTasklistItems(id);

                        if (data.d[3] == "Escalated" || data.d[3] == "Reject" || data.d[3] == "Resolve" || data.d[3] == "Resolve-MyIncident") {
                            document.getElementById('escalateionSpanDiv').style.display = 'block';
                            document.getElementById("escalationHead").innerHTML = data.d[3] + " Notes";

                            if (data.d[3] == "Resolve-MyIncident")
                                document.getElementById("escalationHead").innerHTML = "Resolve Notes";

                            document.getElementById("escalatedSpan").innerHTML = data.d[15];

                        }
                        document.getElementById('resolveLi').style.display = data.d[16];
                        document.getElementById('disResolveLi').style.display = data.d[16];
                        document.getElementById('compResolveLi').style.display = data.d[16];
                        document.getElementById('compResolveLi2').style.display = data.d[16];
                        document.getElementById("notesSpan").innerHTML = data.d[17];

                        //document.getElementById('rejResolveLi').style.display = data.d[16];
                    }
                    else {
                        if (data.d[3] == "Escalated" || data.d[3] == "Reject" || data.d[3] == "Resolve" || data.d[3] == "Resolve-MyIncident") {
                            document.getElementById('escalateionSpanDiv').style.display = 'block';
                            document.getElementById("escalationHead").innerHTML = data.d[3] + " Notes";

                            if (data.d[3] == "Resolve-MyIncident")
                                document.getElementById("escalationHead").innerHTML = "Resolve Notes";

                            document.getElementById("escalatedSpan").innerHTML = data.d[14];
                        }
                        document.getElementById('resolveLi').style.display = data.d[15];
                        document.getElementById('disResolveLi').style.display = data.d[15];
                        document.getElementById('compResolveLi').style.display = data.d[15];
                        document.getElementById('compResolveLi2').style.display = data.d[15];
                        document.getElementById("notesSpan").innerHTML = data.d[16];
                        //document.getElementById('rejResolveLi').style.display = data.d[15];
                    }
                    updateCustomEventViewed(id);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
    function addrowtoTable2() {
        $("#hoteventTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getTableData2",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#hoteventTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTable3() {
        $("#systemCreatedTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getTableData3",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#systemCreatedTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTable4() {
        $("#requestTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getTableData4",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#requestTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTable5() {
        $("#3rdPartyTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getTableData5",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#3rdPartyTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTable6() {
        $("#incidentTypeTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getTableData6",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#incidentTypeTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }

    function userTableData() {
        jQuery("#usersTable tbody").empty();
        jQuery("#usersTable").dataTable().fnClearTable();
        jQuery("#usersTable").dataTable().fnDraw();
        jQuery("#usersTable").dataTable().fnDestroy();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getUserTableData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#usersTable tbody").append(data.d[i]);

                    }
                    jQuery("#usersTable").DataTable({
                        "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                        'iDisplayLength': 5,
                        "order": [[0, "asc"]]
                    });
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function assignUserTableData() { 
        jQuery("#assignUsersTable tbody").empty();
        jQuery("#assignUsersTable").dataTable().fnClearTable();
        jQuery("#assignUsersTable").dataTable().fnDraw();
        jQuery("#assignUsersTable").dataTable().fnDestroy();
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/getAssignUserTableData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#assignUsersTable tbody").append(data.d[i]);

                    }
                    jQuery("#assignUsersTable").DataTable({
                        "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                        'iDisplayLength': 5,
                        "order": [[0, "asc"]]
                    });
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function insertNewTask(id, assigneetype, assigneename, assigneeid, templatename, longi, lati, incidentId) {
        var output = "";
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/inserttask",
            data: "{'id':'" + id + "','assigneetype':'" + assigneetype + "','assigneename':'" + assigneename + "','assigneeid':'" + assigneeid + "','templatename':'" + templatename + "','longi':'" + longi + "','lati':'" + lati + "','incidentId':'" + incidentId + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else
                    output = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
    function insertNewIncident(name, desc, locationid, incidenttype, notificationid, taskid, receivedby, longi, lati, status, ins, msgtask, phoneNo, emailAdd, imgpath) {
        var output = "";
        jQuery.ajax({
            type: "POST",
            url: "Incident.aspx/insertNewIncident",
            data: "{'name':'" + name + "','desc':'" + desc + "','locationid':'" + locationid + "','incidenttype':'" + incidenttype + "','notificationid':'" + notificationid + "','taskid':'" + taskid + "','receivedby':'" + receivedby + "','longi':'" + longi + "','lati':'" + lati + "','status':'" + status + "','instructions':'" + ins + "','msgtask':'" + msgtask + "','phoneNo':'" + phoneNo + "','emailAdd':'" + emailAdd + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else
                    output = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }

    function dispatchUserchoiceTable(id, name,type) {
        var element = document.getElementById(id + "-" + name); 
        var result = element.innerHTML.indexOf("ADDED");
        if (result < 0) {

            var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
            if (exists == false) {
                var myOption;
                myOption = document.createElement("Option");
                myOption.text = name; //Textbox's value
                myOption.value = id; //Textbox's value
                sendToListBox.add(myOption);
                addnametoUserDispatchList(name, id, type);

                element.style.color = "#3ebb64";
                element.className = "green-color";
                element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
            }
        }
        else {
            var elSel = document.getElementById('sendToListBox');
            var i;
            for (i = elSel.length - 1; i >= 0; i--) {
                if (elSel.options[i].value == id) {
                                           removenameFromDispatchList(elSel.options[i].text);
                        elSel.remove(i);
                    }
                }

                element.style.color = "#b2163b";
                element.className = "red-color";
                element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
            }
        }
        var infoinfowindow;
        function userchoiceTable(id, name,displayname) {
            try {
                var element = document.getElementById(id + name);
                var result = element.innerHTML.indexOf("ADDED");
                if (result < 0) {
                    var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                    if (exists == false) {
                        var myOption;
                        myOption = document.createElement("Option");
                        myOption.text = name; //Textbox's value
                        myOption.value = id; //Textbox's value
                        sendToListBox.add(myOption);
                        addnametoDispatchList(name, id, displayname);

                        element.style.color = "#3ebb64";
                        element.className = "green-color";
                        element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';

                        if (document.getElementById("tbLatitude").value == "" && document.getElementById("tbLongitude").value == "") {
                            jQuery.ajax({
                                type: "POST",
                                url: "Incident.aspx/getLongLatByUserId",
                                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                                async: false,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    if (data.d[0] == "LOGOUT") {
                                        showError("Session has expired. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    } else {
                                        var myLatlng = new google.maps.LatLng(data.d[0], data.d[1] + 4);
                                        var marker = new google.maps.Marker({
                                            position: myLatlng,
                                            map: map,
                                            draggable: true,
                                            title: name
                                        });
                                        marker.setIcon('https://testportalcdn.azureedge.net/Images/gpspinmarker.png');
                                        dispatchMarkers[name] = marker;
                                        showAlert("Please remember to move user marker to desired assigned location!");
                                    }
                                },
                                error: function () {
                                    showError("Session timeout. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            });
                        }
                    }
                }
                else {
                    var elSel = document.getElementById('sendToListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) {
                        if (elSel.options[i].value == id) {
 
                            removenameFromDispatchList(elSel.options[i].text);
                            elSel.remove(i);
                        }
                    }
                    element.style.color = "#b2163b";
                    element.className = "red-color";
                    element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';

                    if (dispatchMarkers[name] != null) {
                        dispatchMarkers[name].setMap(null);
                        dispatchMarkers[name] = null;
                    }
                }
            }
            catch (err) {
                //alert(err)
            }
        }
        function userdispatchChoice(id, name,displayname) {
            var element = document.getElementById(id+name);
            var result = element.innerHTML.indexOf("ADDED");
            if (result < 0) {

                var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                if (exists == false) {
                    var myOption;
                    myOption = document.createElement("Option");
                    myOption.text = name; //Textbox's value
                    myOption.value = id; //Textbox's value
                    sendToListBox.add(myOption);
                                       addnametoUserDispatchList(name, id,displayname);

                    element.style.color = "#3ebb64";
                    element.className = "green-color";
                    element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
                }
            }
            else {
                var elSel = document.getElementById('sendToListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {
                    if (elSel.options[i].value == id) {
                                                removenameFromDispatchList(elSel.options[i].text);
                        elSel.remove(i);
                    }
                }

                element.style.color = "#b2163b";
                element.className = "red-color";
                element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
            }
        }

        var dispatchMarkers = new Array();
        function userchoice2(id, name, displayname) {

            if (dispatchMarkers[name] != null) {
                dispatchMarkers[name].setMap(null);
                dispatchMarkers[name] = null;
            }
            var element = document.getElementById(id +"-"+name);
            var result = element.innerHTML.indexOf("ADDED");
            if (result < 0) {

                var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                if (exists == false) {
                    var myOption;
                    myOption = document.createElement("Option");
                    myOption.text = name; //Textbox's value
                    myOption.value = id; //Textbox's value
                    sendToListBox.add(myOption);
                    addnametoDispatchList(name, id, displayname);

                    element.style.color = "#3ebb64";
                    element.className = "green-borders light-button green-color";
                    element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';

                    if (document.getElementById("tbLatitude").value == "" && document.getElementById("tbLatitude").value == "") {

                        jQuery.ajax({
                            type: "POST",
                            url: "Incident.aspx/getLongLatByUserId",
                            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d[0] == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                } else {
                                    var myLatlng = new google.maps.LatLng(data.d[0], data.d[1] + 4);
                                    var marker = new google.maps.Marker({
                                        position: myLatlng,
                                        map: map,
                                        draggable: true,
                                        title: name
                                    });
                                    marker.setIcon('https://testportalcdn.azureedge.net/Images/gpspinmarker.png');
                                    dispatchMarkers[name] = marker;
                                    //showAlert("Please remember to move user marker to desired assigned location!");
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                }
            }
            else {
                var elSel = document.getElementById('sendToListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {
                    if (elSel.options[i].value == id) {
                        removenameFromDispatchList(elSel.options[i].text);
                        elSel.remove(i);
                    }
                }

                element.style.color = "#b2163b";
                element.className = "red-color";
                element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
            }
        }
        function userchoice(id, name,displayname) {

            if (dispatchMarkers[name] != null) {
                dispatchMarkers[name].setMap(null);
                dispatchMarkers[name] = null;
            }
            var element = document.getElementById(id + name);
            if (element) {
                var result = element.innerHTML.indexOf("ADDED");
                if (result < 0) {

                    var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                    if (exists == false) {
                        var myOption;
                        myOption = document.createElement("Option");
                        myOption.text = name; //Textbox's value
                        myOption.value = id; //Textbox's value
                        sendToListBox.add(myOption);
                        addnametoDispatchList(name, id, displayname);

                        element.style.color = "#3ebb64";
                        element.className = "green-borders light-button green-color";
                        element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';

                        if (document.getElementById("tbLatitude").value == "" && document.getElementById("tbLatitude").value == "") {

                            jQuery.ajax({
                                type: "POST",
                                url: "Incident.aspx/getLongLatByUserId",
                                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                                async: false,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    if (data.d[0] == "LOGOUT") {
                                        showError("Session has expired. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    } else {
                                        var myLatlng = new google.maps.LatLng(data.d[0], data.d[1] + 4);
                                        var marker = new google.maps.Marker({
                                            position: myLatlng,
                                            map: map,
                                            draggable: true,
                                            title: name
                                        });
                                        marker.setIcon('https://testportalcdn.azureedge.net/Images/gpspinmarker.png');
                                        dispatchMarkers[name] = marker;
                                        //showAlert("Please remember to move user marker to desired assigned location!");
                                    }
                                },
                                error: function () {
                                    showError("Session timeout. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            });
                        }
                    }
                }
                else {
                    var elSel = document.getElementById('sendToListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) {
                        if (elSel.options[i].value == id) {
                            removenameFromDispatchList(elSel.options[i].text);
                            elSel.remove(i);
                        }
                    }

                    element.style.color = "#b2163b";
                    element.className = "red-color";
                    element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                }
            }
            else {
                var elSel = document.getElementById('sendToListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {
                    if (elSel.options[i].value == id) {
                        removenameFromDispatchList(elSel.options[i].text);
                        elSel.remove(i);
                    }
                }
            }
        }
        function locationChoice(id, name) {
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getLocationById",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('tbLongitude').value = data.d[0];
                        document.getElementById('tbLatitude').value = data.d[1];
                        document.getElementById('divLongLat').style.display = 'block';
                        document.getElementById('divLocSel').style.display = 'block';
                        document.getElementById('vertices').value = "";
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function verticesChoice(id, name) {
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getLocationById",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('divLocSel').style.display = 'none';
                        document.getElementById('divLongLat').style.display = 'none';

                        document.getElementById('tbLongitude').value = "";
                        document.getElementById('tbLatitude').value = "";
                        document.getElementById('vertices').value = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function locationOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getLocationById",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length < 3) {
                            document.getElementById('tbLongitude').value = data.d[0];
                            document.getElementById('tbLatitude').value = data.d[1];
                            document.getElementById('divLocSel').style.display = 'block';
                            document.getElementById('divLongLat').style.display = 'block';

                            document.getElementById("vertices").value = "";
                            for (var i = 0; i < Object.size(polylinesArray) ; i++) {
                                if (polylinesArray[i] != null) {
                                    polylinesArray[i].setMap(null);
                                }
                            }
                        }
                        else {
                            document.getElementById('divLongLat').style.display = 'none';
                            document.getElementById('tbLongitude').value = "";
                            document.getElementById('tbLatitude').value = "";
                            document.getElementById('vertices').value = data.d;

                            for (var i = 0; i < Object.size(polylinesArray) ; i++) {
                                if (polylinesArray[i] != null) {
                                    polylinesArray[i].setMap(null);
                                }
                            }
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function taskOnChange(e) {
            // alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
        }
        function selectAssigneeTypeChange(e) {
            // alert(e.id + ' ' + e.options[e.selectedIndex].value);
            document.getElementById("assigneeTypeSelectionID").text = e.options[e.selectedIndex].value;
            if (e.options[e.selectedIndex].value == "User") {
                document.getElementById('divSelectUser').style.display = 'block';
                document.getElementById('divSelectDev').style.display = 'none';
                document.getElementById('divSelectGroup').style.display = 'none';
            }
            else if (e.options[e.selectedIndex].value == "Device") {
                document.getElementById('divSelectUser').style.display = 'none';
                document.getElementById('divSelectDev').style.display = 'block';
                document.getElementById('divSelectGroup').style.display = 'none';
            }
            else {
                document.getElementById('divSelectUser').style.display = 'none';
                document.getElementById('divSelectDev').style.display = 'none';
                document.getElementById('divSelectGroup').style.display = 'block';
            }
        }
        function sendAlarmSelectOnChange(e) {
            if (e.options[e.selectedIndex].value != "0") {
                var exists = jQuery("#sendToListBox option[value=" + e.options[e.selectedIndex].value + "]").length > 0;
                if (exists == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Incident.aspx/getLongLatByUserId",
                        data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                if (document.getElementById("assigneeTypeSelectionID").text != "Group") {
                                    var myLatlng = new google.maps.LatLng(data.d[0], data.d[1] + 4);
                                    var myOption;
                                    myOption = document.createElement("Option");
                                    myOption.text = data.d[2]; //Textbox's value
                                    myOption.value = e.options[e.selectedIndex].value; //Textbox's value
                                    sendToListBox.add(myOption);
                                    addnametoAlarmList(data.d[2], e.options[e.selectedIndex].value, e.options[e.selectedIndex].text);
                                }
                                else {
                                    var myLatlng = new google.maps.LatLng(data.d[0], data.d[1] + 4);
                                    var myOption;
                                    myOption = document.createElement("Option");
                                    myOption.text = e.options[e.selectedIndex].text; //Textbox's value
                                    myOption.value = e.options[e.selectedIndex].value; //Textbox's value
                                    sendToListBox.add(myOption);
                                    addnametoAlarmList(e.options[e.selectedIndex].text, e.options[e.selectedIndex].value, e.options[e.selectedIndex].text);
                                }
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
        }
        function searchSelectOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].text); // display
            var exists = jQuery("#sendToListBox option[value=" + e.options[e.selectedIndex].value + "]").length > 0;
            if (exists == false) {
                var myOption;
                myOption = document.createElement("Option");
                myOption.text = e.options[e.selectedIndex].text; //Textbox's value
                myOption.value = e.options[e.selectedIndex].value; //Textbox's value
                sendToListBox.add(myOption);
                               addnametoDispatchList(e.options[e.selectedIndex].text, e.options[e.selectedIndex].value);
            }
        }
        function addnametoAlarmList(name, id,uname) {
            var ul = document.getElementById("alarmToSendList");
            var li = document.createElement("li");
            li.setAttribute("id", "li-" + name);
            li.innerHTML = '<a href="#"  class="capitalize-text" >' + uname + '<i class="fa fa-close" onclick="liOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;)"></i></a>';
            ul.appendChild(li);
        }
        function addnametoDispatchList(name, id,displayname) {
            var ul = document.getElementById("toDispatchList");
            var li = document.createElement("li");
            li.setAttribute("id", "li-" + name);
            li.innerHTML = '<a href="#"  class="capitalize-text" >' + displayname + '<i class="fa fa-close" onclick="liOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;)"></i></a>';
            ul.appendChild(li);
        }
        function addnametoUserDispatchList(name, id, dname) {
            document.getElementById("UsersToDispatchListDIV").style.display = "block";
            var ul = document.getElementById("UsersToDispatchList");
            var li = document.createElement("li");
            li.setAttribute("id", "li-" + name);
            li.innerHTML = '<a href="#"  class="capitalize-text" >' + dname + '<i class="fa fa-close" onclick="usersliOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;)"></i></a>';
            ul.appendChild(li);
        }
        function deletemarker(name) {
            try {
                for (var i = 0; i < markers2.length; i++) {
                    if (markers2[i] != null) {
                        if (markers2[i].id == name) {
                            markers2[i].setMap(null);
                            markers2[i] = null;
                            break;
                        }
                    }
                }
            }
            catch (err) {
               // alert('deletemarker-' + err);
                //alert(err);
            }
        }
        function liOnclickRemove(name, id) {
            deletemarker(name); 
            userchoice(id, name);
            removenameFromDispatchList(name);
            removeFromList(name);
            var thismarker = myMarkers[name];
            thismarker.infoWindow.close(); 
        }
        function usersliOnclickRemove(name, id) {
            userchoice2(id, name);
            removeFromList(name);
            removenameFromDispatchList(name);
            var thismarker = myMarkersIncidentLocation[name];
            thismarker.infoWindowIncidentLocation.close();
        }
        function removenameFromDispatchList(name) {
            var element = document.getElementById("li-" + name);
            element.parentNode.removeChild(element);
        }
        function resizeend() {
            if (new Date() - rtime < delta) {
                setTimeout(resizeend, delta);
            } else {
                timeout = false;
                Initialize();
            }
        }
        jQuery(window).resize(function () {
            rtime = new Date();
            if (timeout === false) {
                timeout = true;
                setTimeout(resizeend, delta);
            }
        });
        function createInfoWindow(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent(popupContent);
                infoWindow.open(map, this);
            });
        }
        function cleardispatchList() {
            try {
                var elSel = document.getElementById('sendToListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {
                    var element = document.getElementById("li-" + elSel.options[i].text);
                    var element2 = document.getElementById(elSel.options[i].value + "-" + elSel.options[i].text);
                    if (element2) {
                        element2.style.color = "#b2163b";
                        element2.className = "red-color";
                        element2.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                    }
                    if (element) {
                        element.parentNode.removeChild(element);
                        elSel.remove(i);
                    }
                }
            }
            catch (err) {
                showError('Error 42: Failed to clear dispatch list-' + err);
            }
        }
        function removeFromList(name) {
            var elSel = document.getElementById('sendToListBox');
            var i;
            for (i = elSel.length - 1; i >= 0; i--) {
                if (elSel.options[i].text == name) {

                    elSel.remove(i);
                }
            }
        }
        var canclickonmap = false;
        var canclickonmapName;
        function clickonmap(name) {
            var markerfound = false;
            try {
                for (var i = 0; i < markers2.length; i++) {
                    if (markers2[i] != null) {
                        if (markers2[i].id == canclickonmapName) {
                            markerfound = true;
                            break;
                        }
                    }
                }
                if (markerfound) {
                    canclickonmap = false;
                }
                else {
                    canclickonmapName = name;
                    canclickonmap = true;
                }
            }
            catch (err) {
                showError('Error 60: Problem loading page element.-' + err);
            }

        }
        function createInfoWindowLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindowLocation.setContent(popupContent);
                infoWindowLocation.open(mapLocation, this);
            });
        }
        function getLocation(obj) {


            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 8,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            var poligonCoords = [];
            var first = true;
            if (document.getElementById("vertices").value != "") {

                google.maps.event.addListener(map, 'click', function (event) {
                    if (canclickonmap) {
                        var location = event.latLng;

                        //Create a marker and placed it on the map.
                        var marker2 = new google.maps.Marker({
                            position: location,
                            map: map
                        });
                        var el = document.getElementById('li-' + canclickonmapName);
                        if (el) {
                            el.className = 'active';
                        }
                        marker2.id = canclickonmapName;
                        marker2.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        markers2.push(marker2);
                        //Attach click event handler to the marker.
                        google.maps.event.addListener(marker2, "click", function (e) {
                            var infoWindow = new google.maps.InfoWindow({
                                content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                                //'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value=' + canclickonmapName + '></input>'
                            });
                            infoWindow.open(map, marker2);
                        });
                        canclickonmap = false;
                    }
                });

                var res = document.getElementById("vertices").value.split(",");
                for (var i = 0; i < res.length; i++) {
                    var point = new google.maps.LatLng(res[i].replace('(', ''), res[i + 1].replace(')', ''));
                    if (first) {
                        var contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">Incident Location</p>';
                        var marker = new google.maps.Marker({ position: point, map: map, title: "Incident Location" });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png');
                        myMarkers["Incident Location"] = marker;
                        createInfoWindow(marker, contentString);
                        first = false;
                    }
                    poligonCoords.push(point);
                    i = i + 1
                }
                if (poligonCoords.length > 0) {
                    var poligon = new google.maps.Polyline({
                        path: poligonCoords,
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });
                    poligon.setMap(map);
                }
            }
            else {
                var contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">Incident Location</p>';
                var myLatlng = new google.maps.LatLng(document.getElementById("tbLatitude").value, document.getElementById("tbLongitude").value);
                var marker = new google.maps.Marker({ position: myLatlng, map: map, title: "Incident Location" });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/tool.png')
                myMarkers["Incident Location"] = marker;
                createInfoWindow(marker, contentString);
            }

            for (var i = 0; i < obj.length; i++) {

                var isuser = false;
                if (obj[i].hasOwnProperty('DisplayName')) {
                    isuser = true;
                }

                var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].DisplayName + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '" onclick="userchoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;,&apos;' + obj[i].DisplayName + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                if (isuser) {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].DisplayName });
                }
                else {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username });
                }

                if (obj[i].State == "YELLOW") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                }
                else if (obj[i].State == "GREEN") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/useronline.png')
                }
                else if (obj[i].State == "BLUE") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                }
                else if (obj[i].State == "OFFUSER") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/useroffline.png')
                }
                else if (obj[i].State == "OFFCLIENT") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                }
                myMarkers[obj[i].Username] = marker;
                createInfoWindow(marker, contentString);
            }

            //}, 1000);

        }
        var drawingManager;
        var markers2 = [];
        var uniqueId = 1;
        var selectedShape;
        var sourceLat = '<%=sourceLat%>';
        var sourceLon = '<%=sourceLon%>';
        function getLocationNoMarkers() {

            try {
                locationAllowed = true;
                //setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                mapLocation = new google.maps.Map(document.getElementById("map_canvasLocation"), mapOptions);

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                mapLocation.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                mapLocation.addListener('bounds_changed', function () {
                    searchBox.setBounds(mapLocation.getBounds());
                });

                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }


                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function (place) {

                        alldeletemarker();
                        deleteSelectedShape();
                        if (!place.geometry) {
                            console.log("Returned place contains no geometry");
                            return;
                        }
                        var icon = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        var marker2 = new google.maps.Marker({
                            position: place.geometry.location,
                            title: place.name,
                            map: mapLocation
                        });
                        marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                        //Attach click event handler to the marker.
                        google.maps.event.addListener(marker2, "click", function (e) {
                            var infoWindow = new google.maps.InfoWindow({
                                content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + place.geometry.location.lat() + '&apos;,&apos;' + place.geometry.location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                            });
                            infoWindow.open(mapLocation, marker2);
                        });
                         
                        markers2.push(marker2);

                        drawingManager.setDrawingMode(null);

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }

                    });
                    mapLocation.fitBounds(bounds);
                });


                google.maps.event.addListener(mapLocation, 'click', function (event) {
                    alldeletemarker();
                    deleteSelectedShape();
                    jQuery('#vertices').val('');
                    document.getElementById('divLocSel').style.display = 'block';
                    document.getElementById('divLongLat').style.display = 'block';

                    var location = event.latLng;

                    //Create a marker and placed it on the map.
                    var marker2 = new google.maps.Marker({
                        position: location,
                        map: mapLocation
                    });
                    marker2.id = uniqueId;
                    marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                    uniqueId++;
                    markers2.push(marker2);
                    //Attach click event handler to the marker.
                    google.maps.event.addListener(marker2, "click", function (e) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                        });
                        infoWindow.open(mapLocation, marker2);
                    });
                });
            }
            catch (err) {
            }
        }
        function getLocationMarkers(obj) {
            try {


                locationAllowed = true;
                //setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                mapLocation = new google.maps.Map(document.getElementById("map_canvasLocation"), mapOptions);

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                mapLocation.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                mapLocation.addListener('bounds_changed', function () {
                    searchBox.setBounds(mapLocation.getBounds());
                });

                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }


                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function (place) {

                        alldeletemarker();
                        deleteSelectedShape();
                        if (!place.geometry) {
                            console.log("Returned place contains no geometry");
                            return;
                        }
                        var icon = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        var marker2 = new google.maps.Marker({
                            position: place.geometry.location,
                            title: place.name,
                            map: mapLocation
                        });
                        marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                        //Attach click event handler to the marker.
                        google.maps.event.addListener(marker2, "click", function (e) {
                            var infoWindow = new google.maps.InfoWindow({
                                content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + place.geometry.location.lat() + '&apos;,&apos;' + place.geometry.location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                            });
                            infoWindow.open(mapLocation, marker2);
                        });

                        // Create a marker for each place.
                        //markers.push(new google.maps.Marker({
                        //    map: mapLocation,
                        //    icon: icon,
                        //    title: place.name,
                        //    position: place.geometry.location
                        //}));

                        markers2.push(marker2);

                        drawingManager.setDrawingMode(null);

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }

                    });
                    mapLocation.fitBounds(bounds);
                });


                google.maps.event.addListener(mapLocation, 'click', function (event) {
                    alldeletemarker();
                    deleteSelectedShape();
                    jQuery('#vertices').val('');
                    document.getElementById('divLocSel').style.display = 'block';
                    document.getElementById('divLongLat').style.display = 'block';

                    var location = event.latLng;

                    //Create a marker and placed it on the map.
                    var marker2 = new google.maps.Marker({
                        position: location,
                        map: mapLocation
                    });
                    marker2.id = uniqueId;
                    marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                    uniqueId++;
                    markers2.push(marker2);
                    //Attach click event handler to the marker.
                    google.maps.event.addListener(marker2, "click", function (e) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                        });
                        infoWindow.open(mapLocation, marker2);
                    });
                });
                var secondfirst = true;
                var currentSubLocation;
                var subpoligonCoords = [];
                var polyCounter = 0;
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="locationChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    if (obj[i].State == "PURPLE") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png')
                        myMarkersLocation[obj[i].Username] = marker;
                        createInfoWindowLocation(marker, contentString);
                    }
                    else {
                        var contentString2 = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="verticesChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                        if (secondfirst) {

                            currentSubLocation = obj[i].State;
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/sublocation.png');
                            myMarkersLocation[obj[i].Username] = marker;
                            createInfoWindowLocation(marker, contentString2);
                            secondfirst = false;
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                        else {
                            if (currentSubLocation == obj[i].State) {
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                subpoligonCoords.push(point);
                            }
                            else {
                                if (subpoligonCoords.length > 0) {
                                    var subpoligon = new google.maps.Polyline({
                                        path: subpoligonCoords,
                                        geodesic: true,
                                        strokeColor: '#FF0000',
                                        strokeOpacity: 1.0,
                                        strokeWeight: 2
                                    });
                                    subpoligon.setMap(mapLocation);
                                    polylinesArray[polyCounter] = subpoligon;
                                    polyCounter++;
                                }
                                subpoligonCoords = [];
                                currentSubLocation = obj[i].State;
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/sublocation.png');
                                myMarkersLocation[obj[i].Username] = marker;
                                createInfoWindowLocation(marker, contentString2);
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                subpoligonCoords.push(point);
                            }
                        }
                    }
                }
                if (subpoligonCoords.length > 0) {
                    var subpoligon = new google.maps.Polyline({
                        path: subpoligonCoords,
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });
                    subpoligon.setMap(mapLocation);
                    polylinesArray[polyCounter] = subpoligon;
                    polyCounter++;
                }
                drawingManager = new google.maps.drawing.DrawingManager({
                    drawingMode: google.maps.drawing.OverlayType.POLYLINE,
                    drawingControl: true,
                    drawingControlOptions: {
                        position: google.maps.ControlPosition.TOP_CENTER,
                        drawingModes: [google.maps.drawing.OverlayType.POLYLINE]
                    }
                });
                drawingManager.setMap(mapLocation);
                google.maps.event.addListener(drawingManager, "overlaycomplete", function (event) {
                    alldeletemarker();
                    deleteSelectedShape();
                    jQuery('#vertices').val('');
                    jQuery('#vertices').val(event.overlay.getPath().getArray());
                    document.getElementById("tbLatitude").value = "";
                    document.getElementById("tbLongitude").value = "";
                    document.getElementById('divLocSel').style.display = 'none';
                    document.getElementById('divLongLat').style.display = 'none';

                    var newShape = event.overlay;
                    newShape.type = event.type;
                    setSelection(newShape);
                    drawingManager.setDrawingMode(null);
                });

                //}, 1000);

            }
            catch (err)
            {
                showError('Error 43: Problem faced during getting markers-' + err);
            }
        }
        function alldeletemarker() {
            try {
                for (var i = 0; i < markers2.length; i++) {
                    markers2[i].setMap(null);
                    markers2[i] = null;
                }
                markers2 = [];
            }
            catch (err) {
                showError('Error 45: Problem faced during deleting of markers-' + err);
            }
        }
        function useLocation(lat, long) {
            document.getElementById("tbLatitude").value = lat;
            document.getElementById("tbLongitude").value = long;
            document.getElementById('divLocSel').style.display = 'block';
            document.getElementById('divLongLat').style.display = 'block';
            document.getElementById("pac-input").value = "";
        }
        function deleteSelectedShape() {
            if (selectedShape) {
                selectedShape.setMap(null);
            }
        }
        function setSelection(shape) {
            selectedShape = shape;
        }
        function getLocationNoOnline() {


            locationAllowed = true;
            setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

            }, 1000);
        }
        function Initialize() {
            jQuery.ajax({
                type: "POST",
                url: "Incident.aspx/getGPSData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "]") {
                        getLocationNoOnline();
                    }
                    else {
                        var obj = jQuery.parseJSON(data.d)
                        getLocation(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function showAllInci() {
            document.getElementById('t1d').style.display = "none";
            document.getElementById('t2d').style.display = "none";
            document.getElementById('t3d').style.display = "none";
            document.getElementById('t4d').style.display = "none";
            document.getElementById('t5d').style.display = "none";
        }

    </script>
    <!-- ============================================
    MAIN CONTENT SECTION
    =============================================== -->
        <section class="content-wrapper" role="main">
            <div class="content">
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">Incidents</span></h3>
                                </div>
                                <div class="col-md-7">
                                    <div class="panel-control">
                                        <ul  class="nav nav-tabs nav-main">
                                            <li class="active"><a data-toggle="tab" href="#home-tab" onclick="showAllInci()">Mobile Incidents</a>
                                            </li>
                                            <li><a data-toggle="tab" href="#sytem-tab" onclick="jQuery('.show-component').show();">System Generated</a>
                                            </li>
                                            <li style="display:none;"><a data-toggle="tab" href="#3rdParty-tab" onclick="jQuery('.show-component').show();">3rd party</a>
                                            </li>
                                            <li><a data-toggle="tab" href="#incidentType-tab" onclick="jQuery('.show-component').show();">Settings</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div role="group" class="pull-right">
                                        <%=siteName%>
                                        <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                        <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                            <div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                        <li class="active"><a href="#component-all-incident" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-my-incident"  data-toggle="tab">My Incidents</a>
                                                    </li>
                                                    <li><a href="#component-number-1"  data-toggle="tab">Mobile Hot Events</a>
                                                    </li>
                                                    <li style="display:none"><a href="#component-number-2"  data-toggle="tab">Hot Events</a>
                                                    </li>
                                                    <li <%=requestDisplay%>><a href="#component-number-3"  data-toggle="tab">Requests</a>
                                                    </li>
                                                    <li><a href="#component-number-4"  data-toggle="tab">Resolved</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark" style="display:<%=dispatchDisplay%>;">
                                                    <li ><a href="#" data-target="#newDocument" data-toggle="modal" onclick="clearCreateIncidentCard()" class="capitalize-text">+ NEW INCIDENT</a>
                                                    </li>

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row horizontal-chart">

                                            <div class="col-md-15 text-center panel-seperator panel-heading">
                                                <h3 class="panel-title capitalize-text">STATUS</h3>
                                            </div>

                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PENDING</p>
                                                </div>
                                                <div class="inline-block">
                                                   <div class="easyPieChart" data-percent="<%=pendingPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f44e4b">
                                                        <span class="percentage text-dark fa fa-1x">
                                                        <span class="data-percent"><asp:Label ID="lbPendingpercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                   </div>
                                                </div>
                                                <div class="inline-block ">
                                                    <h3><asp:Label ID="lbPending" runat="server"></asp:Label></h3>

                                                </div>
                                            </div>
                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PROGRESS</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart"  data-percent="<%=inprogressPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f2c400">
                                                        <span class="percentage text-dark fa fa-1x">
                                                          <span class="data-percent"><asp:Label ID="lbInprogresspercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbInprogress" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">COMPLETED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" data-percent="<%=completedPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#3ebb64">
                                                        <span class="percentage text-dark fa fa-1x">
                                                                        <span class="data-percent"><asp:Label ID="lbCompletedpercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbCompleted" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 chart-total">
                                                <p><span class="capitalize-text"><asp:Label ID="lbTotalAlarms" runat="server"></asp:Label></span>TOTAL INCIDENTS</p>
                                            </div>
                                        </div>

                                        <div class="row show-component component-all-incident">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ALL INCIDENTS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" order-of-rows="desc" order-of-column="4" id="allincidentTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                
                                                                
                                                                
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div> 
                                        <div id="t1d" style="display:none" class="row show-component component-my-incident">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MY INCIDENTS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" order-of-rows="desc" order-of-column="4" id="myincidentTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">HANDLED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div id="t2d" style="display:none" class="row show-component component-number-1">
                                                <incidentTableUC:MyIncidentTableUC id="MobileHotEventTableControl" runat="server" />
                                        </div>
                                        <!-- /.table -->
                                         <div id="t3d" style="display:none" class="row show-component component-number-2">
                                            <incidentTableUC:MyIncidentTableUC id="HotEventTableControl" runat="server" />
                                        </div>
                                        <!-- /.table -->
                                        <div id="t4d" style="display:none" class="row show-component component-number-3">
                                            <div class="col-md-12" <%=requestDisplay%>>
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">REQUESTS</h3>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<%=RequestHandledPercentage%>" aria-valuemin="1" aria-valuemax="100" style="width: <%=RequestHandledPercentage%>%">
                                                                    </div>
                                                                </div>                                                          
                                                            </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"><%=RequestHandledPercentage%>% Handled</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" order-of-rows="desc" order-of-column="4" id="requestTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                               
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div id="t5d" style="display:none" class="row show-component component-number-4">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">RESOLVED</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="resolvedTable" order-of-rows="desc" order-of-column="4"  role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="tab-pane fade" id="sytem-tab">
                              <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark" style="display:<%=dispatchDisplay%>;">
                                                    <li ><a href="#"  data-target="#newDocument" data-toggle="modal" class="capitalize-text">+ NEW INCIDENT</a>
                                                    </li>

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row horizontal-chart">

                                            <div class="col-md-15 text-center panel-seperator panel-heading">
                                                <h3 class="panel-title capitalize-text">STATUS</h3>
                                            </div>

                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PENDING</p>
                                                </div>
                                                <div class="inline-block">
                                                   <div class="easyPieChart" data-percent="<%=pendingSystemPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f44e4b">
                                                        <span class="percentage text-dark fa fa-1x">
                                                        <span class="data-percent"><%=pendingSystemPercent%></span>%
                                                        </span>
                                                   </div>
                                                </div>
                                                <div class="inline-block ">
                                                    <h3><asp:Label ID="lbPendingSystemPercent" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PROGRESS</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart"  data-percent="<%=inprogressSystemPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f2c400">
                                                        <span class="percentage text-dark fa fa-1x">
                                                          <span class="data-percent"><%=inprogressSystemPercent%></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbInprogressSystemPercent" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">COMPLETED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" data-percent="<%=completedSystemPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#3ebb64">
                                                        <span class="percentage text-dark fa fa-1x">
                                                                        <span class="data-percent"><%=completedSystemPercent%></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbCompletedSystemPercent" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 chart-total">
                                                <p><span class="capitalize-text"><asp:Label ID="lbTotalSystemCreated" runat="server"></asp:Label></span>TOTAL SYSTEM CREATED</p>
                                            </div>
                                        </div>
                                        <div class="row show-component component-number-4">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">SYSTEM CREATED</h3>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<%=systemHandledPercentage%>" aria-valuemin="1" aria-valuemax="100" style="width: <%=systemHandledPercentage%>%">
                                                                    </div>
                                                                </div>                                                          
                                                            </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"><%=systemHandledPercentage%>% Handled</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="systemCreatedTable" role="grid" order-of-rows="desc" order-of-column="4">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
																<th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="tab-pane fade" id="3rdParty-tab">
                                                              <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark" style="display:<%=dispatchDisplay%>;">
                                                    <li ><a href="#" data-target="#newDocument" data-toggle="modal" class="capitalize-text">+ NEW INCIDENT</a>
                                                    </li>

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div id="incieventStatusLastHeader" class="row horizontal-chart">

                                            <div class="col-md-15 text-center panel-seperator panel-heading">
                                                <h3 class="panel-title capitalize-text">STATUS</h3>
                                            </div>

                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PENDING</p>
                                                </div>
                                                <div class="inline-block">
                                                   <div class="easyPieChart" data-percent="<%=pending3rdPartyPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f44e4b">
                                                        <span class="percentage text-dark fa fa-1x">
                                                        <span class="data-percent"><%=pending3rdPartyPercent%></span>%
                                                        </span>
                                                   </div>
                                                </div>
                                                <div class="inline-block ">
                                                    <h3><asp:Label ID="lbpending3rdParty" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PROGRESS</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart"  data-percent="<%=inprogress3rdPartyPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f2c400">
                                                        <span class="percentage text-dark fa fa-1x">
                                                          <span class="data-percent"><%=inprogress3rdPartyPercent%></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbinprogress3rdParty" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">COMPLETED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" data-percent="<%=completed3rdPartyPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#3ebb64">
                                                        <span class="percentage text-dark fa fa-1x">
                                                                        <span class="data-percent"><%=completed3rdPartyPercent%></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbcompleted3rdParty" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 chart-total">
                                                <p><span class="capitalize-text"><asp:Label ID="lbtotal3rdParty" runat="server"></asp:Label></span>TOTAL 3RD PARTY</p>
                                            </div>
                                        </div>
                                        <div class="row show-component component-number-4">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">3RD PARTY</h3>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<%=thirdpartyHandledPercentage%>" aria-valuemin="1" aria-valuemax="100" style="width: <%=thirdpartyHandledPercentage%>%">
                                                                    </div>
                                                                </div>                                                          
                                                            </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"><%=thirdpartyHandledPercentage%>% Handled</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="3rdPartyTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="tab-pane fade" id="incidentType-tab">
                              <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                       <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-number-5"  data-toggle="tab">Incident Type</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#" data-target="#newIncidentType" data-toggle="modal" class="capitalize-text">+ NEW TYPE</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-number-5">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">INCIDENT TYPE</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="incidentTypeTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                  <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="CREATEDBY">CREATED BY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div  class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>   
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                  			<label class="select select-o" >
                                                                                <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>      
                                                            <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                                      <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Contract</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                   <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                         <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                        
                                            </div>
                                                         <div class="row" style="display:none;">
                                            <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </div>
                <!-- /panel-body -->
            </div>
            <!-- /.panel -->
            <div aria-hidden="true" aria-labelledby="newDocument" role="dialog" tabindex="-1" id="newDocument" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">                 
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                      <h4 class="modal-title capitalize-text"><%=CreateNewIncident%></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input placeholder="<%=Receivedby%>" id="tbIReceivedBy" <%=direction%> class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input placeholder="<%=PhoneNumber%>" id="tbPhone" <%=direction%> class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input placeholder="<%=Email%>" id="tbEmail" <%=direction%> class="form-control">
                                    </div>
                                </div>
                                <div class="row" id="divLocSel">
                                    <div class="col-md-12">
                                        <label class="select select-o" <%=direction%>>
                                            <select id="locationSelect"  onchange="locationOnChange(this);" runat="server">
                                            </select>
                                         </label>
                                    </div>
                                </div>
                                <div class="row" id="divLongLat"> 
                                    <div class="col-md-6">
                                        <input id="tbLongitude" placeholder="<%=Longi%>" <%=direction%> class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <input id="tbLatitude" placeholder="<%=Lat%>" <%=direction%>  class="form-control">
                                    </div>                                  
                                </div>      
                                <div class="row">
                                    <div class="col-md-12">
                                        <input placeholder="<%=IncidentName%>" <%=direction%> id="tbIncidentName" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="<%=IncidentDescription%>" <%=direction%> id="tbIDescription" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>                              
                            </div>
                            <div class="col-md-8">
                                <input id="pac-input" class="controls" type="text" placeholder="Search Location">
                                <div id="map_canvasLocation" style="width:100%;height:442px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs">
                                    <li ><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" data-dismiss="modal" ><%=Next%></a>
                                    </li>
                                    <li  id="liLoadSaved" ><a href="#" class="capitalize-text" onclick="loadSavedLocation()" ><%=ClearSavedLocation%></a>
                                    </li>
                                    <li  id="liClearSaved" style="display:none;"><a href="#" class="capitalize-text" onclick="clearSavedLocation()">Load Saved Location</a>
                                    </li>
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>     
            <div aria-hidden="true" aria-labelledby="newDocument2" role="dialog" tabindex="-1" id="newDocument2" class="modal fade videoModal" style="display: none;">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">                 
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                      <h4 class="modal-title capitalize-text"><%=CreateNewIncident%></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                 <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:442px;">
                                <div class="row" style="display:none;">
                                    <div class="col-md-12">
                                    <select id="searchSelect" class="selectpicker form-control" onchange="searchSelectOnChange(this);" data-live-search="true" runat="server">
                                    </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row horizontal-navigation">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs" id="toDispatchList">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="red-color"><b><%=dispatchoptions%></b></p>
                                <div class="row" style="display:none;">
                                      <div class="col-md-4">
                                           <div class='row nice-checkbox'>
                                               <input onclick="showCamDuration()" id='newcameraCheck' type='checkbox' name='niceCheck'>
                                               <label for='newcameraCheck'><%=Cameras%></label>
                                           </div>
                                       </div>
                                    <div class="col-md-8">
                                       <div class="row mb-1x" id="incidentCameraSelectDiv">
                                         <label class="select select-o">
                                            <select id="newCameraSelect" runat="server">
                                            </select>
                                         </label>
                                       </div>

                                    </div>
                                </div>
                                <div id="camDurationDiv" style="display:none;">
                                    <div style="margin-bottom:25px" class="row horizontal-navigation text-center">   
                                        Access Duration:                                              
                                        <a style="font-size:14px;" href="#" onclick="durationselect(15)" ><i id="camfilter1" class="fa fa-square-o"></i>15 MIN</a>
                                       |<a style="font-size:14px;" href="#" onclick="durationselect(30)"><i id="camfilter2" class="fa fa-square-o"></i>30 MIN</a>
									                                             </div>
                                <div  style="margin-top:-14px;margin-bottom:25px" class="row horizontal-navigation text-center">
                                    <a style="font-size:14px;" href="#" onclick="durationselect(45)"><i id="camfilter3" class="fa fa-square-o"></i>45 MIN</a>
									          
									            |<a style="font-size:14px;" href="#" onclick="durationselect(60)"><i id="camfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                |<a style="font-size:14px;" href="#" onclick="durationselect(120)"><i id="camfilter5" class="fa fa-square-o"></i>2 HOUR</a>
                                </div>
                                </div>
                                <div class="row" <%=taskDisplay%>>
                                      <div class="col-md-4">
                                           <div class='row nice-checkbox' >
                                               <input id='newTaskCheck' type='checkbox' name='niceCheck'>
                                               <label for='newTaskCheck'><%=Task%></label>
                                           </div>
                                          </div>
                                                                      <div class="col-md-8">
                                                                                                                 <div class="row">
                                        <label class="select select-o">
                                            <select id="taskSelect"  runat="server">
                                            </select>
                                         </label>
                                        </div>
                                                                          </div>
                                                                    </div>
                                <div class="row"  style="display:none;"> 

                                               <div class="text-center">Attachment Photo</div>

                                    </div>
                                <div class="row" >
                                                      <div style="height:50px;" enctype="multipart/form-data" id="dz-test" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                                        <div class="dz-message">
                                                          <h1>BROWSE</h1>
                                                        </div>
                                                      </div>
                                         </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea <%=direction%> placeholder="<%=Instructions%>" id="instructionTextarea" class="form-control" rows="6"></textarea>
                                    </div>
                                </div>  
                               <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                               <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                               </div>                            
                            </div>
                            <div class="col-md-8">
                                <div class="tab-content">
                                <div class="tab-pane fade active in" id="divLocTabs">
                                    <div id="map_canvas" style="width:100%;height:442px;"></div>
                                </div>
                                <div class="tab-pane fade " id="assign-user-tab">
                                                
                                  <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MY USERS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive"> 
                                                        <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="usersTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">USER
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <%--<th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">LOCATION<i class="fa fa-sort ml-2x"></i>
                                                                </th>--%>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">ACTION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                               
                                </div>
                                    </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs">
                                    <li><a href="#" class="capitalize-text" data-target="#newDocument" data-toggle="modal" data-dismiss="modal"><%=Back%></a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="dispatchNewIncident('Park')"><%=Park%></a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="dispatchNewIncident('Dispatch')"><%=Dispatch%></a>
                                    </li>
                                    <li  id="liIncidentDispatchUser"><a href="#assign-user-tab" data-toggle="tab" onclick="dispatchuserTab()" class="capitalize-text"><%=DispatchFromUsers%></a>
                                    </li>
                                    <li id="liIncidentDispatchMap" style="display:none;"><a href="#divLocTabs" data-toggle="tab" onclick="dispatchmapTab()" class="capitalize-text">DISPATCH FROM MAP</a>
                                    </li>
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="closeModal();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="newAlarm" role="dialog" tabindex="-1" id="newAlarm" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                      <h4 class="modal-title capitalize-text">SEND AN ALARM</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="select select-o">
                                           <select id="selectAssigneeType" onchange="selectAssigneeTypeChange(this);">
                                              <option>User</option>
                                              <option style="display:none;">Device</option>
                                              <option>Group</option>
                                            </select>
                                         </label>
                                    </div>
                                    <div id="divSelectUser"  class="col-md-6">
                                            <select id="sendAlarmSelect" class="selectpicker form-control" onchange="sendAlarmSelectOnChange(this);" data-live-search="true" runat="server">
                                            </select>
                                    </div>
                                   <div id="divSelectDev" style="display:none;" class="col-md-6">
                                            <select id="sendDeviceSelect" class="selectpicker form-control" onchange="sendAlarmSelectOnChange(this);" data-live-search="true" runat="server">
                                            </select>
                                    </div>
                                    <div id="divSelectGroup" style="display:none;" class="col-md-6">
                                            <select id="sendGroupSelect" class="selectpicker form-control" onchange="sendAlarmSelectOnChange(this);" data-live-search="true" runat="server">
                                            </select>
                                    </div>
                                </div>
                                <div class="row border-top-bottom">
                                    <div class="col-md-12">  
                                                <div class="inline-block">
                                                    <label class="control-label mt-1x" for="tagsinput">To:</label>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="row horizontal-navigation">
                                                        <div class="panel-control">
                                                            <ul class="nav nav-tabs" id="alarmToSendList">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="Write your message here." id="sendAlarmMessagetxtArea" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>  
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>                               
                                    <li ><a href="#" onclick="sendAlarmNotification()">SEND</a>
                                    </li>
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
  
            <div aria-hidden="true" aria-labelledby="deleteIncidentModal" role="dialog" tabindex="-1" id="deleteIncidentModal" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
                            <div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal">CANCEL</a>
                                        </li>   
                                        <li><a href="#" data-dismiss="modal" onclick="deleteIncidentType()"><i class='fa fa-trash'></i>DELETE</a>
                                        </li>   
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="errorMessageModal" role="dialog" tabindex="-1" id="errorMessageModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
<%--					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					</div>--%>
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h4 class="red-color text-center">Please provide all needed information</h4>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CLOSE</a>
									    </li>		
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            		                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="taskDocument" role="dialog" tabindex="-1" id="taskDocument" class="modal fade videoModal" style="display: none;z-index:100000">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="taskheaderImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="taskincidentNameHeader"></h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" data-target="#viewDocument1" onclick="rowchoice(document.getElementById('rowidChoice').text)" data-toggle="modal" class="close" type="button" ><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Status: <span id="taskstatusSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created by: <span id="taskusernameSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Assigned to: <span id="tasktypeSpan"></span></p>
						</div>		
                                   <div class="col-md-3">
							<p>Task Type: <span id="ttypeSpan"></span></p>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Location: <span id="tasklocSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created on: <span id="tasktimeSpan"></span></p>
						</div>	
                         <div class="col-md-3">
							<p>Assigned on: <span id="assignedTimeSpan"></span></p>
						</div>
                          <div class="col-md-3">
							<p id="incidentItemsList"></p>
						</div>									
					  </div>				
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5" style="border-right: 1px #bbbbbb solid;">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li class="active" id="taskliInfo"><a href="#taskinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="taskliNotes"><a href="#notes-tab" data-toggle="tab" class="capitalize-text">NOTES</a>
                                            </li>
                                            <li id="taskliActi"><a href="#taskactivity-tab" data-toggle="tab" onclick="tracebackOn('task')" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="taskliAtta"><a href="#taskattachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
                                        <div class="tab-content">
										<div class="tab-pane fade active in" id="taskinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Description:</b></p>
													<p id="taskdescriptionSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Checklist Name : </b><b id="checklistnameSpan"></b><i id="checklistnamespanFA" style="margin-left:5px;" class="fa fa-check-square-o"></i></p>
                                                    <ul id="checklistItemsList" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>		
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
                                        <div class="tab-pane fade" id="notes-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
													<p id="taskinstructionSpan"></p>
												</div>
											</div>	
											<div class="row">
												<div class="col-md-12">
													<p class="red-color"><b>Checklist Notes:</b></p>
													<p id="checklistNotesSpan"></p>
												</div>
											</div>	
                                           <div id="pchecklistItemsListNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Unchecked Items:</b></p>
													 <ul id="checklistItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
                                            <div id="pCanvasNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Canvas Notes:</b></p>
													 <ul id="canvasItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
										<div class="tab-pane fade" id="taskactivity-tab">
                                                    <div id="taskdivIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div>
										<div class="tab-pane fade" id="taskattachments-tab">	
                                              <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="taskattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>								
										</div>
							            </div>
									</div>
								</div>
							</div>

                            <div class="col-md-1"  style="width:40px;">

                                <i id="taskrotationDIV1" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb" class="fa fa-angle-double-left  fa-2x" onclick="taskbackImg()"></i>

                            </div>

							<div class="col-md-5" id="taskdivAttachmentHolder" style="width:440px;margin-top:4px;border-right: 1px #bbbbbb solid;border-left: 1px #bbbbbb solid;">
								<div class="tab-pane fade active in" id="tasklocation-tab">
                                    <div id="taskmap_canvasIncidentLocation" style="width:100%;height:380px;"></div>
									<div id="taskdivAttachment" class="overlapping-map-image">
									</div>
								</div>		
                                <div class="tab-pane fade" id="taskrejection-tab">
	                                      <p class="red-color text-center"><b>Rejection Notes:</b></p>
									        <div class="col-md-12" style="height:410px;">
										        <textarea placeholder="Rejection Notes" id="taskrejectionTextarea" class="form-control" rows="12"></textarea>
									        </div>
                                </div>									
							</div>

                            <div class="col-md-1"  style="width:40px;">
                                 <i id="taskrotationDIV2" class="fa fa-angle-double-right  fa-2x" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb;"  onclick="tasknextImg()"></i>

                             </div>
						</div>
                        <div class="row">
                                                        <div class="col-md-5">

                            </div>
                                    <div class="col-md-7" style="display:none;border-left:0px;" id="taskAudioDIV"> 
                                        <audio id="taskAudio" style="width: 100%;" width="100%" controls><source id="taskAudioSrc" type="audio/mpeg"/></audio>
                                    </div>
                        </div>
					</div>
					<div class="modal-footer">
                                               <div class="row" id="rowtasktracebackUser" style="display:none;">
                        <div class="col-md-5">
                            </div>
                               <div class="col-md-7" style="text-align:left;margin-top:-8px;">
 <a style="font-size:16px;" href="#" id="playpausefilter" onclick="playpauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1,'task')"><i id="taskfilter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5,'task')"><i id="taskfilter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30,'task')"><i id="taskfilter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60,'task')"><i id="taskfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                            </div>
                        </div>
						<div class="row horizontal-navigation">
							<div class="panel-control">
                                <ul class="nav nav-tabs" id="taskinitialOptionsDiv">
									<li><a href="#" data-dismiss="modal" data-target="#viewDocument1" onclick="rowchoice(document.getElementById('rowidChoice').text)"  data-toggle="modal">CLOSE</a>
									</li>
								</ul>
								<ul class="nav nav-tabs" id="taskhandleOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal" data-target="#viewDocument1" onclick="rowchoice(document.getElementById('rowidChoice').text)"  data-toggle="modal">CLOSE</a>
									</li>		
                                    <li><a href="#">ACCEPT</a>
									</li>
									<li><a href="#" data-target="#rejection-tab" data-toggle="tab"  onclick="rejectionSelect();">REJECT</a>
									</li>							
								</ul>
                                <ul class="nav nav-tabs" id="taskrejectOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal" data-target="#viewDocument1" onclick="rowchoice(document.getElementById('rowidChoice').text)"  data-toggle="modal">CLOSE</a>
									</li>
                                    <li><a href="#">ASSIGN</a>
									</li>
									<li><a href="#">REASSIGN</a>
									</li>
									<li><a href="#">SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="newIncidentType" role="dialog" tabindex="-1" id="newIncidentType" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">NEW INCIDENT TYPE</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Incident Type Name" id="newIncidentTypeinput"/>
                              </div>
                           </div>
                            <p style="margin-left:10px;" class="red-color">Select an Icon</p>
                                                   
                                               <div class="row" style="margin-top:5px;">
                                                   
 <div class="col-md-2" >
     </div>
                               <div class="col-md-1" style="margin-right:5px;">
                                   <img style="height:40px;" id="img1" onclick="thisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_1.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="img2" onclick="thisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_2.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="img3" onclick="thisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_3.png"/>
                               </div> 
                               <div class="col-md-1" style="margin-right:5px;">
                                   <img style="height:40px;" id="img4" onclick="thisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_4.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="img5" onclick="thisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_5.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="img6" onclick="thisImgClick(this)"  src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_6.png"/>
                               </div> 
                               <div class="col-md-1" style="margin-right:5px;">
                                   <img style="height:40px;" id="img7" onclick="thisImgClick(this)"  src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_7.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="img8" onclick="thisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_8.png"/>
                               </div>
                             <div class="col-md-2" >
     </div>
                                                   </div>
                            <p style="margin-left:10px;" class="red-color">*Icons are for MIMS Watch app only</p>
                        </form> 
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#"  onclick="savenewIncidentType()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="editIncidentType" role="dialog" tabindex="-1" id="editIncidentType" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">EDIT INCIDENT TYPE</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Edit Incident Type" id="editIncidentTypeinput"/>
                              </div>
                           </div>
                                                        <p style="margin-left:10px;" class="red-color">Select an Icon</p>

                                            <div class="row" style="margin-top:5px;">
 <div class="col-md-2" >
     </div>
                               <div class="col-md-1" style="margin-right:5px;">
                                   <img style="height:40px;" id="eimg1" onclick="ethisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_1.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="eimg2" onclick="ethisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_2.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="eimg3" onclick="ethisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_3.png"/>
                               </div> 
                               <div class="col-md-1" style="margin-right:5px;">
                                   <img style="height:40px;" id="eimg4" onclick="ethisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_4.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="eimg5" onclick="ethisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_5.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="eimg6" onclick="ethisImgClick(this)"  src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_6.png"/>
                               </div> 
                               <div class="col-md-1" style="margin-right:5px;">
                                   <img style="height:40px;" id="eimg7" onclick="ethisImgClick(this)"  src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_7.png"/>
                               </div>
                                <div class="col-md-1" style="margin-right:5px;">
                                    <img style="height:40px;" id="eimg8" onclick="ethisImgClick(this)" src="https://livemimslob.blob.core.windows.net/mobileevent/incident_type_8.png"/>
                               </div>
                             <div class="col-md-2" >
     </div>
                                                   </div>
                                                                                  <p style="margin-left:10px;" class="red-color">*Icons are for MIMS Watch app only</p>
                        </form> 
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" data-dismiss="modal" onclick="saveeditIncidentType()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="successfulReport" role="dialog" tabindex="-1" id="successfulReport" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" id="successfulReportScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>
                         <div aria-hidden="true" aria-labelledby="deleteAttachModal" role="dialog" tabindex="-1" id="deleteAttachModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#viewDocument1').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#viewDocument1').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachment()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
              <IncidentCard:MyIncidentCard id="IncidentCardControl" runat="server" />
            <select style="color:white;background-color: #5D5D5D;width:125px;height:105px;margin-top:-15px;display:none;" multiple="multiple" id="sendToListBox" ></select>               
              <asp:HiddenField runat="server" ID="tbUserID" />
              <asp:HiddenField runat="server" ID="tbUserName" />
                        <input style="display:none;" id="imagePostAttachment" type="text"/>
              <input style="display:none;" id="rowChoiceIncidentType" type="text"/>
              <input style="display:none;" id="rowIncidentName" type="text"/>
              <input style="display:none;" id="rowChoiceTasks" type="text"/>
              <input style="display:none;" id="rowidChoice" type="text"/>
              <input style="display:none;" id="rowidChoiceAttachment" type="text"/>
              <input style="display:none;" id="cameraDuration" type="text"/>
              <input style="display:none;" type="text"  name="vertices" value="" id="vertices" />
              <input style="display:none;" id="rowLongitude" type="text"/>  
              <input style="display:none;" id="rowLatitude" type="text"/>  
             <input style="display:none;" id="imgnewtype" type="text" value="0"/>  
              <input style="display:none;" id="assigneeTypeSelectionID" type="text"/>  
              <asp:HiddenField runat="server" ID="mileInfo" />
              <asp:HiddenField runat="server" ID="tbincidentName2" />
              <input style="display:none;" id="imagePath" type="text"/>
            <input style="display:none;" id="mobimagePath" type="text"/>
            <input style="display:none;" id="mobimagePath2" type="text"/>
              <asp:Button class="specialTaskbutton tasksfont" ID="btnSave" style="display:none" OnClick="btnSave_Click"   runat="server" Text="Save" />
             </section>
        <!-- /MAIN -->
</asp:Content>