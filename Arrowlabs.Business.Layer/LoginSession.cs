﻿// -----------------------------------------------------------------------
// <copyright file="Login.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data.SqlClient;
    using System.Data;
    using Arrowlabs.Mims.Audit.Models;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class LoginSession
    {
        
        public string Username { get; set; }
        public string SessionId { get; set; }
        public bool LoggedIn { get; set; }
        public string UpdatedBy { get; set; }
        public int DeviceType { get; set; }
        public string AccountName { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        //LoginHistory
        public string MacAddress { get; set; }
        public string IPAddress { get; set; }
        public string PCName { get; set; }

        public string CustomerUName { get; set; }

        private static LoginSession GetLoginSessionFromSqlReader(SqlDataReader reader)
        {
            var login = new LoginSession();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            login.Username = reader["Username"].ToString();
            login.SessionId = reader["SessionId"].ToString();

            login.LoggedIn = (String.IsNullOrEmpty(reader["LoggedIn"].ToString())) ? false : Convert.ToBoolean(reader["LoggedIn"].ToString());

            if (columns.Contains("DeviceType"))
                login.DeviceType = Convert.ToInt32(reader["DeviceType"].ToString());

            if (columns.Contains("CustomerId"))
                login.CustomerId = (String.IsNullOrEmpty(reader["CustomerId"].ToString())) ? 0 : Convert.ToInt32(reader["CustomerId"].ToString()); 

            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                login.CustomerUName = reader["CustomerUName"].ToString();
        
            return login;
        }
        private static LoginSession GetLoginHistoryFromSqlReader(SqlDataReader reader)
        {
            var login = new LoginSession();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            login.Username = reader["MacAddress"].ToString();
            login.SessionId = reader["PCName"].ToString();
            login.LoggedIn = (String.IsNullOrEmpty(reader["Status"].ToString())) ? false : Convert.ToBoolean(reader["Status"].ToString());
            login.LoginDate = Convert.ToDateTime(reader["LoginDate"].ToString());
            login.LogoutDate = Convert.ToDateTime(reader["LogoutDate"].ToString());


            if (columns.Contains("CustomerId"))
                login.CustomerId = (String.IsNullOrEmpty(reader["CustomerId"].ToString())) ? 0 : Convert.ToInt32(reader["CustomerId"].ToString()); 

            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                login.CustomerUName = reader["CustomerUName"].ToString();

            return login;
        }
        public static List<LoginSession> GetLoginHistoryByMacAddress(string username, string dbConnection)
        {
            var loginStatus = new List<LoginSession>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLoginHistoryByMacAddress", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = username;
                
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    loginStatus.Add(GetLoginHistoryFromSqlReader(reader));
                }
                connection.Close();
                reader.Close();
            }
            return loginStatus;
        }

        public static LoginSession GetLoginStatusByUserIdAndSessionId(string username, string sessionId, string dbConnection)
        {
            var loginStatus = new LoginSession();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLoginStatusByUserIdAndSessionId", connection);
                
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = username;

                sqlCommand.Parameters.Add(new SqlParameter("@SessionId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@SessionId"].Value = sessionId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    loginStatus = GetLoginSessionFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return loginStatus;
        }

        public static LoginSession GetConcurrentLoginByUsernameAndSessionId(string username, string sessionId, string dbConnection)
        {
            var loginStatus = new LoginSession();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetConcurrentLoginByUsernameAndSessionId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = username;

                sqlCommand.Parameters.Add(new SqlParameter("@SessionId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@SessionId"].Value = sessionId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    loginStatus = GetLoginSessionFromSqlReader(reader);             
                }
                connection.Close();
                reader.Close();
            }
            return loginStatus;
        }

        public static int GetAllMimsMobileOnlineByDeviceType(int deviceType, string dbConnection)
        {
            var totalOnlineUserByDeviceType = 0;

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOnlineByDeviceType", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DeviceType", SqlDbType.Int));
                sqlCommand.Parameters["@DeviceType"].Value = deviceType;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                totalOnlineUserByDeviceType = (int)sqlCommand.ExecuteScalar();

                connection.Close();
            }
            return totalOnlineUserByDeviceType;
        }
      
        public static LoginSession GetLoginStatusByUsernameAndSessionId(string username, string sessionId, string dbConnection)
        {
            var loginStatus = new LoginSession();
            
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetLoginStatusByUsernameAndSessionId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Username"].Value = username;

                    sqlCommand.Parameters.Add(new SqlParameter("@SessionId", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SessionId"].Value = sessionId;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        loginStatus = GetLoginSessionFromSqlReader(reader);
                    }
                    connection.Close();
                    reader.Close();
                }
            return loginStatus;
        }

        public static bool GetLoginStatusByUsername(string username , string dbConnection)
        {
            bool isLoggedIn = false;
            var loginStatus = new LoginSession();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLoginStatusByUsername", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = username;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    loginStatus = GetLoginSessionFromSqlReader(reader);

                    if (loginStatus.LoggedIn)
                    {
                        isLoggedIn = true;
                        break;
                    }
                }
                connection.Close();
                reader.Close();
            }
            return isLoggedIn;
        }

        public static bool LogoutEveryoneExceptByUsernameAndSessionId(string username, string sessionId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("LogoutEveryoneExceptByUsernameAndSessionId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Username"].Value = username;

                    sqlCommand.Parameters.Add(new SqlParameter("@SessionId", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SessionId"].Value = sessionId;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool DeleteLoginByUsername(string username, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("DeleteLoginByUsername", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Username"].Value = username;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool LogoutUserByUsernameAndSessionId(string username, string sessionId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("LogoutUserByUsernameAndSessionId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Username"].Value = username;

                    sqlCommand.Parameters.Add(new SqlParameter("@SessionId", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SessionId"].Value = sessionId;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
             }
            catch(Exception)
            {
                return false;
            }
        }

        public static bool LogoutUserBySessionId(string sessionId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("LogoutUserBySessionId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SessionId", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SessionId"].Value = sessionId;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool InsertLogin(LoginSession login, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            try
            {
                if (login != null)
                {
                    var action = Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertLogin", connection);

                        cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                        cmd.Parameters["@Username"].Value = login.Username;

                        cmd.Parameters.Add(new SqlParameter("@SessionId", SqlDbType.NVarChar));
                        cmd.Parameters["@SessionId"].Value = login.SessionId;

                        cmd.Parameters.Add(new SqlParameter("@LoggedIn", SqlDbType.Bit));
                        cmd.Parameters["@LoggedIn"].Value = login.LoggedIn;

                        cmd.Parameters.Add(new SqlParameter("@DeviceType", SqlDbType.Int));
                        cmd.Parameters["@DeviceType"].Value = login.DeviceType;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = login.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = login.CustomerId;

                        cmd.CommandType = CommandType.StoredProcedure;

                        int id = cmd.ExecuteNonQuery();

                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {
                                var audit = new LoginAudit();
                                audit.BaseAudit = BaseAudit.FillBaseAudit(login.AccountName, action, login.UpdatedBy);
                                Reflection.CopyProperties(login, audit);
                                LoginAudit.InsertLoginAudit(audit, auditDbConnection);
                            }
                        }
                        catch (Exception ex)
                        {
                            MIMSLog.MIMSLogSave("Business Layer", "InsertLogin", ex, dbConnection);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool InsertOrUpdateLoginHistory(LoginSession login, string dbConnection,
    string auditDbConnection = "", bool isAuditEnabled = true)
        {
            try
            {
                if (login != null)
                {
                    var action = Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateLoginHistory", connection);

                        cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                        cmd.Parameters["@MacAddress"].Value = login.MacAddress;

                        cmd.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                        cmd.Parameters["@PCName"].Value = login.PCName;

                        cmd.Parameters.Add(new SqlParameter("@IPAddress", SqlDbType.NVarChar));
                        cmd.Parameters["@IPAddress"].Value = login.IPAddress;

                        cmd.Parameters.Add(new SqlParameter("@LoginDate", SqlDbType.DateTime));
                        cmd.Parameters["@LoginDate"].Value = login.LoginDate;

                        cmd.Parameters.Add(new SqlParameter("@LogoutDate", SqlDbType.DateTime));
                        cmd.Parameters["@LogoutDate"].Value = login.LogoutDate;

                        cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Bit));
                        cmd.Parameters["@Status"].Value = login.LoggedIn;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = login.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = login.CustomerId;
                        
                        cmd.Parameters.Add(new SqlParameter("@SessionId", SqlDbType.NVarChar));
                        cmd.Parameters["@SessionId"].Value = login.SessionId;

                        cmd.CommandType = CommandType.StoredProcedure;

                        int id = cmd.ExecuteNonQuery();

                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {
                                var audit = new LoginAudit();
                                audit.BaseAudit = BaseAudit.FillBaseAudit(login.AccountName, action, login.UpdatedBy);
                                Reflection.CopyProperties(login, audit);
                                LoginAudit.InsertLoginAudit(audit, auditDbConnection);
                            }
                        }
                        catch (Exception ex)
                        {
                            MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateLoginHistory", ex, dbConnection);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool LogoutUserByUsername(string username, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("LogoutUserByUsername", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Username"].Value = username;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
             }
            catch(Exception)
            {
                return false;
            }
        }

        

        
    }

}
