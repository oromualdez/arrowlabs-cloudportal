﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class ActivityType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }

        public string selected { get; set; }
        public string CustomerUName { get; set; }
        private static ActivityType GetActivityTypeFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();


            var notification = new ActivityType();

            if (reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                notification.Name = reader["Name"].ToString(); 

            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedDate"] != DBNull.Value)
                notification.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["UpdatedBy"] != DBNull.Value)
                notification.UpdatedBy = reader["UpdatedBy"].ToString(); 

            if (reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                notification.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains( "CustomerUName"))
                notification.CustomerUName = reader["CustomerUName"].ToString(); 
             

            return notification;
        }

        public static int InsertorUpdateActivityType(ActivityType notification, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateActivityType", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = notification.Name; 

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = notification.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = notification.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;
                     

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateActivityType";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            notification.Id = id;
                            var audit = new ActivityTypeAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, notification.UpdatedBy);
                            Reflection.CopyProperties(notification, audit);
                            ActivityTypeAudit.InsertActivityTypeAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateActivityType", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static List<ActivityType> GetAllActivityType(string dbConnection)
        {
            var assetList = new List<ActivityType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllActivityType", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = GetActivityTypeFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        public static ActivityType GetActivityTypeById(int id, string dbConnection)
        {
            ActivityType asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetActivityTypeById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetActivityTypeFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return asset;
        }
        public static ActivityType GetActivityTypeByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            ActivityType asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetActivityTypeByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetActivityTypeFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return asset;
        }
        public static ActivityType GetActivityTypeByName(string name, string dbConnection)
        {
            ActivityType asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetActivityTypeByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetActivityTypeFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return asset;
        }
        public static List<ActivityType> GetAllActivityTypeByLevel5(int siteId, string dbConnection)
        {
            var itemFounds = new List<ActivityType>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllActivityTypeByLevel5", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetActivityTypeFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ActivityType", "GetAllActivityTypeBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }
        public static List<ActivityType> GetAllActivityTypeBySiteId(int siteId, string dbConnection)
        {
            var itemFounds = new List<ActivityType>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllActivityTypeBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetActivityTypeFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ActivityType", "GetAllActivityTypeBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }
        public static List<ActivityType> GetAllActivityTypeByCId(int siteId, string dbConnection)
        {
            var itemFounds = new List<ActivityType>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllActivityTypeByCId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetActivityTypeFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ActivityType", "GetAllActivityTypeBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }
        public static bool DeleteActivityTypeById(int empid, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteActivityTypeById", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = empid;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteActivityTypeById";

                    cmd.ExecuteNonQuery();
                }
                return true;

            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("ActivityType", "DeleteActivityTypeById", ex, dbConnection);
            }
            return false;
        }
    }
}
