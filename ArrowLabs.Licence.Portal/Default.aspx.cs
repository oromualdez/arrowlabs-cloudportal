﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Arrowlabs.Business.Layer;
using System.Text;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using System.Web.Services;
using ArrowLabs.Licence.Portal.Helpers;
using System.Threading;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Web.Mvc;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ArrowLabs.Licence.Portal
{
    
    public partial class _Default : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static string Incidentidentifier { get; set; }
        static ClientLicence getClientLic;

        int completedCount;
        int inprogressCount;
        int pendingCount;
        int total;
        protected int onlinecount;
        protected int offlinecount;
        protected int idlecount;


        protected string lbHandledAlarms ="0";
        protected string lbPendingAlarms = "0";
        protected string pendingPercentDemo;
        protected string pendingPercentDemoCount;

        protected string inprogressPercentDemo;
        protected string inprogressPercentDemoCount;

        protected string completedPercentDemo;
        protected string completedPercentDemoCount;

        protected string acceptedPercent;
        protected string acceptedPercentCount;

        protected string totalTasksCount;

        protected string handledTeamTasks;

        protected int ActualDemo = 0;
        protected int PlannedDemo = 0;

        protected string ActualDemoCount = "0";
        protected string PlannedDemoCount = "0";

        protected int taskUser1Avg = 0;
        protected int taskUser2Avg = 0;
        protected int taskUser3Avg = 0;
        protected int taskUser4Avg = 0;
        protected int taskUser5Avg = 0;

        protected string taskUser1 = " ";
        protected string taskUser2 = " ";
        protected string taskUser3 = " ";
        protected string taskUser4 = " ";
        protected string taskUser5 = " ";

        protected string AvgTotal = "0";

        protected string lbTop1OffenceEvUser = "";
        protected string lbTop2OffenceEvUser = "";
        protected string lbTop3OffenceEvUser = "";
        protected string lbTop4OffenceEvUser = "";
        protected string lbTop5OffenceEvUser = "";

        protected string Top1Ins = "0";
        protected string Top2Ins = "0";
        protected string Top3Ins = "0";
        protected string Top4Ins = "0";
        protected string Top5Ins = "0";

        protected string Top1InsCount = "0";
        protected string Top2InsCount = "0";
        protected string Top3InsCount = "0";
        protected string Top4InsCount = "0";
        protected string Top5InsCount = "0";

        protected string Top1InsName = " ";
        protected string Top2InsName = " ";
        protected string Top3InsName = " ";
        protected string Top4InsName = " ";
        protected string Top5InsName = " ";

        protected string Top1TicCount = "0";
        protected string Top2TicCount = "0";
        protected string Top3TicCount = "0";
        protected string Top4TicCount = "0";
        protected string Top5TicCount = "0";

        protected string Top1TicName = "";
        protected string Top2TicName = "";
        protected string Top3TicName = "";
        protected string Top4TicName = "";
        protected string Top5TicName = "";

        protected string Top1Tic = "0";
        protected string Top2Tic = "0";
        protected string Top3Tic = "0";
        protected string Top4Tic = "0";
        protected string Top5Tic = "0";

        protected int SkillSet1Week1Count = 0;
        protected int SkillSet1Week2Count = 0;
        protected int SkillSet1Week3Count = 0;
        protected int SkillSet1Week4Count = 0;

        protected int SkillSet2Week1Count = 0;
        protected int SkillSet2Week2Count = 0;
        protected int SkillSet2Week3Count = 0;
        protected int SkillSet2Week4Count = 0;

        protected int SkillSet3Week1Count = 0;
        protected int SkillSet3Week2Count = 0;
        protected int SkillSet3Week3Count = 0;
        protected int SkillSet3Week4Count = 0;

        protected int SkillSet4Week1Count = 0;
        protected int SkillSet4Week2Count = 0;
        protected int SkillSet4Week3Count = 0;
        protected int SkillSet4Week4Count = 0;

        protected int SkillSet5Week1Count = 0;
        protected int SkillSet5Week2Count = 0;
        protected int SkillSet5Week3Count = 0;
        protected int SkillSet5Week4Count = 0;


        protected string SkillSet1Name = " ";
        protected string SkillSet2Name = " ";
        protected string SkillSet3Name = " ";
        protected string SkillSet4Name = " ";
        protected string SkillSet5Name = " ";

        protected string demoWeek1 = "1";
        protected string demoWeek2 = "2";
        protected string demoWeek3 = "3";
        protected string demoWeek4 = "4";
        protected string demoWeek5 = "5";

        protected string locID;
        protected string incidentName;
        protected string incidentIns;
        protected string incidentDesc;
        protected string incidentRecBy;
        protected string incidentLong;
        protected string incidentLat;
        protected string incidentTaskId;
        protected string incidentAction;
        protected string IsTask;
        protected string incidentType;
        protected string resolvedPercent="0";
        protected string pendingPercent = "0";
        protected string HandledPercentage = "0";
        protected string inprogressPercent = "0";
        protected string completedPercent = "0";
        protected string datetoday;
        protected string hot1Percent = "0";
        protected string hot2Percent = "0";
        protected string hot3Percent = "0";
        protected string hot4Percent = "0";
        protected string hot5Percent = "0";
        protected string day1;
        protected string day2;
        protected string day3;
        protected string day4;
        protected string day5;
        protected string day6;
        protected string day7;
        protected string loggedInId;
        protected string userinfoDisplay = "block";
        protected int day1week1;
        protected int day2week1;
        protected int day3week1;
        protected int day4week1;
        protected int day5week1;
        protected int day6week1;
        protected int day7week1;

        protected int day1week2;
        protected int day2week2;
        protected int day3week2;
        protected int day4week2;
        protected int day5week2;
        protected int day6week2;
        protected int day7week2;

        protected int day1week3;
        protected int day2week3;
        protected int day3week3;
        protected int day4week3;
        protected int day5week3;
        protected int day6week3;
        protected int day7week3;

        protected int day1week4;
        protected int day2week4;
        protected int day3week4;
        protected int day4week4;
        protected int day5week4;
        protected int day6week4;
        protected int day7week4;

        protected int ticketday1week1;
        protected int ticketday2week1;
        protected int ticketday3week1;
        protected int ticketday4week1;
        protected int ticketday5week1;
        protected int ticketday6week1;
        protected int ticketday7week1;

        protected int ticketday1week2;
        protected int ticketday2week2;
        protected int ticketday3week2;
        protected int ticketday4week2;
        protected int ticketday5week2;
        protected int ticketday6week2;
        protected int ticketday7week2;

        protected int ticketday1week3;
        protected int ticketday2week3;
        protected int ticketday3week3;
        protected int ticketday4week3;
        protected int ticketday5week3;
        protected int ticketday6week3;
        protected int ticketday7week3;

        protected int ticketday1week4;
        protected int ticketday2week4;
        protected int ticketday3week4;
        protected int ticketday4week4;
        protected int ticketday5week4;
        protected int ticketday6week4;
        protected int ticketday7week4;

        protected int notificationCount1;
        protected int notificationCount2;
        protected int notificationCount3;
        protected int notificationCount4;
        protected string cusEvId { get; set; }
        protected string incidentId;
        protected string longi;
        protected string lati;
        protected string alarmMSG;
        protected string senderName;
        protected string senderName2;
        protected string senderName3;
        protected string username;
        protected string ipaddress;
        protected string sendToManager;
        protected string assignedTo;
        protected string sendToTag;
        protected string sendMangId;

        protected string hot1display;
        protected string hot2display;
        protected string hot3display;
        protected string hot4display;
        protected string hot5display;
        protected string dispatchSurv;
        protected string escalatedView;
        protected string dispatchDisplay;

        protected string sourceLat;
        protected string sourceLon;
        protected string currentlocation;
        protected string incidentDisplay;
        protected string taskDisplay;
        protected string otDisplay;
        protected string msbDisplay;
        static string fpath;
        protected string siteName;
        protected void Page_Load(object sender, EventArgs e)
        {
            fpath = Server.MapPath("~/Uploads/");
            var newAuthorize = new MyAuthorize();
            var retVal =  newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            var dtNow = CommonUtility.getDTNow();
            datetoday = dtNow.ToString("MMM dd, yyyy");
            senderName = User.Identity.Name;
            senderName2 = Encrypt.EncryptData(User.Identity.Name, true, dbConnection);
            
            var userinfo = Users.GetUserByName(senderName, dbConnection);
            sourceLat = "25.2049808502197";
            sourceLon = "55.2707939147949";
            currentlocation = "U.A.E";
            msbDisplay = "none";
            if (userinfo != null)
            {

                if (userinfo.SiteId > 0)
                {
                    siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                }

                senderName3 = userinfo.CustomerUName;
                incidentDisplay = "none";
                taskDisplay = "block";
                otDisplay = "block";
                var foundServerModule = false;
                if (userinfo.RoleId == (int)Role.MessageBoardUser)
                {
                    Response.Redirect("~/MessageBoard.aspx");
                    foundServerModule = true;
                }
                else if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var uModules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    if (uModules != null)
                    {
                        uModules = uModules.OrderBy(i => i.ModuleId).ToList();
                        if (uModules.Count > 0)
                        { 
                            var isDash = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Dash).ToList();
                            if (isDash.Count > 0)
                            {
                                var isIncident = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Alarms).ToList();
                                if (isIncident.Count > 0)
                                {
                                    incidentDisplay = "block";
                                    foundServerModule = true;
                                    var isTask = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Tasks).ToList();
                                    if (isTask.Count == 0)
                                    {
                                        taskDisplay = "none";
                                        foundServerModule = true;
                                    }
                                    var isOther = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Database).ToList();
                                    var isVeri = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Verifier).ToList();
                                    if (isOther.Count == 0)
                                    {
                                        otDisplay = "none";
                                        foundServerModule = true;
                                    }
                                }
                                else
                                {
                                    var isTask = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Tasks).ToList();
                                    if (isTask.Count > 0)
                                    {

                                        Response.Redirect("~/TaskDash.aspx");
                                        foundServerModule = true;
                                    }
                                    else
                                    {
                                        var isOther = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Database).ToList();
                                       // var isVeri = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Verifier).ToList();
                                        if (isOther.Count > 0)//&& isVeri.Count > 0)
                                        {

                                            Response.Redirect("~/OthersDash.aspx");
                                            foundServerModule = true;
                                        }
                                        else
                                        {
                                            Response.Redirect("~/Pages/UsersDB.aspx");
                                            foundServerModule = true;
                                        }
                                    }
                                }

                            }
                            else
                            {
                                foreach (var mods in uModules)
                                {

                                    if (mods.ModuleId == (int)Accounts.ModuleTypes.Alarms)
                                    {
                                        Response.Redirect("~/Pages/Incident.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Database)
                                    {
                                        Response.Redirect("~/Pages/Ticketing.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks)
                                    {
                                        Response.Redirect("~/Pages/Tasks.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Reports)
                                    {
                                        Response.Redirect("~/ReportPages/Reports.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Notification)
                                    {
                                        Response.Redirect("~/Pages/Messages.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Verifier)
                                    {
                                        Response.Redirect("~/Pages/VerifierPage.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Devices)
                                    {
                                        Response.Redirect("~/Pages/Devices.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.System || mods.ModuleId == (int)Accounts.ModuleTypes.Chat || mods.ModuleId == (int)Accounts.ModuleTypes.Collaboration || mods.ModuleId == (int)Accounts.ModuleTypes.PostOrder || mods.ModuleId == (int)Accounts.ModuleTypes.DutyRoster)
                                    {
                                        Response.Redirect("~/Pages/UsersDB.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Warehouse || mods.ModuleId == (int)Accounts.ModuleTypes.LostandFound || mods.ModuleId == (int)Accounts.ModuleTypes.ServerKey)
                                    {
                                        Response.Redirect("~/Pages/Lost.aspx");
                                        foundServerModule = true;
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Pages/UsersDB.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Pages/UsersDB.aspx");
                    }

                    if(!foundServerModule)
                        Response.Redirect("~/Pages/UsersDB.aspx");
                }
                 
            }
            try
            {
                var settings = ConfigSettings.GetConfigSettings(CommonUtility.dbConnection);
                ipaddress = settings.ChatServerUrl + "/signalr";
                var firstDayOfMonth = new DateTime(dtNow.Year, dtNow.Month, 1);
                day1 = firstDayOfMonth.ToString("yyyy-MM-dd");
                day2 = firstDayOfMonth.AddDays(1).ToString("yyyy-MM-dd");
                day3 = firstDayOfMonth.AddDays(2).ToString("yyyy-MM-dd");
                day4 = firstDayOfMonth.AddDays(3).ToString("yyyy-MM-dd");
                day5 = firstDayOfMonth.AddDays(4).ToString("yyyy-MM-dd");
                day6 = firstDayOfMonth.AddDays(5).ToString("yyyy-MM-dd");
                day7 = firstDayOfMonth.AddDays(6).ToString("yyyy-MM-dd");


                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);


                if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser || userinfo.RoleId == (int)Role.Regional)
                {
                    if (userinfo.CustomerInfoId > 0)
                    {
                        var gSite = Arrowlabs.Business.Layer.CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                        if (gSite != null)
                        {
                            sourceLat = gSite.Lati;
                            sourceLon = gSite.Long;
                            if (!string.IsNullOrEmpty(gSite.Country))
                                currentlocation = gSite.Country.ToUpper();
                            else
                                currentlocation = "N/A";
                        }
                    }
                }
                else
                {
                    if (userinfo.SiteId > 0)
                    {
                        var gSite = Arrowlabs.Business.Layer.Site.GetSiteById(userinfo.SiteId, dbConnection);
                        if (gSite != null)
                        {
                            sourceLat = gSite.Lati;
                            sourceLon = gSite.Long;
                            currentlocation = ReverseGeocode.RetrieveFormatedAddress(sourceLat, sourceLon, getClientLic).ToUpper();
                        }
                    }
                }
                dispatchDisplay = "block";
                if (getClientLic != null)
                {
                    if (!getClientLic.isDispatch)
                        dispatchDisplay = "none";
                    if (!getClientLic.isSurveillance)
                        dispatchSurv = "style='display:none;'";
                    if (!getClientLic.isTask)
                        taskDisplay = "none";
                    if (getClientLic.isIncident)
                        incidentDisplay = "block";
                }
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    {
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Surveillance && getClientLic.isSurveillance)
                        {
                            dispatchSurv = "style='display:block'";
                        }
                        else if (mods.ModuleId == (int)Accounts.ModuleTypes.Dispatch && getClientLic.isDispatch)
                        {
                            dispatchDisplay = "block";
                        }
                        else if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks && getClientLic.isTask)
                        {
                            taskDisplay = "block";
                        }
                        else if (mods.ModuleId == (int)Accounts.ModuleTypes.MessageBoard)
                        {
                            msbDisplay = "block";
                        }
                    }
                }
                if (incidentDisplay == "none" && taskDisplay == "block")
                {
                    Response.Redirect("~/TaskDash.aspx");
                }
                if (userinfo != null)
                {
                    loggedInId = userinfo.ID.ToString();
                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.Regional || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        escalatedView = "style='display:none'";
                    }
                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        escalatedView = "style='display:none'";
                        var taskslist = UserTask.GetAllTaskTemplates(dbConnection);

                        //selectTaskTemplate.DataTextField = "Name";
                        //selectTaskTemplate.DataValueField = "Id";
                        //selectTaskTemplate.DataSource = taskslist;
                        //selectTaskTemplate.DataBind();
                        userinfoDisplay = "block";
                        if(userinfo.RoleId == (int)Role.ChiefOfficer)
                            userinfoDisplay = "none";
                    }
                    else
                    {
                        if (userinfo.RoleId == (int)Role.ChiefOfficer)
                            userinfoDisplay = "none";
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            userinfoDisplay = "block";
                        //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                        //if (pushDev != null)
                        //{
                        //    if (!string.IsNullOrEmpty(pushDev.Username))
                        //    {
                        //        pushDev.IsServerPortal = true;
                        //        pushDev.SiteId = userinfo.SiteId;
                        //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection, dbConnectionAudit,true);
                        //    }
                        //}

                        PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);

                        var tasktemplateList = UserTask.GetAllTemplateTasksByManagerId(userinfo.ID, dbConnection);
                        if (userinfo.RoleId == (int)Role.Admin)
                        {
                            //escalatedView = "style='display:none'";
                            //var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            //foreach (var item in sessions)
                            //{
                            //    var tempTemplates = UserTask.GetAllTemplateTasksByManagerId(item, dbConnection);
                            //    foreach (var temp in tempTemplates)
                            //    {
                            //        if (temp.Id > 0)
                            //            tasktemplateList.Add(temp);
                            //    }
                            //}
                        }
                        //selectTaskTemplate.DataTextField = "Name";
                        //selectTaskTemplate.DataValueField = "Id";
                        //selectTaskTemplate.DataSource = tasktemplateList;
                        //selectTaskTemplate.DataBind();

                        userinfoDisplay = "none";
                    }
                }

                var allmanager = new List<Users>();
                var alldirectors = new List<Users>();
                var unassigned = new Users();
                unassigned.Username = "Unassigned";
                unassigned.ID = 0;
                allmanager.Add(unassigned);
                alldirectors.Add(unassigned);
                allmanager.AddRange(Users.GetAllManagers(dbConnection));
                alldirectors.AddRange(Users.GetAllAdmins(dbConnection));

                editmanagerpickerSelect.DataTextField = "CustomerUName";
                editmanagerpickerSelect.DataValueField = "ID";
                editmanagerpickerSelect.DataSource = allmanager;
                editmanagerpickerSelect.DataBind();

                editdirpickerSelect.DataTextField = "CustomerUName";
                editdirpickerSelect.DataValueField = "ID";
                editdirpickerSelect.DataSource = alldirectors;
                editdirpickerSelect.DataBind();

                //var CameraLists = MilestoneCamera.GetAllMilestoneCamera(dbConnection);
                //cameraSelect.DataTextField = "CameraName";
                //cameraSelect.DataValueField = "CamDetails";
                //cameraSelect.DataSource = CameraLists;
                //cameraSelect.DataBind();

                Incidentidentifier = Request.QueryString["Identifier"];
                incidentName = Request.QueryString["IncName"];
                incidentAction = "Dispatch";
                cusEvId = "0";
               
                IsTask = "false";
                incidentId = "0";
                {

                    if (!string.IsNullOrEmpty(Incidentidentifier))
                    {
                        var incClass = CustomEvent.GetCustomEventById(Convert.ToInt32(Incidentidentifier), dbConnection);
                        if (incClass != null)
                        {
                            incidentId = incClass.EventId.ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "showIncident", "showIncident();", true);
                            sendMangId = Convert.ToString(userinfo.ID);
                            incidentId = incClass.EventId.ToString();
                            incidentName = incClass.Name;
                            incidentLat = incClass.Latitude;
                            incidentLong = incClass.Longtitude;
                            locID = incClass.LocationId.ToString();
                            incidentDesc = incClass.Description;
                            incidentTaskId = incClass.TemplateTaskId.ToString();
                            incidentIns = incClass.Instructions;
                            incidentRecBy = incClass.ReceivedBy;
                            //cusEvId = incClass.CustomEventId.ToString();
                            incidentAction = incClass.StatusName;
                            if (incClass.TemplateTaskId > 0)
                                IsTask = "True";
                            else
                                IsTask = "False";
                            incidentType = incClass.IncidentType.ToString();


                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(incidentName))
                        {
                            incidentLat = Request.QueryString["IncLat"];
                            incidentLong = Request.QueryString["IncLongi"];
                            locID = Request.QueryString["LocId"];
                            incidentDesc = Request.QueryString["IncDesc"];
                            incidentTaskId = Request.QueryString["IncCreateTask"];
                            incidentIns = Request.QueryString["IncInstruction"];
                            incidentRecBy = Request.QueryString["ReceivedBy"];
                            IsTask = Request.QueryString["IsTask"];
                            incidentType = Request.QueryString["IncType"];
                            cusEvId = Request.QueryString["CusID"];

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "showIncident", "showIncident();", true);
                        }
                    }
                    getEventStatus(userinfo);
                    getOnlineUsers(userinfo);
                }
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "PageLoad", ex, dbConnection, userinfo.SiteId);
            }
        }
        public void getOnlineUsers(Users userinfo)
        {
            var users = new List<Users>();
            var devices = new List<HealthCheck>();
            try
            {
                onlinecount = 0;
                offlinecount = 0;
                idlecount = 0;
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var usr in sessions)
                        {
                            users.Add(usr);
                            var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                            foreach (var subsubuser in getallUsers)
                            {
                                users.Add(subsubuser);
                            }
                        }
                        var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                        unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                        foreach (var subsubuser in unassigned)
                        {
                            users.Add(subsubuser);
                        } 
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                       // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                      //  foreach (var site in sites)
                      //  {
                        users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                        
                       // }
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        users = Users.GetAllUsersBySiteId(userinfo.SiteId,dbConnection);
                        users = users.Where(i => i.RoleId != (int)Role.Director).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                        users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        users = Users.GetAllUsers(dbConnection);
                        users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        users = Users.GetAllUsers(dbConnection);
                        devices = HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                    }
                    var grouped = users.GroupBy(item => item.ID);
                    users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                    foreach (var item in users)
                    {
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            onlinecount++;
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            idlecount++;
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            offlinecount++;
                        }
                    }
                    foreach (var dev in devices)
                    {
                        if (dev != null)
                        {
                            if (dev.Status == 1)
                            {
                                onlinecount++;
                            }
                            else
                            {
                                offlinecount++;
                            }
                        }
                    }
                    lbOnlineCount.Text = onlinecount.ToString();
                    lbOfflineCount.Text = offlinecount.ToString();
                    lbIdleCount.Text = idlecount.ToString();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getOnlineUsers", ex, dbConnection, userinfo.SiteId);
            }
        }
        public void getEventStatus(Users userinfo)
        {
            try
            {
                var cusEvents = new List<CustomEvent>();
                var resolvedIncidents = new List<CustomEvent>();
                var preresolvedIncidents = new List<CustomEvent>();
                pendingCount = 0;
                inprogressCount = 0;
                completedCount = 0;
                total = 0;

                ticketday1week1 = 0;
                ticketday2week1 = 0;
                ticketday3week1 = 0;
                ticketday4week1 = 0;
                ticketday5week1 = 0;
                ticketday6week1 = 0;
                ticketday7week1 = 0;

                ticketday1week2 = 0;
                ticketday2week2 = 0;
                ticketday3week2 = 0;
                ticketday4week2 = 0;
                ticketday5week2 = 0;
                ticketday6week2 = 0;
                ticketday7week2 = 0;

                ticketday1week3 = 0;
                ticketday2week3 = 0;
                ticketday3week3 = 0;
                ticketday4week3 = 0;
                ticketday5week3 = 0;
                ticketday6week3 = 0;
                ticketday7week3 = 0;

                ticketday1week4 = 0;
                ticketday2week4 = 0;
                ticketday3week4 = 0;
                ticketday4week4 = 0;
                ticketday5week4 = 0;
                ticketday6week4 = 0;
                ticketday7week4 = 0;


                if (userinfo != null)
                {
                    //var vehs = ClientManager.GetAllClientManager(dbConnection);
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent,userinfo.SiteId, dbConnection);
                        var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request,userinfo.SiteId ,dbConnection);
 
                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                cusEvents.Add(custom);
                        }
                        foreach (var custom in reqcustomeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                cusEvents.Add(custom);
                        }
                        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateAndManagerIdHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection);

                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                preresolvedIncidents.Add(custom);
                        }
                        //preresolvedIncidents = CustomEvent.GetAllCustomEventByDateAndManagerIdHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                        //preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndDirectorIdHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection));
                        foreach (var manguser in managerusers)
                        {
                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                            var pclist = new List<string>();
                            var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent,userinfo.SiteId ,dbConnection);
                            var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request,userinfo.SiteId ,dbConnection);
                  
                            foreach (var custom in customeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    cusEvents.Add(custom);
                            }
                            foreach (var custom in reqcustomeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    cusEvents.Add(custom);
                            }
                            //preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), manguser, userinfo.SiteId, dbConnection));
                        }
                        preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndDirectorIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));
                        foreach (var manguser in managerusers)
                        {
                            preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), manguser, dbConnection));

                            var pclist = new List<string>();
                            var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                            foreach (var custom in customeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    preresolvedIncidents.Add(custom);
                            }
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId,dbConnection);
                        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                        preresolvedIncidents = preresolvedIncidents.Where(i => i.SiteId == userinfo.SiteId).ToList();


                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                        preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                       // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        var totalincidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                       // foreach (var site in sites)
                       // {
                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));
                            preresolvedIncidents.AddRange(cusEvents);
                       // }

                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));

                        var glist = CustomEvent.GetAllCustomEventByDateHandledAndSiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.SiteId, dbConnection);
                        preresolvedIncidents.AddRange(glist.Where(i => i.HandledBy == userinfo.Username).ToList());
                        
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                    }
                     
                    var userList = new List<string>();
                    var offenceuserList = new List<string>();
                    if (cusEvents.Count > 0)
                    {
                        var grouped2 = cusEvents.GroupBy(item => item.EventId);
                        cusEvents = grouped2.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                        var isValid = false;
                        var isMangUser = false;

                        if (userinfo.RoleId == (int)Role.Manager)
                            isMangUser = true;

                        foreach (var evs in cusEvents)
                        {

                            if (isMangUser)
                            {
                                if (evs.HandledBy == "LiveMIMSAlarmService" || evs.HandledBy == userinfo.Username)
                                    isValid = true;
                                else
                                    isValid = false;
                            }
                            else
                                isValid = true;

                            if (isValid)
                            {
                                if (evs.EventType != (int)CustomEvent.EventTypes.DriverOffence)
                                {
                                    if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                    {
                                        pendingCount++;
                                    }
                                    else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                    {
                                        completedCount++;
                                    }
                                    else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                                    {

                                    }
                                    else
                                    {
                                        if (evs.StatusName != "Escalated")
                                            inprogressCount++;
                                    }
                                    if (evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var reqEv = HotEvent.GetHotEventById(evs.Identifier, dbConnection);
                                        if (reqEv != null)
                                        {
                                            if (CommonUtility.getPCName(reqEv).ToLower() != "mims mobile")
                                            {
                                                var retname = CommonUtility.getPCName(reqEv).ToLower();
                                                if (!string.IsNullOrEmpty(retname))
                                                {
                                                    if (retname.ToLower() != userinfo.Username.ToLower())
                                                    {
                                                        userList.Add(retname);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (evs.UserName.ToLower() != userinfo.Username.ToLower())
                                                    userList.Add(evs.CustomerUName.ToLower());
                                            }
                                        }
                                    }
                                    if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                    {
                                        if (evs.UserName.ToLower() != userinfo.Username.ToLower())
                                        {
                                            userList.Add(evs.CustomerUName.ToLower());
                                        }
                                    }
                                }
                                else
                                {
                                    //offenceuserList.Add(evs.UserName.ToLower());
                                }
                            }
                        }
                    }
                    var offences = new List<CustomEvent>();
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        offences = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        offences.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection));
                        var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var allman in allmanagers)
                        {
                            offences.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), allman, userinfo.SiteId, dbConnection));
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);

                        //foreach (var site in sites)
                        offences.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        offences = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        offences = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                    }
                    foreach (var off in offences)
                    {
                        if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                        {
                            offenceuserList.Add(off.CustomerUName.ToLower());
                        }
                    }

                    var grouped = preresolvedIncidents.GroupBy(item => item.EventId);
                    preresolvedIncidents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();
                    

                    foreach (var resolve in preresolvedIncidents)
                    {
                        if (resolve.EventType != (int)CustomEvent.EventTypes.DriverOffence && resolve.Handled && resolve.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                            resolvedIncidents.Add(resolve);
                    }
                    total = pendingCount + inprogressCount + completedCount + resolvedIncidents.Count;
                    Dictionary<string, int> counts = userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());
                    var items = from pair in counts
                                orderby pair.Value descending
                                select pair;

                    Dictionary<string, int> offencecounts = offenceuserList.GroupBy(x => x)
                    .ToDictionary(g => g.Key,
                        g => g.Count());
                    var offenceitems = from pair in offencecounts
                                       orderby pair.Value descending
                                       select pair;
                    var offenceusercounter = 0;

                    if (offenceuserList.Count > 0)
                    {
                        foreach (KeyValuePair<string, int> pair in offenceitems)
                        {
                            if (offenceusercounter == 0)
                            {
                                lbTop1OffenceEvUser = pair.Key.ToString();
                                Top1TicName = lbTop1OffenceEvUser;
                                Top1TicCount = pair.Value.ToString();
                            }
                            else if (offenceusercounter == 1)
                            {
                                lbTop2OffenceEvUser = pair.Key.ToString();
                                Top2TicName = lbTop2OffenceEvUser;
                                Top2TicCount = pair.Value.ToString();
                            }
                            else if (offenceusercounter == 2)
                            {
                                lbTop3OffenceEvUser = pair.Key.ToString();
                                Top3TicName = lbTop3OffenceEvUser;
                                Top3TicCount = pair.Value.ToString();
                            }
                            else if (offenceusercounter == 3)
                            {
                                lbTop4OffenceEvUser = pair.Key.ToString();
                                Top4TicName = lbTop4OffenceEvUser;
                                Top4TicCount = pair.Value.ToString();
                            }
                            else if (offenceusercounter == 4)
                            {
                                lbTop5OffenceEvUser = pair.Key.ToString();
                                Top5TicName = lbTop5OffenceEvUser;
                                Top5TicCount = pair.Value.ToString();
                                break;
                            }
                            offenceusercounter++;
                        }
                    }
                    var ticTotal = Convert.ToInt32(Top1TicCount) + Convert.ToInt32(Top2TicCount) + Convert.ToInt32(Top3TicCount) + Convert.ToInt32(Top4TicCount) + Convert.ToInt32(Top5TicCount);
                    Top1Tic = ((int)Math.Round((float)Convert.ToInt32(Top1TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                    Top2Tic = ((int)Math.Round((float)Convert.ToInt32(Top2TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                    Top3Tic = ((int)Math.Round((float)Convert.ToInt32(Top3TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                    Top4Tic = ((int)Math.Round((float)Convert.ToInt32(Top4TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                    Top5Tic = ((int)Math.Round((float)Convert.ToInt32(Top5TicCount) / (float)ticTotal * (float)100)).ToString() + "%";

                    var hoteventusercounter = 0;
                    var hotTotal = 0;
                    var hot1Count = 0;
                    var hot2Count = 0;
                    var hot3Count = 0;
                    var hot4Count = 0;
                    var hot5Count = 0;
                    if (userList.Count > 0)
                    {
                        foreach (KeyValuePair<string, int> pair in items)
                        {
                            if (hoteventusercounter == 0)
                            {
 
                                hotTotal = hotTotal + pair.Value;
                                hot1Count = pair.Value;
                            }
                            else if (hoteventusercounter == 1)
                            {
 
                                hotTotal = hotTotal + pair.Value;
                                hot2Count = pair.Value;
                            }
                            else if (hoteventusercounter == 2)
                            {
 
                                hotTotal = hotTotal + pair.Value;
                                hot3Count = pair.Value;
                            }
                            else if (hoteventusercounter == 3)
                            {
 
                                hotTotal = hotTotal + pair.Value;
                                hot4Count = pair.Value;
                            }
                            else if (hoteventusercounter == 4)
                            {
 
                                hotTotal = hotTotal + pair.Value;
                                hot5Count = pair.Value;
                                break;
                            }
                            hoteventusercounter++;
                        }
                    }
                    if (hot1Count > 0)
                    {
                        hot1Percent = ((int)Math.Round((float)hot1Count / (float)hotTotal * (float)100)).ToString() + "%";
                        hot1display = "block";
                    }
                    else
                    {
                        hot1Percent = "0%";
                        hot1display = "none";
                    }
                    if (hot2Count > 0)
                    {
                        hot2Percent = ((int)Math.Round((float)hot2Count / (float)hotTotal * (float)100)).ToString() + "%";
                        hot2display = "block";
                    }
                    else
                    {
                        hot2Percent = "0%";
                        hot2display = "none";
                    }
                    if (hot3Count > 0)
                    {
                        hot3Percent = ((int)Math.Round((float)hot3Count / (float)hotTotal * (float)100)).ToString() + "%";
                        hot3display = "block";
                    }
                    else
                    {
                        hot3Percent = "0%";
                        hot3display = "none";
                    }
                    if (hot4Count > 0)
                    {
                        hot4Percent = ((int)Math.Round((float)hot4Count / (float)hotTotal * (float)100)).ToString() + "%";
                        hot4display = "block";
                    }
                    else
                    {
                        hot4Percent = "0%";
                        hot4display = "none";
                    }
                    if (hot5Count > 0)
                    {
                        hot5Percent = ((int)Math.Round((float)hot5Count / (float)hotTotal * (float)100)).ToString() + "%";
                        hot5display = "block";
                    }
                    else
                    {
                        hot5Percent = "0%";
                        hot5display = "none";
                    }
                    var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                    var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                    var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                    var resolvedPercentage = (int)Math.Round((float)resolvedIncidents.Count / (float)total * (float)100);

                    if (resolvedIncidents.Count > 0)
                    {
                        resolvedPercent = resolvedPercentage.ToString();
                        //lbresolvedPercent.Text = resolvedIncidents.Count.ToString();
                    }
                    else
                    {
                        resolvedPercent = "0";
                        //lbresolvedPercent.Text = "0";
                    }
                    var newCount = inprogressCount + completedCount + resolvedIncidents.Count;

                    if (newCount > 0)
                        HandledPercentage = ((int)Math.Round((float)newCount / (float)total * (float)100)).ToString();
                    else
                        HandledPercentage = "0";

                    lbHandledAlarms = newCount.ToString();
                    if (pendingCount > 0)
                    {
                        pendingPercent = pendingPercentage.ToString();
                        lbPendingAlarms = pendingCount.ToString();
                        //lbPending.Text = pendingCount.ToString();
                        //lbPendingpercent.Text = pendingPercentage.ToString();
                    }
                    else
                    {
                        pendingPercent = "0";
                       // lbPending.Text = "0";
                       // lbPendingpercent.Text = "0";
                        lbPendingAlarms = "0";
                    }
                    if (inprogressCount > 0)
                    {
                        inprogressPercent = inprogressPercentage.ToString();
                        //lbInprogresspercent.Text = inprogressPercentage.ToString();
                        //lbInprogress.Text = inprogressCount.ToString();
                    }
                    else
                    {
                       // lbInprogress.Text = "0";
                       // lbInprogresspercent.Text = "0";
                        inprogressPercent = "0";
                    }
                    if (completedCount > 0)
                    {
                        completedPercent = completedPercentage.ToString();
                        //lbCompletedpercent.Text = completedPercentage.ToString();
                        //lbCompleted.Text = completedCount.ToString();
                    }
                    else
                    {
                        completedPercent = "0";
                        //lbCompletedpercent.Text = "0";
                       // lbCompleted.Text = "0";
                    }

                   // lbTotalAlarms.Text = total.ToString();
                    lbTotalEvents.Text = total.ToString();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getEventStatus", ex, dbConnection, userinfo.SiteId);
            }
        }

        [WebMethod]
        public static List<string> getEventStatusRetrieve(string fromdate, string todate, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var fromDt = Convert.ToDateTime(fromdate);
                var toDt = Convert.ToDateTime(todate);
                toDt = toDt.Add(new TimeSpan(23, 59, 0));
                var intList = new List<int>();
                var cusEvents = new List<CustomEvent>();
                var resolvedIncidents = new List<CustomEvent>();
                var preresolvedIncidents = new List<CustomEvent>();
                var pendingCount = 0;
                var inprogressCount = 0;
                var completedCount = 0;
                var total = 0;
                if (fromDt < toDt)
                {
                    if (userinfo != null)
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                            var pclist = new List<string>();
                            var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                            var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);

                            foreach (var custom in customeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    cusEvents.Add(custom);
                            }
                            foreach (var custom in reqcustomeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    cusEvents.Add(custom);
                            }
                            preresolvedIncidents = CustomEvent.GetAllCustomEventByDateAndManagerIdHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection);

                            foreach (var custom in customeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    preresolvedIncidents.Add(custom);
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                            foreach (var manguser in managerusers)
                            {
                                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                                var pclist = new List<string>();
                                var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                                var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);

                                foreach (var custom in customeventlist)
                                {
                                    var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                    if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                        cusEvents.Add(custom);
                                }
                                foreach (var custom in reqcustomeventlist)
                                {
                                    var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                    if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                        cusEvents.Add(custom);
                                }
                            }
                            preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndDirectorIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));
                            foreach (var manguser in managerusers)
                            {
                                preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), manguser, dbConnection));

                                var pclist = new List<string>();
                                var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                                foreach (var custom in customeventlist)
                                {
                                    var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                    if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                        preresolvedIncidents.Add(custom);
                                }
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            cusEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId, dbConnection);
                            preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                            preresolvedIncidents = preresolvedIncidents.Where(i => i.SiteId == userinfo.SiteId).ToList();


                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                            preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                            preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            var totalincidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));
                            preresolvedIncidents.AddRange(cusEvents);

                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));

                            var glist = CustomEvent.GetAllCustomEventByDateHandledAndSiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.SiteId, dbConnection);
                            preresolvedIncidents.AddRange(glist.Where(i => i.HandledBy == userinfo.Username).ToList());

                        }
                        else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                            preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                        }

                        var userList = new List<string>();
                        var offenceuserList = new List<string>();

                        //cusEvents = cusEvents.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                        //preresolvedIncidents = preresolvedIncidents.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();
                        
                        cusEvents = cusEvents.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                        preresolvedIncidents = preresolvedIncidents.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();


                        if (cusEvents.Count > 0)
                        {
                            var grouped2 = cusEvents.GroupBy(item => item.EventId);
                            cusEvents = grouped2.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                            var isValid = false;
                            var isMangUser = false;

                            if (userinfo.RoleId == (int)Role.Manager)
                                isMangUser = true;

                            foreach (var evs in cusEvents)
                            {

                                if (isMangUser)
                                {
                                    if (evs.HandledBy == "LiveMIMSAlarmService" || evs.HandledBy == userinfo.Username)
                                        isValid = true;
                                    else
                                        isValid = false;
                                }
                                else
                                    isValid = true;

                                if (isValid)
                                {
                                    if (evs.EventType != (int)CustomEvent.EventTypes.DriverOffence)
                                    {
                                        if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                        {
                                            pendingCount++;
                                        }
                                        else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                        {
                                            completedCount++;
                                        }
                                        else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                                        {

                                        }
                                        else
                                        {
                                            if (evs.StatusName != "Escalated")
                                                inprogressCount++;
                                        }
                                        if (evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(evs.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv).ToLower() != "mims mobile")
                                                {
                                                    var retname = CommonUtility.getPCName(reqEv).ToLower();
                                                    if (!string.IsNullOrEmpty(retname))
                                                    {
                                                        if (retname.ToLower() != userinfo.Username.ToLower())
                                                        {
                                                            userList.Add(retname);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (evs.UserName.ToLower() != userinfo.Username.ToLower())
                                                        userList.Add(evs.CustomerUName.ToLower());
                                                }
                                            }
                                        }
                                        if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            if (evs.UserName.ToLower() != userinfo.Username.ToLower())
                                            {
                                                userList.Add(evs.CustomerUName.ToLower());
                                            }
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                        var offences = new List<CustomEvent>();
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            offences = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            offences.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection));
                            var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var allman in allmanagers)
                            {
                                offences.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, allman, userinfo.SiteId, dbConnection));
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            offences.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(toDt, fromDt, userinfo.ID, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            offences = CustomEvent.GetAllCustomEventByDateUNHandledAndCId(toDt, fromDt, userinfo.CustomerInfoId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            offences = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(toDt, fromDt, userinfo.SiteId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            offences = CustomEvent.GetAllCustomEventByDateUNHandled(toDt, fromDt, dbConnection);
                        }

                        //offences = offences.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                        offences = offences.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();


                        foreach (var off in offences)
                        {
                            if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                            {
                                offenceuserList.Add(off.CustomerUName.ToLower());
                            }
                        }



                        var grouped = preresolvedIncidents.GroupBy(item => item.EventId);
                        preresolvedIncidents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();


                        foreach (var resolve in preresolvedIncidents)
                        {
                            if (resolve.EventType != (int)CustomEvent.EventTypes.DriverOffence && resolve.Handled && resolve.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                                resolvedIncidents.Add(resolve);
                        }
                        total = pendingCount + inprogressCount + completedCount + resolvedIncidents.Count;
                        Dictionary<string, int> counts = userList.GroupBy(x => x)
                              .ToDictionary(g => g.Key,
                                            g => g.Count());
                        var items = from pair in counts
                                    orderby pair.Value descending
                                    select pair;

                        Dictionary<string, int> offencecounts = offenceuserList.GroupBy(x => x)
                        .ToDictionary(g => g.Key,
                            g => g.Count());
                        var offenceitems = from pair in offencecounts
                                           orderby pair.Value descending
                                           select pair;
                        var offenceusercounter = 0;
                        var zlbTop1OffenceEvUser = string.Empty;
                        var zlbTop2OffenceEvUser = string.Empty;
                        var zlbTop3OffenceEvUser = string.Empty;
                        var zlbTop4OffenceEvUser = string.Empty;
                        var zlbTop5OffenceEvUser = string.Empty;
                        var zTop1TicCount = "0";
                        var zTop2TicCount = "0";
                        var zTop3TicCount = "0";
                        var zTop4TicCount = "0";
                        var zTop5TicCount = "0";

                        if (offenceuserList.Count > 0)
                        {
                            foreach (KeyValuePair<string, int> pair in offenceitems)
                            {
                                if (offenceusercounter == 0)
                                {
                                    zlbTop1OffenceEvUser = pair.Key.ToString();
                                    //Top1TicName = lbTop1OffenceEvUser;
                                    //Top1TicCount = pair.Value.ToString();
                                }
                                else if (offenceusercounter == 1)
                                {
                                    zlbTop2OffenceEvUser = pair.Key.ToString();
                                    //Top2TicName = lbTop2OffenceEvUser;
                                    //Top2TicCount = pair.Value.ToString();
                                }
                                else if (offenceusercounter == 2)
                                {
                                    zlbTop3OffenceEvUser = pair.Key.ToString();
                                    //Top3TicName = lbTop3OffenceEvUser;
                                    //Top3TicCount = pair.Value.ToString();
                                }
                                else if (offenceusercounter == 3)
                                {
                                    zlbTop4OffenceEvUser = pair.Key.ToString();
                                    //Top4TicName = lbTop4OffenceEvUser;
                                    //Top4TicCount = pair.Value.ToString();
                                }
                                else if (offenceusercounter == 4)
                                {
                                    zlbTop5OffenceEvUser = pair.Key.ToString();
                                    //Top5TicName = lbTop5OffenceEvUser;
                                    //Top5TicCount = pair.Value.ToString();
                                    break;
                                }
                                offenceusercounter++;
                            }
                        }
                        var ticTotal = Convert.ToInt32(zTop1TicCount) + Convert.ToInt32(zTop2TicCount) + Convert.ToInt32(zTop3TicCount) + Convert.ToInt32(zTop4TicCount) + Convert.ToInt32(zTop5TicCount);
                        var zTop1Tic = ((int)Math.Round((float)Convert.ToInt32(zTop1TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                        var zTop2Tic = ((int)Math.Round((float)Convert.ToInt32(zTop2TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                        var zTop3Tic = ((int)Math.Round((float)Convert.ToInt32(zTop3TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                        var zTop4Tic = ((int)Math.Round((float)Convert.ToInt32(zTop4TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                        var zTop5Tic = ((int)Math.Round((float)Convert.ToInt32(zTop5TicCount) / (float)ticTotal * (float)100)).ToString() + "%";

                        if (Convert.ToInt32(zTop1TicCount) <= 0)
                        {
                            zTop1Tic = "0%";
                        }
                        if (Convert.ToInt32(zTop2TicCount) <= 0)
                        {
                            zTop2Tic = "0%";
                        }
                        if (Convert.ToInt32(zTop3TicCount) <= 0)
                        {
                            zTop3Tic = "0%";
                        }
                        if (Convert.ToInt32(zTop4TicCount) <= 0)
                        {
                            zTop4Tic = "0%";
                        }
                        if (Convert.ToInt32(zTop5TicCount) <= 0)
                        {
                            zTop5Tic = "0%";
                        }

                        var hoteventusercounter = 0;
                        var hotTotal = 0;
                        var hot1Count = 0;
                        var hot2Count = 0;
                        var hot3Count = 0;
                        var hot4Count = 0;
                        var hot5Count = 0;

                        var zlbTop1HotEvCount = string.Empty;
                        var zlbTop1HotEvUser = "0";

                        var zlbTop2HotEvCount = string.Empty;
                        var zlbTop2HotEvUser = "0";

                        var zlbTop3HotEvCount = string.Empty;
                        var zlbTop3HotEvUser = "0";

                        var zlbTop4HotEvCount = string.Empty;
                        var zlbTop4HotEvUser = "0";

                        var zlbTop5HotEvCount = string.Empty;
                        var zlbTop5HotEvUser = "0";

                        if (userList.Count > 0)
                        {
                            foreach (KeyValuePair<string, int> pair in items)
                            {
                                if (hoteventusercounter == 0)
                                {
                                    zlbTop1HotEvCount = pair.Value.ToString();
                                    zlbTop1HotEvUser = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot1Count = pair.Value;
                                }
                                else if (hoteventusercounter == 1)
                                {
                                    zlbTop2HotEvCount = pair.Value.ToString();
                                    zlbTop2HotEvUser = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot2Count = pair.Value;
                                }
                                else if (hoteventusercounter == 2)
                                {
                                    zlbTop3HotEvCount = pair.Value.ToString();
                                    zlbTop3HotEvUser = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot3Count = pair.Value;
                                }
                                else if (hoteventusercounter == 3)
                                {
                                    zlbTop4HotEvCount = pair.Value.ToString();
                                    zlbTop4HotEvUser = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot4Count = pair.Value;
                                }
                                else if (hoteventusercounter == 4)
                                {
                                    zlbTop5HotEvCount = pair.Value.ToString();
                                    zlbTop5HotEvUser = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot5Count = pair.Value;
                                    break;
                                }
                                hoteventusercounter++;
                            }
                        }
                        var zhot1Percent = "0%";
                        var zhot2Percent = "0%";
                        var zhot3Percent = "0%";
                        var zhot4Percent = "0%";
                        var zhot5Percent = "0%";

                        if (hot1Count > 0)
                        {
                            zhot1Percent = ((int)Math.Round((float)hot1Count / (float)hotTotal * (float)100)).ToString() + "%";
                            //hot1display = "block";
                        }
                        else
                        {
                            zhot1Percent = "0%";
                            //hot1display = "none";
                        }
                        if (hot2Count > 0)
                        {
                            zhot2Percent = ((int)Math.Round((float)hot2Count / (float)hotTotal * (float)100)).ToString() + "%";
                            //hot2display = "block";
                        }
                        else
                        {
                            zhot2Percent = "0%";
                            // hot2display = "none";
                        }
                        if (hot3Count > 0)
                        {
                            zhot3Percent = ((int)Math.Round((float)hot3Count / (float)hotTotal * (float)100)).ToString() + "%";
                            //hot3display = "block";
                        }
                        else
                        {
                            zhot3Percent = "0%";
                            //hot3display = "none";
                        }
                        if (hot4Count > 0)
                        {
                            zhot4Percent = ((int)Math.Round((float)hot4Count / (float)hotTotal * (float)100)).ToString() + "%";
                            //hot4display = "block";
                        }
                        else
                        {
                            zhot4Percent = "0%";
                            //hot4display = "none";
                        }
                        if (hot5Count > 0)
                        {
                            zhot5Percent = ((int)Math.Round((float)hot5Count / (float)hotTotal * (float)100)).ToString() + "%";
                            //hot5display = "block";
                        }
                        else
                        {
                            zhot5Percent = "0%";
                            //hot5display = "none";
                        }
                        var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                        var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                        var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                        var resolvedPercentage = (int)Math.Round((float)resolvedIncidents.Count / (float)total * (float)100);

                        var zresolvedPercent = "0";
                        var zlbresolvedPercent = "0";

                        if (resolvedIncidents.Count > 0)
                        {
                            zresolvedPercent = resolvedPercentage.ToString();
                            zlbresolvedPercent = resolvedIncidents.Count.ToString();
                        }
                        else
                        {
                            zresolvedPercent = "0";
                            zlbresolvedPercent = "0";
                        }
                        var newCount = inprogressCount + completedCount + resolvedIncidents.Count;

                        var zlbHandledAlarms = "0";

                        zlbHandledAlarms = newCount.ToString();
                        var zpendingPercent = "0";
                        var zlbPending = "0";
                        var zlbPendingpercent = "0";
                        var zlbPendingAlarms = "0";

                        var zlbInprogress = "0";
                        var zlbInprogresspercent = "0";
                        var zinprogressPercent = "0";

                        var zcompletedPercent = "0";
                        var zlbCompletedpercent = "0";
                        var zlbCompleted = "0";

                        if (pendingCount > 0)
                        {
                            zpendingPercent = pendingPercentage.ToString();
                            zlbPendingAlarms = pendingCount.ToString();
                            zlbPending = pendingCount.ToString();
                            zlbPendingpercent = pendingPercentage.ToString();
                        }

                        if (inprogressCount > 0)
                        {
                            zinprogressPercent = inprogressPercentage.ToString();
                            zlbInprogresspercent = inprogressPercentage.ToString();
                            zlbInprogress = inprogressCount.ToString();
                        }
                        if (completedCount > 0)
                        {
                            zcompletedPercent = completedPercentage.ToString();
                            zlbCompletedpercent = completedPercentage.ToString();
                            zlbCompleted = completedCount.ToString();
                        }
                        var zlbTotalAlarms = "0";
                        zlbTotalAlarms = total.ToString();

                        listy.Add(zlbresolvedPercent);
                        listy.Add(zresolvedPercent);

                        listy.Add(zlbCompleted);
                        listy.Add(zlbCompletedpercent);

                        listy.Add(zlbInprogress);
                        listy.Add(zlbInprogresspercent);

                        listy.Add(zlbPending);
                        listy.Add(zlbPendingpercent);

                        listy.Add(zlbTotalAlarms);
                        var sessions = new List<CustomEvent>();
                        var hotEvent6 = 0;
                        var hotEvent12 = 0;
                        var hotEvent18 = 0;
                        var hotEvent24 = 0;
                        var mobEvent6 = 0;
                        var mobEvent12 = 0;
                        var mobEvent18 = 0;
                        var mobEvent24 = 0;

                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            sessions = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            sessions.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection));
                            var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var allman in allmanagers)
                            {
                                sessions.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), allman, userinfo.SiteId, dbConnection));
                            }

                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            sessions = CustomEvent.GetAllCustomEventByDateUNHandledAndCId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.CustomerInfoId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            sessions = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.SiteId, dbConnection);

                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            sessions.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, dbConnection));

                        }
                        else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            sessions = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), dbConnection);

                        }
                         
                        foreach (var tem in sessions)
                        {
                            if (tem.EventType == CustomEvent.EventTypes.HotEvent)
                            {
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()))
                                {
                                    hotEvent6++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) > tem.RecevieTime))
                                {
                                    hotEvent12++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) > tem.RecevieTime))
                                {
                                    hotEvent18++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) > tem.RecevieTime))
                                {
                                    hotEvent24++;
                                }

                            }
                            else if (tem.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()))
                                {
                                    mobEvent6++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) > tem.RecevieTime))
                                {
                                    mobEvent12++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) > tem.RecevieTime))
                                {
                                    mobEvent18++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) > tem.RecevieTime))
                                {
                                    mobEvent24++;
                                }

                            }
                        }
                        listy.Add(mobEvent24.ToString());
                        listy.Add(mobEvent18.ToString());
                        listy.Add(mobEvent12.ToString());
                        listy.Add(mobEvent6.ToString());
                        listy.Add(hotEvent24.ToString());
                        listy.Add(hotEvent18.ToString());
                        listy.Add(hotEvent12.ToString());
                        listy.Add(hotEvent6.ToString());


                        var firstDayOfMonth = new DateTime(fromDt.Year, fromDt.Month, 1);
                        var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                        var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                        var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        listy.Add(firstDayOfMonth.ToString("MMM dd, yyyy"));
                        listy.Add(tenthDayOfMonth.ToString("MMM dd, yyyy"));
                        listy.Add(fifteenDayOfMonth.ToString("MMM dd, yyyy"));
                        listy.Add(twentyDayOfMonth.ToString("MMM dd, yyyy"));
                        listy.Add(lastDayOfMonth.ToString("MMM dd, yyyy"));
                        var offences2 = new List<CustomEvent>();
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            offences2 = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            offences2.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection));
                            var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var allman in allmanagers)
                            {
                                offences2.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, allman, userinfo.SiteId, dbConnection));
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            offences2 = CustomEvent.GetAllCustomEventByDateUNHandledAndCId(toDt, fromDt, userinfo.CustomerInfoId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            offences2 = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(toDt, fromDt, userinfo.SiteId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        { 
                            offences2.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(toDt, fromDt, userinfo.ID, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            offences2 = CustomEvent.GetAllCustomEventByDateUNHandled(toDt, fromDt, dbConnection);
                        }
                        var week1userList = new List<string>();
                        var week2userList = new List<string>();
                        var week3userList = new List<string>();
                        var week4userList = new List<string>();

                        //offences2 = offences2.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                        offences2 = offences2.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();
                         
                        foreach (var off in offences2)
                        {
                            if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                            {
                                if (off.RecevieTime.Value.Ticks > firstDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < tenthDayOfMonth.Ticks)
                                {
                                    week1userList.Add(off.CustomerUName);
                                }
                                else if (off.RecevieTime.Value.Ticks > tenthDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < fifteenDayOfMonth.Ticks)
                                {
                                    week2userList.Add(off.CustomerUName);
                                }
                                else if (off.RecevieTime.Value.Ticks > fifteenDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < twentyDayOfMonth.Ticks)
                                {
                                    week3userList.Add(off.CustomerUName);
                                }
                                else if (off.RecevieTime.Value.Ticks > twentyDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < lastDayOfMonth.Ticks)
                                {
                                    week4userList.Add(off.CustomerUName);
                                }
                            }
                        }
                        Dictionary<string, int> week1counts = week1userList.GroupBy(x => x)
                                  .ToDictionary(g => g.Key,
                                                g => g.Count());
                        Dictionary<string, int> week2counts = week2userList.GroupBy(x => x)
                                  .ToDictionary(g => g.Key,
                                                g => g.Count());
                        Dictionary<string, int> week3counts = week3userList.GroupBy(x => x)
                                  .ToDictionary(g => g.Key,
                                                g => g.Count());
                        Dictionary<string, int> week4counts = week4userList.GroupBy(x => x)
                                  .ToDictionary(g => g.Key,
                                                g => g.Count());

                        var week1items = from pair in week1counts
                                         orderby pair.Value descending
                                         select pair;
                        var week2items = from pair in week2counts
                                         orderby pair.Value descending
                                         select pair;
                        var week3items = from pair in week3counts
                                         orderby pair.Value descending
                                         select pair;
                        var week4items = from pair in week4counts
                                         orderby pair.Value descending
                                         select pair;
                        int count = 0;
                        foreach (KeyValuePair<string, int> pair in week1items)
                        {
                            if (count < 5)
                            {
                                listy.Add(pair.Value.ToString());
                                count++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        if (count < 5)
                        {
                            var i = 0;
                            for (i = 5; i > count; count++)
                            {
                                listy.Add("0");
                            }
                        }
                        count = 0;
                        foreach (KeyValuePair<string, int> pair in week2items)
                        {
                            if (count < 5)
                            {
                                listy.Add(pair.Value.ToString());
                                count++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        if (count < 5)
                        {
                            var i = 0;
                            for (i = 5; i > count; count++)
                            {
                                listy.Add("0");
                            }
                        }
                        count = 0;
                        foreach (KeyValuePair<string, int> pair in week3items)
                        {
                            if (count < 5)
                            {
                                listy.Add(pair.Value.ToString());
                                count++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        if (count < 5)
                        {
                            var i = 0;
                            for (i = 5; i > count; count++)
                            {
                                listy.Add("0");
                            }
                        }
                        count = 0;
                        foreach (KeyValuePair<string, int> pair in week4items)
                        {
                            if (count < 5)
                            {
                                listy.Add(pair.Value.ToString());
                                count++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        if (count < 5)
                        {
                            var i = 0;
                            for (i = 5; i > count; count++)
                            {
                                listy.Add("0");
                            }
                        }

                        listy.Add(zlbTop1OffenceEvUser);
                        listy.Add(zlbTop2OffenceEvUser);
                        listy.Add(zlbTop3OffenceEvUser);
                        listy.Add(zlbTop4OffenceEvUser);
                        listy.Add(zlbTop5OffenceEvUser);



                        listy.Add(zhot1Percent);
                        listy.Add(zhot2Percent);
                        listy.Add(zhot3Percent);
                        listy.Add(zhot4Percent);
                        listy.Add(zhot5Percent);

                        listy.Add(zlbTop1HotEvUser);
                        listy.Add(zlbTop2HotEvUser);
                        listy.Add(zlbTop3HotEvUser);
                        listy.Add(zlbTop4HotEvUser);
                        listy.Add(zlbTop5HotEvUser);

                        listy.Add(zlbTop1HotEvCount);
                        listy.Add(zlbTop2HotEvCount);
                        listy.Add(zlbTop3HotEvCount);
                        listy.Add(zlbTop4HotEvCount);
                        listy.Add(zlbTop5HotEvCount);

                    }      
                }
                else
                {
                    listy.Add("ERROR");
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getEventStatusRetrieve", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add("ERROR");
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getEventStatusTotal(string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var cusEvents = new List<CustomEvent>();
                var resolvedIncidents = new List<CustomEvent>();
                var preresolvedIncidents = new List<CustomEvent>();
                var pendingCount = 0;
                var inprogressCount = 0;
                var completedCount = 0;
                var total = 0;

                var fromDt = new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1); 
                var toDt = fromDt.AddMonths(1).AddDays(-1);
                toDt = toDt.Add(new TimeSpan(23, 59, 0));

                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                        var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);

                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                cusEvents.Add(custom);
                        }
                        foreach (var custom in reqcustomeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                cusEvents.Add(custom);
                        }
                        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateAndManagerIdHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection);

                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                preresolvedIncidents.Add(custom);
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                        foreach (var manguser in managerusers)
                        {
                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                            var pclist = new List<string>();
                            var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                            var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);

                            foreach (var custom in customeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    cusEvents.Add(custom);
                            }
                            foreach (var custom in reqcustomeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    cusEvents.Add(custom);
                            }
                        }
                        preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndDirectorIdHandled(toDt, fromDt, userinfo.ID, dbConnection));
                        foreach (var manguser in managerusers)
                        {
                            preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(toDt, fromDt, manguser, dbConnection));

                            var pclist = new List<string>();
                            var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                            foreach (var custom in customeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    preresolvedIncidents.Add(custom);
                            }
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId, dbConnection);
                        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(toDt, fromDt, dbConnection);
                        preresolvedIncidents = preresolvedIncidents.Where(i => i.SiteId == userinfo.SiteId).ToList();


                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(toDt, fromDt, dbConnection);
                        preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        var totalincidents = CustomEvent.GetAllCustomEventByDateHandled(toDt, fromDt, dbConnection);
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));
                        preresolvedIncidents.AddRange(cusEvents);

                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));

                        var glist = CustomEvent.GetAllCustomEventByDateHandledAndSiteId(toDt, fromDt, userinfo.SiteId, dbConnection);
                        preresolvedIncidents.AddRange(glist.Where(i => i.HandledBy == userinfo.Username).ToList());

                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(toDt, fromDt, dbConnection);
                    }

                    cusEvents = cusEvents.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                    preresolvedIncidents = preresolvedIncidents.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                    cusEvents = cusEvents.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                    preresolvedIncidents = preresolvedIncidents.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                    var userList = new List<string>();
                    var offenceuserList = new List<string>();
                    if (cusEvents.Count > 0)
                    {
                        var grouped2 = cusEvents.GroupBy(item => item.EventId);
                        cusEvents = grouped2.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                        var isValid = false;
                        var isMangUser = false;

                        if (userinfo.RoleId == (int)Role.Manager)
                            isMangUser = true;

                        foreach (var evs in cusEvents)
                        {

                            if (isMangUser)
                            {
                                if (evs.HandledBy == "LiveMIMSAlarmService" || evs.HandledBy == userinfo.Username)
                                    isValid = true;
                                else
                                    isValid = false;
                            }
                            else
                                isValid = true;

                            if (isValid)
                            {
                                if (evs.EventType != (int)CustomEvent.EventTypes.DriverOffence)
                                {
                                    if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                    {
                                        pendingCount++;
                                    }
                                    else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                    {
                                        completedCount++;
                                    }
                                    else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                                    {

                                    }
                                    else
                                    {
                                        if (evs.StatusName != "Escalated")
                                            inprogressCount++;
                                    }
                                    if (evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var reqEv = HotEvent.GetHotEventById(evs.Identifier, dbConnection);
                                        if (reqEv != null)
                                        {
                                            if (CommonUtility.getPCName(reqEv).ToLower() != "mims mobile")
                                            {
                                                var retname = CommonUtility.getPCName(reqEv).ToLower();
                                                if (!string.IsNullOrEmpty(retname))
                                                {
                                                    if (retname.ToLower() != userinfo.Username.ToLower())
                                                    {
                                                        userList.Add(retname);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (evs.UserName.ToLower() != userinfo.Username.ToLower())
                                                    userList.Add(evs.CustomerUName.ToLower());
                                            }
                                        }
                                    }
                                    if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                    {
                                        if (evs.UserName.ToLower() != userinfo.Username.ToLower())
                                        {
                                            userList.Add(evs.CustomerUName.ToLower());
                                        }
                                    }
                                }
                                else
                                {
 
                                }
                            }
                        }
                    }
                    var offences = new List<CustomEvent>();
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        offences = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        offences.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection));
                        var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var allman in allmanagers)
                        {
                            offences.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, allman, userinfo.SiteId, dbConnection));
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        offences.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndCId(toDt, fromDt, userinfo.CustomerInfoId, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        offences.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(toDt, fromDt, userinfo.ID, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        offences = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(toDt, fromDt, userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        offences = CustomEvent.GetAllCustomEventByDateUNHandled(toDt, fromDt, dbConnection);
                    }

                    offences = offences.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                    offences = offences.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                    foreach (var off in offences)
                    {
                        if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                        {
                            offenceuserList.Add(off.CustomerUName.ToLower());
                        }
                    }

                    var grouped = preresolvedIncidents.GroupBy(item => item.EventId);
                    preresolvedIncidents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();


                    foreach (var resolve in preresolvedIncidents)
                    {
                        if (resolve.EventType != (int)CustomEvent.EventTypes.DriverOffence && resolve.Handled && resolve.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                            resolvedIncidents.Add(resolve);
                    }
                    total = pendingCount + inprogressCount + completedCount + resolvedIncidents.Count;
                    Dictionary<string, int> counts = userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());
                    var items = from pair in counts
                                orderby pair.Value descending
                                select pair;

                    Dictionary<string, int> offencecounts = offenceuserList.GroupBy(x => x)
                    .ToDictionary(g => g.Key,
                        g => g.Count());
                    var offenceitems = from pair in offencecounts
                                       orderby pair.Value descending
                                       select pair;
                    var offenceusercounter = 0;
                    var zlbTop1OffenceEvUser = string.Empty;
                    var zlbTop2OffenceEvUser = string.Empty;
                    var zlbTop3OffenceEvUser = string.Empty;
                    var zlbTop4OffenceEvUser = string.Empty;
                    var zlbTop5OffenceEvUser = string.Empty;
                    var zTop1TicCount = "0";
                    var zTop2TicCount = "0";
                    var zTop3TicCount = "0";
                    var zTop4TicCount = "0";
                    var zTop5TicCount = "0";

                    if (offenceuserList.Count > 0)
                    {
                        foreach (KeyValuePair<string, int> pair in offenceitems)
                        {
                            if (offenceusercounter == 0)
                            {
                                zlbTop1OffenceEvUser = pair.Key.ToString();
                                //Top1TicName = lbTop1OffenceEvUser;
                                //Top1TicCount = pair.Value.ToString();
                            }
                            else if (offenceusercounter == 1)
                            {
                                zlbTop2OffenceEvUser = pair.Key.ToString();
                                //Top2TicName = lbTop2OffenceEvUser;
                                //Top2TicCount = pair.Value.ToString();
                            }
                            else if (offenceusercounter == 2)
                            {
                                zlbTop3OffenceEvUser = pair.Key.ToString();
                                //Top3TicName = lbTop3OffenceEvUser;
                                //Top3TicCount = pair.Value.ToString();
                            }
                            else if (offenceusercounter == 3)
                            {
                                zlbTop4OffenceEvUser = pair.Key.ToString();
                                //Top4TicName = lbTop4OffenceEvUser;
                                //Top4TicCount = pair.Value.ToString();
                            }
                            else if (offenceusercounter == 4)
                            {
                                zlbTop5OffenceEvUser = pair.Key.ToString();
                                //Top5TicName = lbTop5OffenceEvUser;
                                //Top5TicCount = pair.Value.ToString();
                                break;
                            }
                            offenceusercounter++;
                        }
                    }
                    var ticTotal = Convert.ToInt32(zTop1TicCount) + Convert.ToInt32(zTop2TicCount) + Convert.ToInt32(zTop3TicCount) + Convert.ToInt32(zTop4TicCount) + Convert.ToInt32(zTop5TicCount);
                    var zTop1Tic = ((int)Math.Round((float)Convert.ToInt32(zTop1TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                    var zTop2Tic = ((int)Math.Round((float)Convert.ToInt32(zTop2TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                    var zTop3Tic = ((int)Math.Round((float)Convert.ToInt32(zTop3TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                    var zTop4Tic = ((int)Math.Round((float)Convert.ToInt32(zTop4TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                    var zTop5Tic = ((int)Math.Round((float)Convert.ToInt32(zTop5TicCount) / (float)ticTotal * (float)100)).ToString() + "%";

                    var hoteventusercounter = 0;
                    var hotTotal = 0;
                    var hot1Count = 0;
                    var hot2Count = 0;
                    var hot3Count = 0;
                    var hot4Count = 0;
                    var hot5Count = 0;

                    var zlbTop1HotEvCount = string.Empty;
                    var zlbTop1HotEvUser = "0";

                    var zlbTop2HotEvCount = string.Empty;
                    var zlbTop2HotEvUser = "0";

                    var zlbTop3HotEvCount = string.Empty;
                    var zlbTop3HotEvUser = "0";

                    var zlbTop4HotEvCount = string.Empty;
                    var zlbTop4HotEvUser = "0";

                    var zlbTop5HotEvCount = string.Empty;
                    var zlbTop5HotEvUser = "0";
                     
                    if (userList.Count > 0)
                    {
                        foreach (KeyValuePair<string, int> pair in items)
                        {
                            if (hoteventusercounter == 0)
                            {
                                zlbTop1HotEvCount = pair.Value.ToString();
                                zlbTop1HotEvUser = pair.Key.ToString();
                                hotTotal = hotTotal + pair.Value;
                                hot1Count = pair.Value;
                            }
                            else if (hoteventusercounter == 1)
                            {
                                zlbTop2HotEvCount = pair.Value.ToString();
                                zlbTop2HotEvUser = pair.Key.ToString();
                                hotTotal = hotTotal + pair.Value;
                                hot2Count = pair.Value;
                            }
                            else if (hoteventusercounter == 2)
                            {
                                zlbTop3HotEvCount = pair.Value.ToString();
                                zlbTop3HotEvUser = pair.Key.ToString();
                                hotTotal = hotTotal + pair.Value;
                                hot3Count = pair.Value;
                            }
                            else if (hoteventusercounter == 3)
                            {
                                zlbTop4HotEvCount = pair.Value.ToString();
                                zlbTop4HotEvUser = pair.Key.ToString();
                                hotTotal = hotTotal + pair.Value;
                                hot4Count = pair.Value;
                            }
                            else if (hoteventusercounter == 4)
                            {
                                zlbTop5HotEvCount = pair.Value.ToString();
                                zlbTop5HotEvUser = pair.Key.ToString();
                                hotTotal = hotTotal + pair.Value;
                                hot5Count = pair.Value;
                                break;
                            }
                            hoteventusercounter++;
                        }
                    }
                    var zhot1Percent = "0%";
                    var zhot2Percent = "0%";
                    var zhot3Percent = "0%";
                    var zhot4Percent = "0%";
                    var zhot5Percent = "0%";

                    if (hot1Count > 0)
                    {
                        zhot1Percent = ((int)Math.Round((float)hot1Count / (float)hotTotal * (float)100)).ToString() + "%";
                        //hot1display = "block";
                    }
                    else
                    {
                        zhot1Percent = "0%";
                        //hot1display = "none";
                    }
                    if (hot2Count > 0)
                    {
                        zhot2Percent = ((int)Math.Round((float)hot2Count / (float)hotTotal * (float)100)).ToString() + "%";
                        //hot2display = "block";
                    }
                    else
                    {
                        zhot2Percent = "0%";
                       // hot2display = "none";
                    }
                    if (hot3Count > 0)
                    {
                        zhot3Percent = ((int)Math.Round((float)hot3Count / (float)hotTotal * (float)100)).ToString() + "%";
                        //hot3display = "block";
                    }
                    else
                    {
                        zhot3Percent = "0%";
                        //hot3display = "none";
                    }
                    if (hot4Count > 0)
                    {
                        zhot4Percent = ((int)Math.Round((float)hot4Count / (float)hotTotal * (float)100)).ToString() + "%";
                        //hot4display = "block";
                    }
                    else
                    {
                        zhot4Percent = "0%";
                        //hot4display = "none";
                    }
                    if (hot5Count > 0)
                    {
                        zhot5Percent = ((int)Math.Round((float)hot5Count / (float)hotTotal * (float)100)).ToString() + "%";
                        //hot5display = "block";
                    }
                    else
                    {
                        zhot5Percent = "0%";
                        //hot5display = "none";
                    }
                    var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                    var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                    var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                    var resolvedPercentage = (int)Math.Round((float)resolvedIncidents.Count / (float)total * (float)100);

                    var zresolvedPercent = "0";
                    var zlbresolvedPercent = "0";

                    if (resolvedIncidents.Count > 0)
                    {
                        zresolvedPercent = resolvedPercentage.ToString();
                        zlbresolvedPercent = resolvedIncidents.Count.ToString();
                    }
                    else
                    {
                        zresolvedPercent = "0";
                        zlbresolvedPercent = "0";
                    }
                    var newCount = inprogressCount + completedCount + resolvedIncidents.Count;

                    var zlbHandledAlarms = "0";

                    zlbHandledAlarms = newCount.ToString();
                    var zpendingPercent = "0";
                    var zlbPending = "0";
                    var zlbPendingpercent = "0";
                    var zlbPendingAlarms = "0";

                                            var zlbInprogress = "0";
                        var zlbInprogresspercent = "0";
                        var zinprogressPercent = "0";

                        var zcompletedPercent = "0";
                        var zlbCompletedpercent = "0";
                        var zlbCompleted = "0";

                    if (pendingCount > 0)
                    {
                        zpendingPercent = pendingPercentage.ToString();
                        zlbPendingAlarms = pendingCount.ToString();
                        zlbPending = pendingCount.ToString();
                        zlbPendingpercent = pendingPercentage.ToString();
                    } 

                    if (inprogressCount > 0)
                    {
                        zinprogressPercent = inprogressPercentage.ToString();
                        zlbInprogresspercent = inprogressPercentage.ToString();
                        zlbInprogress = inprogressCount.ToString();
                    }
                    if (completedCount > 0)
                    {
                        zcompletedPercent = completedPercentage.ToString();
                        zlbCompletedpercent = completedPercentage.ToString();
                        zlbCompleted = completedCount.ToString();
                    }
                    var zlbTotalAlarms = "0";
                    zlbTotalAlarms = total.ToString();

                    listy.Add(zlbresolvedPercent);
                    listy.Add(zresolvedPercent);

                    listy.Add(zlbCompleted);
                    listy.Add(zlbCompletedpercent);

                    listy.Add(zlbInprogress);
                    listy.Add(zlbInprogresspercent);

                    listy.Add(zlbPending);
                    listy.Add(zlbPendingpercent);

                    listy.Add(zlbTotalAlarms);
                    var sessions = new List<CustomEvent>();
                    var hotEvent6 = 0;
                    var hotEvent12 = 0;
                    var hotEvent18 = 0;
                    var hotEvent24 = 0;
                    var mobEvent6 = 0;
                    var mobEvent12 = 0;
                    var mobEvent18 = 0;
                    var mobEvent24 = 0;

                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        sessions = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        sessions.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection));
                        var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var allman in allmanagers)
                        {
                            sessions.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), allman, userinfo.SiteId, dbConnection));
                        } 
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        sessions = CustomEvent.GetAllCustomEventByDateUNHandledAndCId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.CustomerInfoId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        sessions = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.SiteId, dbConnection);

                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    { 
                        sessions.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, dbConnection));

                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        sessions = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), dbConnection);

                    }
                    foreach (var tem in sessions)
                    {
                        if (tem.EventType == CustomEvent.EventTypes.HotEvent)
                        {
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()))
                            {
                                hotEvent6++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) > tem.RecevieTime))
                            {
                                hotEvent12++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) > tem.RecevieTime))
                            {
                                hotEvent18++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) > tem.RecevieTime))
                            {
                                hotEvent24++;
                            }

                        }
                        else if (tem.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()))
                            {
                                mobEvent6++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) > tem.RecevieTime))
                            {
                                mobEvent12++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) > tem.RecevieTime))
                            {
                                mobEvent18++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) > tem.RecevieTime))
                            {
                                mobEvent24++;
                            }

                        }
                    }
                    listy.Add(mobEvent24.ToString());
                    listy.Add(mobEvent18.ToString());
                    listy.Add(mobEvent12.ToString());
                    listy.Add(mobEvent6.ToString());
                    listy.Add(hotEvent24.ToString()); 
                    listy.Add(hotEvent18.ToString()); 
                    listy.Add(hotEvent12.ToString()); 
                    listy.Add(hotEvent6.ToString());


                    var firstDayOfMonth = new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1);
                    var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                    var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                    var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    listy.Add(firstDayOfMonth.ToString("MMM dd, yyyy"));
                    listy.Add(tenthDayOfMonth.ToString("MMM dd, yyyy"));
                    listy.Add(fifteenDayOfMonth.ToString("MMM dd, yyyy"));
                    listy.Add(twentyDayOfMonth.ToString("MMM dd, yyyy"));
                    listy.Add(lastDayOfMonth.ToString("MMM dd, yyyy"));
                    var offences2 = new List<CustomEvent>();
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        offences2 = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        offences2.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, userinfo.ID, userinfo.SiteId, dbConnection));
                        var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var allman in allmanagers)
                        {
                            offences2.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(toDt, fromDt, allman, userinfo.SiteId, dbConnection));
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        offences2 = CustomEvent.GetAllCustomEventByDateUNHandledAndCId(toDt, fromDt, userinfo.CustomerInfoId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        offences2 = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(toDt, fromDt, userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                        offences2.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(toDt, fromDt, userinfo.ID, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        offences2 = CustomEvent.GetAllCustomEventByDateUNHandled(toDt, fromDt, dbConnection);
                    }
                    var week1userList = new List<string>();
                    var week2userList = new List<string>();
                    var week3userList = new List<string>();
                    var week4userList = new List<string>();

                    offences2 = offences2.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                    offences2 = offences2.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                    foreach (var off in offences2)
                    {
                        if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                        {
                            if (off.RecevieTime.Value.Ticks > firstDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < tenthDayOfMonth.Ticks)
                            {
                                week1userList.Add(off.CustomerUName);
                            }
                            else if (off.RecevieTime.Value.Ticks > tenthDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < fifteenDayOfMonth.Ticks)
                            {
                                week2userList.Add(off.CustomerUName);
                            }
                            else if (off.RecevieTime.Value.Ticks > fifteenDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < twentyDayOfMonth.Ticks)
                            {
                                week3userList.Add(off.CustomerUName);
                            }
                            else if (off.RecevieTime.Value.Ticks > twentyDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < lastDayOfMonth.Ticks)
                            {
                                week4userList.Add(off.CustomerUName);
                            }
                        }
                    }
                    Dictionary<string, int> week1counts = week1userList.GroupBy(x => x)
                              .ToDictionary(g => g.Key,
                                            g => g.Count());
                    Dictionary<string, int> week2counts = week2userList.GroupBy(x => x)
                              .ToDictionary(g => g.Key,
                                            g => g.Count());
                    Dictionary<string, int> week3counts = week3userList.GroupBy(x => x)
                              .ToDictionary(g => g.Key,
                                            g => g.Count());
                    Dictionary<string, int> week4counts = week4userList.GroupBy(x => x)
                              .ToDictionary(g => g.Key,
                                            g => g.Count());

                    var week1items = from pair in week1counts
                                     orderby pair.Value descending
                                     select pair;
                    var week2items = from pair in week2counts
                                     orderby pair.Value descending
                                     select pair;
                    var week3items = from pair in week3counts
                                     orderby pair.Value descending
                                     select pair;
                    var week4items = from pair in week4counts
                                     orderby pair.Value descending
                                     select pair;
                    int count = 0;
                    foreach (KeyValuePair<string, int> pair in week1items)
                    {
                        if (count < 5)
                        {
                            listy.Add(pair.Value.ToString());
                            count++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (count < 5)
                    {
                        var i = 0;
                        for (i = 5; i > count; count++)
                        {
                            listy.Add("0");
                        }
                    }
                    count = 0;
                    foreach (KeyValuePair<string, int> pair in week2items)
                    {
                        if (count < 5)
                        {
                            listy.Add(pair.Value.ToString());
                            count++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (count < 5)
                    {
                        var i = 0;
                        for (i = 5; i > count; count++)
                        {
                            listy.Add("0");
                        }
                    }
                    count = 0;
                    foreach (KeyValuePair<string, int> pair in week3items)
                    {
                        if (count < 5)
                        {
                            listy.Add(pair.Value.ToString());
                            count++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (count < 5)
                    {
                        var i = 0;
                        for (i = 5; i > count; count++)
                        {
                            listy.Add("0");
                        }
                    }
                    count = 0;
                    foreach (KeyValuePair<string, int> pair in week4items)
                    {
                        if (count < 5)
                        {
                            listy.Add(pair.Value.ToString());
                            count++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (count < 5)
                    {
                        var i = 0;
                        for (i = 5; i > count; count++)
                        {
                            listy.Add("0");
                        }
                    }

                    listy.Add(zlbTop1OffenceEvUser);
                    listy.Add(zlbTop2OffenceEvUser);
                    listy.Add(zlbTop3OffenceEvUser);
                    listy.Add(zlbTop4OffenceEvUser);
                    listy.Add(zlbTop5OffenceEvUser);



                    listy.Add(zhot1Percent);
                    listy.Add(zhot2Percent);
                    listy.Add(zhot3Percent);
                    listy.Add(zhot4Percent);
                    listy.Add(zhot5Percent);

                    listy.Add(zlbTop1HotEvUser);
                    listy.Add(zlbTop2HotEvUser);
                    listy.Add(zlbTop3HotEvUser);
                    listy.Add(zlbTop4HotEvUser);
                    listy.Add(zlbTop5HotEvUser);

                    listy.Add(zlbTop1HotEvCount);
                    listy.Add(zlbTop2HotEvCount);
                    listy.Add(zlbTop3HotEvCount);
                    listy.Add(zlbTop4HotEvCount);
                    listy.Add(zlbTop5HotEvCount);

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default", "getEventStatusTotal", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add("ERROR");
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getActivityReportData(int id,string uname , Users userinfo)
        {
            var listy = new List<string>();
            //var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
//                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
//HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

//                if (isValidSession == null)
//                {
//                    listy.Add("LOGOUT");
//                    return listy;
//                }

                var sessions = new List<CustomEvent>();
                var hotEvent6 = 0;
                var hotEvent12 = 0;
                var hotEvent18 = 0;
                var hotEvent24 = 0;
                var mobEvent6 = 0;
                var mobEvent12 = 0;
                var mobEvent18 = 0;
                var mobEvent24 = 0;
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        sessions = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        sessions.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection));
                        var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var allman in allmanagers)
                        {
                            sessions.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), allman, userinfo.SiteId, dbConnection));
                        }
                    
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        sessions = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)),userinfo.SiteId, dbConnection);

                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                        sessions.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), userinfo.ID, dbConnection));

                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        sessions = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), dbConnection);
                       
                    }
                    foreach (var tem in sessions)
                    {
                        if (tem.EventType == CustomEvent.EventTypes.HotEvent)
                        {
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()))
                            {
                                hotEvent6++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) > tem.RecevieTime))
                            {
                                hotEvent12++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) > tem.RecevieTime))
                            {
                                hotEvent18++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) > tem.RecevieTime))
                            {
                                hotEvent24++;
                            }

                        }
                        else if (tem.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()))
                            {
                                mobEvent6++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) > tem.RecevieTime))
                            {
                                mobEvent12++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) > tem.RecevieTime))
                            {
                                mobEvent18++;
                            }
                            if ((CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) > tem.RecevieTime))
                            {
                                mobEvent24++;
                            }

                        }
                    }
                    listy.Add(mobEvent24.ToString());
                    listy.Add(hotEvent24.ToString());

                    listy.Add(mobEvent18.ToString());
                    listy.Add(hotEvent18.ToString());

                    listy.Add(mobEvent12.ToString());
                    listy.Add(hotEvent12.ToString());

                    listy.Add(mobEvent6.ToString());
                    listy.Add(hotEvent6.ToString());

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getActivityReportData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        
        [WebMethod]
        public static List<string> getOffenceChart(int id,string uname , Users userinfo)
        {
            var listy = new List<string>();
           // var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
//                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
//HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

//                if (isValidSession == null)
//                {
//                    listy.Add("LOGOUT");
//                    return listy;
//                }

                var firstDayOfMonth = new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1);
                var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                listy.Add(firstDayOfMonth.ToString("MMM dd, yyyy"));
                listy.Add(tenthDayOfMonth.ToString("MMM dd, yyyy"));
                listy.Add(fifteenDayOfMonth.ToString("MMM dd, yyyy"));
                listy.Add(twentyDayOfMonth.ToString("MMM dd, yyyy"));
                listy.Add(lastDayOfMonth.ToString("MMM dd, yyyy"));
                var offences = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    offences = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID,userinfo.SiteId ,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    offences.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection));
                    var allmanagers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var allman in allmanagers)
                    {
                        offences.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), allman, userinfo.SiteId, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    offences = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)),userinfo.SiteId ,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    offences.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    offences = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }
                var week1userList = new List<string>();
                var week2userList = new List<string>();
                var week3userList = new List<string>();
                var week4userList = new List<string>();
                foreach (var off in offences)
                {
                    if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                    {
                        if (off.RecevieTime.Value.Ticks > firstDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < tenthDayOfMonth.Ticks)
                        {
                            week1userList.Add(off.UserName);
                        }
                        else if (off.RecevieTime.Value.Ticks > tenthDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < fifteenDayOfMonth.Ticks)
                        {
                            week2userList.Add(off.UserName);
                        }
                        else if (off.RecevieTime.Value.Ticks > fifteenDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < twentyDayOfMonth.Ticks)
                        {
                            week3userList.Add(off.UserName);
                        }
                        else if (off.RecevieTime.Value.Ticks > twentyDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < lastDayOfMonth.Ticks)
                        {
                            week4userList.Add(off.UserName);
                        }
                    }
                }
                Dictionary<string, int> week1counts = week1userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());
                Dictionary<string, int> week2counts = week2userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());
                Dictionary<string, int> week3counts = week3userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());
                Dictionary<string, int> week4counts = week4userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());

                var week1items = from pair in week1counts
                                 orderby pair.Value descending
                                 select pair;
                var week2items = from pair in week2counts
                                 orderby pair.Value descending
                                 select pair;
                var week3items = from pair in week3counts
                                 orderby pair.Value descending
                                 select pair;
                var week4items = from pair in week4counts
                                 orderby pair.Value descending
                                 select pair;
                int count = 0;
                foreach (KeyValuePair<string, int> pair in week1items)
                {
                    if (count < 5)
                    {
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (count < 5)
                {
                    var i = 0;
                    for (i = 5; i > count; count++)
                    {
                        listy.Add("0");
                    }
                }
                count = 0;
                foreach (KeyValuePair<string, int> pair in week2items)
                {
                    if (count < 5)
                    {
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (count < 5)
                {
                    var i = 0;
                    for (i = 5; i > count; count++)
                    {
                        listy.Add("0");
                    }
                }
                count = 0;
                foreach (KeyValuePair<string, int> pair in week3items)
                {
                    if (count < 5)
                    {
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (count < 5)
                {
                    var i = 0;
                    for (i = 5; i > count; count++)
                    {
                        listy.Add("0");
                    }
                }
                count = 0;
                foreach (KeyValuePair<string, int> pair in week4items)
                {
                    if (count < 5)
                    {
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (count < 5)
                {
                    var i = 0;
                    for (i = 5; i > count; count++)
                    {
                        listy.Add("0");
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getOffenceChart", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getReminders(int id,string uname ,Users userinfo)
        {
            var listy = new List<string>();
         //   var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

//                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
//HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

//                if (isValidSession == null)
//                {
//                    listy.Add("LOGOUT");
//                    return listy;
//                }

                var getreminders = Reminders.GetAllRemindersByDateAndCreator(CommonUtility.getDTNow().Date, userinfo.Username, dbConnection);
                var remindercount = 0;
                foreach (var rem in getreminders)
                {
                    if (remindercount == 2)
                        break;

                    if (!rem.IsCompleted)
                    {
                        var colorcode = string.Empty;
                        var colorcode2 = string.Empty;
                        if (rem.ColorCode == (int)Reminders.ColorCodes.Green)
                        {
                            colorcode = "green-border";
                            colorcode2 = "green";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Blue)
                        {
                            colorcode = "blue-border";
                            colorcode2 = "blue";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Yellow)
                        {
                            colorcode = "yellow-border";
                            colorcode2 = "yellow";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Red)
                        {
                            colorcode = "red-border";
                            colorcode2 = "red";
                        }

                        var date = rem.FromDate + "-" + rem.ToDate + " " + rem.ReminderDate.Value.ToString("dddd MMMM dd, yyyy");

                        var jsonstring = "<div onclick='reminderChoice(&apos;" + rem.ID + "&apos;,&apos;" + date + "&apos;,&apos;" + rem.ReminderNote + "&apos;,&apos;" + rem.ReminderTopic + "&apos;,&apos;" + colorcode2 + "&apos;)' href='#' data-target='#" + colorcode2 + "reminderboxs' data-toggle='modal'><time style='margin-left:15px'>" + rem.FromDate + "-" + rem.ToDate + "</time><p style='margin-left:15px'>" + rem.ReminderTopic + "</p><div style='margin-left:-5px' class='vertical-line " + colorcode + "'></div></div>";
                        listy.Add(jsonstring);
                        remindercount++;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getReminders", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string deleteReminder(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }


                Reminders.DeleteReminderById(id, dbConnection);
                SystemLogger.SaveSystemLog(dbConnectionAudit, "Default", id.ToString(), id.ToString(), userinfo, "Delete Reminders" + id);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "deleteReminder", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static List<string> getRecentActivity(int id,string uname ,Users userinfo)
        {
            var listy = new List<string>();
           // var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

//                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
//HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

//                if (isValidSession == null)
//                {
//                    listy.Add("LOGOUT");
//                    return listy;
//                }

                var getrecenthistory = new List<EventHistory>();
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    getrecenthistory = EventHistory.GetTopEventHistory(dbConnection);
                else if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    var getrecenthistory2 = EventHistory.GetTopEventHistoryBySiteId(userinfo.SiteId, dbConnection);
                    foreach (var rec in getrecenthistory2)
                    {
                        var gCus = CustomEvent.GetCustomEventById(rec.EventId, dbConnection);
                        if (gCus != null && gCus.SiteId == userinfo.SiteId)
                            getrecenthistory.Add(rec);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    var getrecenthistory2 = EventHistory.GetTopEventHistoryBySiteId(userinfo.SiteId, dbConnection);
                    foreach (var rec in getrecenthistory2)
                    {
                        var gCus = CustomEvent.GetCustomEventById(rec.EventId, dbConnection);
                        if (gCus != null && gCus.SiteId == userinfo.SiteId)
                            getrecenthistory.Add(rec);
                    }
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    var getrecenthistory2 = EventHistory.GetTopEventHistoryByCId(userinfo.CustomerInfoId, dbConnection);
                    foreach (var rec in getrecenthistory2)
                    {
                        var gCus = CustomEvent.GetCustomEventById(rec.EventId, dbConnection);
                        if (gCus != null && gCus.CustomerId == userinfo.CustomerInfoId)
                            getrecenthistory.Add(rec);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    //{
                    var getrecenthistory2 = EventHistory.GetTopEventHistoryByLevel5(userinfo.ID, dbConnection);
                        foreach (var rec in getrecenthistory2)
                        {
                            //if (rec.CreatedBy != userinfo.Username)
                            //{
                                //var gCus = CustomEvent.GetCustomEventById(rec.EventId, dbConnection);
                                //if (gCus.SiteId == site.SiteId)
                              //      getrecenthistory.Add(rec);
                            //}
                            //else
                                getrecenthistory.Add(rec);
                        }
                   // }
                }
                var usersStringList = new List<string>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    usersStringList.Add(userinfo.Username.ToLower());
                    var userlist = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                    foreach (var user in userlist)
                    {
                        usersStringList.Add(user.Username.ToLower());
                    }
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    usersStringList.Add(userinfo.Username.ToLower());
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        usersStringList.Add(usr.Username.ToLower());
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            usersStringList.Add(subsubuser.Username.ToLower());
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        usersStringList.Add(subsubuser.Username.ToLower());
                    } 
                }

                getrecenthistory = getrecenthistory.Where(i => i.IncidentAction != 10).ToList();
                var grouped = getrecenthistory.GroupBy(item => item.Id);
                getrecenthistory = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                foreach (var rem in getrecenthistory)
                {
                    
                    if (usersStringList.Count > 0)
                    {
                        rem.CreatedBy = System.Text.RegularExpressions.Regex.Replace(rem.CreatedBy, @"\s+", "");
                        rem.UserName = System.Text.RegularExpressions.Regex.Replace(rem.UserName, @"\s+", "");
                        if ((rem.CreatedBy.ToLower() == userinfo.Username.ToLower() || usersStringList.Contains(rem.CreatedBy.ToLower())) && usersStringList.Contains(rem.UserName.ToLower()))//usersStringList.Contains(rem.CreatedBy) || usersStringList.Contains(rem.UserName))
                        {
                            var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                            if (gethotevent != null && gethotevent.StatusName != "Escalated")
                            {
                                if (gethotevent.EventType != CustomEvent.EventTypes.DriverOffence)
                                {
                                    var isvalid = false;
                                    if(gethotevent.EventType == CustomEvent.EventTypes.Incident)
                                    {
                                        isvalid = true;
                                    }
                                    else if (gethotevent.isViewed && (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent || gethotevent.EventType == CustomEvent.EventTypes.Request))
                                    {
                                        isvalid = true;
                                    }
                                    if (isvalid)
                                    {
                                        if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            gethotevent.Name = gethotevent.EventTypeName;
                                            //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                                            //if (mobEv != null)
                                            //{
                                             //   if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                              //      gethotevent.Name = "None";
                                               // else
                                                //    gethotevent.Name = mobEv.EventTypeName;
                                            //}
                                        }
                                        else if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            //var reqEv = HotEvent.GetHotEventById(gethotevent.Identifier, dbConnection);
                                            //var splitName = reqEv.Name.Split('^');

                                            var regname = gethotevent.RequestName;
                                            var splitName = regname.Split('^');
                                            if (splitName.Length > 1)
                                            {
                                                gethotevent.Name = splitName[1];
                                            }
                                        }
                                        var incidentType = gethotevent.Name;

                                        var incidentLocation = string.Empty;

                                        var actioninfo = string.Empty;

                                        if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                                        {
                                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(gethotevent.Latitude.ToString(), gethotevent.Longtitude.ToString(), getClientLic);

                                            actioninfo = "created";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1' ><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span></br>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                        {
                                            actioninfo = "completed";

                                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                                        {
                                            actioninfo = "dispatched";

                                            incidentLocation = "to " + rem.ACustomerUName;

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                                        {
                                            actioninfo = "resolved";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                                        {
                                            actioninfo = "rejected";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                                        {
                                            actioninfo = "parked";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                        {
                                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                                            actioninfo = "engaged";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.ACustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span></br>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                                        {
                                            actioninfo = "released";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                                        {
                                            actioninfo = "arrived to the incident ";

                                            incidentLocation = " at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + rem.ACustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.Regional || userinfo.RoleId == (int)Role.ChiefOfficer || userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                            if (gethotevent != null)
                            {

                                if (gethotevent.EventType != CustomEvent.EventTypes.DriverOffence)
                                {
                                    var isvalid = false;

                                    if(gethotevent.EventType == CustomEvent.EventTypes.Incident)
                                    {
                                        isvalid = true;
                                    }
                                    else if (gethotevent.isViewed && (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent || gethotevent.EventType == CustomEvent.EventTypes.Request))
                                    {
                                        isvalid = true;
                                    }
                                    if (userinfo.RoleId == (int)Role.Director && isvalid)
                                    {
                                        if (gethotevent.StatusName != "Escalated")
                                            isvalid = true;
                                        else
                                            isvalid = false;
                                    }
                                    if (isvalid)
                                    {
                                        if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            gethotevent.Name = gethotevent.EventTypeName;
                                            //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                                            //if (mobEv != null)
                                            //{
                                            //    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                            //        gethotevent.Name = "None";
                                            //    else
                                            //        gethotevent.Name = mobEv.EventTypeName;
                                            //}
                                        }
                                        else if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            //var reqEv = HotEvent.GetHotEventById(gethotevent.Identifier, dbConnection);
                                             
                                            var splitName = gethotevent.RequestName.Split('^');
                                            if (splitName.Length > 1)
                                            {
                                                gethotevent.Name = splitName[1];
                                            }
                                        }
                                        var incidentType = gethotevent.Name;

                                        var incidentLocation = string.Empty;

                                        var actioninfo = string.Empty;
                                        if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                                        {
                                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(gethotevent.Latitude.ToString(), gethotevent.Longtitude.ToString(), getClientLic);

                                            actioninfo = "created";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1' ><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span></br>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                        {
                                            actioninfo = "completed";

                                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.ACustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                                        {
                                            actioninfo = "dispatched";

                                            incidentLocation = "to " + rem.ACustomerUName;

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                                        {
                                            actioninfo = "resolved";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                                        {
                                            actioninfo = "rejected";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                                        {
                                            actioninfo = "parked";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.CustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                        {
                                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                                            actioninfo = "engaged";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.ACustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span></br>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                                        {
                                            actioninfo = "released";

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.ACustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                                        {
                                            actioninfo = "arrived to the incident ";

                                            incidentLocation = " at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                                            var jsonstring = "<div class='col-md-2 col-sm-1'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2 col-sm-1' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + rem.EventId + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8 col-sm-10' ><p><span>" + rem.ACustomerUName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                            listy.Add(jsonstring);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "getRecentActivity", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        
        [WebMethod]
        public static List<string> getLiveActivity(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var getrecenthistory = new List<CustomEvent>();
                var dtnow = new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, CommonUtility.getDTNow().Day);

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    getrecenthistory = CustomEvent.GetAllCustomEventByUnViewedDateRange(dtnow.Add(new TimeSpan(23, 59, 0)), dtnow, dbConnection);
                else if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                    getrecenthistory = CustomEvent.GetAllCustomEventByUnViewedDateRangeAndSiteId(dtnow.Add(new TimeSpan(23, 59, 0)), dtnow, userinfo.SiteId, dbConnection);
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin  )
                    getrecenthistory = CustomEvent.GetAllCustomEventByUnViewedDateRangeAndCId(dtnow.Add(new TimeSpan(23, 59, 0)), dtnow, userinfo.CustomerInfoId, dbConnection);
                
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var site in sites)
                        getrecenthistory.AddRange(CustomEvent.GetAllCustomEventByUnViewedDateRangeAndLevel5(dtnow.Add(new TimeSpan(23, 59, 0)), dtnow, userinfo.ID, dbConnection));
                }
                var usersStringList = new List<string>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var userlist = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
 
                    foreach (var user in userlist)
                    {
                        usersStringList.Add(user.Username.ToLower());
                    }
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        usersStringList.Add(usr.Username.ToLower());
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            usersStringList.Add(subsubuser.Username.ToLower());
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        usersStringList.Add(subsubuser.Username.ToLower());
                    } 
                }


                foreach (var gethotevent in getrecenthistory)
                {
                    if (usersStringList.Count > 0)
                    {
                        gethotevent.UserName = System.Text.RegularExpressions.Regex.Replace(gethotevent.UserName, @"\s+", "");
                        gethotevent.UserName = gethotevent.UserName.ToLower();
                        if (usersStringList.Contains(gethotevent.UserName))
                        {
                            if (gethotevent != null)
                            {
                                if (gethotevent.EventType != CustomEvent.EventTypes.DriverOffence && gethotevent.EventType != CustomEvent.EventTypes.Incident)
                                {
                                    if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var splitName = gethotevent.ExtraName.Split('^');
                                        if (splitName.Length > 1)
                                        {
                                            gethotevent.ExtraName = splitName[1];
                                        }
                                    }
                                    var incidentType = gethotevent.ExtraName;

                                    var incidentLocation = string.Empty;

                                    var actioninfo = string.Empty;

                                    actioninfo = "created";

                                    var militarytime = gethotevent.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString("HH:mm");

                                    var jsonstring = "<div class='col-md-2 col-sm-1'><p style='font-size: 11'>" + militarytime + "</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + gethotevent.EventId + "&apos;)'><span style='margin-left:3px;margin-top: 2px;' class='circle-point-container'><span id='Inc┴" + gethotevent.EventId + "' class='circle-point3 circle-point-white' ></span></span></div><div class='col-md-8' ><p style='font-size: 11'><span style='margin-left: 0px;'>" + gethotevent.CustomerUName.ToLower() + "</span></br> created <span> " + incidentType + "-" + gethotevent.EventId + "</span>" + incidentLocation + "</p></div><div style='margin-top:7px;height:75%;'  class='vertical-line'></div>";

                                    listy.Add(jsonstring);
                                    listy.Add(gethotevent.EventId.ToString());
                                }

                            }
                        }
                    }
                    else
                    {
                        if (userinfo.RoleId == (int)Role.Regional || userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.ChiefOfficer || userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            if (gethotevent != null)
                            {
                                if (gethotevent.EventType != CustomEvent.EventTypes.DriverOffence && gethotevent.EventType != CustomEvent.EventTypes.Incident)
                                {
                                    if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var splitName = gethotevent.ExtraName.Split('^');
                                        if (splitName.Length > 1)
                                        {
                                            gethotevent.ExtraName = splitName[1];
                                        }
                                    }
                                    var incidentType = gethotevent.ExtraName;

                                    var incidentLocation = string.Empty;

                                    var actioninfo = string.Empty;

                                    actioninfo = "created";

                                    var militarytime = gethotevent.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString("HH:mm");

                                    var jsonstring = "<div class='col-md-2 col-sm-1'><p style='font-size: 11'>" + militarytime + "</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + gethotevent.EventId + "&apos;)'><span style='margin-left:5px;margin-top: 2px;' class='circle-point-container'><span id='Inc┴" + gethotevent.EventId + "' class='circle-point3 circle-point-white' ></span></span></div><div class='col-md-8' ><p style='font-size: 11'><span style='margin-left: 0px;'>" + gethotevent.CustomerUName.ToLower() + "</span></br> created <span> " + incidentType + "-" + gethotevent.EventId + "</span>" + incidentLocation + "</p></div><div style='margin-top:7px;height:75%;'  class='vertical-line'></div>";

                                    listy.Add(jsonstring);
                                    listy.Add(gethotevent.EventId.ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "getLiveActivity", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getGPSData(int id, string uname)
        {
            var json = string.Empty; 

            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
    HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }
            else
            {
                

                var cusEvents = new List<CustomEvent>();
                var errorevents = new CustomEvent();
                try
                {
                    json += "[";
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {

                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                                var pclist = new List<string>();
                                var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                                var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);

                                foreach (var custom in customeventlist)
                                {
                                    var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                    if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                        cusEvents.Add(custom);
                                }
                                foreach (var custom in reqcustomeventlist)
                                {
                                    var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                    if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                        cusEvents.Add(custom);
                                }
                            }
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {
                                var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));

                                foreach (var manguser in managerusers)
                                {
                                    cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                                    var pclist = new List<string>();
                                    var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                                    var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);

                                    foreach (var custom in customeventlist)
                                    {
                                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                            cusEvents.Add(custom);
                                    }
                                    foreach (var custom in reqcustomeventlist)
                                    {
                                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                            cusEvents.Add(custom);
                                    }
                                }
                            }
                            else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            {
                                cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Regional)
                            { 
                                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));

                                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                            }
                            if (cusEvents != null)
                            {
                                if (cusEvents.Count > 0)
                                {
                                    var isValid = false;
                                    var isMangUser = false;

                                    if (userinfo.RoleId == (int)Role.Manager)
                                        isMangUser = true;
                                    foreach (var item in cusEvents)
                                    {
                                        if (isMangUser)
                                        {
                                            if (item.HandledBy == "LiveMIMSAlarmService" || item.HandledBy == userinfo.Username)
                                                isValid = true;
                                            else
                                                isValid = false;
                                        }
                                        else
                                            isValid = true;

                                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve || item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                                            isValid = false;

                                        if (isValid)
                                        {
                                            errorevents = item;
                                            if (item.EventType != CustomEvent.EventTypes.DriverOffence)
                                            {
                                                if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                                {
                                                    item.Name = item.EventTypeName; 
                                                }
                                                else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    if (item.EventType == CustomEvent.EventTypes.Request)
                                                    {
                                                        var splitName = item.RequestName.Split('^');
                                                        if (splitName.Length > 1)
                                                        {
                                                            item.Name = splitName[1];
                                                        }
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(item.Longtitude) && !string.IsNullOrEmpty(item.Latitude))
                                                {
                                                    if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                                    {
                                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"PURPLE\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + item.Name + "-" + item.CustomerIncidentId + "\"},";
                                                    }
                                                    else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete || item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                                                    {
                                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"GREEN\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + item.Name + "-" + item.CustomerIncidentId + "\"},";
                                                    }
                                                    else
                                                    {
                                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"YELLOW\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + item.Name + "-" + item.CustomerIncidentId + "\"},";
                                                    }
                                                }
                                                else
                                                {
                                                    if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                                    {
                                                        var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, item.EventId);
                                                        foreach (var geofence in geofencelocs)
                                                        {
                                                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED" + item.EventId.ToString() + "\",\"Logs\" : \"#5e0021\"},";
                                                        }
                                                    }
                                                    else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                                    {
                                                        var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, item.EventId);
                                                        foreach (var geofence in geofencelocs)
                                                        {
                                                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED" + item.EventId.ToString() + "\",\"Logs\" : \"#17ff33\"},";
                                                        }
                                                    }
                                                    else
                                                    {                        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                                                        var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, item.EventId);
                                                        foreach (var geofence in geofencelocs)
                                                        {
                                                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED" + item.EventId.ToString() + "\",\"Logs\" : \"#ff791c\"},";
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";

                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Default.aspx", "getGPSData" + errorevents.EventId, ex, dbConnection, userinfo.SiteId);
                }
            }
            return json;
        }

        [WebMethod]
        public static string getGPSDataTasks(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                json += "[";
                var fullcollection = new List<UserTask>();

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                    fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Date, dbConnection);
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                }
                else
                    fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);

                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        foreach (var item in fullcollection)
                        {
                            if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                            {
                                if (item.Status == (int)TaskStatus.Pending)
                                    json += "{ \"Username\" : \"" + item.Name + "-" + item.Id + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Task\"},";
                                else if (item.Status == (int)TaskStatus.InProgress)
                                    json += "{ \"Username\" : \"" + item.Name + "-" + item.Id + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"YELLOW\",\"Logs\" : \"Task\"},";
                                else if (item.Status == (int)TaskStatus.Completed)
                                    json += "{ \"Username\" : \"" + item.Name + "-" + item.Id + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"GREEN\",\"Logs\" : \"Task\"},";

                            }
                        }
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getGPSDataTasks", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string getGPSDataOthers(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var cusEvents = new List<CustomEvent>();
            try
            {
                json += "[";
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                            foreach (var manguser in managerusers)
                            {
                                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                            }
                        }
                        else
                        {
                            cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                        }
                    }
                }
                foreach (var item in cusEvents)
                {
                    if (item.EventType == CustomEvent.EventTypes.DriverOffence)
                        json += "{ \"Username\" : \"" + item.Name  + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"YELLOW\",\"Logs\" : \"INCIDENT\"},";
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getGPSDataOthers", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

       

        [WebMethod]
        public static MainList getusershealtcheck(int id, int onlinecount, int offlinecount, int idlecount,string uname)
        {
            MainList mList = new MainList();
            var healthtotal = onlinecount + offlinecount + idlecount;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    mList.onlineuserspercentage = listy;
                    return mList;
                }

                var lp15 = (int)Math.Round((float)onlinecount / (float)healthtotal * (float)100);
                var lp30 = (int)Math.Round((float)offlinecount / (float)healthtotal * (float)100);
                var lp60 = (int)Math.Round((float)idlecount / (float)healthtotal * (float)100);
                if (lp15 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp15.ToString() + "%");

                if (lp30 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp30.ToString() + "%");

                if (lp60 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp60.ToString() + "%");

                mList.onlineuserspercentage = listy;
                mList.recentActivity = getRecentActivity(id, uname, userinfo);
                mList.reminders = getReminders(id, uname, userinfo);
                mList.rowtoTable = getTableData(id, uname, userinfo);
                mList.assignUserTable = getAssignUserTableData(id, uname, userinfo);
                //mList.topOffences = getOffenceChart(id, uname, userinfo);
                //mList.ActivityReport = getActivityReportData(id, uname, userinfo);

            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getusershealtcheck", ex, dbConnection, userinfo.SiteId);
            }
            return mList;
        }
        [WebMethod]
        public static List<string> getTop5Values(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var hotAlarm = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID,userinfo.SiteId ,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)),userinfo.SiteId ,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var site in sites)
                        hotAlarm.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }

                var userList = new List<string>();
                foreach (var hot in hotAlarm)
                {
                    if (hot.EventType == CustomEvent.EventTypes.HotEvent || hot.EventType == CustomEvent.EventTypes.MobileHotEvent || hot.EventType == CustomEvent.EventTypes.Request)
                    {

                        userList.Add(hot.UserName);

                    }
                }
                Dictionary<string, int> counts = userList.GroupBy(x => x)
                                      .ToDictionary(g => g.Key,
                                                    g => g.Count());
                var items = from pair in counts
                            orderby pair.Value descending
                            select pair;
                int count = 0;
                foreach (KeyValuePair<string, int> pair in items)
                {

                    if (count < 5)
                    {
                        listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getTop5Values", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTop5OffenceValues(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var prevDate = CommonUtility.getDTNow().Subtract(new TimeSpan(31, 0, 0, 0));
                var todayDate = CommonUtility.getDTNow();
                listy.Add(prevDate.Year.ToString() + "-" + prevDate.Month.ToString() + "-" + prevDate.Day.ToString());
                listy.Add(todayDate.Year.ToString() + "-" + todayDate.Month.ToString() + "-" + todayDate.Day.ToString());
                listy.Add(prevDate.Year.ToString() + "-" + prevDate.Month.ToString());
                var offences = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    offences = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID,userinfo.SiteId ,dbConnection);
                }
                else
                {
                    offences = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }
                var userList = new List<string>();
                foreach (var off in offences)
                {
                    if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                    {

                        userList.Add(off.UserName);

                    }
                }
                Dictionary<string, int> counts = userList.GroupBy(x => x)
                                      .ToDictionary(g => g.Key,
                                                    g => g.Count());
                var items = from pair in counts
                            orderby pair.Value descending
                            select pair;
                int count = 0;
                foreach (KeyValuePair<string, int> pair in items)
                {
                    if (count < 5)
                    {
                        listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getTop5OffenceValues", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableData(int id, string uname , Users userinfo)
        {
            var listy = new List<string>();
           // var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
//                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
//HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

//                if (isValidSession == null)
//                {
//                    listy.Add("LOGOUT");
//                    return listy;
//                }
                var allcustomEvents = new List<CustomEvent>();
                var vehs = ClientManager.GetAllClientManager(dbConnection);
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));

                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                }
                var imageclass = string.Empty;

                allcustomEvents = allcustomEvents.Where(i => i.EventType == CustomEvent.EventTypes.MobileHotEvent || i.EventType == CustomEvent.EventTypes.Request || i.EventType == CustomEvent.EventTypes.Incident).ToList();

                var grouped = allcustomEvents.GroupBy(item => item.EventId);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                foreach (var item in allcustomEvents)
                {
                    if (item.StatusName != "Resolve")
                    {
                        if (item.EventType != CustomEvent.EventTypes.DriverOffence)
                        {
                            if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                item.Name = item.EventTypeName;
                                //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                //if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                //    item.Name = "None";
                                //else
                                //    item.Name = mobEv.EventTypeName;

                                //imageclass = "circle-point-red";
                            }
                            else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                            {

                                //var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                                //if (reqEv != null)
                                //{
                                //    if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                //        item.UserName = CommonUtility.getPCName(reqEv);
                                //}

                                if (item.EventType == CustomEvent.EventTypes.Request)
                                {
                                    var splitName = item.RequestName.Split('^');
                                    if (splitName.Length > 1)
                                    {
                                        item.Name = splitName[1];
                                    }
                                }
                            }
                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                var dir = Accounts.GetDirectorByManagerName(userinfo.Username, dbConnection);
                                if (dir != null)
                                {
                                    var isGood = false;
                                    if (item.StatusName == "Pending")
                                    {
                                        isGood = true;
                                    }
                                    else
                                    {
                                        if (item.HandledBy == userinfo.Username)
                                        {
                                            isGood = true;
                                        }
                                    }
                                    if (isGood)
                                    {
                                        if (item.StatusName != "Escalated" && item.HandledBy != dir.Username)
                                        {
                                            var getHotName = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                            if (string.IsNullOrEmpty(getHotName.EventTypeName))
                                                getHotName.EventTypeName = "None";

                                            if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                                imageclass = "circle-point-red";
                                            else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                                imageclass = "circle-point-green";
                                            else
                                                imageclass = "circle-point-yellow";

                                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                            listy.Add(returnstring);
                                        }
                                    }
                                }
                                else
                                {
                                    if (item.StatusName != "Escalated")
                                    {
                                        var isGood = false;
                                        if (item.StatusName == "Pending")
                                        {
                                            isGood = true;
                                        }
                                        else
                                        {
                                            if (item.HandledBy == userinfo.Username)
                                            {
                                                isGood = true;
                                            }
                                        }
                                        if (isGood)
                                        {
                                            var getHotName = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                            if (string.IsNullOrEmpty(getHotName.EventTypeName))
                                                getHotName.EventTypeName = "None";

                                            if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                                imageclass = "circle-point-red";
                                            else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                                imageclass = "circle-point-green";
                                            else
                                                imageclass = "circle-point-yellow";

                                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                            listy.Add(returnstring);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (item.StatusName != "Escalated")
                                { 
                                    if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                        imageclass = "circle-point-red";
                                    else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                        imageclass = "circle-point-green";
                                    else
                                        imageclass = "circle-point-yellow";

                                    var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                    listy.Add(returnstring); 
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getTableData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
      
        
        [WebMethod]
        public static string getIncidentLocationData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                json += "[";
                if (id > 0)
                {
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            var item = CustomEvent.GetCustomEventById(id, dbConnection);
                            if (item.EventType == CustomEvent.EventTypes.Incident)
                            {
                                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                                if (geofence.Count > 0)
                                {
                                    foreach (var geo in geofence)
                                    {
                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                    }
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getIncidentLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getGPSDataUsers(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var users = new List<Users>();
               // var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                var vehs = new List<ClientManager>();
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var usr in sessions)
                            {
                                users.Add(usr);
                                var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                                foreach (var subsubuser in getallUsers)
                                {
                                    users.Add(subsubuser);
                                } 
                            }
                            var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                            unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                            foreach (var subsubuser in unassigned)
                            {
                                users.Add(subsubuser);
                            } 
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            users = Users.GetAllUsersBySiteId(userinfo.SiteId,dbConnection); 
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        { 
                            users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                             
                        }
                        else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            users = Users.GetAllUsers(dbConnection);
                            users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.SuperAdmin)
                        {
                            users = Users.GetAllUsers(dbConnection);
                           // devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                        }
                    }
                }
                json += "[";


                var grouped = users.GroupBy(item => item.ID);
                users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in users)
                {
                    if (item.Username != userinfo.Username)
                    {
                        var newLoginDate = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString("d MMM  hh:mm tt");
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            if (item.RoleId == (int)Role.Manager || item.RoleId == (int)Role.Admin)
                            {
                                if (item.IsServerPortal)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " "+item.LastName + "\"},";
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                            }
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"YELLOW\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                        }
                    }
                }
                //foreach (var dev in devices)
                //{
                //    var camString = "";
                //    var camList = ClientCamera.GetAllClientCamerasByMacAddress(dev.MacAddress, dbConnection);
                //    var camCounter = 0;

                //    foreach (var cam in camList)
                //    {
                //        camString += "CameraName" + camCounter + "=" + cam.CameraName + "&";
                //        camCounter++;
                //    }
                //    if (camCounter == 0)
                //        camString = "NO CAM";

                //    if (dev.Status == 1)
                //    {
                //        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                //        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                //        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"BLUE\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";
                //    }
                //    else if (dev.Status == 0)
                //    {
                //        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                //        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                //        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"OFFCLIENT\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";

                //    }
                //    else {
                //         var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                //        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                //        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \""+dev.Status+"\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";

                //    }

                //}
                if (id > 0)
                {
                    var item2 = CustomEvent.GetCustomEventById(id, dbConnection);
                    if (item2 != null)
                    {
                        json += "{ \"Username\" : \"" + item2.Name + "\",\"Id\" : \"" + item2.EventId.ToString() + "\",\"Long\" : \"" + item2.Longtitude.ToString() + "\",\"Lat\" : \"" + item2.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"User\"},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getGPSDataUsers", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string getGPSDataFull(int id, bool locCB, bool eventsCB, bool peopleCB, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var users = new List<Users>();
                var cusEvents = new List<CustomEvent>();
                var locations = new List<Location>();
                var vehs = new List<ClientManager>();
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {

                        if (locCB)
                        {
                            if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            {
                                locations = Location.GetAllLocation(dbConnection);
                                locations.AddRange(Location.GetAllSubLocation(dbConnection));
                            }
                            else if (userinfo.RoleId == (int)Role.Regional)
                            {
                                locations.AddRange(Location.GetAllMainLocationByLevel5(userinfo.ID, dbConnection));
                                locations.AddRange(Location.GetAllSubLocationByLevel5(userinfo.ID, dbConnection));
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                locations = Location.GetAllMainLocationByCustomerId(userinfo.CustomerInfoId, dbConnection);
                                locations.AddRange(Location.GetAllSubLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                            }
                            else
                            {
                                locations = Location.GetAllMainLocationBySiteId(userinfo.SiteId, dbConnection);
                                locations.AddRange(Location.GetAllSubLocationBySiteId(userinfo.SiteId, dbConnection));
                            }

                            var groupedlocations = locations.GroupBy(item => item.ID);
                            locations = groupedlocations.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                        }
                        if (peopleCB)
                        {
                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {
                                var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                                foreach (var usr in sessions)
                                {
                                    users.Add(usr);
                                    var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                                    foreach (var subsubuser in getallUsers)
                                    {
                                        users.Add(subsubuser);
                                    }
                                }
                                var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                                unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                                foreach (var subsubuser in unassigned)
                                {
                                    users.Add(subsubuser);
                                }
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                                users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                            }
                            else if (userinfo.RoleId == (int)Role.Regional)
                            {
                                users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                            }
                            else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                            {
                                users = Users.GetAllUsers(dbConnection);
                                users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                            }
                            else if (userinfo.RoleId == (int)Role.SuperAdmin)
                            {
                                users = Users.GetAllUsers(dbConnection);
                            }
                        }
                        
                        if (eventsCB)
                        {


                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                                var pclist = new List<string>();
                                var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                                var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);

                                foreach (var custom in customeventlist)
                                {
                                    var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                    if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                        cusEvents.Add(custom);
                                }
                                foreach (var custom in reqcustomeventlist)
                                {
                                    var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                    if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                        cusEvents.Add(custom);
                                }
                            }
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {
                                var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));

                                foreach (var manguser in managerusers)
                                {
                                    cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                                    var pclist = new List<string>();
                                    var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                                    var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);

                                    foreach (var custom in customeventlist)
                                    {
                                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                            cusEvents.Add(custom);
                                    }
                                    foreach (var custom in reqcustomeventlist)
                                    {
                                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                            cusEvents.Add(custom);
                                    }
                                }
                            }
                            else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            {
                                cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Regional)
                            {
                                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));

                                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                            } 
                        }
                    }
                }
                json += "[";

                foreach (var loc in locations)
                {
                    var locn = "MAINLOCATION";
                    if (loc.ParentLocationId > 0)
                        locn = "SUBLOCATION";
                    var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyLocationId(dbConnection, loc.ID);
                    if (geofencelocs.Count > 0)
                    {
                        foreach (var geofence in geofencelocs)
                        {
                            if (loc.ParentLocationId > 0)
                                json += "{ \"Username\" : \"" + loc.LocationDesc + "\",\"Id\" : \"" + loc.ID.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"REDL" + loc.ID.ToString() + "\",\"Logs\" : \"#1b93c0\"},";
                            else
                                json += "{ \"Username\" : \"" + loc.LocationDesc + "\",\"Id\" : \"" + loc.ID.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"REDL" + loc.ID.ToString() + "\",\"Logs\" : \"#b2163b\"},";
                            //json += "{ \"Username\" : \"" + loc.LocationDesc + "\",\"Id\" : \"" + loc.ID.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    else
                    {
                        json += "{ \"Username\" : \"" + loc.LocationDesc + "\",\"Uname\" : \"" + loc.LocationDesc + "\",\"Id\" : \"" + loc.ID.ToString() + "\",\"Long\" : \"" + loc.Longitude.ToString() + "\",\"Lat\" : \"" + loc.Latitude.ToString() + "\",\"LastLog\" : \"" + loc.CreatedDate.Value.ToShortDateString() + "\",\"State\" : \"" + locn + "\",\"Logs\" : \"Places\",\"DName\" : \"" + loc.LocationDesc + "\"},";

                    }
                }

                var grouped = users.GroupBy(item => item.ID);
                users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();
                
                foreach (var item in users)
                {
                    if (item.Username != userinfo.Username)
                    {
                        var newLoginDate = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString("d MMM  hh:mm tt");
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            if (item.RoleId == (int)Role.Manager || item.RoleId == (int)Role.Admin)
                            {
                                if (item.IsServerPortal)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                            }
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                        }
                    }
                }

                if (cusEvents != null)
                {
                    if (cusEvents.Count > 0)
                    {
                        var isValid = false;
                        var isMangUser = false;

                        if (userinfo.RoleId == (int)Role.Manager)
                            isMangUser = true;
                        foreach (var item in cusEvents)
                        {
                            if (isMangUser)
                            {
                                if (item.HandledBy == "LiveMIMSAlarmService" || item.HandledBy == userinfo.Username)
                                    isValid = true;
                                else
                                    isValid = false;
                            }
                            else
                                isValid = true;

                            if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve || item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                                isValid = false;

                            if (isValid)
                            {
                                if (item.EventType != CustomEvent.EventTypes.DriverOffence)
                                {
                                    if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                    {
                                        item.Name = item.EventTypeName;
                                    }
                                    else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                                    { 
                                        if (item.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var splitName = item.RequestName.Split('^');
                                            if (splitName.Length > 1)
                                            {
                                                item.Name = splitName[1];
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(item.Longtitude) && !string.IsNullOrEmpty(item.Latitude))
                                    {
                                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                        {
                                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"PURPLE\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + item.Name + "-" + item.CustomerIncidentId + "\"},";
                                        }
                                        else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete || item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                                        {
                                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"GREEN\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + item.Name + "-" + item.CustomerIncidentId + "\"},";
                                        }
                                        else
                                        {
                                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"YELLOW\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + item.Name + "-" + item.CustomerIncidentId + "\"},";
                                        }
                                    }
                                    else
                                    {
                                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                        {
                                            var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, item.EventId);
                                            foreach (var geofence in geofencelocs)
                                            {
                                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED" + item.EventId.ToString() + "\",\"Logs\" : \"#5e0021\"},";
                                            }
                                        }
                                        else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                        {
                                            var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, item.EventId);
                                            foreach (var geofence in geofencelocs)
                                            {
                                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED" + item.EventId.ToString() + "\",\"Logs\" : \"#17ff33\"},";
                                            }
                                        }
                                        else
                                        {                       
                                            var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, item.EventId);
                                            foreach (var geofence in geofencelocs)
                                            {
                                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED" + item.EventId.ToString() + "\",\"Logs\" : \"#ff791c\"},";
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getGPSDataUsers", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static List<string> getAssignUserTableData(int id,string uname , Users userinfo)
        {
            var listy = new List<string>();
           // var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

//                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
//HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

//                if (isValidSession == null)
//                {
//                    listy.Add("LOGOUT");
//                    return listy;
//                }

                var allusers = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        allusers.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            allusers.Add(subsubuser);
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    } 
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    //{
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                    //}
                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection); 
                }

                var grouped = allusers.GroupBy(item => item.ID);
                allusers = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();
                foreach (var item in allusers)
                {
                    //if (item.DeviceType == (int)Users.DeviceTypes.Mobile || item.DeviceType == (int)Users.DeviceTypes.MobileAndClient || item.RoleId == (int)Role.Manager)
                    //{
                        if (item.Username != userinfo.Username)
                        {
                            var returnstring = "<tr role='row' class='odd'><td>" + item.CustomerUName + "</td><td class='sorting_1'>" + item.Status + "</td><td><a href='#' class='red-color' id=" + item.ID + item.Username + " onclick='dispatchUserchoiceTable(&apos;" + item.ID + "&apos;,&apos;" + item.Username + "&apos;,&apos;" + item.CustomerUName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                            listy.Add(returnstring);
                        }
                    //}
                }
                foreach (var dev in devices)
                {
                    if (dev.Status == 1)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Online</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='dispatchUserchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                    else
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Offline</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='dispatchUserchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getAssignUserTableData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            try
            {
                var customEventData = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);

                if (customEventData != null)
                {


                    if (customEventData.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(customEventData.Identifier, dbConnection);
                        
                        customEventData.Name = customEventData.EventTypeName;

                        //if (string.IsNullOrEmpty(mobEv.EventTypeName))
                        //    customEventData.Name = "None";
                        //else
                        //    customEventData.Name = mobEv.EventTypeName;

                        customEventData.Description = customEventData.MobDescription;
                    }
                    else if (customEventData.EventType == CustomEvent.EventTypes.Request)
                    {
                        //var reqEv = HotEvent.GetHotEventById(customEventData.Identifier, dbConnection);


                        var splitName = customEventData.RequestName.Split('^');
                        if (splitName.Length > 1)
                        {
                            customEventData.Description = "Request " + splitName[1];
                            customEventData.Name = splitName[1];
                        }
                        //if (reqEv != null)
                        //{
                        //    if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                        //        customEventData.UserName = CommonUtility.getPCName(reqEv);
                        //}
                    }
                    else if (customEventData.EventType == CustomEvent.EventTypes.HotEvent)
                    {
                        var reqEv = HotEvent.GetHotEventById(customEventData.Identifier, dbConnection);
                        if (reqEv != null)
                            customEventData.UserName = CommonUtility.getPCName(reqEv);
                    }
                    if (!string.IsNullOrEmpty(customEventData.UserName))
                    {
                        var getUserInfo = Users.GetUserByName(customEventData.UserName, dbConnection);
                        if (getUserInfo != null)
                        {
                            if (string.IsNullOrEmpty(customEventData.EmailAddress))
                                customEventData.EmailAddress = getUserInfo.Email;
                            if (string.IsNullOrEmpty(customEventData.PhoneNumber))
                                customEventData.PhoneNumber = getUserInfo.Telephone;
                        }
                    }


                    listy.Add(customEventData.CustomerUName);
                    listy.Add(customEventData.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customEventData.Event);
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        var dir = Accounts.GetDirectorByManagerName(userinfo.Username, dbConnection);
                        if (dir != null)
                        {
                            if (dir.Username == customEventData.HandledBy && dir.Username != customEventData.UserName)
                            {
                                if (customEventData.EventType == CustomEvent.EventTypes.Incident)
                                    listy.Add(customEventData.StatusName + "-Resolve");
                                else
                                {

                                    if (userinfo.Username != customEventData.UserName)
                                        listy.Add(customEventData.StatusName + "-Hold");
                                    else
                                        listy.Add(customEventData.StatusName + "-MyIncident");
                                }

                            }
                            else if (dir.Username == customEventData.UserName)
                            {
                                if (userinfo.Username == customEventData.HandledBy)
                                {
                                    listy.Add(customEventData.StatusName);
                                }
                                else
                                {
                                    if (dir.Username == customEventData.HandledBy)
                                    {
                                        listy.Add(customEventData.StatusName + "-Hold");
                                    }
                                    else
                                        listy.Add(customEventData.StatusName + "-MyIncident");
                                }
                            }
                            else
                                listy.Add(customEventData.StatusName);
                        }
                        else
                            listy.Add(customEventData.StatusName);
                    }
                    else
                        listy.Add(customEventData.StatusName);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customEventData.Latitude.ToString(), customEventData.Longtitude.ToString(), getClientLic);
                    if (!string.IsNullOrEmpty(geolocation))
                        listy.Add(geolocation);
                    else
                    {
                        var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, customEventData.EventId);
                        if (geofence.Count > 0)
                        {
                            geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        }
                        listy.Add(geolocation);
                    }
                    if (!string.IsNullOrEmpty(customEventData.ReceivedBy))
                    {
                        listy.Add(customEventData.ReceivedBy);
                    }
                    else
                    {
                        listy.Add(customEventData.CustomerUName);
                    }
                    listy.Add(customEventData.PhoneNumber);
                    listy.Add(customEventData.EmailAddress);
                    listy.Add(customEventData.Description);
                    if (!string.IsNullOrEmpty(customEventData.Instructions))
                    {
                        var splitIns = customEventData.Instructions.Split('|');
                        if (splitIns.Length > 0)
                            customEventData.Instructions = splitIns[0];
                    }
                    listy.Add(customEventData.Instructions);
                    listy.Add(customEventData.Name + "-" + customEventData.CustomerIncidentId);
                    listy.Add("circle-point " + CommonUtility.getImgIncidentPriority(customEventData.Event));
                    listy.Add(customEventData.Longtitude.ToString());
                    listy.Add(customEventData.Latitude.ToString());
                    var gettasks = UserTask.GetAllTaskByIncidentId(id, dbConnection);
                    var foundtask = false;
                    if (gettasks.Count > 0)
                    {
                        foreach (var task in gettasks)
                        {
                            if (task.Status != (int)TaskStatus.Cancelled)
                            {
                                foundtask = true;
                                break;
                            }
                        }
                    }
                    if (foundtask)
                        listy.Add("TASK");

                    var foundEsca = false;
                    var evsHistory = EventHistory.GetEventHistoryByEventId(id, dbConnection);
                    if (evsHistory.Count > 0)
                    {
                        if (customEventData.StatusName == "Escalated")//customEventData.Escalate == (int)CustomEvent.EscalateTypes.Escalate)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                        else if (customEventData.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                        else if (customEventData.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (!foundEsca)
                        listy.Add("N/A");

                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        if (customEventData.StatusName == "Escalated")//customEventData.Escalate == (int)CustomEvent.EscalateTypes.Escalate)
                        {
                            listy.Add("none");
                        }
                        else
                            listy.Add("block");
                    }
                    else
                    {
                        listy.Add("block");
                    }
                    listy.Add(customEventData.Notes);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Default", "getTableRowData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getEventHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var allEventHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                foreach (var rem in allEventHistory)
                {
                    var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                    if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        gethotevent.Name = gethotevent.EventTypeName;

                        //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                        //if (mobEv != null)
                        //{
                        //    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                        //        gethotevent.Name = "None";
                        //    else
                        //        gethotevent.Name = mobEv.EventTypeName;
                        //}
                    }
                    else if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                    {
                        //var reqEv = HotEvent.GetHotEventById(gethotevent.Identifier, dbConnection);
                        var splitName = gethotevent.RequestName.Split('^');
                        if (splitName.Length > 1)
                        {
                            gethotevent.Name = splitName[1];
                        }
                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerUName;
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "engaged ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";

                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);


                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getEventHistoryData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        static List<string> VideoExtensions = new List<string> { ".AVI", ".MP4", ".MKV", ".FLV" };
        [WebMethod]
        public static string uploadMobileAttachment(int id, string imgpath, string uname)
        {
            var retid = false;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (!string.IsNullOrEmpty(imgpath))
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var newimgPath = imgpath.Replace('|', '\\');
                    //var bmap = new System.Drawing.Bitmap(newimgPath);
                    var newmobattach = new MobileHotEventAttachment();
                    //var mimsconfigsettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    //var savestring = mimsconfigsettings.FilePath.Remove(mimsconfigsettings.FilePath.Length - 5);
                    //savestring = savestring + "HotEvents\\" + Guid.NewGuid();
                    //if (!System.IO.Directory.Exists(savestring))
                    //    System.IO.Directory.CreateDirectory(savestring);
                    //savestring = savestring + "\\" + Guid.NewGuid() + ".jpg";
                    //newmobattach.Attachment = CommonUtility.ImageToByte2(bmap);

                    //System.IO.MemoryStream data = new System.IO.MemoryStream();
                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);

                            newmobattach.AttachmentPath = savestring;//string.Empty;//savestring;
                            newmobattach.CreatedBy = userinfo.Username;
                            newmobattach.CreatedDate = CommonUtility.getDTNow();
                            newmobattach.EventId = id;
                            newmobattach.SiteId = userinfo.SiteId;
                            newmobattach.CustomerId = userinfo.CustomerInfoId;
                            newmobattach.UpdatedBy = userinfo.Username;
                            newmobattach.UpdatedDate = CommonUtility.getDTNow();
                            newmobattach.IsPortal = true;
                            retid = MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newmobattach, dbConnection);
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Default", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Default", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Default", "uploadMobileAttachment", ex, dbConnection, userinfo.SiteId);
                }
            }
            return retid.ToString();
        }
        [WebMethod]
        public static string getAttachmentDataIcons(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);
                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;

                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {

                                        var imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {
                                        imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + 0 + ")' data-target='#video-" + i + "-tab'/>";
                            listy += retstring;
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {
                                        imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {
                                        imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getAttachmentDataIcons", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#location-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Incident Location</p></div></div></div>";
 
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);

                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {
                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");

                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" +  requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                //iTab++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {
                                var imgstring = String.Format(requiredString);

                                var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;
                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" +  requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                   // iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {
                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                    //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + 0 + ")' data-target='#video-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Playback</p></div></div></div>";
                            listy += retstring;
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" +  requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                 //   iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstring = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border onclick='hideIncidentplay()' clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                 //   iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()' ><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "getAttachmentDataTab", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection); 
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);
                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {

                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" + requiredString + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                            {
                                //var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source src='" + requiredString + "' type='audio/mpeg' /></audio>";
                                //listy.Add(retstring);
                                //i++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {

                                var imgstring = requiredString;
                                var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }

                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" +  requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    //var retstring = "<audio id='Video" + i + "' width='100%' height='380px'  controls ><source type='audio/mpeg' src='" +  requiredString + "' /></audio>";
                                    //listy.Add(retstring);
                                    //i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring =  requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" + hotEv.playbackURL + "' /></video>";
                            listy.Add(retstring);
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" +  requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    //var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source type='audio/mpeg' src='" +  requiredString + "' /></audio>";
                                    //listy.Add(retstring);
                                    //i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring =  requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" +  requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    //var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source type='audio/mpeg' src='" +  requiredString + "' /></audio>";
                                    //listy.Add(retstring);
                                    //i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring =  requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getProtoAttachmentData(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);
                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {

                            var requiredString = string.Empty;
                            var imgstring = string.Empty;
                            if (item.IsVideo)
                            {
                                requiredString = item.AttachmentPath;

                                imgstring = requiredString;
                            }
                            else
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                //requiredString = item.AttachmentPath.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                imgstring = item.AttachmentPath;//mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            }
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                var retstring = imgstring;
                                listy = retstring;
                                i++;
                            }
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                //var requiredString = item.AttachmentPath.Substring(index, (item.AttachmentPath.Length - index));
                                var requiredString = item.AttachmentPath;//requiredString.Replace("\\", "/");
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    //var retstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                                    listy = requiredString;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "getProtoAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getIsLive(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                listy = "FAIL";
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);
                    if (mobEv != null)
                    {
                        if (mobEv.WebPort > 0)
                        {
                            listy = "SUCCESS";
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "getIsLive", er, dbConnection, userinfo.SiteId);
                listy = "ERROR";
            }
            return listy;
        }


        [WebMethod]
        public static string getLocationData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var item = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                var tracebackhistory = TraceBackHistory.GetTracBackHistoryByIncidentId(item.EventId, dbConnection);
                json += "[";
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (item != null)
                        {
                            if (!string.IsNullOrEmpty(item.Longtitude) && !string.IsNullOrEmpty(item.Longtitude))
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else
                            {
                                var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, item.EventId);
                                foreach (var geofence in geofencelocs)
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"#800000\"},";
                                }
                            }
                            foreach (var tb in tracebackhistory)
                            {

                            }
                        }
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string isUser(string id, string uname)
        {
            var result = "false";
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);


            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            var getuser = Users.GetUserByName(id, dbConnection);
            if (getuser != null)
                result = "true";

            return result;
        }

        [WebMethod]
        public static string getAssigneeType(string id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            if (CommonUtility.isNumeric(id))
                return "User";
            else
                return "Device";
        }

        [WebMethod]
        public static string changeIncidentState(string id, string state, string ins, string uname)
        {
            int taskId1 = 0;
            var retVal = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retValve = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retValve)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (!string.IsNullOrEmpty(id))
                    {
                        var oldIInfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        incidentinfo.UpdatedBy = userinfo.Username;

                        if (state != "Reject" && state != "Resolve")
                            incidentinfo.Instructions = ins;

                        incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue(state.ToLower());
                        incidentinfo.Handled = false;
                        //incidentinfo.SiteId = userinfo.SiteId;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        var returnV = CommonUtility.CreateIncident(incidentinfo);
                        if (returnV != "")
                        {
                            taskId1 = Convert.ToInt32(returnV);
                            CustomEvent.CustomEventHandledById(taskId1, false, userinfo.Username, dbConnection);
                            //taskId1 = CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection);
                            retVal = incidentinfo.Name;

                            if (incidentinfo.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                var getMobName = MobileHotEvent.GetMobileHotEventByGuid(incidentinfo.Identifier, dbConnection);
                                {
                                    if (string.IsNullOrEmpty(getMobName.EventTypeName))
                                        retVal = "None";
                                    else
                                        retVal = getMobName.EventTypeName;

                                }
                            }

                            if (taskId1 < 1)
                            {
                                taskId1 = Convert.ToInt32(id);

                            }

                            if (incidentinfo.IncidentStatus != (int)CustomEvent.IncidentActionStatus.Dispatch)
                            {
                                if (oldIInfo.IncidentStatus != incidentinfo.IncidentStatus)
                                {
                                    var EventHistoryEntry = new EventHistory();
                                    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                                    EventHistoryEntry.CreatedBy = userinfo.Username;
                                    EventHistoryEntry.EventId = taskId1;
                                    EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                                    EventHistoryEntry.UserName = userinfo.Username;
                                    EventHistoryEntry.Remarks = ins;
                                    EventHistoryEntry.SiteId = userinfo.SiteId;
                                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                                }
                            }
                            else
                            {

                                //Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                                var notifications = Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                if (notifications.Count > 0)
                                {
                                    var firstNotification = notifications.FirstOrDefault();
                                    if (firstNotification.AssigneeType == (int)Notification.NotificationAssigneeType.User)
                                    {
                                        var userNotification = notifications.Where(x => x.AssigneeId == userinfo.ID).FirstOrDefault();
                                        if (userNotification != null)
                                        {
                                            Arrowlabs.Business.Layer.Notification.UpdateNotificationStatus(userNotification.Id, true, dbConnection);
                                            //TraceBackHistory.DeleteTraceBackHistoryByIncidentId(userNotification.Id, dbConnection);
                                        }
                                    }
                                }

                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    task.Status = (int)TaskStatus.Cancelled;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }
                            }
                            if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                            {
                                incidentinfo.Handled = true;
                                incidentinfo.HandledTime = CommonUtility.getDTNow();
                                incidentinfo.HandledBy = userinfo.Username;
                                incidentinfo.SiteId = userinfo.SiteId;
                                taskId1 = CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection, dbConnectionAudit, true);
                                if (taskId1 < 1)
                                {
                                    taskId1 = Convert.ToInt32(id);
                                    incidentinfo.EventId = Convert.ToInt32(id);
                                }
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    var tskhistory = new TaskEventHistory();
                                    if (task.Status == (int)TaskStatus.Completed)
                                    {
                                        task.Status = (int)TaskStatus.Accepted;
                                        tskhistory.Action = (int)TaskAction.Accepted;
                                        tskhistory.Remarks = "Incident connected to task has been resolved";
                                    }
                                    else
                                    {
                                        task.Status = (int)TaskStatus.Cancelled;
                                        tskhistory.Action = (int)TaskAction.Rejected;
                                        tskhistory.Remarks = "Incident connected to task has been resolved prior to completion";
                                    }
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }
                                CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection);
                                CustomEvent.CustomEventHandledById(incidentinfo.EventId, true, userinfo.Username, dbConnection);
                            }
                            else if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Release)
                            {
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {

                                    var tskhistory = new TaskEventHistory();

                                    task.Status = (int)TaskStatus.Cancelled;
                                    tskhistory.Action = (int)TaskAction.Rejected;
                                    tskhistory.Remarks = "Incident connected to task has been released prior to completion";
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);

                                }
                            }
                            else if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                            {
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(incidentinfo.EventId, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, CommonUtility.dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(incidentinfo.EventId, true, dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(incidentinfo.EventId, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    var tskhistory = new TaskEventHistory();
                                    task.Status = (int)TaskStatus.Rejected;
                                    tskhistory.Action = (int)TaskAction.Rejected;
                                    tskhistory.Remarks = "Incident connected to task has been rejected prior to completion";
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }
                            }
                        }
                    }

                }
                catch (Exception er)
                {
                    MIMSLog.MIMSLogSave("Incident", "changeIncidentState", er, dbConnection, userinfo.SiteId);
                }
                return retVal;
            }
        }
        [WebMethod]
        public static string sendpushMultipleNotification(string[] userIds, string msg,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                else
                {
                    Thread thread = new Thread(() => CommonUtility.sendPushNotificationAlarm(userIds, msg, dbConnection));
                    thread.Start();
                    //Thread thread = new Thread(() => PushNotificationClient.SendtoAppleMultiUserNotification("Alarm: " + msg, userIds, dbConnection));
                    //thread.Start();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "sendpushMultipleNotification", ex, dbConnection, userinfo.SiteId);
                return "FAIL";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string getCurrentLocation(string  lat, string lng,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var retVal = string.Empty;
            try
            {
                retVal = ReverseGeocode.RetrieveFormatedAddress(lat, lng, getClientLic);
                var split = retVal.Split('-');
                retVal = split[split.Length-1];
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getCurrentLocation", ex, dbConnection, userinfo.SiteId);
            }
            return retVal;
        }
        
        [WebMethod]
        public static string insertNewReminder(string name, string date, string reminder,int colorcode,string todate,string fromdate,string uname)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {

                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

              
                try
                {
                    var newReminder = new Reminders();
                    newReminder.ReminderTopic = name;
                    newReminder.ReminderNote = reminder;
                    newReminder.ToDate = todate;
                    newReminder.FromDate = fromdate;
                    newReminder.ColorCode = colorcode;
                    newReminder.ReminderDate = Convert.ToDateTime(date);
                    newReminder.CreatedBy = userinfo.Username;
                    newReminder.CreatedDate = CommonUtility.getDTNow();
                    newReminder.UpdatedBy = userinfo.Username;
                    newReminder.UpdatedDate = CommonUtility.getDTNow();
                    newReminder.SiteId = userinfo.SiteId;
                    newReminder.CustomerId = userinfo.CustomerInfoId;
                    Reminders.InsertorUpdateReminder(newReminder, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Default.aspx", "insertNewReminder", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
            
        }
        [WebMethod]
        public static string inserttask(string id, string assigneetype, string assigneename, string assigneeid, string templatename, string longi, string lati, int incidentId, string uname)
        {
            int taskId1 = 0;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var dtnow = CommonUtility.getDTNow();
                if (Convert.ToBoolean(id))
                {
                    if (Convert.ToInt32(templatename) > 0)
                    {
                        var temptasks = UserTask.GetTaskById(Convert.ToInt32(templatename), dbConnection);
                        var newTasks = new UserTask();
                        newTasks.IsSignature = temptasks.IsSignature;
                        newTasks.Name = temptasks.Name;
                        newTasks.Description = temptasks.Description;
                        newTasks.IncidentId = Convert.ToInt32(incidentId);
                        if (assigneetype == TaskAssigneeType.Location.ToString())
                        {
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);
                            newTasks.AssigneeType = (int)TaskAssigneeType.Location;
                        }
                        else if (assigneetype == TaskAssigneeType.Group.ToString())
                        {
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);
                            newTasks.AssigneeType = (int)TaskAssigneeType.Group;
                        }
                        else if (assigneetype == TaskAssigneeType.User.ToString())
                        {
                            newTasks.AssigneeType = (int)TaskAssigneeType.User;
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);

                        }
                        else if (assigneetype == TaskAssigneeType.Device.ToString())
                        {
                            newTasks.AssigneeType = (int)TaskAssigneeType.Device;
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);

                        }
                        newTasks.CreateDate = dtnow;
                        newTasks.CreatedBy = userinfo.Username;
                        newTasks.ManagerName = userinfo.Username;
                        newTasks.ManagerId = userinfo.ID;
                        newTasks.TaskTypeId = temptasks.TaskTypeId;
                        newTasks.IsDeleted = false;
                        newTasks.UpdatedDate = dtnow;

                        if (!string.IsNullOrEmpty(longi))
                            newTasks.Longitude = Convert.ToDouble(longi);
                        else
                            newTasks.Longitude = 0;

                        if (!string.IsNullOrEmpty(lati))
                            newTasks.Latitude = Convert.ToDouble(lati);
                        else
                            newTasks.Latitude = 0;

                        newTasks.IsSignature = temptasks.IsSignature;



                        var selectedTaskStartDateTime = CommonUtility.getDTNow();
                        newTasks.StartDate = selectedTaskStartDateTime;
                        newTasks.EndDate = selectedTaskStartDateTime;

                        newTasks.Priority = temptasks.Priority;
                        newTasks.TemplateCheckListId = temptasks.TemplateCheckListId;
                        newTasks.SiteId = userinfo.SiteId;
                        newTasks.CustomerId = userinfo.CustomerInfoId;
                        taskId1 = UserTask.InsertorUpdateTask(newTasks, dbConnection, dbConnectionAudit, true);
                        newTasks.Id = taskId1;

                        if (newTasks.IsSignature)
                            UserTask.UpdateTaskSignature(newTasks.Id, true, dbConnection);

                        var tskhistory = new TaskEventHistory();
                        tskhistory.Action = (int)TaskAction.Pending;
                        tskhistory.CreatedBy = userinfo.Username;
                        tskhistory.CreatedDate = CommonUtility.getDTNow();
                        tskhistory.TaskId = taskId1;
                        tskhistory.SiteId = userinfo.SiteId;
                        tskhistory.CustomerId = userinfo.CustomerInfoId;
                        TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);

                        var allTemplateCheckListItems = TemplateCheckListItem.GetAllTemplateCheckListItems(dbConnection);
                        if (allTemplateCheckListItems != null && allTemplateCheckListItems.Count > 0)
                        {
                            var selectedTemplateCheckListItems = allTemplateCheckListItems.Where(i => i.ParentCheckListId == newTasks.TemplateCheckListId).ToList();
                            if (selectedTemplateCheckListItems.Count > 0)
                            {
                                foreach (var templateCheckListItem in selectedTemplateCheckListItems)
                                {
                                    var taskCheckList = new TaskCheckList();
                                    taskCheckList.IsChecked = templateCheckListItem.IsChecked;
                                    taskCheckList.TaskId = taskId1;
                                    taskCheckList.UpdatedDate = dtnow;
                                    taskCheckList.CreatedDate = dtnow;
                                    taskCheckList.CreatedBy = Environment.UserName;
                                    taskCheckList.TemplateCheckListItemId = templateCheckListItem.Id;
                                    taskCheckList.SiteId = userinfo.SiteId;
                                    taskCheckList.CustomerId = userinfo.CustomerInfoId;
                                    TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckList, dbConnection, dbConnectionAudit, true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "inserttask", er, dbConnection, userinfo.SiteId);
            }
            return taskId1.ToString();
        }
        [WebMethod]
        public static string insertNewIncident(string name, string desc, string locationid, string incidenttype, string notificationid, string taskid, string receivedby, string longi, string lati, string status, string instructions, string msgtask, string incidentId,string uname)
        {
            int taskId1 = 0;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    CustomEvent newincident = null;
                    var dtnow = CommonUtility.getDTNow();

                    if (!string.IsNullOrEmpty(incidentId))
                        if (Convert.ToInt32(incidentId) > 0)
                            newincident = CustomEvent.GetCustomEventById(Convert.ToInt32(incidentId), dbConnection);
                        else
                        {
                            newincident = new CustomEvent();
                            newincident.Identifier = Guid.NewGuid();
                            newincident.CreatedBy = userinfo.Username;
                        }

                    if (newincident != null)
                    {
                        newincident.UserName = userinfo.Username;

                        newincident.RecevieTime = dtnow;
                        newincident.EventType = CustomEvent.EventTypes.Incident;
                        newincident.Name = name;
                        newincident.Description = desc;
                        newincident.LocationId = Convert.ToInt32(locationid);
                        newincident.IncidentType = Convert.ToInt32(incidenttype);
                        newincident.ReceivedBy = receivedby;

                        newincident.CreatedDate = dtnow;
                        newincident.Longtitude = longi;
                        newincident.Latitude = lati;
                        newincident.Instructions = instructions;

                        if (Convert.ToBoolean(msgtask))
                            newincident.TemplateTaskId = Convert.ToInt32(taskid);
                        else
                            newincident.TemplateTaskId = 0;

                        newincident.IncidentStatus = (int)CustomEvent.IncidentActionStatus.Dispatch;
                        newincident.SiteId = userinfo.SiteId;
                        taskId1 = CustomEvent.InsertorUpdateIncident(newincident, dbConnection, dbConnectionAudit, true);



                        if (taskId1 < 1)
                        {
                            taskId1 = Convert.ToInt32(incidentId);
                        }
                        else
                        {
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = dtnow;
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = taskId1;
                            EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                            EventHistoryEntry.UserName = receivedby;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                        }

                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Default.aspx", "insertNewIncident", ex, dbConnection, userinfo.SiteId);
                }
            }
            return taskId1.ToString();
        }
        //User Profile
        [WebMethod]
        public static int changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                return 0;
            }
            else
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return 0;
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id;
                }
                else
                    return 0;
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return 0;
            }

            if(id > 1)
            {
                var getuser = Users.GetUserById(id, dbConnection);
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection,dbConnectionAudit,true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection,dbConnectionAudit,true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection,dbConnectionAudit,true))
                {
                    return id;
                }
                else
                    return 0;
            }
            else if (userinfo.ID > 0)
            {
                var getuser = userinfo;
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName   ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                         
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection,dbConnectionAudit,true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection,dbConnectionAudit,true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection,dbConnectionAudit,true))
                {
                    return 1;
                }
                else
                    return 0;
            }
            return 0;
        }
        private static string AddGroupToUserProfileList(Group item)
        {
            var members = Users.GetAllUserByGroupId(item.Id, dbConnection);
            var colorcode = string.Empty;
            if (item.ColorCode == (int)Reminders.ColorCodes.Green)
                colorcode = "green-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Blue)
                colorcode = "blue-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Yellow)
                colorcode = "yellow-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Red)
                colorcode = "red-column";

            var retstring = "<div class='inline-block mr-2x inherited-vertical-align'><span class='" + colorcode + " column-diemention extra-height'></span></div><div class='inline-block mr-2x inherited-vertical-align'><h4 class='no-margin-top blue-color'>" + item.Name + "</h4><h5 class='mb-4x' id='" + item.Name + "-" + item.Id + "'>MEMBERS(" + members.Count + ")</h5></div>";

            return retstring;
        }
        [WebMethod]
        public static List<string> getUserProfileData(int id)
        {
            var listy = new List<string>();
            // var test = Users.GetUserById(id, dbConnection);
            var customData = Users.GetUserById(id, dbConnection);
            var supervisorId = 0;
            if (customData != null)
            {
                listy.Add(customData.Username);
                listy.Add(customData.FirstName + " " + customData.LastName);
                listy.Add(customData.Telephone);
                listy.Add(customData.Email);
                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                listy.Add(geoLoc);
                listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                if (customData.RoleId == (int)Role.Operator)
                {
                    var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                    if (usermanager != null)
                    {
                        listy.Add(usermanager.CustomerUName);
                        supervisorId = usermanager.ID;
                    }
                    else
                        listy.Add("Unassigned");
                }
                else if (customData.RoleId == (int)Role.Manager)
                {
                    var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                    if (getdir != null)
                    {
                        listy.Add(getdir.CustomerUName);
                        supervisorId = getdir.ID;
                    }
                    else
                        listy.Add("Unassigned");
                }
                else if (customData.RoleId == (int)Role.UnassignedOperator)
                {
                    listy.Add("Unassigned");
                }
                else
                {
                    listy.Add(" ");
                }
                listy.Add("Group Name");
                listy.Add(customData.Status);
                listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);//"Images/icon-user-default.png";//images / custom - images / user - 1.png;
                //if (userImg != null)
                //{
                //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                //}
                var fontstyle = string.Empty;
                if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                {
                    fontstyle = "style='color:lime;'";
                }
                //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                var monitor = string.Empty;
                //if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                if (customData.RoleId != (int)Role.SuperAdmin)
                {
                    // if (pushDev != null)
                    //  {
                    //if (!string.IsNullOrEmpty(pushDev.Username))
                    //{
                    if (customData.IsServerPortal)
                    {
                        monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        fontstyle = "";
                    }
                    else
                    {
                        monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                    }
                    // }
                    //  else
                    // {
                    //     monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                    //    fontstyle = "";
                    // }
                    //}
                }
                listy.Add(imgSrc);
                listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle,monitor));
                listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                listy.Add(customData.FirstName);
                listy.Add(customData.LastName);
                listy.Add(supervisorId.ToString());
                listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                listy.Add(customData.Latitude.ToString());
                listy.Add(customData.Longitude.ToString());
                var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                if (customData.RoleId != (int)Role.Regional)
                {
                    if (customData.SiteId == 0)
                        listy.Add("N/A");
                    else
                        listy.Add(userSiteDisplay);
                }
                else
                {
                    listy.Add("Multiple");
                }

                

                listy.Add(customData.Gender);
                listy.Add(customData.EmployeeID);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getUserRecentActivity(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }
            try
            {
                var entryCount = 0;
                var userDetails = Users.GetUserById(id, dbConnection);
                var loginhistory = LoginSession.GetLoginHistoryByMacAddress(userDetails.Username, dbConnection);
                var getrecenthistory = EventHistory.GetEventHistoryByUser(userDetails.Username, dbConnection);
                var getMyTasks = UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.Completed, dbConnection);
                getMyTasks.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.InProgress, dbConnection));
                getMyTasks.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.Pending, dbConnection));
                getMyTasks.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.Accepted, dbConnection));

                List<UserTask> SortedList = getMyTasks.OrderByDescending(o => o.CreateDate).ToList();

                var getVerifiers = Verifier.GetAllVerifierByCreatedBy(userDetails.Username, dbConnection);

                List<Verifier> vSortedList = getVerifiers.OrderByDescending(o => o.CreatedDate).ToList();

                var ehistorylist = CommonUtility.getUserHistory(getrecenthistory, SortedList, vSortedList, loginhistory, getClientLic);

                foreach (var ehistory in ehistorylist)
                {
                    if (ehistory.username.ToLower() == userDetails.CustomerUName.ToLower())
                    {
                        ehistory.username = ehistory.username.ToLower();
                        if (entryCount == 10)
                            break;
                        if (ehistory.type == "Cus")
                        {
                            var jsonstring = "<div class='col-md-2'><p>" + ehistory.date.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + ehistory.id + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + ehistory.username + "</span></br>" + ehistory.actioninfo + "<span>" + ehistory.name + "</span>" + ehistory.incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (ehistory.type == "Tsk")
                        {
                            var jsonstring = "<div class='col-md-2'><p>" + ehistory.date.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + ehistory.id + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + ehistory.username + "</span></br>" + ehistory.actioninfo + "<span>" + ehistory.name + "</span>" + ehistory.incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (ehistory.type == "Ver")
                        {
                            var jsonstring = "<div class='col-md-2'><p>" + ehistory.date.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2' data-target='#verificationDocument'  data-toggle='modal' onclick='assignVerifierId(&apos;" + ehistory.id + "&apos;) '><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + ehistory.username + "</span></br>" + ehistory.actioninfo + "<span>" + ehistory.name + "</span>" + ehistory.incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (ehistory.type == "Log")
                        {
                            var jsonstring = "<div class='col-md-2'><p>" + ehistory.date.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + ehistory.username + "</span></br>" + ehistory.actioninfo + "<span>" + ehistory.name + "</span>" + ehistory.incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        } 
                    }
                }
            }
            catch (Exception EX)
            {
                var logid = MIMSLog.MIMSLogSave("Default", "getUserRecentActivity", EX, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static List<string> getGroupDataFromUser(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var usergroups = GroupUser.GetGroupsByUserId(id, dbConnection);
                    foreach (var group in usergroups)
                    {
                        var groupInfo = Group.GetGroupById(group.GroupId, dbConnection);
                        if (groupInfo != null)
                        {
                            listy.Add(AddGroupToUserProfileList(groupInfo));
                            listy.Add(groupInfo.Id.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getGroupDataFromUser", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getReminderSelectorDates(int id,string uname)
        {
            var listy = new List<string>();

            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            // Set the start time (00:00 means 12:00 AM)
            DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(7, 0, 0));
            // Set the end time (23:55 means 11:55 PM)
            DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(17, 0, 0));
            //Set 5 minutes interval
            TimeSpan Interval = new TimeSpan(1, 0, 0);
            //To set 1 hour interval
            //TimeSpan Interval = new TimeSpan(1, 0, 0);           

            while (StartTime <= EndTime)
            {
                listy.Add(StartTime.ToString("HH:mm"));
                StartTime = StartTime.Add(Interval);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getDateRangeReminder(string date,string uname)
        {
            var listy = new List<string>();

            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var split = date.Split(':');
            // Set the start time (00:00 means 12:00 AM)
            DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(Convert.ToInt32(split[0]), 0, 0));
            // Set the end time (23:55 means 11:55 PM)
            DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(18, 0, 0));
            //Set 5 minutes interval
            TimeSpan Interval = new TimeSpan(1, 0, 0);
            //To set 1 hour interval
            //TimeSpan Interval = new TimeSpan(1, 0, 0);           
            //fromReminderDate.Items.Clear();
            //toReminderDate.Items.Clear();
            while (StartTime <= EndTime)
            {
                listy.Add(StartTime.ToString("HH:mm"));
                StartTime = StartTime.Add(Interval);
            }
            return listy;
        }
        // Task
        [WebMethod]
        public static List<string> taskgetAttachmentData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<video id='Video" + i + " width='100%' height='380px' muted controls ><source src='" + item.DocumentPath + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                //if (item.ChecklistId == 0)
                                //{
                                //    var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source src='" + item.DocumentPath + "' type='audio/mpeg' /></audio>";
                                //    listy.Add(retstring);
                                //    i++;
                                //}
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img onclick='rotateMe(this);' src='" + item.DocumentPath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }
                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "taskgetAttachmentData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideTaskplay()' data-target='#tasklocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Task Location</p></div></div></div>";
                var i = 1;
                var iTab = 1;
                if (attachments.Count > 0)
                { 
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideTaskplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                if (item.ChecklistId == 0)
                                {
                                    //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                    listy += retstring;
                                    //iTab++;
                                }
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                                var imgstring = String.Format(item.DocumentPath);

                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideTaskplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;

                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstring = "<div class='row static-height-with-border clickable-row' onclick='hideTaskplay()' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getAttachmentDataTab", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataIcons(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (i == 4)
                            {
                                var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#taskattachments-tab' onclick='hideTaskplay()'/>";
                                listy += retstring;
                                i++;
                                break;
                            }
                            else
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                {
                                    if (item.ChecklistId == 0)
                                    {
                                        //var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)'/>";

                                        listy += retstring;
                                        i++;
                                    }
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                    //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                    var retstring = "<img src='" + item.DocumentPath + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "taskgetAttachmentDataIcons", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableRowDataTicket(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var customData = CustomEvent.GetCustomEventById(id, dbConnection);

                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    listy.Add(offenceinfo.PlateNumber);
                    listy.Add(offenceinfo.PlateSource);
                    listy.Add(offenceinfo.PlateCode);
                    listy.Add(offenceinfo.VehicleMake);
                    listy.Add(offenceinfo.VehicleColor);
                    listy.Add(offenceinfo.UserName);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longtitude.ToString(), getClientLic);
                    listy.Add(geolocation);
                    listy.Add(offenceinfo.CreatedDate.AddHours(userinfo.TimeZone).ToString());
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Default", "getTableRowDataTicket", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableRowDataTask(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.CustomerUName);
                    listy.Add(customData.CreateDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.ACustomerUName);
                    listy.Add(customData.StatusDescription);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geolocation);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Description);
                    listy.Add(customData.Notes);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.StartDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.CheckListNotes);
                    listy.Add("circle-point " + CommonUtility.getImgStatus(customData.StatusDescription));
                    var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(customData.TemplateCheckListId.ToString(), dbConnection);
                    if (parentTasks != null)
                        listy.Add(parentTasks.Name);
                    else
                        listy.Add("None");

                    listy.Add(customData.TaskTypeName);

                    if (customData.IncidentId > 0)
                    {
                        var getCus = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                        if (getCus != null)
                        {
                            if (getCus.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                getCus.Name = getCus.EventTypeName;
                                //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus.Identifier, dbConnection);
                                //if (mobEv != null)
                                //{
                                //    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                //    {

                                //    }
                                //    else
                                //        getCus.Name = mobEv.EventTypeName;
                                //}
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.HotEvent || getCus.EventType == CustomEvent.EventTypes.Request)
                            {
                                //var reqEv = HotEvent.GetHotEventById(getCus.Identifier, dbConnection);
                                //if (reqEv != null)
                                //{
                                    //if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                    //    getCus.UserName = CommonUtility.getPCName(reqEv);

                                    if (getCus.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var splitName = getCus.RequestName.Split('^');
                                        if (splitName.Length > 1)
                                            getCus.Name = splitName[1];
                                    }
                                //}
                            }

                            listy.Add(getCus.Name + "|" + customData.IncidentId);
                        
                        
                        }
                        else
                            listy.Add("None");
                    }
                    else
                        listy.Add("None");


                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getTableRowDataTask", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var eventData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);//EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(eventData.Id, dbConnection);

                var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(id), dbConnection);
                if (tasklinks.Count > 0)
                {
                    foreach (var link in tasklinks)
                    {
                        var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                        foreach (var forl in forlink)
                        {
                            forl.isSubTask = true;
                            taskeventhistory.Add(forl);
                        }
                    }
                    taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                }

                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                foreach (var task in taskeventhistory)
                {
                    var taskn = "Task";
                    if (task.isSubTask)
                    {
                        taskn = "Sub-task";
                    }
                    if (eventData.AssigneeType == (int)TaskAssigneeType.Group)
                    {
                        if (task.Action == (int)TaskAction.Update)
                        {
                            acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                                + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> updated<span class='red-color'>" + taskn
                                + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                            listy.Add(acceptedData);
                        }
                    }

                    if (task.Action == (int)TaskAction.Complete)
                    {
                        completedBy = task.CustomerUName;
                        var compLocation = string.Empty;
                        var geoLoc = ReverseGeocode.RetrieveFormatedAddress(eventData.EndLatitude.ToString(), eventData.EndLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc))
                            compLocation = " at " + geoLoc;
                        var compInfo = "completed";
                        var compstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + completedBy + "</span>" + compInfo + "<span class='red-color'>" + taskn
                            + "</span>" + compLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(compstring);
                    }
                    else if (task.Action == (int)TaskAction.InProgress)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "started";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.OnRoute)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "on route";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Pause)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "paused";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Resume)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "resumed";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Accepted)
                    {
                        acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> accepted<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(acceptedData);
                    }
                    else if (task.Action == (int)TaskAction.Rejected)
                    {
                        rejectedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> rejected<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(rejectedData);
                    }
                    else if (task.Action == (int)TaskAction.Assigned)
                    {
                        assignedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> assigned<span class='red-color'>" + taskn
                            + "</span> to " + task.ACustomerUName + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(assignedData);
                    }
                    else if (task.Action == (int)TaskAction.Pending)
                    {
                        var incidentLocation = string.Empty;
                        var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(eventData.Latitude.ToString(), eventData.Longitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc3))
                            incidentLocation = " at " + geoLoc3;
                        var actioninfo = "created";

                        var gUser = Users.GetUserByName(eventData.CreatedBy, dbConnection);

                        var dataString = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + gUser.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + taskn
                            + "</span>for " + task.ACustomerUName + " " + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(dataString);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTaskHistoryData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataIconsTicket(int id)
        {
            var listy = string.Empty;
            //var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
            //var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
            //var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

            //var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/OffenceImagePath.jpeg";
            //var retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-1-tab'/>";
            //listy += retstring2;

            //if (!string.IsNullOrEmpty(attachments.Image1Path))
            //{
            //    imgstring = settings.MIMSMobileAddress + "/Images/videoicon.png";
            //    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#video-0-tab'/>";
            //    listy += retstring2;

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath1.jpeg";
            //    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-2-tab'/>";
            //    listy += retstring2;

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath2.jpeg";
            //    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-3-tab'/>";
            //    listy += retstring2;

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath3.jpeg";
            //    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-4-tab'/>";
            //    listy += retstring2;

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath4.jpeg";
            //    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-5-tab'/>";
            //    listy += retstring2;

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath5.jpeg";
            //    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-6-tab'/>";
            //    listy += retstring2;

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath6.jpeg";
            //    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-7-tab'/>";
            //    listy += retstring2;
            //}
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTabTicket(int id)
        {
            var listy = string.Empty;
            var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
            listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#ticketlocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Location</p></div></div></div>";
            listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-1-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Offence Image</p></div></div></div>";

            var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
            if (!string.IsNullOrEmpty(attachments.Image1Path))
            {
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(1)' data-target='#video-0-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Video</p></div></div></div>";
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-2-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 1</p></div></div></div>";
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-3-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 2</p></div></div></div>";
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-4-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 3</p></div></div></div>";
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-5-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 4</p></div></div></div>";
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-6-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 5</p></div></div></div>";
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-7-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 6</p></div></div></div>";
            }


            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentDataTicket(int id)
        {
            var listy = new List<string>();
            //var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
            //var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
            //var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

            //var retstring = "<video id='Video0' width='100%' height='380px' autoplay='autoplay' type='video/ogg; codecs=theo' muted controls ><source src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' /></video>";
            ////var retstring = "<video id='Video0' width='100%' height='380px' src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' type='video/ogg; codecs=theo' autoplay='autoplay' />";
            //listy.Add(retstring);

            //var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/OffenceImagePath.jpeg";
            //var retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
            //listy.Add(retstring2);

            //if (!string.IsNullOrEmpty(attachments.Image1Path))
            //{
            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath1.jpeg";
            //    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
            //    listy.Add(retstring2);

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath2.jpeg";
            //    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
            //    listy.Add(retstring2);

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath3.jpeg";
            //    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
            //    listy.Add(retstring2);

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath4.jpeg";
            //    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
            //    listy.Add(retstring2);

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath5.jpeg";
            //    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
            //    listy.Add(retstring2);

            //    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath6.jpeg";
            //    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
            //    listy.Add(retstring2);
            //}
            return listy;
        }
        [WebMethod]
        public static List<string> getTicketItems(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var customData = CustomEvent.GetCustomEventById(id, dbConnection);
                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence1))
                        listy.Add(offenceinfo.Offence1);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence2))
                        listy.Add(offenceinfo.Offence2);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence3))
                        listy.Add(offenceinfo.Offence3);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence4))
                        listy.Add(offenceinfo.Offence4);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("OthersDash", "getChecklistData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getChecklistData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var chklisty = new List<string>();
                var isAllChecked = true;
                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);


                var groupedlist = new List<TaskCheckList>();
                var groupedItems = sessions.GroupBy(x => x.Id);
                var groupedlistItems = groupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                foreach (var child in groupedlistItems)
                {
                    var attachfiles = sessions.Where(i => i.Id == child.Id).ToList();
                    var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                    if (mp3files.Count > 0)
                    {
                        //Yes
                        groupedlist.Add(mp3files[0]);
                    }
                    else
                    {
                        groupedlist.Add(attachfiles[0]);
                    }
                }

                foreach (var item in groupedlist)
                {
                    var stringCheck = string.Empty;
                    if (item.IsChecked)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                        isAllChecked = false;
                    }
                    var imgsrc = string.Empty;
                    if (!string.IsNullOrEmpty(item.DocumentPath) && System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                    {

                        imgsrc = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                    }

                    if (item.CheckListItemType == 4 || item.CheckListItemType == 5)
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|True|" + imgsrc);
                    }
                    else if (item.CheckListItemType == 3)
                    {
                        if (!string.IsNullOrEmpty(item.TemplateCheckListItemNote))
                            chklisty.Add(item.Name + "|" + stringCheck + "|3|" + item.TemplateCheckListItemNote + "|" + imgsrc);
                        else
                            chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    else
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    if (item.ChildCheckList != null)
                    {
                        //var audiofiles = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                        //var glist = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() != ".MP3").ToList();

                        var childgroupedlist = new List<TaskCheckList>();
                        var childgroupedItems = item.ChildCheckList.GroupBy(x => x.Id);
                        var childgroupedlistItems = childgroupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                        foreach (var child in childgroupedlistItems)
                        {
                            var attachfiles = item.ChildCheckList.Where(i => i.Id == child.Id).ToList();
                            var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                            if (mp3files.Count > 0)
                            {
                                //Yes
                                childgroupedlist.Add(mp3files[0]);
                            }
                            else
                            {
                                childgroupedlist.Add(attachfiles[0]);
                            }
                        }

                        foreach (var child in childgroupedlist)
                        {
                            var imgsrc2 = string.Empty;
                            if (!string.IsNullOrEmpty(child.DocumentPath) && System.IO.Path.GetExtension(child.DocumentPath).ToUpperInvariant() == ".MP3")
                            {

                                imgsrc2 = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + child.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                            }

                            if (child.IsChecked)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                                isAllChecked = false;
                            }
                            chklisty.Add(child.Name + "|" + stringCheck + "|False|" + imgsrc2);
                        }
                    }
                }
                listy.Add(isAllChecked.ToString());
                listy.AddRange(chklisty);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getChecklistData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskListData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);
                if (usertask.Count > 0)
                    listy.Add(usertask[0].Name);
                foreach (var task in usertask)
                {
                    if (task.Status != (int)TaskStatus.Cancelled)
                        listy.Add(task.ACustomerUName + "|" + task.Id);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getTaskListData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getTaskLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            if (item.StatusDescription == "Pending")
                            {
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                            {
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "OnRoute")
                            {
                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || item.StatusDescription == "Rejected")
                            {
                                if (item.EndLatitude > 0 && item.EndLongitude > 0)
                                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getDispatchUserList(int id, string ttype,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        json += "<a href='#' style='color:" + CommonUtility.getHexColor(0) + "' ><i  class='fa fa-check-square-o'></i>" + item.ACustomerUName + "</a>";
                    }
                    else
                    {
                        var colorCount = 0;
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        foreach (var noti in notification)
                        {
                            if (noti.Prefix == "ARL" && noti.IsHandled)
                            {
                                var tbColor = string.Empty;
                                if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                                {

                                }
                                else
                                {
                                    tbColor = CommonUtility.getHexColor(colorCount);
                                    assigneeList.Add(noti.AssigneeId);
                                    colorCount++;
                                    json += "<a href='#' style='color:" + tbColor + ";font-size:16px;' onclick='tracebackOnUser(&apos;" + id + "&apos;,&apos;" + ttype + "&apos;,&apos;" + noti.AssigneeId + "&apos;)'><i id='" + id + noti.AssigneeId + "'  class='fa fa-check-square-o'></i>" + noti.ACustomerUName + "</a>|";

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getDispatchUserList", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getTracebackLocationData(int id, string ttype, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    json += "[";
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);

                        if (item.StatusDescription == "Pending")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        { 
                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";


                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            if (traceback.Count > 0)
                            {
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                    json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    else
                    {
                        var item = CustomEvent.GetCustomEventById(id, dbConnection);
                        var getEventHistory = EventHistory.GetEventHistoryByEventId(id, dbConnection);
                        var isCompleted = false;
                        //if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject || item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                        //{

                        foreach (var getEv in getEventHistory)
                        {
                            if (getEv.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                            {
                                isCompleted = true;
                                break;
                            }
                        }
                        //}
                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete || isCompleted)
                        {
                            if (item.EventType == CustomEvent.EventTypes.Incident)
                            {
                                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                                if (geofence.Count > 0)
                                {
                                    foreach (var geo in geofence)
                                    {
                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                    }
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            var colorCount = 0;
                            var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                            var notiuserlist = new List<string>();
                            foreach (var noti in notification)
                            {
                                if (noti.Prefix == "ARL" && noti.IsHandled)
                                {
                                    noti.AssigneeName = System.Text.RegularExpressions.Regex.Replace(noti.AssigneeName, @"\s+", "");
                                    if (!notiuserlist.Contains(noti.AssigneeName))
                                    {
                                        notiuserlist.Add(noti.AssigneeName);
                                        var tbColor = string.Empty;
                                        if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                                        {

                                        }
                                        else
                                        {
                                            tbColor = CommonUtility.getHexColor(colorCount);
                                            assigneeList.Add(noti.AssigneeId);
                                            colorCount++;
                                        }
                                        var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                                        if (traceback.Count > 0)
                                        {

                                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                            foreach (var tb in traceback)
                                            {
                                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                                json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                            }
                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                        }
                                        else
                                        {

                                            var engfound = false;
                                            var compfound = false;
                                            var engLongi = string.Empty;
                                            var engLati = string.Empty;
                                            var engDate = string.Empty;
                                            var compLongi = string.Empty;
                                            var compLati = string.Empty;
                                            var compDate = string.Empty;
                                            foreach (var ev in getEventHistory)
                                            {
                                                if (ev.UserName == noti.AssigneeName)
                                                {
                                                    if (!engfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                        {
                                                            engLongi = ev.Longtitude;
                                                            engLati = ev.Latitude;
                                                            engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                            engfound = true;

                                                        }
                                                    }
                                                    if (!compfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                        {
                                                            compLongi = ev.Longtitude;
                                                            compLati = ev.Latitude;
                                                            compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                                            compfound = true;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getTracebackLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getTracebackLocationDataByUser(int id, int duration, string ttype, int[] userIds, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    json += "[";
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);

                        if (item.StatusDescription == "Pending")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        {
                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";


                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }

                        if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            var curTime = CommonUtility.getDTNow();
                            var firstTime = false;
                            if (traceback.Count > 0)
                            {
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);

                                    if (!firstTime)
                                    {
                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                        firstTime = true;
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";

                                    }
                                    else
                                    {
                                        if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                        {
                                            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                            curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                        }
                                    }
                                }
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    else
                    {
                        var item = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (item.EventType == CustomEvent.EventTypes.Incident)
                        {
                            var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                            if (geofence.Count > 0)
                            {
                                foreach (var geo in geofence)
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                        }
                        else
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        var colorCount = 0;
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        var notiuserlist = new List<string>();
                        foreach (var noti in notification)
                        {
                            if (noti.Prefix == "ARL" && noti.IsHandled)
                            {
                                if (!userIds.Contains(noti.AssigneeId))//(noti.AssigneeId != userIds[0])
                                {
                                    noti.AssigneeName = System.Text.RegularExpressions.Regex.Replace(noti.AssigneeName, @"\s+", "");
                                    if (!notiuserlist.Contains(noti.AssigneeName))
                                    {
                                        notiuserlist.Add(noti.AssigneeName);
                                        var tbColor = string.Empty;
                                        if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                                        {

                                        }
                                        else
                                        {
                                            tbColor = CommonUtility.getHexColor(colorCount);
                                            assigneeList.Add(noti.AssigneeId);
                                            colorCount++;
                                        }

                                        var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                                        var curTime = CommonUtility.getDTNow();
                                        var firstTime = false;
                                        var evHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection);
                                        var engLongi = string.Empty;
                                        var engLati = string.Empty;
                                        var engDate = string.Empty;
                                        var compLongi = string.Empty;
                                        var compLati = string.Empty;
                                        var compDate = string.Empty;
                                        if (traceback.Count > 0)
                                        {
                                            //json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                            var engstart = false;

                                            foreach (var tb in traceback)
                                            {
                                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                                if (!engstart)
                                                {
                                                    engstart = true;
                                                    var engfound = false;
                                                    var compfound = false;
                                                    foreach (var ev in evHistory)
                                                    {
                                                        if (ev.UserName == getUser.Username)
                                                        {
                                                            if (!engfound)
                                                            {
                                                                if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                                {
                                                                    engLongi = ev.Longtitude;
                                                                    engLati = ev.Latitude;
                                                                    engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                                    json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                                    json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                                    engfound = true;

                                                                }
                                                            }
                                                            if (!compfound)
                                                            {
                                                                if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                                {
                                                                    compLongi = ev.Longtitude;
                                                                    compLati = ev.Latitude;
                                                                    compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                                    compfound = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (!firstTime)
                                                {
                                                    curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                                    firstTime = true;
                                                    json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";

                                                }
                                                else
                                                {
                                                    if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                                    {
                                                        json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                                    }
                                                }
                                            }
                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                            //json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                        }
                                        else
                                        {
                                            var getEventHistory = EventHistory.GetEventHistoryByEventId(id, dbConnection);
                                            var engfound = false;
                                            var compfound = false;
 
                                            foreach (var ev in getEventHistory)
                                            {
                                                if (ev.UserName == noti.AssigneeName)
                                                {
                                                    if (!engfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                        {
                                                            engLongi = ev.Longtitude;
                                                            engLati = ev.Latitude;
                                                            engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                            engfound = true;

                                                        }
                                                    }
                                                    if (!compfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                        {
                                                            compLongi = ev.Longtitude;
                                                            compLati = ev.Latitude;
                                                            compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                                            compfound = true;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "getTracebackLocationDataByUser", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        //VERIFIER PART FOR EDI DEMO
        [WebMethod]
        public static string getVerifyLocationData(int id)
        {
            var json = string.Empty;
            json += "[";
            if (id > 0)
            {
                var customData = Verifier.GetVerifierById(id, dbConnection);

                if (customData != null)
                {
                    json += "{ \"Username\" : \"" + customData.TypeName + "\",\"Id\" : \"" + customData.Id.ToString() + "\",\"Long\" : \"" + customData.Longitude.ToString() + "\",\"Lat\" : \"" + customData.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                }
            }
            json = json.Substring(0, json.Length - 1);
            json += "]";
            return json;
        }
        [WebMethod]
        public static List<string> getVerifierRequestData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var customData = Verifier.GetVerifierById(id, dbConnection);
                var caseInfo = Enrollment.GetAllEnrollmentByCaseId(customData.CaseId, dbConnection);
                var newCaseId = new List<int>();
                listy.Add(customData.CreatedBy);
                listy.Add(customData.TypeName);
                listy.Add(customData.CaseId.ToString());
                listy.Add(customData.CreatedDate.ToString());
                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                listy.Add(geoLoc);

                if (caseInfo != null)
                {
                    listy.Add(caseInfo.Reason);
                    listy.Add(caseInfo.YearOfBirth);
                    listy.Add(caseInfo.Ethnicity);
                    listy.Add(caseInfo.PID);
                    listy.Add(caseInfo.Gender);
                    listy.Add(caseInfo.ListName);
                }
                else if (customData.EnrollmentResult != null)
                {
                    listy.Add(customData.EnrollmentResult.Reason);
                    listy.Add(customData.EnrollmentResult.YearOfBirth);
                    listy.Add(customData.EnrollmentResult.Ethnicity);
                    listy.Add(customData.EnrollmentResult.PID);
                    listy.Add(customData.EnrollmentResult.Gender);
                    listy.Add(customData.EnrollmentResult.ListName);
                }
                else
                {
                    listy.Add(customData.Reason);
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                }
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + ".jpeg";
                listy.Add(imgstring);

                if (customData.Request == (int)Verifier.RequestTypes.Verify)
                {
                    var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
                    if (resultImg != null)
                    {
                        string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                        listy.Add("data:image/png;base64," + base64String1);
                    }
                    var runVerify = VerifierResults.GetVerifierResultByVerifierId(customData.Id, dbConnection);
                    foreach (var verify in runVerify)
                    {
                        newCaseId.Add(verify.CaseId);
                        imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + "/" + verify.Percent + ".jpeg";
                        listy.Add(imgstring);
                        listy.Add(((int)Math.Round(Convert.ToSingle(verify.Percent) * (float)100)).ToString());
                    }
                }
                var newcaseInfo = Verifier.GetCaseInformationByCaseId(newCaseId[0], dbConnection);
                if (!string.IsNullOrEmpty(newcaseInfo))
                {
                    listy.Add(newcaseInfo);
                }
                else
                {
                    listy.Add("N/A");
                }
                foreach (var caseid in newCaseId)
                {
                    var casename = Verifier.GetCaseInformationByCaseId(caseid, dbConnection);

                    if (!string.IsNullOrEmpty(casename))
                    {
                        listy.Add(casename);
                        listy.Add(caseid.ToString());
                    }
                    else
                    {
                        listy.Add("N/A");
                        listy.Add(caseid.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getVerifierRequestData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getEnrolledData(int id)
        {
            var listy = new List<string>();
            var customData = Enrollment.GetAllEnrollmentByCaseId(id, dbConnection);
            listy.Add(customData.CreatedBy);
            listy.Add(customData.TypeName);
            listy.Add(customData.CaseId.ToString());
            listy.Add(customData.CreatedDate.ToString());
            if (!string.IsNullOrEmpty(customData.Reason))
                listy.Add(customData.Reason);
            else
                listy.Add("N/A");
            listy.Add(customData.PID);
            listy.Add(customData.YearOfBirth);
            listy.Add(customData.Gender);
            listy.Add(customData.Ethnicity);
            listy.Add(customData.ListName);
            var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
            if (resultImg != null)
            {
                string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                listy.Add("data:image/png;base64," + base64String1);
            }
            else
            {
                listy.Add("Images/icon-user-default.png");
            }
            listy.Add(customData.Name);
            return listy;
        }

        [WebMethod]
        public static List<string> rejectReDispatch(string id, string ins, string uname)
        {
            //int taskId1 = 0;
            var listy = new List<string>();
            var newAuthorize = new MyAuthorize();
            var retValve = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retValve)
            {
                listy.Add("LOGOUT");
                return listy;
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var uulist = new List<Users>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        listy.Add(incidentinfo.Name);
                        incidentinfo.UpdatedBy = userinfo.Username;
                        //incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue("reject");
                        //incidentinfo.SiteId = userinfo.SiteId;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        //var returnV = CommonUtility.CreateIncident(incidentinfo);
                        //if (returnV != "")
                        //{
                        //taskId1 = Convert.ToInt32(returnV);
                        //var retVal = incidentinfo.Name;

                        //if (incidentinfo.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        //{
                        //    var getMobName = MobileHotEvent.GetMobileHotEventByGuid(incidentinfo.Identifier, dbConnection);
                        //    {
                        //        if (string.IsNullOrEmpty(getMobName.EventTypeName))
                        //            retVal = "None";
                        //        else
                        //            retVal = getMobName.EventTypeName;

                        //    }
                        //}
                        //listy.Add(retVal);
                        //if (taskId1 < 1)
                        //{
                        //    taskId1 = Convert.ToInt32(id);
                        //}
                        var EventHistoryEntry = new EventHistory();
                        //EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                        //EventHistoryEntry.CreatedBy = userinfo.Username;
                        //EventHistoryEntry.EventId = taskId1;
                        //EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                        //EventHistoryEntry.UserName = userinfo.Username;
                        //EventHistoryEntry.Remarks = ins;
                        //EventHistoryEntry.SiteId = userinfo.SiteId;
                        //EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                        //if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                        //{
                        var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(incidentinfo.EventId, dbConnection);
                        foreach (var noti in notifications)
                        {
                            if (noti.AssigneeType == (int)TaskAssigneeType.User)
                            {
                                var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                taskuser.Engaged = false;
                                taskuser.EngagedIn = string.Empty;


                                if (!listy.Any(str => str.Contains(taskuser.Username)))
                                {
                                    if (taskuser.Username != userinfo.Username)
                                    {
                                        listy.Add(taskuser.Username);
                                        listy.Add(noti.AssigneeId.ToString());
                                        uulist.Add(taskuser);
                                    }

                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                }

                            }
                            else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                            {
                                var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                foreach (var taskuser in groupusers)
                                {
                                    var splitinfo = taskuser.EngagedIn.Split('-');
                                    if (splitinfo.Length > 1)
                                    {
                                        if (splitinfo[1] == noti.Id.ToString())
                                        {
                                            taskuser.Engaged = false;
                                            taskuser.EngagedIn = string.Empty;
                                            Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                        }
                                    }
                                }
                            }
                        }
                        Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusPrefixByIncidentId(incidentinfo.EventId, true, "REJ", dbConnection);
                        var taskinfo = UserTask.GetAllTaskByIncidentId(incidentinfo.EventId, dbConnection);
                        foreach (var task in taskinfo)
                        {
                            task.Status = (int)TaskStatus.Pending;
                            task.RejectionNotes = ins;
                            UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                            var tskhistory = new TaskEventHistory();
                            tskhistory.Action = (int)TaskAction.Assigned;
                            tskhistory.CreatedBy = userinfo.Username;
                            tskhistory.Remarks = task.AssigneeName;
                            tskhistory.CreatedDate = CommonUtility.getDTNow();
                            tskhistory.TaskId = task.Id;
                            tskhistory.SiteId = userinfo.SiteId;
                            tskhistory.CustomerId = userinfo.CustomerInfoId;
                            TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                        }
                        //}
                        incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        incidentinfo.UpdatedBy = userinfo.Username;
                        incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue("dispatch");
                        //incidentinfo.SiteId = userinfo.SiteId;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        var returnV = CommonUtility.CreateIncident(incidentinfo);
                        if (returnV != "")
                        {
                            //foreach (var u in uulist)
                            //{
                            //    MIMSLog.MIMSLogSave(ins, id, new Exception(), dbConnection);
                            //    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            //    EventHistoryEntry.CreatedBy = userinfo.Username;
                            //    EventHistoryEntry.EventId = Convert.ToInt32(id);
                            //    EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                            //    EventHistoryEntry.UserName = u.Username;
                            //    EventHistoryEntry.Remarks = ins;
                            //    EventHistoryEntry.SiteId = userinfo.SiteId;
                            //    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            //}
                        }
                        else
                        {
                            listy.Clear();
                        }
                        //}
                    }
                }
                catch (Exception er)
                {
                    listy.Clear();
                    MIMSLog.MIMSLogSave("Incident", "rejectReDispatch", er, dbConnection, userinfo.SiteId);
                }

            }
            return listy;
        }
        [WebMethod]
        public static string CreatePDFIncident(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var cusevent = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (cusevent != null)
                            return GeneratePDFIncident(cusevent, userinfo);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Incident", "CreatePDFIncident", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }

        [WebMethod]
        public static string escalateIncident(int id, string ins, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        incidentinfo.UpdatedBy = userinfo.Username;
                        incidentinfo.IncidentStatus = (int)CustomEvent.IncidentActionStatus.Escalated;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        var returnV = CommonUtility.CreateIncident(incidentinfo);
                        if (returnV != "")
                        {
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = id;
                            EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                            EventHistoryEntry.UserName = userinfo.Username;
                            EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            CustomEvent.CustomEventHandledById(id, true, userinfo.Username, dbConnection);
                            var escaLevel = 0;
                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                escaLevel = (int)CustomEvent.EscalateTypes.Level2;
                            }
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {
                                escaLevel = (int)CustomEvent.EscalateTypes.Level3;
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                escaLevel = (int)CustomEvent.EscalateTypes.Level4;
                            }
                            CustomEvent.UpdateCustomEventbyEscalate(id, escaLevel, dbConnection);
                            Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(id, true,dbConnection);
                            json = "SUCCESS";
                        }

                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Incident.aspx", "escalateIncident", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }


        private static string GeneratePDFIncident(CustomEvent cusevent, Users userinfo)
        {
            string fileName = string.Empty;
            try
            {
                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                var path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };
                doc.Open();
                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                Font rweb = new Font(z_cn, 11, Font.NORMAL, arrowred);

                Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                Font eleventimesI = new Font(f_cn, 11, Font.ITALIC, Color.BLACK);

                Font mbtimes = new Font(h_cn, 18, Font.BOLD, arrowred);
                Font cbtimes = new Font(h_cn, 13, Font.BOLD, arrowred);

                float[] columnWidthsH = new float[] { 25, 60, 15 };

                var mainheadertable = new PdfPTable(3);
                mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainheadertable.WidthPercentage = 100;
                mainheadertable.SetWidths(columnWidthsH);
                var newTRIlist = new List<TimelineReportItems>();

                var headerMain = new PdfPCell();
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    headerMain.AddElement(new Phrase("G4S INCIDENT REPORT", mbtimes));
                }
                else
                {
                    headerMain.AddElement(new Phrase("MIMS INCIDENT REPORT", mbtimes));
                }
                headerMain.PaddingBottom = 10;
                headerMain.HorizontalAlignment = 1;
                headerMain.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell1 = new PdfPCell();
                pdfCell1.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell2 = new PdfPCell();
                pdfCell2.Border = Rectangle.NO_BORDER;
                mainheadertable.AddCell(pdfCell1);
                mainheadertable.AddCell(headerMain);
                mainheadertable.AddCell(pdfCell2);

                var taskdataTable = new PdfPTable(2);
                var taskdataTableA = new PdfPTable(2);
                var taskdataTableB = new PdfPTable(2);
                taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTable.WidthPercentage = 100;

                taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                taskdataTableA.WidthPercentage = 100;
                taskdataTableB.WidthPercentage = 100;

                float[] columnWidths2575 = new float[] { 25, 75 };
                float[] columnWidths3070 = new float[] { 30, 70 };
                float[] columnWidths3565 = new float[] { 35, 65 };
                float[] columnWidths4060 = new float[] { 40, 60 };
                taskdataTableA.SetWidths(columnWidths4060);

                taskdataTableB.SetWidths(columnWidths4060);

                //SIDE A
                var header1 = new PdfPCell();
                header1.AddElement(new Phrase("Incident Name:", beleventimes));
                header1.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1);

                var header1a = new PdfPCell();
                header1a.AddElement(new Phrase(cusevent.Name, eleventimes));
                header1a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1a);

                var header2 = new PdfPCell();
                header2.AddElement(new Phrase("Incident Type:", beleventimes));
                header2.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2);

                var header2a = new PdfPCell();
                header2a.AddElement(new Phrase(cusevent.Event, eleventimes));
                header2a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2a);

                var header3 = new PdfPCell();
                header3.AddElement(new Phrase("Created Date:", beleventimes));
                header3.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3);

                var header3a = new PdfPCell();
                header3a.AddElement(new Phrase(cusevent.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), eleventimes));
                header3a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3a);

                var header4 = new PdfPCell();
                header4.AddElement(new Phrase("Created by:", beleventimes));
                header4.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4);

                var header4a = new PdfPCell();
                header4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                header4a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4a);

                var header5 = new PdfPCell();
                header5.AddElement(new Phrase("Received by:", beleventimes));
                header5.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header5);

                if (string.IsNullOrEmpty(cusevent.ReceivedBy))
                {
                    cusevent.ReceivedBy = cusevent.CustomerFullName;
                }

                var header5a = new PdfPCell();
                header5a.AddElement(new Phrase(cusevent.ReceivedBy, eleventimes));
                header5a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header5a);

                if (!string.IsNullOrEmpty(cusevent.UserName))
                {
                    var getUserInfo = Users.GetUserByName(cusevent.UserName, dbConnection);
                    if (getUserInfo != null)
                    {
                        if (string.IsNullOrEmpty(cusevent.EmailAddress))
                            cusevent.EmailAddress = getUserInfo.Email;
                        if (string.IsNullOrEmpty(cusevent.PhoneNumber))
                            cusevent.PhoneNumber = getUserInfo.Telephone;
                    }
                }

                var header6 = new PdfPCell();
                header6.AddElement(new Phrase("Contact Number:", beleventimes));
                header6.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header6);

                var header6a = new PdfPCell();
                header6a.AddElement(new Phrase(cusevent.PhoneNumber, eleventimes));
                header6a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header6a);

                var header7 = new PdfPCell();
                header7.AddElement(new Phrase("Contact Email:", beleventimes));
                header7.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header7);

                var header7a = new PdfPCell();
                header7a.AddElement(new Phrase(cusevent.EmailAddress, eleventimes));
                header7a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header7a);

                var header8 = new PdfPCell();
                header8.AddElement(new Phrase("Description:", beleventimes));
                header8.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8);

                if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    cusevent.Description = cusevent.MobDescription;

                var header8a = new PdfPCell();
                header8a.AddElement(new Phrase(cusevent.Description, eleventimes));
                header8a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8a);

                var header9 = new PdfPCell();
                header9.AddElement(new Phrase("Instructions:", beleventimes));
                header9.Border = Rectangle.NO_BORDER;
                header9.PaddingBottom = 20;
                taskdataTableA.AddCell(header9);

                if (!string.IsNullOrEmpty(cusevent.Instructions))
                {
                    var splitIns = cusevent.Instructions.Split('|');
                    if (splitIns.Length > 0)
                        cusevent.Instructions = splitIns[0];
                }

                var header9a = new PdfPCell();
                header9a.AddElement(new Phrase(cusevent.Instructions, eleventimes));
                header9a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header9a);

                //SIDE B

                var headerB2 = new PdfPCell();
                headerB2.AddElement(new Phrase("Incident Status:", beleventimes));
                headerB2.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2);

                var headerB2a = new PdfPCell();
                headerB2a.AddElement(new Phrase(cusevent.StatusName, eleventimes));
                headerB2a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2a);

                var headerB1 = new PdfPCell();
                headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                headerB1.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB1);


                var siteinfo = Arrowlabs.Business.Layer.Site.GetSiteById(cusevent.SiteId, dbConnection);
                if (siteinfo != null)
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase(siteinfo.Name, eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }
                else
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase("N/A", eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }


                var headerB3 = new PdfPCell();
                headerB3.AddElement(new Phrase("Dispatched By:", beleventimes));
                headerB3.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB3);

                var headerB3a = new PdfPCell();
                headerB3a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                headerB3a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB3a);


                if (cusevent.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                {
                    var headerB4 = new PdfPCell();
                    headerB4.AddElement(new Phrase("Resolved By:", beleventimes));
                    headerB4.Border = Rectangle.NO_BORDER;
                    headerB4.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB4);

                    var headerB4a = new PdfPCell();
                    headerB4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                    headerB4a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB4a);

                    var headerB5 = new PdfPCell();
                    headerB5.AddElement(new Phrase("Resolved Notes:", beleventimes));
                    headerB5.Border = Rectangle.NO_BORDER;
                    headerB5.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB5);

                    var evsHistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                    var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve);
                    if (isEsca != null)
                    {
                        foreach (var esca in isEsca)
                        {
                            var headerB5a = new PdfPCell();
                            headerB5a.AddElement(new Phrase(esca.Remarks, eleventimes));
                            headerB5a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB5a);
                            break;
                        }
                    }
                    else
                    {
                        var headerB5a = new PdfPCell();
                        headerB5a.AddElement(new Phrase("N/A", eleventimes));
                        headerB5a.Border = Rectangle.NO_BORDER;
                        taskdataTableB.AddCell(headerB5a);
                    }
                }

                taskdataTable.AddCell(taskdataTableA);
                taskdataTable.AddCell(taskdataTableB);

                var maptextdataTable = new PdfPTable(1);
                maptextdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                maptextdataTable.WidthPercentage = 100;

                var geolocation = ReverseGeocode.RetrieveFormatedAddress(cusevent.Latitude.ToString(), cusevent.Longtitude.ToString(), getClientLic);
                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, cusevent.EventId);
                if (string.IsNullOrEmpty(geolocation))
                {
                    if (geofence.Count > 0)
                    {
                        geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        cusevent.Longtitude = geofence[0].Longitude.ToString();
                        cusevent.Latitude = geofence[0].Latitude.ToString();

                    }
                }

                if (string.IsNullOrEmpty(geolocation))
                    geolocation = "Not Specified";

                var mapheader = new PdfPCell();
                mapheader.AddElement(new Phrase("Incident Location:" + geolocation, eleventimes));
                mapheader.Border = Rectangle.NO_BORDER;
                maptextdataTable.AddCell(mapheader);

                var mapdataTable = new PdfPTable(2);
                mapdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                mapdataTable.WidthPercentage = 100;
                float[] columnWidths7030 = new float[] { 70, 30 };
                mapdataTable.SetWidths(columnWidths7030);
                //MAP

                //outer.AddCell(table);
                var latComp = string.Empty;
                var longComp = string.Empty;
                var latEng = string.Empty;
                var longEng = string.Empty;
                var arrived = string.Empty;
                var vargeopath = string.Empty; //&path=color:0xff0000ff|weight:5|25.09773,55.16316|25.0978512224338,55.1634863298386
                var tracebackpath = string.Empty; //&path=color:blue|weight:3|25.09773,55.16316|25.0978512224338,55.1634863298386


                var incidenteventhistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;

                foreach (var rem in incidenteventhistory)
                {
                    var timelineItem = new TimelineReportItems();

                    if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        cusevent.Name = cusevent.EventTypeName;
                    }
                    else if (cusevent.EventType == CustomEvent.EventTypes.Request)
                    {
                        var splitName = cusevent.RequestName.Split('^');
                        if (splitName.Length > 1)
                        {
                            cusevent.Name = splitName[1];
                        }
                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerFullName;
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "engaged ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";
                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    newTRIlist.Add(timelineItem);
                }


                if (geofence.Count > 0)
                {
                    vargeopath = "&path=color:0xff0000ff|weight:5|";
                    foreach (var geo in geofence)
                    {
                        vargeopath += geo.Latitude + "," + geo.Longitude + "|";
                    }
                    vargeopath = vargeopath.Substring(0, vargeopath.Length - 1);
                }
                var assigneeList = new List<int>();
                var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(cusevent.EventId, dbConnection);

                var colorCount = 0;
                var legendTraceback = new List<string>();
                foreach (var noti in notification)
                {
                    var tbColor = string.Empty;
                    if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                    {

                    }
                    else
                    {
                        tbColor = CommonUtility.getColorName(colorCount);
                        assigneeList.Add(noti.AssigneeId);
                        legendTraceback.Add(tbColor + " Path:  " + noti.ACustomerFullName + " route");
                        colorCount++;
                    }
                    var tracebackhistory = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);
                    var tbCount = 0;
                    var tbRemovedPointsCount = 0;

                    var tbContinueCountStart = 0;
                    if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                    {
                        tbRemovedPointsCount = tracebackhistory.Count - CommonUtility.tracebackreportmaxcount;
                        tbContinueCountStart = (CommonUtility.tracebackreportmaxcount / 2) + tbRemovedPointsCount;
                    }
                    if (tracebackhistory.Count > 0)
                    {
                        tracebackpath += "&path=color:blue|weight:5|";
                        tracebackpath += latEng + "," + longEng + "|";
                        foreach (var trace in tracebackhistory)
                        {
                            if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                            {
                                tbCount++;
                                if (tbCount < (CommonUtility.tracebackreportmaxcount / 2))
                                    tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                                else if (tbCount > tbContinueCountStart)
                                    tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                            }
                            else
                                tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                        }
                        tracebackpath += latComp + "," + longComp;
                    }
                }
                var maplat = cusevent.Latitude;
                var maplong = cusevent.Longtitude;
                var taskLocation = string.Empty;
                if (cusevent.Latitude != "0" && cusevent.Longtitude != "0")
                {
                    taskLocation = "&markers=color:red%7Clabel:T%7C" + cusevent.Latitude + "," + cusevent.Longtitude;
                }
                else
                {
                    maplat = latEng;
                    maplong = longEng;
                }
                //Map style roadMap
                String mapURL = "https://maps.googleapis.com/maps/api/staticmap?" +
"center=" + cusevent.Latitude + "," + cusevent.Longtitude + "&" +
"size=700x360" + tracebackpath + vargeopath + arrived + "&markers=color:green%7Clabel:C%7C" + latComp + "," + longComp + "&markers=color:yellow%7Clabel:E%7C" + latEng + "," + longEng + taskLocation + "&zoom=17" + "&maptype=png" + "&sensor=true";
                iTextSharp.text.Image LocationImage = iTextSharp.text.Image.GetInstance(mapURL);
                LocationImage.ScaleToFit(380, 320);
                LocationImage.Alignment = iTextSharp.text.Image.UNDERLYING;
                LocationImage.SetAbsolutePosition(0, 0);
                PdfPCell locationcell = new PdfPCell(LocationImage);
                locationcell.PaddingTop = 5;
                locationcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right 
                locationcell.Border = 0;
                mapdataTable.AddCell(locationcell);


                PdfPTable legendtable = new PdfPTable(1);
                legendtable.DefaultCell.Border = Rectangle.NO_BORDER;
                legendtable.HorizontalAlignment = 0;
                Font redfont = new Font();
                redfont.Size = 11;
                Font yellowfont = new Font();
                yellowfont.Size = 11;
                Font greenfont = new Font();
                greenfont.Size = 11;
                Font bluefont = new Font();
                bluefont.Size = 11;
                redfont.Color = Color.RED;
                legendtable.AddCell(new Phrase("Legend:", ueleventimes));
                legendtable.AddCell(new Phrase("I:  Incident Location", redfont));
                yellowfont.Color = new Color(204, 204, 0);
                legendtable.AddCell(new Phrase("E:  Engaged Location", yellowfont));
                bluefont.Color = iTextSharp.text.Color.BLUE;
                legendtable.AddCell(new Phrase("A:  Arrived Location", bluefont));
                greenfont.Color = new Color(0, 204, 0);
                legendtable.AddCell(new Phrase("C:  Completed Location", greenfont));
                var pathCell = new PdfPCell(new Phrase("Red Path:  Geofence", eleventimes));
                pathCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                legendtable.AddCell(pathCell);
                foreach (var legendtrace in legendTraceback)
                {
                    var traceCell = new PdfPCell(new Phrase(legendtrace, eleventimes));
                    traceCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    legendtable.AddCell(traceCell);
                }
                mapdataTable.AddCell(legendtable);

                iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                paragraphTable.SpacingBefore = 120f;
                Paragraph p = new Paragraph();
                p.IndentationLeft = 10;

                p.Add(mainheadertable);
                p.Add(taskdataTable);
                p.Add(maptextdataTable);
                p.Add(mapdataTable);

                PdfPTable table = new PdfPTable(1);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                //Map style roadMap

                doc.Add(paragraphTable);

                doc.Add(p);

                //Attachments

                var listycanvasnotes = new List<string>();

                var attachments = new List<MobileHotEventAttachment>();


                if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusevent.Identifier, dbConnection);
                    if (mobEv != null)
                        attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection));


                }

                attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusevent.EventId, dbConnection));

                var videoCount = 0;
                var imgCount = 0;
                var pdfCount = 0;

                PdfPTable taskimagetable = new PdfPTable(2);
                taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskimagetable.WidthPercentage = 100;

                var atheader1 = new PdfPCell();
                atheader1.AddElement(new Phrase("Attachments", cbtimes));
                atheader1.Border = Rectangle.NO_BORDER;
                atheader1.PaddingTop = 10;
                atheader1.PaddingBottom = 10;
                atheader1.Colspan = 2;
                taskimagetable.AddCell(atheader1);
                taskimagetable.KeepTogether = true;
                if (attachments.Count > 0)
                {
                    var attachcount = 0;
                    var canvasattachcount = 0;
                    var attachlist = new List<MobileHotEventAttachment>();
                    var videosList = new List<MobileHotEventAttachment>();

                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                            {
                                videoCount++;
                            }
                            else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".MP3")
                            {
                                videoCount++;
                            }
                            else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                            {
                                pdfCount++;
                            }
                            else
                            {
                                attachlist.Add(item);
                                attachcount++;
                                imgCount++;
                                listycanvasnotes.Add("Image " + attachcount);
                            }
                        }
                    }
                    taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                    var countz = 0;
                    foreach (var item in attachlist)
                    {
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                        {

                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".MP3")
                        {

                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                        {

                        }
                        else
                        {
                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.AttachmentPath);

                            if (image.Height > image.Width)
                            {
                                //Maximum height is 800 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Height;
                                image.ScalePercent(percentage * 100);
                            }
                            else
                            {
                                //Maximum width is 600 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Width;
                                image.ScalePercent(percentage * 100);
                            }

                            //image.ScaleAbsolute(120f, 155.25f);
                            image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                            if (!string.IsNullOrEmpty(listycanvasnotes[countz]))
                            {
                                Paragraph pcell = new Paragraph(listycanvasnotes[countz].ToUpper());
                                pcell.Alignment = Element.ALIGN_LEFT;
                                pcell.SpacingAfter = 10;
                                imgCell1.AddElement(pcell);
                            }
                            imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                            imgCell1.AddElement(new Chunk(image, 0, 0));
                            imgCell1.PaddingTop = 10;
                            taskimagetable.AddCell(imgCell1);
                            countz++;
                        }
                    }
                    taskimagetable.CompleteRow();
                }

                var extraattachment = new PdfPTable(3);
                extraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                extraattachment.WidthPercentage = 100;
                var phrase2 = new Phrase();
                phrase2.Add(
                    new Chunk("PDF Attached:  ", btimes)
                );
                phrase2.Add(new Chunk(pdfCount.ToString(), times));

                var zheaderA3a = new PdfPCell();
                zheaderA3a.AddElement(phrase2);
                zheaderA3a.Border = Rectangle.NO_BORDER;
                zheaderA3a.PaddingTop = 20;
                zheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(zheaderA3a);

                var phrase32 = new Phrase();
                phrase32.Add(
                    new Chunk("Media Attached:  ", btimes)
                );
                phrase32.Add(new Chunk(videoCount.ToString(), times));

                var aheaderA3a = new PdfPCell();
                aheaderA3a.AddElement(phrase32);
                aheaderA3a.Border = Rectangle.NO_BORDER;
                aheaderA3a.PaddingTop = 20;
                aheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aheaderA3a);

                var phrase323 = new Phrase();
                phrase323.Add(
                    new Chunk("Images Attached:  ", btimes)
                );
                phrase323.Add(new Chunk(imgCount.ToString(), times));

                var aaheaderA3a = new PdfPCell();
                aaheaderA3a.AddElement(phrase323);
                aaheaderA3a.Border = Rectangle.NO_BORDER;
                aaheaderA3a.PaddingTop = 20;
                aaheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aaheaderA3a);

                doc.NewPage();

                doc.Add(taskimagetable);

                doc.Add(extraattachment);


                PdfPTable timetable = new PdfPTable(2);
                timetable.WidthPercentage = 100;
                timetable.DefaultCell.Border = Rectangle.NO_BORDER;
                timetable.SetWidths(columnWidths2575);

                if (newTRIlist.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Incident Timeline", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    timetable.AddCell(headeventCell);

                    foreach (var lit in newTRIlist)
                    {
                        var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(eventCell);
                    }
                }
                doc.Add(timetable);

                //TASKS LOOP

                var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    int count = 1;

                    foreach (var task in utask)
                    {
                        var tskheadertable = new PdfPTable(2);
                        tskheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskheadertable.WidthPercentage = 100;
                        float[] columnWidths2080 = new float[] { 20, 80 };
                        tskheadertable.SetWidths(columnWidths2080);

                        var tskheader0 = new PdfPCell();
                        //tskheader0.AddElement(new Phrase("Task #:" + count, cbtimes));

                        tskheader0.AddElement(new Phrase("Attached Task", cbtimes));
                        tskheader0.Border = Rectangle.NO_BORDER;
                        tskheader0.Colspan = 2;
                        tskheadertable.AddCell(tskheader0);

                        var tskheader1 = new PdfPCell();
                        tskheader1.AddElement(new Phrase("Task Name:", beleventimes));
                        tskheader1.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1);

                        var tskheader1a = new PdfPCell();
                        tskheader1a.AddElement(new Phrase(task.Name, eleventimes));
                        tskheader1a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1a);

                        var header1z = new PdfPCell();
                        header1z.AddElement(new Phrase("Task Number:", beleventimes));
                        header1z.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1z);

                        var header1az = new PdfPCell();
                        header1az.AddElement(new Phrase(task.NewCustomerTaskId, eleventimes));
                        header1az.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1az);

                        var tskheader2 = new PdfPCell();
                        tskheader2.AddElement(new Phrase("Task Description:", beleventimes));
                        tskheader2.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2);

                        var tskheader2a = new PdfPCell();
                        tskheader2a.AddElement(new Phrase(task.Description, eleventimes));
                        tskheader2a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2a);

                        var chklistName = "None";
                        var newTCLlist = new List<TaskChecklistReportItems>();
                        var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(task.TemplateCheckListId.ToString(), dbConnection);
                        if (parentTasks != null)
                        {
                            chklistName = parentTasks.Name;

                            var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(task.Id, dbConnection);

                            foreach (var item in sessions)
                            {
                                var newTCL = new TaskChecklistReportItems();
                                newTCL.isParent = true;

                                if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, eleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                else
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, beleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                newTCL.Notes = item.TemplateCheckListItemNote;

                                var myattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(item.Id, dbConnection);
                                newTCL.Attachments = myattachments;


                                var stringCheck = string.Empty;
                                var itemNotes = string.Empty;

                                if (item.IsChecked)
                                {
                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("3", gweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;
                                }
                                else
                                {
                                    List myList = new ZapfDingbatsList(54);

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("6", rweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;

                                    //if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                    //    chkCounter++;
                                }

                                newTCLlist.Add(newTCL);

                                if (item.ChildCheckList != null)
                                {
                                    if (item.ChildCheckList.Count > 0)
                                    {
                                        foreach (var child in item.ChildCheckList)
                                        {

                                            var cnewTCL = new TaskChecklistReportItems();
                                            cnewTCL.isParent = false;

                                            var ncaheaderA = new PdfPCell();
                                            ncaheaderA.AddElement(new Phrase(child.Name, eleventimesI));
                                            ncaheaderA.Border = Rectangle.NO_BORDER;
                                            cnewTCL.itemName = ncaheaderA;

                                            cnewTCL.Notes = child.TemplateCheckListItemNote;

                                            var mycattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(child.Id, dbConnection);
                                            cnewTCL.Attachments = mycattachments;

                                            if (item.IsChecked)
                                            {
                                                //gweb rweb
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("3", gweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                            }
                                            else
                                            {
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("6", rweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                                //chkCounter++;
                                            }

                                            newTCLlist.Add(cnewTCL);
                                        }
                                    }
                                }
                            }
                        }
                        var tskheader3 = new PdfPCell();
                        tskheader3.AddElement(new Phrase("Checklist Name:", beleventimes));
                        tskheader3.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3);

                        var tskheader3a = new PdfPCell();
                        tskheader3a.AddElement(new Phrase(chklistName, eleventimes));
                        tskheader3a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3a);

                        doc.Add(tskheadertable);

                        var newchecklistTable = new PdfPTable(4);
                        newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        newchecklistTable.WidthPercentage = 100;
                        float[] columnWidthsCHK = new float[] { 30, 10, 30, 30 };
                        newchecklistTable.SetWidths(columnWidthsCHK);

                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("Checklist", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.PaddingTop = 10;
                        cheader1.PaddingBottom = 10;
                        cheader1.Colspan = 4;
                        newchecklistTable.AddCell(cheader1);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Item Name", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.PaddingLeft = 5;
                        ncaheader1.PaddingBottom = 5;
                        ncaheader1.BackgroundColor = Color.DARK_GRAY;

                        newchecklistTable.AddCell(ncaheader1);

                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Status", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Notes", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("Attachments", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader4);

                        foreach (var item in newTCLlist)
                        {
                            newchecklistTable.AddCell(item.itemName);

                            newchecklistTable.AddCell(item.Status);

                            var ncaheaderAB = new PdfPCell();
                            ncaheaderAB.AddElement(new Phrase(item.Notes, eleventimes));
                            ncaheaderAB.Border = Rectangle.NO_BORDER;
                            newchecklistTable.AddCell(ncaheaderAB);

                            if (item.Attachments.Count > 0)
                            {
                                PdfPTable imgTable = new PdfPTable(1);
                                imgTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                foreach (var attach in item.Attachments)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant()))
                                    {
                                        imgTable.AddCell(new Phrase("Video Attached", eleventimes));
                                    }
                                    else if (System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant() == ".MP3")
                                    {
                                        imgTable.AddCell(new Phrase("Audio Attached", eleventimes));
                                    }
                                    else if (System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        imgTable.AddCell(new Phrase("PDF Attached", eleventimes));
                                    }
                                    else
                                    {
                                        //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + task.Id + "/{0}", System.IO.Path.GetFileName(attach.DocumentPath));

                                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(attach.DocumentPath);

                                        if (image.Height > image.Width)
                                        {
                                            //Maximum height is 800 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Height;
                                            image.ScalePercent(percentage * 100);
                                        }
                                        else
                                        {
                                            //Maximum width is 600 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Width;
                                            image.ScalePercent(percentage * 100);
                                        }

                                        //image.ScaleAbsolute(120f, 155.25f);
                                        image.Border = Rectangle.NO_BORDER;
                                        iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1l.Border = Rectangle.NO_BORDER;
                                        imgCell1l.AddElement(new Chunk(image, 0, 0));
                                        imgTable.AddCell(imgCell1l);
                                        if (!string.IsNullOrEmpty(attach.ImageNote) && attach.ImageNote != "Task Attachment")
                                            imgTable.AddCell(new Phrase("Img Note:" + attach.ImageNote, eleventimes));
                                    }
                                }
                                newchecklistTable.AddCell(imgTable);
                            }
                            else
                            {
                                newchecklistTable.AddCell(new Phrase(" ", eleventimes));
                            }
                        }

                        doc.Add(newchecklistTable);

                        var tsklistycanvasnotes = new List<string>();
                        var tskattachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(task.Id, dbConnection);
                        var tskvideoCount = 0;
                        var tskimgCount = 0;
                        var tskpdfCount = 0;

                        PdfPTable tsktaskimagetable = new PdfPTable(2);
                        tsktaskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktaskimagetable.WidthPercentage = 100;

                        var tskatheader1 = new PdfPCell();
                        tskatheader1.AddElement(new Phrase("Task Attachments", cbtimes));
                        tskatheader1.Border = Rectangle.NO_BORDER;
                        tskatheader1.PaddingTop = 10;
                        tskatheader1.PaddingBottom = 10;
                        tskatheader1.Colspan = 2;
                        tsktaskimagetable.AddCell(tskatheader1);

                        if (tskattachments.Count > 0)
                        {
                            var attachcount = 0;
                            var canvasattachcount = 0;
                            var attachlist = new List<UserTaskAttachment>();

                            foreach (var item in tskattachments)
                            {
                                if (!string.IsNullOrEmpty(item.DocumentPath) && item.ChecklistId == 0)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        tskpdfCount++;
                                    }
                                    else
                                    {
                                        attachlist.Add(item);
                                        attachcount++;
                                        tskimgCount++;
                                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                                        {
                                            tsklistycanvasnotes.Add("Canvas Notes Image " + attachcount + ":" + item.ImageNote);
                                            canvasattachcount++;
                                        }
                                        else
                                        {
                                            tsklistycanvasnotes.Add("Image " + attachcount);
                                            canvasattachcount++;
                                        }
                                    }
                                }
                            }

                            var countz = 0;
                            foreach (var item in attachlist)
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {

                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                {

                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.DocumentPath);

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    if (!string.IsNullOrEmpty(tsklistycanvasnotes[countz]))
                                    {
                                        Paragraph pcell = new Paragraph(tsklistycanvasnotes[countz].ToUpper());
                                        pcell.Alignment = Element.ALIGN_LEFT;
                                        pcell.SpacingAfter = 10;
                                        imgCell1.AddElement(pcell);
                                    }
                                    imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                    imgCell1.AddElement(new Chunk(image, 0, 0));
                                    imgCell1.PaddingTop = 10;
                                    tsktaskimagetable.AddCell(imgCell1);
                                    countz++;
                                }
                            }
                            tsktaskimagetable.CompleteRow();
                        }

                        var tskextraattachment = new PdfPTable(3);
                        tskextraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskextraattachment.WidthPercentage = 100;
                        var tskphrase2 = new Phrase();
                        tskphrase2.Add(
                            new Chunk("PDF Attached:  ", btimes)
                        );
                        tskphrase2.Add(new Chunk(tskpdfCount.ToString(), times));

                        var tskzheaderA3a = new PdfPCell();
                        tskzheaderA3a.AddElement(tskphrase2);
                        tskzheaderA3a.Border = Rectangle.NO_BORDER;
                        tskzheaderA3a.PaddingTop = 20;
                        tskzheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskzheaderA3a);

                        var tskphrase32 = new Phrase();
                        tskphrase32.Add(
                            new Chunk("Media Attached:  ", btimes)
                        );
                        tskphrase32.Add(new Chunk(tskvideoCount.ToString(), times));

                        var tskaheaderA3a = new PdfPCell();
                        tskaheaderA3a.AddElement(tskphrase32);
                        tskaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaheaderA3a.PaddingTop = 20;
                        tskaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaheaderA3a);

                        var tskphrase323 = new Phrase();
                        tskphrase323.Add(
                            new Chunk("Images Attached:  ", btimes)
                        );
                        tskphrase323.Add(new Chunk(tskimgCount.ToString(), times));

                        var tskaaheaderA3a = new PdfPCell();
                        tskaaheaderA3a.AddElement(tskphrase323);
                        tskaaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaaheaderA3a.PaddingTop = 20;
                        tskaaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaaheaderA3a);

                        doc.Add(tsktaskimagetable);

                        doc.Add(tskextraattachment);

                        var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(task.Id, dbConnection);

                        taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();

                        var tsknewTRIlist = new List<TimelineReportItems>();

                        foreach (var taskEv in taskeventhistory)
                        {
                            var timelineItem = new TimelineReportItems();
                            var taskn = "Task";
                            if (taskEv.isSubTask)
                            {
                                taskn = "Sub-task";
                            }

                            if (taskEv.Action == (int)TaskAction.Complete)
                            {
                                completedBy = taskEv.CustomerFullName;
                                var compLocation = string.Empty;
                                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(task.EndLatitude.ToString(), task.EndLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc))
                                    compLocation = " at " + geoLoc;
                                var compInfo = "completed";
                                latComp = task.EndLatitude.ToString();
                                longComp = task.EndLongitude.ToString();
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                                var compstring = completedBy + " " + compInfo + " " + taskn + " " + compLocation;

                                timelineItem.TimelineActivity = compstring;
                                //listy.Add(compstring);
                            }
                            else if (taskEv.Action == (int)TaskAction.InProgress)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "started";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.OnRoute)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "onroute";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Pause)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "paused";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Resume)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "resumed";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Accepted)
                            {
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                acceptedData = taskEv.CustomerFullName + " accepted " + taskn;
                                //listy.Add(acceptedData);
                                timelineItem.TimelineActivity = acceptedData;
                            }
                            else if (taskEv.Action == (int)TaskAction.Rejected)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                rejectedData = taskEv.CustomerFullName + " rejected Task " + taskn;

                                timelineItem.TimelineActivity = rejectedData;
                                //listy.Add(rejectedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Assigned)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                assignedData = taskEv.CustomerFullName + " assigned " + taskn + " to " + taskEv.ACustomerFullName;

                                timelineItem.TimelineActivity = assignedData;

                                //listy.Add(assignedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Pending)
                            {
                                var incidentLocation = string.Empty;
                                var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(task.Latitude.ToString(), task.Longitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc3))
                                    incidentLocation = " at " + geoLoc3;
                                var actioninfo = "created";
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                actioninfo = task.CustomerFullName + " " + actioninfo + " " + taskn + " for " + taskEv.ACustomerFullName + " " + incidentLocation;
                                timelineItem.TimelineActivity = actioninfo;
                                //listy.Add(actioninfo);
                            }
                            tsknewTRIlist.Add(timelineItem);
                        }

                        PdfPTable tsktimetable = new PdfPTable(2);
                        tsktimetable.WidthPercentage = 100;
                        tsktimetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktimetable.SetWidths(columnWidths2575);

                        if (tsknewTRIlist.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Task Timeline", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            tsktimetable.AddCell(headeventCell);

                            foreach (var lit in tsknewTRIlist)
                            {
                                var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(eventCell);
                            }
                        }

                        doc.Add(tsktimetable);


                        PdfPTable notestable = new PdfPTable(2);
                        notestable.WidthPercentage = 100;
                        notestable.DefaultCell.Border = Rectangle.NO_BORDER;
                        notestable.SetWidths(columnWidths2575);

                        var tremarks = TaskRemarks.GetTaskRemarksByTaskId(task.Id, dbConnection);

                        if (tremarks.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Notes", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            notestable.AddCell(headeventCell);
                            foreach (var remark in tremarks)
                            {
                                var dateCell = new PdfPCell(new Phrase(remark.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString(), redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                notestable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(remark.FName + " " + remark.LName + ":" + remark.Remarks, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                notestable.AddCell(eventCell);
                            }
                        }

                        doc.Add(notestable);
                    }
                }
                //END OF TASKS



                Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                var endTable = new PdfPTable(1);
                endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                endTable.WidthPercentage = 100;

                var endheader = new PdfPCell();
                endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                endheader.Border = Rectangle.NO_BORDER;
                endTable.AddCell(endheader);
                doc.Add(endTable);

                writer.Flush();
                doc.Close();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                        return vpath;
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Incident", "GeneratePDFIncident", exp, dbConnection, userinfo.SiteId);
                return "Failed to generate report!";
            }
            return "Failed to generate report!";
        }

        [WebMethod]
        public static string geofenceSuccess(int returnV,string uname)
        {
            var retVal = "FAIL";
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (returnV > 0)
                {
                    var geofenceCheck = MIMSLog.GetAllMIMSLogsByMacAddress(returnV.ToString(), dbConnection);
                    if (geofenceCheck.Count > 0)
                    {
                        return "SUCCESS";
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "geofenceSuccess", ex, dbConnection, userinfo.SiteId);
            }
            return retVal;
        }

        [WebMethod]
        public static string deleteIncident(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                if (id > 0)
                {
                    CustomEvent.DeleteCustomEventById(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Default", id.ToString(), id.ToString(), userinfo, "Delete Incident" + id);
                    json = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "deleteIncident", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
            return json;
        }

        [WebMethod]
        public static string markReminderAsRead(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var reminderInfo = Reminders.GetReminderById(id, dbConnection);
                reminderInfo.IsCompleted = true;
                Reminders.InsertorUpdateReminder(reminderInfo, dbConnection, dbConnectionAudit, true);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "markReminderAsRead", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static string checkLiveVideo(string camera, string uname)
        {
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var ipaddress = CommonUtility.milestonemobile;
                var port = CommonUtility.milestoneport;
                if (!string.IsNullOrEmpty(ipaddress) && CommonUtility.isNumeric(port))
                {
                    using (TcpClient tcpClient = new TcpClient(ipaddress, Convert.ToInt32(port)))
                    {
                        var alarm = new ServiceController("Milestone Mobile Service");
                        if (alarm != null)
                        {
                            if (alarm.Status.Equals(ServiceControllerStatus.Running))
                            {
                                retVal = "SUCCESS";
                            }
                            else
                            {
                                retVal = "Sorry,failed to connect to mobile video service.";
                            }
                        }
                        else
                        {
                            retVal = "Sorry,failed to connect to mobile video service.";
                        }
                        tcpClient.GetStream().Close();
                        tcpClient.Close();
                    }
                }
                else
                    retVal = "Problem with milestone settings.";
                return retVal;
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident", "checkLiveVideo", ex, dbConnection, userinfo.SiteId);
                return "Sorry,failed to connect to mobile video service.";
            }
        }
		[WebMethod]
        public static List<string> getChecklistNotesData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                foreach (var item in sessions)
                {
                    if (item.CheckListItemType == 5)
                    {
                        if (item.ChildCheckList != null)
                        {
                            foreach (var child in item.ChildCheckList)
                            {
                                if (!child.IsChecked)
                                    listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default", "getChecklistNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getCanvasNotesData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var count = 1;
                foreach (var item in sessions)
                {
                    if (!string.IsNullOrEmpty(item.DocumentPath))
                    {
                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                        {
                            listy.Add("Attachment " + count + "|" + item.ImageNote);
                        }
                        count++;    //listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default", "getCanvasNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string deleteAttachmentData(int id, int incidentid, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {

                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                try
                {

                    var ret = MobileHotEventAttachment.GetMobileHotEventAttachmentById(id, dbConnection);
                    if (ret != null)
                    {

                        MobileHotEventAttachment.DeleteMobileHotEventAttachmentById(id, dbConnection);

                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "UserTaskAttachment", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());

                        CommonUtility.CloudDeleteFile(ret[0].AttachmentPath);

                        listy = "Successfully deleted entry";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Default", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string UpdateCustomEventView(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }


                    if (id > 0)
                    {
                        CustomEvent.UpdateCustomEventView(id, dbConnection);
                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Default", "UpdateCustomEventView", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }

        [WebMethod]
        public static string attachFileToTask(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new MobileHotEventAttachment();

                    var newimgPath = filepath.Replace('|', '\\');
                    //var mimsconfigsettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    //var savestring = mimsconfigsettings.FilePath.Remove(mimsconfigsettings.FilePath.Length - 5);
                    //savestring = savestring + "HotEvents";
                    //if (!System.IO.Directory.Exists(savestring))
                    //    System.IO.Directory.CreateDirectory(savestring);

                    //if (System.IO.Path.GetExtension(newimgPath).ToUpperInvariant() == ".PDF")
                    //{
                    //    var bytes = File.ReadAllBytes(newimgPath);
                    //    var Tsavestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + ".pdf";
                    //    if (!System.IO.File.Exists(Tsavestring))
                    //    {
                    //        savestring = Tsavestring;
                    //        File.WriteAllBytes(savestring, bytes);
                    //    }
                    //    else
                    //    {
                    //        savestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + "(1).pdf";
                    //        File.WriteAllBytes(savestring, bytes);
                    //    }
                    //}
                    //else
                    //{
                    //    var bmap = new System.Drawing.Bitmap(newimgPath);
                    //    var Tsavestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + ".jpg";
                    //    if (!System.IO.File.Exists(Tsavestring))
                    //    {
                    //        savestring = Tsavestring;
                    //        bmap.Save(savestring);
                    //    }
                    //    else
                    //    {
                    //        savestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + "(1).jpg";
                    //        bmap.Save(savestring);
                    //    }
                    //}


                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);
                            newObj.AttachmentName = System.IO.Path.GetFileName(savestring);
                            newObj.AttachmentPath = savestring;
                            //newObj.SiteId = cusEv.SiteId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            newObj.CreatedBy = userinfo.Username;
                            newObj.CreatedDate = CommonUtility.getDTNow();
                            newObj.EventId = id;
                            newObj.IsPortal = true;
                            newObj.SiteId = userinfo.SiteId;
                            newObj.CustomerId = userinfo.CustomerInfoId;
                            MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newObj, dbConnection, dbConnectionAudit, true);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Incident", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Incident", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToTask", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

         

    }

    public class MainList
    {
        public List<string> onlineuserspercentage { get; set; }
        public List<string> recentActivity { get; set; }
        public List<string> reminders { get; set; }
        public List<string> rowtoTable { get; set; }
        public List<string> assignUserTable { get; set; }
        public List<string> topOffences { get; set; }
        public List<string> ActivityReport { get; set; }
    }
}
