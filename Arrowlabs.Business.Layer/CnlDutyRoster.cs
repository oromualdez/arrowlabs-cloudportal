﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class CnlDutyRoster
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public string ChildLocation { get; set; }
        [DataMember]
        public string DutyPost { get; set; }
        [DataMember]
        public int EmployeeId { get; set; }
        [DataMember]
        public string EmployeeName { get; set; }
        [DataMember]
        public string Time { get; set; }
        [DataMember]
        public string ShiftType { get; set; }
        [DataMember]
        public string Day { get; set; }
        [DataMember]
        public int DayNumber { get; set; }
        public string UpdatedBy { get; set; }


        private static CnlDutyRoster GetCnlDutyRosterFromSqlReader(SqlDataReader reader)
        {
            var dutyRoster = new CnlDutyRoster();

            if (reader["ID"] != DBNull.Value)
                dutyRoster.Id = Convert.ToInt32(reader["ID"].ToString());
            if (reader["Location"] != DBNull.Value)
                dutyRoster.Location = reader["Location"].ToString();
            if (reader["ChildLocation"] != DBNull.Value)
                dutyRoster.ChildLocation = reader["ChildLocation"].ToString();
            if (reader["DutyPost"] != DBNull.Value)
                dutyRoster.DutyPost = reader["DutyPost"].ToString();
            if (reader["EmployeeName"] != DBNull.Value)
                dutyRoster.EmployeeName = reader["EmployeeName"].ToString();
            if (reader["Time"] != DBNull.Value)
                dutyRoster.Time = reader["Time"].ToString();
            if (reader["ShiftType"] != DBNull.Value)
                dutyRoster.ShiftType = reader["ShiftType"].ToString();
            if (reader["DayNumber"] != DBNull.Value)
                dutyRoster.DayNumber = Convert.ToInt32(reader["DayNumber"].ToString());
            if (reader["Day"] != DBNull.Value)
                dutyRoster.Day = reader["Day"].ToString();
            return dutyRoster;
        }

        /// <summary>
        /// It returns all notifications
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<CnlDutyRoster> GetAllCnlDutyRostersByEmployeeId(int employeeId, string dbConnection)
        {
            var dutyRosters = new List<CnlDutyRoster>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllDutyRostersByEmployeeId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.Int));
                sqlCommand.Parameters["@EmployeeId"].Value = employeeId;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var dutyRoster = GetCnlDutyRosterFromSqlReader(reader);
                    dutyRosters.Add(dutyRoster);
                }
                connection.Close();
                reader.Close();
            }
            return dutyRosters;
        }
    }
}
