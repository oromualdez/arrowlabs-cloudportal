﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class VehicleMake
    {
        public int ID { get; set; }
        public string VehicleMakeDesc { get; set; }
        public string VehicleMakeDesc_AR { get; set; }
        public string VehicleMakeCode { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public DatabaseOperation Operation { get; set; }
        public int CustomerId { get; set; }

        public int SiteId { get; set; }

        public static List<VehicleMake> GetAllVehicleMake(string dbConnection)
        {
            var vehicleMakes = new List<VehicleMake>();
       
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleMake", connection);

                sqlCommand.CommandText = "GetAllVehicleMake";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                
                while (reader.Read())
                {
                    var vehicleMake = new VehicleMake();

                    vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                    vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                    vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                    vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                    vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    vehicleMake.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        vehicleMake.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        vehicleMake.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId"))
                        vehicleMake.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        vehicleMake.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    vehicleMakes.Add(vehicleMake);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleMakes;
        }

        public static List<VehicleMake> GetAllVehicleMakeByDate(DateTime updatedDate, string dbConnection)
        {
            try
            {
                var vehicleMakes = new List<VehicleMake>();
                var vehicleMakeAudits = VehicleMake_Audit.GetAllVehicleMake_AuditByDate(updatedDate, dbConnection);
                DataSet dataset = new DataSet();
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("Id");
                foreach (var vehicleMakeAudit in vehicleMakeAudits)
                {
                    if (vehicleMakeAudit.Operation == "Updated" || vehicleMakeAudit.Operation == "Inserted")
                    {

                        var dr = dataTable.NewRow();
                        dr["Id"] = vehicleMakeAudit.Id;
                        dataTable.Rows.Add(dr);
                    }
                    else
                    {
                        var vehicleMake = new VehicleMake();
                        vehicleMake.ID = vehicleMakeAudit.Id;
                        vehicleMake.VehicleMakeDesc = DatabaseOperation.Unknown.ToString();
                        vehicleMake.VehicleMakeDesc_AR = DatabaseOperation.Unknown.ToString();
                        vehicleMake.VehicleMakeCode = DatabaseOperation.Unknown.ToString();
                        vehicleMake.CreatedDate = DateTime.Now;
                        vehicleMake.UpdatedDate = DateTime.Now;
                        vehicleMake.CreatedBy = DatabaseOperation.Unknown.ToString();
                        vehicleMake.Operation = DatabaseOperation.Deleted;
                        vehicleMakes.Add(vehicleMake);
                    }
                }
                if (dataTable.Rows.Count > 0)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var sqlCommand = new SqlCommand("GetAllVehicleMakeByDate", connection);
                        sqlCommand.Parameters.AddWithValue("@VehicleMakes", dataTable);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.CommandText = "GetAllVehicleMakeByDate";

                        var reader = sqlCommand.ExecuteReader();
                        while (reader.Read())
                        {
                            var vehicleMake = new VehicleMake();

                            vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                            vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                            vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                            vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                            vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                            vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                            vehicleMake.CreatedBy = reader["CreatedBy"].ToString();
                            vehicleMake.Operation = DatabaseOperation.Updated;
                            vehicleMakes.Add(vehicleMake);
                        }
                        connection.Close();
                        reader.Close();
                    }
                }
                return vehicleMakes;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public static List<VehicleMake> GetAllVehicleMakeByCId(int id,string dbConnection)
        {
            var vehicleMakes = new List<VehicleMake>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleMakeByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVehicleMakeByCId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList(); 
                while (reader.Read())
                {
                    var vehicleMake = new VehicleMake();

                    vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                    vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                    vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                    vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                    vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    vehicleMake.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains( "AccountId"))
                        vehicleMake.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("SiteId"))
                        vehicleMake.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        vehicleMake.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains( "AccountName"))
                        vehicleMake.AccountName = reader["AccountName"].ToString();

                    vehicleMakes.Add(vehicleMake);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleMakes;
        }

        public static List<VehicleMake> GetAllVehicleMakeBySiteId(int id, string dbConnection)
        {
            var vehicleMakes = new List<VehicleMake>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleMakeBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVehicleMakeBySiteId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var vehicleMake = new VehicleMake();

                    vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                    vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                    vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                    vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                    vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    vehicleMake.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("AccountId"))
                        vehicleMake.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("SiteId"))
                        vehicleMake.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        vehicleMake.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("AccountName"))
                        vehicleMake.AccountName = reader["AccountName"].ToString();

                    vehicleMakes.Add(vehicleMake);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleMakes;
        }

        public static List<VehicleMake> GetAllVehicleMakeByLevel5(int id, string dbConnection)
        {
            var vehicleMakes = new List<VehicleMake>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleMakeByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVehicleMakeByLevel5";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var vehicleMake = new VehicleMake();

                    vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                    vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                    vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                    vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                    vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    vehicleMake.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("AccountId"))
                        vehicleMake.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("SiteId"))
                        vehicleMake.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        vehicleMake.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("AccountName"))
                        vehicleMake.AccountName = reader["AccountName"].ToString();

                    vehicleMakes.Add(vehicleMake);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleMakes;
        }

        public static int InsertorUpdateVehicleMake(VehicleMake vehicleMake, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var id = 0;
            if (vehicleMake != null)
            {
                id = vehicleMake.ID;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVehicleMake", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = vehicleMake.ID;

                    cmd.Parameters.Add(new SqlParameter("@VEHICLEMAKEDESC", SqlDbType.NVarChar));
                    cmd.Parameters["@VEHICLEMAKEDESC"].Value = vehicleMake.VehicleMakeDesc;

                    cmd.Parameters.Add(new SqlParameter("@VEHICLEMAKEDESC_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@VEHICLEMAKEDESC_AR"].Value = vehicleMake.VehicleMakeDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@VEHICLEMAKECODE", SqlDbType.NVarChar));
                    cmd.Parameters["@VEHICLEMAKECODE"].Value = vehicleMake.VehicleMakeCode;


                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = vehicleMake.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = vehicleMake.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = vehicleMake.CreatedBy.ToLower();

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = vehicleMake.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = vehicleMake.SiteId;
                    
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateVehicleMake";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            vehicleMake.ID = id;
                            var audit = new VehicleMakeAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, vehicleMake.UpdatedBy);
                            Reflection.CopyProperties(vehicleMake, audit);
                            VehicleMakeAudit.InsertVehicleMakeAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateVehicleMake", ex, auditDbConnection);
                    }

                }
            }
            return id;
        }


        public static DateTime GetLastEffectiveDate(string dbConnection)
        {
            var effectiveDate = DateTime.Now;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleMake_Audit_EffectiveDate", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetVehicleMake_Audit_EffectiveDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["EffectiveDate"] != DBNull.Value)
                        effectiveDate = Convert.ToDateTime(reader["EffectiveDate"].ToString());
                }
                connection.Close();
                reader.Close();
            }
            return effectiveDate;
        }

        public static bool DeleteVehicleMakeById(int colorCode, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteVehicleMakeById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = colorCode;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteVehicleMakeById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static VehicleMake GetVehicleMakeByName(string name, string dbConnection)
        {
            VehicleMake vehicleMake = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleMakeByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    vehicleMake = new VehicleMake();
                    vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                    vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                    vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                    vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                    vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    vehicleMake.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        vehicleMake.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        vehicleMake.AccountName = reader["AccountName"].ToString();

                }
                connection.Close();
                reader.Close();
            }
            return vehicleMake;
        }

        public static VehicleMake GetVehicleMakeById(int id, string dbConnection)
        {
            VehicleMake vehicleMake = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleMakeById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    vehicleMake = new VehicleMake();
                    vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                    vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                    vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                    vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                    vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    vehicleMake.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId"))
                        vehicleMake.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("AccountName"))
                        vehicleMake.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("CustomerId"))
                        vehicleMake.CustomerId = Convert.ToInt32(reader["CustomerId"]);
                    if (columns.Contains("SiteId"))
                        vehicleMake.SiteId = Convert.ToInt32(reader["SiteId"]);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleMake;
        }

        public static VehicleMake GetVehicleMakeByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            VehicleMake vehicleMake = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleMakeByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    vehicleMake = new VehicleMake();
                    vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                    vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                    vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                    vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                    vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    vehicleMake.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        vehicleMake.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        vehicleMake.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("CustomerId"))
                        vehicleMake.CustomerId = Convert.ToInt32(reader["CustomerId"]);
                    if (columns.Contains("SiteId"))
                        vehicleMake.SiteId = Convert.ToInt32(reader["SiteId"]);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleMake;
        }
        public static VehicleMake GetVehicleMakeByNameAndCId(string name,int id ,string dbConnection)
        {
            VehicleMake vehicleMake = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleMakeByNameAndCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    vehicleMake = new VehicleMake();
                    vehicleMake.ID = Convert.ToInt32(reader["Id"].ToString());
                    vehicleMake.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
                    vehicleMake.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
                    vehicleMake.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
                    vehicleMake.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    vehicleMake.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    vehicleMake.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        vehicleMake.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        vehicleMake.AccountName = reader["AccountName"].ToString();

                }
                connection.Close();
                reader.Close();
            }
            return vehicleMake;
        }

    }

    internal class VehicleMake_Audit
    {
        public int Id { get; set; }
        public string Operation { get; set; }
        public DateTime EffectiveDate { get; set; }

        public static List<VehicleMake_Audit> GetAllVehicleMake_AuditByDate(DateTime updatedDate, string dbConnection)
        {
            var vehicleMake_Audits = new List<VehicleMake_Audit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleMake_AuditByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetAllVehicleMake_AuditByDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var vehicleMake_Audit = new VehicleMake_Audit();

                    if (reader["Id"] != DBNull.Value)
                        vehicleMake_Audit.Id = Convert.ToInt32(reader["Id"].ToString());

                    vehicleMake_Audit.Operation = reader["Operation"].ToString();
                    vehicleMake_Audit.EffectiveDate = Convert.ToDateTime(reader["EffectiveDate"].ToString());
                    vehicleMake_Audits.Add(vehicleMake_Audit);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleMake_Audits;
        }
    }
}
