﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class UserAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int ID { get; set; }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string State { get; set; }
        public string Register { get; set; }
        public int DeviceType { get; set; }
        public string imageURL { get; set; }
        public bool Engaged { get; set; }
        public string EngagedIn { get; set; }
        public int AccountId { get; set; }
        public int ImageId { get; set; }
        public string Status { get; set; }
        public int SiteId { get; set; }
        public int CustomerInfoId { get; set; }
        public string Telephone { get; set; }
        public string Gender { get; set; }
        public int DayOffUsed { get; set; } 
        public string EmployeeID { get; set; } 

        public int ContractLinkId { get; set; }

        public int CustomerLinkId { get; set; }  
        public enum UserState : int
        {
            Unknown = -1,
            UnRegistered = 0,
            Enable = 1,
            Disable = 2,
            Curfew = 3
        }
        public enum DeviceTypes : int
        {
            Unknown = -1,
            None = 0,
            Phone = 1,
            Tablet = 2,
            Both = 3
        }
        public static List<UserAudit> GetAllUserAudit(string dbConnection)
        {
            var userList = new List<UserAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserAudit", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var user = GetUserAuditFromSqlReader(reader);
                    userList.Add(user);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
        public static bool InsertUserAudit(UserAudit userAudit, string dbConnection)
        {
            try
            {
                if (userAudit != null)
                {

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertUserAudit", connection);

                        var baseAuditId = BaseAudit.InsertBaseAudit(userAudit.BaseAudit, dbConnection);
                        cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                        cmd.Parameters.Add("@Id", SqlDbType.Int).Value = userAudit.ID;

                        cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = userAudit.Username;

                        cmd.Parameters.Add("@Password", SqlDbType.NVarChar).Value = userAudit.Password;

                        cmd.Parameters.Add("@Email", SqlDbType.NVarChar).Value = userAudit.Email;

                        cmd.Parameters.Add("@RoleId", SqlDbType.Int).Value = userAudit.RoleId;

                        cmd.Parameters.Add("@Latitude", SqlDbType.Float).Value = userAudit.Latitude;

                        cmd.Parameters.Add("@Longitude", SqlDbType.Float).Value = userAudit.Longitude;

                        cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = userAudit.LastLogin;

                        cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;

                        cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = userAudit.CreatedBy;

                        cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = userAudit.FirstName;

                        cmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = userAudit.LastName;

                        cmd.Parameters.Add("@Register", SqlDbType.NVarChar).Value = userAudit.Register;

                        cmd.Parameters.Add("@DeviceType", SqlDbType.Int).Value = userAudit.DeviceType;

                        cmd.Parameters.Add("@ContractLinkId", SqlDbType.Int).Value = userAudit.ContractLinkId;

                        cmd.Parameters.Add("@CustomerLinkId", SqlDbType.Int).Value = userAudit.CustomerLinkId;
                         
                        cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = userAudit.SiteId;

                        cmd.Parameters.Add("@CustomerInfoId", SqlDbType.Int).Value = userAudit.CustomerInfoId;
                        
                        cmd.Parameters.Add("@Gender", SqlDbType.NVarChar).Value = userAudit.Gender;

                        cmd.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = userAudit.EmployeeID;

                        cmd.Parameters.Add("@Telephone", SqlDbType.NVarChar).Value = userAudit.Telephone;

                        cmd.Parameters.Add("@DayOffUsed", SqlDbType.NVarChar).Value = userAudit.DayOffUsed;
                         
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static UserAudit GetUserAuditFromSqlReader(SqlDataReader reader)
        {
            var userAudit = new UserAudit();
            userAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("ID") && reader["ID"] != DBNull.Value)
                userAudit.ID = Convert.ToInt32(reader["ID"].ToString());
            if (columns.Contains("Username") && reader["Username"] != DBNull.Value)
                userAudit.Username = reader["Username"].ToString();
            if (columns.Contains("Password") && reader["Password"] != DBNull.Value)
                userAudit.Password = reader["Password"].ToString();
            if (columns.Contains("FirstName") && reader["FirstName"] != DBNull.Value)
                userAudit.FirstName = reader["FirstName"].ToString();
            if (columns.Contains("LastName") && reader["LastName"] != DBNull.Value)
                userAudit.LastName = reader["LastName"].ToString();
            if (columns.Contains("Email") && reader["Email"] != DBNull.Value)
                userAudit.Email = reader["Email"].ToString();
            if (columns.Contains("RoleId") && reader["RoleId"] != DBNull.Value)
                userAudit.RoleId = Convert.ToInt32(reader["RoleId"].ToString());
            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                userAudit.Latitude = Convert.ToSingle(reader["Latitude"].ToString());
            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                userAudit.Longitude = Convert.ToSingle(reader["Longitude"].ToString());
            if (columns.Contains("LastLogin") && reader["LastLogin"] != DBNull.Value)
                userAudit.LastLogin = Convert.ToDateTime(reader["LastLogin"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                userAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                userAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("DeviceType") && reader["DeviceType"] != DBNull.Value)
                userAudit.DeviceType = Convert.ToInt32(reader["DeviceType"].ToString());
            if (columns.Contains("Engaged") && reader["Engaged"] != DBNull.Value)
                userAudit.Engaged = Convert.ToBoolean(reader["Engaged"].ToString());
            if (columns.Contains("EngagedIn") && reader["EngagedIn"] != DBNull.Value)
                userAudit.EngagedIn = reader["EngagedIn"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                userAudit.SiteId =Convert.ToInt32(reader["SiteId"].ToString());
            if (columns.Contains("CustomerInfoId") && reader["CustomerInfoId"] != DBNull.Value)
                userAudit.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"].ToString());
             
            if (columns.Contains("Register") && reader["Register"] != DBNull.Value)
            {
                userAudit.Register = reader["Register"].ToString();
                if (userAudit.Register == ((int)UserState.Disable).ToString())
                    userAudit.State = UserState.Disable.ToString();
                else if (userAudit.Register == ((int)UserState.Enable).ToString())
                    userAudit.State = UserState.Enable.ToString();
                else if (userAudit.Register == ((int)UserState.UnRegistered).ToString())
                    userAudit.State = UserState.UnRegistered.ToString();
                else if (userAudit.Register == ((int)UserState.Curfew).ToString())
                    userAudit.State = UserState.Curfew.ToString();
                else
                    userAudit.State = UserState.Unknown.ToString();
            }

            return userAudit;
        }


    }
}
