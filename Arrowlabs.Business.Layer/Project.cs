﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
   public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string SystypeName { get; set; }
        public int SystypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int SiteId { get; set; }
        public int UserId { get; set; }
        public string OUName { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }

        public int CustomerInfoId { get; set; }

        public string CustomerUName { get; set; }

        public string CustomerCName { get; set; }

        public static int InsertOrUpdateProject(Project project, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = project.Id;

            var action = (project.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (project != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateProject", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = project.Id;


                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = project.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = project.Name;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = project.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = project.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                    cmd.Parameters["@StartDate"].Value = project.StartDate;

                    cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                    cmd.Parameters["@EndDate"].Value = project.EndDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = project.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = project.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = project.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SystypeId", SqlDbType.NVarChar));
                    cmd.Parameters["@SystypeId"].Value = project.SystypeId;

                    
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = project.SiteId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = project.UserId;
                    
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateProject";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            project.Id = id;
                            var audit = new ProjectAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, project.UpdatedBy);
                            Reflection.CopyProperties(project, audit);
                            ProjectAudit.InsertProjectAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertProjectAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static Project GetProjectFromSqlReader(SqlDataReader reader)
        {
            var project = new Project();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                project.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["Name"] != DBNull.Value)
                project.Name = reader["Name"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                project.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["UpdatedDate"] != DBNull.Value)
                project.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                project.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedBy"] != DBNull.Value)
                project.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["CustomerId"] != DBNull.Value)
                project.CustomerId =  Convert.ToInt32(reader["CustomerId"].ToString());

            if (reader["SystypeId"] != DBNull.Value)
                project.SystypeId = Convert.ToInt32(reader["SystypeId"].ToString());

            if (reader["StartDate"] != DBNull.Value)
                project.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());
           
            if (reader["EndDate"] != DBNull.Value)
                project.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());

            if (reader["SiteId"] != DBNull.Value)
                project.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                project.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("CustomerCName") && reader["CustomerCName"] != DBNull.Value)
                project.CustomerCName = reader["CustomerCName"].ToString();
            

            project.SystypeName = "N/A";
            project.CustomerName = "N/A";
            project.OUName = "N/A";

            if (columns.Contains( "CustomerName") && reader["CustomerName"] != DBNull.Value)
                project.CustomerName = reader["CustomerName"].ToString();

            if (columns.Contains( "UserId") && reader["UserId"] != DBNull.Value)
                project.UserId = Convert.ToInt32(reader["UserId"].ToString());

            if (columns.Contains( "CustomerInfoId") && reader["CustomerInfoId"] != DBNull.Value)
                            project.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"].ToString());
            

                        if (columns.Contains( "FName") && reader["FName"] != DBNull.Value)
                project.FName = reader["FName"].ToString();

                        if (columns.Contains( "LName") && reader["LName"] != DBNull.Value)
                project.LName = reader["LName"].ToString(); 

            if (columns.Contains( "SystypeName") && reader["SystypeName"] != DBNull.Value)
                project.SystypeName = reader["SystypeName"].ToString();

            if (columns.Contains( "OUName") && reader["OUName"] != DBNull.Value)
                project.OUName = reader["OUName"].ToString();
            
           
            return project;
        }
        public static List<Project> GetProjectByCustomerId(int id, string dbConnection)
        {
            var projects = new List<Project>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetProjectByCustomerId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var project = GetProjectFromSqlReader(reader);
                    projects.Add(project);
                }
                connection.Close();
                reader.Close();
            }
            return projects;
        }
        public static Project GetProjectByName(string name, string dbConnection)
        {
            Project project = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetProjectByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    project = GetProjectFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return project;
        }
        public static Project GetProjectById(int id, string dbConnection)
        {
            Project project = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetProjectById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    project = GetProjectFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return project;
        }
        public static List<Project> GetAllProject(string dbConnection)
        {
            var projects = new List<Project>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllProject";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var project = GetProjectFromSqlReader(reader);
                    projects.Add(project);
                }
                connection.Close();
                reader.Close();
            }
            return projects;
        }

        public static List<Project> GetAllProjectsBySiteId(int siteId, string dbConnection)
        {
            var projects = new List<Project>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllProjectsBySiteId";
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var project = GetProjectFromSqlReader(reader);
                    projects.Add(project);
                }
                connection.Close();
                reader.Close();
            }
            return projects;
        }

        public static List<Project> GetAllProjectsByLevel5(int siteId, string dbConnection)
        {
            var projects = new List<Project>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllProjectsByLevel5";
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var project = GetProjectFromSqlReader(reader);
                    projects.Add(project);
                }
                connection.Close();
                reader.Close();
            }
            return projects;
        }

        public static List<Project> GetAllProjectByCustomerInfoId(int customerInfoId, string dbConnection)
        {
            var projects = new List<Project>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllProjectByCustomerInfoId";
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerInfoId"].Value = customerInfoId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var project = GetProjectFromSqlReader(reader);
                    projects.Add(project);
                }
                connection.Close();
                reader.Close();
            }
            return projects;
        }

        public static void DeleteProjectById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteProjectById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteProjectById";

                cmd.ExecuteNonQuery();
            }
        }
    }
}
