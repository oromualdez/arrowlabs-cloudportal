﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ContractInfoAduit
    {
        public int Id { get; set; }
        public int SystemTypeId { get; set; }
        public string ContractRef { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int PPMVisitPerYear { get; set; }
        public int PPMTotalHour { get; set; }
        public int TotalPPMHour { get; set; }
        public int SRVisitPerYear { get; set; }
        public int SRHourVisitPer { get; set; }
        public int SRTotalHours { get; set; }
        public int SRTotal { get; set; }
        public string ScopeOfWorks { get; set; }
        public string SLA { get; set; }
        public string AnnualContractValue { get; set; }
        public string TotalContractValue { get; set; }
        public string MonthlyContractValue { get; set; }
        public string Remarks { get; set; }
        public int BaseAuditId { get; set; }
        public string PaymentTerms { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int SiteId { get; set; }
        public int ContractType { get; set; }
        public BaseAudit BaseAudit { get; set; }
        public bool Active{ get; set; }
        public int CustomerInfoId { get; set; }

        public static int InsertContractInfoAudit(ContractInfoAduit contractInfoAduit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(contractInfoAduit.BaseAudit, dbConnection);

            int id = 0;
            if (contractInfoAduit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertContractInfoAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = contractInfoAduit.Id;

                    cmd.Parameters.Add(new SqlParameter("@SystemTypeId", SqlDbType.Int));
                    cmd.Parameters["@SystemTypeId"].Value = contractInfoAduit.SystemTypeId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = contractInfoAduit.CustomerInfoId;
                    
                    cmd.Parameters.Add(new SqlParameter("@ContractRef", SqlDbType.NVarChar));
                    cmd.Parameters["@ContractRef"].Value = contractInfoAduit.ContractRef;

                    cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                    cmd.Parameters["@StartDate"].Value = contractInfoAduit.StartDate;

                    cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                    cmd.Parameters["@EndDate"].Value = contractInfoAduit.EndDate;

                    cmd.Parameters.Add(new SqlParameter("@PPMVisitPerYear", SqlDbType.Int));
                    cmd.Parameters["@PPMVisitPerYear"].Value = contractInfoAduit.PPMVisitPerYear;

                    cmd.Parameters.Add(new SqlParameter("@PPMTotalHour", SqlDbType.Int));
                    cmd.Parameters["@PPMTotalHour"].Value = contractInfoAduit.PPMTotalHour;

                    cmd.Parameters.Add(new SqlParameter("@TotalPPMHour", SqlDbType.Int));
                    cmd.Parameters["@TotalPPMHour"].Value = contractInfoAduit.TotalPPMHour;

                    cmd.Parameters.Add(new SqlParameter("@SRVisitPerYear", SqlDbType.Int));
                    cmd.Parameters["@SRVisitPerYear"].Value = contractInfoAduit.SRVisitPerYear;

                    cmd.Parameters.Add(new SqlParameter("@SRHourVisitPer", SqlDbType.Int));
                    cmd.Parameters["@SRHourVisitPer"].Value = contractInfoAduit.SRHourVisitPer;

                    cmd.Parameters.Add(new SqlParameter("@SRTotalHours", SqlDbType.Int));
                    cmd.Parameters["@SRTotalHours"].Value = contractInfoAduit.SRTotalHours;

                    cmd.Parameters.Add(new SqlParameter("@SRTotal", SqlDbType.Int));
                    cmd.Parameters["@SRTotal"].Value = contractInfoAduit.SRTotal;

                    cmd.Parameters.Add(new SqlParameter("@ScopeOfWorks", SqlDbType.NVarChar));
                    cmd.Parameters["@ScopeOfWorks"].Value = contractInfoAduit.ScopeOfWorks;

                    cmd.Parameters.Add(new SqlParameter("@SLA", SqlDbType.NVarChar));
                    cmd.Parameters["@SLA"].Value = contractInfoAduit.SLA;

                    cmd.Parameters.Add(new SqlParameter("@AnnualContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@AnnualContractValue"].Value = contractInfoAduit.AnnualContractValue;

                    cmd.Parameters.Add(new SqlParameter("@TotalContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@TotalContractValue"].Value = contractInfoAduit.TotalContractValue;

                    cmd.Parameters.Add(new SqlParameter("@MonthlyContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@MonthlyContractValue"].Value = contractInfoAduit.MonthlyContractValue;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = contractInfoAduit.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@PaymentTerms", SqlDbType.NVarChar));
                    cmd.Parameters["@PaymentTerms"].Value = contractInfoAduit.PaymentTerms;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = contractInfoAduit.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = contractInfoAduit.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = contractInfoAduit.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = contractInfoAduit.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = contractInfoAduit.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@ContractType", SqlDbType.NVarChar));
                    cmd.Parameters["@ContractType"].Value = contractInfoAduit.ContractType;
                    cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit));
                    cmd.Parameters["@Active"].Value = contractInfoAduit.Active;
                    
                    
                    cmd.CommandText = "InsertContractInfoAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                }
            }
            return id;
        }
    }
}