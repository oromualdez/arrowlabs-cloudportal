﻿using Arrowlabs.Business.Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ArrowLabs.Licence.Portal.Helpers; 

namespace ArrowLabs.Licence.Portal.Controls.Modals
{
    public partial class NewTask : System.Web.UI.UserControl
    {
        protected string grpOptionView;
        protected string customersDisplay;
        protected string tskOptionView;

        protected string namePlaceholder;
        protected string descPlaceholder;
        protected string longiPlaceholder;
        protected string latiPlaceholder;
        protected string assigntomyselfPlaceholder;
        protected string sendPlaceholder;
        protected string saveastemplatePlaceholder;

        protected string severePlaceholder;
        protected string highPlaceholder;
        protected string mediumPlaceholder;
        protected string lowPlaceholder;

        protected string userPlaceholder;
        protected string groupPlaceholder;
        protected string multiplePlaceholder;

        protected string taskTypePlaceholder;
        protected string selectChecklistPlaceholder;
        protected string selectAssigneePlaceholder;
        protected string selectLocationPlaceholder;

        protected string requiresignaturePlaceholder;

        protected string selectAccountPlaceholder;
        protected string selectContractPlaceholder;
        protected string selectProjectPlaceholder; 

        protected void Page_Load(object sender, EventArgs e)
        {
            tskOptionView = "none";
            grpOptionView = "none";
            customersDisplay = "none";
            var dbConnection = CommonUtility.dbConnection;
            var user = HttpContext.Current.User.Identity;
            if (user != null)
            {
                var userinfo = Users.GetUserByName(user.Name, dbConnection);

                 namePlaceholder = "Name";
                 descPlaceholder = "Description";
                 longiPlaceholder = "Longitude";
                 latiPlaceholder = "Latitude";
                 assigntomyselfPlaceholder = "Assign to myself";
                 sendPlaceholder = "SEND";
                 saveastemplatePlaceholder = "SAVE AS TEMPLATE";
                 severePlaceholder = "Severe";
                 highPlaceholder = "High";
                 mediumPlaceholder = "Medium";
                 lowPlaceholder = "Low";

                 userPlaceholder = "User";
                 groupPlaceholder = "Group";
                 multiplePlaceholder = "Multiple";

                 taskTypePlaceholder = "Task Type";
                 selectChecklistPlaceholder = "Select Checklist";
                 selectAssigneePlaceholder = "Select Assignee";
                 selectLocationPlaceholder = "Select Location";

                 requiresignaturePlaceholder = "REQUIRES SIGNATURE";

                 selectAccountPlaceholder = "Select Account";
                 selectContractPlaceholder = "Select Contract";

                 selectProjectPlaceholder = "Select Project";

                 //if (userinfo.ID == 479)
                 //{
                 //    selectAccountPlaceholder = "هذا مثال على ذلك";
                 //    selectContractPlaceholder = "هذا مثال على ذلك";
                 //    selectProjectPlaceholder = "هذا مثال على ذلك";

                 //    namePlaceholder = "هذا مثال على ذلك";
                 //    descPlaceholder = "هذا مثال على ذلك";
                 //    longiPlaceholder = "هذا مثال على ذلك";
                 //    latiPlaceholder = "هذا مثال على ذلك";
                 //    assigntomyselfPlaceholder = "هذا مثال على ذلك";
                 //    sendPlaceholder = "هذا مثال على ذلك";
                 //    saveastemplatePlaceholder = "هذا مثال على ذلك";
                 //    severePlaceholder = "هذا مثال على ذلك";
                 //    highPlaceholder = "هذا مثال على ذلك";
                 //    mediumPlaceholder = "هذا مثال على ذلك";
                 //    lowPlaceholder = "هذا مثال على ذلك";

                 //    userPlaceholder = "هذا مثال على ذلك";
                 //    groupPlaceholder = "هذا مثال على ذلك";
                 //    multiplePlaceholder = "هذا مثال على ذلك";

                 //    taskTypePlaceholder = "هذا مثال على ذلك";

                 //    selectChecklistPlaceholder = "هذا مثال على ذلك";

                 //    selectAssigneePlaceholder = "هذا مثال على ذلك";
                 //    selectLocationPlaceholder = "هذا مثال على ذلك";

                 //    requiresignaturePlaceholder = "هذا مثال على ذلك";
                 //}
                //479
                 if (userinfo.RoleId != (int)Role.SuperAdmin)
                 {
                     var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                     foreach (var mods in modules)
                     {
                         if (mods.ModuleId == (int)Accounts.ModuleTypes.Collaboration)
                         {
                             grpOptionView = "block";
                         }
                         if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks)
                         {

                             tskOptionView = "block";
                         }
                         if (mods.ModuleId == (int)Accounts.ModuleTypes.Customer)
                         {

                             customersDisplay = "block";
                         }
                     }
                 }
                 else
                 {
                     tskOptionView = "block";
                     grpOptionView = "block";
                     customersDisplay = "block";
                 }
                var startlocations = new List<Location>();
                var locations = new List<Location>();
                var tasktypes = new List<TaskCategory>();

                var tskType = new TaskCategory();
                tskType.CategoryId = 0;
                tskType.Description = taskTypePlaceholder;
                tasktypes.Add(tskType);

                var gtTypes = TaskCategory.GetAllTaskCategories(dbConnection);
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        gtTypes = TaskCategory.GetAllTaskCategoriesByCId(userinfo.CustomerInfoId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        gtTypes = TaskCategory.GetAllTaskCategoriesByLevel5(userinfo.ID, dbConnection);
                    }
                    else
                    {
                        gtTypes = TaskCategory.GetAllTaskCategoriesBySiteId(userinfo.SiteId, dbConnection);
                    }
                }
                else
                {
                    gtTypes = TaskCategory.GetAllTaskCategories(dbConnection);
                }
                tasktypes.AddRange(gtTypes);

                taskTypeSelect.DataTextField = "Description";
                taskTypeSelect.DataValueField = "CategoryId";
                taskTypeSelect.DataSource = tasktypes;
                taskTypeSelect.DataBind();

                var usersearchSelectItems = new List<SearchSelectItem>();
                var groupsearchSelectItems = new List<SearchSelectItem>();

                var allusers = new List<Users>(); 

                var allgroups = new List<Group>();

                var tasktemplateList = new List<TemplateCheckList>();
                var finaltasktemplateList = new List<TemplateCheckList>();
                var TemplateCheck = new TemplateCheckList();
                TemplateCheck.Name = selectChecklistPlaceholder;
                TemplateCheck.Id = -1; 
                finaltasktemplateList.Add(TemplateCheck);
                 
                 
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    locations.AddRange(Location.GetAllLocation(dbConnection));
                    locations.AddRange(Location.GetAllSubLocation(dbConnection));

                    allusers = Users.GetAllUsers(dbConnection);
                    allgroups = Group.GetAllGroup(dbConnection);

                    if (userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                        allgroups = allgroups.Where(i => i.CreatedBy == userinfo.Username).ToList();
                    }
                }
                else if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                {
                    locations.AddRange(Location.GetAllLocationBySiteId(userinfo.SiteId, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationBySiteId(userinfo.SiteId, dbConnection));
                    tasktemplateList.AddRange(TemplateCheckList.GetAllTemplateCheckListBySiteId(userinfo.SiteId, dbConnection));
                    if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var allmanagers = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                        allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                        foreach (var usermanager in allmanagers)
                        {
                            allusers.Add(usermanager);
                            var allmanagerusers = Users.GetAllFullUsersByManagerIdForDirector(usermanager.ID, dbConnection);
                            allusers.AddRange(allmanagerusers);
                        }
                        var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                        unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                        foreach (var subsubuser in unassigned)
                        {
                            allusers.Add(subsubuser);
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Manager)
                    {
                        allusers.AddRange(Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection));
                        allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        allusers.AddRange(Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection));
                        allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    locations.AddRange(Location.GetAllMainLocationByLevel5(userinfo.ID, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationByLevel5(userinfo.ID, dbConnection));

                    tasktemplateList.AddRange(TemplateCheckList.GetAllTemplateCheckListByLevel5(userinfo.ID, dbConnection));

                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection)); 
                    allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser)
                {
                    locations.AddRange(Location.GetAllMainLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));

                    tasktemplateList.AddRange(TemplateCheckList.GetAllTemplateCheckListByCId(userinfo.CustomerInfoId, dbConnection));

                    allusers.AddRange(Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection));
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                }
                allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                var grouped3 = allusers.GroupBy(item => item.ID);
                allusers = grouped3.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();


                var newSearchSelectItem2 = new SearchSelectItem();
                newSearchSelectItem2.ID = "0";
                newSearchSelectItem2.Name = selectAssigneePlaceholder;
                usersearchSelectItems.Add(newSearchSelectItem2);
                groupsearchSelectItems.Add(newSearchSelectItem2);

                if (userinfo.RoleId == (int)Role.Director)
                {
                    foreach (var searchSelectUser in allusers)
                    {
                        if (searchSelectUser.RoleId != (int)Role.Director)
                        {
                            var newSearchSelectItem = new SearchSelectItem();
                            newSearchSelectItem.ID = searchSelectUser.ID.ToString();
                            newSearchSelectItem.Name = searchSelectUser.CustomerUName;
                            usersearchSelectItems.Add(newSearchSelectItem);
                        }
                    }
                }
                else
                {
                    foreach (var searchSelectUser in allusers)
                    {
                        if (searchSelectUser.Username != userinfo.Username)
                        {
                            var newSearchSelectItem = new SearchSelectItem();
                            newSearchSelectItem.ID = searchSelectUser.ID.ToString();
                            newSearchSelectItem.Name = searchSelectUser.CustomerUName;
                            usersearchSelectItems.Add(newSearchSelectItem);
                        }
                    }
                }
                foreach (var searchSelectGroup in allgroups)
                {
                    var newSearchSelectItem = new SearchSelectItem();
                    newSearchSelectItem.ID = searchSelectGroup.Id.ToString();
                    newSearchSelectItem.Name = searchSelectGroup.Name;
                    groupsearchSelectItems.Add(newSearchSelectItem);
                }

                var grouped2 = tasktemplateList.GroupBy(item => item.Id);
                tasktemplateList = grouped2.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                tasktemplateList = tasktemplateList.OrderBy(item => item.Name).ToList();

                finaltasktemplateList.AddRange(tasktemplateList);

                var grouped = locations.GroupBy(item => item.ID);
                locations = grouped.Select(grp => grp.OrderBy(item => item.LocationDesc).First()).ToList();
                locations = locations.OrderBy(i => i.LocationDesc).ToList();

                var glocations = new List<Location>();
                var loc = new Location();
                loc.ID = 0;
                loc.LocationDesc = selectLocationPlaceholder;
                loc.LocationDesc_AR = selectLocationPlaceholder;
                loc.CreatedDate = CommonUtility.getDTNow();
                loc.CreatedBy = userinfo.Username;
                glocations.Add(loc);
                glocations.AddRange(locations);

                startlocations.AddRange(glocations.Where(i=>i.LocationTypeId != 2).ToList());

                locationSelect.DataTextField = "LOCATIONDESC";
                locationSelect.DataValueField = "Id";
                locationSelect.DataSource = startlocations;
                locationSelect.DataBind();

                groupsearchSelect.DataTextField = "Name";
                groupsearchSelect.DataValueField = "ID";
                groupsearchSelect.DataSource = groupsearchSelectItems;
                groupsearchSelect.DataBind();

                checklistSelect.DataTextField = "Name";
                checklistSelect.DataValueField = "Id";
                checklistSelect.DataSource = finaltasktemplateList;
                checklistSelect.DataBind();

                usersearchSelect.DataTextField = "Name";
                usersearchSelect.DataValueField = "ID";
                usersearchSelect.DataSource = usersearchSelectItems;
                usersearchSelect.DataBind();
            }
        }
    }
}