﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class DutyRosterAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Location { get; set; }

        public string ChildLocation { get; set; }

        public string DutyPost { get; set; }
        public int SN { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }

        public string Time { get; set; }

        public string ShiftType { get; set; }

        public string Day { get; set; }
        public int DayNumber { get; set; }

        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }
        private static DutyRosterAudit GetDutyRosterAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var dutyRoster = new DutyRosterAudit();
            dutyRoster.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            //if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
            //    dutyRoster.Id = Convert.ToInt32(reader["Id"].ToString());
            //if (columns.Contains("Description") && reader["Description"] != DBNull.Value)
            //    dutyRoster.Description = reader["Description"].ToString();
            //if (columns.Contains("UserId") && reader["UserId"] != DBNull.Value)
            //    dutyRoster.UserId = (String.IsNullOrEmpty(reader["UserId"].ToString())) ? 0 : Convert.ToInt32(reader["UserId"].ToString());
            //if (columns.Contains("LocationId") && reader["LocationId"] != DBNull.Value)
            //    dutyRoster.LocationId = Convert.ToInt32(reader["LocationId"].ToString());
            //if (columns.Contains("SubLocationId") && reader["SubLocationId"] != DBNull.Value)
            //    dutyRoster.SubLocationId = Convert.ToInt32(reader["SubLocationId"].ToString());
            //if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
            //    dutyRoster.IsDeleted = (String.IsNullOrEmpty(reader["IsDeleted"].ToString())) ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
            //if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
            //    dutyRoster.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            //if (columns.Contains("ShiftStartTime") && reader["ShiftStartTime"] != DBNull.Value)
            //    dutyRoster.ShiftStartTime = Convert.ToDateTime(reader["ShiftStartTime"].ToString());
            //if (columns.Contains("ShiftEndTime") && reader["ShiftEndTime"] != DBNull.Value)
            //    dutyRoster.ShiftEndTime = Convert.ToDateTime(reader["ShiftEndTime"].ToString());
            //if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
            //    dutyRoster.CreatedBy = reader["CreatedBy"].ToString();
            //dutyRoster.SubLocationName = "All";
            //if (columns.Contains("LocationName") && reader["LocationName"] != DBNull.Value)
            //    dutyRoster.LocationName = (String.IsNullOrEmpty(reader["LocationName"].ToString())) ? "All" : reader["LocationName"].ToString();
            //if (columns.Contains("SubLocationName") && reader["SubLocationName"] != DBNull.Value)
            //    dutyRoster.SubLocationName = (String.IsNullOrEmpty(reader["SubLocationName"].ToString())) ? "All" : reader["SubLocationName"].ToString();
            //if (columns.Contains("Username") && reader["Username"] != DBNull.Value)
            //    dutyRoster.Username = (String.IsNullOrEmpty(reader["Username"].ToString())) ? "All" : reader["Username"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                dutyRoster.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            

            return dutyRoster;
        }

        /// <summary>
        /// It returns all duty roasters
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<DutyRosterAudit> GetAllDutyRosterAudits(string dbConnection)
        {
            var dutyRosters = new List<DutyRosterAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllDutyRosterAudits", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var dutyRoaster = GetDutyRosterAuditFromSqlReader(reader);
                    dutyRosters.Add(dutyRoaster);
                }
                connection.Close();
                reader.Close();
            }
            return dutyRosters;
        }

        public static bool InsertDutyRosterAudit(DutyRosterAudit customEvent, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(customEvent.BaseAudit, dbConnection);

            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertDutyRosterAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customEvent.Id;

                    cmd.Parameters.Add(new SqlParameter("@ParentID", SqlDbType.Int));
                    cmd.Parameters["@ParentID"].Value = customEvent.ParentId;

                    cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.NVarChar));
                    cmd.Parameters["@Location"].Value = customEvent.Location;

                    cmd.Parameters.Add(new SqlParameter("@ChildLocation", SqlDbType.NVarChar));
                    cmd.Parameters["@ChildLocation"].Value = customEvent.ChildLocation;

                    cmd.Parameters.Add(new SqlParameter("@DutyPost", SqlDbType.NVarChar));
                    cmd.Parameters["@DutyPost"].Value = customEvent.DutyPost;

                    cmd.Parameters.Add(new SqlParameter("@SN", SqlDbType.Int));
                    cmd.Parameters["@SN"].Value = customEvent.SN;

                    cmd.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.Int));
                    cmd.Parameters["@EmployeeId"].Value = customEvent.EmployeeId;

                    cmd.Parameters.Add(new SqlParameter("@EmployeeName", SqlDbType.NVarChar));
                    cmd.Parameters["@EmployeeName"].Value = customEvent.EmployeeName;

                    cmd.Parameters.Add(new SqlParameter("@Time", SqlDbType.NVarChar));
                    cmd.Parameters["@Time"].Value = customEvent.Time;

                    cmd.Parameters.Add(new SqlParameter("@ShiftType", SqlDbType.NVarChar));
                    cmd.Parameters["@ShiftType"].Value = customEvent.ShiftType;

                    cmd.Parameters.Add(new SqlParameter("@Day", SqlDbType.NVarChar));
                    cmd.Parameters["@Day"].Value = customEvent.Day;

                    cmd.Parameters.Add(new SqlParameter("@DayNumber", SqlDbType.Int));
                    cmd.Parameters["@DayNumber"].Value = customEvent.DayNumber;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customEvent.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = customEvent.CustomerId;


                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customEvent.CreatedBy;


                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customEvent.CreatedDate;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
