﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
   public class CustomerAudit
    {
        public int Id { get; set; }
        public int BaseAuditId { get; set; }
        public bool Status { get; set; }
        public string LocationAddress { get; set; }
        public string ContractRef { get; set; }
        public string ClientName { get; set; }
        public string SalesAccount { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int PPMVisitPerYear { get; set; }
        public int PPMTotalHour { get; set; }
        public int TotalPPMHour { get; set; }
        public int SRVisitPerYear { get; set; }
        public int SRHourVisitPer { get; set; }
        public int SRTotalHours { get; set; }
        public int SRTotal { get; set; }
        public string ScopeOfWorks { get; set; }
        public string SLA { get; set; }
        public string AnnualContractValue { get; set; }
        public string TotalContractValue { get; set; }
        public string MonthlyContractValue { get; set; }
        public string Remarks { get; set; }
        public string PaymentTerms { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string CustomerType { get; set; }
        public int SiteId { get; set; }
        public BaseAudit BaseAudit { get; set; }
        public int CustomerInfoId { get; set; }

        public static int InsertCustomerAudit(CustomerAudit customerAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(customerAudit.BaseAudit, dbConnection);

            int id = 0;
            if (customerAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomerAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customerAudit.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = customerAudit.CustomerInfoId;
                     
                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Bit));
                    cmd.Parameters["@Status"].Value = customerAudit.Status;

                    cmd.Parameters.Add(new SqlParameter("@LocationAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationAddress"].Value = customerAudit.LocationAddress;

                    cmd.Parameters.Add(new SqlParameter("@ContractRef", SqlDbType.NVarChar));
                    cmd.Parameters["@ContractRef"].Value = customerAudit.ContractRef;

                    cmd.Parameters.Add(new SqlParameter("@ClientName", SqlDbType.NVarChar));
                    cmd.Parameters["@ClientName"].Value = customerAudit.ClientName;

                    cmd.Parameters.Add(new SqlParameter("@SalesAccount", SqlDbType.NVarChar));
                    cmd.Parameters["@SalesAccount"].Value = customerAudit.SalesAccount;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customerAudit.Name;

                    cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                    cmd.Parameters["@Email"].Value = customerAudit.Email;

                    cmd.Parameters.Add(new SqlParameter("@ContactNo", SqlDbType.NVarChar));
                    cmd.Parameters["@ContactNo"].Value = customerAudit.ContactNo;

                    cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                    cmd.Parameters["@StartDate"].Value = customerAudit.StartDate;

                    cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                    cmd.Parameters["@EndDate"].Value = customerAudit.EndDate;

                    cmd.Parameters.Add(new SqlParameter("@PPMVisitPerYear", SqlDbType.Int));
                    cmd.Parameters["@PPMVisitPerYear"].Value = customerAudit.PPMVisitPerYear;

                    cmd.Parameters.Add(new SqlParameter("@PPMTotalHour", SqlDbType.Int));
                    cmd.Parameters["@PPMTotalHour"].Value = customerAudit.PPMTotalHour;

                    cmd.Parameters.Add(new SqlParameter("@TotalPPMHour", SqlDbType.Int));
                    cmd.Parameters["@TotalPPMHour"].Value = customerAudit.TotalPPMHour;

                    cmd.Parameters.Add(new SqlParameter("@SRVisitPerYear", SqlDbType.Int));
                    cmd.Parameters["@SRVisitPerYear"].Value = customerAudit.SRVisitPerYear;

                    cmd.Parameters.Add(new SqlParameter("@SRHourVisitPer", SqlDbType.Int));
                    cmd.Parameters["@SRHourVisitPer"].Value = customerAudit.SRHourVisitPer;

                    cmd.Parameters.Add(new SqlParameter("@SRTotalHours", SqlDbType.Int));
                    cmd.Parameters["@SRTotalHours"].Value = customerAudit.SRTotalHours;

                    cmd.Parameters.Add(new SqlParameter("@SRTotal", SqlDbType.Int));
                    cmd.Parameters["@SRTotal"].Value = customerAudit.SRTotal;

                    cmd.Parameters.Add(new SqlParameter("@ScopeOfWorks", SqlDbType.NVarChar));
                    cmd.Parameters["@ScopeOfWorks"].Value = customerAudit.ScopeOfWorks;

                    cmd.Parameters.Add(new SqlParameter("@SLA", SqlDbType.NVarChar));
                    cmd.Parameters["@SLA"].Value = customerAudit.SLA;

                    cmd.Parameters.Add(new SqlParameter("@AnnualContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@AnnualContractValue"].Value = customerAudit.AnnualContractValue;

                    cmd.Parameters.Add(new SqlParameter("@TotalContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@TotalContractValue"].Value = customerAudit.TotalContractValue;

                    cmd.Parameters.Add(new SqlParameter("@MonthlyContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@MonthlyContractValue"].Value = customerAudit.MonthlyContractValue;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = customerAudit.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@PaymentTerms", SqlDbType.NVarChar));
                    cmd.Parameters["@PaymentTerms"].Value = customerAudit.PaymentTerms;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customerAudit.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = customerAudit.UpdatedDate;


                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customerAudit.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customerAudit.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customerAudit.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerType", SqlDbType.NVarChar));
                    cmd.Parameters["@CustomerType"].Value = customerAudit.CustomerType;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertCustomerAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }

    }
}
