﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class  AssetCategoryAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public int CustomerId { get; set; }

        private static AssetCategoryAudit GetAssetCategoryAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var assetCategory = new AssetCategoryAudit();
            assetCategory.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                assetCategory.Id = Convert.ToInt32(reader["Id"].ToString());

            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                assetCategory.Name = reader["Name"].ToString();

            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                assetCategory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                assetCategory.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                assetCategory.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                assetCategory.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
            return assetCategory;
        }
        public static List<AssetCategoryAudit> GetAllAssetCategoryAudit(string dbConnection)
        {
            var assetCategoryList = new List<AssetCategoryAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAssetCategoryAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var assetCategoryAudit = GetAssetCategoryAuditFromSqlReader(reader);
                    assetCategoryList.Add(assetCategoryAudit);
                }
                connection.Close();
                reader.Close();
            }
            return assetCategoryList;
        }
        public static bool InsertAssetCategoryAudit(AssetCategoryAudit assetCategory, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(assetCategory.BaseAudit, dbConnection);

            if (assetCategory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertAssetCategoryAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = assetCategory.Id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = assetCategory.Name;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = assetCategory.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = assetCategory.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = assetCategory.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = assetCategory.CustomerId;
                     
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
