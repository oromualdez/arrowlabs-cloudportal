﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;


namespace Arrowlabs.Business.Layer
{
    public class UserAnswerSurvey
    {
        public int Id { get; set; }
        public int SurveyId { get; set; }
        public int UserId { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CustomerUName { get; set; }
        public int Answer { get; set; }

        private static UserAnswerSurvey GetUserAnswerSurveyFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();


            var notification = new UserAnswerSurvey();

            if (reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["UserId"] != DBNull.Value)
                notification.UserId = Convert.ToInt32(reader["UserId"].ToString());

            if (reader["SurveyId"] != DBNull.Value)
                notification.SurveyId = Convert.ToInt32(reader["SurveyId"].ToString());

            if (reader["Answer"] != DBNull.Value)
                notification.Answer = Convert.ToInt32(reader["Answer"].ToString());
             
            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                notification.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                notification.CustomerUName = reader["CustomerUName"].ToString();


            return notification;
        }

        public static int InsertorUpdateUserAnswerSurvey(UserAnswerSurvey notification, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateUserAnswerSurvey", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;
                     
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;
                     
                    cmd.Parameters.Add(new SqlParameter("@SurveyId", SqlDbType.Int));
                    cmd.Parameters["@SurveyId"].Value = notification.SurveyId;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = notification.UserId;

                    cmd.Parameters.Add(new SqlParameter("@Answer", SqlDbType.Int));
                    cmd.Parameters["@Answer"].Value = notification.Answer;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateUserAnswerSurvey";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            notification.Id = id;
                            var audit = new UserAnswerSurveyAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, notification.CreatedBy);
                            Reflection.CopyProperties(notification, audit);
                            UserAnswerSurveyAudit.InsertUserAnswerSurveyAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertUserAnswerSurveyAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static List<UserAnswerSurvey> GetAllUserAnswerSurveyBySurveyIdAndUserId(int siteId,int userId, string dbConnection)
        {
            var itemFounds = new List<UserAnswerSurvey>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllUserAnswerSurveyBySurveyIdAndUserId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SurveyId", SqlDbType.Int));
                    sqlCommand.Parameters["@SurveyId"].Value = siteId;
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = userId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetUserAnswerSurveyFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("UserAnswerSurvey", "GetAllUserAnswerSurveyBySurveyIdAndUserId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<UserAnswerSurvey> GetAllUserAnswerSurveyBySurveyId(int siteId, string dbConnection)
        {
            var itemFounds = new List<UserAnswerSurvey>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllUserAnswerSurveyBySurveyId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SurveyId", SqlDbType.Int));
                    sqlCommand.Parameters["@SurveyId"].Value = siteId; 
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetUserAnswerSurveyFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("UserAnswerSurvey", "GetAllUserAnswerSurveyBySurveyId", exp, dbConnection);

            }
            return itemFounds;
        }
    }
}
