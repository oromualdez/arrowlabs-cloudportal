﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class UserModulesAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int UserId { get; set; }
        public int ModuleId { get; set; }
        public string AccountName { get; set; }
        public string UserName { get; set; }
        public int SiteId { get; set; }

        public static bool InsertUserModuleMap(int UserId, int ModuleId, string dbConnection, string username, BaseAudit baseAudit)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(baseAudit, dbConnection);

            if (UserId > 0 && ModuleId > 0)
            {


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertUserModuleMap", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = UserId;

                    cmd.Parameters.Add(new SqlParameter("@ModuleId", SqlDbType.Int));
                    cmd.Parameters["@ModuleId"].Value = ModuleId;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = username;              
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertUserModuleMap";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
