﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddOnTaskCard.ascx.cs" Inherits="ArrowLabs.Licence.Portal.Controls.Modals.AddOnTaskCard" %>
<script>

</script>        
<div aria-hidden="true" aria-labelledby="taskDocument" role="dialog" tabindex="-1" id="taskDocument" class="modal fade videoModal" style="display: none;">   
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="taskheaderImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="taskincidentNameHeader"></h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" data-target="#ticketingViewCard" onclick="rowchoice(document.getElementById('rowidChoiceTicket').value)" data-toggle="modal" class="close" type="button" ><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Status: <span id="taskstatusSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created by: <span id="taskusernameSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Assigned to: <span id="tasktypeSpan"></span></p>
						</div>		
                                   <div class="col-md-3">
							<p>Task Type: <span id="ttypeSpan"></span></p>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Location: <span id="tasklocSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created on: <span id="tasktimeSpan"></span></p>
						</div>	
                         <div class="col-md-3">
							<p>Assigned on: <span id="assignedTimeSpan"></span></p>
						</div>
                          <div class="col-md-3">
							<p id="incidentItemsList"></p>
						</div>									
					  </div>				
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5" style="border-right: 1px #bbbbbb solid;">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li class="active" id="taskliInfo"><a href="#taskinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="taskliNotes"><a href="#notes-tab" data-toggle="tab" class="capitalize-text">NOTES</a>
                                            </li>
                                            <li id="taskliActi"><a href="#taskactivity-tab" data-toggle="tab" onclick="tracebackOn('task')" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="taskliAtta"><a href="#taskattachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
                                        <div class="tab-content">
										<div class="tab-pane fade active in" id="taskinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Description:</b></p>
													<p id="taskdescriptionSpan"></p>
												</div>
                                                <div class="col-md-12" id="dvCustomerNameSpan">
													<p class="red-color"><b>Account Name:</b></p>
													<p id="CustomerNameSpan"></p>
												</div>
                                                <div class="col-md-12" id="dvContractNameSpan">
													<p class="red-color"><b>Contract Name:</b></p>
													<p id="ContractNameSpan"></p>
												</div>
                                                 <div class="col-md-12" id="dvProjectNameSpan">
													<p class="red-color"><b>Project Name:</b></p>
													<p id="ProjectNameSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Checklist Name : </b><b id="checklistnameSpan"></b><i id="checklistnamespanFA" style="margin-left:5px;" class="fa fa-check-square-o"></i></p>
                                                    <ul id="checklistItemsList" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>		
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
                                        <div class="tab-pane fade" id="notes-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x" style="display:none;">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
													<p id="taskinstructionSpan"></p>
												</div>
											</div>
                                                <div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
                                                    <div id="taskRemarksList" >
												    </div>
												</div>
											</div>	
											<div class="row">
												<div class="col-md-12">
													<p class="red-color"><b>Checklist Notes:</b></p>
													<p id="checklistNotesSpan"></p>
												</div>
											</div>	
                                           <div id="pchecklistItemsListNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Unchecked Items:</b></p>
													 <ul id="checklistItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
                                            <div id="pCanvasNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Canvas Notes:</b></p>
													 <ul id="canvasItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
										<div class="tab-pane fade" id="taskactivity-tab">
                                                    <div id="taskdivIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div>
										<div class="tab-pane fade" id="taskattachments-tab" onclick="startRot();nextbackTask();">	
                                              <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="taskattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>								
										</div>
							            </div>
									</div>
								</div>
							</div>

                            
                            <div class="col-md-1"  style="width:40px;">

                                <i id="taskrotationDIV1" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb" class="fa fa-angle-double-left  fa-2x" onclick="taskbackImg()"></i>

                            </div>

							<div class="col-md-5" id="taskdivAttachmentHolder" style="width:440px;margin-top:4px;border-right: 1px #bbbbbb solid;border-left: 1px #bbbbbb solid;">
                                <div class="tab-pane fade" id="tremarks-tab">
                                                                            								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li><a href="#tasklocation-tab" data-toggle="tab" onclick="hideAllRemarks()" class="capitalize-text">BACK</a>
                                            </li> 										
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                                    <div id="taskRemarksList2"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div> 
								<div class="tab-pane fade active in" id="tasklocation-tab">
                                    <div id="taskmap_canvasIncidentLocation" style="width:100%;height:380px;"></div>
									<div id="taskdivAttachment" class="overlapping-map-image">
									</div>
								</div>		
                                <div class="tab-pane fade" id="taskrejection-tab">
	                                      <p class="red-color text-center"><b>Rejection Notes:</b></p>
									        <div class="col-md-12" style="height:350px;">
										        <textarea placeholder="Rejection Notes" id="taskrejectionTextarea" class="form-control" rows="12"></textarea>
									        </div>
                                </div>									
							</div>

                                                         <div class="col-md-1"  style="width:40px;">
                                 <i id="taskrotationDIV2" class="fa fa-angle-double-right  fa-2x" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb;"  onclick="tasknextImg()"></i>

                             </div>
						</div>
 
                        <div class="row">
                                                        <div class="col-md-5">

                            </div>
                                    <div class="col-md-7" style="display:none;border-left:0px;" id="taskAudioDIV"> 
                                        <audio id="taskAudio" style="width: 100%;" width="100%" controls><source id="taskAudioSrc" type="audio/mpeg"/></audio>
                                    </div>
                        </div>
					</div>
					<div class="modal-footer">
                                               <div class="row" id="rowtasktracebackUser" style="display:none;">
                        <div class="col-md-5">
                            </div>
                               <div class="col-md-7" style="text-align:left;margin-top:-8px;">
 <a style="font-size:16px;" href="#" id="playpausefilter" onclick="playpauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1,'task')"><i id="taskfilter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5,'task')"><i id="taskfilter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30,'task')"><i id="taskfilter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60,'task')"><i id="taskfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                            </div>
                        </div>
						<div class="row horizontal-navigation">
							<div class="panel-control">
                                <ul class="nav nav-tabs" id="taskinitialOptionsDiv">
									<li><a href="#" data-dismiss="modal" data-target="#ticketingViewCard" onclick="rowchoice(document.getElementById('rowidChoiceTicket').value)"  data-toggle="modal">CLOSE</a>
									</li>
								</ul>
								<ul class="nav nav-tabs" id="taskhandleOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal" data-target="#ticketingViewCard" onclick="rowchoice(document.getElementById('rowidChoiceTicket').value)"  data-toggle="modal">CLOSE</a>
									</li>		
                                    <li><a href="#">ACCEPT</a>
									</li>
									<li><a href="#" data-target="#taskrejection-tab" data-toggle="tab"  onclick="rejectionSelect();">REJECT</a>
									</li>							
								</ul>
                                <ul class="nav nav-tabs" id="taskrejectOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal" data-target="#ticketingViewCard" onclick="rowchoice(document.getElementById('rowidChoiceTicket').value)"  data-toggle="modal">CLOSE</a>
									</li>
                                    <li><a href="#">ASSIGN</a>
									</li>
									<li><a href="#">REASSIGN</a>
									</li>
									<li><a href="#">SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	