﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class DutyRoster
    {
                public int Id { get; set; }
                public int ParentId { get; set; }
                public string Location { get; set; }

        public string Notes { get; set; }

                public string DutyPost { get; set; }
                public int SN { get; set; }
                public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }

                public string Time { get; set; }
                public DateTime? DutyDateTime { get; set; }
        public string ShiftType { get; set; }

        public string Day { get; set; }
        public int DayNumber { get; set; }

       public int SiteId { get; set; }

       public int CustomerId { get; set; }

       public int DutyStatus { get; set; }

       public DateTime? StartDutyTime { get; set; }

       public bool ShiftOn { get; set; }

       public bool TaskOn { get; set; }

        public string CreatedBy { get; set; }
         
        public DateTime? CreatedDate { get; set; }

        public string CustomerUName { get; set; }

        private static DutyRoster GetDutyRosterFromSqlReader(SqlDataReader reader)
        {
            var dutyRoaster = new DutyRoster();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                dutyRoaster.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["ParentId"] != DBNull.Value)
                dutyRoaster.ParentId = Convert.ToInt32(reader["ParentId"].ToString());
            if (reader["Location"] != DBNull.Value)
                dutyRoaster.Location = reader["Location"].ToString();

            if (reader["ChildLocation"] != DBNull.Value)
                dutyRoaster.Notes = reader["ChildLocation"].ToString();

            if (reader["DutyPost"] != DBNull.Value)
                dutyRoaster.DutyPost = reader["DutyPost"].ToString();

            if (reader["SN"] != DBNull.Value)
                dutyRoaster.SN = Convert.ToInt32(reader["SN"].ToString());

            if (reader["EmployeeId"] != DBNull.Value)
                dutyRoaster.EmployeeId = Convert.ToInt32(reader["EmployeeId"].ToString());

            if (reader["EmployeeName"] != DBNull.Value)
                dutyRoaster.EmployeeName = reader["EmployeeName"].ToString();


            if (reader["ShiftType"] != DBNull.Value)
                dutyRoaster.ShiftType = reader["ShiftType"].ToString();

            if (reader["Day"] != DBNull.Value)
                dutyRoaster.Day = reader["Day"].ToString();

            if (reader["DayNumber"] != DBNull.Value)
                dutyRoaster.DayNumber = Convert.ToInt32(reader["DayNumber"].ToString());

            if (reader["SiteId"] != DBNull.Value)
                dutyRoaster.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (reader["CustomerId"] != DBNull.Value)
                dutyRoaster.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
            

            if (reader["CreatedBy"] != DBNull.Value)
                dutyRoaster.CreatedBy = reader["CreatedBy"].ToString().ToLower();

            if (reader["CreatedDate"] != DBNull.Value)
                dutyRoaster.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains( "CustomerUName"))
                dutyRoaster.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("DutyStatus"))
            {
                if (reader["DutyStatus"] != DBNull.Value & !string.IsNullOrEmpty(reader["DutyStatus"].ToString()))
                    dutyRoaster.DutyStatus = Convert.ToInt32(reader["DutyStatus"].ToString());
            }
            if (columns.Contains("StartDutyTime"))
            {
                if (reader["StartDutyTime"] != DBNull.Value)
                    dutyRoaster.StartDutyTime = Convert.ToDateTime(reader["StartDutyTime"].ToString());
            }

            if (columns.Contains("ShiftOn"))
            {
                if (reader["ShiftOn"] != DBNull.Value)
                    dutyRoaster.ShiftOn = Convert.ToBoolean(reader["ShiftOn"].ToString());
            }
                        if (columns.Contains("ShiftOn"))
            {
                if (reader["TaskOn"] != DBNull.Value)
                    dutyRoaster.TaskOn = Convert.ToBoolean(reader["TaskOn"].ToString());
            }
            
            if (reader["Time"] != DBNull.Value)
            {
                dutyRoaster.Time = reader["Time"].ToString();
                try
                {
                    dutyRoaster.DutyDateTime = Convert.ToDateTime(dutyRoaster.Time);
                }
                catch (Exception ex)
                {

                }

            }

            return dutyRoaster;
        }

        public static int InsertorUpdateDutyRoster(DutyRoster customEvent, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            try
            {
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                if (customEvent != null)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertorUpdateDutyRoster", connection);


                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = customEvent.Id;

                        cmd.Parameters.Add(new SqlParameter("@ParentID", SqlDbType.Int));
                        cmd.Parameters["@ParentID"].Value = customEvent.ParentId;

                        cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.NVarChar));
                        cmd.Parameters["@Location"].Value = customEvent.Location;

                        cmd.Parameters.Add(new SqlParameter("@ChildLocation", SqlDbType.NVarChar));
                        cmd.Parameters["@ChildLocation"].Value = customEvent.Notes;

                        cmd.Parameters.Add(new SqlParameter("@DutyPost", SqlDbType.NVarChar));
                        cmd.Parameters["@DutyPost"].Value = customEvent.DutyPost;

                        cmd.Parameters.Add(new SqlParameter("@SN", SqlDbType.Int));
                        cmd.Parameters["@SN"].Value = customEvent.SN;

                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.Int));
                        cmd.Parameters["@EmployeeId"].Value = customEvent.EmployeeId;

                        cmd.Parameters.Add(new SqlParameter("@EmployeeName", SqlDbType.NVarChar));
                        cmd.Parameters["@EmployeeName"].Value = customEvent.EmployeeName;

                        cmd.Parameters.Add(new SqlParameter("@Time", SqlDbType.NVarChar));
                        cmd.Parameters["@Time"].Value = customEvent.Time;

                        cmd.Parameters.Add(new SqlParameter("@ShiftType", SqlDbType.NVarChar));
                        cmd.Parameters["@ShiftType"].Value = customEvent.ShiftType;

                        cmd.Parameters.Add(new SqlParameter("@Day", SqlDbType.NVarChar));
                        cmd.Parameters["@Day"].Value = customEvent.Day;

                        cmd.Parameters.Add(new SqlParameter("@DayNumber", SqlDbType.Int));
                        cmd.Parameters["@DayNumber"].Value = customEvent.DayNumber;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = customEvent.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = customEvent.CustomerId;
                        

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = customEvent.CreatedBy;


                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreatedDate"].Value = customEvent.CreatedDate;

                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandText = "InsertorUpdateDutyRoster";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.ExecuteNonQuery();

                        id = (int)returnParameter.Value;

                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {
                                customEvent.Id = id;
                                var audit = new DutyRosterAudit();
                                audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customEvent.CreatedBy);
                                Reflection.CopyProperties(customEvent, audit);
                                DutyRosterAudit.InsertDutyRosterAudit(audit, auditDbConnection);
                            }
                        }
                        catch (Exception ex)
                        {
                            MIMSLog.MIMSLogSave("DutyRoster", "InsertorUpdateDutyRosterAudit", ex, dbConnection);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("DutyRoster", "InsertorUpdateDutyRoster", ex, dbConnection);
            }
            return id;
        }

        public static bool DeleteDutyRosterByEmpIdAndTime(int empid, string time,string dbConnection)
        {
            try
            {

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();

                        var cmd = new SqlCommand("DeleteDutyRosterByEmpIdAndTime", connection);

                        cmd.Parameters.Add(new SqlParameter("@EmpId", SqlDbType.Int));
                        cmd.Parameters["@EmpId"].Value = empid;

                        cmd.Parameters.Add(new SqlParameter("@time", SqlDbType.NVarChar));
                        cmd.Parameters["@time"].Value = time;

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.CommandText = "DeleteDutyRosterByEmpIdAndTime";

                        cmd.ExecuteNonQuery();
                    }
                    return true;
                
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("DutyRoster", "DeleteDutyRosterByEmpIdAndTime", ex, dbConnection);
            }
            return false;
        }

        public static bool DeleteDutyRosterByEmpId(int empid, string dbConnection)
        {
            try
            {

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();

                        var cmd = new SqlCommand("DeleteDutyRosterByEmpId", connection);

                        cmd.Parameters.Add(new SqlParameter("@EmpId", SqlDbType.Int));
                        cmd.Parameters["@EmpId"].Value = empid;


                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.CommandText = "DeleteDutyRosterByEmpId";

                        cmd.ExecuteNonQuery();
                    }
                    return true;
                
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("DutyRoster", "DeleteDutyRosterByEmpId", ex, dbConnection);
            }
            return false;
        }

        public static DutyRoster GetFirstDutyRostersByEmployeeId(int empid, string dbConnection)
        {
            DutyRoster customEv = null;
            try
            {
                
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetFirstDutyRostersByEmployeeId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.Int));
                    sqlCommand.Parameters["@EmployeeId"].Value = empid;




                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "GetFirstDutyRostersByEmployeeId";

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        customEv = GetDutyRosterFromSqlReader(reader);
                    }

                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("DutyRoster", "GetFirstDutyRostersByEmployeeId", ex, dbConnection);
            }
            return customEv;
        }

        public static List<DutyRoster> GetTodayDutyRostersByEmployeeId(int empid,DateTime dt ,DateTime enddt,string dbConnection)
        {
            var customEv = new List<DutyRoster>();
 
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetTodayDutyRostersByEmpId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@EmpId", SqlDbType.Int));
                    sqlCommand.Parameters["@EmpId"].Value = empid;

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.Date));
                    sqlCommand.Parameters["@Date"].Value = dt;

                    sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Date));
                    sqlCommand.Parameters["@EndDate"].Value = enddt; 

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "GetTodayDutyRostersByEmpId";

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var customEvs = GetDutyRosterFromSqlReader(reader);
                        customEv.Add(customEvs);
                    }

                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("DutyRoster", "GetTodayDutyRostersByEmployeeId", ex, dbConnection);
            }
            return customEv;
        }

        public static List<DutyRoster> GetAllDutyRosterByDate(string from, string to, string dbConnection)
        {
            var customEv = new List<DutyRoster>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDutyRosterByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = from;

                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = to;

  

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllDutyRosterByDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetDutyRosterFromSqlReader(reader);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }
        public static List<DutyRoster> GetAllDutyRosterByDateLevel5AndLocation(string from, string to, int siteid, string location, string dbConnection)
        {
            var customEv = new List<DutyRoster>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDutyRosterByDateLevel5AndLocation", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = from;

                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = to;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid;

                sqlCommand.Parameters.Add(new SqlParameter("@LocationName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LocationName"].Value = location;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllDutyRosterByDateLevel5AndLocation";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetDutyRosterFromSqlReader(reader);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<DutyRoster> GetAllDutyRosterByDateSiteAndLocation(string from, string to,int siteid,string location ,string dbConnection)
        {
            var customEv = new List<DutyRoster>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDutyRosterByDateSiteAndLocation", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = from;

                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = to;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.Parameters.Add(new SqlParameter("@LocationName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LocationName"].Value = location;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllDutyRosterByDateSiteAndLocation";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetDutyRosterFromSqlReader(reader);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<DutyRoster> GetAllDutyRosterByDateCustomerAndLocation(string from, string to, int id, string location, string dbConnection)
        {
            var customEv = new List<DutyRoster>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDutyRosterByDateCustomerAndLocation", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = from;

                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = to;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@LocationName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LocationName"].Value = location;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllDutyRosterByDateCustomerAndLocation";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetDutyRosterFromSqlReader(reader);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        

        public static List<DutyRoster> GetAllDutyRosterByDateByManagerId(string from, string to, int mangid, string dbConnection)
        {
            var customEv = new List<DutyRoster>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDutyRosterByDateByManagerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = from;

                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = to;

                sqlCommand.Parameters.Add(new SqlParameter("@mangid", SqlDbType.Int));
                sqlCommand.Parameters["@mangid"].Value = mangid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllDutyRosterByDateByManagerId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetDutyRosterFromSqlReader(reader);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }


        public static List<DutyRoster> GetDutyRosterByEmpIdAndTime(string from, string to, int mangid, string dbConnection)
        {
            var customEv = new List<DutyRoster>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetDutyRosterByEmpIdAndTime", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = from;

                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = to;

                sqlCommand.Parameters.Add(new SqlParameter("@EmpId", SqlDbType.Int));
                sqlCommand.Parameters["@EmpId"].Value = mangid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetDutyRosterByEmpIdAndTime";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetDutyRosterFromSqlReader(reader);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static DutyRoster GetDutyRosterByEmpIdByTime(string from, int mangid, string dbConnection)
        {
            DutyRoster customEv = null;

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetDutyRosterByEmpIdByTime", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = from;

                sqlCommand.Parameters.Add(new SqlParameter("@EmpId", SqlDbType.Int));
                sqlCommand.Parameters["@EmpId"].Value = mangid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetDutyRosterByEmpIdByTime";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customEv = GetDutyRosterFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static bool UpdateDutyRosterByShift(int taskId, bool dt, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("UpdateDutyRosterByShift", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskId;
                    cmd.Parameters.Add(new SqlParameter("@Shift", SqlDbType.TinyInt));
                    cmd.Parameters["@Shift"].Value = dt;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "UpdateDutyRosterByShift";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "UpdateTaskSignature", ex, dbConnection);
                return false;
            }
            return true;
        }

        public static bool UpdateDutyRosterByTask(int taskId, bool dt, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("UpdateDutyRosterByTask", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskId;
                    cmd.Parameters.Add(new SqlParameter("@Task", SqlDbType.TinyInt));
                    cmd.Parameters["@Task"].Value = dt;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "UpdateDutyRosterByTask";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "UpdateTaskSignature", ex, dbConnection);
                return false;
            }
            return true;
        }
    }
}
