﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{
    public class Settings
    {
        public int WindowID { get; set; }
        public int ActiveTile { get; set; }
        public int IntegratedCameraIndex { get; set; }
        public int IntegratedResolutionIndex { get; set; }
        public string AxisUsername { get; set; }
        public string AxisPassword { get; set; }
        public string AxisIP { get; set; }
        public string XtralisIP { get; set; }
        public string XtralisPort { get; set; }
        public string XtralisUsername { get; set; }
        public string XtralisPassword { get; set; }
        public string MilestoneIP { get; set; }
        public string MilestonePort { get; set; }
        public int MilestoneCameraIndex { get; set; }
        public int MilestoneAuthIndex { get; set; }
        public string MilestoneUsername { get; set; }
        public string MilestonePassword { get; set; }   
      public int XtralisCameraIndex {get;set;}
      public int XtralisResolutionIndex { get; set; }
      public bool SyncXtralis { get; set; }

        public static List<Settings> GetAllSettings(string dbConnection)
        {
            var settingsList = new List<Settings>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSettings", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var setClass = new Settings();

                    setClass.WindowID = Convert.ToInt32(reader["Id"].ToString());
                    if (!string.IsNullOrEmpty(reader["Tile"].ToString()))
                        setClass.ActiveTile = Convert.ToInt32(reader["Tile"].ToString());
                    if (!string.IsNullOrEmpty(reader["IntegratedCameraIndex"].ToString()))
                        setClass.IntegratedCameraIndex = Convert.ToInt32(reader["IntegratedCameraIndex"].ToString());
                    if (!string.IsNullOrEmpty(reader["IntegratedResolutionIndex"].ToString()))
                        setClass.IntegratedResolutionIndex = Convert.ToInt32(reader["IntegratedResolutionIndex"].ToString());
                    setClass.AxisUsername = reader["AxisUsername"].ToString();
                    setClass.AxisPassword = reader["AxisPassword"].ToString();
                    setClass.AxisIP = reader["AxisIP"].ToString();
                    setClass.XtralisIP = reader["XtralisIpAddress"].ToString();
                    setClass.XtralisPort = reader["XtralisPort"].ToString();
                    setClass.XtralisUsername = reader["XtralisUsername"].ToString();
                    setClass.XtralisPassword = reader["XtralisPassword"].ToString();
                    setClass.MilestoneIP = reader["MilestoneIpAddress"].ToString();
                    setClass.MilestonePort = reader["MilestonePort"].ToString();
                   
                    if (!string.IsNullOrEmpty(reader["MilestoneCameraIndex"].ToString()))
                        setClass.MilestoneCameraIndex = Convert.ToInt32(reader["MilestoneCameraIndex"].ToString());
                   
                    if (!string.IsNullOrEmpty(reader["MilestoneAuthIndex"].ToString()))
                        setClass.MilestoneAuthIndex = Convert.ToInt32(reader["MilestoneAuthIndex"].ToString());
                  
                    if (!string.IsNullOrEmpty(reader["XtralisCameraIndex"].ToString()))
                        setClass.XtralisCameraIndex = Convert.ToInt32(reader["XtralisCameraIndex"].ToString());
                    
                    if (!string.IsNullOrEmpty(reader["XtralisResolutionIndex"].ToString()))
                        setClass.XtralisResolutionIndex = Convert.ToInt32(reader["XtralisResolutionIndex"].ToString());
                   
                    if (!string.IsNullOrEmpty(reader["SyncXtralis"].ToString()))
                        setClass.SyncXtralis = Convert.ToBoolean(reader["SyncXtralis"].ToString());
                  
                    setClass.MilestoneUsername = reader["MilestoneUsername"].ToString();
                    setClass.MilestonePassword = reader["MilestonePassword"].ToString();

                    settingsList.Add(setClass);
                }
                connection.Close();
                reader.Close();
            }
            return settingsList;
        }

        public static bool InsertOrUpdateSettings(Settings settingClass, string dbConnection)
        {
            if (settingClass != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateSettings", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = settingClass.WindowID;

                    cmd.Parameters.Add(new SqlParameter("@Tile", SqlDbType.Int));
                    cmd.Parameters["@Tile"].Value = settingClass.ActiveTile;

                    cmd.Parameters.Add(new SqlParameter("@IntegratedCameraIndex", SqlDbType.Int));
                    cmd.Parameters["@IntegratedCameraIndex"].Value = settingClass.IntegratedCameraIndex;

                    cmd.Parameters.Add(new SqlParameter("@IntegratedResolutionIndex", SqlDbType.Int));
                    cmd.Parameters["@IntegratedResolutionIndex"].Value = settingClass.IntegratedResolutionIndex;

                    cmd.Parameters.Add(new SqlParameter("@AxisUsername", SqlDbType.NVarChar));
                    cmd.Parameters["@AxisUsername"].Value = settingClass.AxisUsername;

                    cmd.Parameters.Add(new SqlParameter("@AxisPassword", SqlDbType.NVarChar));
                    cmd.Parameters["@AxisPassword"].Value = settingClass.AxisPassword;

                    cmd.Parameters.Add(new SqlParameter("@AxisIP", SqlDbType.NVarChar));
                    cmd.Parameters["@AxisIP"].Value = settingClass.AxisIP;

                    cmd.Parameters.Add(new SqlParameter("@XtralisIpAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@XtralisIpAddress"].Value = settingClass.XtralisIP;

                    cmd.Parameters.Add(new SqlParameter("@XtralisPort", SqlDbType.VarChar));
                    cmd.Parameters["@XtralisPort"].Value = settingClass.XtralisPort;

                    cmd.Parameters.Add(new SqlParameter("@XtralisUsername", SqlDbType.NVarChar));
                    cmd.Parameters["@XtralisUsername"].Value = settingClass.XtralisUsername;

                    cmd.Parameters.Add(new SqlParameter("@XtralisPassword", SqlDbType.NVarChar));
                    cmd.Parameters["@XtralisPassword"].Value = settingClass.XtralisPassword;

                    cmd.Parameters.Add(new SqlParameter("@MilestoneIpAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MilestoneIpAddress"].Value = settingClass.MilestoneIP;

                    cmd.Parameters.Add(new SqlParameter("@MilestonePort", SqlDbType.NVarChar));
                    cmd.Parameters["@MilestonePort"].Value = settingClass.MilestonePort;

                    cmd.Parameters.Add(new SqlParameter("@MilestoneCameraIndex", SqlDbType.Int));
                    cmd.Parameters["@MilestoneCameraIndex"].Value = settingClass.MilestoneCameraIndex;

                    cmd.Parameters.Add(new SqlParameter("@MilestoneAuthIndex", SqlDbType.Int));
                    cmd.Parameters["@MilestoneAuthIndex"].Value = settingClass.MilestoneAuthIndex;

                    cmd.Parameters.Add(new SqlParameter("@MilestoneUsername", SqlDbType.NVarChar));
                    cmd.Parameters["@MilestoneUsername"].Value = settingClass.MilestoneUsername;

                    cmd.Parameters.Add(new SqlParameter("@MilestonePassword", SqlDbType.NVarChar));
                    cmd.Parameters["@MilestonePassword"].Value = settingClass.MilestonePassword;
                   
                    cmd.Parameters.Add(new SqlParameter("@SyncXtralis", SqlDbType.Bit));

                    cmd.Parameters["@SyncXtralis"].Value = settingClass.SyncXtralis;

                    cmd.Parameters.Add(new SqlParameter("@XtralisCameraIndex", SqlDbType.Int));
                    cmd.Parameters["@XtralisCameraIndex"].Value = settingClass.XtralisCameraIndex;
                  
                    cmd.Parameters.Add(new SqlParameter("@XtralisResolutionIndex", SqlDbType.Int));
                    cmd.Parameters["@XtralisResolutionIndex"].Value = settingClass.XtralisResolutionIndex;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateSettings";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
