﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class MimsLogAudit
    {
        public BaseAudit BaseAudit { get; set; }   
        public int Id { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public DateTime? CurrentDateTime { get; set; }
        public string UpdatedBy { get; set; }
        public string MacAddress { get; set; }
        public string CurrentDateTimeToString { get; set; }
        public int SiteId { get; set; }

        public static bool InsertMimsLogAudit(MimsLogAudit mimsLogAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(mimsLogAudit.BaseAudit, dbConnection);

            if (mimsLogAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMimsLogAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = mimsLogAudit.Id;
                    cmd.Parameters.Add("@Message", SqlDbType.NVarChar).Value = mimsLogAudit.Message;
                    cmd.Parameters.Add("@Exception", SqlDbType.NVarChar).Value = mimsLogAudit.Exception;
                    cmd.Parameters.Add("@MacAddress", SqlDbType.NVarChar).Value = mimsLogAudit.MacAddress;
                    cmd.Parameters.Add("@CurrentDateTime", SqlDbType.DateTime).Value = mimsLogAudit.CurrentDateTime;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = mimsLogAudit.SiteId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                }
                return true;
            }
            return false;
        }
    }
}
