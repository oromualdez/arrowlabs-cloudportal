﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class LanguageAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string LanguageName { get; set; }
        public string Main { get; set; }
        public string Picture { get; set; }
        public string Session { get; set; }
        public string Settings { get; set; }
        public string Delete { get; set; }
        public string Save { get; set; }
        public string Connect { get; set; }
        public string Yes { get; set; }
        public string No { get; set; }
        public string Disconnect { get; set; }
        public string Resolution { get; set; }
        public string Camera { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Integrated { get; set; }
        public string XtralisSync { get; set; }
        public string PlateData { get; set; }
        public string VehicleData { get; set; }
        public string Fine { get; set; }
        public string Process { get; set; }
        public string PlateNo { get; set; }

        public string Source { get; set; }
        public string Maker { get; set; }
        public string Color { get; set; }
        public string Subtract { get; set; }
        public string Add { get; set; }
        public string Driving { get; set; }
        public string Parking { get; set; }
        public string Regulation { get; set; }
        public string Usage { get; set; }
        public string Other { get; set; }
        public string Overpass { get; set; }
        public string Port { get; set; }
        public string EnterIP { get; set; }
        public string ErrorMessage1 { get; set; }
        public string ErrorMessage2 { get; set; }
        public string ErrorMessage3 { get; set; }
        public string ErrorMessage4 { get; set; }
        public string ErrorMessage5 { get; set; }
        public string ErrorMessage6 { get; set; }
        public string ErrorMessage7 { get; set; }
        public string ErrorMessage8 { get; set; }
        public string ErrorMessage9 { get; set; }
        public string ErrorMessage10 { get; set; }
        public string ErrorMessage11 { get; set; }
        public string ErrorMessage12 { get; set; }
        public string ErrorMessage13 { get; set; }
        public string ErrorMessage14 { get; set; }
        public string ErrorMessage15 { get; set; }
        public string ErrorMessage16 { get; set; }
        public string ErrorMessage17 { get; set; }
        public string ErrorMessage18 { get; set; }
        public string ErrorMessage19 { get; set; }
        public string ErrorMessage20 { get; set; }

        public string HotAvailable { get; set; }
        public string GPSAvailable { get; set; }
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string E { get; set; }
        public string F { get; set; }
        public string G { get; set; }
        public string H { get; set; }
        public string I { get; set; }
        public string J { get; set; }
        public string K { get; set; }
        public string L { get; set; }
        public string M { get; set; }
        public string N { get; set; }
        public string O { get; set; }
        public string P { get; set; }
        public string Q { get; set; }
        public string R { get; set; }
        public string S { get; set; }
        public string T { get; set; }
        public string U { get; set; }
        public string V { get; set; }
        public string W { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Z { get; set; }
        public string Period { get; set; }
        public string ALL { get; set; }
        public string Dash { get; set; }
        public string PlaybackRequest { get; set; }
        public string MainA { get; set; }
        public string MainB { get; set; }

        public string FineSource1 { get; set; }
        public string FineSource2 { get; set; }
        public string FineSource3 { get; set; }
        public string FineSource4 { get; set; }
        public string FineSource5 { get; set; }
        public string FineSource6 { get; set; }
        public string FineSource7 { get; set; }

        public string FineCode1 { get; set; }
        public string FineCode2 { get; set; }
        public string FineCode3 { get; set; }

        public string RED { get; set; }
        public string ORANGE { get; set; }
        public string YELLOW { get; set; }
        public string WHITE { get; set; }
        public string BLACK { get; set; }
        public string BLUE { get; set; }
        public string CASTLE { get; set; }
        public string MOTORCYCLE { get; set; }

        public string LoginPageTB1 { get; set; }
        public string LoginPageTB2 { get; set; }
        public string LoginPageButton { get; set; }
        public string LoginPageErrorMessage { get; set; }

        public string NewPageTB1 { get; set; }
        public string NewPageTB2 { get; set; }
        public string NewPageTB3 { get; set; }
        public string NewPageTB4 { get; set; }
        public string NewPageTB5 { get; set; }
        public string NewPageErrorMessage1 { get; set; }
        public string NewPageErrorMessage2 { get; set; }

        private static LanguageAudit GetLanguageAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var langClass = new LanguageAudit();
            langClass.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            langClass.LanguageName = reader["LanguageName"].ToString();
            langClass.Main = reader["Main"].ToString();
            langClass.Picture = reader["Picture"].ToString();
            langClass.Session = reader["Session"].ToString();
            langClass.Settings = reader["Settings"].ToString();
            langClass.Delete = reader["DeleteWord"].ToString();
            langClass.Save = reader["SaveWord"].ToString();
            langClass.Connect = reader["Connect"].ToString();
            langClass.Yes = reader["Yes"].ToString();
            langClass.No = reader["No"].ToString();
            langClass.Disconnect = reader["Disconnect"].ToString();
            langClass.Resolution = reader["Resolution"].ToString();
            langClass.Camera = reader["Camera"].ToString();
            langClass.UserName = reader["UserName"].ToString();
            langClass.Password = reader["Password"].ToString();
            langClass.Integrated = reader["Integrated"].ToString();
            langClass.XtralisSync = reader["Sync"].ToString();
            langClass.PlateData = reader["PlateData"].ToString();
            langClass.VehicleData = reader["VehicleData"].ToString();
            langClass.Fine = reader["Fine"].ToString();
            langClass.Process = reader["Process"].ToString();
            langClass.PlateNo = reader["PlateNo"].ToString();
            langClass.Source = reader["Source"].ToString();
            langClass.Maker = reader["Maker"].ToString();
            langClass.Color = reader["Color"].ToString();
            langClass.Subtract = reader["Subtract"].ToString();
            langClass.Add = reader["AddWord"].ToString();
            langClass.Driving = reader["Driving"].ToString();
            langClass.Parking = reader["Parking"].ToString();
            langClass.Regulation = reader["Regulation"].ToString();
            langClass.Usage = reader["Usage"].ToString();
            langClass.Other = reader["Other"].ToString();
            langClass.Overpass = reader["Overpass"].ToString();
            langClass.Port = reader["Port"].ToString();
            langClass.EnterIP = reader["EnterIP"].ToString();
            langClass.ErrorMessage1 = reader["ErrorMessage1"].ToString();
            langClass.ErrorMessage2 = reader["ErrorMessage2"].ToString();
            langClass.ErrorMessage3 = reader["ErrorMessage3"].ToString();
            langClass.ErrorMessage4 = reader["ErrorMessage4"].ToString();
            langClass.ErrorMessage5 = reader["ErrorMessage5"].ToString();
            langClass.ErrorMessage6 = reader["ErrorMessage6"].ToString();
            langClass.ErrorMessage7 = reader["ErrorMessage7"].ToString();
            langClass.ErrorMessage8 = reader["ErrorMessage8"].ToString();
            langClass.ErrorMessage9 = reader["ErrorMessage9"].ToString();
            langClass.ErrorMessage10 = reader["ErrorMessage10"].ToString();
            langClass.ErrorMessage11 = reader["ErrorMessage11"].ToString();
            langClass.ErrorMessage12 = reader["ErrorMessage12"].ToString();
            langClass.ErrorMessage13 = reader["ErrorMessage13"].ToString();
            langClass.ErrorMessage14 = reader["ErrorMessage14"].ToString();
            langClass.ErrorMessage15 = reader["ErrorMessage15"].ToString();
            langClass.ErrorMessage16 = reader["ErrorMessage16"].ToString();
            langClass.ErrorMessage17 = reader["ErrorMessage17"].ToString();
            langClass.ErrorMessage18 = reader["ErrorMessage18"].ToString();
            langClass.ErrorMessage19 = reader["ErrorMessage19"].ToString();
            langClass.ErrorMessage20 = reader["ErrorMessage20"].ToString();
            langClass.HotAvailable = reader["HotAvailable"].ToString();
            langClass.GPSAvailable = reader["GPSAvailable"].ToString();
            langClass.A = reader["A"].ToString();
            langClass.B = reader["B"].ToString();
            langClass.C = reader["C"].ToString();
            langClass.D = reader["D"].ToString();
            langClass.E = reader["E"].ToString();
            langClass.F = reader["F"].ToString();
            langClass.G = reader["G"].ToString();
            langClass.H = reader["H"].ToString();
            langClass.I = reader["I"].ToString();
            langClass.J = reader["J"].ToString();
            langClass.K = reader["K"].ToString();
            langClass.L = reader["L"].ToString();
            langClass.M = reader["M"].ToString();
            langClass.N = reader["N"].ToString();
            langClass.O = reader["O"].ToString();
            langClass.P = reader["P"].ToString();
            langClass.Q = reader["Q"].ToString();
            langClass.R = reader["R"].ToString();
            langClass.S = reader["S"].ToString();
            langClass.T = reader["T"].ToString();
            langClass.U = reader["U"].ToString();
            langClass.V = reader["V"].ToString();
            langClass.W = reader["W"].ToString();
            langClass.X = reader["X"].ToString();
            langClass.Y = reader["Y"].ToString();
            langClass.Z = reader["Z"].ToString();
            langClass.Period = reader["Period"].ToString();
            langClass.ALL = reader["ALL"].ToString();
            langClass.Dash = reader["Dash"].ToString();
            langClass.PlaybackRequest = reader["PlaybackRequest"].ToString();
            langClass.MainA = reader["MainA"].ToString();
            langClass.MainB = reader["MainB"].ToString();
            langClass.FineSource1 = reader["FineSource1"].ToString();
            langClass.FineSource2 = reader["FineSource2"].ToString();
            langClass.FineSource3 = reader["FineSource3"].ToString();
            langClass.FineSource4 = reader["FineSource4"].ToString();
            langClass.FineSource5 = reader["FineSource5"].ToString();
            langClass.FineSource6 = reader["FineSource6"].ToString();
            langClass.FineSource7 = reader["FineSource7"].ToString();
            langClass.FineCode1 = reader["FineCode1"].ToString();
            langClass.FineCode2 = reader["FineCode2"].ToString();
            langClass.FineCode3 = reader["FineCode3"].ToString();
            langClass.RED = reader["RED"].ToString();
            langClass.ORANGE = reader["ORANGE"].ToString();
            langClass.YELLOW = reader["YELLOW"].ToString();
            langClass.WHITE = reader["WHITE"].ToString();
            langClass.BLACK = reader["BLACK"].ToString();
            langClass.BLUE = reader["BLUE"].ToString();
            langClass.CASTLE = reader["CASTLE"].ToString();
            langClass.MOTORCYCLE = reader["MOTORCYCLE"].ToString();
            langClass.LoginPageTB1 = reader["LoginPageTB1"].ToString();
            langClass.LoginPageTB2 = reader["LoginPageTB2"].ToString();
            langClass.LoginPageButton = reader["LoginPageButton"].ToString();
            langClass.LoginPageErrorMessage = reader["LoginPageErrorMessage"].ToString();
            langClass.NewPageTB1 = reader["NewPageTB1"].ToString();
            langClass.NewPageTB2 = reader["NewPageTB2"].ToString();
            langClass.NewPageTB3 = reader["NewPageTB3"].ToString();
            langClass.NewPageTB4 = reader["NewPageTB4"].ToString();
            langClass.NewPageTB5 = reader["NewPageTB5"].ToString();
            langClass.NewPageErrorMessage1 = reader["NewPageErrorMessage1"].ToString();
            langClass.NewPageErrorMessage2 = reader["NewPageErrorMessage2"].ToString();

            return langClass;
        }
        public static LanguageAudit GetLanguageAudit(string dbConnection)
        {
            var languageAudit = new LanguageAudit();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetLanguageAudit", connection);
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    languageAudit = GetLanguageAuditFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return languageAudit;
        }

        public static bool InsertLanguageAudit(LanguageAudit laungaugeAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(laungaugeAudit.BaseAudit, dbConnection);

            if (laungaugeAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertLanguageAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@LanguageName", SqlDbType.NVarChar));
                    cmd.Parameters["@LanguageName"].Value = laungaugeAudit.LanguageName;

                    cmd.Parameters.Add(new SqlParameter("@Main", SqlDbType.NVarChar));
                    cmd.Parameters["@Main"].Value = laungaugeAudit.Main;

                    cmd.Parameters.Add(new SqlParameter("@Picture", SqlDbType.NVarChar));
                    cmd.Parameters["@Picture"].Value = laungaugeAudit.Picture;

                    cmd.Parameters.Add(new SqlParameter("@Session", SqlDbType.NVarChar));
                    cmd.Parameters["@Session"].Value = laungaugeAudit.Session;

                    cmd.Parameters.Add(new SqlParameter("@Settings", SqlDbType.NVarChar));
                    cmd.Parameters["@Settings"].Value = laungaugeAudit.Settings;

                    cmd.Parameters.Add(new SqlParameter("@DeleteWord", SqlDbType.NVarChar));
                    cmd.Parameters["@DeleteWord"].Value = laungaugeAudit.Delete;

                    cmd.Parameters.Add(new SqlParameter("@SaveWord", SqlDbType.NVarChar));
                    cmd.Parameters["@SaveWord"].Value = laungaugeAudit.Save;

                    cmd.Parameters.Add(new SqlParameter("@Connect", SqlDbType.NVarChar));
                    cmd.Parameters["@Connect"].Value = laungaugeAudit.Connect;

                    cmd.Parameters.Add(new SqlParameter("@Yes", SqlDbType.NVarChar));
                    cmd.Parameters["@Yes"].Value = laungaugeAudit.Yes;

                    cmd.Parameters.Add(new SqlParameter("@No", SqlDbType.NVarChar));
                    cmd.Parameters["@No"].Value = laungaugeAudit.No;

                    cmd.Parameters.Add(new SqlParameter("@Disconnect", SqlDbType.NVarChar));
                    cmd.Parameters["@Disconnect"].Value = laungaugeAudit.Disconnect;

                    cmd.Parameters.Add(new SqlParameter("@Resolution", SqlDbType.NVarChar));
                    cmd.Parameters["@Resolution"].Value = laungaugeAudit.Resolution;

                    cmd.Parameters.Add(new SqlParameter("@Camera", SqlDbType.NVarChar));
                    cmd.Parameters["@Camera"].Value = laungaugeAudit.Camera;
                    //
                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = laungaugeAudit.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                    cmd.Parameters["@Password"].Value = laungaugeAudit.Password;

                    cmd.Parameters.Add(new SqlParameter("@Integrated", SqlDbType.NVarChar));
                    cmd.Parameters["@Integrated"].Value = laungaugeAudit.Integrated;

                    cmd.Parameters.Add(new SqlParameter("@Sync", SqlDbType.NVarChar));
                    cmd.Parameters["@Sync"].Value = laungaugeAudit.XtralisSync;

                    cmd.Parameters.Add(new SqlParameter("@PlateData", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateData"].Value = laungaugeAudit.PlateData;

                    cmd.Parameters.Add(new SqlParameter("@VehicleData", SqlDbType.NVarChar));
                    cmd.Parameters["@VehicleData"].Value = laungaugeAudit.VehicleData;

                    cmd.Parameters.Add(new SqlParameter("@Fine", SqlDbType.NVarChar));
                    cmd.Parameters["@Fine"].Value = laungaugeAudit.Fine;

                    cmd.Parameters.Add(new SqlParameter("@Process", SqlDbType.NVarChar));
                    cmd.Parameters["@Process"].Value = laungaugeAudit.Process;

                    cmd.Parameters.Add(new SqlParameter("@PlateNo", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateNo"].Value = laungaugeAudit.PlateNo;

                    cmd.Parameters.Add(new SqlParameter("@Source", SqlDbType.NVarChar));
                    cmd.Parameters["@Source"].Value = laungaugeAudit.Source;

                    cmd.Parameters.Add(new SqlParameter("@Maker", SqlDbType.NVarChar));
                    cmd.Parameters["@Maker"].Value = laungaugeAudit.Maker;

                    cmd.Parameters.Add(new SqlParameter("@Color", SqlDbType.NVarChar));
                    cmd.Parameters["@Color"].Value = laungaugeAudit.Color;

                    cmd.Parameters.Add(new SqlParameter("@Subtract", SqlDbType.NVarChar));
                    cmd.Parameters["@Subtract"].Value = laungaugeAudit.Subtract;

                    cmd.Parameters.Add(new SqlParameter("@AddWord", SqlDbType.NVarChar));
                    cmd.Parameters["@AddWord"].Value = laungaugeAudit.Add;

                    cmd.Parameters.Add(new SqlParameter("@Driving", SqlDbType.NVarChar));
                    cmd.Parameters["@Driving"].Value = laungaugeAudit.Driving;

                    cmd.Parameters.Add(new SqlParameter("@Parking", SqlDbType.NVarChar));
                    cmd.Parameters["@Parking"].Value = laungaugeAudit.Parking;

                    cmd.Parameters.Add(new SqlParameter("@Regulation", SqlDbType.NVarChar));
                    cmd.Parameters["@Regulation"].Value = laungaugeAudit.Regulation;

                    cmd.Parameters.Add(new SqlParameter("@Usage", SqlDbType.NVarChar));
                    cmd.Parameters["@Usage"].Value = laungaugeAudit.Usage;

                    cmd.Parameters.Add(new SqlParameter("@Other", SqlDbType.NVarChar));
                    cmd.Parameters["@Other"].Value = laungaugeAudit.Other;

                    cmd.Parameters.Add(new SqlParameter("@Overpass", SqlDbType.NVarChar));
                    cmd.Parameters["@Overpass"].Value = laungaugeAudit.Overpass;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.NVarChar));
                    cmd.Parameters["@Port"].Value = laungaugeAudit.Port;

                    cmd.Parameters.Add(new SqlParameter("@EnterIP", SqlDbType.NVarChar));
                    cmd.Parameters["@EnterIP"].Value = laungaugeAudit.EnterIP;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage1", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage1"].Value = laungaugeAudit.ErrorMessage1;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage2", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage2"].Value = laungaugeAudit.ErrorMessage2;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage3", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage3"].Value = laungaugeAudit.ErrorMessage3;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage4", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage4"].Value = laungaugeAudit.ErrorMessage4;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage5", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage5"].Value = laungaugeAudit.ErrorMessage5;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage6", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage6"].Value = laungaugeAudit.ErrorMessage6;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage7", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage7"].Value = laungaugeAudit.ErrorMessage7;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage8", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage8"].Value = laungaugeAudit.ErrorMessage8;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage9", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage9"].Value = laungaugeAudit.ErrorMessage9;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage10", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage10"].Value = laungaugeAudit.ErrorMessage10;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage11", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage11"].Value = laungaugeAudit.ErrorMessage11;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage12", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage12"].Value = laungaugeAudit.ErrorMessage12;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage13", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage13"].Value = laungaugeAudit.ErrorMessage13;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage14", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage14"].Value = laungaugeAudit.ErrorMessage14;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage15", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage15"].Value = laungaugeAudit.ErrorMessage15;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage16", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage16"].Value = laungaugeAudit.ErrorMessage16;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage17", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage17"].Value = laungaugeAudit.ErrorMessage17;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage18", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage18"].Value = laungaugeAudit.ErrorMessage18;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage19", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage19"].Value = laungaugeAudit.ErrorMessage19;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage20", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage20"].Value = laungaugeAudit.ErrorMessage20;

                    cmd.Parameters.Add(new SqlParameter("@HotAvailable", SqlDbType.NVarChar));
                    cmd.Parameters["@HotAvailable"].Value = laungaugeAudit.HotAvailable;

                    cmd.Parameters.Add(new SqlParameter("@GPSAvailable", SqlDbType.NVarChar));
                    cmd.Parameters["@GPSAvailable"].Value = laungaugeAudit.GPSAvailable;

                    cmd.Parameters.Add(new SqlParameter("@A", SqlDbType.NVarChar));
                    cmd.Parameters["@A"].Value = laungaugeAudit.A;

                    cmd.Parameters.Add(new SqlParameter("@B", SqlDbType.NVarChar));
                    cmd.Parameters["@B"].Value = laungaugeAudit.B;

                    cmd.Parameters.Add(new SqlParameter("@C", SqlDbType.NVarChar));
                    cmd.Parameters["@C"].Value = laungaugeAudit.C;

                    cmd.Parameters.Add(new SqlParameter("@D", SqlDbType.NVarChar));
                    cmd.Parameters["@D"].Value = laungaugeAudit.D;

                    cmd.Parameters.Add(new SqlParameter("@E", SqlDbType.NVarChar));
                    cmd.Parameters["@E"].Value = laungaugeAudit.E;

                    cmd.Parameters.Add(new SqlParameter("@F", SqlDbType.NVarChar));
                    cmd.Parameters["@F"].Value = laungaugeAudit.F;

                    cmd.Parameters.Add(new SqlParameter("@G", SqlDbType.NVarChar));
                    cmd.Parameters["@G"].Value = laungaugeAudit.G;

                    cmd.Parameters.Add(new SqlParameter("@H", SqlDbType.NVarChar));
                    cmd.Parameters["@H"].Value = laungaugeAudit.H;

                    cmd.Parameters.Add(new SqlParameter("@I", SqlDbType.NVarChar));
                    cmd.Parameters["@I"].Value = laungaugeAudit.I;

                    cmd.Parameters.Add(new SqlParameter("@J", SqlDbType.NVarChar));
                    cmd.Parameters["@J"].Value = laungaugeAudit.J;

                    cmd.Parameters.Add(new SqlParameter("@K", SqlDbType.NVarChar));
                    cmd.Parameters["@K"].Value = laungaugeAudit.K;

                    cmd.Parameters.Add(new SqlParameter("@L", SqlDbType.NVarChar));
                    cmd.Parameters["@L"].Value = laungaugeAudit.L;

                    cmd.Parameters.Add(new SqlParameter("@M", SqlDbType.NVarChar));
                    cmd.Parameters["@M"].Value = laungaugeAudit.M;

                    cmd.Parameters.Add(new SqlParameter("@N", SqlDbType.NVarChar));
                    cmd.Parameters["@N"].Value = laungaugeAudit.N;

                    cmd.Parameters.Add(new SqlParameter("@O", SqlDbType.NVarChar));
                    cmd.Parameters["@O"].Value = laungaugeAudit.O;

                    cmd.Parameters.Add(new SqlParameter("@P", SqlDbType.NVarChar));
                    cmd.Parameters["@P"].Value = laungaugeAudit.P;

                    cmd.Parameters.Add(new SqlParameter("@Q", SqlDbType.NVarChar));
                    cmd.Parameters["@Q"].Value = laungaugeAudit.Q;

                    cmd.Parameters.Add(new SqlParameter("@R", SqlDbType.NVarChar));
                    cmd.Parameters["@R"].Value = laungaugeAudit.R;

                    cmd.Parameters.Add(new SqlParameter("@S", SqlDbType.NVarChar));
                    cmd.Parameters["@S"].Value = laungaugeAudit.S;

                    cmd.Parameters.Add(new SqlParameter("@T", SqlDbType.NVarChar));
                    cmd.Parameters["@T"].Value = laungaugeAudit.T;

                    cmd.Parameters.Add(new SqlParameter("@U", SqlDbType.NVarChar));
                    cmd.Parameters["@U"].Value = laungaugeAudit.U;

                    cmd.Parameters.Add(new SqlParameter("@V", SqlDbType.NVarChar));
                    cmd.Parameters["@V"].Value = laungaugeAudit.V;

                    cmd.Parameters.Add(new SqlParameter("@W", SqlDbType.NVarChar));
                    cmd.Parameters["@W"].Value = laungaugeAudit.W;

                    cmd.Parameters.Add(new SqlParameter("@X", SqlDbType.NVarChar));
                    cmd.Parameters["@X"].Value = laungaugeAudit.X;

                    cmd.Parameters.Add(new SqlParameter("@Y", SqlDbType.NVarChar));
                    cmd.Parameters["@Y"].Value = laungaugeAudit.Y;

                    cmd.Parameters.Add(new SqlParameter("@Z", SqlDbType.NVarChar));
                    cmd.Parameters["@Z"].Value = laungaugeAudit.Z;

                    cmd.Parameters.Add(new SqlParameter("@Period", SqlDbType.NVarChar));
                    cmd.Parameters["@Period"].Value = laungaugeAudit.Period;

                    cmd.Parameters.Add(new SqlParameter("@ALL", SqlDbType.NVarChar));
                    cmd.Parameters["@ALL"].Value = laungaugeAudit.ALL;

                    cmd.Parameters.Add(new SqlParameter("@Dash", SqlDbType.NVarChar));
                    cmd.Parameters["@Dash"].Value = laungaugeAudit.Dash;

                    cmd.Parameters.Add(new SqlParameter("@PlaybackRequest", SqlDbType.NVarChar));
                    cmd.Parameters["@PlaybackRequest"].Value = laungaugeAudit.PlaybackRequest;

                    cmd.Parameters.Add(new SqlParameter("@MainA", SqlDbType.NVarChar));
                    cmd.Parameters["@MainA"].Value = laungaugeAudit.MainA;

                    cmd.Parameters.Add(new SqlParameter("@MainB", SqlDbType.NVarChar));
                    cmd.Parameters["@MainB"].Value = laungaugeAudit.MainB;
                    //
                    cmd.Parameters.Add(new SqlParameter("@FineSource1", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource1"].Value = laungaugeAudit.FineSource1;

                    cmd.Parameters.Add(new SqlParameter("@FineSource2", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource2"].Value = laungaugeAudit.FineSource2;

                    cmd.Parameters.Add(new SqlParameter("@FineSource3", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource3"].Value = laungaugeAudit.FineSource3;

                    cmd.Parameters.Add(new SqlParameter("@FineSource4", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource4"].Value = laungaugeAudit.FineSource4;

                    cmd.Parameters.Add(new SqlParameter("@FineSource5", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource5"].Value = laungaugeAudit.FineSource5;

                    cmd.Parameters.Add(new SqlParameter("@FineSource6", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource6"].Value = laungaugeAudit.FineSource6;

                    cmd.Parameters.Add(new SqlParameter("@FineSource7", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource7"].Value = laungaugeAudit.FineSource7;

                    cmd.Parameters.Add(new SqlParameter("@FineCode1", SqlDbType.NVarChar));
                    cmd.Parameters["@FineCode1"].Value = laungaugeAudit.FineCode1;

                    cmd.Parameters.Add(new SqlParameter("@FineCode2", SqlDbType.NVarChar));
                    cmd.Parameters["@FineCode2"].Value = laungaugeAudit.FineCode2;

                    cmd.Parameters.Add(new SqlParameter("@FineCode3", SqlDbType.NVarChar));
                    cmd.Parameters["@FineCode3"].Value = laungaugeAudit.FineCode3;

                    cmd.Parameters.Add(new SqlParameter("@RED", SqlDbType.NVarChar));
                    cmd.Parameters["@RED"].Value = laungaugeAudit.RED;

                    cmd.Parameters.Add(new SqlParameter("@ORANGE", SqlDbType.NVarChar));
                    cmd.Parameters["@ORANGE"].Value = laungaugeAudit.ORANGE;

                    cmd.Parameters.Add(new SqlParameter("@YELLOW", SqlDbType.NVarChar));
                    cmd.Parameters["@YELLOW"].Value = laungaugeAudit.YELLOW;

                    cmd.Parameters.Add(new SqlParameter("@WHITE", SqlDbType.NVarChar));
                    cmd.Parameters["@WHITE"].Value = laungaugeAudit.WHITE;

                    cmd.Parameters.Add(new SqlParameter("@BLACK", SqlDbType.NVarChar));
                    cmd.Parameters["@BLACK"].Value = laungaugeAudit.BLACK;

                    cmd.Parameters.Add(new SqlParameter("@BLUE", SqlDbType.NVarChar));
                    cmd.Parameters["@BLUE"].Value = laungaugeAudit.BLUE;

                    cmd.Parameters.Add(new SqlParameter("@CASTLE", SqlDbType.NVarChar));
                    cmd.Parameters["@CASTLE"].Value = laungaugeAudit.CASTLE;

                    cmd.Parameters.Add(new SqlParameter("@MOTORCYCLE", SqlDbType.NVarChar));
                    cmd.Parameters["@MOTORCYCLE"].Value = laungaugeAudit.MOTORCYCLE;

                    cmd.Parameters.Add(new SqlParameter("@LoginPageTB1", SqlDbType.NVarChar));
                    cmd.Parameters["@LoginPageTB1"].Value = laungaugeAudit.LoginPageTB1;

                    cmd.Parameters.Add(new SqlParameter("@LoginPageTB2", SqlDbType.NVarChar));
                    cmd.Parameters["@LoginPageTB2"].Value = laungaugeAudit.LoginPageTB2;

                    cmd.Parameters.Add(new SqlParameter("@LoginPageButton", SqlDbType.NVarChar));
                    cmd.Parameters["@LoginPageButton"].Value = laungaugeAudit.LoginPageButton;

                    cmd.Parameters.Add(new SqlParameter("@LoginPageErrorMessage", SqlDbType.NVarChar));
                    cmd.Parameters["@LoginPageErrorMessage"].Value = laungaugeAudit.LoginPageErrorMessage;

                    cmd.Parameters.Add(new SqlParameter("@NewPageTB1", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB1"].Value = laungaugeAudit.NewPageTB1;

                    cmd.Parameters.Add(new SqlParameter("@NewPageTB2", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB2"].Value = laungaugeAudit.NewPageTB2;

                    cmd.Parameters.Add(new SqlParameter("@NewPageTB3", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB3"].Value = laungaugeAudit.NewPageTB3;

                    cmd.Parameters.Add(new SqlParameter("@NewPageTB4", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB4"].Value = laungaugeAudit.NewPageTB4;

                    cmd.Parameters.Add(new SqlParameter("@NewPageTB5", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB5"].Value = laungaugeAudit.NewPageTB5;

                    cmd.Parameters.Add(new SqlParameter("@NewPageErrorMessage1", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageErrorMessage1"].Value = laungaugeAudit.NewPageErrorMessage1;

                    cmd.Parameters.Add(new SqlParameter("@NewPageErrorMessage2", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageErrorMessage2"].Value = laungaugeAudit.NewPageErrorMessage2;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
