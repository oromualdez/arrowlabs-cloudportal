﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{
    public class Manager
    {
        public int ManagerId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }

        public static List<Manager> GetAllManagers(string connectionstring)
        {
            var managers = new List<Manager>();

            using (var connection = new SqlConnection(connectionstring))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("GetAllManager", connection))
                {
                    try
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        var resultSet = command.ExecuteReader();
                        while (resultSet.Read())
                        {
                            var manager = new Manager();

                            manager.ManagerId = Convert.ToInt32(resultSet["ManagerId"].ToString());
                            manager.Name = resultSet["Name"].ToString();
                            manager.Title = resultSet["Title"].ToString();
                            managers.Add(manager);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex.Message);
                        throw ex;
                    }
                }
            }
            return managers;
        }
    }
}
