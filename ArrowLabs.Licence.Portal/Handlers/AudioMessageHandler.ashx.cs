﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for AudioMessageHandler
    /// </summary>
    public class AudioMessageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (context.Request.Files.Count > 0)
                {
                    HttpFileCollection files = context.Request.Files;
                    string fname = string.Empty;
                    HttpPostedFile file = files[0];
                    var user = HttpContext.Current.User.Identity;
                    var dbConnection = CommonUtility.dbConnection; 
                    if (user != null)
                    {
                        var userinfo = Users.GetUserByName(user.Name, dbConnection);

                        if (userinfo != null)
                        {
                            var getFileName = System.IO.Path.GetFileName(file.FileName);

                            var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, file.InputStream);

                            if (savestring != "FAIL")
                            {
                                fname = savestring;
                            }
                        }
                    }
                    //for (int i = 0; i < files.Count; i++)
                    // {

                    //var mimssettings = Arrowlabs.Business.Layer.MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection);

                    //var preappPath = mimssettings.FilePath.Substring(0, mimssettings.FilePath.Length - 5);

                    // string appPath = preappPath + @"AudioAlarm\";
                    //var filePath = appPath;
                    //if (!System.IO.Directory.Exists(filePath))
                    //{
                    //    System.IO.Directory.CreateDirectory(filePath);
                    //}

                    //fname = filePath + file.FileName;
                    //if (System.IO.File.Exists(fname))
                    //    fname = context.Server.MapPath("~/Uploads/" + Syst file.FileName + "_Copy");

                    //file.SaveAs(fname);
                    //if (file.InputStream != null)
                    //    file.InputStream.Close();
                    // if (!System.IO.File.Exists(fname))
                    // {
                    //    var fileStream = System.IO.File.Create(fname);
                    //     file.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
                    //      file.InputStream.CopyTo(fileStream);
                    //      fileStream.Close();
                    /// }
                    //}



                    context.Response.ContentType = "text/plain";
                    context.Response.Write(fname);
                    //context.Response.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("AuidoAlarmHandler", "ProcessRequest", ex, CommonUtility.dbConnection);
                context.Response.Write("Error");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}