﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class Customers : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static ClientLicence getClientLic;

        protected string senderName;
        protected string ipaddress;
        protected string senderName2;
        protected string userinfoDisplay;
        protected string senderName3;
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            try
            {
                senderName = User.Identity.Name;
                senderName2 = Encrypt.EncryptData(senderName, true, dbConnection);
                var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
                ipaddress = configtSettings.ChatServerUrl + "/signalr";
                var userinfo = Users.GetUserByName(senderName, dbConnection);
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    System.Web.Security.FormsAuthentication.SignOut();
                    Response.Redirect("~/Default.aspx");
                }
                if (userinfo != null)
                {
                    senderName3 = userinfo.CustomerUName;
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        userinfoDisplay = "none";
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "Page_Load", err, dbConnection);
            }

        }
        [WebMethod]
        public static List<string> getTableData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = new List<Arrowlabs.Business.Layer.CustomerInfo>();
                sessions = Arrowlabs.Business.Layer.CustomerInfo.GetAllCustomerInfo(dbConnection);
                foreach (var item in sessions)
                {
                    var modules = item.Modules.Split('?');
                    var modsCount = modules.Length;
                    if (modsCount > 0)
                        modsCount = modsCount - 4;

                    var status = "<a  class='green-color mr-2x' onclick='userChangeStatus(&apos;" + item.Id + "&apos;,&apos;" + item.Active + "&apos;)'><i class='fa fa-check-circle'></i> Activate</a>";
                    if(!item.Active)
                        status = "<a  class='red-color mr-2x' onclick='userChangeStatus(&apos;" + item.Id + "&apos;,&apos;" + item.Active + "&apos;)'><i class='fa fa-minus-circle'></i> Suspend</a>";

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Id + "</td><td class='sorting_1'><a data-toggle='modal' href='#newUser' onclick='rowchoice(&apos;" + item.Id + "&apos;)'>" + item.Email + "</a></td><td>" + item.Name + "</td><td>" + item.TotalUser.ToString() + "</td><td>" + modsCount.ToString() + "</td><td id='" + item.Email + "_td'>" + status + "<a href='#' data-target='#deleteSite'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a></td></tr>";
                    
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Site", "getTableData", err, dbConnection);
            }
            return listy;
        }
        [WebMethod]
        public static string deleteSiteData(int id,string uname)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {

                return "LOGOUT";
            }
            else
            {
                var listy = string.Empty;
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    if (id > 0)
                    {
                        var c = Arrowlabs.Business.Layer.CustomerInfo.GetCustomerInfoById(id, dbConnection);
                        if (c != null)
                        {
                            id = c.Id;
                            if (Arrowlabs.Business.Layer.CustomerInfo.DeleteCustomerInfoById(id, dbConnection))
                            {
                                Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("CustomerInfo was deleted with id=" + id, "CustomerInfo Successfully", new Exception(), dbConnection);

                                SystemLogger.SaveSystemLog(dbConnectionAudit, "CustomerInfo", id.ToString(), id.ToString(), userinfo, "CustomerInfo DELETE" + id);

                                listy = "SUCCESS";
                            }
                            else
                            {
                                listy = "Error occured while trying to delete client";
                            }
                            if (listy == "SUCCESS")
                            {
                                //CommonUtility.CloudDeleteContainer(string.Concat(id, "blob"));
                            }
                        }
                        else
                        {
                            listy = "Error: Client does not exist.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    listy = ex.Message;
                    MIMSLog.MIMSLogSave("Site", "deleteSiteData", ex, dbConnection);
                }
                return listy;
            }
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        } 
        //User Profile
        [WebMethod]
        public static string changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo.ID > 0)
            {
                var getuser = userinfo;
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                            
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    return userinfo.ID;
                }
                else
                    return 0;
            }
            return userinfo.ID;
        }
        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                   // var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                    {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                        //if (pushDev != null)
                        //{
                        //    if (!string.IsNullOrEmpty(pushDev.Username))
                        //    {
                        //        if (pushDev.IsServerPortal)
                        //        {
                        //            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        //            fontstyle = "";
                        //        }
                        //        else
                        //        {
                        //            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        //        }
                        //    }
                        //    else
                        //    {
                        //        monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        //        fontstyle = "";
                        //    }
                        //}
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Site", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        [WebMethod]
        public static List<string> changeCustomerState(int id, string status, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomerInfo.GetCustomerInfoById(id, dbConnection);

                var statusElem = string.Empty;
                if (status == "True")
                {
                    status = "False";
                    statusElem = "<a  class='red-color mr-2x' onclick='userChangeStatus(&apos;" + customData.Id + "&apos;,&apos;False&apos;)'><i class='fa fa-minus-circle'></i> Suspend</a><a href='#' data-target='#deleteSite'  data-toggle='modal' onclick='rowchoice(&apos;" + customData.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Customer", "2", "1", userinfo, "Customer Change state-" + id);

                    listy.Add(customData.Email + "_td");
                    listy.Add(statusElem);

                    CommonUtility.sendEmail(customData.Email, customData.Name + " " + customData.LastName, customData.Email, "", "MIMS Account Deactivation", "Deactivate");

                }
                else
                {
                    status = "True";
                    statusElem = "<a  class='green-color mr-2x' onclick='userChangeStatus(&apos;" + customData.Id + "&apos;,&apos;True&apos;)'><i class='fa fa-check-circle'></i> Activate</a><a href='#' data-target='#deleteSite'  data-toggle='modal' onclick='rowchoice(&apos;" + customData.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    listy.Add(customData.Email + "_td");
                    listy.Add(statusElem);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Customer", "1", "2", userinfo, "Customer Change state-" + id);

                    //Create User with emailaddress as superadmin for his instance.
                    //Update Column Add CustomerInfoId
                    //Update Inserorupdateuser with new CustomerINfoId
                    //MakeNewRole for Customer admin.
                    //Site Table have to add CustomerInfoId
                    //Insert or update Site need to add customerinfoid field.
                    //var gUser = Users.GetUserByName()
                    var newUser = Users.GetUserByName(customData.Email, dbConnection);
                    if (newUser != null)
                    {
                        //newUser = new Users();
                        newUser.Username = customData.Email;
                        newUser.CustomerInfoId = customData.Id;
                        newUser.RoleId = (int)Role.CustomerSuperadmin;
                        //newUser.Password = Encrypt.EncryptData("1234", true, dbConnection);
                        //newUser.Gender = gender;
                        //newUser.EmployeeID = employeeid;
                        newUser.Telephone = customData.PhoneNo;
                        newUser.FirstName = customData.Name;
                        newUser.LastName = customData.LastName;
                        newUser.Email = customData.Email;
                        newUser.Engaged = false;
                        newUser.EngagedIn = "";
                        newUser.Register = ((int)Users.UserState.Enable).ToString();
                        var now = CommonUtility.getDTNow();
                        newUser.CreatedDate = now;
                        newUser.LastLogin = now;
                        newUser.CreatedBy = userinfo.Username;
                        newUser.UpdatedBy = userinfo.Username;
                        newUser.UpdatedDate = now;
                        //newUser.DeviceType = devicetype;
                        var uid = Users.InsertOrUpdateUsers(newUser, dbConnection, dbConnectionAudit, true);

                        //var modules = customData.Modules.Split('?');

                        //UserModules.DeleteUserModuleMapByUserId(id, dbConnection);

                        //UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.System, dbConnection, customData.Email, 0, customData.Id);
                        //UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Dash, dbConnection, customData.Email, 0, customData.Id);
                        //UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Reports, dbConnection, customData.Email, 0, customData.Id);
                        //var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Notification).ToString()).ToList();
                        //if (isNoti != null && isNoti.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Notification, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileNotification, dbConnection, customData.Email, 0, customData.Id);
                        //}
                        //var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        //if (isInci != null && isInci.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Incident, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Alarms, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileAlarms, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileHotevent, dbConnection, customData.Email, 0, customData.Id);
                            
                        //}

                        //var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        //if (isTask != null && isTask.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Tasks, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileTasks, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        //if (isTicket != null && isTicket.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Ticketing, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Database, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileTicketing, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        //if (isPO != null && isPO.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.PostOrder, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobilePostOrder, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        //if (isVeri != null && isVeri.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Verifier, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileVerifier, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        //if (isChat != null && isChat.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Chat, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileConversation, dbConnection, customData.Email, 0, customData.Id);
                            
                        //}

                        //var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        //if (isDR != null && isDR.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.DutyRoster, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileDutyRoster, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        //if (isLF != null && isLF.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.LostandFound, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileLostAndFound, dbConnection, customData.Email, 0, customData.Id);

                        //}

                        //var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        //if (isSurv != null && isSurv.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Surveillance, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileSurveillance, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        //if (isRequest != null && isRequest.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Request, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileRequest, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        //if (isCollab != null && isCollab.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Collaboration, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileCollaboration, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        //if (isDisp != null && isDisp.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Dispatch, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Location).ToString()).ToList();
                        //if (isLoc != null && isLoc.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Location, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileLocation, dbConnection, customData.Email, 0, customData.Id);
                        //}

                        //var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        //if (isWare != null && isWare.Count > 0)
                        //{
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Warehouse, dbConnection, customData.Email, 0, customData.Id);
                        //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileAsset, dbConnection, customData.Email, 0, customData.Id);
                        //    //UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileKey, dbConnection, customData.Email, 0);
                        //    //UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.ServerKey, dbConnection, customData.Email, 0);
                        //}

                        CommonUtility.sendEmail(customData.Email, customData.Name + " " + customData.LastName, customData.Email, newUser.Password, "Welcome to MIMS", "Success");
                    }
                }
                CustomerInfo.UpdateCustomerInfoStatus(id, Convert.ToBoolean(status), dbConnection);
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Customers", "changeCustomerState", err, dbConnection, userinfo.SiteId);

            }
            return listy;
        }

        [System.Web.Services.WebMethod]
        public static string saveInfo(string name, string address, string country, string email, string phoneno,
            string total,
            bool notimodule, bool incimodule, bool taskmodule, bool ticketmodule,
            bool postmodule, bool verimodule, bool chatmodule, bool drmodule, bool lfmodule,
            bool survmodule, bool reqmodule, bool collabmodule, bool dispatchmodule, bool locationmodule,
             bool waremodule, string lastname, string id, bool activity,string uname,
            string mbCountInput, string incidentCountInput, string taskCountInput, string ticketCountInput, string poCountInput,
            string wareCountInput, string chatCountInput, string drCountInput, string lfCountInput, string conCountInput,
            string reqCountInput, string surCountInput, string disCountInput, string actCountInput
            )
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var moduleString = string.Empty;
                moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Notification).ToString() + "?";
                moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Location).ToString() + "?";
                if (survmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Surveillance).ToString() + "?";
                }
                if (notimodule) //NEW MESSAGE BOARD
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.MessageBoard).ToString() + "?";
                }
                if (locationmodule) //NEW CONTRACT
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Contract).ToString() + "?";
                }
                if (ticketmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Ticketing).ToString() + "?";
                }
                if (taskmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Task).ToString() + "?";
                }
                if (incimodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Incident).ToString() + "?";
                }
                if (waremodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Warehouse).ToString() + "?";
                }
                if (chatmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Chat).ToString() + "?";
                }
                //if (collabmodule)
                //{
                moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Collaboration).ToString() + "?";
                //}
                if (reqmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Request).ToString() + "?";
                }
                if (drmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.DutyRoster).ToString() + "?";
                }
                if (postmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.PostOrder).ToString() + "?";
                }
                if (lfmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.LostandFound).ToString() + "?";
                }
                if (verimodule)
                {
                    //moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Verification).ToString() + "?";
                }
                if (dispatchmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Dispatch).ToString() + "?";
                }
                if (activity)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Activity).ToString() + "?";
                }
                var infoData = CustomerInfo.GetCustomerInfoById(Convert.ToInt32(id), dbConnection);//new CustomerInfo();
                var split = country.Split('(');
                var cname = split[0];
                var spli2 = split[1].Split(')');
                var doubles = spli2[0];
                var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);
                //infoData.Active = false;
                infoData.Address = address;
                infoData.Country = cname;
                infoData.TimeZone = ctimezone;
                //infoData.CreatedBy = "SystemGenerated";
                //infoData.CreatedDate = CommonUtility.getDTNow();
                infoData.Email = email;
                infoData.Modules = moduleString;
                infoData.Name = name;
                infoData.LastName = lastname;
                infoData.PhoneNo = phoneno;
                infoData.TotalUser = Convert.ToInt32(total);
                infoData.UpdatedBy = "SystemGenerated";
                infoData.UpdatedDate = CommonUtility.getDTNow();

                var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                if (latlng.Count > 0)
                {
                    infoData.Lati = latlng[0];
                    infoData.Long = latlng[1];
                }

                CustomerInfo.InsertorUpdateCustomerInfo(infoData, CommonUtility.dbConnection, CommonUtility.dbConnectionAudit, true);


                var newUser = Users.GetUserByName(infoData.Email, dbConnection);
                var nCMM = new CustomerModuleMap();
                nCMM.CustomerId = infoData.Id;
                if (newUser != null)
                {
                    newUser.Username = infoData.Email;
                    //newUser.Password = Encrypt.EncryptData(password, true, CommonUtility.dbConnection);
                    newUser.Telephone = infoData.PhoneNo;
                    newUser.FirstName = name;
                    newUser.LastName = lastname;
                    newUser.Email = infoData.Email;

                    Users.InsertOrUpdateUsers(newUser, CommonUtility.dbConnection, CommonUtility.dbConnectionAudit, true);

                    var modules = infoData.Modules.Split('?');

                    UserModules.DeleteUserModuleMapByUserId(newUser.ID, dbConnection);

                    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.System, dbConnection, infoData.Email, 0, infoData.Id);
                    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Dash, dbConnection, infoData.Email, 0, infoData.Id);
                    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Reports, dbConnection, infoData.Email, 0, infoData.Id);
                    var gallusers = Users.GetAllUsersByCustomerId(newUser.CustomerInfoId, dbConnection);
                    gallusers = gallusers.Where(i => i.Username != newUser.Username).ToList();
                    var isCus = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                    if (isCus != null && isCus.Count > 0)
                    {
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Customer, CommonUtility.dbConnection, infoData.Email, 0, infoData.Id);

                        if (!string.IsNullOrEmpty(conCountInput) && CommonUtility.isNumeric(conCountInput))
                        {
                            nCMM.COC = conCountInput;
                        }
                        else
                        {
                            nCMM.COC = "0";
                        } 
                    }
                    else
                    {
                        nCMM.COC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Customer, CommonUtility.dbConnection);
                        }
                    }
                    var isMB = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                    if (isMB != null && isMB.Count > 0)
                    {
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MessageBoard, CommonUtility.dbConnection, infoData.Email, 0, infoData.Id);

                        if (!string.IsNullOrEmpty(mbCountInput) && CommonUtility.isNumeric(mbCountInput))
                        {
                            nCMM.MBC = mbCountInput;
                        }
                        else
                        {
                            nCMM.MBC = "0";
                        }
                    
                    }
                    else
                    {
                        nCMM.MBC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MessageBoard, CommonUtility.dbConnection);
                        }
                    }
                    var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Notification).ToString()).ToList();
                    if (isNoti != null && isNoti.Count > 0)
                    {
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Notification, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileNotification, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Notification, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileNotification, CommonUtility.dbConnection);
                        }
                    }
                    var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                    if (isInci != null && isInci.Count > 0)
                    {

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Customer, CommonUtility.dbConnection, infoData.Email, 0, infoData.Id);

                        if (!string.IsNullOrEmpty(incidentCountInput) && CommonUtility.isNumeric(incidentCountInput))
                        {
                            nCMM.INC = incidentCountInput;
                        }
                        else
                        {
                            nCMM.INC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Incident, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Alarms, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileAlarms, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileHotevent, dbConnection, infoData.Email, 0, infoData.Id);

                    }
                    else
                    {
                        nCMM.INC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Incident, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Alarms, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileAlarms, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileHotevent, CommonUtility.dbConnection);
                        }
                    }
                    var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                    if (isTask != null && isTask.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(taskCountInput) && CommonUtility.isNumeric(taskCountInput))
                        {
                            nCMM.TAC = taskCountInput;
                        }
                        else
                        {
                            nCMM.TAC = "0";
                        }
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Tasks, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileTasks, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        nCMM.TAC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Tasks, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileTasks, CommonUtility.dbConnection);
                        }
                    }
                    var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                    if (isTicket != null && isTicket.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(ticketCountInput) && CommonUtility.isNumeric(ticketCountInput))
                        {
                            nCMM.TIC = ticketCountInput;
                        }
                        else
                        {
                            nCMM.TIC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Ticketing, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Database, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileTicketing, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        nCMM.TIC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Ticketing, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Database, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileTicketing, CommonUtility.dbConnection);
                        }
                    }
                    var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                    if (isPO != null && isPO.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(poCountInput) && CommonUtility.isNumeric(poCountInput))
                        {
                            nCMM.PPC = poCountInput;
                        }
                        else
                        {
                            nCMM.PPC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.PostOrder, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobilePostOrder, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        nCMM.PPC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.PostOrder, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobilePostOrder, CommonUtility.dbConnection);
                        }
                    }
                    //var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                    //if (isVeri != null && isVeri.Count > 0)
                    //{
                    //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Verifier, dbConnection, infoData.Email, 0, infoData.Id);
                    //    UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileVerifier, dbConnection, infoData.Email, 0, infoData.Id);
                    //}
                    //else
                    //{
                    //    foreach (var u in gallusers)
                    //    {
                    //        UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Verifier, CommonUtility.dbConnection);
                    //        UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileVerifier, CommonUtility.dbConnection);
                    //    }
                    //}
                    var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                    if (isChat != null && isChat.Count > 0)
                    {


                        if (!string.IsNullOrEmpty(chatCountInput) && CommonUtility.isNumeric(chatCountInput))
                        {
                            nCMM.CHC = chatCountInput;
                        }
                        else
                        {
                            nCMM.CHC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Chat, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileConversation, dbConnection, infoData.Email, 0, infoData.Id);

                    }
                    else
                    {
                        nCMM.CHC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Chat, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileConversation, CommonUtility.dbConnection);
                        }
                    }
                    var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                    if (isDR != null && isDR.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(drCountInput) && CommonUtility.isNumeric(drCountInput))
                        {
                            nCMM.DRC = drCountInput;
                        }
                        else
                        {
                            nCMM.DRC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.DutyRoster, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileDutyRoster, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        nCMM.DRC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.DutyRoster, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileDutyRoster, CommonUtility.dbConnection);
                        }
                    }
                    var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                    if (isLF != null && isLF.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(lfCountInput) && CommonUtility.isNumeric(lfCountInput))
                        {
                            nCMM.LFC = lfCountInput;
                        }
                        else
                        {
                            nCMM.LFC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.LostandFound, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileLostAndFound, dbConnection, infoData.Email, 0, infoData.Id);


                    }
                    else
                    {
                        nCMM.LFC = "0";

                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.LostandFound, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileLostAndFound, CommonUtility.dbConnection);
                        }
                    }
                    var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                    if (isSurv != null && isSurv.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(surCountInput) && CommonUtility.isNumeric(surCountInput))
                        {
                            nCMM.SUC = surCountInput;
                        }
                        else
                        {
                            nCMM.SUC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Surveillance, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileSurveillance, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        nCMM.SUC = "0";

                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Surveillance, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileSurveillance, CommonUtility.dbConnection);
                        }
                    }
                    var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                    if (isRequest != null && isRequest.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(reqCountInput) && CommonUtility.isNumeric(reqCountInput))
                        {
                            nCMM.REC = reqCountInput;
                        }
                        else
                        {
                            nCMM.REC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Request, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileRequest, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        nCMM.REC = "0";

                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Request, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileRequest, CommonUtility.dbConnection);
                        }
                    }
                    var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                    if (isCollab != null && isCollab.Count > 0)
                    {
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Collaboration, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileCollaboration, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Collaboration, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileCollaboration, CommonUtility.dbConnection);
                        }
                    }
                    var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                    if (isDisp != null && isDisp.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(disCountInput) && CommonUtility.isNumeric(disCountInput))
                        {
                            nCMM.DIC = disCountInput;
                        }
                        else
                        {
                            nCMM.DIC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Dispatch, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {

                        nCMM.DIC = "0";

                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Dispatch, CommonUtility.dbConnection);
                        }
                    }
                    var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Location).ToString()).ToList();
                    if (isLoc != null && isLoc.Count > 0)
                    {
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Location, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileLocation, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Location, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileLocation, CommonUtility.dbConnection);
                        }
                    }
                    var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                    if (isAct != null && isAct.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(actCountInput) && CommonUtility.isNumeric(actCountInput))
                        {
                            nCMM.ACC = actCountInput;
                        }
                        else
                        {
                            nCMM.ACC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Activity, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileActivity, dbConnection, infoData.Email, 0, infoData.Id);
                    }
                    else
                    {
                        nCMM.ACC = "0";

                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Activity, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileActivity, CommonUtility.dbConnection);
                        }
                    }
                    var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                    if (isWare != null && isWare.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(wareCountInput) && CommonUtility.isNumeric(wareCountInput))
                        {
                            nCMM.WAC = wareCountInput;
                        }
                        else
                        {
                            nCMM.WAC = "0";
                        }

                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.Warehouse, dbConnection, infoData.Email, 0, infoData.Id);
                        UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileAsset, dbConnection, infoData.Email, 0, infoData.Id);
                        //UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.MobileKey, dbConnection, infoData.Email, 0);
                        //UserModules.InsertUserModuleMap(newUser.ID, (int)Accounts.ModuleTypes.ServerKey, dbConnection, infoData.Email, 0);
                    }
                    else
                    {
                        nCMM.WAC = "0";
                        foreach (var u in gallusers)
                        {
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.Warehouse, CommonUtility.dbConnection);
                            UserModules.DeleteUserModuleMapByUserIdAndModuleId(u.ID, (int)Accounts.ModuleTypes.MobileAsset, CommonUtility.dbConnection);
                        }
                    }
                    nCMM.CreatedBy = userinfo.Username;
                    nCMM.CreatedDate = CommonUtility.getDTNow().ToString();
                    nCMM.UpdatedBy = userinfo.Username;
                    nCMM.UpdatedDate = CommonUtility.getDTNow().ToString();

                    CustomerModuleMap.InsertCustomerModuleMap(nCMM, dbConnection, dbConnectionAudit, true);
                } 
                listy = "SUCCESS";
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Devices", "changeUserState", er, CommonUtility.dbConnection, 0);
                listy = er.Message;
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getCustomerInfo(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                listy.Add("SUCCESS");
                var getCustomerInfo = CustomerInfo.GetCustomerInfoById(id, dbConnection);
                listy.Add(getCustomerInfo.Address);
                listy.Add(getCustomerInfo.Country);
                listy.Add(getCustomerInfo.Email);
                listy.Add(getCustomerInfo.Name);
                listy.Add(getCustomerInfo.PhoneNo);
                listy.Add(getCustomerInfo.TotalUser.ToString());

                var modules = getCustomerInfo.Modules.Split('?');
                //CHANGE  TO MESSAGEBOARD
                var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                if (isNoti != null && isNoti.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                if (isInci != null && isInci.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                if (isTask != null && isTask.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                if (isTicket != null && isTicket.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                if (isPO != null && isPO.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                if (isVeri != null && isVeri.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                if (isChat != null && isChat.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                if (isDR != null && isDR.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                if (isLF != null && isLF.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                if (isSurv != null && isSurv.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                if (isRequest != null && isRequest.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                if (isCollab != null && isCollab.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                if (isDisp != null && isDisp.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");
                //CHANGED TO CONTRACT
                var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                if (isLoc != null && isLoc.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                if (isWare != null && isWare.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                if (isAct != null && isAct.Count > 0)
                    listy.Add("true");
                else
                    listy.Add("false");

                listy.Add(getCustomerInfo.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                listy.Add(getCustomerInfo.LastName);

                listy.Add(getCustomerInfo.MessageBoardCount.ToString());
                listy.Add(getCustomerInfo.IncidentCount.ToString());
                listy.Add(getCustomerInfo.TaskCount.ToString());
                listy.Add(getCustomerInfo.TicketCount.ToString());
                listy.Add(getCustomerInfo.PostOrderCount.ToString());
                listy.Add(getCustomerInfo.WarehouseCount.ToString());
                listy.Add(getCustomerInfo.ChatCount.ToString());
                listy.Add(getCustomerInfo.DutyRosterCount.ToString());
                listy.Add(getCustomerInfo.LostFoundCount.ToString());
                listy.Add(getCustomerInfo.ContractCount.ToString());
                listy.Add(getCustomerInfo.RequestCount.ToString());
                listy.Add(getCustomerInfo.SurveillanceCount.ToString());
                listy.Add(getCustomerInfo.DispatchCount.ToString());
                listy.Add(getCustomerInfo.ActivityCount.ToString());

            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Customers", "getCustomerInfo", err, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add(err.Message);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                var devinuse = 0;
                foreach (var dev in getalldevs)
                {
                    if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                        devinuse++;
                }
                var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                if (licenseInfo != null)
                {
                    var remaining = (cInfo.TotalUser - users).ToString();
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A"); //Remaining Client
                    listy.Add("N/A"); //Total Client
                    listy.Add("N/A"); //Used Client
                    listy.Add(remaining); // Remaining Mobile
                    listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                    listy.Add(users.ToString()); // Used
                    //licenseInfo.RemainingMobileUsers = remaining;
                    //licenseInfo.UsedMobileUsers = users.ToString();

                    listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                    listy.Add(licenseInfo.isNotification.ToString().ToLower());
                    listy.Add(licenseInfo.isLocation.ToString().ToLower());
                    listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                    listy.Add(licenseInfo.isTask.ToString().ToLower());
                    listy.Add(licenseInfo.isIncident.ToString().ToLower());
                    listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                    listy.Add(licenseInfo.isChat.ToString().ToLower());
                    listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                    listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                    listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                    listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                    listy.Add(licenseInfo.isVerification.ToString().ToLower());
                    listy.Add(licenseInfo.isRequest.ToString().ToLower());
                    listy.Add(licenseInfo.isDispatch.ToString().ToLower());

                    //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("TaskDash", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
    }
}