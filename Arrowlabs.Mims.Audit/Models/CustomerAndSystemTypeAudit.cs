﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class CustomerAndSystemTypeAudit
    {
        public int Id { get; set; }
        public int AuditBaseId { get; set; }
        public int CustomerId { get; set; }
        public int CustSystemTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public int CustomerInfoId { get; set; }
        public BaseAudit BaseAudit { get; set; }

        public static int InsertCustomerAndSystemTypeAudit(CustomerAndSystemTypeAudit customerAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(customerAudit.BaseAudit, dbConnection);

            int id = 0;
            if (customerAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomerAndSystemTypeAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customerAudit.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = customerAudit.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = customerAudit.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@CustSystemTypeId", SqlDbType.Int));
                    cmd.Parameters["@CustSystemTypeId"].Value = customerAudit.CustSystemTypeId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customerAudit.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = customerAudit.UpdatedDate;


                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customerAudit.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customerAudit.UpdatedBy;


                    cmd.CommandText = "InsertCustomerAndSystemTypeAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                }
            }
            return id;
        }
    }
}
