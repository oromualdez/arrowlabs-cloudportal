﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Arrowlabs.Mims.Audit.Models
{
    public class PlateSourceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Description_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int CustomerId { get; set; }
        public int SiteId { get; set; }
        
        private static PlateSourceAudit GetPlateSourceAuditFromSqlReader(SqlDataReader reader)
        {
            var plateSource = new PlateSourceAudit();
            plateSource.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
           
            if (columns.Contains("CODE") && reader["CODE"] != DBNull.Value)
                plateSource.Code = reader["CODE"].ToString();
           
            if (columns.Contains("DESCRIPTION") && reader["DESCRIPTION"] != DBNull.Value)
                plateSource.Description = reader["DESCRIPTION"].ToString();
            
            if (columns.Contains("DESCRIPTION_AR") && reader["DESCRIPTION_AR"] != DBNull.Value)
                plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
            
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                plateSource.CreatedBy = reader["CreatedBy"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                plateSource.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return plateSource;
        }
        public static List<PlateSourceAudit> GetAllPlateSourceAudit(string dbConnection)
        {
            var plateSources = new List<PlateSourceAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllPlateSourceAudit", connection);
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var plateSource = GetPlateSourceAuditFromSqlReader(reader);
                    plateSources.Add(plateSource);
                }
                connection.Close();
                reader.Close();
            }
            return plateSources;
        }

        public static bool InsertPlateSourceAudit(PlateSourceAudit plateSource, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(plateSource.BaseAudit, dbConnection);

            if (plateSource != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertPlateSourceAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = plateSource.ID;

                    cmd.Parameters.Add("@CODE", SqlDbType.NVarChar).Value = plateSource.Code;

                    cmd.Parameters.Add("@DESCRIPTION", SqlDbType.NVarChar).Value = plateSource.Description;

                    cmd.Parameters.Add("@DESCRIPTION_AR", SqlDbType.NVarChar).Value = plateSource.Description_AR;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = plateSource.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = plateSource.CreatedBy;

                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = plateSource.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = plateSource.CustomerId;
                    

                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


    }
}
