﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Arrowlabs.Business.Layer;
using System.IO;
using System.Text;
using System.Configuration;
using ArrowLabs.Licence.Portal.Helpers;

namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for LicenseHandler
    /// </summary>
    public class LicenseHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                context.Response.Expires = -1;

                foreach (string file in context.Request.Files)
                {
                    HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
                    string FileName = string.Empty;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        string[] files = hpf.FileName.Split(new char[] { '\\' });
                        FileName = files[files.Length - 1];
                    }
                    else
                    {
                        FileName = hpf.FileName;
                    }
                    if (hpf.ContentLength == 0)
                        continue;

                    string savedFileName = context.Server.MapPath("~/Uploads/" + FileName);
                    string ext = Path.GetExtension(savedFileName);
                    if (ext.ToLower() == ".arl")
                    {
                        hpf.SaveAs(savedFileName);
                        var bytes = File.ReadAllBytes(savedFileName); ;
                        var licenceInfo = Decrypt.DecryptData(Encoding.ASCII.GetString(bytes), true, dbConnection);
                        var splitLicence = licenceInfo.Split('#');
                        var oldLic = ClientLicence.GetLicenseByClientID(splitLicence[0], dbConnection);
                        if (oldLic != null)
                        {
                            if (splitLicence[2] == "MOC")
                            {
                                oldLic.TotalServerClients = splitLicence[1];
                                oldLic.ActiveServerClients = splitLicence[1];
                                oldLic.UpdatedBy = file;
                                oldLic.UpdatedDate = DateTime.Now.ToString();
                                ClientLicence.InsertClientLicence(oldLic, dbConnection);
                                context.Response.Write("Success");
                            }
                            else
                            {
                                if (splitLicence[1] == oldLic.Name)
                                {
                                    oldLic.TotalDevices = splitLicence[4];
                                    oldLic.Curfew = splitLicence[5];
                                    oldLic.TotalMobileUsers = splitLicence[9];
                                    oldLic.ExpiryDate = splitLicence[6];
                                    oldLic.UpdatedBy = file;
                                    oldLic.UpdatedDate = DateTime.Now.ToString();

                                    ClientLicence.InsertClientLicence(oldLic, dbConnection);
                                    if (splitLicence.Length > 10)
                                    {
                                        oldLic.Modules = splitLicence[10];
                                        ClientLicence.InsertClientLicenceModuleByClientID(oldLic, dbConnection);
                                    }

                                    context.Response.Write("Success");
                                }
                                else
                                {
                                    context.Response.Write("Uploaded license does not match licence.");
                                }
                            }
                        }
                        else
                        {
                            context.Response.Write("Previous licence was not found!");
                        }
                    }
                    else
                    {
                        context.Response.Write("Uploaded file type does not match licence.");
                    }
                }
            }
            catch(Exception EX)
            {
                MIMSLog.MIMSLogSave("LicenseHandler", "ProcessRequest", EX, CommonUtility.dbConnection);
                context.Response.Write("Problem occured while trying to upload license.-" + EX.Message);
            }
        }
        string dbConnection = ConfigurationManager.ConnectionStrings["ArrowLabsPortal"].ConnectionString;
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}