﻿using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace ArrowLabs.Licence.Portal
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //Session.Timeout = 720;
            var sessionId = Session.SessionID;
            Session["SessionId"] = sessionId;
            sessionId = (String)Session["SessionId"];
            if (sessionId == null || String.IsNullOrEmpty(sessionId))
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (ex is HttpRequestValidationException)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.Write(@"
                <html><head><title>HTML Not Allowed</title>
                <script language='JavaScript'><!--
                function back() { history.go(-1); } //--></script></head>
                <body style='font-family: Arial, Sans-serif;'>
                <h1>Oops!</h1>
                <p>I'm sorry, but HTML entry is not allowed on that page.</p>
                <p>Please make sure that your entries do not contain 
                any angle brackets like &lt; or &gt;.</p>
                <p><a href='javascript:back()'>Go back</a></p>
                </body></html>
                ");
                Response.End();
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
            var sessionId = (String)Session["SessionId"];
            if (sessionId != null && !String.IsNullOrEmpty(sessionId))
            {
                Arrowlabs.Business.Layer.LoginSession.LogoutUserBySessionId(sessionId, CommonUtility.dbConnection);
                Session["SessionId"] = "";
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}