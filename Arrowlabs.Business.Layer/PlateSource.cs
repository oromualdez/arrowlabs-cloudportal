﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
  public  class PlateSource
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Description_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int CustomerId { get; set; }

        public int SiteId { get; set; }

        public static List<PlateSource> GetAllPlateSource(string dbConnection)
        {
            var plateSources = new List<PlateSource>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllPlateSource", connection);


                sqlCommand.CommandText = "GetAllPlateSource";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateSource = new PlateSource();

                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        plateSource.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        plateSource.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateSources.Add(plateSource);
                }
                connection.Close();
                reader.Close();
            }
            return plateSources;
        }

        public static List<PlateSource> GetAllPlateSourceByPlateCode(int id, string dbConnection)
        {
            var plateSources = new List<PlateSource>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllPlateSourceByPlateCode", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllPlateSourceByPlateCode";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateSource = new PlateSource();

                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        plateSource.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        plateSource.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateSources.Add(plateSource);
                }
                connection.Close();
                reader.Close();
            }
            return plateSources;
        }
        public static List<PlateSource> GetAllPlateSourceByCId(int id,string dbConnection)
        {
            var plateSources = new List<PlateSource>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllPlateSourceByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllPlateSourceByCId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateSource = new PlateSource();

                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        plateSource.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        plateSource.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateSources.Add(plateSource);
                }
                connection.Close();
                reader.Close();
            }
            return plateSources;
        }

        public static List<PlateSource> GetAllPlateSourceBySiteId(int id, string dbConnection)
        {
            var plateSources = new List<PlateSource>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllPlateSourceBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllPlateSourceBySiteId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateSource = new PlateSource();

                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        plateSource.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        plateSource.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateSources.Add(plateSource);
                }
                connection.Close();
                reader.Close();
            }
            return plateSources;
        }

        public static List<PlateSource> GetAllPlateSourceByLevel5(int id, string dbConnection)
        {
            var plateSources = new List<PlateSource>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllPlateSourceByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllPlateSourceByLevel5";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateSource = new PlateSource();

                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        plateSource.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        plateSource.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateSources.Add(plateSource);
                }
                connection.Close();
                reader.Close();
            }
            return plateSources;
        }

        public static List<PlateSource> GetAllPlateSourceByDate(DateTime updatedDate, string dbConnection)
        {
            var plateSources = new List<PlateSource>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllPlateSourceByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetAllPlateSourceByDate";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateSource = new PlateSource();

                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        plateSource.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        plateSource.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateSources.Add(plateSource);
                }
                connection.Close();
                reader.Close();
            }
            return plateSources;
        }

        public static int InsertorUpdatePlateSource(PlateSource plateSource, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            if (plateSource != null)
            {
                id = plateSource.ID;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdatePlateSource", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = plateSource.ID;

                    cmd.Parameters.Add(new SqlParameter("@CODE", SqlDbType.NVarChar));
                    cmd.Parameters["@CODE"].Value = plateSource.Code;

                    cmd.Parameters.Add(new SqlParameter("@DESCRIPTION", SqlDbType.NVarChar));
                    cmd.Parameters["@DESCRIPTION"].Value = plateSource.Description;

                    cmd.Parameters.Add(new SqlParameter("@DESCRIPTION_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@DESCRIPTION_AR"].Value = plateSource.Description_AR;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = plateSource.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = plateSource.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = plateSource.CreatedBy.ToLower();

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = plateSource.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = plateSource.SiteId;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            plateSource.ID = id;
                            var audit = new PlateSourceAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, plateSource.UpdatedBy);
                            Reflection.CopyProperties(plateSource, audit);
                            PlateSourceAudit.InsertPlateSourceAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdatePlateSource", ex, dbConnection);
                    }

                }
            }
            return id;
        }


        public static bool DeletePlateSourceById(int colorCode, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeletePlateSourceById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = colorCode;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeletePlateSourceById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static PlateSource GetPlateSourceByName(string name, string dbConnection)
        {
            PlateSource plateSource = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetPlateSourceByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    plateSource = new PlateSource();
                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                }
                connection.Close();
                reader.Close();
            }
            return plateSource;
        }

        public static PlateSource GetPlateSourceById(int id, string dbConnection)
        {
            PlateSource plateSource = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetPlateSourceById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    plateSource = new PlateSource();
                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    plateSource.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                }
                connection.Close();
                reader.Close();
            }
            return plateSource;
        }

        public static PlateSource GetPlateSourceByNameAndCId(string name,int id, string dbConnection)
        {
            PlateSource plateSource = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetPlateSourceByNameAndCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    plateSource = new PlateSource();
                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                }
                connection.Close();
                reader.Close();
            }
            return plateSource;
        }

        public static PlateSource GetPlateSourceByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            PlateSource plateSource = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetPlateSourceByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    plateSource = new PlateSource();
                    plateSource.ID = Convert.ToInt32(reader["Id"].ToString());
                    plateSource.Code = reader["CODE"].ToString();
                    plateSource.Description = reader["DESCRIPTION"].ToString();
                    plateSource.Description_AR = reader["DESCRIPTION_AR"].ToString();
                    plateSource.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateSource.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateSource.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                }
                connection.Close();
                reader.Close();
            }
            return plateSource;
        }
    }
}
