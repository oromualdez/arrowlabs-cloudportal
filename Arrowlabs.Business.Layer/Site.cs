﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class Site
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public int CustomerInfoId { get; set; }
        public string CustomerUName { get; set; }

        public string Long { get; set; }

        public string Lati { get; set; }

        public string MessageBoardVideoPath { get; set; }

        public string RssFeedUrl { get; set; } 
         

        private static Site GetSiteFromSqlReader(SqlDataReader reader)
        {
            var group = new Site();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                group.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["Name"] != DBNull.Value)
                group.Name = reader["Name"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                group.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["Updateddate"] != DBNull.Value)
                group.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                group.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedBy"] != DBNull.Value)
                group.CreatedBy = reader["UpdatedBy"].ToString();

            if (columns.Contains( "CustomerInfoId") && reader["CustomerInfoId"] != DBNull.Value)
                group.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"]);

            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                group.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains( "Lati") && reader["Lati"] != DBNull.Value)
                group.Lati = reader["Lati"].ToString();

            if (columns.Contains( "Long") && reader["Long"] != DBNull.Value)
                group.Long = reader["Long"].ToString();

            if (columns.Contains("MessageBoardVideoPath") && reader["MessageBoardVideoPath"] != DBNull.Value)
                group.MessageBoardVideoPath = reader["MessageBoardVideoPath"].ToString();
            if (columns.Contains("RssFeedUrl") && reader["RssFeedUrl"] != DBNull.Value)
                            group.RssFeedUrl = reader["RssFeedUrl"].ToString();
            
             
            return group;
        }
        public static List<Site> GetAllSite(string dbConnection)
        {
            var groups = new List<Site>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSites", connection);
                sqlCommand.CommandText = "GetAllSites";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var group = GetSiteFromSqlReader(reader);
                    groups.Add(group);
                }
                connection.Close();
                reader.Close();
            }
            return groups;
        }

        public static List<Site> GetAllSiteByLevel5(int id, string dbConnection)
        {
            var groups = new List<Site>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSiteByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = id;
                sqlCommand.CommandText = "GetAllSiteByLevel5";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var group = GetSiteFromSqlReader(reader);
                    groups.Add(group);
                }
                connection.Close();
                reader.Close();
            }
            return groups;
        }

        public static Site GetSiteById(int id,string dbConnection)
        {
            Site group = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetSiteById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandText = "GetSiteById";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    group = GetSiteFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return group;
        }

        public static Site GetSiteByName(string name, string dbConnection)
        {
            Site group = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetSiteByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandText = "GetSiteByName";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    group = GetSiteFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return group;
        }
        public static Site GetSiteByNameAndCId(string name,int id ,string dbConnection)
        {
            Site group = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetSiteByNameAndCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandText = "GetSiteByNameAndCId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    group = GetSiteFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return group;
        }
        public static bool DeleteSiteById(int groupId, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteSiteById", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = groupId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteSiteById";

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "DeleteSiteById", ex, dbConnection);
                return false;
            }
        }
        public static int InsertorUpdateSite(Site site, string dbConnection,
    string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int siteId = site.Id;
            var action = (siteId == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;


            var id = 0;
            var isNew = false;
            if (site.Id == 0)
            {
                isNew = true;
            }
            if (site != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateSite", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = site.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = site.Name;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = site.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = site.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = site.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = site.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Lati", SqlDbType.NVarChar));
                    cmd.Parameters["@Lati"].Value = site.Lati;

                    cmd.Parameters.Add(new SqlParameter("@Long", SqlDbType.NVarChar));
                    cmd.Parameters["@Long"].Value = site.Long;

                    cmd.Parameters.Add(new SqlParameter("@MessageBoardVideoPath", SqlDbType.NVarChar));
                    cmd.Parameters["@MessageBoardVideoPath"].Value = site.MessageBoardVideoPath;


                    cmd.Parameters.Add(new SqlParameter("@RssFeedUrl", SqlDbType.NVarChar));
                    cmd.Parameters["@RssFeedUrl"].Value = site.RssFeedUrl;
                    

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = site.CustomerInfoId;
                    

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertorUpdateSite";

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    if (siteId == 0)
                        siteId = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            site.Id = siteId;
                            var audit = new SiteAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, site.UpdatedBy);
                            Reflection.CopyProperties(site, audit);
                            SiteAudit.InsertSiteAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateSite", ex, dbConnection);
                    }
                }
            }

            return id;
        }
    }
}
