﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class NotificationChatLookUp
    {    
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int NotificationId { get; set; }
        public string UpdatedBy { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public bool IsRead { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public int GroupId { get; set; }

        public static bool InsertorUpdateNotificationChatLookUp(NotificationChatLookUp notificationChatLookUp,
            string dbConnection,   string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (notificationChatLookUp != null)
            {
                int id = notificationChatLookUp.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateNotificationChatLookUp", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notificationChatLookUp.Id;

                    cmd.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.Int));
                    cmd.Parameters["@NotificationId"].Value = notificationChatLookUp.NotificationId;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = notificationChatLookUp.UserId;

                    cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                    cmd.Parameters["@GroupId"].Value = notificationChatLookUp.GroupId;
                    

                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = notificationChatLookUp.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@IsRead", SqlDbType.Bit));
                    cmd.Parameters["@IsRead"].Value = notificationChatLookUp.IsRead;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notificationChatLookUp.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = notificationChatLookUp.UpdatedDate;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            notificationChatLookUp.Id = id;
                            var audit = new NotificationChatLookUpAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, notificationChatLookUp.UpdatedBy);
                            Reflection.CopyProperties(notificationChatLookUp, audit);
                            NotificationChatLookUpAudit.InsertNotificationChatLookUpAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateNotificationChatLookUp", ex, dbConnection);
                    }

                }
                return true;
            }
            return false;
        }
        
    }
}
