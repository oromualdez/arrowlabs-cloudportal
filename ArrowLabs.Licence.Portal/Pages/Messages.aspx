﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.Messages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
         <style>
             
                          #newMessages {
    overflow-y:scroll;
} 
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}

	#record { height: 15vh; }
	#record.recording { 
		background: red;
		background: -webkit-radial-gradient(center, ellipse cover, #ff0000 0%,lightgrey 75%,lightgrey 100%,#7db9e8 100%); 
		background: -moz-radial-gradient(center, ellipse cover, #ff0000 0%,lightgrey 75%,lightgrey 100%,#7db9e8 100%); 
		background: radial-gradient(center, ellipse cover, #ff0000 0%,lightgrey 75%,lightgrey 100%,#7db9e8 100%); 
	}
	#save, #save img { height: 10vh; }
	#save { opacity: 0.25;}
	#save[download] { opacity: 1;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript" src="https://testportalcdn.azureedge.net/Scripts/recorderworker.js"></script> 
    <script type="text/javascript" src="https://testportalcdn.azureedge.net/Scripts/audiodisplay.js"></script>
    <script type="text/javascript" src="https://testportalcdn.azureedge.net/Scripts/audiomain.js"></script>
    <script type="text/javascript" src="https://testportalcdn.azureedge.net/Scripts/recorder.js"></script>

    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        var loggedinUsername = '<%=senderName2%>';
        //User-Profile
        function changePassword() {
            try {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;
                var isErr = false;
                if (!isErr) { 
                    if (!letterGood) {
                        showAlert('Password does not contain letter');
                        isErr = true;
                    }
                    if (!isErr) {
                        if (!capitalGood) {
                            showAlert('Password does not contain capital letter');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!numGood) {
                            showAlert('Password does not contain number');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!lengthGood) {
                            showAlert('Password length not enough');
                            isErr = true;
                        }
                    }
                }
                if (!isErr) {
                    if (newPw == confPw && newPw != "" && confPw != "") {
                        $.ajax({
                            type: "POST",
                            url: "Messages.aspx/changePW",
                            data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                } else {
                                    jQuery('#changePasswordModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                    jQuery('#successfulDispatch').modal('show');
                                    document.getElementById("newPwInput").value = "";
                                    document.getElementById("confirmPwInput").value = "";
                                    document.getElementById("oldPwInput").value = confPw;
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        showAlert("Kindly match new password with confirm password.")
                    }
                }
            }
            catch (ex)
            { showAlert('Error 60: Problem loading page element.-' + ex) }
        }
        function editUnlock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
            document.getElementById("editProfileA").style.display = "none";
            document.getElementById("saveProfileA").style.display = "block";
            document.getElementById("profileEmailAddDIV").style.display = "none";
            document.getElementById("profileEmailAddEditDIV").style.display = "block";
            document.getElementById("userFullnameSpanDIV").style.display = "none";
            document.getElementById("userFullnameSpanEditDIV").style.display = "block";
            if (document.getElementById('profileRoleName').innerHTML == "User") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "block";
                document.getElementById("dirInfoDIV").style.display = "none";


            }
            else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "none";
                document.getElementById("dirInfoDIV").style.display = "block";
            }
        }
        function editJustLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        function editLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
            var uID = document.getElementById('rowidChoice').text;
            var role = document.getElementById('UserRoleSelector').value;
            var roleid = 0;
            var supervisor = 0;
            if (role == "User") {
                supervisor = document.getElementById('MainContent_editmanagerpickerSelect').value;
                if (supervisor > 0) {
                    roleid = 3;
                }
                else {
                    roleid = 5;
                }
            }
            else if (role == "Manager") {
                supervisor = document.getElementById('MainContent_editdirpickerSelect').value;
                roleid = 2;
            }
            else if (role == "Director") {
                roleid = 1;
            }

            var retVal = saveUserProfile(uID, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
            if (retVal > 0) {
                assignUserProfileData(retVal);
                showAlert('Successfully updated user info.');
            }
            else {
                alert('Error 62: Problem occured while trying to get user profile.');
            }

        }
        function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
            var output = 0;
            $.ajax({
                type: "POST",
                url: "Messages.aspx/addUserProfile",
                data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                }
            });
            return output;
        }
        function saveTZ() {
            var scountr = $("#countrySelect option:selected").text();
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/saveTZ",
                data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                        jQuery('#successfulDispatch').modal('show');
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getserverInfo() {
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getServerData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                         
                        document.getElementById('mobileRemaining').value = data.d[9];
                        document.getElementById('mobileTotal').value = data.d[10];
                        document.getElementById('mobileUsed').value = data.d[11];

                        document.getElementById("countrySelect").value = data.d[28];
                        jQuery('#countrySelect').selectpicker('val', data.d[28]);


                        document.getElementById('surveillanceCheck').checked = false;
                        document.getElementById('notificationCheck').checked = false;
                        document.getElementById('locationCheck').checked = false;
                        document.getElementById('ticketingCheck').checked = false;
                        document.getElementById('taskCheck').checked = false;
                        document.getElementById('incidentCheck').checked = false;
                        document.getElementById('warehouseCheck').checked = false;
                        document.getElementById('chatCheck').checked = false;
                        document.getElementById('collaborationCheck').checked = false;
                        document.getElementById('lfCheck').checked = false;
                        document.getElementById('dutyrosterCheck').checked = false;
                        document.getElementById('postorderCheck').checked = false;
                        document.getElementById('verificationCheck').checked = false;
                        document.getElementById('requestCheck').checked = false;
                        document.getElementById('dispatchCheck').checked = false;
                        document.getElementById('activityCheck').checked = false;

                        if (data.d[12] == "true")
                            document.getElementById('surveillanceCheck').checked = true;
                        if (data.d[13] == "true")
                            document.getElementById('notificationCheck').checked = true;
                        if (data.d[14] == "true")
                            document.getElementById('locationCheck').checked = true;
                        if (data.d[15] == "true")
                            document.getElementById('ticketingCheck').checked = true;
                        if (data.d[16] == "true")
                            document.getElementById('taskCheck').checked = true;
                        if (data.d[17] == "true")
                            document.getElementById('incidentCheck').checked = true;
                        if (data.d[18] == "true")
                            document.getElementById('warehouseCheck').checked = true;
                        if (data.d[19] == "true")
                            document.getElementById('chatCheck').checked = true;
                        if (data.d[20] == "true")
                            document.getElementById('collaborationCheck').checked = true;
                        if (data.d[21] == "true")
                            document.getElementById('lfCheck').checked = true;
                        if (data.d[22] == "true")
                            document.getElementById('dutyrosterCheck').checked = true;
                        if (data.d[23] == "true")
                            document.getElementById('postorderCheck').checked = true;
                        if (data.d[24] == "true")
                            document.getElementById('verificationCheck').checked = true;
                        if (data.d[25] == "true")
                            document.getElementById('requestCheck').checked = true;
                        if (data.d[26] == "true")
                            document.getElementById('dispatchCheck').checked = true;
                        if (data.d[27] == "true")
                            document.getElementById('activityCheck').checked = true;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function assignUserProfileData(uId) {
            document.getElementById('rowidChoice').text = uId;
            try {
                $.ajax({
                    type: "POST",
                    url: "Messages.aspx/getUserProfileData",
                    data: "{'id':'" + uId + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            try {
                                document.getElementById('containerDiv2').style.display = "none";
                                document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                document.getElementById('profileRoleName').innerHTML = data.d[5];
                                document.getElementById('profileManagerName').innerHTML = data.d[6];
                                document.getElementById('userStatusSpan').innerHTML = data.d[8];

                                if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                    document.getElementById('containerDiv2').style.display = "block";
                                    document.getElementById('defaultGenderDiv').style.display = "none";
                                    getserverInfo();
                                }

                                var el = document.getElementById('userStatusIconSpan');
                                if (el) {
                                    el.className = data.d[9];
                                }
                                document.getElementById('userprofileImgSrc').src = data.d[10];
                                document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                document.getElementById('userFirstnameSpan').value = data.d[13];
                                document.getElementById('userLastnameSpan').value = data.d[14];
                                document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                document.getElementById('profileEmailAddEdit').value = data.d[3];

                                document.getElementById('oldPwInput').value = data.d[16];

                                document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                document.getElementById('profileGender').innerHTML = data.d[20];
                            }
                            catch (err) { alert(err) }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            catch (err)
            { alert('Error 60: Problem loading page element.-' + err) }
        }
        function forceLogout() {
            document.getElementById('<%= closingbtn.ClientID %>').click();
        }
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }

        function onTextRadio() {

            document.getElementById('sendAlarmMessagetxtArea').style.display = "block";
            document.getElementById('viz').style.display = "none";
            document.getElementById('controls').style.display = "none";


        }

        function onAudioRadio() {
            document.getElementById('sendAlarmMessagetxtArea').style.display = "none";
            document.getElementById('viz').style.display = "block";
            document.getElementById('controls').style.display = "block";

        }

        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
        jQuery(document).ready(function () {



            'use strict';

            // basic style
            jQuery('#calendar1').clndr();

            // with inverse bg
            jQuery('#calendar2').clndr();


            // calendar
            var moment = window.moment,
            currentMonth = moment().format('YYYY-MM'),
            nextMonth = moment().add('month', 1).format('YYYY-MM'),
            myEvents = [{ date: currentMonth + '-' + '10', title: 'Persian Kitten Auction', location: 'Center for Beautiful Cats' },
            { date: currentMonth + '-' + '19', title: 'Cat Frisbee', location: 'Jefferson Park' },
            { date: currentMonth + '-' + '23', title: 'Kitten Demonstration', location: 'Center for Beautiful Cats' },
            { date: nextMonth + '-' + '07', title: 'Small Cat Photo Session', location: 'Center for Cat Photography' }];

            jQuery('#calendar3').clndr({
                template: $('#full-clndr-template').html(),
                daysOfTheWeek: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                events: myEvents,
                targets: {
                    nextButton: 'clndr-next-btn',
                    previousButton: 'clndr-prev-btn',
                    nextYearButton: 'clndr-next-year-btn',
                    previousYearButton: 'clndr-prev-year-btn',
                    todayButton: 'clndr-today-btn',
                    day: 'day',
                    empty: 'empty'
                },
            });
            
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getCalendarDays",
                data: "{'id':'99','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById('calendarRow1').innerHTML = data.d[0];
                            document.getElementById('calendarRow2').innerHTML = data.d[1];
                            document.getElementById('calendarRow3').innerHTML = data.d[2];
                            document.getElementById('calendarRow4').innerHTML = data.d[3];
                            document.getElementById('calendarRow5').innerHTML = data.d[4];
                            document.getElementById('dateHeader').innerHTML = data.d[5];
                            document.getElementById('currentdate').text = data.d[6];
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getMessages",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            for (var i = 0; i < data.d.length; i++) {
                                $("#messagesTable tbody").append(data.d[i]);
                            }
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

            try {
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    if ($(e.target).attr('href') == "#home-tab" || $(e.target).attr('href') == "#reminders-tab")
                        localStorage.setItem('activeTabMessage', $(e.target).attr('href'));
                    //alert($(e.target).attr('href')+'----------NOW Selected');
                });
                var activeTab = localStorage.getItem('activeTabMessage');
                if (activeTab) {
                    //alert(activeTab +'--------Selected');
                    //$('#myTab a[href="' + activeTab + '"]').tab('show'); 
                    jQuery('a[data-toggle="tab"][href="' + activeTab + '"]').tab('show');
                }
                localStorage.removeItem("activeTabDev");
                localStorage.removeItem("activeTabInci");
                localStorage.removeItem("activeTabTask");
                localStorage.removeItem("activeTabTick");
                localStorage.removeItem("activeTabUB");
                localStorage.removeItem("activeTabVer");
                localStorage.removeItem("activeTabLost");

                $('input[type=password]').keyup(function () {
                    // keyup event code here
                    var pswd = $(this).val();
                    if (pswd.length < 8) {
                        $('#length').removeClass('valid').addClass('invalid');
                        lengthGood = false;
                    } else {
                        $('#length').removeClass('invalid').addClass('valid');
                        lengthGood = true;
                    }
                    //validate letter
                    if (pswd.match(/[A-z]/)) {
                        $('#letter').removeClass('invalid').addClass('valid');
                        letterGood = true;

                    } else {
                        $('#letter').removeClass('valid').addClass('invalid');
                        letterGood = false;
                    }

                    //validate capital letter
                    if (pswd.match(/[A-Z]/)) {
                        $('#capital').removeClass('invalid').addClass('valid');
                        capitalGood = true;
                    } else {
                        $('#capital').removeClass('valid').addClass('invalid');

                        capitalGood = false;
                    }

                    //validate number
                    if (pswd.match(/\d/)) {
                        $('#number').removeClass('invalid').addClass('valid');
                        numGood = true;

                    } else {
                        $('#number').removeClass('valid').addClass('invalid');
                        numGood = false;
                    }
                });
                $('input[type=password]').focus(function () {
                    // focus code here
                    $('#pswd_info').show();
                });
                $('input[type=password]').blur(function () {
                    // blur code here
                    $('#pswd_info').hide();
                });
            } catch (err)
            { alert(err); }

            addreminders();
            userTableData();
            getReminderSelectorDates();
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = mm + '/' + dd + '/' + yyyy;
            getReminderTableData(today,"");
        });
        function dateback()
        {
            var id = document.getElementById('currentdate').text;
            document.getElementById('calendarRow1').innerHTML = "";
            document.getElementById('calendarRow2').innerHTML = "";
            document.getElementById('calendarRow3').innerHTML = "";
            document.getElementById('calendarRow4').innerHTML = "";
            document.getElementById('calendarRow5').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getCalendarDays",
                data: "{'id':'" + (parseInt(id) - 1) + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {

                        if (data.d.length > 0) {
                            document.getElementById('calendarRow1').innerHTML = data.d[0];
                            document.getElementById('calendarRow2').innerHTML = data.d[1];
                            document.getElementById('calendarRow3').innerHTML = data.d[2];
                            document.getElementById('calendarRow4').innerHTML = data.d[3];
                            document.getElementById('calendarRow5').innerHTML = data.d[4];
                            document.getElementById('dateHeader').innerHTML = data.d[5];
                            document.getElementById('currentdate').text = data.d[6];
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function dateforward()
        {
            var id = document.getElementById('currentdate').text;
            document.getElementById('calendarRow1').innerHTML = "";
            document.getElementById('calendarRow2').innerHTML = "";
            document.getElementById('calendarRow3').innerHTML = "";
            document.getElementById('calendarRow4').innerHTML = "";
            document.getElementById('calendarRow5').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getCalendarDays",
                data: "{'id':'" + (parseInt(id) + 1) + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else { 
                        if (data.d.length > 0) {
                            document.getElementById('calendarRow1').innerHTML = data.d[0];
                            document.getElementById('calendarRow2').innerHTML = data.d[1];
                            document.getElementById('calendarRow3').innerHTML = data.d[2];
                            document.getElementById('calendarRow4').innerHTML = data.d[3];
                            document.getElementById('calendarRow5').innerHTML = data.d[4];
                            document.getElementById('dateHeader').innerHTML = data.d[5];
                            document.getElementById('currentdate').text = data.d[6];
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function reminderClose() {
            $("#messagesTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getMessages",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            for (var i = 0; i < data.d.length; i++) {
                                $("#messagesTable tbody").append(data.d[i]);
                            }
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

            addreminders();
            userTableData();
            getReminderSelectorDates();
        }
        function reminderChoice(id, time, description, name, color) {
            document.getElementById('rowidReminderChoice').text = id;
            document.getElementById(color + 'RemName').innerHTML = name;
            document.getElementById(color + 'RemDate').innerHTML = time;
            document.getElementById(color + 'RemDesc').innerHTML = description;
        }
        function reminderRead() {
            var id = document.getElementById('rowidReminderChoice').text;
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/markReminderAsRead",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "SUCCESS") {
                        getReminderTableData(document.getElementById('reminderDateTableHeader').innerHTML, "");
                        addreminders();
                    }
                    else {
                        showAlert("Error 71: Problem occured while trying to mark reminder as read. - " + data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function reminderDelete() {
            var id = document.getElementById('rowidReminderChoice').text;
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/deleteReminder",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "SUCCESS") {
                        showLoader();
                        location.reload();
                    }
                    else {
                        showAlert("Error 72: Problem occured while trying to delete reminder. - " + data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function reminderBack() {
            getReminderTableData(document.getElementById('reminderDateTableHeader').innerHTML, "sub");
        }
        function reminderForward() {
            getReminderTableData(document.getElementById('reminderDateTableHeader').innerHTML, "add");
        }
        function getReminderTableData(id,state)
        {
            document.getElementById('reminderDateRow7AM').innerHTML = "";
            document.getElementById('reminderDateRow8AM').innerHTML = "";
            document.getElementById('reminderDateRow9AM').innerHTML = "";
            document.getElementById('reminderDateRow10AM').innerHTML = "";
            document.getElementById('reminderDateRow11AM').innerHTML = "";
            document.getElementById('reminderDateRow12NN').innerHTML = "";
            document.getElementById('reminderDateRow1PM').innerHTML = "";
            document.getElementById('reminderDateRow2PM').innerHTML = "";
            document.getElementById('reminderDateRow3PM').innerHTML = "";
            document.getElementById('reminderDateRow4PM').innerHTML = "";
            document.getElementById('reminderDateRow5PM').innerHTML = "";
            document.getElementById('reminderDateRow6PM').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getReminderTableData",
                data: "{'id':'" + id + "','state':'" + state + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('reminderDateTableHeader').innerHTML = data.d[0];
                        var div = document.createElement('div');
                        div.className = 'row';
                        div.innerHTML = data.d[1];
                        document.getElementById('reminderDateRow7AM').appendChild(div);

                        var div2 = document.createElement('div');
                        div2.className = 'row';
                        div2.innerHTML = data.d[2];
                        document.getElementById('reminderDateRow8AM').appendChild(div2);

                        var div3 = document.createElement('div');
                        div3.className = 'row';
                        div3.innerHTML = data.d[3];
                        document.getElementById('reminderDateRow9AM').appendChild(div3);

                        var div4 = document.createElement('div');
                        div4.className = 'row';
                        div4.innerHTML = data.d[4];
                        document.getElementById('reminderDateRow10AM').appendChild(div4);

                        var div5 = document.createElement('div');
                        div5.className = 'row';
                        div5.innerHTML = data.d[5];
                        document.getElementById('reminderDateRow11AM').appendChild(div5);

                        var div6 = document.createElement('div');
                        div6.className = 'row';
                        div6.innerHTML = data.d[6];
                        document.getElementById('reminderDateRow12NN').appendChild(div6);

                        var div7 = document.createElement('div');
                        div7.className = 'row';
                        div7.innerHTML = data.d[7];
                        document.getElementById('reminderDateRow1PM').appendChild(div7);

                        var div8 = document.createElement('div');
                        div8.className = 'row';
                        div8.innerHTML = data.d[8];
                        document.getElementById('reminderDateRow2PM').appendChild(div8);

                        var div9 = document.createElement('div');
                        div9.className = 'row';
                        div9.innerHTML = data.d[9];
                        document.getElementById('reminderDateRow3PM').appendChild(div9);

                        var div10 = document.createElement('div');
                        div10.className = 'row';
                        div10.innerHTML = data.d[10];
                        document.getElementById('reminderDateRow4PM').appendChild(div10);

                        var div11 = document.createElement('div');
                        div11.className = 'row';
                        div11.innerHTML = data.d[11];
                        document.getElementById('reminderDateRow5PM').appendChild(div11);

                        var div12 = document.createElement('div');
                        div12.className = 'row';
                        div12.innerHTML = data.d[12];
                        document.getElementById('reminderDateRow6PM').appendChild(div12);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
            
        }
        function fromReminderDateChange()
        {
            var select = document.getElementById("toReminderDate");
            document.getElementById("toReminderDate").options.length = 0;
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getDateRangeReminder",
                data: "{'date':'" + document.getElementById('fromReminderDate2').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var opt = document.createElement('option');
                            opt.value = data.d[i];
                            opt.innerHTML = data.d[i];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getReminderSelectorDates()
        {
            var select1 = document.getElementById('fromReminderDate2');
            var select2 = document.getElementById('toReminderDate');
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getReminderSelectorDates",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var opt = document.createElement('option');
                            opt.value = data.d[i];
                            opt.innerHTML = data.d[i];
                            select1.appendChild(opt);
                            var opt2 = document.createElement('option');
                            opt2.value = data.d[i];
                            opt2.innerHTML = data.d[i];
                            select2.appendChild(opt2);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addreminders() {
            document.getElementById('divReminder').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getReminders",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'help-block mb-4x';

                            div.innerHTML = data.d[i];

                            document.getElementById('divReminder').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        function userTableData() {
            jQuery("#usersTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getUserTableData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#usersTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function isEmptyOrSpaces(str) {
            return str === null || str.match(/^ *$/) !== null;
        }
        function newReminderInsert() {
            var topic = document.getElementById("tbReminderName").value;
            var reminder = document.getElementById("tbReminderNotes").value;
            var remdate = document.getElementById("tbReminderDate").value;
            var todate = document.getElementById("toReminderDate").value;
            var fromdate = document.getElementById("fromReminderDate2").value;
    
            var colorcode = 0;
            if (document.getElementById('cbRemBlue').checked) {
                colorcode = 1;
            }
            else if (document.getElementById('cbRemGreen').checked) {
                colorcode = 2;
            }
            else if (document.getElementById('cbRemYellow').checked) {
                colorcode = 3;
            }
            else if (document.getElementById('cbRemRed').checked) {
                colorcode = 0;
            }
            var isPass = true;
            if (isEmptyOrSpaces(topic)) {
                isPass = false;
                showAlert("Kindly provide reminder topic");
            }
            else {
                if (isSpecialChar(topic)) {
                    isPass = false;
                    showAlert("Kindly remove special character reminder topic");
                }
            }
            if (!isEmptyOrSpaces(reminder)) {
                if (isSpecialChar(reminder)) {
                    isPass = false;
                    showAlert("Kindly remove special character reminder notes");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Messages.aspx/insertNewReminder",
                    data: "{'name':'" + topic + "','date':'" + remdate + "','reminder':'" + reminder + "','colorcode':'" + colorcode + "','todate':'" + todate + "','fromdate':'" + fromdate + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newReminder').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Reminder created.";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showAlert("Error 63: Problem occured while trying to insert reminder. - " + data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function getAssigneeType(stringVal,intid) {
            var output = "";
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getAssigneeType",
                data: "{'uname':'" + stringVal + "','id':'" + intid  + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
            return output;
        }
        function sendAlarmNotification() {
            try {
                if (document.getElementById('TextRadio').checked) {
                    hideLoader();
                    var msg = document.getElementById("sendAlarmMessagetxtArea").value;
                    if (!isEmptyOrSpaces(msg)) {
                        if (!isSpecialChar(msg)) {
                            if (msg.indexOf('-') > -1) {
                                showAlert("Kindly remove special character '-' from the message.");
                            }
                            else {
                                var list = document.getElementById("sendToListBox");
                                if (list.length > 0) {
                                    var selected = [];
                                    for (i = 0; i < list.length; i++) {
                                        var retType = getAssigneeType(list.options[i].text, list.options[i].value);
                                        if (retType == "User") {
                                            chat.server.sendtoUser(btoa(list.options[i].text), msg, list.options[i].value);
                                            selected.push(list.options[i].text);
                                        }
                                        else if (retType == "Device") {
                                            chat.server.sendtoDevice(btoa(list.options[i].text), msg);
                                        }
                                        else {
                                            chat.server.sendtoGroup(btoa(list.options[i].text), list.options[i].value, msg);
                                            sendpushNotification(list.options[i].value, list.options[i].text, retType, msg, msg);
                                        }
                                    }

                                    if (selected.length > 0)
                                        sendpushMultipleNotification(selected, msg);

                                    jQuery('#newMessages').modal('hide');
                                    document.getElementById("sendAlarmMessagetxtArea").value = "";
                                    document.getElementById('successincidentScenario').innerHTML = "Message has been successfully sent";
                                    jQuery('#successfulDispatch').modal('show');
                                    cleardispatchList();
                                }
                                else {
                                    showAlert("Kindly provide to whom you want to send message to");
                                }
                            }
                        }
                    }
                    else {
                        showAlert("Kindly provide message you want to send");
                    }
                } else {
               
                    var audio = document.getElementById("audioSave");
                    var hidefile = document.getElementById("hidefile").value;
                    var list = document.getElementById("sendToListBox");

                    if (list.length > 0 && audio.src != '') {
                        //var file = ;
                        jQuery('#newMessages').modal('hide');
                        var xhr = new XMLHttpRequest();
                        xhr.open('GET', audio.src, true);
                        xhr.responseType = 'blob';
                        xhr.onload = function (e) {
                            if (this.status == 200) {
                                var file = new File([this.response], uuidv4().split('-')[0] + '.wav', {
                                    lastModified: new Date(0), // optional - default = now
                                    type: "audio/wav" // optional - default = ''
                                });
                                var data = new FormData();
                                //data.append(file.name, file);
                                data.append(file.name, file);

                                jQuery.ajax({
                                    url: "../Handlers/AudioMessageHandler.ashx",
                                    type: "POST",
                                    data: data,
                                    contentType: false,
                                    processData: false,
                                    success: function (result) {

                                        var list = document.getElementById("sendToListBox");
                                        if (list.length > 0) {
                                            var selected = [];
                                            msg = "Voice Note"
                                            for (i = 0; i < list.length; i++) {

                                                var retType = getAssigneeType(list.options[i].text, list.options[i].value);
                                                if (retType == "User") {
                                                    chat.server.sendtoUserAudio(btoa(list.options[i].text), msg, list.options[i].value, btoa(result));
                                                    selected.push(list.options[i].text);
                                                }
                                                else if (retType == "Device") {
                                                    chat.server.sendtoDevice(btoa(list.options[i].text), msg);
                                                }
                                                else {
                                                    chat.server.sendtoGroupAudio(btoa(list.options[i].text), list.options[i].value, msg, btoa(result));
                                                    sendpushNotification(list.options[i].value, list.options[i].text, retType, msg, msg);
                                                }
                                            }

                                            if (selected.length > 0)
                                                sendpushMultipleNotification(selected, msg);

                                            jQuery('#newMessages').modal('hide');
                                            hideLoader();
                                            audio.src = '';
                                            audio.style.display = 'none';
                                            document.getElementById("sendAlarmMessagetxtArea").value = "";
                                            document.getElementById('successincidentScenario').innerHTML = "Message has been successfully sent";
                                            jQuery('#successfulDispatch').modal('show');
                                            cleardispatchList();

                                        } 
                                        else { 
                                            jQuery('#newMessages').modal('show');
                                            showAlert("Kindly provide to whom you want to send message to");
                                            hideLoader();
                                        }
                                        //myDropzone.removeAllFiles(true);
                                    },
                                    error: function (err) {
                                    }
                                });

                            }
                        };
                        xhr.send();

                    }

                    else {  
                        showAlert("Kindly provide to whom you want to send message or Record the audio!");
                        hideLoader();
                    }
                }
            }
            catch (ex) {
                
                showAlert('Error 60: Problem loading page element.-' + ex);
                hideLoader();
            }
        }
        function uuidv4() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
        function sendpushNotification(id, aName, aType, name, msg) {
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/pushNotificationSend",
                data: "{'id':'" + id + "','assigneename':'" + aName + "','assigneetype':'" + aType + "','name':'" + name + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                traditional: true,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function cleardispatchList() {
            try {
                var elSel = document.getElementById('sendToListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {
                    var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                    var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                    document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                    document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                    var element = document.getElementById("li-" + elSel.options[i].text);
                    if (element) {
                        element.parentNode.removeChild(element);
                        elSel.remove(i);
                    }
                }
            }
            catch (err) {
                alert('Error 42: Failed to clear dispatch list-' + err);
            }
        }
        function sendpushMultipleNotification(selectedUserIds, messages) {
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/sendpushMultipleNotification",
                data: JSON.stringify({ userIds: selectedUserIds, msg: messages, uname: loggedinUsername }),
                dataType: "json",
                traditional: true,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function removeFromList(name) {
            var elSel = document.getElementById('sendToListBox');
            var i;
            for (i = elSel.length - 1; i >= 0; i--) {
                if (elSel.options[i].text == name) {
                    var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                    var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                    document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                    document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                    elSel.remove(i);
                }
            }
        }
        function liOnclickRemove(name,id) {
            removeFromList(name);
            removenameFromDispatchList(name);
            var element = document.getElementById(id + name);
            element.style.color = "#b2163b";
            element.className = "red-color";
            element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
        }
        function addnametoDispatchList(name,id,displayname) {
            var ul = document.getElementById("toDispatchList");
            var li = document.createElement("li");
            li.setAttribute("id", "li-" + name);
            li.innerHTML = '<a href="#"  class="capitalize-text" onclick="liOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;)">' + displayname + '<i class="fa fa-close" onclick="liOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;)"></i></a>';
            ul.appendChild(li);
        }
        function removenameFromDispatchList(name) {
            var element = document.getElementById("li-" + name);
            element.parentNode.removeChild(element);
        }
        function userchoiceTable(id, name,displayname) {
            try{
                var element = document.getElementById(id + name);
                var result = element.innerHTML.indexOf("ADDED");
                if (result < 0) {
                    var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                    if (exists == false) {
                        var myOption;
                        myOption = document.createElement("Option");
                        myOption.text = name; //Textbox's value
                        myOption.value = id; //Textbox's value
                        sendToListBox.add(myOption);
                        document.getElementById("<%=tbUserID.ClientID%>").value = document.getElementById("<%=tbUserID.ClientID%>").value + '-' + id;
                        document.getElementById("<%=tbUserName.ClientID%>").value = document.getElementById("<%=tbUserName.ClientID%>").value + '-' + name;
                        addnametoDispatchList(name,id,displayname);
                    }
                    element.style.color = "#3ebb64";
                    element.className = "green-color";
                    element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
                }
                else {
                    var elSel = document.getElementById('sendToListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) {
                        if (elSel.options[i].value == id) {
                            var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                            var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                            document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                            document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                            removenameFromDispatchList(elSel.options[i].text);
                            elSel.remove(i);
                        }
                    }
                    element.style.color = "#b2163b";
                    element.className = "red-color";
                    element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                }
            }
            catch(err)
            {
                alert(err)
            }
        }
        function messageviewChoice(id) {
            document.getElementById('rowidmessageChoice').text = id;
            document.getElementById('userhistorymessagelist').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/getUserMsgView",
                data: "{'id':'" + document.getElementById('rowidmessageChoice').text + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d[0] == "SUCCESS") {
                        for (var i = 1; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('userhistorymessagelist').appendChild(div);
                        }
                        //for (var i = 1; i < data.d.length; i++) {
                        //    var res = data.d[i].split("-");
                        //    var ul = document.getElementById("userhistorymessagelist");
                        //    var li = document.createElement("li");

                        //    li.innerHTML = '<a style="margin-left:5px;cursor: default;" href="#" class="capitalize-text" >' + res[0] + '-' + res[1] + '</a>';

                        //    ul.appendChild(li);
                        //}
                    }
                    else {
                        showAlert(data.d[0]);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function messageChoice(id) {
            document.getElementById('rowidmessageChoice').text = id;
        }
        function deleteMessage() {
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/deleteMessage",
                data: "{'id':'" + document.getElementById('rowidmessageChoice').text + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "SUCCESS") {
                        showLoader();
                        location.reload();
                    }
                    else {
                        showAlert('Error 45: Failed to delete message.-' + data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function resendMessage() {
            jQuery.ajax({
                type: "POST",
                url: "Messages.aspx/resendMessage",
                data: "{'id':'" + document.getElementById('rowidmessageChoice').text + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            if (data.d[3] == "0") {
                                var selected = [];

                                if (data.d[4] == "False")
                                    chat.server.sendtoUser(btoa(data.d[1]), data.d[0], data.d[2]);
                                else
                                    chat.server.sendtoUserAudio(btoa(data.d[1]), "Voice Note", data.d[2], btoa(data.d[5]));

                                selected.push(data.d[1]);

                                if (selected.length > 0)
                                    sendpushMultipleNotification(selected, data.d[0]);
                            }
                            else { 
                                if (data.d[4] == "False")
                                    chat.server.sendtoGroup(btoa(data.d[1]), data.d[2], data.d[0]);
                                else
                                    chat.server.sendtoGroupAudio(btoa(data.d[1]), data.d[2], "Voice Note", btoa(data.d[5]));
                                 
                                sendpushNotification(data.d[2], data.d[1], "Group", data.d[0], data.d[0]);
                            }
                            jQuery('#newMessages').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Message has been successfully re sent";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else {
                            showAlert("Error 46: Error occured trying to send message.");
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    </script>
<section class="content-wrapper" role="main">
            <div class="content">
               <div class="content-body">
                  <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                     <div class="panel-heading">
                        <div class="row">
                           <div class="col-md-2">
                              <h3 class="panel-title"><span class="hidden-xs">Messages</span></h3>
                           </div>
                           <div class="col-md-7">
                              <div class="panel-control">
                                 <ul id="demo3-tabs" class="nav nav-tabs nav-main">
                                    <li <%=isMessagesDisplay%> <%=messagesDisplay%>><a data-toggle="tab" href="#home-tab">Messages</a>
                                    </li>
                                    <li <%=remindersDisplay%>><a data-toggle="tab" href="#reminders-tab">Reminders</a>
                                    </li>                                    
                                 </ul>
                                 <!-- /.nav -->
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div role="group" class="pull-right">
                                  <%=siteName%>
                                 <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData(1)'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                              <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                             <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                    </div>
                           </div>
                        </div>
                     </div>
                     <div class="panel-body">
                           <div class="tab-content">
                              <div class="row mb-4x">
                                     <div class="tab-content">
                                     <div class="tab-pane fade <%=actmessagesDisplay%>" id="home-tab">
                                    <div class="row show-component component-number-1">
                                                                         <div class="col-md-2">
                                    <div class="row vertical-navigation new-events">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs nav-contrast-dark main-navigation">
                                             <li <%=isMessagesDisplay%> ><a href="#" data-target="#newMessages" data-toggle="modal" class="capitalize-text">+ NEW MESSAGE</a>
                                             </li>
                                             <li><a href="#" data-target="#newReminder" data-toggle="modal" class="capitalize-text">+ NEW REMINDER</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                 </div>
                                       <div class="col-md-10">
                                          <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable panel-no-fill" data-init-panel="true">
                                             <div class="panel-heading mb-2x">
                                                <div class="row no-gutter">
                                                   <div class="col-md-6">
                                                      <h3 class="panel-title capitalize-text">SENT MESSAGES</h3>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <label style="display:none;" class="select select-o customized-drop-down">
                                                         <select>
                                                            <option>Arrange by</option>
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                         </select>
                                                      </label>
                                                   </div>                                                   
                                                </div>                                             
                                                <div class="row no-gutter gray-border">
                                                   <div class="col-md-12">
                                                      <input type="search" class="form-control gray-color datatable-search" placeholder="Search"><i class="fa fa-search fa-1x red-color"></i>
                                                      <div class="clearfx"></div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="table-responsive">
                                                   <table id="messagesTable" class="table table-condensed table-noborder table-striped bordered-top datatable-table" role="grid" order-of-rows="desc">
                                                      <thead>
                                                         <tr style="display:none" role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                            <th class="less-padding" tabindex="0" rowspan="1" colspan="1" aria-label="" >
                                                               <div style="display:none;" class="nice-checkbox">
                                                                  <input id="majorCheckbox-1" type="checkbox" name="majorCheckbox" />
                                                                  <label for="majorCheckbox-1"></label>
                                                               </div>
                                                            </th>
                                                            <th tabindex="0" rowspan="1" colspan="1" aria-label="" >
                                                            <a style="display:none"  class="delete-container right-block-align" href="#">
                                                                  <i class="fa fa-trash light-color"></i>
                                                                  <span class="light-gray">Delete Selected</span>                         
                                                                  </a>
                                                            </th>
                                                         </tr>
                                                      </thead>
                                                      <tbody>
                                                                                                                                     
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    </div>
                                     <div class="tab-pane fade <%=actremindersDisplay%>"" id="reminders-tab">
                                                                          <div class="col-md-2">
                                    <div class="row vertical-navigation new-events">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs nav-contrast-dark main-navigation">
                                             <li <%=isMessagesDisplay%> ><a href="#" data-target="#newMessages" data-toggle="modal" class="capitalize-text">+ NEW MESSAGE</a>
                                             </li>
                                             <li><a href="#" data-target="#newReminder" data-toggle="modal" class="capitalize-text">+ NEW REMINDER</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                 </div>
                                         <div class="col-md-7">
                                         <div data-fill-color="true" class="panel fade in reminder-event" data-init-panel="true">
                                                                                <div class="panel-heading">
                                                            <a class="btn btn-nofill btn-default pull-left" onclick="reminderBack()" href="#"><i class="icon-arrow-left fa fa-2x white-color"></i></a>
                                                            <a class="btn btn-nofill btn-default pull-right"  onclick="reminderForward()" href="#"><i class="icon-arrow-right white-color fa fa-2x"></i></a>
                                                            <h3 class="month lead text-center help-block mt-2x white-color" id="reminderDateTableHeader"></h3>
                                                                                </div>
                                                                                <div class="panel-body no-padding">
                                                                                        <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:778px">
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      7 am
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow7AM" >
                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      8 am
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow8AM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      9 am
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow9AM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      10 am
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow10AM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      11 am
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow11AM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      12 nn
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow12NN">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      1 pm
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow1PM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      2 pm
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow2PM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      3 pm
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow3PM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      4 pm
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow4PM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      5 pm
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow5PM">

                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="row border-bottom">
                                                                                             <div class="col-md-1 border-right">
                                                                                                   <h3> 
                                                                                                      6 pm
                                                                                                   </h3>
                                                                                             </div>
                                                                                             <div class="col-md-11" id="reminderDateRow6PM">

                                                                                             </div>
                                                                                          </div>                                                                            
                                                                                        </div>
                                                                                        <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                                        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                                </div>
                                                                                <!-- /.panel-body -->
                                                                            </div>
                                         </div>
                                         <div class="col-md-3">
<div class="row">
<div class="col-md-12 no-hpadding">
              <div id="calendar3">
                <script type="text/template" id="full-clndr-template">
                  <div class="row">
                    <div class="col-md-12 no-hpadding gray-border">
                      <div class="clndr-ctrl btn-toolbar red-background">
                        <button class="btn btn-nofill btn-default pull-left" onclick="dateback();return false;"><i class="icon-arrow-left fa fa-2x white-color"></i></button>
                        <button class="btn btn-nofill btn-default pull-right" onclick="dateforward();return false;"><i class="icon-arrow-right white-color fa fa-2x"></i></button>
                        <p class="month lead text-center help-block mt-2x white-color" id="dateHeader"></p>
                      </div>
                      <table id="calendarTable" class="clndr-table" border="0" cellspacing="0" cellpadding="0">
                         <thead>
                        <tr class="header-days">
                            <td class="header-day">Sun</td>
                            <td class="header-day">Mon</td>
                            <td class="header-day">Tue</td>
                            <td class="header-day">Wed</td>
                            <td class="header-day">Thu</td>
                            <td class="header-day">Fri</td>
                            <td class="header-day">Sat</td>
                          </tr>
                        </thead>
                         <tbody>
                             <tr id="calendarRow1">
                             </tr>
                             <tr id="calendarRow2">
                             </tr>
                             <tr id="calendarRow3">
                             </tr>
                             <tr id="calendarRow4">
                             </tr>
                             <tr id="calendarRow5">
                             </tr>
                        </tbody>
                      </table>
                    </div><!-- /.cols -->
                  </div><!-- /.row</script>
              </div><!-- /#calendar3 -->


</div>
</div>
                                <div class="row border-gray-rl pt-2x">
                                   <div class="col-md-12">
                                      <p class="current-time text-center">

                                      </p>
                                   </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12 no-hpadding">
                                        <div data-context="success" class="panel fade in panel-transparent" data-init-panel="true">
                                            <div class="panel-heading">
                                                <h3 class="panel-title capitalize-text">HIGHLIGHTS FOR TODAY</h3>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div id="divReminder">
                                                </div>    
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>  

                                        </div>
                                        </div>

                                        </div>
                              </div>
                                     
<div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div  class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>    
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                  			<label class="select select-o" >
                                                                                <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>     
                                                                     <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                                      <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Contract</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                   <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                         <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                        
                                            </div>
                                                         <div class="row" style="display:none;">
                                            <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                                            
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>  
                                     </div> 
                                         </div>             
                                  </div>
                           </div>
                        </div>
        
                     </div>
                  </div>
                  <!-- /tab-content -->
               </div>
               <!-- /panel-body -->
            </div>
            <!-- /.panel -->
            <div aria-hidden="true" aria-labelledby="newMessages" role="dialog" tabindex="-1" id="newMessages" class="modal fade videoModal" style="display: none;">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <div class="row">
                           <div class="col-md-11">
                              <h4 class="modal-title capitalize-text">SEND A MESSAGE</h4>
                           </div>
                           <div class="col-md-1">
                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                           </div>
                        </div>
                     </div>
                     <div class="modal-body">
                        <div class="row mb-3x">
                           <div class="col-md-8">
                                 <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MY USERS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="panel-body">
                                       <div class="table-responsive">
                                          <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" role="grid" id="usersTable" number-of-rows="5">
                                             <thead>
                                                <tr role="row">
                                                   <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="USER" aria-sort="ascending">USER<i class="fa fa-sort ml-2x gray-color"></i>
                                                   </th>
                                                   <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATE<i class="fa fa-sort ml-2x gray-color"></i>
                                                   </th>
                                                   <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                   </th>
                                                </tr>
                                             </thead>
                                             <tbody>

                                             </tbody>
                                          </table>
                                       </div>
                                       <div class="clearfix"></div>
                                    </div>
                                 </div>
                           </div>
                           <div class="col-md-4 border-left">
                              <div class="row border-bottom">
                                       <div class="col-md-1 mt-1x">
                                          <p>
                                             To: 
                                          </p>
                                       </div>

                                    <div class="col-md-11">
                                        <div class="row horizontal-navigation">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs" id="toDispatchList">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                              </div>
                              <div class="row">
                                 <textarea rows="3" class="form-control" id="sendAlarmMessagetxtArea" placeholder="Write your message here"></textarea>

                              </div>
                                 <div class="col-md-12">
                                         <div id="viz" class="text-center" style="display:none" >
                                		    <canvas id="analyser" width="100%" height="20"></canvas>
		                                    <canvas id="wavedisplay" style="display:none;" width="250" height="150"></canvas>
	                                    </div>
	                                    <div id="controls" class="text-center" style="display:none;width:100%">
		                                    <img  id="record" src="https://testportalcdn.azureedge.net/Images/mic128.png" onclick="toggleRecording(this);">
		                                    <a style="display:none" id="save" href="#"><img src="https://testportalcdn.azureedge.net/Images/save.svg"></a>
	                                    <audio  controls id="audioSave" style="display:none;width:100%">
                                            <source src="" type="audio/wav">
                                        </audio>

                                        </div>

                                    </div>
     <div class="row col-md-12 pull-right">
                                           
                                        <div  class="nice-radio inline-block no-vmargin">
                                          <input type="radio" checked="checked" id="TextRadio" onclick="onTextRadio()"  name="niceRadio">
                                          <label for="TextRadio">Message</label>
                                       </div>
                                          <!--/TextRadio> -->
                                       <div  class="nice-radio inline-block no-vmargin">
                                          <input type="radio" id="AudioRadio" onclick="onAudioRadio()" name="niceRadio">
                                          <label for="AudioRadio">Audio</label>
                                        </div><!--/AudioRadio-->              
                                     </div>   
                                     
                           </div>                           
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" onclick="showLoader();sendAlarmNotification()">SEND</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="greenreminderboxs" role="dialog" tabindex="-1" id="greenreminderboxs" class="modal fade reminder-boxs greenModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="greenRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color " id="greenRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                          
                           </div>
                           <div class="row border-top">
                           </div>
                         
                           <div class="row mb-2x">
                              <p id="greenRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                              <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="bluereminderboxs" role="dialog" tabindex="-1" id="bluereminderboxs" class="modal fade reminder-boxs blueModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="blueRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="blueRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                            
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="blueRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                              <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="redreminderboxs" role="dialog" tabindex="-1" id="redreminderboxs" class="modal fade reminder-boxs redModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="redRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="redRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="redRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                              <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="yellowreminderboxs" role="dialog" tabindex="-1" id="yellowreminderboxs" class="modal fade reminder-boxs yellowModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="yellowRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="yellowRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                            
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="yellowRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                              <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="newReminder" role="dialog" tabindex="-1" id="newReminder" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">NEW REMINDER</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Reminder Name" id="tbReminderName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
                                          <div class="input-group input-group-in">
                                            <input id="tbReminderDate" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">
                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                    </div>
								</div>
                                <div class="row">
									<div class="col-md-6">
                                      <p class="font-bold red-color no-vmargin" >From:</p>
                                        <label class="select select-o">
                                         <select id="fromReminderDate2" onchange="fromReminderDateChange()" >
                                         </select>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                      <p class="font-bold red-color no-vmargin" >To:</p>
                                        <label class="select select-o">
                                             <select id="toReminderDate" >
                                             </select>
                                         <//label>
                                    </div>
								</div>
		                        <div class="row">
                                  <div class="col-md-4">
                                     <p>Reminder Color</p>
                                  </div>
                                  <div class="col-md-8">
                                     <div class="nice-radio filled-color blue-radio nice-radio-inline inline-block no-vmargin">
                                        <input type="radio" name="niceRadioAlt" id="cbRemBlue" checked="checked" class="hidden radio-o"> 
                                        <label for="cbRemBlue" class="vertical-align-top"></label>
                                     </div>
                                     <div class="nice-radio filled-color green-radio nice-radio-inline inline-block">
                                        <input type="radio" name="niceRadioAlt" id="cbRemGreen" class="hidden radio-o">
                                        <label for="cbRemGreen" class="vertical-align-top"></label>
                                     </div>
                                     <div class="nice-radio filled-color yellow-radio nice-radio-inline inline-block">
                                        <input type="radio" name="niceRadioAlt" id="cbRemYellow" class="hidden radio-o">
                                        <label for="cbRemYellow" class="vertical-align-top"></label>
                                     </div>
                                     <div class="nice-radio filled-color lightred-radio nice-radio-inline inline-block">
                                        <input type="radio" name="niceRadioAlt" id="cbRemRed" class="hidden radio-o">
                                        <label for="cbRemRed" class="vertical-align-top"></label>
                                     </div>
                                  </div>
                                </div>
								<div class="row">
									<div class="col-md-12">
										<textarea placeholder="Notes" id="tbReminderNotes" class="form-control" rows="3"></textarea>
									</div>
								</div>								
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
									<li><a href="#" data-dismiss="modal" class="capitalize-text">CANCEL</a>
									</li>
                                    <li ><a href="#" class="capitalize-text" onclick="newReminderInsert()">CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <i class="fa fa-smile-o fa-6x green-color"></i>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="deleteModal" role="dialog" tabindex="-1" id="deleteModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                         <div class="row">
                            <h4 class="text-center" id="todeleteLoc"></h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: This action cannot be undone!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li > <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li ><a href="#" data-dismiss="modal" onclick="deleteMessage()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>

    
                <div aria-hidden="true" aria-labelledby="viewmsgModal" role="dialog" tabindex="-1" id="viewmsgModal" class="modal fade" style="display: none;">
				        <div class="modal-dialog modal-sm">
				          <div class="modal-content">
                              <div class="modal-header">
                                                                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                                 <h4 class="modal-title capitalize-text">MESSAGE HISTORY</h4>
                              </div>
					        <div class="modal-body" >
 
                                    <div class="row">
							        <div class="col-md-12">
                                       <div id="userhistorymessagelist" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                                    </div>
                                        <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                 </div>  
					        </div>
                              <div class="modal-footer">
                                  <div class="horizontal-navigation ">
							            <div class="panel-control ">
								            <ul class="nav nav-tabs text-center">
									            <li > <a href="#" data-dismiss="modal">CLOSE</a>
									            </li>	 
								            </ul>
								            <!-- /.nav -->
							            </div>
						            </div>
                                  </div>
				          </div><!-- /.modal-content -->
				        </div><!-- /.modal-dialog -->
			         </div>
            <div aria-hidden="true" aria-labelledby="resendModal" role="dialog" tabindex="-1" id="resendModal" class="modal fade" style="display: none;">
				        <div class="modal-dialog modal-sm">
				          <div class="modal-content">
					        <div class="modal-body" >
                                <div class="row">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                                </div>
                                    <div class="row">
							        <div class="text-center">
                                        <a><i class='fa fa-envelope fa-4x'></i></a>
                                    </div>
                                 </div>
                                <div class="row">
                                    <h4 style="color:gray" class="text-center">Are you sure you want to resend this message?</h4>
                                </div>
                                <div class="row">
						            <div class="horizontal-navigation ">
							            <div class="panel-control ">
								            <ul class="nav nav-tabs text-center">
									            <li> <a href="#" data-dismiss="modal">CANCEL</a>
									            </li>	
                                                <li ><a href="#" data-dismiss="modal" onclick="resendMessage()">SEND</a>
									            </li>	
								            </ul>
								            <!-- /.nav -->
							            </div>
						            </div>
                                </div>
					        </div>
				          </div><!-- /.modal-content -->
				        </div><!-- /.modal-dialog -->
			         </div>
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            		                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
                <select style="color:white;background-color: #5D5D5D;width:125px;height:105px;margin-top:-15px;display:none;" multiple="multiple" id="sendToListBox" ></select>               
              <asp:HiddenField runat="server" ID="tbUserID" />
              <asp:HiddenField runat="server" ID="tbUserName" />
              <input style="display:none;" id="rowidReminderChoice" type="text"/>
              <input style="display:none;" id="rowidmessageChoice" type="text"/>
              <input style="display:none;" id="currentdate" type="text"/>
               <input style="display:none;" id="rowidChoice" type="text"/>
    <input style="display:none;" id="hidefile" type="text"/>
         </section>

         <!-- /MAIN -->
</asp:Content>
