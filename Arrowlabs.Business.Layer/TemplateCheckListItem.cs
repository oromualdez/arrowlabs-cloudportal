﻿namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data.SqlClient;
    using System.Data;
    using System.Runtime.Serialization;
    using Arrowlabs.Mims.Audit.Models;

    public class TemplateCheckListItem
    {
        // Type 1
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string UpdatedBy { get; set; }
        public int ParentCheckListId { get; set; }
        public int ChildCheckListId { get; set; }
        public int CheckListItemType { get; set; }
        public bool IsChecked { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int CustomerId { get; set; }
        public int SiteId { get; set; }
        public string CheckListLocation { get; set; }
        public bool IsCamera { get; set; }
        // Type 2
        public int EntryQuantity { get; set; }
        public int ExitQuantity { get; set; }

        // Mapping Attributes
        public string ParentCheckListName { get; set; }
        public string ChildCheckListName { get; set; }

        private static TemplateCheckListItem GetTemplateCheckListItemFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var templateCheckListItem = new TemplateCheckListItem();

            if (reader["Id"] != DBNull.Value)
                templateCheckListItem.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                templateCheckListItem.Name = reader["Name"].ToString();
            if (reader["Quantity"] != DBNull.Value)
                templateCheckListItem.Quantity = Convert.ToInt32(reader["Quantity"].ToString());
            if (reader["EntryQuantity"] != DBNull.Value)
                templateCheckListItem.EntryQuantity = (reader["EntryQuantity"] != null) ? Convert.ToInt32(reader["EntryQuantity"].ToString()) : 0;
            if (reader["ExitQuantity"] != DBNull.Value)
                templateCheckListItem.ExitQuantity = (reader["ExitQuantity"] != null) ? Convert.ToInt32(reader["ExitQuantity"].ToString()) : 0;
            if (reader["ParentCheckListId"] != DBNull.Value)
                templateCheckListItem.ParentCheckListId = Convert.ToInt32(reader["ParentCheckListId"].ToString());
            if (reader["ChildCheckListId"] != DBNull.Value)
                templateCheckListItem.ChildCheckListId = (!String.IsNullOrEmpty(reader["ChildCheckListId"].ToString())) ? Convert.ToInt32(reader["ChildCheckListId"].ToString()) : 0;

            templateCheckListItem.CheckListItemType = Convert.ToInt32(reader["CheckListItemType"].ToString());
            
            if (reader["IsChecked"] != DBNull.Value)
                templateCheckListItem.IsChecked = Convert.ToBoolean(reader["IsChecked"].ToString().ToLower());



            if (reader["IsDeleted"] != DBNull.Value)
                templateCheckListItem.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString().ToLower());
            if (reader["CreatedDate"] != DBNull.Value)
                templateCheckListItem.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                templateCheckListItem.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                templateCheckListItem.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["ParentName"] != DBNull.Value)
                templateCheckListItem.ParentCheckListName = reader["ParentName"].ToString();

            if (reader["CustomerId"] != DBNull.Value)
                templateCheckListItem.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (reader["SiteId"] != DBNull.Value)
                templateCheckListItem.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            

            if (columns.Contains("CheckListLocation") && reader["CheckListLocation"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["CheckListLocation"].ToString()))
                    templateCheckListItem.CheckListLocation = reader["CheckListLocation"].ToString();
            }
            if (columns.Contains("IsCamera") && reader["IsCamera"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["IsCamera"].ToString()))
                    templateCheckListItem.IsCamera = Convert.ToBoolean(reader["IsCamera"].ToString());
            }
            return templateCheckListItem;
        }

        /// <summary>
        /// It will insert or update check list child item.
        /// </summary>
        /// <param name="checkListItem"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static int InsertorUpdateTemplateCheckListItem(TemplateCheckListItem checkListItem, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            if (checkListItem != null)
            {
                id = checkListItem.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateTemplateCheckListItem", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = checkListItem.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = checkListItem.Name;

                    cmd.Parameters.Add(new SqlParameter("@ParentCheckListId", SqlDbType.Int));
                    cmd.Parameters["@ParentCheckListId"].Value = checkListItem.ParentCheckListId;

                    cmd.Parameters.Add(new SqlParameter("@ChildCheckListId", SqlDbType.Int));
                    cmd.Parameters["@ChildCheckListId"].Value = checkListItem.ChildCheckListId;

                    cmd.Parameters.Add(new SqlParameter("@Quantity", SqlDbType.Int));
                    cmd.Parameters["@Quantity"].Value = checkListItem.Quantity;

                    cmd.Parameters.Add(new SqlParameter("@EntryQuantity", SqlDbType.Int));
                    cmd.Parameters["@EntryQuantity"].Value = checkListItem.EntryQuantity;

                    cmd.Parameters.Add(new SqlParameter("@ExitQuantity", SqlDbType.Int));
                    cmd.Parameters["@ExitQuantity"].Value = checkListItem.ExitQuantity;

                    cmd.Parameters.Add(new SqlParameter("@CheckListItemType", SqlDbType.Int));
                    cmd.Parameters["@CheckListItemType"].Value = checkListItem.CheckListItemType;

                    cmd.Parameters.Add(new SqlParameter("@IsChecked", SqlDbType.Int));
                    cmd.Parameters["@IsChecked"].Value = checkListItem.IsChecked;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = checkListItem.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = checkListItem.SiteId;
                    
                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = checkListItem.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = checkListItem.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = checkListItem.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = checkListItem.CreatedBy;

                                        cmd.Parameters.Add(new SqlParameter("@IsCamera", SqlDbType.Bit));
                    cmd.Parameters["@IsCamera"].Value = checkListItem.IsCamera;

                                        cmd.Parameters.Add(new SqlParameter("@CheckListLocation", SqlDbType.NVarChar));
                    cmd.Parameters["@CheckListLocation"].Value = checkListItem.CheckListLocation;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            checkListItem.Id = id;
                            var audit = new TemplateCheckListItemAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, checkListItem.UpdatedBy);
                            Reflection.CopyProperties(checkListItem, audit);
                            TemplateCheckListItemAudit.InsertTemplateCheckListItemAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateTemplateCheckListItem", ex, auditDbConnection);
                    }
                }
            }
            return id;
        }

        /// <summary>
        /// It will return all check list child items.
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<TemplateCheckListItem> GetAllTemplateCheckListItems(string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckListItem>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListItems", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListItemFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }

                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        /// <summary>
        /// It will return all check list child items of specified type.
        /// </summary>
        /// <param name="typeId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<TemplateCheckListItem> GetAllTemplateCheckListItemsByTypeId(int typeId, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckListItem>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListItemsByTypeId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.Int));
                sqlCommand.Parameters["@TypeId"].Value = typeId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListItemFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }  

        /// <summary>
        /// It will return children of check list items.
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<TemplateCheckListItem> GetAllTemplateCheckListItemsChildren(string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckListItem>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListItemsChildren", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListItemFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }

                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }
     
        public static List<TemplateCheckListItem> GetAllTemplateCheckListItemsChildrenByName(string Name,string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckListItem>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListItemsChildrenByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = Name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllTemplateCheckListItemsChildrenByName";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListItemFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }

                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        public static List<TemplateCheckListItem> GetAllTemplateCheckListItemsByParentId(int id, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckListItem>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListItemsByParentId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllTemplateCheckListItemsByParentId";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListItemFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }
        
        public static List<TemplateCheckListItem> GetTemplateCheckListItemsByParentName(string Name, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckListItem>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTemplateCheckListItemsByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = Name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTemplateCheckListItemsByName";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListItemFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        public static TemplateCheckListItem GetAllTemplateCheckListItemsById(int id, string dbConnection)
        {
            TemplateCheckListItem templateCheckListItems = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListItemsById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllTemplateCheckListItemsById";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    templateCheckListItems = GetTemplateCheckListItemFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        public static int GetLastTemplateCheckListItemId(string dbConnection)
        {
            int lastUserId = 0;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLastTemplateCheckListItemId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    lastUserId = Convert.ToInt32(reader["ID"].ToString());
                    break;
                }
                connection.Close();
                reader.Close();
            }
            return lastUserId;
        }
    }
}
