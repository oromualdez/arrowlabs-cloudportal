﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

namespace Arrowlabs.Mims.Audit.Models
{
    public class HotEventAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public Guid Identifier { get; set; }
        public string Name { get; set; }
        public string Detail1 { get; set; }
        public string Detail2 { get; set; }
        public string Detail3 { get; set; }
        public string Detail4 { get; set; }
        public int CamType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool Status { get; set; }
        public int SiteId { get; set; }

        public static DateTime DateFormater(string dateTime)
        {
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                var format = "MM/dd/yyyy hh:mm:sstt";

                DateTime dt2 = DateTime.Parse(dateTime, provider, System.Globalization.DateTimeStyles.AssumeLocal);
                return DateTime.ParseExact(dateTime, format, provider);

            }
            catch (Exception ex)
            {
                return DateTime.Now;
            }
        }
        private static HotEventAudit GetHotEventAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var hotEventAudit = new HotEventAudit();
            hotEventAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Identifier") && reader["Identifier"] != DBNull.Value)
                hotEventAudit.Identifier = Guid.Parse((reader["Identifier"].ToString()));
            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                hotEventAudit.Name = reader["Name"].ToString();
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                hotEventAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                hotEventAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (columns.Contains("CamType") && reader["CamType"] != DBNull.Value)
                hotEventAudit.CamType = Convert.ToInt16(reader["CamType"].ToString());
            if (columns.Contains("Detail1") && reader["Detail1"] != DBNull.Value)
                hotEventAudit.Detail1 = reader["Detail1"].ToString();
            if (columns.Contains("Detail2") && reader["Detail2"] != DBNull.Value)
                hotEventAudit.Detail2 = reader["Detail2"].ToString();
            if (columns.Contains("Detail3") && reader["Detail3"] != DBNull.Value)
                hotEventAudit.Detail3 = reader["Detail3"].ToString();
            if (columns.Contains("Detail4") && reader["Detail4"] != DBNull.Value)
                hotEventAudit.Detail4 = reader["Detail4"].ToString();
            if (columns.Contains("Status") && reader["Status"] != DBNull.Value)
                hotEventAudit.Status = Convert.ToBoolean(reader["Status"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                hotEventAudit.SiteId = Convert.ToInt16(reader["SiteId"].ToString());
            

            return hotEventAudit;
        }
        public static List<HotEventAudit> GetAllHotEventAudit(string dbConnection)
        {
            var hotEvents = new List<HotEventAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllHotEventAudit";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetHotEventAuditFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }
        public static bool InsertHotEventAudit(HotEventAudit hotEventAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(hotEventAudit.BaseAudit, dbConnection);

            if (hotEventAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertHotEventAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Identifier", SqlDbType.UniqueIdentifier).Value = hotEventAudit.Identifier;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = hotEventAudit.Name;
                    cmd.Parameters.Add("@Detail1", SqlDbType.Xml).Value = hotEventAudit.Detail1;
                    cmd.Parameters.Add("@Detail2", SqlDbType.Xml).Value = hotEventAudit.Detail2;
                    cmd.Parameters.Add("@Detail3", SqlDbType.Xml).Value = hotEventAudit.Detail3;
                    cmd.Parameters.Add("@Detail4", SqlDbType.Xml).Value = hotEventAudit.Detail4;
                    cmd.Parameters.Add("@CamType", SqlDbType.Int).Value = hotEventAudit.CamType;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = hotEventAudit.CreatedBy;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("@Status", SqlDbType.Bit).Value = hotEventAudit.Status;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = hotEventAudit.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
