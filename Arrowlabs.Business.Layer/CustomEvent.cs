﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public static class DataRecordExtensions
    {
        //public static bool HasColumn(this IDataRecord dr, string columnName)
        //{
        //    for (int i = 0; i < dr.FieldCount; i++)
        //    {
        //        if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
        //            return true;
        //    }
        //    return false;
        //}
    }
    [Serializable]
    [DataContract]
    public class CustomEvent
    {
        public enum EventTypes : int
        {
            Unknown = -1,
            DriverOffence = 0,
            HotEvent = 1,
            MilestoneHotEvent = 2,
            MobileHotEvent = 3,
            MobileOffenceEvent = 4,
            Request = 5,
            MobileUserEngage = 6,
            Incident = 7
           
        }
        public enum EscalateTypes : int
        {
            Unknown = -1,
            None = 0,
            Level2 = 1,
            Level3 = 2,
            Level4 = 3

        }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string Notes { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public EventTypes EventType { get; set; }
        [DataMember]
        public Guid Identifier { get; set; }
        [DataMember]
        public int EventId { get; set; }
        [DataMember]
        public DateTime? RecevieTime { get; set; }
        [DataMember]
        public bool IsHotEvent { get; set; }
        [DataMember]
        public bool IsDriverOffence { get; set; }
        [DataMember]
        public string Status { get; set; }
        public string UpdatedBy { get; set; }
        [DataMember]
        public string View { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Event { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longtitude { get; set; }
        [DataMember]
        public string HandledBy { get; set; }
        [DataMember]
        public bool Handled { get; set; }
        [DataMember]
        public DateTime? HandledTime { get; set; }
        [DataMember]
        public string imageURL { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public int AccountId { get; set; }
        [DataMember]
        public string ExtraName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int LocationId { get; set; }
        [DataMember]
        public int IncidentType { get; set; }
        [DataMember]
        public string IncidentTypeName { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int IncidentStatus { get; set; }
        [DataMember]
        public string StatusName { get; set; }
        [DataMember]
        public string Instructions { get; set; }

        [DataMember]
        public string CameraDetails { get; set; }


        [DataMember]
        public List<Users> UserList { get; set; }

        [DataMember]
        public bool AppleNotification = false;

        [DataMember]
        public int TemplateTaskId { get; set; }
        [DataMember]
        public int SiteId   { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string SessionId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerUName { get; set; }
        [DataMember]
        public int Escalate { get; set; }
        [DataMember]
        public bool isViewed = false;

        public string CustomerIncidentId { get; set; }

        [DataMember]
        public string EventTypeName { get; set; }

        [DataMember]
        public int WebPort { get; set; }

        [DataMember]
        public string RequestName { get; set; }

        [DataMember]
        public string MobDescription { get; set; }

        public string CustomerFullName { get; set; }

        public string CustomerHandleFullName { get; set; }

        public enum IncidentTypes
        {
            MobileIncident = 0,
            ThirdParty = 1,
            SystemCreated = 2
        }
        public enum IncidentActionStatus
        {
            Pending = 0,
            Park = 1,
            Dispatch = 2,
            Engage = 3,
            Complete = 4,
            Release = 5,
            Resolve = 6,
            Reject = 7,
            Arrived = 8,
            Leave = 9,
            Escalated = 10,
            Updated = 11,
            OnRoute = 12,
            Pause = 13,
            Resume = 14

        }
        public static List<string> GetIncidentActionStatus()
        {
            return Enum.GetNames(typeof(IncidentActionStatus)).ToList();
        }
        public static List<string> GetIncidentTypes()
        {
            return Enum.GetNames(typeof(IncidentTypes)).ToList();

        }
        public static CustomEvent.EventTypes GetEventTypeValue(int val)
        {
            if ((int)CustomEvent.EventTypes.HotEvent == val)
                return CustomEvent.EventTypes.HotEvent;
            if ((int)CustomEvent.EventTypes.MobileHotEvent == val)
                return CustomEvent.EventTypes.MobileHotEvent;
            if ((int)CustomEvent.EventTypes.Request == val)
                return CustomEvent.EventTypes.Request;
            if ((int)CustomEvent.EventTypes.DriverOffence == val)
                return CustomEvent.EventTypes.DriverOffence;
            if ((int)CustomEvent.EventTypes.Incident == val)
                return CustomEvent.EventTypes.Incident;
            if ((int)CustomEvent.EventTypes.MilestoneHotEvent == val)
                return CustomEvent.EventTypes.MilestoneHotEvent;
            if ((int)CustomEvent.EventTypes.MobileOffenceEvent == val)
                return CustomEvent.EventTypes.MobileOffenceEvent;
            if ((int)CustomEvent.EventTypes.MobileUserEngage == val)
                return CustomEvent.EventTypes.MobileUserEngage;
            return 0;
        }
        public static int GetIncidentStatusValue(string val)
        {
            if (IncidentActionStatus.Dispatch.ToString() == val)
                return (int)IncidentActionStatus.Dispatch;
            else if (IncidentActionStatus.Pending.ToString() == val)
                return (int)IncidentActionStatus.Pending;
            else if (IncidentActionStatus.Park.ToString() == val)
                return (int)IncidentActionStatus.Park;
            else if (IncidentActionStatus.Engage.ToString() == val)
                return (int)IncidentActionStatus.Engage;
            else if (IncidentActionStatus.Release.ToString() == val)
                return (int)IncidentActionStatus.Release;
            else if (IncidentActionStatus.Complete.ToString() == val)
                return (int)IncidentActionStatus.Complete;
            else if (IncidentActionStatus.Reject.ToString() == val)
                return (int)IncidentActionStatus.Reject;
            else if (IncidentActionStatus.Resolve.ToString() == val)
                return (int)IncidentActionStatus.Resolve;
            else if (IncidentActionStatus.Arrived.ToString() == val)
                return (int)IncidentActionStatus.Arrived;
            else if (IncidentActionStatus.Leave.ToString() == val)
                return (int)IncidentActionStatus.Leave;
            else if (IncidentActionStatus.Escalated.ToString() == val)
                return (int)IncidentActionStatus.Escalated;
            return 0;
        }
        private static CustomEvent GetCustomEventFromSqlReader(SqlDataReader reader, string dbConnection)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();


            var notification = new CustomEvent();

            if (reader["EventId"] != DBNull.Value)
                notification.EventId = Convert.ToInt32(reader["EventId"].ToString());
            if (reader["Status"] != DBNull.Value)
                notification.IncidentStatus = Convert.ToInt32(reader["Status"].ToString());

            if (notification.IncidentStatus == (int)IncidentActionStatus.Dispatch)
                notification.StatusName = IncidentActionStatus.Dispatch.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Pending)
                notification.StatusName = IncidentActionStatus.Pending.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Park)
                notification.StatusName = IncidentActionStatus.Park.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Engage)
                notification.StatusName = IncidentActionStatus.Engage.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Release)
                notification.StatusName = IncidentActionStatus.Release.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Complete)
                notification.StatusName = IncidentActionStatus.Complete.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Reject)
                notification.StatusName = IncidentActionStatus.Reject.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Resolve)
                notification.StatusName = IncidentActionStatus.Resolve.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Arrived)
                notification.StatusName = IncidentActionStatus.Arrived.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Leave)
                notification.StatusName = IncidentActionStatus.Leave.ToString();
            else if (notification.IncidentStatus == (int)IncidentActionStatus.Escalated)
                notification.StatusName = IncidentActionStatus.Escalated.ToString();

            if (reader["IncidentType"] != DBNull.Value)
                notification.IncidentType = Convert.ToInt32(reader["IncidentType"].ToString());

            if (notification.IncidentType == (int)IncidentTypes.MobileIncident)
                notification.IncidentTypeName = IncidentTypes.MobileIncident.ToString();
            else if (notification.IncidentType == (int)IncidentTypes.SystemCreated)
                notification.IncidentTypeName = IncidentTypes.SystemCreated.ToString();
            else if (notification.IncidentType == (int)IncidentTypes.ThirdParty)
                notification.IncidentTypeName = IncidentTypes.ThirdParty.ToString();

            if (reader["LocationId"] != DBNull.Value)
                notification.LocationId = Convert.ToInt32(reader["LocationId"].ToString());

            if (reader["TemplateTaskId"] != DBNull.Value)
                notification.TemplateTaskId = Convert.ToInt32(reader["TemplateTaskId"].ToString());

            if (reader["Instructions"] != DBNull.Value)
                notification.Instructions = reader["Instructions"].ToString();

            if (reader["Description"] != DBNull.Value)
                notification.Description = reader["Description"].ToString();
            if (reader["ReceivedBy"] != DBNull.Value)
                notification.ReceivedBy = reader["ReceivedBy"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["Identifier"] != DBNull.Value)
                notification.Identifier = Guid.Parse((reader["Identifier"].ToString()));
            if (reader["Name"] != DBNull.Value)
                notification.Name = reader["Name"].ToString();
            if (reader["RecevieTime"] != DBNull.Value)
                notification.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
            if (reader["EventType"] != DBNull.Value)
                notification.EventType = (EventTypes)reader["EventType"];

            notification.Event = notification.EventType.ToString();

            if (reader["Username"] != DBNull.Value)
                notification.UserName = reader["Username"].ToString();
            if (reader["HandledBy"] != DBNull.Value)
                notification.HandledBy = reader["HandledBy"].ToString();
            if (reader["Longtitude"] != DBNull.Value)
                notification.Longtitude = reader["Longtitude"].ToString();
            if (reader["Latitude"] != DBNull.Value)
                notification.Latitude = reader["Latitude"].ToString();

            if (reader["HandledTime"] != DBNull.Value)
                notification.HandledTime = Convert.ToDateTime(reader["HandledTime"]);

            if (reader["Handled"].ToString() == "True")
            {
                notification.Status = "Handled";
                notification.Handled = true;
            }
            else
            {
                notification.Status = "Pending";
                notification.Handled = false;
            }
            if (notification.EventType == EventTypes.DriverOffence)
            {
                notification.IsDriverOffence = true;
                notification.imageURL = @"../Images/yellow1.png";
            }
            else
            {
                notification.IsHotEvent = true;
                notification.imageURL = @"../Images/red.png";
            }
            if (columns.Contains( "AccountId"))
                notification.AccountId = Convert.ToInt32(reader["AccountId"]);

            if (columns.Contains( "AccountName"))
                notification.AccountName = reader["AccountName"].ToString();

            if (columns.Contains("CustomerIncidentId"))
                notification.CustomerIncidentId = reader["CustomerIncidentId"].ToString();

            if (columns.Contains( "CreatedDateByEventId"))
            {
                if (reader["CreatedDateByEventId"] != DBNull.Value)
                    notification.RecevieTime = Convert.ToDateTime(reader["CreatedDateByEventId"].ToString());
            }

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains( "Escalate") && reader["Escalate"] != DBNull.Value)
                notification.Escalate = Convert.ToInt32(reader["Escalate"]);

            if (columns.Contains( "isViewed") && reader["isViewed"] != DBNull.Value)
                notification.isViewed = Convert.ToBoolean(reader["isViewed"].ToString());

            if (columns.Contains( "ExtraName") && reader["ExtraName"] != DBNull.Value)
                notification.ExtraName = reader["ExtraName"].ToString();
            if (columns.Contains( "Notes") && reader["Notes"] != DBNull.Value)
                notification.Notes = reader["Notes"].ToString();

            if (columns.Contains( "Phone") && reader["Phone"] != DBNull.Value)
                notification.PhoneNumber = reader["Phone"].ToString();

            if (columns.Contains( "Email") && reader["Email"] != DBNull.Value)
                notification.EmailAddress = reader["Email"].ToString();
            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                notification.CustomerUName = reader["CustomerUName"].ToString();
            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                notification.CustomerId = Convert.ToInt32(reader["CustomerId"]);

            if (columns.Contains( "EventTypeName") && reader["EventTypeName"] != DBNull.Value)
                notification.EventTypeName = reader["EventTypeName"].ToString();
            else
            {
                notification.EventTypeName = "None";
            }
            notification.RequestName = string.Empty;
            if (columns.Contains( "RequestName") && reader["RequestName"] != DBNull.Value)
                notification.RequestName = reader["RequestName"].ToString();

            if (columns.Contains("MobDescription") && reader["MobDescription"] != DBNull.Value)
                notification.MobDescription = reader["MobDescription"].ToString();
            

            if (columns.Contains( "WebPort") && reader["WebPort"] != DBNull.Value)
                notification.WebPort = Convert.ToInt32(reader["WebPort"]);

            if (columns.Contains("CustomerFullName") && reader["CustomerFullName"] != DBNull.Value)
                notification.CustomerFullName = reader["CustomerFullName"].ToString();

            if (columns.Contains("CustomerHandleFullName") && reader["CustomerHandleFullName"] != DBNull.Value)
                notification.CustomerHandleFullName = reader["CustomerHandleFullName"].ToString();

            

            notification.View = "View";

            if (notification.Name == "Milestone Hot Event")
                notification.Name = "Hot Event";

            return notification;
        }

        public static CustomEvent GetAllCustomHotEventByIdentifier(Guid identifier, string dbConnection)
        {
            CustomEvent CusEvent = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllCustomHotEventByIdentifier", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@Identifier"].Value = identifier;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomHotEventByIdentifier";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    CusEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    //CusEvent = new CustomEvent();
                    //CusEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //CusEvent.Name = reader["Name"].ToString();
                    //CusEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //CusEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //CusEvent.EventType = (EventTypes)reader["EventType"];
                    //CusEvent.Event = CusEvent.EventType.ToString();
                    //CusEvent.UserName = reader["Username"].ToString();
                    //CusEvent.HandledBy = reader["HandledBy"].ToString();
                    //CusEvent.Longtitude = reader["Longtitude"].ToString();
                    //CusEvent.Latitude = reader["Latitude"].ToString();

                    //if (reader["HandledTime"] != DBNull.Value)
                    //    CusEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    //if (reader["Handled"].ToString() == "True")
                    //{
                    //    CusEvent.Status = "Handled";
                    //    CusEvent.Handled = true;
                    //}
                    //else
                    //{
                    //    CusEvent.Status = "Pending";
                    //    CusEvent.Handled = false;
                    //}

                    //if(columns.Contains("AccountId"))
                    //    CusEvent.AccountId = Convert.ToInt32(reader["AccountId"]);

                    //if (columns.Contains( "AccountName"))
                    //    CusEvent.AccountName = reader["AccountName"].ToString();

                    //CusEvent.View = "View";
                }
                connection.Close();
                reader.Close();
            }
            return CusEvent;
        }

        public static CustomEvent GetCustomEventById(int id, string dbConnection)
        {
            CustomEvent customEvent = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetCustomEventById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetCustomEventById";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    if (customEvent.EventType == EventTypes.DriverOffence)
                    {
                        customEvent.IsDriverOffence = true;
                        customEvent.imageURL = @"../Images/yellow1r.png";
                    }
                    else if (customEvent.EventType == EventTypes.Request)
                    {
                        customEvent.IsDriverOffence = true;
                        customEvent.imageURL = @"../Images/bluer.png";
                    }
                    else
                    {
                        if (customEvent.IncidentStatus == (int)IncidentActionStatus.Engage)
                            customEvent.imageURL = @"../Images/bluer.png";
                        else if (customEvent.IncidentStatus == (int)IncidentActionStatus.Release || customEvent.IncidentStatus == (int)IncidentActionStatus.Complete)
                            customEvent.imageURL = @"../Images/green-r.png";

                        else
                            customEvent.imageURL = @"../Images/redr.png";
                        customEvent.IsHotEvent = true;

                    }
                }
                connection.Close();
                reader.Close();
            }
            return customEvent;
        }

        public static int InsertCustomEventUNHandled(CustomEvent customEvent, string dbConnection)
        {
            int id = 0;
            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomEvent", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = customEvent.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customEvent.Name;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customEvent.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = customEvent.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = customEvent.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                    cmd.Parameters["@HandledBy"].Value = customEvent.HandledBy;

                    cmd.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                    cmd.Parameters["@EventType"].Value = (int)customEvent.EventType;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customEvent.SiteId;

                    //cmd.Parameters.Add(new SqlParameter("@HandledTime", SqlDbType.DateTime));
                    //cmd.Parameters["@HandledTime"].Value = customEvent.HandledTime;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertCustomEvent";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }

        public static int InsertCustomEventUNHandled(CustomEvent customEvent, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;
            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomEvent", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = customEvent.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customEvent.Name;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customEvent.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = customEvent.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = customEvent.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                    cmd.Parameters["@HandledBy"].Value = customEvent.HandledBy;

                    cmd.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                    cmd.Parameters["@EventType"].Value = (int)customEvent.EventType;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customEvent.SiteId;

                    //cmd.Parameters.Add(new SqlParameter("@HandledTime", SqlDbType.DateTime));
                    //cmd.Parameters["@HandledTime"].Value = customEvent.HandledTime;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertCustomEvent";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            customEvent.EventId = id;
                            var audit = new CustomEventAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customEvent.UpdatedBy);
                            Reflection.CopyProperties(customEvent, audit);
                            CustomEventAudit.InsertCustomEvent(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertCustomEventUNHandled", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static int InsertCustomEvent(CustomEvent customEvent, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = customEvent.EventId;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomEvent", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = customEvent.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customEvent.Name;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customEvent.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = customEvent.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = customEvent.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                    cmd.Parameters["@HandledBy"].Value = customEvent.HandledBy;

                    cmd.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                    cmd.Parameters["@EventType"].Value = (int)customEvent.EventType;

                    cmd.Parameters.Add(new SqlParameter("@HandledTime", SqlDbType.DateTime));
                    cmd.Parameters["@HandledTime"].Value = customEvent.HandledTime;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customEvent.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertCustomEvent";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    if ((int)returnParameter.Value > 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            customEvent.EventId = id;
                            var audit = new CustomEventAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customEvent.UpdatedBy);
                            Reflection.CopyProperties(customEvent, audit);
                            CustomEventAudit.InsertCustomEvent(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertCustomEvent", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static void CustomEventHandledById(int id, bool handled, string handledby, string dbConnection)
        {
            using (var sql = new SqlConnection(dbConnection))
            {
                sql.Open();
                var cmd = new SqlCommand("CustomEventHandled", sql);

                cmd.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                cmd.Parameters["@Handled"].Value = handled;
                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;
                cmd.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                cmd.Parameters["@HandledBy"].Value = handledby;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "CustomEventHandled";

                cmd.ExecuteNonQuery();
                sql.Close();
            }
        }

        public static void UpdateCustomEventAudit(Guid Identifier, bool Handled, DateTime HandledTime, string HandledBy, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateCustomEventAudit", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@Identifier"].Value = Identifier;

                sqlCommand.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                sqlCommand.Parameters["@Handled"].Value = Handled;

                sqlCommand.Parameters.Add(new SqlParameter("@HandledTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@HandledTime"].Value = HandledTime;

                sqlCommand.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                sqlCommand.Parameters["@HandledBy"].Value = HandledBy;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateCustomEventAudit";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
        }

        public static List<CustomEvent> GetAllCustomEventsBySiteId(int siteId,string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEventsBySiteId";
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllCustomEventsByLevel5(int siteId, string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEventsByLevel5";
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllCustomEventByCId(int siteId, string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEventByCId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }
        

        public static List<CustomEvent> GetAllCustomEvents(string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEvents";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllCustomEventsbyRectangle(string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEvents";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    //var customEvent = new CustomEvent();

                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();

                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.UserName = reader["Username"].ToString();
                    //customEvent.View = "View";
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //customEvent.Latitude = reader["Latitude"].ToString();
                    //if (reader["HandledTime"] != DBNull.Value)
                    //    customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    if (customEvent.EventType == EventTypes.DriverOffence)
                    {
                        customEvent.IsDriverOffence = true;
                        customEvent.imageURL = @"../Images/yellow1r.png";
                    }
                    else if (customEvent.EventType == EventTypes.Request)
                    {
                        customEvent.IsDriverOffence = true;
                        customEvent.imageURL = @"../Images/bluer.png";
                    }
                    else
                    {
                        customEvent.IsHotEvent = true;
                        if (customEvent.IncidentStatus == (int)IncidentActionStatus.Engage)
                            customEvent.imageURL = @"../Images/bluer.png";
                        else if (customEvent.IncidentStatus == (int)IncidentActionStatus.Release || customEvent.IncidentStatus == (int)IncidentActionStatus.Complete)
                            customEvent.imageURL = @"../Images/green-r.png";
                        else
                            customEvent.imageURL = @"../Images/redr.png";
                    }

                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllCustomEventsByManagerId(int userId, string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEventsByManagerId";
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //if (customEvent.Name == "Milestone Hot Event")
                    //    customEvent.Name = "Hot Event";
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.UserName = reader["Username"].ToString();
                    //customEvent.View = "View";
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //customEvent.Latitude = reader["Latitude"].ToString();
                    //if (reader["HandledTime"] != DBNull.Value)
                    //    customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    //if (customEvent.EventType == EventTypes.DriverOffence)
                    //{
                    //    customEvent.IsDriverOffence = true;
                    //    customEvent.imageURL = @"../Images/yellow1.png";
                    //}
                    //else
                    //{
                    //    customEvent.IsHotEvent = true;
                    //    customEvent.imageURL = @"../Images/red.png";
                    //}
                    //if (columns.Contains( "AccountId"))
                    //    customEvent.AccountId = Convert.ToInt32(reader["AccountId"]);

                    //if (columns.Contains( "AccountName"))
                    //    customEvent.AccountName = reader["AccountName"].ToString();
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }
        public static List<CustomEvent> GetAllCustomEventByCreator(string name, string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEventByCreator";
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllCustomEventByCreatorAndEventType(string name, int evtype, string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEventByCreatorAndEventType";
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = evtype;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllCustomEventByIncidentType(string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEventByIncidentType";

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllCustomEventsByManagerIdRectangleImage(int userId, string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEventsByManagerId";
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    if (customEvent.EventType == EventTypes.DriverOffence)
                    {
                        customEvent.IsDriverOffence = true;
                        customEvent.imageURL = @"../Images/yellow1r.png";
                    }
                    else
                    {
                        customEvent.IsHotEvent = true;

                        if (customEvent.IncidentStatus == (int)IncidentActionStatus.Engage)
                            customEvent.imageURL = @"../Images/bluer.png";
                        else if (customEvent.IncidentStatus == (int)IncidentActionStatus.Release || customEvent.IncidentStatus == (int)IncidentActionStatus.Complete)
                            customEvent.imageURL = @"../Images/green-r.png";
                        else
                            customEvent.imageURL = @"../Images/redr.png";
                    }
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllCustomEventByTypeAndManagerId(int Type, int userId, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByTypeAndManagerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByTypeAndManagerId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.UserName = reader["Username"].ToString();
                    //customEvent.View = "View";
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //customEvent.Latitude = reader["Latitude"].ToString();
                    //if (reader["HandledTime"] != DBNull.Value)
                    //    customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    //if (customEvent.EventType == EventTypes.DriverOffence)
                    //{
                    //    customEvent.IsDriverOffence = true;
                    //    customEvent.imageURL = @"../Images/yellow1.png";
                    //}
                    //else
                    //{
                    //    customEvent.IsHotEvent = true;
                    //    customEvent.imageURL = @"../Images/red.png";
                    //}
                    //if (columns.Contains( "AccountId"))
                    //    customEvent.AccountId = Convert.ToInt32(reader["AccountId"]);

                    //if (columns.Contains( "AccountName"))
                    //    customEvent.AccountName = reader["AccountName"].ToString();
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }
        public static List<CustomEvent> GetAllCustomEventByTypeAndCId(int Type, int userId, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByTypeAndCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByTypeAndCId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
 
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByTypeByLevel5(int Type, int userId, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByTypeByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByTypeByLevel5";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllFullCustomEvents(string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllFullCustomEvents";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.Event = customEvent.EventType.ToString();
                    //customEvent.UserName = reader["Username"].ToString();
                    //if (reader["HandledBy"] != DBNull.Value)
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //if (reader["Longtitude"] != DBNull.Value)
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //if (reader["Latitude"] != DBNull.Value)
                    //customEvent.Latitude = reader["Latitude"].ToString();

                    //if (reader["Handled"].ToString() == "True")
                    //{
                    //    customEvent.Status = "Handled";
                    //    customEvent.Handled = true;
                    //}
                    //else
                    //{
                    //    customEvent.Status = "Pending";
                    //    customEvent.Handled = false;
                    //}
                    //customEvent.View = "View";
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllFullCustomEventsByManagerId(int userId, string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllFullCustomEventsByManagerId";
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.Event = customEvent.EventType.ToString();
                    //customEvent.UserName = reader["Username"].ToString();
                    //if (reader["HandledBy"] != DBNull.Value)
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //if (reader["Longtitude"] != DBNull.Value)
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //if (reader["Latitude"] != DBNull.Value)
                    //customEvent.Latitude = reader["Latitude"].ToString();

                    //if (reader["Handled"].ToString() == "True")
                    //{
                    //    customEvent.Status = "Handled";
                    //    customEvent.Handled = true;
                    //}
                    //else
                    //{
                    //    customEvent.Status = "Pending";
                    //    customEvent.Handled = false;
                    //}
                    //customEvent.View = "View";
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }


        public static List<CustomEvent> GetTestEvents(string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllReportFullDriverOffence", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllReportFullDriverOffence";
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = new CustomEvent();


                    customEvent.UserName = reader["Username"].ToString();


                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static void InsertCustomEventAuditUnhandled(CustomEvent customEvent, string dbConnection)
        {
            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomEventAudit", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = customEvent.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customEvent.Name;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customEvent.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = customEvent.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = customEvent.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                    cmd.Parameters["@HandledBy"].Value = customEvent.HandledBy;

                    cmd.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                    cmd.Parameters["@EventType"].Value = (int)customEvent.EventType;

                    cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                    cmd.Parameters["@EventId"].Value = customEvent.EventId;

                    cmd.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                    cmd.Parameters["@Handled"].Value = customEvent.Handled;

                    //cmd.Parameters.Add(new SqlParameter("@HandledTime", SqlDbType.DateTime));
                    //cmd.Parameters["@HandledTime"].Value = customEvent.HandledTime;

                    cmd.Parameters.Add(new SqlParameter("@RecevieTime", SqlDbType.DateTime));
                    cmd.Parameters["@RecevieTime"].Value = customEvent.RecevieTime;

                    cmd.CommandText = "InsertCustomEventAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void InsertCustomEventAudit(CustomEvent customEvent, string dbConnection)
        {
            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomEventAudit", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = customEvent.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customEvent.Name;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customEvent.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = customEvent.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = customEvent.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                    cmd.Parameters["@HandledBy"].Value = customEvent.HandledBy;

                    cmd.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                    cmd.Parameters["@EventType"].Value = (int)customEvent.EventType;

                    cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                    cmd.Parameters["@EventId"].Value = customEvent.EventId;

                    cmd.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                    cmd.Parameters["@Handled"].Value = customEvent.Handled;

                    cmd.Parameters.Add(new SqlParameter("@HandledTime", SqlDbType.DateTime));
                    cmd.Parameters["@HandledTime"].Value = customEvent.HandledTime;

                    cmd.Parameters.Add(new SqlParameter("@RecevieTime", SqlDbType.DateTime));
                    cmd.Parameters["@RecevieTime"].Value = customEvent.RecevieTime;

                    cmd.CommandText = "InsertCustomEventAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void InsertCustomEventAuditToBeHandled(CustomEvent customEvent, string dbConnection)
        {
            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomEventAudit", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = customEvent.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customEvent.Name;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customEvent.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = customEvent.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = customEvent.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                    cmd.Parameters["@HandledBy"].Value = customEvent.HandledBy;

                    cmd.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                    cmd.Parameters["@EventType"].Value = (int)customEvent.EventType;

                    cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                    cmd.Parameters["@EventId"].Value = customEvent.EventId;

                    cmd.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                    cmd.Parameters["@Handled"].Value = customEvent.Handled;

                    cmd.Parameters.Add(new SqlParameter("@HandledTime", SqlDbType.DateTime));
                    cmd.Parameters["@HandledTime"].Value = customEvent.HandledTime;

                    cmd.CommandText = "InsertCustomEventAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<CustomEvent> GetAllFullCustomEventAudit(string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllFullCustomEventAudit";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = new CustomEvent();
                    customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    customEvent.Name = reader["Name"].ToString();
                    customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    customEvent.EventType = (EventTypes)reader["EventType"];
                    customEvent.Event = customEvent.EventType.ToString();
                    customEvent.UserName = reader["Username"].ToString();
                    customEvent.HandledBy = reader["HandledBy"].ToString();
                    customEvent.Longtitude = reader["Longtitude"].ToString();
                    customEvent.Latitude = reader["Latitude"].ToString();
                    if (reader["HandledTime"] != DBNull.Value)
                        customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    if (reader["Handled"].ToString() == "True")
                    {
                        customEvent.Status = "Handled";
                        customEvent.Handled = true;
                    }
                    else
                    {
                        customEvent.Status = "Pending";
                        customEvent.Handled = false;
                    }
                    customEvent.View = "View";
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<CustomEvent> GetAllFullCustomEventAuditReport(string dbConnection)
        {
            var customEvents = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllFullCustomEventAudit";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = new CustomEvent();
                    customEvent.Name = reader["Name"].ToString();
                    customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    customEvent.UserName = reader["Username"].ToString();
                    customEvent.HandledBy = reader["HandledBy"].ToString();
                    customEvent.Longtitude = reader["Longtitude"].ToString();
                    customEvent.Latitude = reader["Latitude"].ToString();
                    if (reader["HandledTime"] != DBNull.Value)
                        customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    if (reader["Handled"].ToString() == "True")
                    {
                        customEvent.Status = "Handled";
                        customEvent.Handled = true;
                    }
                    else
                    {
                        customEvent.Status = "Pending";
                        customEvent.Handled = false;
                    }
                    customEvent.View = "View";
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static bool DeleteCustomEventByDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteCustomEventByDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteCustomEventByDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteCustomEventById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteCustomEventById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteCustomEventById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static List<CustomEvent> GetAllCustomEventByDateHandled(DateTime startDate, DateTime fromDate, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.UserName = reader["Username"].ToString();
                    //customEvent.View = "View";
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //customEvent.Latitude = reader["Latitude"].ToString();
                    //if (reader["HandledTime"] != DBNull.Value)
                    //    customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    //if (customEvent.EventType == EventTypes.DriverOffence)
                    //    customEvent.IsDriverOffence = true;
                    //else
                    //    customEvent.IsHotEvent = true;

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }


        public static List<CustomEvent> GetAllCustomEventByDateHandledAndLevel5(DateTime startDate, DateTime fromDate, int siteid, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateHandledAndLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateHandledAndLevel5";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }


        public static List<CustomEvent> GetAllCustomEventByDateHandledAndSiteId(DateTime startDate, DateTime fromDate,int siteid ,string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateHandledAndSiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateHandledAndSiteId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByDateHandledAndCId(DateTime startDate, DateTime fromDate, int siteid, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateHandledAndCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateHandledAndCId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }


        public static List<CustomEvent> GetAllCustomEventByDateUNHandledAndLevel5(DateTime startDate, DateTime fromDate, int siteid, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateUNHandledAndLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateUNHandledAndLevel5";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }
        public static List<CustomEvent> GetAllCustomEventByDateUNHandledAndCId(DateTime startDate, DateTime fromDate, int siteid, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateUNHandledAndCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@CId", SqlDbType.Int));
                sqlCommand.Parameters["@CId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateUNHandledAndCId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }
       
        
        public static List<CustomEvent> GetAllCustomEventByDateUNHandledAndSite(DateTime startDate, DateTime fromDate,int siteid ,string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateUNHandledAndSite", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateUNHandledAndSite";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }
        public static List<CustomEvent> GetAllCustomEventByDateUNHandled(DateTime startDate, DateTime fromDate, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateUNHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateUNHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.UserName = reader["Username"].ToString();
                    //customEvent.View = "View";
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //customEvent.Latitude = reader["Latitude"].ToString();
                    //if (reader["HandledTime"] != DBNull.Value)
                    //    customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    //if (customEvent.EventType == EventTypes.DriverOffence)
                    //    customEvent.IsDriverOffence = true;
                    //else
                    //    customEvent.IsHotEvent = true;

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }
        public static List<CustomEvent> GetAllCustomEventByDateAndManagerIdHandledBySiteId(DateTime startDate, DateTime fromDate, int userId,int siteId ,string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateAndManagerIdHandledBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateAndManagerIdHandledBySiteId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }
        public static List<CustomEvent> GetAllCustomEventByDateAndDirectorIdHandledBySiteId(DateTime startDate, DateTime fromDate, int userId, int siteId, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateAndDirectorIdHandledBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateAndDirectorIdHandledBySiteId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByDateAndManagerIdHandled(DateTime startDate, DateTime fromDate, int userId, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateAndManagerIdHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateAndManagerIdHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByDateAndDirectorIdHandled(DateTime startDate, DateTime fromDate, int userId, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateAndDirectorIdHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateAndDirectorIdHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        

        public static List<CustomEvent> GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(DateTime startDate, DateTime fromDate, int userId,int siteId ,string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateAndManagerIdUNHandledBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateAndManagerIdUNHandledBySiteId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByDateAndManagerIdUNHandled(DateTime startDate, DateTime fromDate, int userId, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByDateAndManagerIdUNHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByDateAndManagerIdUNHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.UserName = reader["Username"].ToString();
                    //customEvent.View = "View";
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //customEvent.Latitude = reader["Latitude"].ToString();
                    //if (reader["HandledTime"] != DBNull.Value)
                    //    customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    //if (customEvent.EventType == EventTypes.DriverOffence)
                    //    customEvent.IsDriverOffence = true;
                    //else
                    //    customEvent.IsHotEvent = true;

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByStatusCIdAndHandled(int status, int siteId, bool handle, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByStatusCIdAndHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = status;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                sqlCommand.Parameters["@Handled"].Value = handle;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByStatusCIdAndHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }


        public static List<CustomEvent> GetAllCustomEventByStatusLevel5AndHandled(int status, int siteId, bool handle, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByStatusLevel5AndHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = status;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteId;
                sqlCommand.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                sqlCommand.Parameters["@Handled"].Value = handle;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByStatusLevel5AndHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByStatusSiteIdAndHandled(int status, int siteId,bool handle, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByStatusSiteIdAndHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = status;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;
                sqlCommand.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                sqlCommand.Parameters["@Handled"].Value = handle;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByStatusSiteIdAndHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByTypeBySiteId(int Type,int siteId ,string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByTypeBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByTypeBySiteId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByType(int Type, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByType", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByType";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);
                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventsByIntervalUnHandled(string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventsByIntervalUnHandled", connection);

                sqlCommand.CommandText = "GetAllCustomEventsByIntervalUnHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.UserName = reader["Username"].ToString();
                    //customEvent.View = "View";
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //customEvent.Latitude = reader["Latitude"].ToString();
                    //if (reader["HandledTime"] != DBNull.Value)
                    //    customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    //if (customEvent.EventType == EventTypes.DriverOffence)
                    //    customEvent.IsDriverOffence = true;
                    //else
                    //    customEvent.IsHotEvent = true;

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventsByIntervalHandled(string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventsByIntervalHandled", connection);

                sqlCommand.CommandText = "GetAllCustomEventsByIntervalHandled";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    //customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    //customEvent.Name = reader["Name"].ToString();
                    //customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    //customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    //customEvent.EventType = (EventTypes)reader["EventType"];
                    //customEvent.UserName = reader["Username"].ToString();
                    //customEvent.View = "View";
                    //customEvent.HandledBy = reader["HandledBy"].ToString();
                    //customEvent.Longtitude = reader["Longtitude"].ToString();
                    //customEvent.Latitude = reader["Latitude"].ToString();
                    //if (reader["HandledTime"] != DBNull.Value)
                    //    customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    //if (customEvent.EventType == EventTypes.DriverOffence)
                    //    customEvent.IsDriverOffence = true;
                    //else
                    //    customEvent.IsHotEvent = true;

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static string DateFormatter(DateTime date)
        {
            string formattedDate = date.ToString("MM/dd/yyyy hh:mm:sstt");
            return formattedDate;
        }

        public static List<CustomEvent> GetAllEventsByEventId(Guid identifier, string dbConnection)
        {
            var customEv = new List<CustomEvent>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllEventsByEventId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@Identifier"].Value = identifier;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomHotEventByIdentifier";
                sqlCommand.CommandText = "GetAllEventsByEventId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = new CustomEvent();

                    customEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    customEvent.Name = reader["Name"].ToString();
                    customEvent.EventId = Convert.ToInt32(reader["EventId"]);
                    customEvent.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);
                    customEvent.EventType = (EventTypes)reader["EventType"];
                    customEvent.UserName = reader["Username"].ToString();
                    customEvent.View = "View";
                    customEvent.HandledBy = reader["HandledBy"].ToString();
                    customEvent.Longtitude = reader["Longtitude"].ToString();
                    customEvent.Latitude = reader["Latitude"].ToString();
                    if (reader["HandledTime"] != DBNull.Value)
                        customEvent.HandledTime = Convert.ToDateTime(reader["HandledTime"]);
                    if (customEvent.EventType == EventTypes.DriverOffence)
                        customEvent.IsDriverOffence = true;
                    else
                        customEvent.IsHotEvent = true;

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }




        public static int InsertorUpdateIncident(CustomEvent customEvent, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateIncident", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = customEvent.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customEvent.Name;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customEvent.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = customEvent.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = customEvent.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@HandledBy", SqlDbType.NVarChar));
                    cmd.Parameters["@HandledBy"].Value = customEvent.HandledBy;

                    cmd.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                    cmd.Parameters["@EventType"].Value = (int)customEvent.EventType;

                    cmd.Parameters.Add(new SqlParameter("@HandledTime", SqlDbType.DateTime));
                    cmd.Parameters["@HandledTime"].Value = customEvent.HandledTime;

                    cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                    cmd.Parameters["@EventId"].Value = customEvent.EventId;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = customEvent.Description;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = customEvent.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@IncidentType", SqlDbType.Int));
                    cmd.Parameters["@IncidentType"].Value = customEvent.IncidentType;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = customEvent.IncidentStatus;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customEvent.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customEvent.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ReceivedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@ReceivedBy"].Value = customEvent.ReceivedBy;

                    cmd.Parameters.Add(new SqlParameter("@Instructions", SqlDbType.NVarChar));
                    cmd.Parameters["@Instructions"].Value = customEvent.Instructions;

                    cmd.Parameters.Add(new SqlParameter("@TemplateTaskId", SqlDbType.Int));
                    cmd.Parameters["@TemplateTaskId"].Value = customEvent.TemplateTaskId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customEvent.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateIncident";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            customEvent.EventId = id;
                            var audit = new CustomEventAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customEvent.UpdatedBy);
                            Reflection.CopyProperties(customEvent, audit);
                            CustomEventAudit.InsertCustomEvent(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertCustomEvent", ex, dbConnection);
                    }
                }
            }
            return id;
        }
        public static bool UpdateCustomEventbyStatus(int eventId, int status, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateCustomEventbyStatus", connection);

                cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                cmd.Parameters["@EventId"].Value = eventId;

                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                cmd.Parameters["@Status"].Value = status;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool UpdateCustomEventbyEscalate(int eventId, int status, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateCustomEventbyEscalated", connection);

                cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                cmd.Parameters["@EventId"].Value = eventId;

                cmd.Parameters.Add(new SqlParameter("@Escalate", SqlDbType.Int));
                cmd.Parameters["@Escalate"].Value = status;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool UpdateCustomEventView(int eventId, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateCustomEventView", connection);

                cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                cmd.Parameters["@EventId"].Value = eventId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool UpdateCustomEventNotesById(int eventId, string notes, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateCustomEventNotesById", connection);

                cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                cmd.Parameters["@EventId"].Value = eventId;

                cmd.Parameters.Add(new SqlParameter("@Notes", SqlDbType.NVarChar));
                cmd.Parameters["@Notes"].Value = notes;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool UpdateCustomEventDetailsById(int eventId, string phone, string email,string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateCustomEventDetailsById", connection);

                cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                cmd.Parameters["@EventId"].Value = eventId;

                cmd.Parameters.Add(new SqlParameter("@Phone", SqlDbType.NVarChar));
                cmd.Parameters["@Phone"].Value = phone;

                cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                cmd.Parameters["@Email"].Value = email;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static List<CustomEvent> GetAllCustomEventByUnViewedDateRange(DateTime toDateTime, DateTime frmDateTime, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByUnViewedDateRange", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = toDateTime;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = frmDateTime;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByUnViewedDateRange";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByUnViewedDateRangeAndLevel5(DateTime toDateTime, DateTime frmDateTime, int siteid, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByUnViewedDateRangeAndLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = toDateTime;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = frmDateTime;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByUnViewedDateRangeAndLevel5";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByUnViewedDateRangeAndSiteId(DateTime toDateTime, DateTime frmDateTime, int siteid, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByUnViewedDateRangeAndSiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = toDateTime;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = frmDateTime;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByUnViewedDateRangeAndSiteId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

        public static List<CustomEvent> GetAllCustomEventByUnViewedDateRangeAndCId(DateTime toDateTime, DateTime frmDateTime, int siteid, string dbConnection)
        {
            var customEv = new List<CustomEvent>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllCustomEventByUnViewedDateRangeAndCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = toDateTime;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = frmDateTime;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllCustomEventByUnViewedDateRangeAndCId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventFromSqlReader(reader, dbConnection);

                    customEv.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEv;
        }

    }
}
