﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Arrowlabs.Mims.Audit.Models
{
    public class MessageBoardAudit
    {
        public BaseAudit BaseAudit { get; set; }

        public int Id { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public string HeaderTitle { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DisplayDate { get; set; }
        public bool isFlashAlert { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public string UrlLink { get; set; }
        

        public static int InsertorUpdateMessageBoardAudit(MessageBoardAudit notification, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(notification.BaseAudit, dbConnection);

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMessageBoardAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@HeaderTitle", SqlDbType.NVarChar));
                    cmd.Parameters["@HeaderTitle"].Value = notification.HeaderTitle;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = notification.Description;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@DisplayDate", SqlDbType.DateTime));
                    cmd.Parameters["@DisplayDate"].Value = notification.DisplayDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = notification.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = notification.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = notification.ImagePath;

                    cmd.Parameters.Add(new SqlParameter("@URLLink", SqlDbType.NVarChar));
                    cmd.Parameters["@URLLink"].Value = notification.UrlLink;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@isFlashAlert", SqlDbType.Bit));
                    cmd.Parameters["@isFlashAlert"].Value = notification.isFlashAlert;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertMessageBoardAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return 0;
        }
    }
}
