﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public enum PushNotificationDeviceType
    {
        None = 0,
        Apple = 1,
        Android = 2
    }
    public class PushNotificationDeviceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string Username { get; set; }
        public string DeviceToken { get; set; }
        public int DeviceType { get; set; }
        public DateTime? CreatedDate { get; set; }

        public int SiteId { get; set; }

        private static PushNotificationDeviceAudit GetPushNotificationDeviceAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var pushNotificationDevice = new PushNotificationDeviceAudit();

            pushNotificationDevice.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Username") && reader["Username"] != DBNull.Value)
                pushNotificationDevice.Username = reader["Username"].ToString();

            if (columns.Contains("DeviceToken") && reader["DeviceToken"] != DBNull.Value)
                pushNotificationDevice.DeviceToken = reader["DeviceToken"].ToString();

            if (columns.Contains("DeviceType") && reader["DeviceType"] != DBNull.Value)
                pushNotificationDevice.DeviceType = Convert.ToInt16(reader["DeviceType"].ToString());

            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                pushNotificationDevice.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                pushNotificationDevice.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            return pushNotificationDevice;
        }
        public static bool InsertPushNotificationDeviceAudit(PushNotificationDeviceAudit pushNotificationDevice, string dbConnection)
        {
            if (pushNotificationDevice != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertPushNotificationDeviceAudit", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    var baseAuditId = BaseAudit.InsertBaseAudit(pushNotificationDevice.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = pushNotificationDevice.Username;

                    cmd.Parameters.Add("@DeviceToken", SqlDbType.NVarChar).Value = pushNotificationDevice.DeviceToken;

                    cmd.Parameters.Add("@DeviceType", SqlDbType.Int).Value = pushNotificationDevice.DeviceType;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = pushNotificationDevice.CreatedDate;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = pushNotificationDevice.SiteId;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static List<PushNotificationDeviceAudit> GetAllPushNotificationDeviceAudit(string dbConnection)
        {
            var pushNotificationDevices = new List<PushNotificationDeviceAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPushNotificationDeviceAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    pushNotificationDevices.Add(GetPushNotificationDeviceAuditFromSqlReader(reader));
                }
                connection.Close();
                reader.Close();
            }
            return pushNotificationDevices;
        }

    }
}
