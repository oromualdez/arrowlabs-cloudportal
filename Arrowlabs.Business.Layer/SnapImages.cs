﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{
    public class SnapImages
    {
        public string ImagePath { get; set; }
        public int Status { get; set; }
        public int Id { get; set; }

        public static List<SnapImages> GetAllSnapImages(string dbConnection)
        {
            var SnapImagesList = new List<SnapImages>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSnapImages", connection);


                sqlCommand.CommandText = "GetAllSnapImages";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var snapImages = new SnapImages();

                    snapImages.ImagePath = reader["ImagePath"].ToString();
                    snapImages.Status = Convert.ToInt32(reader["Status"].ToString());
                    snapImages.Id = Convert.ToInt32(reader["Id"].ToString());

                    SnapImagesList.Add(snapImages);
                }
                connection.Close();
                reader.Close();
            }
            return SnapImagesList;
        }

        public static bool InsertOrUpdateSnapImages(SnapImages snapImagesClass, string dbConnection)
        {
            if (snapImagesClass != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateSnapImages", connection);

                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = snapImagesClass.ImagePath;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = snapImagesClass.Status;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateSnapImages";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static bool DeleteSnapImageById(int Id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteSnapImage", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = Id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteSnapImage";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }

}
