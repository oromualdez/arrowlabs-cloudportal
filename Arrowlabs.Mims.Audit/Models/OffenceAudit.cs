﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class OffenceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int ID { get; set; }
        public string OffenceDesc { get; set; }
        public string OffenceDesc_AR { get; set; }
        public int OffenceTypeID { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string OffenceName { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        private static OffenceAudit GetOffenceAuditFromSqlReader(SqlDataReader reader)
        {
            var offenceAudit = new OffenceAudit();
            offenceAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                offenceAudit.ID = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("OFFENCEDESC") && reader["OFFENCEDESC"] != DBNull.Value)
               offenceAudit.OffenceDesc = reader["OFFENCEDESC"].ToString();
            if (columns.Contains("OFFENCEDESC_AR") && reader["OFFENCEDESC_AR"] != DBNull.Value)
                offenceAudit.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
            if (columns.Contains("OFFENCETYPEID") && reader["OFFENCETYPEID"] != DBNull.Value)
                offenceAudit.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
            if (columns.Contains("CreateDate") && reader["CreateDate"] != DBNull.Value)
                offenceAudit.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                offenceAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                offenceAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            

            return offenceAudit;
        }
        
        public static List<OffenceAudit> GetAllOffenceAudit(string dbConnection)
        {
            var offenceAudits = new List<OffenceAudit>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceAudit", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var offence =  GetOffenceAuditFromSqlReader(reader);
                    offenceAudits.Add(offence);
                }
                connection.Close();
                reader.Close();
            }
            return offenceAudits;
        }

        public static bool InsertOffenceAudit(OffenceAudit offenceAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(offenceAudit.BaseAudit, dbConnection);

            if (offenceAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOffenceAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@ID", SqlDbType.Int).Value = offenceAudit.ID;
                    cmd.Parameters.Add("@OffenceDesc", SqlDbType.NVarChar).Value = offenceAudit.OffenceDesc;
                    cmd.Parameters.Add("@OffenceDesc_AR", SqlDbType.NVarChar).Value = offenceAudit.OffenceDesc_AR;
                    cmd.Parameters.Add("@OffenceTypeID", SqlDbType.Int).Value = offenceAudit.OffenceTypeID;
                    cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = offenceAudit.CreateDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = offenceAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = offenceAudit.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = offenceAudit.CustomerId; 
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
