﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class DatabaseParam
    {
        public string ServerName;
        public string DatabaseName;
        public string UserName;
        public string Password;
        //Data file parameters
        //public string DataFileName;
        //public string DataPathName;
     //   public string DataFileSize;
     //   public string DataFileGrowth;
        //Log file parameters
     //   public string LogFileName;
        //public string LogPathName;
     //   public string LogFileSize;
      //  public string LogFileGrowth;
    }
    public class DatabaseCreation
    {
        public static void CreateDatabase(DatabaseParam DBParam, string con, string tablescript, string storedscript)
        {
            System.Data.SqlClient.SqlConnection tmpConn;
            string sqlCreateDBQuery;
            tmpConn = new SqlConnection();
            tmpConn.ConnectionString = "SERVER = " + DBParam.ServerName + "; DATABASE = master; User ID = " + DBParam.UserName + "; Pwd = " + DBParam.Password;

            sqlCreateDBQuery = " CREATE DATABASE " + DBParam.DatabaseName + " ON PRIMARY "
                                + " (NAME = " + DBParam.DatabaseName + ", "
                                + " FILENAME = 'C:\\" + DBParam.DatabaseName + ".mdf', "
                                + " SIZE = 2304KB,"
                                + "MAXSIZE = UNLIMITED, "
                                + "	FILEGROWTH =1024KB) "
                                + " LOG ON (NAME =" + DBParam.DatabaseName + "_Log, "
                                + " FILENAME = 'C:\\" + DBParam.DatabaseName + "Log.ldf', "
                                + " SIZE = 1MB, "
                                + "MAXSIZE = 5MB, "
                                + "	FILEGROWTH =10%) ";

            SqlCommand myCommand = new SqlCommand(sqlCreateDBQuery, tmpConn);
            try
            {
                tmpConn.Open();
                //  MessageBox.Show(sqlCreateDBQuery);
                myCommand.ExecuteNonQuery();

                //  MessageBox.Show("Database has been created successfully!", "Create Database", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception ex)
            {
                //  MessageBox.Show(ex.ToString(), "Create Database", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                tmpConn.Close();
                ExecuteSql(DBParam.DatabaseName, GetScript(tablescript), con);
                ExecuteStoredProcedure(DBParam.DatabaseName, GetScript(storedscript), con);
            }
            return;
        }
        private static void ExecuteStoredProcedure(string dbname, string Sql, string con)
        {
            string[] SqlLine;
            Regex regex = new Regex("^GO", RegexOptions.IgnoreCase | RegexOptions.Multiline);

            SqlLine = regex.Split(Sql);

            SqlConnection masterConnection = new SqlConnection();

            foreach (var comand in SqlLine)
            {
                try
                {
                    if (!string.IsNullOrEmpty(comand))
                    {
                        System.Data.SqlClient.SqlCommand Command = new System.Data.SqlClient.SqlCommand(comand, masterConnection);

                        // Initialize the connection, open it, and set it to the "master" database
                        masterConnection.ConnectionString = con;
                        Command.Connection.Open();
                        Command.Connection.ChangeDatabase(dbname);
                        try
                        {
                            Command.ExecuteNonQuery();
                        }

                        catch (Exception ex)
                        {
                            // MessageBox.Show("In exception handler: " + ex.Message); 
                        }
                        finally
                        {
                            // Closing the connection should be done in a Finally block
                            Command.Connection.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    // MessageBox.Show("In exception handler: " + ex.Message); 
                }
            }
        }
        public static void ExecuteSql(string dbname, string Sql, string connection)
        {
            SqlConnection masterConnection = new SqlConnection();
            System.Data.SqlClient.SqlCommand Command = new System.Data.SqlClient.SqlCommand(Sql, masterConnection);

            // Initialize the connection, open it, and set it to the "master" database
            masterConnection.ConnectionString = connection;
            Command.Connection.Open();
            Command.Connection.ChangeDatabase(dbname);
            try
            {
                Command.ExecuteNonQuery();
            }

            catch (Exception ex)
            { 
            //    MessageBox.Show("In exception handler: " + ex.Message); 
            }
            finally
            {
                // Closing the connection should be done in a Finally block
                Command.Connection.Close();
            }
        }

        public static void DeleteDatabase(string dbname, string constring)
        {
            var sqlCon = new SqlConnection();
            try
            {
                sqlCon.ConnectionString = constring;
                if (sqlCon.State != ConnectionState.Closed) sqlCon.Close();
                sqlCon.Open();
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.Connection = sqlCon;
                cmd.CommandText = "IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'" + dbname + "')ALTER database ["+dbname+"] set offline with ROLLBACK IMMEDIATE; DROP DATABASE [" + dbname + "]";
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                
                //        IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'MIMSClient')
                //DROP DATABASE [MIMSClient]
            }
            catch (System.Exception ex)
            {
                //  MessageBox.Show(ex.ToString(), "Create Database", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public static string GetScript(string name)
        {
            //Assembly asm = Assembly.GetExecutingAssembly();
            
            //Stream str = asm.GetManifestResourceStream(asm.GetName().Name + "." + name);
            //StreamReader reader = new StreamReader(str);
          //  System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return File.ReadAllText(name);
        }
    }
}
