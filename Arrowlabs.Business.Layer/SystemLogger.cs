﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class SystemLogger
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string Logs { get; set; }
        [DataMember]
        public string PreviousValue { get; set; }
        [DataMember]
        public string NewValue { get; set; }
        [DataMember]
        public string Module { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }

        public static bool InsertSystemLog(SystemLogger eventHistory, string dbConnection)
        {
            int id = 0;

            try
            {
                if (eventHistory != null)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertSystemLogs", connection);

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@Logs", SqlDbType.NVarChar));
                        cmd.Parameters["@Logs"].Value = eventHistory.Logs;

                        cmd.Parameters.Add(new SqlParameter("@PreviousValue", SqlDbType.NVarChar));
                        cmd.Parameters["@PreviousValue"].Value = eventHistory.PreviousValue;

                        cmd.Parameters.Add(new SqlParameter("@NewValue", SqlDbType.NVarChar));
                        cmd.Parameters["@NewValue"].Value = eventHistory.NewValue;

                        cmd.Parameters.Add(new SqlParameter("@Module", SqlDbType.NVarChar));
                        cmd.Parameters["@Module"].Value = eventHistory.Module;

                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreatedDate"].Value = eventHistory.CreatedDate;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;

                        cmd.CommandText = "InsertSystemLogs";
                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();

                        id = (int)returnParameter.Value;

                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("SystemLogger", "InsertSystemLogs", ex, dbConnection);
                return false;
            }
            return true;
        }

        public static bool SaveSystemLog(string dbcon, string module, string newvalue, string oldvalue, Users userinfo,string log)
        {
            var newLog = new SystemLogger();
            newLog.CreatedBy = userinfo.Username;
            newLog.CreatedDate = DateTime.Now;
            newLog.Logs = log;
            newLog.Module = module;
            newLog.NewValue = newvalue;
            newLog.PreviousValue = oldvalue;
            newLog.SiteId = userinfo.SiteId;
            return InsertSystemLog(newLog, dbcon);
        }
    }
}
