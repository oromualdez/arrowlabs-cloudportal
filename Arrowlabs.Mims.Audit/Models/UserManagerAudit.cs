﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class UserManagerAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ManagerId { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        

        public static UserManagerAudit GetUserManagerAuditFromSqlReader(SqlDataReader reader)
        {
            var userManagerAudit = new UserManagerAudit();
            userManagerAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                userManagerAudit.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("UserId") && reader["UserId"] != DBNull.Value)
                userManagerAudit.ManagerId = Convert.ToInt32(reader["UserId"].ToString());
            if (columns.Contains("ManagerId") && reader["ManagerId"] != DBNull.Value)
                userManagerAudit.UserId = Convert.ToInt32(reader["ManagerId"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                userManagerAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return userManagerAudit;           
        }
        public static List<UserManagerAudit> GetAllUserManagerAudit(string dbConnection)
        {
            var userManagerAuditList = new List<UserManagerAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserManagerAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userManagerAudit = GetUserManagerAuditFromSqlReader(reader);
                    userManagerAuditList.Add(userManagerAudit);
                }
                connection.Close();
                reader.Close();
            }
            return userManagerAuditList;
        }
        public static bool InsertUserManagerAudit(UserManagerAudit userAndManager, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("InsertUserManagerAudit", connection);

                var baseAuditId = BaseAudit.InsertBaseAudit(userAndManager.BaseAudit, dbConnection);
                cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = userAndManager.Id;

                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = userAndManager.UserId;

                cmd.Parameters.Add("@ManagerId", SqlDbType.Int).Value = userAndManager.ManagerId;
                cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = userAndManager.SiteId;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = userAndManager.CustomerId;
                
                cmd.CommandType = CommandType.StoredProcedure;
                var reader = cmd.ExecuteNonQuery();
                connection.Close();
            }
            return true;
        }
    }
}
