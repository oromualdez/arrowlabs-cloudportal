﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class LoginAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string Username { get; set; }
        public string SessionId { get; set; }
        public bool LoggedIn { get; set; }
        public string UpdatedBy { get; set; }
        public int DeviceType { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public static bool InsertLoginAudit(LoginAudit login, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(login.BaseAudit, dbConnection);

            if (login != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertLoginAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = login.Username;
                    cmd.Parameters.Add("@SessionId", SqlDbType.NVarChar).Value = login.SessionId;
                    cmd.Parameters.Add("@LoggedIn", SqlDbType.Bit).Value = login.LoggedIn;
                    cmd.Parameters.Add("@DeviceType", SqlDbType.Int).Value = login.DeviceType;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = login.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = login.CustomerId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            return false;
        }
    }
}
