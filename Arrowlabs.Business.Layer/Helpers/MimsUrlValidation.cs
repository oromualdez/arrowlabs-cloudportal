﻿// -----------------------------------------------------------------------
// <copyright file="MimsUrlValidation.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MimsUrlValidation
    {
        public string Url { get; set; }
        public bool IsValid { get; set; }
        public string Message { get; set; }
        public string GPS { get; set; }
    }
}
