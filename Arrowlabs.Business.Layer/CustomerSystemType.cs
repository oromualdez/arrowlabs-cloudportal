﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class CustomerSystemType
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }
        public string Brand { get; set; }
        public int SiteId { get; set; }
        public int CustomerInfoId { get; set; }

        public string CustomerUName { get; set; }
        public static int InsertOrUpdateCustomerSystemType(CustomerSystemType customerSystemType, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = customerSystemType.Id;

            var action = (customerSystemType.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (customerSystemType != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateCustomerSystemType", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customerSystemType.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = customerSystemType.CustomerInfoId;
                    
                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = customerSystemType.Description;
                    
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customerSystemType.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = customerSystemType.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customerSystemType.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customerSystemType.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Brand", SqlDbType.NVarChar));
                    cmd.Parameters["@Brand"].Value = customerSystemType.Brand;

                    cmd.Parameters.Add(new SqlParameter("@Version", SqlDbType.NVarChar));
                    cmd.Parameters["@Version"].Value = customerSystemType.Version;

                    cmd.Parameters.Add(new SqlParameter("@Model", SqlDbType.NVarChar));
                    cmd.Parameters["@Model"].Value = customerSystemType.Model;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customerSystemType.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateCustomerSystemType";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            customerSystemType.Id = id;
                            var audit = new CustomerSystemTypeAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customerSystemType.UpdatedBy);
                            Reflection.CopyProperties(customerSystemType, audit);
                            CustomerSystemTypeAudit.InsertCustomerSystemTypeAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertCustomerSystemTypeAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static CustomerSystemType GetCustomerSystemTypeFromSqlReader(SqlDataReader reader)
        {
            var customerSystemType = new CustomerSystemType();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                customerSystemType.CustomerUName = reader["CustomerUName"].ToString();

            if (reader["Id"] != DBNull.Value)
                customerSystemType.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["Description"] != DBNull.Value)
                customerSystemType.Description = reader["Description"].ToString();
           
            if (reader["CreatedDate"] != DBNull.Value)
                customerSystemType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["UpdatedDate"] != DBNull.Value)
                customerSystemType.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                customerSystemType.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedBy"] != DBNull.Value)
                customerSystemType.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["Model"] != DBNull.Value)
                customerSystemType.Model = reader["Model"].ToString();
            else customerSystemType.Model = "N/A";

            if (reader["Version"] != DBNull.Value)
                customerSystemType.Version = reader["Version"].ToString();
            else customerSystemType.Version = "N/A";
            if (reader["Brand"] != DBNull.Value)
                customerSystemType.Brand = reader["Brand"].ToString();
            else customerSystemType.Brand = "N/A";
            if (reader["SiteId"] != DBNull.Value)
                customerSystemType.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains( "CustomerInfoId") & reader["CustomerInfoId"] != DBNull.Value)
                customerSystemType.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"].ToString());

            return customerSystemType;
        }
        public static List<CustomerSystemType> GetSystemTypesByCustomerId(int id,string dbConnection)
        {
            var customerSystemTypes = new List<CustomerSystemType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetSystemTypesByCustomerId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customerSystemType = GetCustomerSystemTypeFromSqlReader(reader);
                    customerSystemTypes.Add(customerSystemType);
                }
                connection.Close();
                reader.Close();
            }
            return customerSystemTypes;
        }
        public static CustomerSystemType GetCustomerSystemTypeByName(string name, string dbConnection)
        {
            CustomerSystemType customerSystemType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetCustomerSystemTypeByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customerSystemType = GetCustomerSystemTypeFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return customerSystemType;
        }
        public static CustomerSystemType GetCustomerSystemTypeById(int id, string dbConnection)
        {
            CustomerSystemType customerSystemType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetCustomerSystemTypeById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customerSystemType = GetCustomerSystemTypeFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return customerSystemType;
        }
        public static List<CustomerSystemType> GetAllCustomerSystems(string dbConnection)
        {
            var customerSystemTypes = new List<CustomerSystemType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomerSystems";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customerSystemType = GetCustomerSystemTypeFromSqlReader(reader);
                    customerSystemTypes.Add(customerSystemType);
                }
                connection.Close();
                reader.Close();
            }
            return customerSystemTypes;
        }

        public static List<CustomerSystemType> GetAllCustomerSystemsByCustomerInfoId(int customerId,string dbConnection)
        {
            var customerSystemTypes = new List<CustomerSystemType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomerSystemsByCustomerInfoId";
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerInfoId"].Value = customerId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customerSystemType = GetCustomerSystemTypeFromSqlReader(reader);
                    customerSystemTypes.Add(customerSystemType);
                }
                connection.Close();
                reader.Close();
            }
            return customerSystemTypes;
        }

        public static List<CustomerSystemType> GetAllCustomerSystemsBySiteId(int siteId,string dbConnection)
        {
            var customerSystemTypes = new List<CustomerSystemType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomerSystemsBySiteId";
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customerSystemType = GetCustomerSystemTypeFromSqlReader(reader);
                    customerSystemTypes.Add(customerSystemType);
                }
                connection.Close();
                reader.Close();
            }
            return customerSystemTypes;
        }


        public static List<CustomerSystemType> GetAllCustomerSystemsByLevel5(int siteId, string dbConnection)
        {
            var customerSystemTypes = new List<CustomerSystemType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomerSystemsByLevel5";
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customerSystemType = GetCustomerSystemTypeFromSqlReader(reader);
                    customerSystemTypes.Add(customerSystemType);
                }
                connection.Close();
                reader.Close();
            }
            return customerSystemTypes;
        }

        public static void DeleteSystemTypeById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteSystemTypeById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteSystemTypeById";

                cmd.ExecuteNonQuery();
            }
        }
    }
}
