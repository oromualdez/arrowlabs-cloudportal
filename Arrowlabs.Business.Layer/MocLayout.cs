﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class MocLayout
    {
        public enum MOCSettingTypes
        {
            None = 0,
            Offence = 1,
            GPS = 2,
            HotEvent = 3,
            MobileHotEvent = 4,
            MobileOffence = 5
        }
        public enum MocLayoutTypes
        { 
            X1by1=0,
            X2by2=1,
            X2by3=2,
            X3by3=3,
            X1by3=4
        }

        public int Id { get; set; }
        public int grid1x1Tile1 { get; set; }
        public int loadLayout { get; set; }

        public int grid2x2Tile1 { get; set; }
        public int grid2x2Tile2 { get; set; }
        public int grid2x2Tile3 { get; set; }
        public int grid2x2Tile4 { get; set; }

        public int grid1x3Tile1 { get; set; }
        public int grid1x3Tile2 { get; set; }
        public int grid1x3Tile3 { get; set; }
        public int grid1x3Tile4 { get; set; }

        public int grid2x3Tile1 { get; set; }
        public int grid2x3Tile2 { get; set; }
        public int grid2x3Tile3 { get; set; }
        public int grid2x3Tile4 { get; set; }
        public int grid2x3Tile5 { get; set; }
        public int grid2x3Tile6 { get; set; }

        public int grid3x3Tile1 { get; set; }
        public int grid3x3Tile2 { get; set; }
        public int grid3x3Tile3 { get; set; }
        public int grid3x3Tile4 { get; set; }
        public int grid3x3Tile5 { get; set; }
        public int grid3x3Tile6 { get; set; }
        public int grid3x3Tile7 { get; set; }
        public int grid3x3Tile8 { get; set; }
        public int grid3x3Tile9 { get; set; } 

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public static bool DeleteMocLayoutById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMocLayout", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMocLayout";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static MocLayout GetMOCSettingsFromSqlReader(SqlDataReader resultSet)
        {
            var mocLayout = new MocLayout();


            if (resultSet["Id"] != DBNull.Value)
                mocLayout.Id = Convert.ToInt32(resultSet["Id"].ToString());

            if (resultSet["grid1x1Tile1"] != DBNull.Value)
                mocLayout.grid1x1Tile1 = Convert.ToInt16(resultSet["grid1x1Tile1"].ToString());

            if (resultSet["grid2x2Tile1"] != DBNull.Value)
                mocLayout.grid2x2Tile1 = Convert.ToInt32(resultSet["grid2x2Tile1"].ToString());
            if (resultSet["grid2x2Tile2"] != DBNull.Value)
                mocLayout.grid2x2Tile2 = Convert.ToInt32(resultSet["grid2x2Tile2"].ToString());
            if (resultSet["grid2x2Tile3"] != DBNull.Value)
                mocLayout.grid2x2Tile3 = Convert.ToInt32(resultSet["grid2x2Tile3"].ToString());
            if (resultSet["grid2x2Tile4"] != DBNull.Value)
                mocLayout.grid2x2Tile4 = Convert.ToInt32(resultSet["grid2x2Tile4"].ToString());

            if (resultSet["grid1x3Tile1"] != DBNull.Value)
                mocLayout.grid1x3Tile1 = Convert.ToInt32(resultSet["grid1x3Tile1"].ToString());
            if (resultSet["grid1x3Tile2"] != DBNull.Value)
                mocLayout.grid1x3Tile2 = Convert.ToInt32(resultSet["grid1x3Tile2"].ToString());
            if (resultSet["grid1x3Tile3"] != DBNull.Value)
                mocLayout.grid1x3Tile3 = Convert.ToInt32(resultSet["grid1x3Tile3"].ToString());
            if (resultSet["grid1x3Tile4"] != DBNull.Value)
                mocLayout.grid1x3Tile4 = Convert.ToInt32(resultSet["grid1x3Tile4"].ToString());


            if (resultSet["grid2x3Tile1"] != DBNull.Value)
                mocLayout.grid2x3Tile1 = Convert.ToInt32(resultSet["grid2x3Tile1"].ToString());
            if (resultSet["grid2x3Tile2"] != DBNull.Value)
                mocLayout.grid2x3Tile2 = Convert.ToInt32(resultSet["grid2x3Tile2"].ToString());
            if (resultSet["grid2x3Tile3"] != DBNull.Value)
                mocLayout.grid2x3Tile3 = Convert.ToInt32(resultSet["grid2x3Tile3"].ToString());
            if (resultSet["grid2x3Tile4"] != DBNull.Value)
                mocLayout.grid2x3Tile4 = Convert.ToInt32(resultSet["grid2x3Tile4"].ToString());
            if (resultSet["grid2x3Tile5"] != DBNull.Value)
                mocLayout.grid2x3Tile5 = Convert.ToInt32(resultSet["grid2x3Tile5"].ToString());
            if (resultSet["grid2x3Tile6"] != DBNull.Value)
                mocLayout.grid2x3Tile6 = Convert.ToInt32(resultSet["grid2x3Tile6"].ToString());

            if (resultSet["grid3x3Tile1"] != DBNull.Value)
                mocLayout.grid3x3Tile1 = Convert.ToInt32(resultSet["grid3x3Tile1"].ToString());
            if (resultSet["grid3x3Tile2"] != DBNull.Value)
                mocLayout.grid3x3Tile2 = Convert.ToInt32(resultSet["grid3x3Tile2"].ToString());
            if (resultSet["grid3x3Tile3"] != DBNull.Value)
                mocLayout.grid3x3Tile3 = Convert.ToInt32(resultSet["grid3x3Tile3"].ToString());
            if (resultSet["grid3x3Tile4"] != DBNull.Value)
                mocLayout.grid3x3Tile4 = Convert.ToInt32(resultSet["grid3x3Tile4"].ToString());
            if (resultSet["grid3x3Tile5"] != DBNull.Value)
                mocLayout.grid3x3Tile5 = Convert.ToInt32(resultSet["grid3x3Tile5"].ToString());
            if (resultSet["grid3x3Tile6"] != DBNull.Value)
                mocLayout.grid3x3Tile6 = Convert.ToInt32(resultSet["grid3x3Tile6"].ToString());
            if (resultSet["grid3x3Tile7"] != DBNull.Value)
                mocLayout.grid3x3Tile7 = Convert.ToInt32(resultSet["grid3x3Tile7"].ToString());
            if (resultSet["grid3x3Tile8"] != DBNull.Value)
                mocLayout.grid3x3Tile8 = Convert.ToInt32(resultSet["grid3x3Tile8"].ToString());
            if (resultSet["grid3x3Tile9"] != DBNull.Value)
                mocLayout.grid3x3Tile9 = Convert.ToInt32(resultSet["grid3x3Tile9"].ToString());
            if (resultSet["loadLayout"] != DBNull.Value)
                mocLayout.loadLayout = Convert.ToInt32(resultSet["loadLayout"].ToString());
            
            if (resultSet["CreatedBy"] != DBNull.Value)
                mocLayout.CreatedBy = resultSet["CreatedBy"].ToString();

            if (resultSet["CreatedDate"] != DBNull.Value)
                mocLayout.CreatedDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());

            if (resultSet["UpdatedDate"] != DBNull.Value)
                mocLayout.UpdatedDate = Convert.ToDateTime(resultSet["UpdatedDate"].ToString());

            return mocLayout;
        }

        public static  List< MocLayout> GetMocLayoutsCreatedBy(string name, string dbConnection)
        {
            var mocLayouts = new List<MocLayout>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetMocLayoutByCreatedBy", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                   var mocLayout = GetMOCSettingsFromSqlReader(reader);
                   mocLayouts.Add(mocLayout);
                }
                connection.Close();
                reader.Close();
            }
            return mocLayouts;
        }

        public static int InsertOrUpdateMocLayout(MocLayout settingClass, string dbConnection)
        {
            int id = 0;
            if (settingClass != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMocLayout", connection);


                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = settingClass.Id;

                    cmd.Parameters.Add(new SqlParameter("@grid1x1Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid1x1Tile1"].Value = settingClass.grid1x1Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid2x2Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid2x2Tile1"].Value = settingClass.grid2x2Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid2x2Tile2", SqlDbType.Int));
                    cmd.Parameters["@grid2x2Tile2"].Value = settingClass.grid2x2Tile2;

                    cmd.Parameters.Add(new SqlParameter("@grid2x2Tile3", SqlDbType.Int));
                    cmd.Parameters["@grid2x2Tile3"].Value = settingClass.grid2x2Tile3;

                    cmd.Parameters.Add(new SqlParameter("@grid2x2Tile4", SqlDbType.Int));
                    cmd.Parameters["@grid2x2Tile4"].Value = settingClass.grid2x2Tile4;

                    cmd.Parameters.Add(new SqlParameter("@grid1x3Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid1x3Tile1"].Value = settingClass.grid1x3Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid1x3Tile2", SqlDbType.Int));
                    cmd.Parameters["@grid1x3Tile2"].Value = settingClass.grid1x3Tile2;

                    cmd.Parameters.Add(new SqlParameter("@grid1x3Tile3", SqlDbType.Int));
                    cmd.Parameters["@grid1x3Tile3"].Value = settingClass.grid1x3Tile3;

                    cmd.Parameters.Add(new SqlParameter("@grid1x3Tile4", SqlDbType.Int));
                    cmd.Parameters["@grid1x3Tile4"].Value = settingClass.grid1x3Tile4;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile1"].Value = settingClass.grid2x3Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile2", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile2"].Value = settingClass.grid2x3Tile2;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile3", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile3"].Value = settingClass.grid2x3Tile3;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile4", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile4"].Value = settingClass.grid2x3Tile4;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile5", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile5"].Value = settingClass.grid2x3Tile5;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile6", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile6"].Value = settingClass.grid2x3Tile6;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile1"].Value = settingClass.grid3x3Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile2", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile2"].Value = settingClass.grid3x3Tile2;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile3", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile3"].Value = settingClass.grid3x3Tile3;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile4", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile4"].Value = settingClass.grid3x3Tile4;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile5", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile5"].Value = settingClass.grid3x3Tile5;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile6", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile6"].Value = settingClass.grid3x3Tile6;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile7", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile7"].Value = settingClass.grid3x3Tile7;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile8", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile8"].Value = settingClass.grid3x3Tile8;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile9", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile9"].Value = settingClass.grid3x3Tile9;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = settingClass.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedDate"].Value = DateTime.Now;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedDate"].Value = DateTime.Now;
                    cmd.Parameters.Add(new SqlParameter("@loadLayout", SqlDbType.Int));
                    cmd.Parameters["@loadLayout"].Value = settingClass.loadLayout; 

                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateMocLayout";

                    cmd.ExecuteNonQuery();
                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
