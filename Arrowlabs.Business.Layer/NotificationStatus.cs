﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
 public class NotificationStatus
    {
     public int Id { get; set; }
     public int NotificationId { get; set; }
     public int AssigneeId { get; set; }
     public bool Status { get; set; }
     public DateTime? CreatedDate { get; set; }
     public DateTime? UpdatedDate { get; set; } 
     private static NotificationStatus GetNotificationStatusFromSqlReader(SqlDataReader reader, string dbConnection)
     {
         var notificationStatus = new NotificationStatus();

         if (reader["Id"] != DBNull.Value)
             notificationStatus.Id = Convert.ToInt32(reader["Id"].ToString());
         if (reader["AssigneeId"] != DBNull.Value)
             notificationStatus.AssigneeId = Convert.ToInt32(reader["AssigneeId"].ToString());
         if (reader["NotificationId"] != DBNull.Value)
             notificationStatus.NotificationId = Convert.ToInt32(reader["NotificationId"].ToString());
         if (reader["Status"] != DBNull.Value)
             notificationStatus.Status = (String.IsNullOrEmpty(reader["Status"].ToString())) ? false : Convert.ToBoolean(reader["Status"].ToString());
         if (reader["CreatedDate"] != DBNull.Value)
             notificationStatus.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
         if (reader["UpdatedDate"] != DBNull.Value)
             notificationStatus.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

         return notificationStatus;
     }

     public static List<NotificationStatus> GetAllNotificationStatusByNotificationId(int NotificationId, string dbConnection)
     {
         var notificationStatus = new List<NotificationStatus>();
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();

             var sqlCommand = new SqlCommand("GetAllNotificationStatusByNotificationId]", connection);
             sqlCommand.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.Int)).Value = NotificationId;
             sqlCommand.CommandType = CommandType.StoredProcedure;

             var reader = sqlCommand.ExecuteReader();
             while (reader.Read())
             {
                 var notification = GetNotificationStatusFromSqlReader(reader, dbConnection);
                 notificationStatus.Add(notification);
             }
             connection.Close();
             reader.Close();
         }
         return notificationStatus;
     }

     public static NotificationStatus GetAllNotificationStatusById(int id, string dbConnection)
     {
         NotificationStatus notificationStatus = null;
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();

             var sqlCommand = new SqlCommand("GetAllNotificationStatusById", connection);
             sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id;
             sqlCommand.CommandType = CommandType.StoredProcedure;

             var reader = sqlCommand.ExecuteReader();
             while (reader.Read())
             {
                 notificationStatus = GetNotificationStatusFromSqlReader(reader, dbConnection);
                 
             }
             connection.Close();
             reader.Close();
         }
         return notificationStatus;
     }

     public static int InsertorUpdateNotificationStatus(NotificationStatus notification, string dbConnection,
      string auditDbConnection = "", bool isAuditEnabled = true)
     {
         var id = 0;
         var action = (notification.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;
         if (notification != null)
         {
             using (var connection = new SqlConnection(dbConnection))
             {
                 connection.Open();
                 var cmd = new SqlCommand("InsertOrUpdateNotificationStatus", connection);

                 cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                 cmd.Parameters["@Id"].Value = notification.Id;

                 cmd.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.NVarChar));
                 cmd.Parameters["@NotificationId"].Value = notification.NotificationId;

                 cmd.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                 cmd.Parameters["@AssigneeId"].Value = notification.AssigneeId;

                 cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Bit));
                 cmd.Parameters["@Status"].Value = notification.Status;

                 cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                 cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                 cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                 cmd.Parameters["@UpdatedDate"].Value = notification.UpdatedDate;

                 //cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                 //cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;
                 SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                 returnParameter.Direction = ParameterDirection.ReturnValue;
                 cmd.CommandType = CommandType.StoredProcedure;

                 cmd.ExecuteNonQuery();
                 id = (int)returnParameter.Value;

                 try
                 {
                     if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                     {
                         var audit = new NotificationStatusAudit();
                         audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, notification.AssigneeId.ToString());
                         Reflection.CopyProperties(notification, audit);
                         NotificationStatusAudit.InsertorUpdateNotificationStatus(audit, auditDbConnection);
                     }
                 }
                 catch (Exception ex)
                 {
                     MIMSLog.MIMSLogSave("Business Layer Audit", "InsertOrUpdateNotificationStatus", ex, dbConnection);
                 }
             }
         }
         return id;
     }

    }
}
