﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.Reports" EnableViewState="True" %>
 
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
         <style>
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        var loggedinUsername = '<%=senderName2%>';

        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }
        jQuery(document).ready(function () {
            localStorage.removeItem("activeTabDev");
            localStorage.removeItem("activeTabInci");
            localStorage.removeItem("activeTabMessage");
            localStorage.removeItem("activeTabTask");
            localStorage.removeItem("activeTabTick");
            localStorage.removeItem("activeTabUB");
            localStorage.removeItem("activeTabVer");
            localStorage.removeItem("activeTabLost");
            jQuery('#newDocument').on('shown.bs.modal', function () {
                jQuery.ajax({
                    type: "POST",
                    url: "Incident.aspx/getLocationData",
                    data: "{'id':'1'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var obj = jQuery.parseJSON(data.d)
                        getLocationMarkers(obj);
                    }
                });
            });
            jQuery('#newDocument2').on('shown.bs.modal', function () {
                Initialize();
            });

            $('input[type=password]').keyup(function () {
                // keyup event code here
                var pswd = $(this).val();
                if (pswd.length < 8) {
                    $('#length').removeClass('valid').addClass('invalid');
                    lengthGood = false;
                } else {
                    $('#length').removeClass('invalid').addClass('valid');
                    lengthGood = true;
                }
                //validate letter
                if (pswd.match(/[A-z]/)) {
                    $('#letter').removeClass('invalid').addClass('valid');
                    letterGood = true;

                } else {
                    $('#letter').removeClass('valid').addClass('invalid');
                    letterGood = false;
                }

                //validate capital letter
                if (pswd.match(/[A-Z]/)) {
                    $('#capital').removeClass('invalid').addClass('valid');
                    capitalGood = true;
                } else {
                    $('#capital').removeClass('valid').addClass('invalid');

                    capitalGood = false;
                }

                //validate number
                if (pswd.match(/\d/)) {
                    $('#number').removeClass('invalid').addClass('valid');
                    numGood = true;

                } else {
                    $('#number').removeClass('valid').addClass('invalid');
                    numGood = false;
                }
            });
            $('input[type=password]').focus(function () {
                // focus code here
                $('#pswd_info').show();
            });
            $('input[type=password]').blur(function () {
                // blur code here
                $('#pswd_info').hide();
            });

            $.ajax({
                type: "POST",
                url: "Reports.aspx/getCustomerByProjectId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#customerslst");
                    for (var i = 0; i < data.d.length; i++) {
                        cid = data.d[i].Id;
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                         );
                    }
                }
            }); 
            $('#projectlst option').remove(); 
            var projectoption = $("#projectlst");
            projectoption.append($('<option></option>').val(0).html('Select Project'));
            $.ajax({
                type: "POST",
                url: "Reports.aspx/getProjectListByCustomerId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#projectlst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                         );
                    }
                }
            });

            
            document.getElementById("<%=lfstatus2.ClientID%>").value = "All";

            document.getElementById("<%=comparison1.ClientID%>").value = "Yearly";
            document.getElementById("<%=yearly1.ClientID%>").value = "2018";
            document.getElementById("<%=yearly2.ClientID%>").value = "2018";
            document.getElementById("<%=qt1.ClientID%>").value = "Q1 (Jan-Mar)";
            document.getElementById("<%=qt2.ClientID%>").value = "Q1 (Jan-Mar)";
            document.getElementById("<%=type1.ClientID%>").value = "Valuable/Non-Valuable";
            
            document.getElementById("<%=yearlyDuty.ClientID%>").value = "2018";

            document.getElementById("<%=monthlyDuty.ClientID%>").value = "January";
             

            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getYears",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var select = document.getElementById("statusDivSelect3a");
                        var select2 = document.getElementById("statusDivSelect4a");
                        var select3 = document.getElementById("yearselectDutyRoster");
                        var opt = document.createElement('option');
                        opt.value = data.d[i];
                        opt.innerHTML = data.d[i];
                        select.appendChild(opt);

                        var opt2 = document.createElement('option');
                        opt2.value = data.d[i];
                        opt2.innerHTML = data.d[i];
                        select2.appendChild(opt2);

                        var opt3 = document.createElement('option');
                        opt3.value = data.d[i];
                        opt3.innerHTML = data.d[i];
                        select3.appendChild(opt3);
                    }
                }
            }); 

        });
        //Reporting
        function repType(type) {
            document.getElementById("inputReportType").value = type;
        }
         
        function fromDateSelectChange(e) {
            try {
                document.getElementById("<%=fromHD.ClientID%>").value = e.value;
            }
            catch (ex) {
                alert(ex);
            }
        }
        function toDateSelectChange(e) {
            try {
                document.getElementById("<%=toHD.ClientID%>").value = e.value;
            }
            catch (ex) {
                alert(ex);
            }
        }

        function createPDF() {
            var report = document.getElementById("selectReporttype").value;
            var type = document.getElementById("inputReportType").value;
            var toDate = document.getElementById("toDate").value;
            var fromDate = document.getElementById("fromDate").value;
            var searchStatus = document.getElementById("statusDivSelect").value;
            var searchIncidentStatus = document.getElementById("statusDivSelectIncidentStatus").value;
            if(report == "Incidents")
                searchStatus = document.getElementById("statusDivSelectIncident").value;

            if (report != 'Please Select') {
                jQuery.ajax({
                    type: "POST",
                    url: "Reports.aspx/CreatePDF",
                    data: "{'report':'" + report + "','type':'" + type
                        + "','toDate':'" + toDate + "','fromDate':'" + fromDate
                        + "','searchStatus':'" + searchStatus + "','uname':'" + loggedinUsername
                        + "','searchIncidentStatus':'" + searchIncidentStatus + "'}",

                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d != "No entries found!") {
                            window.open(data.d);
                            document.getElementById('successincidentScenario').innerHTML = "Report successfully created!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else {
                            showAlert(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                showAlert('Kindly select type of report you wish to make');
            }

    }
    //User-profile
        function changePassword() {
            try {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;
                var isErr = false;
                if (!isErr) {
                    if (!letterGood) {
                        showAlert('Password does not contain letter');
                        isErr = true;
                    }
                    if (!isErr) {
                        if (!capitalGood) {
                            showAlert('Password does not contain capital letter');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!numGood) {
                            showAlert('Password does not contain number');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!lengthGood) {
                            showAlert('Password length not enough');
                            isErr = true;
                        }
                    }
                }
                if (!isErr) {
                    if (newPw == confPw && newPw != "" && confPw != "") {
                        jQuery.ajax({
                            type: "POST",
                            url: "Reports.aspx/changePW",
                            data: "{'id':'0','password':'" + confPw + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d != "LOGOUT") {
                                    jQuery('#changePasswordModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                    jQuery('#successfulDispatch').modal('show');
                                    document.getElementById("newPwInput").value = "";
                                    document.getElementById("confirmPwInput").value = "";
                                    document.getElementById("oldPwInput").value = confPw;
                                }
                                else {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        showAlert("Kindly match new password with confirm password.")
                    }
                }
            }
            catch (ex)
            { showAlert(ex) }
        }

        function editUnlock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
            document.getElementById("editProfileA").style.display = "none";
            document.getElementById("saveProfileA").style.display = "block";
            document.getElementById("profileEmailAddDIV").style.display = "none";
            document.getElementById("profileEmailAddEditDIV").style.display = "block";
            document.getElementById("userFullnameSpanDIV").style.display = "none";
            document.getElementById("userFullnameSpanEditDIV").style.display = "block";
            if (document.getElementById('profileRoleName').innerHTML == "User") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "block";
                document.getElementById("dirInfoDIV").style.display = "none";


            }
            else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "none";
                document.getElementById("dirInfoDIV").style.display = "block";
            }
        }
        function editJustLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        function editLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
            var role = document.getElementById('UserRoleSelector').value;
            var roleid = 0;
            var supervisor = 0;
            var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
            if (retVal > 0) {
                assignUserProfileData();
            }
            else {
                alert('failed user creation');
            }

        }
        function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
            var output = 0;
            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/addUserProfile",
                data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                }
            });
            return output;
        }
        function saveTZ() {
            var scountr = $("#countrySelect option:selected").text();
            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/saveTZ",
                data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                        jQuery('#successfulDispatch').modal('show');
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getserverInfo() {
            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getServerData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('mobileRemaining').value = data.d[9];
                        document.getElementById('mobileTotal').value = data.d[10];
                        document.getElementById('mobileUsed').value = data.d[11];
                        document.getElementById("countrySelect").value = data.d[28];
                        jQuery('#countrySelect').selectpicker('val', data.d[28]);


                        document.getElementById('surveillanceCheck').checked = false;
                        document.getElementById('notificationCheck').checked = false;
                        document.getElementById('locationCheck').checked = false;
                        document.getElementById('ticketingCheck').checked = false;
                        document.getElementById('taskCheck').checked = false;
                        document.getElementById('incidentCheck').checked = false;
                        document.getElementById('warehouseCheck').checked = false;
                        document.getElementById('chatCheck').checked = false;
                        document.getElementById('collaborationCheck').checked = false;
                        document.getElementById('lfCheck').checked = false;
                        document.getElementById('dutyrosterCheck').checked = false;
                        document.getElementById('postorderCheck').checked = false;
                        document.getElementById('verificationCheck').checked = false;
                        document.getElementById('requestCheck').checked = false;
                        document.getElementById('dispatchCheck').checked = false;
                        document.getElementById('activityCheck').checked = false;

                        if (data.d[12] == "true")
                            document.getElementById('surveillanceCheck').checked = true;
                        if (data.d[13] == "true")
                            document.getElementById('notificationCheck').checked = true;
                        if (data.d[14] == "true")
                            document.getElementById('locationCheck').checked = true;
                        if (data.d[15] == "true")
                            document.getElementById('ticketingCheck').checked = true;
                        if (data.d[16] == "true")
                            document.getElementById('taskCheck').checked = true;
                        if (data.d[17] == "true")
                            document.getElementById('incidentCheck').checked = true;
                        if (data.d[18] == "true")
                            document.getElementById('warehouseCheck').checked = true;
                        if (data.d[19] == "true")
                            document.getElementById('chatCheck').checked = true;
                        if (data.d[20] == "true")
                            document.getElementById('collaborationCheck').checked = true;
                        if (data.d[21] == "true")
                            document.getElementById('lfCheck').checked = true;
                        if (data.d[22] == "true")
                            document.getElementById('dutyrosterCheck').checked = true;
                        if (data.d[23] == "true")
                            document.getElementById('postorderCheck').checked = true;
                        if (data.d[24] == "true")
                            document.getElementById('verificationCheck').checked = true;
                        if (data.d[25] == "true")
                            document.getElementById('requestCheck').checked = true;
                        if (data.d[26] == "true")
                            document.getElementById('dispatchCheck').checked = true;
                        if (data.d[27] == "true")
                            document.getElementById('activityCheck').checked = true;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function assignUserProfileData() {
            try {
                jQuery.ajax({
                    type: "POST",
                    url: "Reports.aspx/getUserProfileData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            try {
                                document.getElementById('containerDiv2').style.display = "none";
                                document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                document.getElementById('profileRoleName').innerHTML = data.d[5];
                                document.getElementById('profileManagerName').innerHTML = data.d[6];
                                document.getElementById('userStatusSpan').innerHTML = data.d[8];

                                if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                    document.getElementById('containerDiv2').style.display = "block";
                                    document.getElementById('defaultGenderDiv').style.display = "none";
                                    getserverInfo();
                                }

                                var el = document.getElementById('userStatusIconSpan');
                                if (el) {
                                    el.className = data.d[9];
                                }
                                document.getElementById('userprofileImgSrc').src = data.d[10];
                                document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                document.getElementById('userFirstnameSpan').value = data.d[13];
                                document.getElementById('userLastnameSpan').value = data.d[14];
                                document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                document.getElementById('profileEmailAddEdit').value = data.d[3];

                                document.getElementById('oldPwInput').value = data.d[16];

                                document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                document.getElementById('profileGender').innerHTML = data.d[20];

                                if (document.getElementById('profileRoleName').innerHTML != "Level 7") {
                                    document.getElementById('deviceTypesDiv').innerHTML = "<i class='fa fa-mobile fa-2x mr-2x'></i><i style='color:lime;' class='fa fa-laptop fa-2x mr-2x'></i>";//data.d[11];

                                }
                            }
                            catch (err) { alert(err) }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            catch (err)
            { alert(err) }
        }
        function forceLogout() {
            document.getElementById('<%= closingbtn.ClientID %>').click();
        }
        function rowchoice(name) {
            assignrowData(name);
            incidentHistoryData(name);
        }
        function incidentHistoryData(id) {
            jQuery('#divIncidentHistoryActivity div').html('');
            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getEventHistoryData",
                data: "{'id':'" + id + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('divIncidentHistoryActivity').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function projectlstOnChange(e) {
            document.getElementById("<%=projectID.ClientID%>").value = e.options[e.selectedIndex].value;

            if (e.options[e.selectedIndex].value > 0) {
                
                document.getElementById('contractslst').disabled = true;
                document.getElementById("<%=contractID.ClientID%>").value = "0";
                $('#contractslst option').remove();
                var contractoption = $("#contractslst");
                contractoption.append($('<option></option>').val(0).html('Select Contract'));
                $.ajax({
                    type: "POST",
                    url: "Reports.aspx/getContractListByCustomerId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#contractslst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                             );
                        }
                    }
                });
            }
            else {
                document.getElementById('contractslst').disabled = "";  
            }
            if (document.getElementById('customerslst').value > 0) {

            }
            else {
                $('#customerslst option').remove();
                if (e.options[e.selectedIndex].value == 0) {
                    $('#contractslst option').remove();
                    $('#projectlst option').remove();
                    var contractoption = $("#contractslst");
                    contractoption.append($('<option></option>').val(0).html('Select Contract'));
                    var projectoption = $("#projectlst");
                    projectoption.append($('<option></option>').val(0).html('Select Project'));
                    $.ajax({
                        type: "POST",
                        url: "Reports.aspx/getContractListByCustomerId",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#contractslst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                                 );
                            }
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "Reports.aspx/getProjectListByCustomerId",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#projectlst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                                 );
                            }
                        }
                    });
                }
                var cid = 0;
                $.ajax({
                    type: "POST",
                    url: "Reports.aspx/getCustomerByProjectId",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#customerslst");
                        for (var i = 0; i < data.d.length; i++) {
                            cid = data.d[i].Id;
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                             );
                        }
                    }
                });
                if (cid > 0) {
                    $('#contractslst option').remove();
                    var contractoption = $("#contractslst");
                    contractoption.append($('<option></option>').val(0).html('Select Contract'));
                    $.ajax({
                        type: "POST",
                        url: "Reports.aspx/getContractListByCustomerId",
                        data: "{'id':'" + cid + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#contractslst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                                 );
                            }
                        }
                    });
                }
            }
        }
        function contractOnChange(e) {
            document.getElementById("<%=contractID.ClientID%>").value = e.options[e.selectedIndex].value;

            if (e.options[e.selectedIndex].value > 0) {
                document.getElementById("<%=projectID.ClientID%>").value = "0";
                document.getElementById('projectlst').disabled = true;  
            }
            else {
                document.getElementById('projectlst').disabled = "";
            }
        }
        function statuslstOnChange(e) {
            document.getElementById("<%=statusID.ClientID%>").value = e.options[e.selectedIndex].value;
        }
        function tasktypeLstOnChange(e) {
            document.getElementById("<%=typeID.ClientID%>").value = e.options[e.selectedIndex].value;
        }
        
        function customersOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
            document.getElementById("<%=lfstatus.ClientID%>").value = e.options[e.selectedIndex].value;
            document.getElementById("<%=contractID.ClientID%>").value = 0;
            $('#contractslst option').remove();

            var contractoption = $("#contractslst");
            contractoption.append($('<option></option>').val(0).html('Select Contract'));


            if (e.options[e.selectedIndex].value > 0) {
 
                $.ajax({
                    type: "POST",
                    url: "Reports.aspx/getContractListByCustomerId",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#contractslst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                             );
                        }
                    }
                });
                if (document.getElementById('projectlst').value > 0) {

                }
                else {
                    $('#projectlst option').remove();
                    var projectoption = $("#projectlst");
                    projectoption.append($('<option></option>').val(0).html('Select Project'));
                    $.ajax({
                        type: "POST",
                        url: "Reports.aspx/getProjectListByCustomerId",
                        data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#projectlst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                                 );
                            }
                        }
                    });
                }

            }
            else {

                $('#customerslst option').remove();
                $.ajax({
                    type: "POST",
                    url: "Reports.aspx/getCustomerByProjectId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#customerslst");
                        for (var i = 0; i < data.d.length; i++) {
                            cid = data.d[i].Id;
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                             );
                        }
                    }
                });
                document.getElementById('contractslst').disabled = "";
                document.getElementById('projectlst').disabled = "";
                $('#projectlst option').remove();
                var projectoption = $("#projectlst");
                projectoption.append($('<option></option>').val(0).html('Select Project'));
                $.ajax({
                    type: "POST",
                    url: "Reports.aspx/getProjectListByCustomerId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#projectlst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                             );
                        }
                    }
                });
            }

        }
        function selectStatusDivDTRoster(e) {
            document.getElementById("<%=dutyStatus.ClientID%>").value = e.value;
            if (e.options[e.selectedIndex].value == "Employee Report") {
                document.getElementById("StatusDivDTUserRoster").style.display = "block";
                document.getElementById("specGen4").style.display = "block";
                document.getElementById("specGen3").style.display = "none";

                document.getElementById("yearselectDutyRosterDIV").style.display = "block";
                document.getElementById("toDiv").style.display = "none";
                document.getElementById("froDiv").style.display = "none";
            }
            else {
                document.getElementById("StatusDivDTUserRoster").style.display = "none";
                document.getElementById("specGen4").style.display = "none";
                document.getElementById("specGen3").style.display = "block";

                document.getElementById("yearselectDutyRosterDIV").style.display = "none";
                document.getElementById("toDiv").style.display = "block";
                document.getElementById("froDiv").style.display = "block";
            }
        }
        function selectStatusDivDTUserRoster(e) {
            document.getElementById("<%=dutyUser.ClientID%>").value = e.value;
            
        }
            
        function selectReporttypeChange(e) {
            if (e.options[e.selectedIndex].value == "Duty Roster") {

                $("#statusDivSelectDTRoster").val("Staff Report");
                document.getElementById("StatusDivDTUserRoster").style.display = "none";
                document.getElementById("StatusDiv").style.display = "none";
                document.getElementById("StatusDivIncident").style.display = "none";
                document.getElementById("StatusDivIncidentStatus").style.display = "none";

                document.getElementById("yearselectDutyRosterDIV").style.display = "none";
                
                document.getElementById("StatusDivDTRoster").style.display = "block";
                document.getElementById("yearselectDutyRosterDIV").style.display = "none";
                document.getElementById("toDiv").style.display = "block";
                document.getElementById("froDiv").style.display = "block";
                document.getElementById("CustomerDiv").style.display = "none";
                document.getElementById("normGen").style.display = "none";
                document.getElementById("specGen").style.display = "none";
                document.getElementById("specGen2").style.display = "none";

                document.getElementById("specGen3").style.display = "block";
                document.getElementById("specGen4").style.display = "none";


                document.getElementById("StatusDiv22").style.display = "none";
                document.getElementById("StatusDiv2").style.display = "none";
                document.getElementById("StatusDiv3").style.display = "none";
                document.getElementById("StatusDiv4").style.display = "none";
                document.getElementById("StatusDiv5").style.display = "none";
            
            }
            else if (e.options[e.selectedIndex].value == "Lost and Found") {
                
                $("#statusDivSelectt").val("All");
                document.getElementById("StatusDiv").style.display = "none";

                document.getElementById("StatusDivDTRoster").style.display = "none";
                document.getElementById("specGen3").style.display = "none";
                document.getElementById("specGen4").style.display = "none";
                document.getElementById("StatusDiv22").style.display = "block";
                document.getElementById("StatusDiv2").style.display = "none";
                document.getElementById("StatusDiv3").style.display = "none";
                document.getElementById("StatusDiv4").style.display = "none";
                document.getElementById("StatusDiv5").style.display = "none";
                document.getElementById("StatusDivIncident").style.display = "none";
                document.getElementById("StatusDivIncidentStatus").style.display = "none";
                document.getElementById("toDiv").style.display = "block";
                document.getElementById("CustomerDiv").style.display = "none";
                document.getElementById("normGen").style.display = "none";
                document.getElementById("specGen").style.display = "none";
                document.getElementById("specGen2").style.display = "block";
                document.getElementById("froDiv").style.display = "block";

                document.getElementById("yearselectDutyRosterDIV").style.display = "none"; 
            }
            else if (e.options[e.selectedIndex].value == "Asset") {

                document.getElementById("StatusDivDTRoster").style.display = "none";
                document.getElementById("specGen3").style.display = "none";
                document.getElementById("specGen4").style.display = "none";
                document.getElementById("StatusDiv22").style.display = "none";
                document.getElementById("StatusDiv2").style.display = "none";
                document.getElementById("StatusDiv3").style.display = "none";
                document.getElementById("StatusDiv4").style.display = "none";
                document.getElementById("StatusDiv5").style.display = "none";
                document.getElementById("StatusDiv").style.display = "block";
                document.getElementById("StatusDivIncident").style.display = "none";
                document.getElementById("StatusDivIncidentStatus").style.display = "none";
                document.getElementById("toDiv").style.display = "block";
                document.getElementById("CustomerDiv").style.display = "none";
                document.getElementById("normGen").style.display = "block";
                document.getElementById("specGen").style.display = "none";
                document.getElementById("specGen2").style.display = "none";
                document.getElementById("froDiv").style.display = "block";

                document.getElementById("yearselectDutyRosterDIV").style.display = "none"; 
            }
            else if (e.options[e.selectedIndex].value == "Task") {

                document.getElementById("StatusDivDTRoster").style.display = "none";
                document.getElementById("specGen3").style.display = "none";
                document.getElementById("specGen4").style.display = "none";
                document.getElementById("StatusDiv22").style.display = "none";
                document.getElementById("StatusDiv2").style.display = "none";
                document.getElementById("StatusDiv3").style.display = "none";
                document.getElementById("StatusDiv4").style.display = "none";
                document.getElementById("StatusDiv5").style.display = "none";
                document.getElementById("StatusDiv").style.display = "none";
                document.getElementById("StatusDivIncident").style.display = "none";
                document.getElementById("StatusDivIncidentStatus").style.display = "none";
                document.getElementById("toDiv").style.display = "block";
                document.getElementById("CustomerDiv").style.display = "block";
                document.getElementById("normGen").style.display = "none";
                document.getElementById("specGen").style.display = "block";
                document.getElementById("specGen2").style.display = "none";
                document.getElementById("froDiv").style.display = "block";

                document.getElementById("yearselectDutyRosterDIV").style.display = "none"; 

            }
            else {

                document.getElementById("StatusDivDTRoster").style.display = "none";
                document.getElementById("specGen3").style.display = "none";
                document.getElementById("specGen4").style.display = "none";
                document.getElementById("StatusDiv2").style.display = "none";
                document.getElementById("StatusDiv3").style.display = "none";
                document.getElementById("StatusDiv4").style.display = "none";
                document.getElementById("StatusDiv5").style.display = "none";
                document.getElementById("StatusDivIncident").style.display = "block";
                document.getElementById("StatusDivIncidentStatus").style.display = "block";
                document.getElementById("StatusDiv22").style.display = "none";
                document.getElementById("StatusDiv").style.display = "none";
                document.getElementById("toDiv").style.display = "block";
                document.getElementById("CustomerDiv").style.display = "none";
                document.getElementById("normGen").style.display = "block";
                document.getElementById("specGen").style.display = "none";
                document.getElementById("specGen2").style.display = "none";
                document.getElementById("froDiv").style.display = "block";

                document.getElementById("yearselectDutyRosterDIV").style.display = "none"; 
            }
        }
        function assignrowData(id) {
            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getTableRowData",
                data: "{'id':'" + id + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("usernameSpan").innerHTML = data.d[0];
                        document.getElementById("timeSpan").innerHTML = data.d[1];
                        document.getElementById("typeSpan").innerHTML = data.d[2];
                        document.getElementById("statusSpan").innerHTML = data.d[3];
                        document.getElementById("locSpan").innerHTML = data.d[4];
                        document.getElementById("receivedBySpan").innerHTML = data.d[5];
                        document.getElementById("phonenumberSpan").innerHTML = data.d[6];
                        document.getElementById("emailSpan").innerHTML = data.d[7];
                        document.getElementById("descriptionSpan").innerHTML = data.d[8];
                        document.getElementById("instructionSpan").innerHTML = data.d[9];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function addrowtoTable1() {

            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getLast15MinutesData",
                data: "{'id':'1','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#last15minutesTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoTable2() {

            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getLast30MinutesData",
                data: "{'id':'1','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#last30minutesTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoTable3() {

            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getLastHourData",
                data: "{'id':'1','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#lasthourTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoTable4() {

            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getTop5OffenceData",
                data: "{'id':'1','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#top5offTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoTable5() {

            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getTop5HotEventData",
                data: "{'id':'1','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#top5hotTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoTable6() {

            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getMobileHotData",
                data: "{'id':'1','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#mobilehotTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoTable7() {

            jQuery.ajax({
                type: "POST",
                url: "Reports.aspx/getHotData",
                data: "{'id':'1','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#hoteventTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function statusDivChanget(e) {
            try {
                document.getElementById("<%=lfstatus2.ClientID%>").value = e.value;

                if (e.options[e.selectedIndex].value == "Comparison") {
                    document.getElementById("StatusDiv2").style.display = "block";
                    document.getElementById("StatusDiv3").style.display = "block";
                    document.getElementById("StatusDiv4").style.display = "block";
                    document.getElementById("StatusDiv5").style.display = "block";

                    document.getElementById("froDiv").style.display = "none";
                    document.getElementById("toDiv").style.display = "none";

                }
                else {
                    document.getElementById("StatusDiv2").style.display = "none";
                    document.getElementById("StatusDiv3").style.display = "none";
                    document.getElementById("StatusDiv4").style.display = "none";
                    document.getElementById("StatusDiv5").style.display = "none";

                    document.getElementById("froDiv").style.display = "block";
                    document.getElementById("toDiv").style.display = "block";
                }
            }
            catch (ex) {
                alert(ex);
            }
        }
        function statusDivChange5(e) {
            document.getElementById("<%=type1.ClientID%>").value = e.options[e.selectedIndex].value;
        }
        function statusDivChange2(e) {
            document.getElementById("<%=comparison1.ClientID%>").value = e.options[e.selectedIndex].value;

            if (e.options[e.selectedIndex].value == "Quarterly") {
                document.getElementById("statusDivSelect3bdiv").style.display = "block";
                document.getElementById("statusDivSelect4bdiv").style.display = "block";
            }
            else {
                document.getElementById("statusDivSelect3bdiv").style.display = "none";
                document.getElementById("statusDivSelect4bdiv").style.display = "none";
            }
        }

         
         function yearselectDutyRosterChange(e) {
            document.getElementById("<%=yearlyDuty.ClientID%>").value = e.options[e.selectedIndex].value;
         } 
        function monthselectDutyRosterChange(e) {
            document.getElementById("<%=monthlyDuty.ClientID%>").value = e.options[e.selectedIndex].value;
        }
        function statusDivSelect3aChange(e) {
            document.getElementById("<%=yearly1.ClientID%>").value = e.options[e.selectedIndex].value;
        }
        function statusDivSelect3bChange(e) {
            document.getElementById("<%=qt1.ClientID%>").value = e.options[e.selectedIndex].value;
        }
        function statusDivSelect4aChange(e) {
            document.getElementById("<%=yearly2.ClientID%>").value = e.options[e.selectedIndex].value;
        }
        function statusDivSelect4bChange(e) {
            document.getElementById("<%=qt2.ClientID%>").value = e.options[e.selectedIndex].value;
        }
    </script>
            <section class="content-wrapper" role="main">
            <div class="content">
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">REPORTS</span></h3>
                                </div>
                                <div class="col-md-7">
                                    <div class="panel-control">
                                        <ul id="demo3-tabs" class="nav nav-tabs nav-main">

                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div role="group" class="pull-right">
                                        <%=siteName%>
                                        <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName3%></a><a style="margin-left:0px;" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                    <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                           <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                              </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
							<div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <!--<div class="col-md-2" style="display:none;">
                                    <div class="row vertical-navigation vertical-components-show" style="display:none;">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-last15minutes"  data-toggle="tab">Recent Incidents</a>
                                                    </li>
                                                    <li><a href="#component-mobilehot" data-toggle="tab">Incidents Status</a>
                                                    </li>
                                                    <li><a href="#component-top5Hot" data-toggle="tab">Top 5 Hot Events</a>
                                                    </li>
                                                    <li><a href="#component-top5off" data-toggle="tab">Top 5 Offences</a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    <div class="row vertical-navigation new-events" style="display:none;">
                                       <div class="panel-control">
											<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Export As <i class="fa fa-angle-down"></i></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#exportDiv" data-toggle="modal" onclick="repType('PDF')"><span class="fa fa-download"></span> PDF</a></li>
                                                <li style="display:none;"><a href="#exportDiv" data-toggle="modal" onclick="repType('EXCEL')"><span class="fa fa-download"></span> MS Excel</a></li>
                                                <li style="display:none;"><a href="#exportDiv" data-toggle="modal" onclick="repType('XML')"><span class="fa fa-download"></span> XML</a></li>
                                              </ul>
					                    </div>
                                    </div>
                                    </div> -->
                                    <div class="col-md-12">
<div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">EXPORT REPORT</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                 						<div class="row" style="margin-top:10px;">
                             <div class="col-md-2 ">
                                 <p class="font-bold red-color no-vmargin" style="margin-top:7px;">Report :</p></div>
							<div class="col-md-10">
										<label class="select select-o">
                                              <select id="selectReporttype" onchange="selectReporttypeChange(this);">
                                                  <option>Please Select</option>
<%--                                                  <option <%=HEView%>>HotEvent</option> --%>
                                                  <option <%=HEView%>>Incidents</option>
                                                  <option <%=DRView%>>Duty Roster</option>
                                                  <option <%=LFView%>>Lost and Found</option>
                                                  <option <%=ASView%>>Asset</option>
                                                  <option <%=TAView%>>Task</option>
                                               </select>
										 </label>
							</div>
						</div>
                        <div class="row" id="froDiv" style="margin-top:10px;">
                                                             <div class="col-md-2 "><p class="font-bold red-color no-vmargin" style="margin-top:7px;">From :</p></div>
									<div class="col-md-10">
										<div class="form-group">
                                          <div class="input-group input-group-in"> 
                                            <input id="fromDate" onchange="fromDateSelectChange(this)" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">
                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                    </div>
								</div>
                        <div class="row" id="toDiv" style="margin-top:10px;">
                                                            <div class="col-md-2 "><p class="font-bold red-color no-vmargin" style="margin-top:7px;">To :</p></div>
									<div class="col-md-10">
										<div class="form-group">
                                          <div class="input-group input-group-in">
                                            <input id="toDate" onchange="toDateSelectChange(this)" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">
                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                    </div>
								</div>
                        <div class="row" id="StatusDiv" style="display:none;margin-top:10px;">
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Status :</p></div>
									<div class="col-md-10">
										<label class="select select-o">
                                              <select id="statusDivSelect">
                                                  <option>All</option>
                                                  <option>Checked In</option>
                                                  <option>Checked Out</option>
                                               </select>
										 </label>
                                    </div>
								</div>



                        <div class="row" id="yearselectDutyRosterDIV" style="display:none;margin-top:10px;"> 
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Date :</p></div>
                                    <div class="col-md-5">
                                         <label class="select select-o">
                                              <select id="yearselectDutyRoster" onchange="yearselectDutyRosterChange(this)"> 
                                               </select>
										 </label>
                                    </div>
									<div class="col-md-5"> 
                                         <label class="select select-o">
                                              <select id="monthselectDutyRoster" onchange="monthselectDutyRosterChange(this)"> 
                                                  <option>January</option>
                                                  <option>February</option>
                                                  <option>March</option>
                                                  <option>April</option>
                                                  <option>May</option>
                                                  <option>June</option>
                                                  <option>July</option>
                                                  <option>August</option>
                                                  <option>September</option>
                                                  <option>October</option>
                                                  <option>November</option>
                                                  <option>December</option>
                                               </select>
										 </label>
                                    </div>
								</div>
                        <div class="row" id="StatusDivDTRoster" style="display:none;margin-top:10px;"> 
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Type :</p></div>
									<div class="col-md-10">
										<label class="select select-o">
                                              <select id="statusDivSelectDTRoster" onchange="selectStatusDivDTRoster(this)">
                                                  <option>Staff Report</option>
                                                  <option>Employee Report</option>
                                               </select>
										 </label>
                                    </div>
								</div>

                                                 <div class="row" id="StatusDivDTUserRoster" style="display:none;margin-top:10px;">
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">User :</p></div>
									<div class="col-md-10">
										<label class="select select-o">
                                              <select id="statusDivSelectDTUserRoster" runat="server" onchange="selectStatusDivDTUserRoster(this)">  
                                               </select>
										 </label>
                                    </div>
								</div>

                       <div class="row" id="StatusDivIncident" style="display:none;margin-top:10px;">
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Type :</p></div>
									<div class="col-md-10">
										<label class="select select-o">
                                              <select id="statusDivSelectIncident">
                                                  <option>All</option>
                                                  <option>MobileHotEvent</option>
                                                  <option>Request</option>
                                                  <option>System Created</option>
                                               </select>
										 </label>
                                    </div>
								</div>
                       <div class="row" id="StatusDivIncidentStatus" style="display:none;margin-top:10px;">
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Status :</p></div>
									<div class="col-md-10">
										<label class="select select-o">
                                              <select id="statusDivSelectIncidentStatus">
                                                  <option>All</option>
                                                  <option>Pending</option>
                                                  <option>Dispatch</option>
                                                  <option>Park</option>
                                                  <option>Engage</option>
                                                  <option>Release</option> 
                                                  <option>Complete</option> 
<%--                                                  <option>Reject</option> --%>
                                                  <option>Resolve</option> 
<%--                                                  <option>Arrived</option> 
                                                  <option>Leave</option> 
                                                  <option>Escalated</option> --%>
                                               </select>
										 </label>
                                    </div>
								</div>  
                        <div class="row" id="StatusDiv22" style="display:none;margin-top:10px;" >
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Type :</p></div>
									<div class="col-md-10">
										<label class="select select-o">
                                              <select id="statusDivSelectt" onchange="statusDivChanget(this)">
                                                  <option>All</option>
                                                 <!--/ <option>Found</option>-->
                                                 <!--/ <option>Return</option>-->
                                                <!--/  <option>Dispose</option>-->
                                                <!--/  <option>Donate</option>-->
                                                  <option>Comparison</option>
                                                  <option>Valuable/Non-Valuable</option>
                                                  <option>Delivered/Not-Delivered</option> 
                                                  <option>Valuable Returned/Not-Returned</option>
                                                  <option>Charity Returned/Not-Returned</option>
                                                  <option>Alcohol Disposed</option> 
                                               </select>
										 </label>
                                    </div>
								</div> 
                                                 <div class="row" id="StatusDiv2" style="margin-top:10px;display:none">
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Comparison :</p></div>
									<div class="col-md-10">
										<label class="select select-o">
                                              <select id="statusDivSelect2" onchange="statusDivChange2(this)">
                                                  <option>Yearly</option>
                                                  <option>Quarterly</option>
                                               </select>
										 </label>
                                    </div>
								</div>
                               <div class="row" id="StatusDiv5" style="margin-top:10px;display:none">
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Type :</p></div>
									<div class="col-md-10">
										<label class="select select-o">
                                              <select id="statusDivSelect5" onchange="statusDivChange5(this)">
                                                  <option>Valuable/Non-Valuable</option>
                                                  <option>Delivered/Not-Delivered</option> 
                                                   <!--/<option>Enquiry</option>-->
                                               </select>
										 </label>
                                    </div>
								</div>
                                <div class="row" id="StatusDiv3" style="margin-top:10px;display:none">
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Range 1 :</p></div>
									<div class="col-md-5">
										<label class="select select-o">
                                              <select id="statusDivSelect3a" onchange="statusDivSelect3aChange(this)">  
                                               </select>
										 </label>
                                    </div>
                                    <div class="col-md-5" style="display:none" id="statusDivSelect3bdiv">
										<label class="select select-o">
                                              <select id="statusDivSelect3b" onchange="statusDivSelect3bChange(this)">
                                                  <option>Q1 (Jan-Mar)</option>
                                                  <option>Q2 (Apr-Jun)</option>
                                                  <option>Q3 (Jul-Sep)</option>
                                                  <option>Q4 (Oct-Dec)</option>
                                               </select>
										 </label>
                                    </div>
								</div>
                                 <div class="row" id="StatusDiv4" style="margin-top:10px;display:none">
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Range 2 :</p></div>
									<div class="col-md-5">
										<label class="select select-o">
                                              <select id="statusDivSelect4a" onchange="statusDivSelect4aChange(this)">
                                               </select>
										 </label>
                                    </div>
                                    <div class="col-md-5" style="display:none" id="statusDivSelect4bdiv">
										<label class="select select-o">
                                              <select id="statusDivSelect4b" onchange="statusDivSelect4bChange(this)">
                                                  <option>Q1 (Jan-Mar)</option>
                                                  <option>Q2 (Apr-Jun)</option>
                                                  <option>Q3 (Jul-Sep)</option>
                                                  <option>Q4 (Oct-Dec)</option>
                                               </select>
										 </label>
                                    </div>
								</div>
                                <div class="row" id="CustomerDiv" style="display:none;margin-top:10px;">
                                    <div class="row" >
                                 <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Account :</p></div>
                                     <div class="col-md-10">
                                        <label class="select select-o">
                                           <select id="customerslst" onchange="customersOnChange(this);">
											</select>
										 </label>
									</div>
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                   <div class="col-md-2"><p class="font-bold red-color no-vmargin" style="margin-top:7px;">Contract :</p></div>
                                     <div class="col-md-10">
                                        <label class="select select-o">
                                           <select id="contractslst" onchange="contractOnChange(this);" >
                                               <option value="0" >Select Contract</option>
											</select>
										 </label>
									</div>
                                        </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="col-md-2">
                                            <p class="font-bold red-color no-vmargin" style="margin-top: 7px;">Project :</p>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="select select-o">
                                                <select id="projectlst" onchange="projectlstOnChange(this);">
                                                    <option value="0">Select Project</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="col-md-2">
                                            <p class="font-bold red-color no-vmargin" style="margin-top: 7px;">Status :</p>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="select select-o">
                                                <select id="statusLst" onchange="statuslstOnChange(this);">
                                                    <option value="-1">Select Status</option>
                                                    <option value="0">Pending</option>
                                                    <option value="1">Inprogress</option>
                                                    <option value="2">Completed</option>
                                                    <option value="3">Accepted</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="col-md-2">
                                            <p class="font-bold red-color no-vmargin" style="margin-top: 7px;">Type :</p>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="select select-o">
                                                <select id="taskTypeSelect" runat="server" onchange="tasktypeLstOnChange(this);">
 
                                                </select>
                                            </label>
                                        </div>
                                    </div>
								</div>

                                                <div class="row horizontal-navigation mt-4x pull-right" >
                                                   <div class="panel-control">
                                                      <ul class="nav nav-tabs">
                                                        <li  id="normGen"><a href="#" onclick="createPDF()">GENERATE</a>
                                                         </li>
                                                         <li id="specGen" style="display:none"><a href="#" onclick="document.getElementById('<%= btnExport.ClientID %>').click();">GENERATE</a>
                                                         </li>
                                                          <li id="specGen2" style="display:none"><a href="#" onclick="document.getElementById('<%= btnExport2.ClientID %>').click();">GENERATE</a>
                                                         </li>
                                                          <li id="specGen4" style="display:none"><a href="#" onclick="document.getElementById('<%= btnExport3.ClientID %>').click();">GENERATE</a>
                                                         </li>
                                                           <li id="specGen3" style="display:none"><a href="#" onclick="document.getElementById('<%= btnExport4.ClientID %>').click();">GENERATE</a>
                                                         </li>
                                                      </ul>
                                                      <!-- /.nav -->
                                                   </div>
                                                </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div  class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>  
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                  			<label class="select select-o" >
                                                                                    <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>       
                                                            <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                                      <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Contract</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                   <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                         <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                        
                                            </div>
                                                         <div class="row" style="display:none;">
                                            <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </div>
                <!-- /panel-body -->
            </div>
                                                  <div class="row" style="display:none;">
                                                     <asp:Chart ID="Chart1" runat="server" Height="350" Width="350">
            <series>
                <asp:Series Name="Series1" ChartType="Pie" IsVisibleInLegend="true" CustomProperties="PieLabelStyle=Disabled">
                <Points>
                    <asp:DataPoint AxisLabel="Returned" YValues="90" />
                    <asp:DataPoint AxisLabel="Disposed" YValues="120" />
                    <asp:DataPoint AxisLabel="Charity" YValues="300" />
                    <asp:DataPoint AxisLabel="Authorities" YValues="240" />
                    <asp:DataPoint AxisLabel="Found" YValues="100" />
                </Points>
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        <titles><asp:Title Name="Title1" ></asp:Title></titles>
                                                         <Legends>
                <asp:Legend>
                </asp:Legend>
            </Legends>
                                                     </asp:Chart> 

                                                     <asp:Chart ID="Chart2" runat="server" Height="350" Width="350">
            <series>
                <asp:Series Name="Series1" ChartType="Pie" IsVisibleInLegend="true" CustomProperties="PieLabelStyle=Disabled">
                <Points>
                    <asp:DataPoint AxisLabel="Returned" YValues="90" />
                    <asp:DataPoint AxisLabel="Disposed" YValues="120" />
                    <asp:DataPoint AxisLabel="Charity" YValues="300" />
                    <asp:DataPoint AxisLabel="Authorities" YValues="240" />
                    <asp:DataPoint AxisLabel="Found" YValues="100" />
                </Points>
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        <titles><asp:Title Name="Title1" ></asp:Title></titles>
                                                         <Legends>
                <asp:Legend>
                </asp:Legend>
            </Legends>
                                                     </asp:Chart> 

                                                                                                           <asp:Chart ID="Chart3" runat="server" Height="350" Width="350">
            <series>
                <asp:Series Name="Series1" ChartType="Pie" IsVisibleInLegend="true" CustomProperties="PieLabelStyle=Disabled">
                <Points>
                    <asp:DataPoint AxisLabel="Returned" YValues="90" />
                    <asp:DataPoint AxisLabel="Disposed" YValues="120" />
                    <asp:DataPoint AxisLabel="Charity" YValues="300" />
                    <asp:DataPoint AxisLabel="Authorities" YValues="240" />
                    <asp:DataPoint AxisLabel="Found" YValues="100" />
                </Points>
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        <titles><asp:Title Name="Title1" ></asp:Title></titles>
                                                         <Legends>
                <asp:Legend>
                </asp:Legend>
            </Legends>
                                                     </asp:Chart> 
                                                       <br />
 <asp:Button  ID="btnExport" runat="server" Text="Export To PDF" OnClick="btnExport_Click" style="display:none"/>
                                                       <asp:Button  ID="btnExport2" runat="server" Text="Export To PDF" OnClick="btnExport2_Click" style="display:none"/>

                                                         <asp:Button  ID="btnExport3" runat="server" Text="Export To PDF" OnClick="btnExport3_Click" style="display:none"/>
                                                        <asp:Button  ID="btnExport4" runat="server" Text="Export To PDF" OnClick="btnExport4_Click" style="display:none"/>
                                                 </div>   
            <!-- /.panel -->
            <div aria-hidden="true" aria-labelledby="exportDiv" role="dialog" tabindex="-1" id="exportDiv" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-sm"></i></button>
					  <h4 class="modal-title capitalize-text">EXPORT REPORT</h4>
					</div>
					<div class="modal-body">

					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li class="active"><a href="#" class="capitalize-text" data-dismiss="modal" onclick="createPDF()">CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>		


			<div aria-hidden="true" aria-labelledby="newOffenceModal" role="dialog" tabindex="-1" id="newOffenceModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW OFFENCE TYPE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Arabic" id="tbArName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="typeSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>				
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li class="active"><a href="#" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>		
			 
			<div aria-hidden="true" aria-labelledby="newOffenceCategoryModal" role="dialog" tabindex="-1" id="newOffenceCategoryModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW OFFENCE CATEGORY</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbCategoryName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Arabic" id="tbArCategoryName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li class="active"><a href="#" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

		 	<div aria-hidden="true" aria-labelledby="newVehicleMakeModal" role="dialog" tabindex="-1" id="newVehicleMakeModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW VEHICLE MAKE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbVehicleMakeName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Arabic" id="tbArVehicleMakeName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li class="active"><a href="#" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

            <div aria-hidden="true" aria-labelledby="newVehicleColorModal" role="dialog" tabindex="-1" id="newVehicleColorModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW VEHICLE COLOR</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbVehicleColorName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Arabic" id="tbArVehicleColorName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li class="active"><a href="#" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
			         
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            		                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li class="active"><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <select style="color:white;background-color: #5D5D5D;width:125px;height:105px;margin-top:-15px;display:none;" multiple="multiple" id="sendToListBox" ></select>				  
              <asp:HiddenField runat="server" ID="tbUserID" />
              <input style="display:none;" id="imagePath" type="text"/>
              <asp:HiddenField runat="server" ID="tbUserName" />
              <input style="display:none;" id="inputReportType" type="text"/>
                <asp:HiddenField runat="server" ID="toHD" />
                <asp:HiddenField runat="server" ID="lfstatus" />
                 <asp:HiddenField runat="server" ID="lfstatus2" />
                <asp:HiddenField runat="server" ID="contractID" />
                <asp:HiddenField runat="server" ID="projectID" />
                <asp:HiddenField runat="server" ID="statusID" />
                <asp:HiddenField runat="server" ID="typeID" />
                <asp:HiddenField runat="server" ID="comparison1" /> 
                <asp:HiddenField runat="server" ID="yearly1" />
                <asp:HiddenField runat="server" ID="yearly2" />
                <asp:HiddenField runat="server" ID="qt1" />
                <asp:HiddenField runat="server" ID="qt2" />
                <asp:HiddenField runat="server" ID="type1" />
                <asp:HiddenField runat="server" ID="dutyUser" />
                <asp:HiddenField runat="server" ID="dutyStatus" />
                <asp:HiddenField runat="server" ID="fromHD" />
                                <asp:HiddenField runat="server" ID="yearlyDuty" />
                <asp:HiddenField runat="server" ID="monthlyDuty" />
                 

             </section>
</asp:Content>

              
