﻿<%@ Page EnableEventValidation="false" Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="TaskDash.aspx.cs" Inherits="ArrowLabs.Licence.Portal.TaskDash" %>
<%@ Register TagPrefix="TicketingCard" TagName="MyTicketingCard" Src="~/Controls/Modals/TicketingCard.ascx" %> 
<%@ Register TagPrefix="IncidentCard" TagName="MyIncidentCard" Src="~/Controls/Modals/IncidentCard.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    	        <style>
                 .autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}
.autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9; 
}
.autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
                    .line-center{
    margin:0;padding:0 10px;
    background:#FFF;
    display:inline-block;
}
h5{

    text-align:center;
    position:relative;
    z-index:2;
    color:gray;
    margin-left:-20px;
}
h5:after{
    content:"";
    position:absolute;
    top:50%;
    left:0;
    right:0;
    border-top:solid 1px gray;
    z-index:-1;
}  
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name="+name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        var loggedinUsername = '<%=senderName2%>';
        var currentlocation = '<%=currentlocation%>';
        var sourceLat = '<%=sourceLat%>';
        var sourceLon = '<%=sourceLon%>';
        var locationAllowed = false;
        var mapUrl;
        var mapUrlLocation;
        var map;
        var mapLocation;
        var mapTaskLocation;
        var myMarkersTasksLocation = new Array();



        var infoWindowTaskLocation;
        var myMarkers = new Array();
        var myMarkersLocation = new Array();
        var divArray = new Array();

        var infoWindow;
        var infoWindowLocation;
        var rtime = new Date(1, 1, 2000, 12, 00, 00);
        var timeout = false;
        var delta = 200;

        var rotate_factor = 0;
        var rotated = false;
 
        function rowchoiceTicket(name) {
            startRot();
            document.getElementById('rowidChoiceTicket').value = name;
            document.getElementById('addticketNotesTA').style.display = 'none';

            //document.getElementById('backTicketCardLi').style.display = 'block';
            //document.getElementById('saveAsTemplateLi').style.display = 'none';

            //document.getElementById('backTicketCardClose').style.display = 'block';
            //document.getElementById('backTaskClose').style.display = 'none';

            assignrowDataTicket(name);
            getticketRemarks(name);
            getChecklistItemsTicket(name);
            ticketHistoryData(name);

            oldDivContainers();
            incidentOldDivContainers()

            ticketinsertAttachmentIcons(name);
            ticketinsertAttachmentTabData(name);
            ticketinsertAttachmentData(name);
            ticketinfotabDefault();
            ticketdispatchAssignMapTab();
            getTasklistItemsTicket(name);
            hideAllTicketRemarks();
        }
        function updateStatus(status) {
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/UpdateTicketStatus",
                data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','status':'" + status + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert(data.d);
                        //if (status == "START") {
                        //    handledClickTicket();
                        //    document.getElementById("liTicketBack").style.display = "none";
                        //    //document.getElementById("liTicketTask").style.display = "block";
                        //    document.getElementById("liTicketStart").style.display = "none";
                        //    document.getElementById("liTicketEnd").style.display = "block";
                        //}
                        //else if (status == "END") {
                        //    handledClickTicket();
                        //    document.getElementById("liTicketBack").style.display = "none";
                        //    document.getElementById("liTicketTask").style.display = "none";
                        //    document.getElementById("liTicketStart").style.display = "none";
                        //    document.getElementById("liTicketEnd").style.display = "none";
                        //}
                        rowchoiceTicket(document.getElementById('rowidChoiceTicket').value);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function handleTicket() {
            var id = document.getElementById('rowidChoiceTicket').value;
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/handleTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "SUCCESS") {
                        jQuery('#ticketingViewCard').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = "Ticket has successfully been resolved";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d[0]);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getChecklistItemsTicket(id) {
            document.getElementById("offencesItemsList").innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getChecklistDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var ul = document.getElementById("offencesItemsList");
                            var li = document.createElement("li");
                            li.appendChild(document.createTextNode(data.d[i]));
                            ul.appendChild(li);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        function ticketinsertAttachmentIcons(id) {
            jQuery('#ticketdivAttachment div').html('');
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getAttachmentDataIconsTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("ticketdivAttachment").innerHTML = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function ticketinsertAttachmentTabData(id) {
            jQuery('#ticketattachments-info-tab div').html('');

            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getAttachmentDataTabTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("ticketattachments-info-tab").innerHTML = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function ticketinsertAttachmentData(id) {
            document.getElementById('ticketrotationDIV1').style.display = "none";
            document.getElementById('ticketrotationDIV2').style.display = "none";
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getAttachmentDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        imgcount = 0;
 
                        for (var i = 0; i < data.d.length; i++) {

                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.align = 'center';
                            div.style.height = '380px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i) + '-tab';
                            document.getElementById('ticketdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'image-' + (i) + '-tab';
                            imgcount++;
                        }

                        if (imgcount > 0) {
                            document.getElementById('ticketrotationDIV1').style.display = "block";
                            document.getElementById('ticketrotationDIV2').style.display = "block";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getTasklistItemsTicket(id) {
            document.getElementById("tickettaskItemsList").innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTaskListDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("tickettaskItemsList");
                            var li = document.createElement("li");
                            var colorRet = 'green';
                            if (res[3] == "Pending") {
                                colorRet = 'red';
                            }
                            else if (res[3] == "InProgress") {
                                colorRet = 'yellow';
                            }
                            var action = "";

                            if (res[4] == "true")
                                action = 'href="#taskDocument"  data-toggle="modal" data-dismiss="modal"   onclick="showTaskDocument(&apos;' + res[2] + '&apos;);"';

                            li.innerHTML = '<i class="fa fa-circle  ' + colorRet + '-color"></i><a style="margin-left:5px;" class="capitalize-text" '+action+' >' + res[0] + '</a>';

                            ul.appendChild(li);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function ticketdispatchAssignMapTab() {
            var el = document.getElementById('video-0-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
            var el3 = document.getElementById('ticketlocation-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            var el4 = document.getElementById('image-1-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }
            var el5 = document.getElementById('image-2-tab');
            if (el5) {
                el5.className = 'tab-pane fade';
            }
            var el6 = document.getElementById('image-3-tab');
            if (el6) {
                el6.className = 'tab-pane fade';
            }
            var el7 = document.getElementById('image-4-tab');
            if (el7) {
                el7.className = 'tab-pane fade';
            }
            var el8 = document.getElementById('image-5-tab');
            if (el8) {
                el8.className = 'tab-pane fade';
            }
            var el9 = document.getElementById('image-6-tab');
            if (el9) {
                el9.className = 'tab-pane fade';
            }
            var el10 = document.getElementById('image-0-tab');
            if (el10) {
                el10.className = 'tab-pane fade';
            }

        }
        function ticketinfotabDefault() {
            var el3 = document.getElementById('ticketinfo-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            var el4 = document.getElementById('ticketattachments-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }
            var el42 = document.getElementById('ticketactivity-tab');
            if (el42) {
                el42.className = 'tab-pane fade';
            }
            var el43 = document.getElementById('ticketnotes-tab');
            if (el43) {
                el43.className = 'tab-pane fade';
            }
            var el44 = document.getElementById('tickettasks-tab');
            if (el44) {
                el44.className = 'tab-pane fade';
            }
            var el2 = document.getElementById('liTicketInfo');
            if (el2) {
                el2.className = 'active';
            }
            var el6 = document.getElementById('liTicketAtta');
            if (el6) {
                el6.className = ' ';
            }
            var el61 = document.getElementById('liTicketAct');
            if (el61) {
                el61.className = ' ';
            }
            var el62 = document.getElementById('liTicketNotes');
            if (el62) {
                el62.className = ' ';
            }
            var el63 = document.getElementById('liTicketTasks');
            if (el63) {
                el63.className = ' ';
            }
        }
        function addnewtask() {
            jQuery('#ticketingViewCard').modal('hide');
        }
        function unhandledClickTicket() {
            document.getElementById("ticketinitialOptionsDiv").style.display = "block";
            document.getElementById("tickethandleOptionsDiv").style.display = "none";
        }
        function handledClickTicket() {
            document.getElementById("ticketinitialOptionsDiv").style.display = "none";
            document.getElementById("tickethandleOptionsDiv").style.display = "block";
        }
        function assignrowDataTicket(id) {
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTableRowDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        // document.getElementById("platenumberSpan").innerHTML = data.d[0];
                        // document.getElementById("platesourceSpan").innerHTML = data.d[1];
                        //  document.getElementById("plateCodeSpan").innerHTML = data.d[2];
                        //  document.getElementById("vehicleMakeSpan").innerHTML = data.d[3];
                        //  document.getElementById("vehicleColorSpan").innerHTML = data.d[4];
                        document.getElementById("ticketusernameSpan").innerHTML = data.d[5];
                        document.getElementById("ticketlocSpan").innerHTML = data.d[6];
                        document.getElementById("tickettimeSpan").innerHTML = data.d[7];
                        document.getElementById("offcatSpan").innerHTML = data.d[8];
                        document.getElementById("offtypeSpan").innerHTML = data.d[9];
                        document.getElementById("liTicketReport").style.display = "none";
                        document.getElementById("liTicketResolve").style.display = "none";
                        document.getElementById("liTicketOpen").style.display = "none";
                        if (data.d[10] == "0") {
                            unhandledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                            document.getElementById("liTicketOpen").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "block";

                        } else if (data.d[10] == "3") {
                            handledClickTicket(); 
                            document.getElementById("liTicketBack").style.display = "none";

                                document.getElementById("liTicketTask").style.display = "none";

                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "block";
                            document.getElementById("liTicketOpen").style.display = "none";

                        } else if (data.d[10] == "4") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                            document.getElementById("liTicketReport").style.display = "block";
                            document.getElementById("liTicketOpen").style.display = "block";
                            if (data.d[16] == "0") {
                                document.getElementById("liTicketResolve").style.display = "none";
                            }
                            else {
                                document.getElementById("liTicketResolve").style.display = "block";
                            }
                            //liTicketResolve
                        }
                        else if (data.d[10] == "5") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                            document.getElementById("liTicketOpen").style.display = "none";
                            document.getElementById("liTicketResolve").style.display = "none";
                            document.getElementById("liTicketReport").style.display = "block";
                        
                            //
                        }
                        else if (data.d[10] == "6") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                            document.getElementById("liTicketOpen").style.display = "none";
                            document.getElementById("liTicketResolve").style.display = "none";
                            document.getElementById("liTicketReport").style.display = "none"; 
                        }
                        document.getElementById("ticketCommentSpan").innerHTML = data.d[11];
                        document.getElementById('dvticketAccountSpan').style.display = "none";
                        document.getElementById('dvticketContract').style.display = "none";
                        if (data.d[12] != "N/A") {
                            document.getElementById("ticketAccountSpan").innerHTML = data.d[12];
                            document.getElementById('dvticketAccountSpan').style.display = "block";
                        }
                        if (data.d[13] != "N/A") {
                            document.getElementById("ticketContract").innerHTML = data.d[13];
                            document.getElementById('dvticketContract').style.display = "block";
                        }
                        document.getElementById("ticketcontractid").value = data.d[14];
                        document.getElementById("ticketcustomerid").value = data.d[15]; 
                        document.getElementById("ticketstatusSpan").innerHTML = data.d[17];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function saveTicketingRemarks() {
            var projn = document.getElementById("addticketNotesTA").value;
            var id = document.getElementById('rowidChoiceTicket').value;
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('addticketNotesTA').value)) {
                isPass = false;
                showAlert("Kindly provide notes to be added");
            }
            else {
                if (isSpecialChar(document.getElementById('addticketNotesTA').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character from notes");
                }
            }
            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "TaskDash.aspx/addNewTicketingRemarks",
                    data: "{'id':'" + id + "','notes':'" + projn + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            showAlert("Successfully added notes");
                            document.getElementById("addticketNotesTA").value = "";
                            getticketRemarks(id);
                            document.getElementById('addticketNotesTA').style.display = 'none';
                        }
                        else if (data.d == "LOGOUT") {
                            document.getElementById('<%= logoutbtn.ClientID %>').click();
                        }
                        else {
                            showAlert('Failed to save notes. ' + data.d);
                        }
                    }
                });
            }
        }
        function addNotesToTicket() {
            if (document.getElementById('addticketNotesTA').style.display != 'block') {
                document.getElementById('addticketNotesTA').style.display = 'block';
            }
            else {
                saveTicketingRemarks();
            }
        }
        function showAllTicketingRemarks(id) {

            document.getElementById("ticketrotationDIV1").style.display = "none";
            document.getElementById("ticketrotationDIV2").style.display = "none";

            var el2 = document.getElementById('ticketlocation-tab');
            if (el2) {
                el2.className = 'tab-pane fade';
            }
            var el = document.getElementById('ticketremarks-tab');
            if (el) {
                el.className = 'tab-pane fade active in';
            }

            for (var i = 0; i < divArray.length; i++) {
                var el2 = document.getElementById(divArray[i]);
                el2.className = 'tab-pane fade';
            }

            jQuery('#ticketRemarksList2 div').html('');
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTicketRemarksData2",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('ticketRemarksList2').appendChild(div);
                    }
                }
            });

        }
        function hideAllTicketRemarks() {

            document.getElementById("ticketrotationDIV1").style.display = "block";
            document.getElementById("ticketrotationDIV2").style.display = "block";

            var el2 = document.getElementById('ticketlocation-tab');
            if (el2) {
                el2.className = 'tab-pane fade active in';
            }
            var el = document.getElementById('ticketremarks-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
        }
        function getticketRemarks(id) {
            jQuery('#ticketRemarksList div').html('');
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTicketRemarksData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('ticketRemarksList').appendChild(div);
                    }
                }
            });
        }
        function ticketHistoryData(id) {
            jQuery('#divTicketActivity div').html('');
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/geTicketHistoryData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('divTicketActivity').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        } 
        var width = 1;
        function move() {
            var elem = document.getElementById("myBar");
            width = 1;
            var id = setInterval(frame, 100);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        function moveTask() {
            var elem = document.getElementById("myBarTask");
            width = 1;
            var id = setInterval(frame, 100);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        function moveTick() {
            var elem = document.getElementById("myBarTick");
            width = 1;
            var id = setInterval(frame, 100);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        var widthincident = 1;
        function moveincident() {
            var elem = document.getElementById("incidentmyBar");
            width = 1;
            var id = setInterval(frame, 100);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        //DEMO EDI 
        var infoVerWindowLocation;
        var myVerMarkersLocation = new Array();
        var mapVerLocation;
        function getVerLocationMarkers(obj) {
            navigator.geolocation.getCurrentPosition(function (position) {
                sourceLat = position.coords.latitude;
                sourceLon = position.coords.longitude;

                locationAllowed = true;
                google.maps.visualRefresh = true;

                var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 12,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                mapVerLocation = new google.maps.Map(document.getElementById("map_verLocation"), mapOptions);
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div id="content">' + obj[i].Username +
                    '<br/></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    var marker = new google.maps.Marker({ position: myLatlng, map: mapVerLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    myVerMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                    createverInfoWindowLocation(marker, contentString);
                }
            });
        }
        function createverInfoWindowLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoVerWindowLocation.setContent(popupContent);
                infoVerWindowLocation.open(mapVerLocation, this);
            });
        }
        function assignVerifierId(id)
        {
            document.getElementById('verifierID').value = id;
            assignVerifierRequestData();
        }
        function assignVerifierRequestData() {
            jQuery('#taskDocument').modal('hide');
            var id = document.getElementById('verifierID').value;
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getVerifyLocationData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var obj = $.parseJSON(data.d)
                    getVerLocationMarkers(obj);
                }
            });
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getVerifierRequestData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById("verifyUserSpan").innerHTML = data.d[0];
                    document.getElementById("verifyTypeSpan").innerHTML = data.d[1];
                    document.getElementById("verifyCaseSpan").innerHTML = data.d[2];
                    document.getElementById("verifyTimeSpan").innerHTML = data.d[3];
                    document.getElementById("verifyLocSpan").innerHTML = data.d[4];
                    document.getElementById("verifyReasonSpan").innerHTML = data.d[5];
                    document.getElementById("verifyBYearSpan").innerHTML = data.d[6];
                    document.getElementById("verifyEthniSpan").innerHTML = data.d[7];
                    document.getElementById("verifyPIDSpan").innerHTML = data.d[8];
                    document.getElementById("verifyGenderSpan").innerHTML = data.d[9];
                    document.getElementById("verifyListTypeSpan").innerHTML = data.d[10];
                    document.getElementById("veriResultIMG").src = data.d[12];
                    document.getElementById("veriSentIMG").src = data.d[11];
                    if (data.d.length > 14) {
                        document.getElementById("verifyHeaderSpan").innerHTML = data.d[23];
                        document.getElementById("veriResultIMG").src = data.d[13];
                        document.getElementById("veriResult1IMG").src = data.d[13];
                        document.getElementById("veriResult2IMG").src = data.d[15];
                        document.getElementById("veriResult3IMG").src = data.d[17];
                        document.getElementById("veriResult4IMG").src = data.d[19];
                        document.getElementById("veriResult5IMG").src = data.d[21];

                        document.getElementById("resultPercent1").innerHTML = data.d[14] + "%";
                        document.getElementById("resultPercent2").innerHTML = data.d[16] + "%";
                        document.getElementById("resultPercent3").innerHTML = data.d[18] + "%";
                        document.getElementById("resultPercent4").innerHTML = data.d[20] + "%";
                        document.getElementById("resultPercent5").innerHTML = data.d[22] + "%";
                        document.getElementById("resultPercentMain").innerHTML = data.d[14] + "%";
                        document.getElementById('progressbarMainDisplay').setAttribute("style", "width:" + data.d[14] + "%");

                        document.getElementById("verCaseName1").text = data.d[24];
                        document.getElementById("verCaseName2").text = data.d[26];
                        document.getElementById("verCaseName3").text = data.d[28];
                        document.getElementById("verCaseName4").text = data.d[30];
                        document.getElementById("verCaseName5").text = data.d[32];

                        document.getElementById("verifyCaseSpan").innerHTML = data.d[25];
                        document.getElementById("verCaseID1").text = data.d[25];
                        document.getElementById("verCaseID2").text = data.d[27];
                        document.getElementById("verCaseID3").text = data.d[29];
                        document.getElementById("verCaseID4").text = data.d[31];
                        document.getElementById("verCaseID5").text = data.d[33];

                    }
                    else {
                        jQuery('#verificationFail').modal('show');
                        document.getElementById("veriResult1IMG").src = "";
                        document.getElementById("veriResult2IMG").src = "";
                        document.getElementById("veriResult3IMG").src = "";
                        document.getElementById("veriResult4IMG").src = "";
                        document.getElementById("veriResult5IMG").src = "";

                        document.getElementById("resultPercent1").innerHTML = "";
                        document.getElementById("resultPercent2").innerHTML = "";
                        document.getElementById("resultPercent3").innerHTML = "";
                        document.getElementById("resultPercent4").innerHTML = "";
                        document.getElementById("resultPercent5").innerHTML = "";
                        document.getElementById("resultPercentMain").innerHTML = "";
                        document.getElementById('progressbarMainDisplay').setAttribute("style", "width:1%");
                    }
                }
            });
        }

        var firstcharttask = false;
        var firstother = false;
        var months = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        function showTaskingCharts(dash)
        {
            try{
                if(dash == "task")
                {
                    document.getElementById("divChartDemo1").style.display = "block";
                    document.getElementById("divChartDemo2").style.display = "block";
                    document.getElementById("divChartDemo3").style.display = "block";
                    document.getElementById("divChartDemo4").style.display = "block";

                    document.getElementById("taskeventStatusLastHeader").style.display = "block";
                    document.getElementById("bigChartDemo").style.display = "block";

                    document.getElementById("taskssDivContainer").style.display = "block";

                    document.getElementById("divrecentActivity2DIV").style.display = "block";
                    
                    if(!firstcharttask){
                         morrisArea = Morris.Area({
                            element: 'hero-area',
                            fillOpacity: 1,
                            data: [
                              {y: '<%=day1%>', week1: <%=day1week1%>,week2: <%=day1week2%>,week3: <%=day1week3%>,week4: <%=day1week4%>},
          {y: '<%=day2%>', week1: <%=day2week1%>,week2: <%=day2week2%>,week3: <%=day2week3%>,week4: <%=day2week4%>},
          {y: '<%=day3%>', week1: <%=day3week1%>,week2: <%=day3week2%>,week3: <%=day3week3%>,week4: <%=day3week4%>},
          {y: '<%=day4%>', week1: <%=day4week1%>,week2: <%=day4week2%>,week3: <%=day4week3%>,week4: <%=day4week4%>},
          {y: '<%=day5%>', week1: <%=day5week1%>,week2: <%=day5week2%>,week3: <%=day5week3%>,week4: <%=day5week4%>},
          {y: '<%=day6%>', week1: <%=day6week1%>,week2: <%=day6week2%>,week3: <%=day6week3%>,week4: <%=day6week4%>},
          {y: '<%=day7%>', week1: <%=day7week1%>,week2: <%=day7week2%>,week3: <%=day7week3%>,week4: <%=day7week4%>},
                            ],
                            xkey: 'y',
                            ykeys: ['week1','week2','week3','week4'],
                            labels: ['Week 1','Week 2','Week 3','Week 4'], 
                            pointSize: 0,
                            hideHover: true,
                            lineWidth: '3px',
                            lineColors: ['#1b93c0','#3ebb64','#f2c400','#f44e4b'],
                            gridTextColor: 'rgba(22, 24, 27, 0.87)',
                            gridLineColor: 'rgba(22, 24, 27, 0.26)',
                            responsive:false,
                            resize: false,
                            xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
                                var month = months[x.getDay()];
                                return month;
                            }
                        });
			
                        var morrisBar = Morris.Bar({
                            element: 'hero-bar',
                            data: [
                              {device: '<%=taskUser1%>', avg: <%=taskUser1Avg%>},
                              {device: '<%=taskUser2%>', avg: <%=taskUser2Avg%>},
                              {device: '<%=taskUser3%>', avg: <%=taskUser3Avg%>},
                              {device: '<%=taskUser4%>', avg: <%=taskUser4Avg%>},
                              {device: '<%=taskUser5%>', avg: <%=taskUser5Avg%>},
                            ],
                            xkey: 'device',
                            ykeys: ['avg'],
                            labels: ['Avg'],
                            barRatio: 0.4,
                            xLabelAngle: 75,
                            hideHover: 'auto',
                            barColors: ['#5D9CEC'],
                            gridTextColor: 'rgba(22, 24, 27, 0.87)',
                            gridLineColor: 'rgba(22, 24, 27, 0.26)',
                            responsive:false,
                            resize: false // NOTE: This has a significant performance impact, so is disabled by default.
                        });
			
                         lineCtx2 = document.getElementById('chartjs-lineDemo').getContext('2d'),
                    lineData = {
                        labels: ['WEEK1', 'WEEK2', 'WEEK3', 'WEEK4'],
                        datasets: [
                          {
                              label: '<%=SkillSet1Name%>',
                              backgroundColor: 'transparent',
                              borderColor: '#1b93c0',
                              pointColor: '#1b93c0',
                              pointStrokeColor: '#ffffff',
                              pointHighlightFill: '#ffffff',
                              pointHighlightStroke: '#1b93c0',
                              data: [<%=SkillSet1Week1Count%>, <%=SkillSet1Week2Count%>, <%=SkillSet1Week3Count%>, <%=SkillSet1Week4Count%>]
                          },
                          {
                              label: '<%=SkillSet2Name%>',
                              backgroundColor: 'transparent',
                              borderColor: '#3ebb64',
                              pointColor: '#3ebb64',
                              pointStrokeColor: '#ffffff',
                              pointHighlightFill: '#ffffff',
                              pointHighlightStroke: '#3ebb64',
                              data: [<%=SkillSet2Week1Count%>, <%=SkillSet2Week2Count%>, <%=SkillSet2Week3Count%>, <%=SkillSet2Week4Count%>]
                          },
                          {
                              label: '<%=SkillSet3Name%>',
                              backgroundColor: 'transparent',
                              borderColor: '#f2c400',
                              pointColor: '#f2c400',
                              pointStrokeColor: '#ffffff',
                              pointHighlightFill: '#ffffff',
                              pointHighlightStroke: '#f2c400',
                              data: [<%=SkillSet3Week1Count%>, <%=SkillSet3Week2Count%>, <%=SkillSet3Week3Count%>, <%=SkillSet3Week4Count%>]
                          },
                          {
                              label: '<%=SkillSet4Name%>',
                              backgroundColor: 'transparent',
                              borderColor: '#f44e4b',
                              pointColor: '#f44e4b',
                              pointStrokeColor: '#ffffff',
                              pointHighlightFill: '#ffffff',
                              pointHighlightStroke: '#f44e4b',
                              data: [<%=SkillSet4Week1Count%>, <%=SkillSet4Week2Count%>, <%=SkillSet4Week3Count%>, <%=SkillSet4Week4Count%>]
                          },
                          {
                              label: '<%=SkillSet5Name%>',
                              backgroundColor: 'transparent',
                              borderColor: '#b2163b',
                              pointColor: '#b2163b',
                              pointStrokeColor: '#ffffff',
                              pointHighlightFill: '#ffffff',
                              pointHighlightStroke: '#b2163b',
                              data: [<%=SkillSet5Week1Count%>, <%=SkillSet5Week2Count%>, <%=SkillSet5Week3Count%>, <%=SkillSet5Week4Count%>]
                          }
                        ]
                    },
                                chartjsLine = new Chart(lineCtx2, {
                                    type: 'line',
                                    data: lineData,
                                    options: {
                                        legend: {
                                            position:'bottom',
                                            labels: {
                                                fontSize: 9,
                                                boxWidth:20
                                            }
                                        },
                                        scaleBeginAtZero: true,
                                        scaleShowVerticalLines: false,
                                        responsive: false
                                    } 
                                });
                      firstcharttask = true;
                  }
              }
          }
          catch(er)
          {
              showAlert(er);
          }
      }
      function addrowtoTableTeamTasks() {
          taskfilter = true;
          $("#teamtasksTable tbody").empty();
          jQuery("#teamtasksTable").dataTable().fnClearTable();
          jQuery("#teamtasksTable").dataTable().fnDraw();
          jQuery("#teamtasksTable").dataTable().fnDestroy();
          $.ajax({
              type: "POST",
              url: "TaskDash.aspx/getTableDataTeamTasks",
              data: "{'id':'0','uname':'" + loggedinUsername + "'}",
              async: false,
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  if(data.d[0] == "LOGOUT"){
                      showError("Session has expired. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }else{
                      for (var i = 0; i < data.d.length; i++) {
                          $("#teamtasksTable tbody").append(data.d[i]);
                      }
                      jQuery("#teamtasksTable").DataTable({
                          "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                          'iDisplayLength': 10,
                          "order": [[3, "desc"]]
                      });
                  }
              },
              error: function () {
                  showError("Session timeout. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
              }
          });
      }
      var taskfilter = false;
      function pickdateTeamTask() {
 
          try {
              //var filterstring = document.getElementById('teamtaskDatepicker').value.split('/');
              //var prefix = filterstring[0];
              //var fix = filterstring[1];
              //if (prefix.charAt(0) === '0') {
              //    prefix = prefix.substr(1);
              //}
              //if (fix.charAt(0) === '0') {
              //    fix = fix.substr(1);
              //}
              //document.getElementById('teamtaskSearchBox').value = prefix + "/" + fix + "/" + filterstring[2];
              //jQuery(jQuery(document.getElementById('teamtaskSearchBox')).parents(".panel-datatable").find(".form-control.input-sm")[0]).val(jQuery(document.getElementById('teamtaskSearchBox')).val()).keyup();
              jQuery("#teamtasksTable tbody").empty();
              jQuery("#teamtasksTable").dataTable().fnClearTable();
              jQuery("#teamtasksTable").dataTable().fnDraw();
              jQuery("#teamtasksTable").dataTable().fnDestroy();
              $.ajax({
                  type: "POST",
                  url: "TaskDash.aspx/getTableDataTeamTasksByDate",
                  data: "{'date':'" + document.getElementById('teamtaskDatepicker').value + "','uname':'" + loggedinUsername + "'}",
                  async: false,
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function (data) {
                      if(data.d[0] == "LOGOUT"){
                          showError("Session has expired. Kindly login again.");
                          setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                      }else{
                          for (var i = 0; i < data.d.length; i++) {
                              $("#teamtasksTable tbody").append(data.d[i]);
                          }
                          jQuery("#teamtasksTable").DataTable({
                              "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                              'iDisplayLength': 10,
                              "order": [[3, "desc"]]
                          });
                      }
                  },
                  error: function () {
                      showError("Session timeout. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                  }
              });
              
          } catch (err) {
              alert(err);
          }
       
      }
      //User-Profile
      function changePassword() {
          try
          {
              var newPw = document.getElementById("newPwInput").value;
              var confPw = document.getElementById("confirmPwInput").value;
              var isErr = false;
              if (!isErr) { 
                  if (!letterGood) {
                      showAlert('Password does not contain letter');
                      isErr = true;
                  }
                  if (!isErr) {
                      if (!capitalGood) {
                          showAlert('Password does not contain capital letter');
                          isErr = true;
                      }
                  }
                  if (!isErr) {
                      if (!numGood) {
                          showAlert('Password does not contain number');
                          isErr = true;
                      }
                  }
                  if (!isErr) {
                      if (!lengthGood) {
                          showAlert('Password length not enough');
                          isErr = true;
                      }
                  }
              }
              if (!isErr) { 
                  if (newPw == confPw && newPw != "" && confPw != "") {
                      $.ajax({
                          type: "POST",
                          url: "TaskDash.aspx/changePW",
                          data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                          async: false,
                          dataType: "json",
                          contentType: "application/json; charset=utf-8",
                          success: function (data) { 
 
                                  jQuery('#changePasswordModal').modal('hide');
                                  document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                  jQuery('#successfulDispatch').modal('show');
                                  document.getElementById("newPwInput").value = "";
                                  document.getElementById("confirmPwInput").value = "";
                                  document.getElementById("oldPwInput").value = confPw;
                              
                          },
                          error: function () {
                              showError("Session timeout. Kindly login again.");
                              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                          }
                      });
                  }
                  else {
                      showAlert("Kindly match new password with confirm password.")
                  }
              }
          }
          catch(ex)
          {showAlert(ex)}
      }
      function editUnlock() {
          document.getElementById("profilePhoneNumberDIV").style.display = "none";
          document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
          document.getElementById("editProfileA").style.display = "none";
          document.getElementById("saveProfileA").style.display = "block";
          document.getElementById("profileEmailAddDIV").style.display = "none";
          document.getElementById("profileEmailAddEditDIV").style.display = "block";
          document.getElementById("userFullnameSpanDIV").style.display = "none";
          document.getElementById("userFullnameSpanEditDIV").style.display = "block";
          if (document.getElementById('profileRoleName').innerHTML == "User")
          {
              document.getElementById("superviserInfoDIV").style.display = "none";
              document.getElementById("managerInfoDIV").style.display = "block";
              document.getElementById("dirInfoDIV").style.display = "none";
            
            
          }
          else if (document.getElementById('profileRoleName').innerHTML == "Manager")
          {
              document.getElementById("superviserInfoDIV").style.display = "none";
              document.getElementById("managerInfoDIV").style.display = "none";
              document.getElementById("dirInfoDIV").style.display = "block";
          }
      }
      function editJustLock() {
          document.getElementById("profilePhoneNumberDIV").style.display = "block";
          document.getElementById("userFullnameSpanEditDIV").style.display = "none";
          document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
          document.getElementById("editProfileA").style.display = "block";
          document.getElementById("saveProfileA").style.display = "none";
          document.getElementById("profileEmailAddDIV").style.display = "block";
          document.getElementById("profileEmailAddEditDIV").style.display = "none";
          document.getElementById("userFullnameSpanDIV").style.display = "block";
          document.getElementById("superviserInfoDIV").style.display = "block";
          document.getElementById("managerInfoDIV").style.display = "none";
          document.getElementById("dirInfoDIV").style.display = "none";
      }
      function editLock() {
          document.getElementById("profilePhoneNumberDIV").style.display = "block";
          document.getElementById("userFullnameSpanEditDIV").style.display = "none";
          document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
          document.getElementById("editProfileA").style.display = "block";
          document.getElementById("saveProfileA").style.display = "none";
          document.getElementById("profileEmailAddDIV").style.display = "block";
          document.getElementById("profileEmailAddEditDIV").style.display = "none";
          document.getElementById("userFullnameSpanDIV").style.display = "block";
          document.getElementById("superviserInfoDIV").style.display = "block";
          document.getElementById("managerInfoDIV").style.display = "none";
          document.getElementById("dirInfoDIV").style.display = "none";
          var uID = document.getElementById('rowidChoice').text;
          var role = document.getElementById('UserRoleSelector').value;
          var roleid = 0;
          var supervisor = 0;
          if (role == "User") {
              supervisor = document.getElementById('MainContent_editmanagerpickerSelect').value;
              if (supervisor > 0) {
                  roleid = 3;
              }
              else {
                  roleid = 5;
              }
          }
          else if (role == "Manager") {
              supervisor = document.getElementById('MainContent_editdirpickerSelect').value;
              roleid = 2;
          }
          else if (role == "Director") {
              roleid = 1;
          }

          var retVal = saveUserProfile(uID, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
          if (retVal > 0) {
              assignUserProfileData(retVal);  
              showAlert('Successfully updated user info.');
          }
          else {
              showAlert('Failed user edit.');
          }

      }
      function getGroupnsOfUser(id) {
          document.getElementById("userGroupList").innerHTML = "";
          $.ajax({
              type: "POST",
              url: "TaskDash.aspx/getGroupDataFromUser",
              data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
              async: false,
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  if(data.d[0] == "LOGOUT"){
                      showError("Session has expired. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }
                  else{
                      for (var i = 0; i < data.d.length; i++) {
                          var div = document.createElement('div');

                          div.className = 'help-block round-border padding-2x mb-2x relative drop-element';

                          div.innerHTML = data.d[i];

                          div.id = data.d[i + 1];

                          document.getElementById('userGroupList').appendChild(div);
                          i++;
                      }
                  }
              },
              error: function () {
                  showError("Session timeout. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
              }
          });
      }
      function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role,img) {
          var output = 0;
          $.ajax({
              type: "POST",
              url: "TaskDash.aspx/addUserProfile",
              data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
              async: false,
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  output = data.d;
              }
          });
          return output;
      }
      function saveTZ() {
          var scountr = $("#countrySelect option:selected").text();
          jQuery.ajax({
              type: "POST",
              url: "TaskDash.aspx/saveTZ",
              data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
              async: false,
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  if (data.d[0] == "LOGOUT") {
                      showError("Session has expired. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  } else {
                      document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                      jQuery('#successfulDispatch').modal('show');
                  }
              },
              error: function () {
                  showError("Session timeout. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              }
          });
      }
      function getserverInfo() {
          jQuery.ajax({
              type: "POST",
              url: "TaskDash.aspx/getServerData",
              data: "{'id':'0','uname':'" + loggedinUsername + "'}",
              async: false,
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {

                  if(data.d == "LOGOUT"){
                      showError("Session has expired. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }else{

                      document.getElementById('mobileRemaining').value = data.d[9];
                      document.getElementById('mobileTotal').value = data.d[10];
                      document.getElementById('mobileUsed').value = data.d[11];
                      document.getElementById("countrySelect").value = data.d[28];
                      jQuery('#countrySelect').selectpicker('val', data.d[28]);

                      document.getElementById('surveillanceCheck').checked = false;
                      document.getElementById('notificationCheck').checked = false;
                      document.getElementById('locationCheck').checked = false;
                      document.getElementById('ticketingCheck').checked = false;
                      document.getElementById('taskCheck').checked = false;
                      document.getElementById('incidentCheck').checked = false;
                      document.getElementById('warehouseCheck').checked = false;
                      document.getElementById('chatCheck').checked = false;
                      document.getElementById('collaborationCheck').checked = false;
                      document.getElementById('lfCheck').checked = false;
                      document.getElementById('dutyrosterCheck').checked = false;
                      document.getElementById('postorderCheck').checked = false;
                      document.getElementById('verificationCheck').checked = false;
                      document.getElementById('requestCheck').checked = false;
                      document.getElementById('dispatchCheck').checked = false;
                      document.getElementById('activityCheck').checked = false;

                      if (data.d[12] == "true")
                          document.getElementById('surveillanceCheck').checked = true;
                      if (data.d[13] == "true")
                          document.getElementById('notificationCheck').checked = true;
                      if (data.d[14] == "true")
                          document.getElementById('locationCheck').checked = true;
                      if (data.d[15] == "true")
                          document.getElementById('ticketingCheck').checked = true;
                      if (data.d[16] == "true")
                          document.getElementById('taskCheck').checked = true;
                      if (data.d[17] == "true")
                          document.getElementById('incidentCheck').checked = true;
                      if (data.d[18] == "true")
                          document.getElementById('warehouseCheck').checked = true;
                      if (data.d[19] == "true")
                          document.getElementById('chatCheck').checked = true;
                      if (data.d[20] == "true")
                          document.getElementById('collaborationCheck').checked = true;
                      if (data.d[21] == "true")
                          document.getElementById('lfCheck').checked = true;
                      if (data.d[22] == "true")
                          document.getElementById('dutyrosterCheck').checked = true;
                      if (data.d[23] == "true")
                          document.getElementById('postorderCheck').checked = true;
                      if (data.d[24] == "true")
                          document.getElementById('verificationCheck').checked = true;
                      if (data.d[25] == "true")
                          document.getElementById('requestCheck').checked = true;
                      if (data.d[26] == "true")
                          document.getElementById('dispatchCheck').checked = true;
                      if (data.d[27] == "true")
                          document.getElementById('activityCheck').checked = true;
                  }
              },
              error: function () {
                  showError("Session timeout. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
              }
          });
      }
        var firstuserMap = false;
        function assignManagerUserProfileData(id) {
            try
            {
                getGroupnsOfUser(id)
                $.ajax({
                    type: "POST",
                    url: "TaskDash.aspx/getUserProfileData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            try{
                                document.getElementById('containerDiv').style.display="none";
                                document.getElementById('containerDiv2').style.display="none";


                                document.getElementById('changePWDIV').style.display = "block";
                                document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                document.getElementById('profileRoleName').innerHTML = data.d[5];
                                document.getElementById('profileManagerName').innerHTML = data.d[6];
                                document.getElementById('userStatusSpan').innerHTML = data.d[8];

                                if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                    document.getElementById('containerDiv2').style.display = "block";
                                    document.getElementById('defaultGenderDiv').style.display = "none";
                                    getserverInfo();
                                }

                                var el = document.getElementById('userStatusIconSpan');
                                if (el) {
                                    el.className = data.d[9];
                                }
                                document.getElementById('userprofileImgSrc').src = data.d[10];
                                document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                document.getElementById('userFirstnameSpan').value = data.d[13];
                                document.getElementById('userLastnameSpan').value = data.d[14];
                                document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                document.getElementById('profileEmailAddEdit').value = data.d[3];

                                document.getElementById('oldPwInput').value = data.d[16];
                                userrecentActivity(id);
                                if (!firstuserMap) {
                                    //getUserLocation(data.d[18], data.d[17], data.d[0]);
                                    locationAllowed = true;
                                    setTimeout(function () {
                                        google.maps.visualRefresh = true;
                                        var Liverpool = new google.maps.LatLng(data.d[17], data.d[18]);

                                        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                                        var mapOptions = {
                                            zoom: 8,
                                            center: Liverpool,
                                            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                                        };

                                        // This makes the div with id "map_canvas" a google map
                                        usermap = new google.maps.Map(document.getElementById("usermap_canvas"), mapOptions);
                                        usermarker = new google.maps.Marker({ position: Liverpool, map: usermap, title: data.d[0] });
                                        usermarker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                                    }, 1000);
                                    firstuserMap = true;
                                }
                                else
                                    updateUserLocation(data.d[18], data.d[17], data.d[0]);

                            
                                document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                document.getElementById('profileGender').innerHTML = data.d[20];
                                 
                                    

                                

                                if (document.getElementById('profileRoleName').innerHTML != "Level 7") {
                                    document.getElementById('deviceTypesDiv').innerHTML = "<i class='fa fa-mobile fa-2x mr-2x'></i><i style='color:lime;' class='fa fa-laptop fa-2x mr-2x'></i>";//data.d[11];

                                }
                            }
                            catch(err){}
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            catch(ex)
            {
                showAlert("Error 62: Problem occured while trying to get user profile. - " + ex)
            }
        }
        function assignUserProfileData(id) {
            try
            {
                getGroupnsOfUser(id)
                $.ajax({
                    type: "POST",
                    url: "TaskDash.aspx/getUserProfileData",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if(data.d[0] == "LOGOUT"){
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }else{
                            try{
                                document.getElementById('containerDiv').style.display="block";
                                document.getElementById('containerDiv2').style.display="none";
                                document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                document.getElementById('profileRoleName').innerHTML = data.d[5];
                                document.getElementById('profileManagerName').innerHTML = data.d[6];
                                document.getElementById('userStatusSpan').innerHTML = data.d[8];
                                var el = document.getElementById('userStatusIconSpan');
                                if (el) {
                                    el.className = data.d[9];
                                }

                                document.getElementById('changePWDIV').style.display = "none";

                                document.getElementById('userprofileImgSrc').src = data.d[10];
                                document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                document.getElementById('userFirstnameSpan').value = data.d[13];
                                document.getElementById('userLastnameSpan').value = data.d[14];
                                document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                document.getElementById('profileEmailAddEdit').value = data.d[3];

                                document.getElementById('oldPwInput').value = data.d[16];
                                userrecentActivity(id);
                                if (!firstuserMap) {
                                    //getUserLocation(data.d[18], data.d[17], data.d[0]);
                                    locationAllowed = true;
                                    setTimeout(function () {
                                        google.maps.visualRefresh = true;
                                        var Liverpool = new google.maps.LatLng(data.d[17], data.d[18]);

                                        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                                        var mapOptions = {
                                            zoom: 8,
                                            center: Liverpool,
                                            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                                        };

                                        // This makes the div with id "map_canvas" a google map
                                        usermap = new google.maps.Map(document.getElementById("usermap_canvas"), mapOptions);
                                        usermarker = new google.maps.Marker({ position: Liverpool, map: usermap, title: data.d[0] });
                                        usermarker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                                    }, 1000);
                                    firstuserMap = true;
                                }
                                else
                                    updateUserLocation(data.d[18], data.d[17], data.d[0]);

                            
                                document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                document.getElementById('profileGender').innerHTML = data.d[20];
                            }
                            catch(err){}
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                    }
                });
            }
            catch(ex)
            {
                showAlert("Error 62: Problem occured while trying to get user profile. - " + ex)
            }
        }
      function userrecentActivity(id) {
          document.getElementById('divrecentUserActivity').innerHTML = "";
          jQuery.ajax({
              type: "POST",
              url: "TaskDash.aspx/getUserRecentActivity",
              data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
              async: false,
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  if(data.d[0] == "LOGOUT"){
                      showError("Session has expired. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }else{
                      for (var i = 0; i < data.d.length; i++) {
                          var div = document.createElement('div');

                          div.className = 'row';

                          div.innerHTML = data.d[i];

                          document.getElementById('divrecentUserActivity').appendChild(div);
                      }
                  }
              },
              error: function () {
                  showError("Session timeout. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
              }
          });
      }
      var usermap;
      var usermarker;
      function getUserLocation(longi,lati,uname) {
          //navigator.geolocation.getCurrentPosition(function (position) {
          //    sourceLat = position.coords.latitude;
          //    sourceLon = position.coords.longitude;

          locationAllowed = true;

          google.maps.visualRefresh = true;
          var Liverpool = new google.maps.LatLng(lati, longi);

          // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
          var mapOptions = {
              zoom: 15,
              center: Liverpool,
              mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
          };

          // This makes the div with id "map_canvas" a google map
          usermap = new google.maps.Map(document.getElementById("usermap_canvas"), mapOptions);
          usermarker = new google.maps.Marker({ position: Liverpool, map: usermap, title: uname });
          usermarker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
          // });
      }
      function updateUserLocation(longi, lati, uname) {
          usermarker.setMap(null);
          var Liverpool = new google.maps.LatLng(lati, longi);
          usermarker = new google.maps.Marker({ position: Liverpool, map: usermap, title: uname });
          usermarker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
      }
      function forceLogout()
      {
          document.getElementById('<%= closingbtn.ClientID %>').click();
      }
      //Reminders
      function newReminderInsert()
      {
          var topic  = document.getElementById("tbReminderName").value;
          var reminder  = document.getElementById("tbReminderNotes").value;
          var remdate  = document.getElementById("tbReminderDate").value;
          var todate = document.getElementById("toReminderDate").value;    
          var fromdate = document.getElementById("fromReminderDate2").value;
          var colorcode = 0;
          if (document.getElementById('cbRemBlue').checked) {
              colorcode = 1;
          }
          else if (document.getElementById('cbRemGreen').checked) {
              colorcode = 2;
          }
          else if (document.getElementById('cbRemYellow').checked) {
              colorcode = 3;
          }
          else if (document.getElementById('cbRemRed').checked) {
              colorcode = 0;
          }
          if(!isEmptyOrSpaces(topic))
          {
              if(!isSpecialChar(topic)){
                  var validReminder = true;
                  if(isSpecialChar(reminder))
                  {
                      validReminder = false;
                  }
                  if(validReminder){
                      jQuery.ajax({
                          type: "POST",
                          url: "TaskDash.aspx/insertNewReminder",
                          data: "{'name':'" + topic + "','date':'" + remdate + "','reminder':'" + reminder+ "','colorcode':'" + colorcode + "','todate':'" + todate + "','fromdate':'" + fromdate + "','uname':'" + loggedinUsername + "'}",
                          async: false,
                          dataType: "json",
                          contentType: "application/json; charset=utf-8",
                          success: function (data) {
                              if(data.d == "SUCCESS")
                              {
                                  jQuery('#newReminder').modal('hide');
                                  document.getElementById('successincidentScenario').innerHTML = "Reminder created.";
                                  jQuery('#successfulDispatch').modal('show');
                              }
                              else if(data.d == "LOGOUT"){
                                  document.getElementById('<%= logoutbtn.ClientID %>').click();
                              }
                              else
                              {
                                  showAlert("Error 63: Problem occured while trying to insert reminder. - " + data.d );
                              }
                          }
                      });
              }
          }
      }
      else
      {
          showAlert("Kindly provide reminder topic.");
      }
  }
        //Dashboard Map
        function clearUsersFromDashMap(markers)
        {
            for (var key in markers) {
                var obj = markers[key];
                obj.setMap(null);
                obj=null;
                // ...s
            }
            myMarkers = new Array();
            //jQuery.ajax({
            //    type: "POST",
            //    url: "TaskDash.aspx/getGPSDataUsers",
            //    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            //    dataType: "json",
            //    contentType: "application/json; charset=utf-8",
            //    success: function (data) {
            //        if (data.d == "]") {
            //            //getLocationNoOnline();
            //        }
            //        else {
            //            var obj = jQuery.parseJSON(data.d)
            //            setMapOnAll(obj,markers);
            //        }
            //    }
            //});
        }
        function clearTasksFromDashMap(markers)
        {
            for (var key in markers) {
                var obj = markers[key];
                obj.setMap(null);
                obj=null;
                // ...s
            }
            myMarkers = new Array();
            //jQuery.ajax({
            //    type: "POST",
            //    url: "TaskDash.aspx/getGPSDataTasks",
            //    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            //    dataType: "json",
            //    contentType: "application/json; charset=utf-8",
            //    success: function (data) {
            //        if (data.d == "]") {
            //            //getLocationNoOnline();
            //        }
            //        else {
            //            var obj = jQuery.parseJSON(data.d)
            //            setMapOnAll(obj,markers);
            //        }
            //    }
            //});
        }
         
  function clearIncidentsFromDashMap(markers)
  {
      jQuery.ajax({
          type: "POST",
          url: "TaskDash.aspx/getGPSData",
          data: "{'id':'0','uname':'" + loggedinUsername + "'}",
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              if(data.d == "LOGOUT"){
                  showError("Session has expired. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              }
              else if (data.d == "]") {
                  //getLocationNoOnline();
              }
              else {
                  var obj = jQuery.parseJSON(data.d)
                  setMapOnAll(obj,myMarkers);
              }
          },
          error: function () {
              showError("Session timeout. Kindly login again.");
              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
          }
      });
  }
  function clearOthersFromDashMap(markers)
  {
      jQuery.ajax({
          type: "POST",
          url: "TaskDash.aspx/getGPSDataOthers",
          data: "{'id':'0','uname':'" + loggedinUsername + "'}",
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              if(data.d == "LOGOUT"){
                  showError("Session has expired. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              }
              else if (data.d == "]") {
                  //getLocationNoOnline();
              }
              else {
                  var obj = jQuery.parseJSON(data.d)
                  setMapOnAll(obj,markers);
              }
          },
          error: function () {
              showError("Session timeout. Kindly login again.");
              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
          }
      });
  }

  var polylinesArray = new Array();
  Object.size = function (obj) {
      var size = 0, key;
      for (key in obj) {
          if (obj.hasOwnProperty(key)) size++;
      }
      return size;
  };
  function updateGPSMapMarker(obj) {
      try {
          var poligonCoords = [];
          var subpoligonCoords = [];
          var first = true;
          var secondfirst = true;
          var previousColor = "";
          var currentSubLocation;
          var polyCounter = 0;

          for (var i = 0; i < Object.size(polylinesArray) ; i++) {
              if (polylinesArray[i] != null) {
                  polylinesArray[i].setMap(null);
              }
          }

          countries = [];

          for (var i = 0; i < obj.length; i++) {
              var myOption;
              if(obj[i].Logs == "User")
              {
                  if (obj[i].State == "BLUE" || obj[i].State == "OFFCLIENT" || obj[i].State == "10" || obj[i].State == "11" || obj[i].State == "12" || obj[i].State == "13" || obj[i].State == "14" || obj[i].State == "15")
                  {
                      if(obj[i].Cams == "NO CAM")
                      {
                          var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-user pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"></div>';
                      }
                      else{
                          var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-user pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#cameraDocument"  data-toggle="modal" onclick="camStart(&apos;' + obj[i].Cams + '&apos;,&apos;' + obj[i].Username + '&apos;)" ><i class="fa fa-eye red-color"></i>CAMERAS</a></div>';
                      }
                  }
                  else
                  {
                      var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-user pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Uname + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#user-profile-tab"  data-toggle="tab" onclick="assignUserProfileData(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                  }
              }
              else if (obj[i].Logs == "Task")
              {
                  var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#taskDocument"  data-toggle="modal" onclick="showTaskDocument(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
              }
              else if (obj[i].Logs == "Verify")
              {
                  var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#verificationDocument"  data-toggle="modal" onclick="assignVerifierId(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
              }
              else
              {
                  var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#viewDocument1"  data-toggle="modal" onclick="rowchoice(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
              }
              var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
              if (obj[i].State == "YELLOW") {
                  var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                  marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                  myMarkers[obj[i].Id] = marker;
                  createInfoWindow(marker, contentString);

                  myOption = document.createElement("Option");
                  myOption.text = obj[i].DName; //Textbox's value
                  myOption.value = obj[i].Lat + "-" + obj[i].Long;
                  usermapListBox.add(myOption);
                  countries.push(obj[i].DName);
              }
              else if (obj[i].State == "GREEN") {
                  if(obj[i].Logs == "User"){
                      var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Uname + "\n" + obj[i].LastLog });
                      marker.setIcon('https://testportalcdn.azureedge.net/Images/useronline.png')
                      myMarkers[obj[i].Id] = marker;
                      createInfoWindow(marker, contentString);

                      myOption = document.createElement("Option");
                      myOption.text = obj[i].DName; //Textbox's value
                      myOption.value = obj[i].Lat + "-" + obj[i].Long;
                      usermapListBox.add(myOption);
                      countries.push(obj[i].DName);
                  }
                  else{
                      var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                      marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                      myMarkers[obj[i].Id] = marker;
                      createInfoWindow(marker, contentString);

                      myOption = document.createElement("Option");
                      myOption.text = obj[i].DName; //Textbox's value
                      myOption.value = obj[i].Lat + "-" + obj[i].Long;
                      usermapListBox.add(myOption);
                      countries.push(obj[i].DName);
                  }
              }
              else if (obj[i].State == "BLUE") {
                  var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                  marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                  myMarkers[obj[i].Id] = marker;
                  createInfoWindow(marker, contentString);

                  myOption = document.createElement("Option");
                  myOption.text = obj[i].DName; //Textbox's value
                  myOption.value = obj[i].Lat + "-" + obj[i].Long;
                  usermapListBox.add(myOption);
                  countries.push(obj[i].DName);
              }
              else if (obj[i].State == "OFFUSER") {
                  if(obj[i].Logs == "User"){
                      var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Uname + "\n" + obj[i].LastLog });
                      marker.setIcon('https://testportalcdn.azureedge.net/Images/useroffline.png')
                      myMarkers[obj[i].Id] = marker;
                      createInfoWindow(marker, contentString);

                      myOption = document.createElement("Option");
                      myOption.text = obj[i].DName; //Textbox's value
                      myOption.value = obj[i].Lat + "-" + obj[i].Long;
                      usermapListBox.add(myOption);
                      countries.push(obj[i].DName);
                  }
                  else{
                      var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                      marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                      myMarkers[obj[i].Id] = marker;
                      createInfoWindow(marker, contentString);

                      myOption = document.createElement("Option");
                      myOption.text = obj[i].DName; //Textbox's value
                      myOption.value = obj[i].Lat + "-" + obj[i].Long;
                      usermapListBox.add(myOption);
                      countries.push(obj[i].DName);
                  }
              }
              else if (obj[i].State == "OFFCLIENT") {
                  var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                  marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                  myMarkers[obj[i].Id] = marker;
                  createInfoWindow(marker, contentString);

                  myOption = document.createElement("Option");
                  myOption.text = obj[i].DName; //Textbox's value
                  myOption.value = obj[i].Lat + "-" + obj[i].Long;
                  usermapListBox.add(myOption);
                  countries.push(obj[i].DName);
              }
              else if (obj[i].State == "PURPLE") {
                  var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                  marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                  myMarkers[obj[i].Id] = marker;
                  createInfoWindow(marker, contentString);

                  myOption = document.createElement("Option");
                  myOption.text = obj[i].DName; //Textbox's value
                  myOption.value = obj[i].Lat + "-" + obj[i].Long;
                  usermapListBox.add(myOption);
                  countries.push(obj[i].DName);
              }
              else {
                  if (secondfirst) {
                      currentSubLocation = obj[i].State;
                      var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                      if (obj[i].Logs == '#17ff33') {
                          marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');

                          myOption = document.createElement("Option");
                          myOption.text = obj[i].Username; //Textbox's value
                          myOption.value = obj[i].Lat + "-" + obj[i].Long;
                          usermapListBox.add(myOption);
                          countries.push(obj[i].Username);
                      }
                      else {
                          marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');

                          myOption = document.createElement("Option");
                          myOption.text = obj[i].Username; //Textbox's value
                          myOption.value = obj[i].Lat + "-" + obj[i].Long;
                          usermapListBox.add(myOption);
                          countries.push(obj[i].Username);
                      }
                      previousColor = obj[i].Logs;
                      myMarkers[obj[i].Id] = marker;
                      createInfoWindow(marker, contentString);
                      secondfirst = false;
                      var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                      subpoligonCoords.push(point);
                  }
                  else {
                      if (currentSubLocation == obj[i].State) {
                          var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                          subpoligonCoords.push(point);
                      }
                      else {
                          if (subpoligonCoords.length > 0) {
                              var subpoligon = new google.maps.Polyline({
                                  path: subpoligonCoords,
                                  geodesic: true,
                                  strokeColor: previousColor,
                                  strokeOpacity: 1.0,
                                  strokeWeight: 2
                              });
                              subpoligon.setMap(map);
                              polylinesArray[polyCounter] = subpoligon;
                              polyCounter++;
                          }
                          subpoligonCoords = [];
                          currentSubLocation = obj[i].State;
                          var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                          if (obj[i].Logs == '#17ff33') {
                              marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');

                              myOption = document.createElement("Option");
                              myOption.text = obj[i].Username; //Textbox's value
                              myOption.value = obj[i].Lat + "-" + obj[i].Long;
                              usermapListBox.add(myOption);
                              countries.push(obj[i].Username);
                          }
                          else {
                              marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');

                              myOption = document.createElement("Option");
                              myOption.text = obj[i].Username; //Textbox's value
                              myOption.value = obj[i].Lat + "-" + obj[i].Long;
                              usermapListBox.add(myOption);
                              countries.push(obj[i].Username);
                          }
                          previousColor = obj[i].Logs;
                          myMarkers[obj[i].Id] = marker;
                          createInfoWindow(marker, contentString);
                          var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                          subpoligonCoords.push(point);
                      }
                  }
              }
          }
          if (subpoligonCoords.length > 0) {
              var subpoligon = new google.maps.Polyline({
                  path: subpoligonCoords,
                  geodesic: true,
                  strokeColor: previousColor,
                  strokeOpacity: 1.0,
                  strokeWeight: 2
              });
              subpoligon.setMap(map);
              polylinesArray[polyCounter]= subpoligon;
              polyCounter++;
          }
      }
      catch (err) {
          alert(err);
      }
  }
  var firsttimeload = false;
  function togglemapDIV(mapview)
  {
      try
      {
          if (mapview == "USERS") {
              //clearIncidentsFromDashMap(myMarkers);
              clearTasksFromDashMap(myMarkers);
              //clearOthersFromDashMap(myMarkers);
              jQuery.ajax({
                  type: "POST",
                  url: "TaskDash.aspx/getGPSDataUsers",
                  data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function (data) {
                      if (data.d == "]") {
                          //getLocationNoOnline();
                      }
                      else {
                          var obj = jQuery.parseJSON(data.d)
                          updateGPSMapMarker(obj);
                      }
                  }
              });
              document.getElementById("headerincidentsDiv").innerHTML = currentlocation+" CENTER RIGHT NOW";
                    
 

                    
          }
          else if(mapview == "TASKS")
          {
              //clearIncidentsFromDashMap(myMarkers);
              clearUsersFromDashMap(myMarkers);
              //clearOthersFromDashMap(myMarkers);
              jQuery.ajax({
                  type: "POST",
                  url: "TaskDash.aspx/getGPSDataTasks",
                  data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function (data) {
                      if (data.d == "]") {
                          //getLocationNoOnline();
                      }
                      else {
                          var obj = jQuery.parseJSON(data.d)
                          updateGPSMapMarker(obj);
                      }
                  }
              });

              document.getElementById("headerincidentsDiv").innerHTML = currentlocation+" CENTER RIGHT NOW";
                    
 

          }
          else if(mapview == "INCIDENTS"){
              if(firsttimeload){
                  clearUsersFromDashMap(myMarkers);
                  clearTasksFromDashMap(myMarkers);
                  clearOthersFromDashMap(myMarkers);

                  document.getElementById("headerincidentsDiv").innerHTML = currentlocation+" Center right now";
 
              }
              else{
                  firsttimeload = true;
              }
          }
          else{
              clearIncidentsFromDashMap(myMarkers);
              clearUsersFromDashMap(myMarkers);
              clearTasksFromDashMap(myMarkers);

              document.getElementById("headerincidentsDiv").innerHTML = currentlocation+" Center right now";
 

          }
      }
      catch(err)
      {
          alert(err)
      }
  }

  function sendpushMultipleNotification(selectedUserIds, messages) {
      jQuery.ajax({
          type: "POST",
          url: "TaskDash.aspx/sendpushMultipleNotification",
          data: JSON.stringify({ userIds: selectedUserIds, msg: messages,uname: loggedinUsername }),
          dataType: "json",
          traditional: true,
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              if(data.d == "LOGOUT"){
                  showError("Session has expired. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              }
          },
          error: function () {
              showError("Session timeout. Kindly login again.");
              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
          }
      });
  }
  function getDispatchUserList(id, type) {
      document.getElementById("rowtracebackUser").style.display = "block";
      jQuery('#tracebackUserFilter div').html('');

      jQuery.ajax({
          type: "POST",
          url: "TaskDash.aspx/getDispatchUserList",
          data: "{'id':'" + id + "','ttype':'" + type + "','uname':'" + loggedinUsername + "'}",
          async: false,
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              if(data.d == "LOGOUT"){
                  showError("Session has expired. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              }else
                  document.getElementById("tracebackUserFilter").innerHTML = data.d;
          },
          error: function () {
              showError("Session timeout. Kindly login again.");
              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
          }
      });
  }
  function tracebackOn() {
      var id = 0;
      id = document.getElementById('rowChoiceTasks').value;
           
      dduration = "0";
      jQuery.ajax({
          type: "POST",
          url: "TaskDash.aspx/getTracebackLocationData",
          data: "{'id':'" + id  + "','uname':'" + loggedinUsername + "'}",
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              if(data.d == "LOGOUT"){
                  showError("Session has expired. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              }else{
                  var obj = jQuery.parseJSON(data.d)
                  if (obj.length > 1) {
                      updateTaskMarker(obj);
                      document.getElementById("rowtasktracebackUser").style.display = "block";
                  }
              }
          },
          error: function () {
              showError("Session timeout. Kindly login again.");
              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
          }
      });
  }
  var sselectedUserIds = [];
  function tracebackOnUser(id,type,uid)
  {
      var ell = document.getElementById(id + uid);
      if (ell) {
          if (ell.className == 'fa fa-square-o') {
              ell.className = 'fa fa-check-square-o';

              var index = sselectedUserIds.indexOf(uid);
              if (index > -1) {
                  sselectedUserIds.splice(index, 1);
              }
          }
          else {
              ell.className = 'fa fa-square-o';
              var a = sselectedUserIds.indexOf(uid);

              if (a < 0)
                  sselectedUserIds.push(uid);

          }
          $.ajax({
              type: "POST",
              url: "TaskDash.aspx/getTracebackLocationDataByUser",
              data: JSON.stringify({ id: id, duration: dduration, ttype: type, userIds: sselectedUserIds ,uname:loggedinUsername}),
              //data: "{'id':'" + id + "','duration':'" + dduration + "','ttype':'" + type + "','userIds':'" + uid + "'}",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  if(data.d == "LOGOUT"){
                      showError("Session has expired. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }else{
                      var obj = $.parseJSON(data.d)

                      if (type == "task")
                          updateTaskMarker(obj);
                      else
                          updateIncidentMarker(obj);
                  }
              },
              error: function () {
                  showError("Session timeout. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
              }
          });
      }
  }
  var dduration = "";
  function tracebackOnFilter(duration) {

      if (duration == "1") {
          var el = document.getElementById('taskfilter1');
          if (el) {
              if (el.className == 'fa fa-check-square-o') {
                  el.className = 'fa fa-square-o';
                  duration = "0";
              }
              else
                  el.className = 'fa fa-check-square-o';
          }
          var ell2 = document.getElementById('taskfilter2');
          if (ell2) {
              ell2.className = 'fa fa-square-o';
          }
          var ell3 = document.getElementById('taskfilter3');
          if (ell3) {
              ell3.className = 'fa fa-square-o';
          }
          var ell4 = document.getElementById('taskfilter4');
          if (ell4) {
              ell4.className = 'fa fa-square-o';
          }
      }
      else if (duration == "5") {
          var el2 = document.getElementById('taskfilter2');
          if (el2) {
              if (el2.className == 'fa fa-check-square-o') {
                  el2.className = 'fa fa-square-o';
                  duration = "0";
              }
              else
                  el2.className = 'fa fa-check-square-o';
          }
          var ell = document.getElementById('taskfilter1');
          if (ell) {
              ell.className = 'fa fa-square-o';
          }
          var ell3 = document.getElementById('taskfilter3');
          if (ell3) {
              ell3.className = 'fa fa-square-o';
          }
          var ell4 = document.getElementById('taskfilter4');
          if (ell4) {
              ell4.className = 'fa fa-square-o';
          }
      }
      else if (duration == "30") {
          var el3 = document.getElementById('taskfilter3');
          if (el3) {
              if (el3.className == 'fa fa-check-square-o') {
                  el3.className = 'fa fa-square-o';
                  duration = "0";
              }
              else
                  el3.className = 'fa fa-check-square-o';
          }
          var ell = document.getElementById('taskfilter1');
          if (ell) {
              ell.className = 'fa fa-square-o';
          }
          var ell2 = document.getElementById('taskfilter2');
          if (ell2) {
              ell2.className = 'fa fa-square-o';
          }
          var ell4 = document.getElementById('taskfilter4');
          if (ell4) {
              ell4.className = 'fa fa-square-o';
          }
      }
      else if (duration == "60") {
          var el4 = document.getElementById('taskfilter4');
          if (el4) {
              if (el4.className == 'fa fa-check-square-o') {
                  el4.className = 'fa fa-square-o';
                  duration = "0";
              }
              else
                  el4.className = 'fa fa-check-square-o';
          }
          var ell = document.getElementById('taskfilter1');
          if (ell) {
              ell.className = 'fa fa-square-o';
          }
          var ell2 = document.getElementById('taskfilter2');
          if (ell2) {
              ell2.className = 'fa fa-square-o';
          }
          var ell3 = document.getElementById('taskfilter3');
          if (ell3) {
              ell3.className = 'fa fa-square-o';
          }
      }

      $.ajax({
          type: "POST",
          url: "TaskDash.aspx/getTracebackLocationByDurationData",
          data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','duration':'" + duration + "','uname':'" + loggedinUsername + "'}",
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              if(data.d == "LOGOUT"){
                  showError("Session has expired. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              }else{
                  var obj = jQuery.parseJSON(data.d)
                  updateTaskMarker(obj);
              }
          },
          error: function () {
              showError("Session timeout. Kindly login again.");
              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
          }
      });
  }
  function reminderChoice(id, time, description, name, color) {
      document.getElementById('rowidReminderChoice').text = id;
      document.getElementById(color + 'RemName').innerHTML = name;
      document.getElementById(color + 'RemDate').innerHTML = time;
      document.getElementById(color + 'RemDesc').innerHTML = description;
  }
  function reminderRead() {
      var id = document.getElementById('rowidReminderChoice').text;
      jQuery.ajax({
          type: "POST",
          url: "TaskDash.aspx/markReminderAsRead",
          data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
          async: false,
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              if(data.d == "LOGOUT"){
                  showError("Session has expired. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              } 
              else if (data.d == "SUCCESS") {
                  showLoader();
                  location.reload();
              }
              else {
                  showAlert(data.d);
              }
          },
          error: function () {
              showError("Session timeout. Kindly login again.");
              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
          }
      });
  }
  function reminderDelete() {
      var id = document.getElementById('rowidReminderChoice').text;
      jQuery.ajax({
          type: "POST",
          url: "TaskDash.aspx/deleteReminder",
          data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
          async: false,
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              if(data.d == "LOGOUT"){
                  showError("Session has expired. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
              } 
              else if (data.d == "SUCCESS") {
                  showLoader();
                  location.reload();
              }
              else {
                  showAlert(data.d);
              }
          },
          error: function () {
              showError("Session timeout. Kindly login again.");
              setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
          }
      });
  }
  function rejecttaskselectAssigneeTypeChange() {
      var assignType = document.getElementById("rejecttaskSelectAssigneeType").value;
      if (assignType == "User") {
          document.getElementById("rejectUsersDiv").style.display = "block";
          document.getElementById("rejectGroupDiv").style.display = "none";
      }
      else if (assignType == "Group") {
          document.getElementById("rejectUsersDiv").style.display = "none";
          document.getElementById("rejectGroupDiv").style.display = "block";
      }
  }
  function userTaskAssign(val) {
      try {
          var isErr = false;
          if (!isEmptyOrSpaces(document.getElementById('rejectionTextarea').value)) {
              if (isSpecialChar(document.getElementById('rejectionTextarea').value)) {
                  isErr = true;
                  showAlert("Kindly remove special character from text area");
              }
          }
          else {
              isErr = true;
              showAlert("Kindly provide rejection notes");
          }
          if (val == "assign") {
              var assignType = document.getElementById("rejecttaskSelectAssigneeType").value;
              if (assignType == "User") {
                  var assignId = document.getElementById("MainContent_rejectusersearchselect").value;
                  var assigneeName = getSelectedText('MainContent_rejectusersearchselect');
              }
              else {
                  var assignId = document.getElementById("MainContent_rejectgroupsearchselect").value;
                  var assigneeName = getSelectedText('MainContent_rejectgroupsearchselect');
              }
          }
          else {
              var assignType = "User";
              var dd = document.getElementById('MainContent_rejectusersearchselect');
              for (var i = 0; i < dd.options.length; i++) {
                  if (dd.options[i].text === document.getElementById("typeSpan").innerHTML) {
                      dd.selectedIndex = i;
                      break;
                  }
              }
              var assignId = document.getElementById("MainContent_rejectusersearchselect").value;
              var assigneeName = getSelectedText('MainContent_rejectusersearchselect');
			  
          } 
          if (!assigneeName && val == "assign") {
              isErr = true;
              showAlert("Kindly provide assignee for the task");
          }

          if(!isErr){
              jQuery.ajax({
                  type: "POST",
                  url: "TaskDash.aspx/assignToNewUserTask",
                  data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','userid':'" + assignId + "','rejection':'" + document.getElementById('rejectionTextarea').value + "','username':'" + assigneeName + "','uname':'" + loggedinUsername + "','type':'" + assignType  + "','ttype':'" + val + "'}",
                  async: false,
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function (data) {
                      if (data.d[0] == "SUCCESS") {
                          if (val == "assign") {
                              if (assignType == "User") {
                                  chat.server.sendtoUser(btoa(data.d[2]), "TASK┴" + document.getElementById("incidentNameHeader").innerHTML + "┴" + document.getElementById("descriptionSpan").innerHTML + "┴0┴0", assignId);
                                  jQuery('#taskDocument').modal('hide');
                                  document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + assigneeName;
                                  jQuery('#successfulDispatch').modal('show');
                              }
                              else {
                                  chat.server.sendtoGroup(btoa(data.d[2]), assignId, "TASK┴" + document.getElementById("incidentNameHeader").innerHTML + "┴" + document.getElementById("descriptionSpan").innerHTML + "┴0┴0");
                                  jQuery('#taskDocument').modal('hide');
                                  document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + assigneeName;
                                  jQuery('#successfulDispatch').modal('show');
                              }
                          }
                          else if (val == "reassign") {
                              if (data.d[1] == "User") {
                                  chat.server.sendtoUser(btoa(data.d[2]), "TASK┴" + document.getElementById("incidentNameHeader").innerHTML + "┴" + document.getElementById("descriptionSpan").innerHTML + "┴0┴0", assignId);
                                  jQuery('#taskDocument').modal('hide');
                                  document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + assigneeName;
                                  jQuery('#successfulDispatch').modal('show');
                              }
                              else {
                                  chat.server.sendtoGroup(btoa(data.d[2]), data.d[3], "TASK┴" + document.getElementById("incidentNameHeader").innerHTML + "┴" + document.getElementById("descriptionSpan").innerHTML + "┴0┴0");
                                  jQuery('#taskDocument').modal('hide');
                                  document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + data.d[2];
                                  jQuery('#successfulDispatch').modal('show');
                              }
                          }
                         
                      }
                      else if (data.d[0] == "LOGOUT") {
                          document.getElementById('<%= logoutbtn.ClientID %>').click();
                      }
                      else {
                          showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                      }
                  },
                  error: function () {
                      showError("Session timeout. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                  }
              });
  }

}
    catch (ex)
    { showAlert('Error 49: Error occured trying to get task information-' + data.d); }
}
function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;

    return elt.options[elt.selectedIndex].text;
}
function taskStateChange(state) {
    var isErr = false;
    if (!isEmptyOrSpaces(document.getElementById('rejectionTextarea').value)) {
        if (isSpecialChar(document.getElementById('rejectionTextarea').value)) {
            isErr = true;
            showAlert("Kindly remove special character from text area");
        }
    }
    if (!isErr) {
        $.ajax({
            type: "POST",
            url: "TaskDash.aspx/ChangeTaskState",
            data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','state':'" + state + "','rejection':'" + document.getElementById('rejectionTextarea').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {
                    document.getElementById('successincidentScenario').innerHTML = "Task successfully updated";
                    jQuery('#successfulDispatch').modal('show');
                }
                else if (data.d == "LOGOUT") {
                    document.getElementById('<%= logoutbtn.ClientID %>').click();
                }
                else {
                    showAlert('Error 47: Error occured trying to change state of task.-' + data.d);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
}
function getEventStatusTotal() {
    try {
        try {
            $("#hero-area").empty();
            $("#hero-bar").empty();

            chartjsLine.destroy();
            chartjsDoughnut.destroy();
        }
        catch(ee){

        }
        jQuery.ajax({
            type: "POST",
            url: "TaskDash.aspx/getEventStatusTotal",
            data: "{'uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d[0] == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (data.d[0] != "ERROR") {
                    document.getElementById('statusAccept').innerHTML = data.d[0];
                    document.getElementById('statusAcceptPercent').innerHTML = data.d[1];

                    document.getElementById('statusComplete').innerHTML = data.d[2];
                    document.getElementById('statusCompletePercent').innerHTML = data.d[3];


                    document.getElementById('statusInprogress').innerHTML = data.d[4];
                    document.getElementById('statusInprogressPercent').innerHTML = data.d[5];

                    document.getElementById('statusPending').innerHTML = data.d[6];
                    document.getElementById('statusPendingPercent').innerHTML = data.d[7];
                    document.getElementById('statusTotal').innerHTML = data.d[8];

                    $('#statusAcceptedPie').data('easyPieChart').update(parseInt(data.d[1]));
                    $('#statusCompletePie').data('easyPieChart').update(parseInt(data.d[3]));
                    $('#statusInprogressPie').data('easyPieChart').update(parseInt(data.d[5]));
                    $('#statusPendingPie').data('easyPieChart').update(parseInt(data.d[7]));

                    doughnutCtxDemo = document.getElementById('chartjs-doughnutDemo').getContext('2d'),
doughnutData = {
    datasets: [{
        data: [data.d[7], data.d[5],data.d[3],data.d[1]], backgroundColor: [
                        '#f44e4b',
                        '#f2c400',
                        '#3ebb64',
                        '#1b93c0'
        ]
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
        'Pending = '+data.d[6],  
'Progress = '+data.d[4],
'Completed = '+data.d[2],
'Accepted = '+data.d[0],
    ]


 
},
    chartjsDoughnut = new Chart(doughnutCtxDemo, {
        type: 'doughnut',
        data: doughnutData,
        options: { 
            legend: {
                position:'bottom',
                labels: {
                    fontSize: 9,
                    boxWidth:20
                }
            },
            segmentShowStroke: false,
            responsive: false,
            tooltipTemplate: ""
        }, 
    } 
    );
                     
                    morrisArea = Morris.Area({
                        element: 'hero-area',
                        fillOpacity: 1,
                        data: [
                          {y: data.d[37], week1: data.d[9],week2: data.d[10],week3: data.d[11],week4: data.d[12]},
          {y: data.d[38], week1: data.d[13],week2: data.d[14],week3: data.d[15],week4: data.d[16]},
          {y: data.d[39],  week1: data.d[17],week2: data.d[18],week3: data.d[19],week4: data.d[20]},
          {y: data.d[40], week1: data.d[21],week2: data.d[22],week3: data.d[23],week4: data.d[24]},
          {y: data.d[41], week1: data.d[25],week2: data.d[26],week3: data.d[27],week4: data.d[28]},
          {y: data.d[42], week1: data.d[29],week2: data.d[30],week3: data.d[31],week4: data.d[32]},
          {y: data.d[43],  week1: data.d[33],week2: data.d[34],week3: data.d[35],week4: data.d[36]},
                        ],
                        xkey: 'y',
                        ykeys: ['week1','week2','week3','week4'],
                        labels: ['Week 1','Week 2','Week 3','Week 4'], 
                        pointSize: 0,
                        hideHover: true,
                        lineWidth: '3px',
                        lineColors: ['#1b93c0','#3ebb64','#f2c400','#f44e4b'],
                        gridTextColor: 'rgba(22, 24, 27, 0.87)',
                        gridLineColor: 'rgba(22, 24, 27, 0.26)',
                        responsive:false,
                        resize: false,
                        xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
                            var month = months[x.getDay()];
                            return month;
                        }
                    });
                      
                    document.getElementById('week1t').innerHTML = data.d[44];
                    document.getElementById('week2t').innerHTML = data.d[45];
                    document.getElementById('week3t').innerHTML = data.d[46];
                    document.getElementById('week4t').innerHTML = data.d[47];

                    var morrisBar = Morris.Bar({
                        element: 'hero-bar',
                        data: [
                          {device: data.d[48], avg: data.d[53]},
                          {device: data.d[49], avg: data.d[54]},
                          {device: data.d[50], avg: data.d[55]},
                          {device: data.d[51], avg: data.d[56]},
                          {device: data.d[52], avg: data.d[57]},
                        ],
                        xkey: 'device',
                        ykeys: ['avg'],
                        labels: ['Avg'],
                        barRatio: 0.4,
                        xLabelAngle: 75,
                        hideHover: 'auto',
                        barColors: ['#5D9CEC'],
                        gridTextColor: 'rgba(22, 24, 27, 0.87)',
                        gridLineColor: 'rgba(22, 24, 27, 0.26)',
                        responsive:false,
                        resize: false // NOTE: This has a significant performance impact, so is disabled by default.
                    });
			
                    lineCtx2 = document.getElementById('chartjs-lineDemo').getContext('2d'),
               lineData = {
                   labels: ['WEEK1', 'WEEK2', 'WEEK3', 'WEEK4'],
                   datasets: [
                     {
                         label: data.d[58],
                         backgroundColor: 'transparent',
                         borderColor: '#1b93c0',
                         pointColor: '#1b93c0',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#1b93c0',
                         data: [data.d[63], data.d[64], data.d[65], data.d[66]]
                     },
                     {
                         label: data.d[59],
                         backgroundColor: 'transparent',
                         borderColor: '#3ebb64',
                         pointColor: '#3ebb64',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#3ebb64',
                         data: [data.d[67], data.d[68], data.d[69], data.d[70]]
                     },
                     {
                         label: data.d[60],
                         backgroundColor: 'transparent',
                         borderColor: '#f2c400',
                         pointColor: '#f2c400',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#f2c400',
                         data: [data.d[71], data.d[72], data.d[73], data.d[74]]
                     },
                     {
                         label: data.d[61],
                         backgroundColor: 'transparent',
                         borderColor: '#f44e4b',
                         pointColor: '#f44e4b',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#f44e4b',
                         data: [data.d[75], data.d[76], data.d[77], data.d[78]]
                     },
                     {
                         label: data.d[62],
                         backgroundColor: 'transparent',
                         borderColor: '#b2163b',
                         pointColor: '#b2163b',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#b2163b',
                         data: [data.d[79], data.d[80], data.d[81], data.d[82]]
                     }
                   ]
               },
                           chartjsLine = new Chart(lineCtx2, {
                               type: 'line',
                               data: lineData,
                               options: {
                                   legend: {
                                       position:'bottom',
                                       labels: {
                                           fontSize: 9,
                                           boxWidth:20
                                       }
                                   },
                                   scaleBeginAtZero: true,
                                   scaleShowVerticalLines: false,
                                   responsive: false
                               } 
                           }); 

                    document.getElementById('top1ins').style.width = data.d[83];
                    document.getElementById('top2ins').style.width = data.d[84];
                    document.getElementById('top3ins').style.width = data.d[85];
                    document.getElementById('top4ins').style.width = data.d[86];
                    document.getElementById('top5ins').style.width = data.d[87];

                    document.getElementById('top1insname').innerHTML = data.d[88];
                    document.getElementById('top2insname').innerHTML = data.d[89];
                    document.getElementById('top3insname').innerHTML = data.d[90];
                    document.getElementById('top4insname').innerHTML = data.d[91];
                    document.getElementById('top5insname').innerHTML = data.d[92];

                    document.getElementById('top1inscount').innerHTML = data.d[93];
                    document.getElementById('top2inscount').innerHTML = data.d[94];
                    document.getElementById('top3inscount').innerHTML = data.d[95];
                    document.getElementById('top4inscount').innerHTML = data.d[96];
                    document.getElementById('top5inscount').innerHTML = data.d[97];
                      
                    document.getElementById('zaTotal').innerHTML = data.d[98];
                }
                else {
                    showError("Kindly select valid date range.");
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    catch (err) {
        alert(err);
    }
}

        var morrisArea;
        var lineCtx2;
        var chartjsDoughnut;
        var chartjsLine;
        var doughnutCtxDemo;
function getEventStatusRetrieve() {
    try { 

        $("#hero-area").empty();
        $("#hero-bar").empty();
        
        chartjsLine.destroy();
        chartjsDoughnut.destroy();
        jQuery.ajax({
            type: "POST",
            url: "TaskDash.aspx/getEventStatusRetrieve",
            data: "{'fromdate':'" + document.getElementById('fromStatusDatePicker').value + "','todate':'" + document.getElementById('toStatusDatePicker').value + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d[0] == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (data.d[0] != "ERROR") {
                    document.getElementById('statusAccept').innerHTML = data.d[0];
                    document.getElementById('statusAcceptPercent').innerHTML = data.d[1];

                    document.getElementById('statusComplete').innerHTML = data.d[2];
                    document.getElementById('statusCompletePercent').innerHTML = data.d[3];


                    document.getElementById('statusInprogress').innerHTML = data.d[4];
                    document.getElementById('statusInprogressPercent').innerHTML = data.d[5];

                    document.getElementById('statusPending').innerHTML = data.d[6];
                    document.getElementById('statusPendingPercent').innerHTML = data.d[7];
                    document.getElementById('statusTotal').innerHTML = data.d[8];

                    $('#statusAcceptedPie').data('easyPieChart').update(parseInt(data.d[1]));
                    $('#statusCompletePie').data('easyPieChart').update(parseInt(data.d[3]));
                    $('#statusInprogressPie').data('easyPieChart').update(parseInt(data.d[5]));
                    $('#statusPendingPie').data('easyPieChart').update(parseInt(data.d[7]));

                    doughnutCtxDemo = document.getElementById('chartjs-doughnutDemo').getContext('2d'),
doughnutData = {
    datasets: [{
        data: [data.d[7], data.d[5],data.d[3],data.d[1]], backgroundColor: [
                        '#f44e4b',
                        '#f2c400',
                        '#3ebb64',
                        '#1b93c0'
        ]
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
        'Pending = '+data.d[6],  
'Progress = '+data.d[4],
'Completed = '+data.d[2],
'Accepted = '+data.d[0],
    ]


 
},
    chartjsDoughnut = new Chart(doughnutCtxDemo, {
        type: 'doughnut',
        data: doughnutData,
        options: { 
            legend: {
                position:'bottom',
                labels: {
                    fontSize: 9,
                    boxWidth:20
                }
            },
            segmentShowStroke: false,
            responsive: false,
            tooltipTemplate: ""
        }, 
    } 
    );
                     
                    morrisArea = Morris.Area({
                        element: 'hero-area',
                        fillOpacity: 1,
                        data: [
                          {y: data.d[37], week1: data.d[9],week2: data.d[10],week3: data.d[11],week4: data.d[12]},
          {y: data.d[38], week1: data.d[13],week2: data.d[14],week3: data.d[15],week4: data.d[16]},
          {y: data.d[39],  week1: data.d[17],week2: data.d[18],week3: data.d[19],week4: data.d[20]},
          {y: data.d[40], week1: data.d[21],week2: data.d[22],week3: data.d[23],week4: data.d[24]},
          {y: data.d[41], week1: data.d[25],week2: data.d[26],week3: data.d[27],week4: data.d[28]},
          {y: data.d[42], week1: data.d[29],week2: data.d[30],week3: data.d[31],week4: data.d[32]},
          {y: data.d[43],  week1: data.d[33],week2: data.d[34],week3: data.d[35],week4: data.d[36]},
                        ],
                        xkey: 'y',
                        ykeys: ['week1','week2','week3','week4'],
                        labels: ['Week 1','Week 2','Week 3','Week 4'], 
                        pointSize: 0,
                        hideHover: true,
                        lineWidth: '3px',
                        lineColors: ['#1b93c0','#3ebb64','#f2c400','#f44e4b'],
                        gridTextColor: 'rgba(22, 24, 27, 0.87)',
                        gridLineColor: 'rgba(22, 24, 27, 0.26)',
                        responsive:false,
                        resize: false,
                        xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
                            var month = months[x.getDay()];
                            return month;
                        }
                    });
                      
                    document.getElementById('week1t').innerHTML = data.d[44];
                    document.getElementById('week2t').innerHTML = data.d[45];
                    document.getElementById('week3t').innerHTML = data.d[46];
                    document.getElementById('week4t').innerHTML = data.d[47];

                    var morrisBar = Morris.Bar({
                        element: 'hero-bar',
                        data: [
                          {device: data.d[48], avg: data.d[53]},
                          {device: data.d[49], avg: data.d[54]},
                          {device: data.d[50], avg: data.d[55]},
                          {device: data.d[51], avg: data.d[56]},
                          {device: data.d[52], avg: data.d[57]},
                        ],
                        xkey: 'device',
                        ykeys: ['avg'],
                        labels: ['Avg'],
                        barRatio: 0.4,
                        xLabelAngle: 75,
                        hideHover: 'auto',
                        barColors: ['#5D9CEC'],
                        gridTextColor: 'rgba(22, 24, 27, 0.87)',
                        gridLineColor: 'rgba(22, 24, 27, 0.26)',
                        responsive:false,
                        resize: false // NOTE: This has a significant performance impact, so is disabled by default.
                    });
			
                    lineCtx2 = document.getElementById('chartjs-lineDemo').getContext('2d'),
               lineData = {
                   labels: ['WEEK1', 'WEEK2', 'WEEK3', 'WEEK4'],
                   datasets: [
                     {
                         label: data.d[58],
                         backgroundColor: 'transparent',
                         borderColor: '#1b93c0',
                         pointColor: '#1b93c0',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#1b93c0',
                         data: [data.d[63], data.d[64], data.d[65], data.d[66]]
                     },
                     {
                         label: data.d[59],
                         backgroundColor: 'transparent',
                         borderColor: '#3ebb64',
                         pointColor: '#3ebb64',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#3ebb64',
                         data: [data.d[67], data.d[68], data.d[69], data.d[70]]
                     },
                     {
                         label: data.d[60],
                         backgroundColor: 'transparent',
                         borderColor: '#f2c400',
                         pointColor: '#f2c400',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#f2c400',
                         data: [data.d[71], data.d[72], data.d[73], data.d[74]]
                     },
                     {
                         label: data.d[61],
                         backgroundColor: 'transparent',
                         borderColor: '#f44e4b',
                         pointColor: '#f44e4b',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#f44e4b',
                         data: [data.d[75], data.d[76], data.d[77], data.d[78]]
                     },
                     {
                         label: data.d[62],
                         backgroundColor: 'transparent',
                         borderColor: '#b2163b',
                         pointColor: '#b2163b',
                         pointStrokeColor: '#ffffff',
                         pointHighlightFill: '#ffffff',
                         pointHighlightStroke: '#b2163b',
                         data: [data.d[79], data.d[80], data.d[81], data.d[82]]
                     }
                   ]
               },
                           chartjsLine = new Chart(lineCtx2, {
                               type: 'line',
                               data: lineData,
                               options: {
                                   legend: {
                                       position:'bottom',
                                       labels: {
                                           fontSize: 9,
                                           boxWidth:20
                                       }
                                   },
                                   scaleBeginAtZero: true,
                                   scaleShowVerticalLines: false,
                                   responsive: false
                               } 
                           }); 

                    document.getElementById('top1ins').style.width = data.d[83];
                    document.getElementById('top2ins').style.width = data.d[84];
                    document.getElementById('top3ins').style.width = data.d[85];
                    document.getElementById('top4ins').style.width = data.d[86];
                    document.getElementById('top5ins').style.width = data.d[87];

                    document.getElementById('top1insname').innerHTML = data.d[88];
                    document.getElementById('top2insname').innerHTML = data.d[89];
                    document.getElementById('top3insname').innerHTML = data.d[90];
                    document.getElementById('top4insname').innerHTML = data.d[91];
                    document.getElementById('top5insname').innerHTML = data.d[92];

                    document.getElementById('top1inscount').innerHTML = data.d[93];
                    document.getElementById('top2inscount').innerHTML = data.d[94];
                    document.getElementById('top3inscount').innerHTML = data.d[95];
                    document.getElementById('top4inscount').innerHTML = data.d[96];
                    document.getElementById('top5inscount').innerHTML = data.d[97];
                    
                    document.getElementById('dmonth').innerHTML = data.d[98]; 
                    document.getElementById('zaTotal').innerHTML = data.d[99];
                }
                else {
                    showError("Kindly select valid date range.");
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
    catch (err) {
        alert(err);
    } 
}
//INCIDENT START
function assignrowDataIncident(id) {
    var output = "";
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getTableRowDataIncident",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d[0] == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                document.getElementById('escalateionSpanDiv').style.display = 'none';
                document.getElementById("usernameSpan").innerHTML = data.d[0];
                document.getElementById("timeSpan").innerHTML = data.d[1];
                document.getElementById("typeSpan").innerHTML = data.d[2];
                        
                var ret = data.d[3];
                var splitRetval = ret.split('-');
                if (splitRetval.length > 0) {
                    ret = splitRetval[0];
                }
                document.getElementById("statusSpan").innerHTML = ret;
                document.getElementById("locSpan").innerHTML = data.d[4];
                document.getElementById("receivedBySpan").innerHTML = data.d[5];
                document.getElementById("phonenumberSpan").innerHTML = data.d[6];
                document.getElementById("emailSpan").innerHTML = data.d[7];
                document.getElementById("descriptionSpan").innerHTML = data.d[8]; 
                document.getElementById("instructionSpan").innerHTML = data.d[9];
                document.getElementById("incidentNameHeader").innerHTML = data.d[10];
                var el = document.getElementById('headerImageClass');
                if (el) {
                    el.className = data.d[11];
                }
                document.getElementById("rowLongitude").text = data.d[12];
                document.getElementById("rowLatitude").text = data.d[13];
                output = data.d[3];
                if (data.d[14] == "TASK") {
                    document.getElementById('checklistDIV').style.display = 'none';
                    getTasklistItems(id);

                    if (data.d[3] == "Escalated" || data.d[3] == "Reject" || data.d[3] == "Resolve" || data.d[3] == "Resolve-MyIncident") {
                        document.getElementById('escalateionSpanDiv').style.display = 'block';
                        document.getElementById("escalationHead").innerHTML = data.d[3] + " Notes";

                        if (data.d[3] == "Resolve-MyIncident")
                            document.getElementById("escalationHead").innerHTML = "Resolve Notes";

                        document.getElementById("escalatedSpan").innerHTML = data.d[15];

                    }
                    document.getElementById('resolveLi').style.display = data.d[16];
                    document.getElementById('disResolveLi').style.display = data.d[16];
                    document.getElementById('compResolveLi').style.display = data.d[16];
                    document.getElementById('compResolveLi2').style.display = data.d[16];
                }
                else {
                    if (data.d[3] == "Escalated" || data.d[3] == "Reject" || data.d[3] == "Resolve" || data.d[3] == "Resolve-MyIncident") {
                        document.getElementById('escalateionSpanDiv').style.display = 'block';
                        document.getElementById("escalationHead").innerHTML = data.d[3] + " Notes";

                        if (data.d[3] == "Resolve-MyIncident")
                            document.getElementById("escalationHead").innerHTML = "Resolve Notes";

                        document.getElementById("escalatedSpan").innerHTML = data.d[14];
                    }
                    document.getElementById('resolveLi').style.display = data.d[15];
                    document.getElementById('disResolveLi').style.display = data.d[15];
                    document.getElementById('compResolveLi').style.display = data.d[15];
                    document.getElementById('compResolveLi2').style.display = data.d[15];
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
    return output;
}
function clearIncidentTabDefault() {
    var el = document.getElementById('activity-tab');
    if (el) {
        el.className = 'tab-pane fade ';
    }
    var el3 = document.getElementById('info-tab');
    if (el3) {
        el3.className = 'tab-pane fade active in';
    }
    var el4 = document.getElementById('attachments-tab');
    if (el4) {
        el4.className = 'tab-pane fade';
    }

    var el2 = document.getElementById('liInfo');
    if (el2) {
        el2.className = 'active';
    }  
    var el5 = document.getElementById('liActi');
    if (el5) {
        el5.className = ' ';
    }
    var el6 = document.getElementById('liAtta');
    if (el6) {
        el6.className = ' ';
    }
    var ell = document.getElementById('taskfilter1');
    if (ell) {
        ell.className = 'fa fa-square-o';
    }
    var ell2 = document.getElementById('taskfilter2');
    if (ell2) {
        ell2.className = 'fa fa-square-o';
    }
    var ell3 = document.getElementById('taskfilter3');
    if (ell3) {
        ell3.className = 'fa fa-square-o';
    }
    var ell4 = document.getElementById('taskfilter4');
    if (ell4) {
        ell4.className = 'fa fa-square-o';
    }
    var elll = document.getElementById('filter1');
    if (elll) {
        elll.className = 'fa fa-square-o';
    }
    var elll2 = document.getElementById('filter2');
    if (elll2) {
        elll2.className = 'fa fa-square-o';
    }
    var elll3 = document.getElementById('filter3');
    if (elll3) {
        elll3.className = 'fa fa-square-o';
    }
    var elll4 = document.getElementById('filter4');
    if (elll4) {
        elll4.className = 'fa fa-square-o';
    }
} 
function incidentOldDivContainers() {
    try {
        for (var i = 0; i < incidentdivArray.length; i++) {
            var el = document.getElementById(incidentdivArray[i]);
            el.parentNode.removeChild(el);
        }
        incidentdivArray = new Array();
    }
    catch (ex) {
        //alert(ex);
    }
}
function incidentHistoryData(id) {
    jQuery('#divIncidentHistoryActivity div').html('');
    $.ajax({
        type: "POST",
        url: "TaskDash.aspx/getEventHistoryDataIncident",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d[0] == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                for (var i = 0; i < data.d.length; i++) {
                    var div = document.createElement('div');

                    div.className = 'row activity-block-container';

                    div.innerHTML = data.d[i];

                    document.getElementById('divIncidentHistoryActivity').appendChild(div);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}
        var myMarkersIncidentLocation = new Array();
function showIncidentDocument(name) {
    startRot();
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getGPSDataUsers",
        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                var obj = jQuery.parseJSON(data.d)
                setMapOnAll(obj, incidentMyMarkersIncidentLocation);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
    clearIncidentTabDefault();

    document.getElementById("escalateionSpanDiv").style.display = "none";
    document.getElementById('rejectionTextarea').value = "";
    document.getElementById('resolutionTextarea').value = "";

    document.getElementById("resolutionTextarea").style.display = "none";

    document.getElementById('escalateTextarea').value = "";
    document.getElementById("rowtasktracebackUser").style.display = "none";
    document.getElementById("rowtracebackUser").style.display = "none";
    document.getElementById('checklistDIV').style.display = 'none';
    document.getElementById("finishLi").style.display = "none";
    document.getElementById("nextLi").style.display = "none";
    document.getElementById("rowidChoice").text = name;
    document.getElementById("handleOptionsDiv").style.display = "none";
    incidentOldDivContainers();
    oldDivContainers();
    var retval = assignrowDataIncident(name);

    var splitRetval = retval.split('-');
    if (splitRetval.length > 0) {
        retval = splitRetval[0];
    }
    if (retval == "Complete") {
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "block";
        document.getElementById("dispatchOptionsDiv").style.display = "none";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("resolutionTextarea").style.display = "block";
        document.getElementById("completedOptionsDiv2").style.display = "none";

        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "none";
    }
    else if (retval == "Dispatch") {
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "block";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "none";
    }
    else if (retval == "Pending") {
        document.getElementById("initialOptionsDiv").style.display = "block";
        document.getElementById("parkLi").style.display = "block";
        document.getElementById("handleOptionsDiv").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "none";
    }
    else if (retval == "Release") {
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "block";
        document.getElementById("parkLi").style.display = "block";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "none";
    }
    else if (retval == "Engage") {
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "block";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "none";
    }
    else if (retval == "Park") {
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "block";
        document.getElementById("parkLi").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "none";
    }
    else if (retval == "Reject") {
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("parkLi").style.display = "none";
        document.getElementById("resolveLi").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "block";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "none";
    }
    else if (retval == "Resolve") {
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "block";
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("parkLi").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "none";
    }
    else if (retval == "Escalated") {
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("completedOptionsDiv2").style.display = "none";
        document.getElementById("resolvedDiv").style.display = "none";
        document.getElementById("initialOptionsDiv").style.display = "none";
        document.getElementById("parkLi").style.display = "none";
        document.getElementById("handleOptionsDiv").style.display = "none";
        document.getElementById("completedOptionsDiv").style.display = "none";
        document.getElementById("dispatchOptionsDiv").style.display = "none";
        document.getElementById("rejectOptionsDiv").style.display = "none";
        document.getElementById("rejectionTextarea").style.display = "none";
        document.getElementById("escalateOptionsDiv").style.display = "none";
        document.getElementById("escalateTextarea").style.display = "none";
        document.getElementById("escalatedDIV").style.display = "block";

    }
    if (splitRetval.length > 0) {

        if (splitRetval[1] == "Resolve") {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "block";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (splitRetval[1] == "MyIncident") {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "block";
            if (retval == "Resolve") {
                document.getElementById("escalatedDIV").style.display = "none";
                document.getElementById("resolvedDiv").style.display = "block";
            }

        }
    }



    incidentHistoryData(name);
    incidentinsertAttachmentIcons(name);
    incidentinsertAttachmentTabData(name);
    incidentinsertAttachmentData(name);
    dispatchAssignMapTab();
    cleardispatchList();
    incidentinfotabDefault();
}
function incidentinfotabDefault() {
    var el = document.getElementById('activity-tab');
    if (el) {
        el.className = 'tab-pane fade ';
    }
    var el3 = document.getElementById('info-tab');
    if (el3) {
        el3.className = 'tab-pane fade active in';
    }
    var el4 = document.getElementById('attachments-tab');
    if (el4) {
        el4.className = 'tab-pane fade';
    }
}
function dispatchAssignMapTab() {
    var el = document.getElementById('incidentAssign-user-tab');
    if (el) {
        el.className = 'tab-pane fade ';
    }
    var el3 = document.getElementById('location-tab');
    if (el3) {
        el3.className = 'tab-pane fade active in';
    }
    var el4 = document.getElementById('incidentNext-user-tab');
    if (el4) {
        el4.className = 'tab-pane fade';
    }
}
function incidentinsertAttachmentData(id) {
    document.getElementById('incirotationDIV1').style.display = "none";
    document.getElementById('incirotationDIV2').style.display = "none";
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getAttachmentDataIncident",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d[0] == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                imgcount = 0;
                document.getElementById('incidentAudioDIV').style.display="none";
                for (var i = 0; i < data.d.length; i++) {
                    if (data.d[i].indexOf("video") >= 0) {
                        var div = document.createElement('div');
                        div.className = 'tab-pane fade';
                        div.innerHTML = data.d[i];
                        div.id = 'video-' + (i + 1) + '-tab';
                        document.getElementById('divAttachmentHolder').appendChild(div);
                        incidentdivArray[i] = 'video-' + (i + 1) + '-tab';
                        imgcount++;
                    } 
                    else {
                        var div = document.createElement('div');
                        div.className = 'tab-pane fade';
                        div.align = 'center';
                        div.style.height = '380px';
                        div.innerHTML = data.d[i];
                        div.id = 'image-' + (i + 1) + '-tab';
                        document.getElementById('divAttachmentHolder').appendChild(div);
                        incidentdivArray[i] = 'image-' + (i + 1) + '-tab';
                        imgcount++;
                    }
                }
                if (imgcount > 0) {
                    document.getElementById('incirotationDIV1').style.display = "block";
                    document.getElementById('incirotationDIV2').style.display = "block";
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}

        function audioincidentplay(sr){
            document.getElementById('incidentAudioDIV').style.display="block";
            document.getElementById('incidentAudioSrc').src = sr; 
            document.getElementById('incidentAudio').load(); //call this to just preload the audio without playing
 
        } 	
        function hideIncidentplay(){
            document.getElementById('incidentAudioDIV').style.display="none";
        }
        function audiotaskplay(sr) {
            document.getElementById('taskAudioDIV').style.display = "block";
            document.getElementById('taskAudioSrc').src = sr;
            document.getElementById('taskAudio').load(); //call this to just preload the audio without playing

        }
        function hideTaskplay() {
            document.getElementById('taskAudioDIV').style.display = "none";
        }

function incidentinsertAttachmentIcons(id) {
    jQuery('#divAttachment div').html('');
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getAttachmentDataIconsIncident",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                document.getElementById("divAttachment").innerHTML = data.d;
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}
function incidentinsertAttachmentTabData(id) {
    jQuery('#attachments-info-tab div').html('');

    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getAttachmentDataTabIncident",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                document.getElementById("attachments-info-tab").innerHTML = data.d;
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}
function viewdurationselect(dur) {
    try {
        //camfilter1
        document.getElementById('cameraDuration').text = dur;
        if (dur == 15) {
            var el = document.getElementById('camviewfilter1');
            if (el) {
                if (el.className == 'fa fa-check-square-o') {
                    el.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el.className = 'fa fa-check-square-o';
            }
            var ell2 = document.getElementById('camviewfilter2');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('camviewfilter3');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('camviewfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
            var ell5 = document.getElementById('camviewfilter5');
            if (ell5) {
                ell5.className = 'fa fa-square-o';
            }

        }
        else if (dur == 30) {
            var el = document.getElementById('camviewfilter2');
            if (el) {
                if (el.className == 'fa fa-check-square-o') {
                    el.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el.className = 'fa fa-check-square-o';
            }
            var ell2 = document.getElementById('camviewfilter1');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('camviewfilter3');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('camviewfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
            var ell5 = document.getElementById('camviewfilter5');
            if (ell5) {
                ell5.className = 'fa fa-square-o';
            }
        }
        else if (dur == 45) {
            var el = document.getElementById('camviewfilter3');
            if (el) {
                if (el.className == 'fa fa-check-square-o') {
                    el.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el.className = 'fa fa-check-square-o';
            }
            var ell2 = document.getElementById('camviewfilter1');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('camviewfilter2');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('camviewfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
            var ell5 = document.getElementById('camviewfilter5');
            if (ell5) {
                ell5.className = 'fa fa-square-o';
            }
        }
        else if (dur == 60) {
            var el = document.getElementById('camviewfilter4');
            if (el) {
                if (el.className == 'fa fa-check-square-o') {
                    el.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el.className = 'fa fa-check-square-o';
            }
            var ell2 = document.getElementById('camviewfilter1');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('camviewfilter2');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('camviewfilter3');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
            var ell5 = document.getElementById('camviewfilter5');
            if (ell5) {
                ell5.className = 'fa fa-square-o';
            }
        }
        else if (dur == 120) {
            var el = document.getElementById('camviewfilter5');
            if (el) {
                if (el.className == 'fa fa-check-square-o') {
                    el.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el.className = 'fa fa-check-square-o';
            }
            var ell2 = document.getElementById('camviewfilter1');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('camviewfilter2');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('camviewfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
            var ell5 = document.getElementById('camviewfilter3');
            if (ell5) {
                ell5.className = 'fa fa-square-o';
            }
        }
    }
    catch (err)
    { alert(err) }
}
function getTasklistItems(id) {
    document.getElementById("taskItemsList").innerHTML = "";
    $.ajax({
        type: "POST",
        url: "TaskDash.aspx/getTaskListData",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d[0] == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                document.getElementById("taskItemsnameSpan").innerHTML = data.d[0];
                for (var i = 1; i < data.d.length; i++) {
                    var res = data.d[i].split("|");
                    var ul = document.getElementById("taskItemsList");
                    var li = document.createElement("li");
                    li.innerHTML = '<a href="#taskDocument"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="showTaskDocument(&apos;' + res[1] + '&apos;)">' + res[0] + '</a>';
                    //li.appendChild(document.createTextNode(data.d[i]));
                    ul.appendChild(li);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}
        var infoWindowIncidentLocation;
function getIncidentLocationMarkers(obj) {


    locationAllowed = true;
    //setTimeout(function () {
    google.maps.visualRefresh = true;
    var firstRed = false;
    var Liverpool;
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].State == "PURPLE") {
            Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
        }
        else if (obj[i].State == "RED") {
            if (!firstRed) {
                Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                firstRed = true;
            }
        }
    }
    // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
    var mapOptions = {
        zoom: 10,
        center: Liverpool,
        mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
    };
    // This makes the div with id "map_canvas" a google map
    incidentmapIncidentLocation = new google.maps.Map(document.getElementById("map_canvasIncidentLocation"), mapOptions);
    var poligonCoords = [];
    var first = true;
    for (var i = 0; i < obj.length; i++) {

        var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '"  onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>'
        //'<div id="content">' +'Name:' + obj[i].Username +'<br/><input class="specialTaskbutton tasksfont"  type="button" id="' + obj[i].Id + obj[i].Username + '" style="width:100px;" value="ADD" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)" />''</div>';

        var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);


        if (obj[i].State == "YELLOW") {
            var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
            incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
            incidentcreateInfoWindowIncidentLocation(marker, contentString);
        }
        else if (obj[i].State == "GREEN") {
            var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
            incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
            incidentcreateInfoWindowIncidentLocation(marker, contentString);
        }
        else if (obj[i].State == "BLUE") {
            var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
            marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
            incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
            incidentcreateInfoWindowIncidentLocation(marker, contentString);
        }
        else if (obj[i].State == "OFFUSER") {
            var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
            incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
            incidentcreateInfoWindowIncidentLocation(marker, contentString);
        }
        else if (obj[i].State == "OFFCLIENT") {
            var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
            marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
            incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
            incidentcreateInfoWindowIncidentLocation(marker, contentString);
        }
        else if (obj[i].State == "PURPLE") {
            var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
            marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
            contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
            incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
            incidentcreateInfoWindowIncidentLocation(marker, contentString);
            document.getElementById("rowIncidentName").text = obj[i].Username;
        }
        else if (obj[i].State == "RED") {
            if (first) {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
                document.getElementById("rowIncidentName").text = obj[i].Username;
                first = false;
            }
            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
            poligonCoords.push(point);
        }

    }
    if (poligonCoords.length > 0) {
        updatepoligon = new google.maps.Polyline({
            path: poligonCoords,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });
        updatepoligon.setMap(incidentmapIncidentLocation);
    }
}
function incidentcreateInfoWindowIncidentLocation(marker, popupContent) {
    google.maps.event.addListener(marker, 'click', function () {
        incidentinfoWindowLocation.setContent(popupContent);
        incidentinfoWindowLocation.open(incidentmapIncidentLocation, this);
    });
}
function updateIncidentMarker(obj) {
    try {
        var poligonCoords = [];
        var tracepoligonCoords = [];
        var first = true;
        var first2 = true;
        var currentSubLocation;
        var secondfirst = true;
        var previousColor = "";
        var subpoligonCoords = [];
        var previousMarker = document.getElementById("rowIncidentName").text;
        for (var i = 0; i < Object.size(incidentmyTraceBackMarkers) ; i++) {
            if (incidentmyTraceBackMarkers[i] != null) {
                incidentmyTraceBackMarkers[i].setMap(null);
            }
        }

        if (typeof tracebackpoligon === 'undefined') {
            // your code here.
        }
        else {
            tracebackpoligon.setMap(null);
        }
        if (typeof previousMarker === 'undefined') {
            // your code here.
        }
        else {
            incidentMyMarkersIncidentLocation[previousMarker].setMap(null);
            if (typeof updatepoligon === 'undefined') {
                // your code here.
            }
            else {
                updatepoligon.setMap(null);
            }
        }
        var tbcounter = 0;
        for (var i = 0; i < obj.length; i++) {

            var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '"  onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>'

            //'<div id="content">' +'Name:' + obj[i].Username +'<br/><input class="specialTaskbutton tasksfont"  type="button" id="' + obj[i].Id + obj[i].Username + '" style="width:100px;" value="ADD" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)" />''</div>';

            var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
            incidentmapIncidentLocation.setCenter(new google.maps.LatLng(obj[i].Lat, obj[i].Long));
            incidentmapIncidentLocation.setZoom(9);
            if (obj[i].State == "YELLOW") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "GREEN") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "BLUE") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "OFFUSER") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "OFFCLIENT") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "PURPLE") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
                document.getElementById("rowIncidentName").text = obj[i].Username;
            }
            else if (obj[i].State == "ENG") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                incidentmyTraceBackMarkers[tbcounter] = marker;
                tbcounter++;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "COM") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png')
                contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                incidentmyTraceBackMarkers[tbcounter] = marker;
                tbcounter++;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                subpoligonCoords.push(point);
                if (subpoligonCoords.length > 0) {
                    var subpoligon = new google.maps.Polyline({
                        path: subpoligonCoords,
                        geodesic: true,
                        strokeColor: previousColor,
                        strokeOpacity: 1.0,
                        strokeWeight: 7,
                        icons: [{
                            icon: iconsetngs, 
                            offset: '100%'
                        }]
                    });
                    subpoligon.setMap(incidentmapIncidentLocation);
                    animateCircle(subpoligon);
                    incidentmyTraceBackMarkers[tbcounter] = subpoligon;
                    tbcounter++;
                }
            }
            else if (obj[i].State == "RED") {
                if (first) {
                    contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                    document.getElementById("rowIncidentName").text = obj[i].Username;
                    first = false;
                }
                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                poligonCoords.push(point);
            }
            else {
                if (secondfirst) {
                    currentSubLocation = obj[i].State;
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                    incidentmyTraceBackMarkers[tbcounter] = marker;
                    tbcounter++;
                    previousColor = obj[i].Logs;
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                    secondfirst = false;
                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    subpoligonCoords.push(point);

                }
                else {
                    if (currentSubLocation == obj[i].State) {
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        subpoligonCoords.push(point);
                        var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                        incidentmyTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                    }
                    else {
                        if (subpoligonCoords.length > 0) {
                            var subpoligon = new google.maps.Polyline({
                                path: subpoligonCoords,
                                geodesic: true,
                                strokeColor: previousColor,
                                strokeOpacity: 1.0,
                                strokeWeight: 7,
                                icons: [{
                                    icon: iconsetngs, 
                                    offset: '100%'
                                }]
                            });
                            subpoligon.setMap(incidentmapIncidentLocation);
                            animateCircle(subpoligon);
                            incidentmyTraceBackMarkers[tbcounter] = subpoligon;
                            tbcounter++;
                        }
                        subpoligonCoords = [];
                        currentSubLocation = obj[i].State;
                        var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                        incidentmyTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                        previousColor = obj[i].Logs;
                        incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                        incidentcreateInfoWindowIncidentLocation(marker, contentString);
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        subpoligonCoords.push(point);
                    }
                }
            }
        }
        if (poligonCoords.length > 0) {
            updatepoligon = new google.maps.Polyline({
                path: poligonCoords,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            updatepoligon.setMap(incidentmapIncidentLocation);
        }
        if (tracepoligonCoords.length > 0) {
            tracebackpoligon = new google.maps.Polyline({
                path: tracepoligonCoords,
                geodesic: true,
                strokeColor: '#1b93c0',
                strokeOpacity: 1.0,
                strokeWeight: 7,
                icons: [{
                    icon: iconsetngs, 
                    offset: '100%'
                }]
            });
            tracebackpoligon.setMap(incidentmapIncidentLocation);
            animateCircle(tracebackpoligon);
        }
        if (subpoligonCoords.length > 0) {
            var subpoligon = new google.maps.Polyline({
                path: subpoligonCoords,
                geodesic: true,
                strokeColor: previousColor,
                strokeOpacity: 1.0,
                strokeWeight: 7,
                icons: [{
                    icon: iconsetngs, 
                    offset: '100%'
                }]
            });
            subpoligon.setMap(incidentmapIncidentLocation);
            animateCircle(subpoligon);
            incidentmyTraceBackMarkers[tbcounter] = subpoligon;
            tbcounter++;
        }
    }
    catch (err) {
        //showAlert(err);
    }
}

function uploadattachment(id, imgpath) {
    if (typeof imgpath === 'undefined') {

    }
    else {
        var res = imgpath.replace(/\\/g, "|")
        try {
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/uploadMobileAttachment",
                data: "{'id':'" + id + "','imgpath':'" + res + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        catch (err) {
            alert(err);
        }
    }
}
function resolveClick() {
    document.getElementById("incidentinitialOptionsDiv").style.display = "none";
    document.getElementById("incidenthandleOptionsDiv").style.display = "none";
    document.getElementById("completedOptionsDiv").style.display = "none";
    document.getElementById("dispatchOptionsDiv").style.display = "none";
    document.getElementById("incidentrejectOptionsDiv").style.display = "none";
    document.getElementById("incidentrejectionTextarea").style.display = "none";
    document.getElementById("resolvedDiv").style.display = "none";
    document.getElementById("escalateTextarea").style.display = "none";
    document.getElementById("completedOptionsDiv2").style.display = "block";
    document.getElementById("resolutionTextarea").style.display = "block";
    document.getElementById("escalateOptionsDiv").style.display = "none";
}

function generateTaskPDF() {
    width = 1;
    moveTask();
    document.getElementById("pdfloadingAcceptTask").style.display = "block";
    document.getElementById("gnNoteTask").innerHTML = "GENERATING REPORT";
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/CreatePDFTask",
        data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d != "Failed to generate report!") {
               // jQuery('#taskDocument').modal('hide');
                window.open(data.d);
                //document.getElementById('successfulReportScenario').innerHTML = "Report successfully created!";
                //jQuery('#successfulReport').modal('show');
                showAlert("Report successfully created!");
                document.getElementById("pdfloadingAcceptTask").style.display = "none";
            }
            else {
                showError(data.d);
                document.getElementById("pdfloadingAcceptTask").style.display = "none";
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}
function generateIncidentPDF() {
    widthincident = 1;
    moveincident();
    document.getElementById("incidentpdfloadingAccept").style.display = "block";
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/CreatePDFIncident",
        data: "{'id':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d != "Failed to generate report!") {
               // jQuery('#incidentDocument').modal('hide');
                window.open(data.d);
                //document.getElementById('successfulReportScenario').innerHTML = "Report successfully created!";
                //jQuery('#successfulReport').modal('show');
                showAlert("Report successfully created!");
                document.getElementById("incidentpdfloadingAccept").style.display = "none";
            }
            else {
                showError(data.d);
                document.getElementById("incidentpdfloadingAccept").style.display = "none";
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}


var incidentinfoWindowLocation;
var incidentmapIncidentLocation;
var incidentMyMarkersIncidentLocation = new Array();
var incidentmyTraceBackMarkers = new Array();

var incidentfirstpressTaskChoice = false;
var incidentdivArray = new Array();
function deleteAttachmentChoiceTicket(id) {
    jQuery('#deleteAttachTicketModal').modal('show');
    document.getElementById('rowidChoiceAttachment').value = id;
    jQuery('#ticketingViewCard').modal('hide');
}

function deleteAttachmentTicket() {
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/deleteAttachmentDataTicket",
        data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d != "LOGOUT") {
                showAlert(data.d);
                rowchoiceTicket(document.getElementById('rowidChoiceTicket').value);
                jQuery('#ticketingViewCard').modal('show');
            }
            else {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}

        function generateTicketPDF() {
            width = 1;
            moveTick();
            document.getElementById("pdfloadingAcceptTick").style.display = "block";
            document.getElementById("gnNoteTick").innerHTML = "GENERATING REPORT";

            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/CreatePDFTicket",
                data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d != "Failed to generate report!") {
                       // jQuery('#ticketingViewCard').modal('hide');
                        //document.getElementById('successincidentScenario').innerHTML = "Report successfully created!";
                        // jQuery('#successfulDispatch').modal('show');
                        showAlert("Report successfully created!");
                        document.getElementById("pdfloadingAccept").style.display = "none";
                        window.open(data.d);  
                    }
                    else {
                        showError(data.d);
                        document.getElementById("pdfloadingAccept").style.display = "none";
                    }
                }
            });
        }

function deleteAttachmentChoiceAsset(id) {
    jQuery('#deleteAttachModal').modal('show');
    document.getElementById('rowidChoiceAttachment').value = id;
    jQuery('#taskDocument').modal('hide');
}
function deleteAttachment() {
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/deleteAttachmentData",
        data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','taskid':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d != "LOGOUT") {
                showAlert(data.d);
                showTaskDocument(document.getElementById('rowidChoiceTasks').text);
                jQuery('#taskDocument').modal('show');
            }
            else {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}
        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
jQuery(document).ready(function () {
    try{        
        document.getElementById("liTicketTask").style.display = "none";
        incidentinfoWindowLocation = new google.maps.InfoWindow();
        infoWindowTaskLocation = new google.maps.InfoWindow();
        infoWindow = new google.maps.InfoWindow();
        infoWindowLocation = new google.maps.InfoWindow();
        infoVerWindowLocation = new google.maps.InfoWindow();
        infoWindowIncidentLocation = new google.maps.InfoWindow();
        
        localStorage.removeItem("activeTabDev");
        localStorage.removeItem("activeTabInci");
        localStorage.removeItem("activeTabMessage");
        localStorage.removeItem("activeTabTask");
        localStorage.removeItem("activeTabTick");
        localStorage.removeItem("activeTabUB");
        localStorage.removeItem("activeTabVer");
        localStorage.removeItem("activeTabLost");    

        $('input[type=password]').keyup(function () {
            // keyup event code here
            var pswd = $(this).val();
            if (pswd.length < 8) {
                $('#length').removeClass('valid').addClass('invalid');
                lengthGood = false;
            } else {
                $('#length').removeClass('invalid').addClass('valid');
                lengthGood = true;
            }
            //validate letter
            if (pswd.match(/[A-z]/)) {
                $('#letter').removeClass('invalid').addClass('valid');
                letterGood = true;
                    
            } else {
                $('#letter').removeClass('valid').addClass('invalid');
                letterGood = false;
            }

            //validate capital letter
            if (pswd.match(/[A-Z]/)) {
                $('#capital').removeClass('invalid').addClass('valid');
                capitalGood = true;
            } else {
                $('#capital').removeClass('valid').addClass('invalid');
                    
                capitalGood = false;
            }

            //validate number
            if (pswd.match(/\d/)) {
                $('#number').removeClass('invalid').addClass('valid');
                numGood = true;
                   
            } else {
                $('#number').removeClass('valid').addClass('invalid');
                numGood = false;
            }
        });
        $('input[type=password]').focus(function () {
            // focus code here
            $('#pswd_info').show();
        });
        $('input[type=password]').blur(function () {
            // blur code here
            $('#pswd_info').hide();
        });

        Initialize();
        <%--jQuery.ajax({
            type: "POST",
            url: "TaskDash.aspx/getGPSDataTasks",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.d == "LOGOUT"){
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (data.d == "]") {
                    //getLocationNoOnline();
                }
                else {
                    var obj = jQuery.parseJSON(data.d)
                    updateGPSMapMarker(obj);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });--%>
        document.getElementById("headerincidentsDiv").innerHTML = currentlocation+" CENTER RIGHT NOW";
                    
 
    }
    catch(err){}

    var myDropzonePost2 = new Dropzone("#dz-post");
    myDropzonePost2.on("addedfile", function (file) {
        /* Maybe display some more file information on your page */
        width = 1;
        move();
        document.getElementById("pdfloadingAccept").style.display = "block";
        document.getElementById("gnNote").innerHTML = "ATTACHING FILE";
        if (file.type != "image/jpeg" && file.type != "image/png" && file.type != "application/pdf") {
            showAlert("Kindly provided a JPEG , PNG Image or PDF for upload");
            this.removeFile(file);
            document.getElementById("pdfloadingAccept").style.display = "none";
        }
        else {
            var data = new FormData();
            data.append(file.name, file);
            jQuery.ajax({
                url: "../Handlers/MobileIncidentUpload.ashx",
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function (result) {
                    document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|")
                    jQuery.ajax({
                        type: "POST",
                        url: "TaskDash.aspx/attachFileToInci",
                        data: "{'id':'" + document.getElementById('rowidChoice').text + "','filepath':'" + document.getElementById("imagePostAttachment").text + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showAlert(data.d);
                                showIncidentDocument(document.getElementById('rowidChoice').text);
                                document.getElementById("pdfloadingAccept").style.display = "none";
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                },
                error: function (err) {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            })
        }
    });

    var myDropzoneTicket = new Dropzone("#dz-postticket");
    myDropzoneTicket.on("addedfile", function (file) { 

        width = 1;
        moveTick();
        document.getElementById("pdfloadingAcceptTick").style.display = "block";
        document.getElementById("gnNoteTick").innerHTML = "ATTACHING FILE";

        var data = new FormData();
        data.append(file.name, file);
        jQuery.ajax({
            url: "../Handlers/MobileIncidentUpload.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|")
                jQuery.ajax({
                    type: "POST",
                    url: "TaskDash.aspx/attachFileToTicket",
                    data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','filepath':'" + document.getElementById("imagePostAttachment").text + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showAlert(data.d);
                            rowchoiceTicket(document.getElementById('rowidChoiceTicket').value);
                            document.getElementById("pdfloadingAcceptTick").style.display = "none";
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            },
            error: function (err) {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        }) 
    });

    var myDropzone2 = new Dropzone("#dz-upload");
    myDropzone2.on("addedfile", function (file) {
        /* Maybe display some more file information on your page */
        file.previewElement.addEventListener("click", function () {
            myDropzone2.removeFile(file);
            document.getElementById("mobimagePath2").text = '';
        });
        if (typeof document.getElementById("mobimagePath2").text === 'undefined' || document.getElementById("mobimagePath2").text == '') {
            if (file.type != "image/jpeg" && file.type != "image/png") {
                showAlert("Kindly provided a JPEG or PNG Image for upload");
                this.removeFile(file);
            }
            else {
                var data = new FormData();
                data.append(file.name, file);
                jQuery.ajax({
                    url: "Handlers/MobileIncidentUpload.ashx",
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        document.getElementById("mobimagePath2").text = result;
                        //myDropzone.removeAllFiles(true);
                    },
                    error: function (err) {
                    }
                });
            }
        }
        else {
            showAlert("You can only add one image at a time kindly delete previous image");
            this.removeFile(file);
        }
    });

    //currentlocation = "U.A.E";

    jQuery('#taskDocument').on('shown.bs.modal', function () {
        if(firstpresstask == false)
        {
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTaskLocationData",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        var obj = $.parseJSON(data.d)
                        getTaskLocationMarkers(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
            firstpresstask = true;
        }
        else
        {
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTaskLocationData",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        var obj = $.parseJSON(data.d)
                        updateTaskMarker(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
    });

    jQuery('#ticketingViewCard').on('shown.bs.modal', function () {
        jQuery.ajax({
            type: "POST",
            url: "TaskDash.aspx/getIncidentLocationData",
            data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    var obj = jQuery.parseJSON(data.d)
                    getTicketLocationMarkers(obj);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    });

    jQuery('#viewDocument1').on('shown.bs.modal', function () {
        if (incidentfirstpressTaskChoice == false) {
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getIncidentLocationData",
                data: "{'id':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        var obj = jQuery.parseJSON(data.d)
                        getIncidentLocationMarkers(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
            incidentfirstpressTaskChoice = true;
        }
        else {
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getIncidentLocationData",
                data: "{'id':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        var obj = jQuery.parseJSON(data.d)
                        updateIncidentMarker(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
    });
    getonlineuserspercentage();
   // addreminders();
   // getReminderSelectorDates();
   // addrowtoTableTeamTasks();
  //  recentActivity2();
   // showTaskingCharts('task');

    getEventStatusTotal();

});
        var mapIncidentLocation;
        function getTicketLocationMarkers(obj) {


            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 15,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapIncidentLocation = new google.maps.Map(document.getElementById("map_canvasTicketLocation"), mapOptions);

            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div id="content">' + obj[i].Username +
                '<br/></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                marker.setIcon('../Images/marker.png')
                myMarkersIncidentLocation[obj[i].Username] = marker;
                createInfoWindowIncidentLocation(marker, contentString);
            }
        }
        function createInfoWindowIncidentLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindowIncidentLocation.setContent(popupContent);
                infoWindowIncidentLocation.open(mapIncidentLocation, this);
            });
        }
function getTaskLocationMarkers(obj) {
    //  navigator.geolocation.getCurrentPosition(function (position) {
    //     sourceLat = position.coords.latitude;
    //     sourceLon = position.coords.longitude;

    locationAllowed = true;
    //setTimeout(function () {
    google.maps.visualRefresh = true;

    var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

    // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
    var mapOptions = {
        zoom: 15,
        center: Liverpool,
        mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
    };

    // This makes the div with id "map_canvas" a google map
    mapTaskLocation = new google.maps.Map(document.getElementById("taskmap_canvasIncidentLocation"), mapOptions);
    var first = true;
    var poligonCoords = [];
    var tbcounter = 0;
    var pincounter = 0;
    for (var i = 0; i < obj.length; i++) {

        var contentString = '<div id="content">' + obj[i].Username +
        '<br/></div>';

        var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    
        if (obj[i].Username == "Pending") {
            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
            marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
            //myMarkersTasksLocation[obj[i].Username] = marker;
            
            myMarkersTasksLocation[pincounter] = marker;
            pincounter++;
            
            createInfoWindowTaskLocation(marker, contentString);
        }
        else if (obj[i].Username == "InProgress") {
            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
            //myMarkersTasksLocation[obj[i].Username] = marker;
            myMarkersTasksLocation[pincounter] = marker;
            pincounter++;
            createInfoWindowTaskLocation(marker, contentString);
        }
        else if (obj[i].Username == "OnRoute") {
            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
            marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
            //myMarkersTasksLocation[obj[i].Username] = marker;
            myMarkersTasksLocation[pincounter] = marker;
            pincounter++;
            createInfoWindowTaskLocation(marker, contentString);
        }
        else if (obj[i].Username == "Completed") {
            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
            if (obj[i].State == "GREEN") {
                marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
            }
            else {
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerX.png');
            }
            //myMarkersTasksLocation[obj[i].Username] = marker;
            myMarkersTasksLocation[pincounter] = marker;
            pincounter++;
            createInfoWindowTaskLocation(marker, contentString);
        }
        else if (obj[i].Username == "Accepted") {
            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
            marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
            //myMarkersTasksLocation[obj[i].Username] = marker;
            myMarkersTasksLocation[pincounter] = marker;
            pincounter++;
            createInfoWindowTaskLocation(marker, contentString);
        }
        else if (obj[i].State == "RED") {
            if (first) {
                first = false;
                document.getElementById('previousTaskUser').text = obj[i].Username;
                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                poligonCoords.push(point);

            }
            else {
                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                poligonCoords.push(point);
                var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].LastLog });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                myTraceBackMarkers[tbcounter] = marker;
                tbcounter++;
            }
        }
        else {
            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                   
            marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
            //myMarkersTasksLocation[obj[i].Username+obj[i].Id] = marker;
            myMarkersTasksLocation[pincounter] = marker;
            pincounter++;
            createInfoWindowTaskLocation(marker, contentString);
        }
    }
    //  });
}

function createInfoWindowTaskLocation(marker, popupContent) {
    google.maps.event.addListener(marker, 'click', function () {
        infoWindowTaskLocation.setContent(popupContent);
        infoWindowTaskLocation.open(mapTaskLocation, this);
    });
}
function fromReminderDateChange()
{
    var select = document.getElementById("toReminderDate");
    document.getElementById("toReminderDate").options.length = 0;
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getDateRangeReminder",
        data: "{'date':'" + document.getElementById('fromReminderDate2').value+ "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d[0] == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                for (var i = 0; i < data.d.length; i++) {
                    var opt = document.createElement('option');
                    opt.value = data.d[i];
                    opt.innerHTML = data.d[i];
                    select.appendChild(opt);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}
function getReminderSelectorDates()
{
    var select1 = document.getElementById('fromReminderDate2');
    var select2 = document.getElementById('toReminderDate');
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getReminderSelectorDates",
        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d[0] == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                for (var i = 0; i < data.d.length; i++) {
                    var opt = document.createElement('option');
                    opt.value = data.d[i];
                    opt.innerHTML = data.d[i];
                    select1.appendChild(opt);
                    var opt2 = document.createElement('option');
                    opt2.value = data.d[i];
                    opt2.innerHTML = data.d[i];
                    select2.appendChild(opt2);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}

 
function RejectionHandle()
{
    document.getElementById("rejectionTextarea").style.display = "block";
    document.getElementById("taskinitialOptionsDiv").style.display = "none";
    document.getElementById("handleOptionsDiv").style.display = "none";
    document.getElementById("completedOptionsDiv").style.display = "none";
    document.getElementById("dispatchOptionsDiv").style.display = "none";
    document.getElementById("rejectOptionsDiv").style.display = "block";
}

function getAssigneeType(stringVal)
{
    var output = "";
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getAssigneeType",
        data: "{'id':'" + stringVal + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            output =  data.d;
        }
    });
    return output;
}
function IncidentStateChange(state) {

    var id = document.getElementById("rowidChoice").text;
    var ins = document.getElementById("selectinstructionTextarea").value;
    var isErr = false;
    if (state == "Reject") {
        ins = document.getElementById("incidentrejectionTextarea").value;

        if (isEmptyOrSpaces(ins)) {
            ins = document.getElementById("resolutionTextarea").value;
        }
    }
    if (state == "Resolve") {
        ins = document.getElementById("resolutionTextarea").value;
    }
    if (!isEmptyOrSpaces(ins)) {
        if (isSpecialChar(ins)) {
            isErr = true;
            showAlert("Kindly remove special character from text area");
        }
    }
    if (!isErr) {
        var imgPath = document.getElementById("mobimagePath2").text;
        uploadattachment(id, imgPath);
        $.ajax({
            type: "POST",
            url: "TaskDash.aspx/changeIncidentState",
            data: "{'id':'" + id + "','state':'" + state + "','ins':'" + ins + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "") {
                    showAlert("Error changing incident state");
                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (state != "Dispatch") {
                    jQuery('#incidentDocument').modal('hide');
                    document.getElementById('successincidentScenario').innerHTML = data.d + " status has been changed to " + state;
                    jQuery('#successfulDispatch').modal('show');
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
            }
        });
    }
}
function insertNewTask(id, assigneetype, assigneename, assigneeid, templatename, longi, lati, incidentId) {
    var output = "";
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/inserttask",
        data: "{'id':'" + id + "','assigneetype':'" + assigneetype + "','assigneename':'" + assigneename + "','assigneeid':'" + assigneeid + "','templatename':'" + templatename + "','longi':'" + longi + "','lati':'" + lati + "','incidentId':'" + incidentId+ "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else
                output = data.d;
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
    return output;
}
function insertNewIncident(name, desc, locationid, incidenttype, notificationid, taskid, receivedby, longi, lati, status, ins, msgtask, incidentId, isrej, remarks) {
    var output = "";
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/insertNewIncident",
        data: "{'name':'" + name + "','desc':'" + desc + "','locationid':'" + locationid + "','incidenttype':'" + incidenttype + "','notificationid':'" + notificationid + "','taskid':'" + taskid + "','receivedby':'" + receivedby + "','longi':'" + longi + "','lati':'" + lati + "','status':'" + status + "','instructions':'" + ins + "','msgtask':'" + msgtask + "','incidentId':'" + incidentId + "','isrej':'" + isrej + "','remarks':'" + remarks + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else
                output = data.d;
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
    return output;
}
function getLocationMarkers(obj) {
    try
    {
        //   navigator.geolocation.getCurrentPosition(function (position) {
        //        sourceLat = position.coords.latitude;
        //       sourceLon = position.coords.longitude;

        locationAllowed = true;
        //setTimeout(function () {
        google.maps.visualRefresh = true;
        var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

        for (var i = 0; i < obj.length; i++) {

            if (obj[i].State == "PURPLE") {
                Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
            }
        }

        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 8,
            center: Liverpool,
            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
        };

        // This makes the div with id "map_canvas" a google map
        mapLocation = new google.maps.Map(document.getElementById("taskmap_canvasIncidentLocation"), mapOptions);
        var poligonCoords = [];
        var first = true;
        for (var i = 0; i < obj.length; i++) {

            var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"><a id="' + obj[i].Id + obj[i].Username + '" href="#" class="red-borders light-button red-color" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color"><i class="fa fa-plus  red-color"></i>ADD</a></div>';
            //'<div class="map-container inline-block"><div class="map-popup"><div class="help-block text-center pt-2x"><i class="fa fa-mobile"></i><p class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"><a id="' + obj[i].Id + obj[i].Username + '" href="#" class="red-borders light-button red-color" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div></div><div class="map-selector"></div></div> ';
            var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    
            if (obj[i].State == "YELLOW") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                createInfoWindowLocation(marker, contentString);
            }
            else if (obj[i].State == "GREEN") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                createInfoWindowLocation(marker, contentString);
            }
            else if (obj[i].State == "BLUE") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                createInfoWindowLocation(marker, contentString);
            }
            else if (obj[i].State == "OFFUSER") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                createInfoWindowLocation(marker, contentString);
            }
            else if (obj[i].State == "OFFCLIENT") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                createInfoWindowLocation(marker, contentString);
            }
            else if (obj[i].State == "PURPLE") {
                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                contentString = '<div class="help-block text-center pt-2x"><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">'+ obj[i].Username + '</p></div><div class="help-block text-center"></div>';
                myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                createInfoWindowLocation(marker, contentString);
                document.getElementById("rowIncidentName").text = obj[i].Username+obj[i].Id;  
            }
            else if (obj[i].State == "RED") {
                if (first) {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    myMarkersLocation[obj[i].Username+obj[i].Id] = marker;
                    createInfoWindowLocation(marker, contentString);
                    document.getElementById("rowIncidentName").text = obj[i].Username+obj[i].Id;  
                    first = false;
                }
                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                poligonCoords.push(point);
            }

        }
        if (poligonCoords.length > 0) {
            //var poligon = new google.maps.Polygon({
            //    map: mapLocation,
            //    paths: poligonCoords,
            //    strokeColor: '#800000',
            //    strokeOpacity: 0.5,
            //    strokeWeight: 3,
            //    fillColor: '#800000',
            //    fillOpacity: 0.35
            //});
            updatepoligon = new google.maps.Polyline({
                path: poligonCoords,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            updatepoligon.setMap(mapLocation);
        }
        //}, 1000);

        //  });
    }catch(err)
    {showAlert(err)}
}
function createInfoWindowLocation(marker, popupContent) {
    google.maps.event.addListener(marker, 'click', function () {
        infoWindowLocation.setContent(popupContent);
        infoWindowLocation.open(mapLocation, this);
    });
}
function play(i) {
    try {
        var player = document.getElementById('Video' + (i + 1));
        player.play();
    } catch (error) {
    }
}
var tracebackpoligon;


function nextMapLiClick() {
    document.getElementById("nextLi").style.display = "block";
    document.getElementById("finishLi").style.display = "none";
    var name = document.getElementById("rowidChoice").text;
    jQuery.ajax({
        type: "POST",
        url: "TaskDash.aspx/getGPSDataUsers",
        data: "{'id':'" + name+ "','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.d == "LOGOUT"){
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }else{
                var obj = jQuery.parseJSON(data.d)
                updateIncidentMarker(obj);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
        }
    });
}
function setMapOnAll(obj,Markers) {
    try {
        for (var i = 0; i < obj.length; i++) {
            if (Markers[obj[i].Username+obj[i].Id] != null) {
                Markers[obj[i].Username+obj[i].Id].setMap(null);
            }
        }
		myMarkers = new Array();
    }
    catch (err) {
        showAlert(err);
    }
}
var firstincidentpress = false;
function addnametoUserDispatchList(name) {
    var ul = document.getElementById("UsersToDispatchList");
    var li = document.createElement("li");
    li.setAttribute("id", "li-" + name);
    li.innerHTML = '<a href="#" onclick="usersliOnclickRemove(&apos;' + name + '&apos;)" class="capitalize-text">' + name + '<i class="fa fa-close"></i></a>';
    ul.appendChild(li);
}
function dispatchUserchoiceTable(id, name) {
    var element = document.getElementById(id + name);
    var result = element.innerHTML.indexOf("ADDED");
    if (result < 0) {

        var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
        if (exists == false) {
            var myOption;
            myOption = document.createElement("Option");
            myOption.text = name; //Textbox's value
            myOption.value = id; //Textbox's value
            sendToListBox.add(myOption);
            document.getElementById("<%=tbUserID.ClientID%>").value = document.getElementById("<%=tbUserID.ClientID%>").value + '-' + id;
            document.getElementById("<%=tbUserName.ClientID%>").value = document.getElementById("<%=tbUserName.ClientID%>").value + '-' + name;
            addnametoUserDispatchList(name);
        }
        element.style.color = "#3ebb64";
        element.className = "green-color";
        element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
    }
    else {
        var elSel = document.getElementById('sendToListBox');
        var i;
        for (i = elSel.length - 1; i >= 0; i--) {
            if (elSel.options[i].value == id) {
                var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                      var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                      document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                      document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                      removenameFromDispatchList(elSel.options[i].text);
                      elSel.remove(i);
                  }
              }
                
              element.style.color = "#b2163b";
              element.className = "red-color";
              element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
          }
      }
      function usersliOnclickRemove(name) {
          removeFromList(name);
          removenameFromDispatchList(name);
          var thismarker = myMarkersLocation[name];
          thismarker.infoWindowLocation.close();
      }
      function removenameFromDispatchList(name) {
          var element = document.getElementById("li-" + name);
          element.parentNode.removeChild(element);
      }
      function removeFromList(name) {
          var elSel = document.getElementById('sendToListBox');
          var i;
          for (i = elSel.length - 1; i >= 0; i--) {
              if (elSel.options[i].text == name) {
                  var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                  var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                  document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                  document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                  elSel.remove(i);
              }
          }
      }
      function userdispatchChoice(id, name) {
          var element = document.getElementById(id + name);
          var result = element.innerHTML.indexOf("ADDED");
          if (result < 0) {

              var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
              if (exists == false) {
                  var myOption;
                  myOption = document.createElement("Option");
                  myOption.text = name; //Textbox's value
                  myOption.value = id; //Textbox's value
                  sendToListBox.add(myOption);
                  document.getElementById("<%=tbUserID.ClientID%>").value = document.getElementById("<%=tbUserID.ClientID%>").value + '-' + id;
                  document.getElementById("<%=tbUserName.ClientID%>").value = document.getElementById("<%=tbUserName.ClientID%>").value + '-' + name;
                  addnametoUserDispatchList(name);
              }
              element.style.color = "#3ebb64";
              element.className = "green-borders light-button green-color";
              element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
          }
          else {
              var elSel = document.getElementById('sendToListBox');
              var i;
              for (i = elSel.length - 1; i >= 0; i--) {
                  if (elSel.options[i].value == id) {
                      var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                      var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                      document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                      document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                      removenameFromDispatchList(elSel.options[i].text);
                      elSel.remove(i);
                  }
              }
                
              element.style.color = "#b2163b";
              element.className = "red-borders light-button red-color";
              element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
          }
      }
      function assignUserTableData() {

          jQuery.ajax({
              type: "POST",
              url: "TaskDash.aspx/getAssignUserTableData",
              data: "{'id':'0','uname':'" + loggedinUsername + "'}",
              async: false,
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  if(data.d[0] == "LOGOUT"){
                      showError("Session has expired. Kindly login again.");
                      setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                  }else{
                      for (var i = 0; i < data.d.length; i++) {
                          jQuery("#assignUsersTable tbody").append(data.d[i]);

                      }
                  }
              },
              error: function () {
                  showError("Session timeout. Kindly login again.");
                  setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
              }
          });
      }
      function infotabDefault() {
          var el = document.getElementById('activity-tab');
          if (el) {
              el.className = 'tab-pane fade ';
          }
          var el3 = document.getElementById('info-tab');
          if (el3) {
              el3.className = 'tab-pane fade active in';
          }
          var el4 = document.getElementById('attachments-tab');
          if (el4) {
              el4.className = 'tab-pane fade';
          }
          var el2 = document.getElementById('liInfo');
          if (el2) {
              el2.className = 'active';
          }
          var el5 = document.getElementById('liActi');
          if (el5) {
              el5.className = ' ';
          }
          var el6 = document.getElementById('liAtta');
          if (el6) {
              el6.className = ' ';
          }
      }
      function cleardispatchList() {
          try {
              var elSel = document.getElementById('sendToListBox');
              var i;
              for (i = elSel.length - 1; i >= 0; i--) {
                  var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                    var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                    document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                    document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                    var element = document.getElementById("li-" + elSel.options[i].text);
                    if (element) {
                        element.parentNode.removeChild(element);
                        elSel.remove(i);
                    }
                }
            }
            catch (err) {
                alert(err);
            }
        } 
        function insertAttachmentIcons(id) {
            jQuery('#taskdivAttachment div').html('');
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getAttachmentDataIcons",
                data: "{'id':'" + id+ "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        //for (var i = 0; i < data.d.length; i++) {
                        document.getElementById("taskdivAttachment").innerHTML = data.d;
                        //}
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function insertAttachmentTabData(id)
        {
            jQuery('#taskattachments-info-tab div').html('');
            
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getAttachmentDataTab",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        document.getElementById("taskattachments-info-tab").innerHTML = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }

        var imgcount = 0;

        function incinextImg() {
            var found = false;
            for (var i = 0; i < incidentdivArray.length; i++) {
                var el = document.getElementById(incidentdivArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i + 1 < imgcount) {
                        var el3 = document.getElementById(incidentdivArray[i + 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("location-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (incidentdivArray.length > 0) {
                    var ell = document.getElementById(incidentdivArray[0]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("location-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }
        var incidentdivArray = new Array();
        function incibackImg() {
            var found = false;
            for (var i = 0; i < incidentdivArray.length; i++) {
                var el = document.getElementById(incidentdivArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i == 0) {
                        var elz = document.getElementById("location-tab");
                        if (elz) {
                            elz.className = 'tab-pane fade active in';
                        }
                    }
                    else if (i - imgcount < imgcount) {
                        var el3 = document.getElementById(incidentdivArray[i - 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("location-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (incidentdivArray.length > 0) {
                    var ell = document.getElementById(incidentdivArray[imgcount - 1]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("location-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }

        function nextImg() {
            var found = false;
            hideTaskplay();
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i + 1 < imgcount) {
                        var el3 = document.getElementById(divArray[i + 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("tasklocation-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (divArray.length > 0) {
                    var ell = document.getElementById(divArray[0]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("tasklocation-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }
        function backImg() {
            hideTaskplay();
            var found = false;
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i == 0) {
                        var elz = document.getElementById("tasklocation-tab");
                        if (elz) {
                            elz.className = 'tab-pane fade active in';
                        }
                    }
                    else if (i - imgcount < imgcount) {
                        var el3 = document.getElementById(divArray[i - 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("tasklocation-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (divArray.length > 0) {
                    var ell = document.getElementById(divArray[imgcount - 1]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("tasklocation-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }

        function ticketnextImg() {
            var found = false;
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i + 1 < imgcount) {
                        var el3 = document.getElementById(divArray[i + 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("ticketlocation-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (divArray.length > 0) {
                    var ell = document.getElementById(divArray[0]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("ticketlocation-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }
        function ticketbackImg() {
            var found = false;
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i == 0) {
                        var elz = document.getElementById("ticketlocation-tab");
                        if (elz) {
                            elz.className = 'tab-pane fade active in';
                        }
                    }
                    else if (i - imgcount < imgcount) {
                        var el3 = document.getElementById(divArray[i - 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("ticketlocation-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (divArray.length > 0) {
                    var ell = document.getElementById(divArray[imgcount - 1]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("ticketlocation-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }       
        function nextbackTask() {
            document.getElementById("rotationDIV1").style.display = "block";
            document.getElementById("rotationDIV2").style.display = "block";
        }

        function insertAttachmentData(id) {
            document.getElementById("rotationDIV1").style.display = "none";
            document.getElementById("rotationDIV2").style.display = "none";
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getAttachmentData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        imgcount = 0;
                        document.getElementById('taskAudioDIV').style.display="none";
                        for (var i = 0; i < data.d.length; i++) {
                            if (data.d[i].indexOf("video") >= 0) {
                                var div = document.createElement('div');
                                div.className = 'tab-pane fade';
                                div.innerHTML = data.d[i];
                                div.id = 'video-' + (i + 1) + '-tab';
                                document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                divArray[i] = 'video-' + (i + 1) + '-tab';
                                imgcount++;
                            }
                            else {
                                var div = document.createElement('div');
                                div.className = 'tab-pane fade';
                                div.align = 'center';
                                div.style.height = '380px';
                                div.innerHTML = data.d[i];
                                div.id = 'image-' + (i + 1) + '-tab';
                                document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                divArray[i] = 'image-' + (i + 1) + '-tab';
                                imgcount++;
                            }
                        }
                        if (imgcount > 0) {
                            document.getElementById("rotationDIV1").style.display = "block";
                            document.getElementById("rotationDIV2").style.display = "block";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function oldDivContainers() {
            try
            {
                for (var i = 0; i < divArray.length; i++) {
                    var el = document.getElementById(divArray[i]);
                    el.parentNode.removeChild(el);
                }
                divArray = new Array();
            }
            catch(ex)
            {
                //alert(ex);
            }
        }
        function taskHistoryData(id)
        {
            jQuery('#taskdivIncidentHistoryActivity div').html('');
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getEventHistoryData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('taskdivIncidentHistoryActivity').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function resolveClick() {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";

            document.getElementById("completedOptionsDiv2").style.display = "block";
            document.getElementById("resolutionTextarea").style.display = "block";
        }
        function redispatch() {
            var id = document.getElementById("rowidChoice").text;
            var ins = document.getElementById("rejectionTextarea").value;
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/rejectReDispatch",
                data: "{'id':'" + id + "','ins':'" + ins + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "") {
                        showAlert("Error redispatching incident");
                    }
                    else {
                        document.getElementById('successincidentScenario').innerHTML = data.d;
                        jQuery('#successfulDispatch').modal('show');
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function handledClick() {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "block";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";

            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
        }
        function nextLiClick() {
            document.getElementById("nextLi").style.display = "block";
            document.getElementById("finishLi").style.display = "none";
        }
        function finishLiClick() {
            document.getElementById("finishLi").style.display = "block";
            document.getElementById("nextLi").style.display = "none";
        }
        function assignrowData(id) {
            var output = "";
            document.getElementById('rejectionTextarea').innerHTML = "";
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTableRowData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        document.getElementById("taskusernameSpan").innerHTML = data.d[1];
                        document.getElementById("tasktimeSpan").innerHTML = data.d[2];
                        document.getElementById("tasktypeSpan").innerHTML = data.d[3];
                        document.getElementById("taskstatusSpan").innerHTML = data.d[4];
                        document.getElementById("tasklocSpan").innerHTML = data.d[5];
                        document.getElementById("taskdescriptionSpan").innerHTML = data.d[9];
                        document.getElementById("taskinstructionSpan").innerHTML = data.d[10];
                        document.getElementById("taskincidentNameHeader").innerHTML = data.d[11];
                        document.getElementById("assignedTimeSpan").innerHTML = data.d[12];
                        document.getElementById("checklistNotesSpan").innerHTML = data.d[13];
                        document.getElementById("checklistnameSpan").innerHTML = data.d[15];

                        var el = document.getElementById('headerImageClass');
                        if (el) {
                            el.className = data.d[14];
                        }
                        output = data.d[0];
                        document.getElementById('pRejectionNotes').style.display = "none";
                        //document.getElementById('acceptLiHandle').style.display = data.d[16];
                    
                        if (data.d[17] != "N-A") {
                            document.getElementById('pRejectionNotes').style.display = "block";
                            document.getElementById('rejectionListNotes').innerHTML = data.d[17];
                        
                        }
                        document.getElementById('ttypeSpan').innerHTML = data.d[18];

                        document.getElementById("incidentItemsList").innerHTML = "";
                        document.getElementById('incidentItemsListDIV').style.display = "none";

                        var res = data.d[19].split("|");
                        if (res.length > 0) {
                            if (res[0] != "None") {
                                if (res[0] == "Incident")
                                    document.getElementById("incidentItemsList").innerHTML = res[0] + ': <a style="color:#b2163b;" href="#viewDocument1"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="showIncidentDocument(&apos;' + res[2] + '&apos;)">' + res[1] + '</a>';
                                else
                                    document.getElementById("incidentItemsList").innerHTML = res[0] + ': <a style="color:#b2163b;" href="#ticketingViewCard"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="rowchoiceTicket(&apos;' + res[2] + '&apos;)">' + res[1] + '</a>';
                                document.getElementById('incidentItemsListDIV').style.display = "block";
                            }
                        }

                        var res2 = data.d[20].split("|");
                        document.getElementById("linkparent").innerHTML = "";
                        document.getElementById('linkparentDIV').style.display = "none"; 

                        if (res2.length > 0) {
                            if (res2[0] != "None") {
                                document.getElementById("linkparent").innerHTML = 'Main-Task: <a style="color:#b2163b;" href="#"  href="#taskDocument" data-toggle="modal" data-dismiss="modal" class="capitalize-text" onclick="showTaskDocument(&apos;' + res2[1] + '&apos;);linkChoiceView(&apos;' + id + '&apos;);">' + res2[0] + '</a>';
                                document.getElementById('linkparentDIV').style.display = "block"; 
                            }
                        }

                        document.getElementById("CustomerNameSpan").innerHTML = "";
                        document.getElementById("SystemTypeSpan").innerHTML = "";
                        document.getElementById("ProjectNameSpan").innerHTML = "";
                        document.getElementById("ContractNameSpan").innerHTML = "";

                        document.getElementById('dvCustomerNameSpan').style.display = "none";
                        document.getElementById('dvSystemTypeSpan').style.display = "none";
                        document.getElementById('dvProjectNameSpan').style.display = "none";
                        document.getElementById('dvContractNameSpan').style.display = "none";

                        //NEWCUSTOMER
                        if (data.d.length > 20) {
                            if (data.d[21] != "N/A") {
                                document.getElementById("CustomerNameSpan").innerHTML = data.d[21];
                                document.getElementById('dvCustomerNameSpan').style.display = "block";
                            }
                            if (data.d[22] != "N/A") {
                                document.getElementById("SystemTypeSpan").innerHTML = data.d[22];
                                document.getElementById('dvSystemTypeSpan').style.display = "block";
                            }
                            if (data.d[23] != "N/A") {
                                document.getElementById("ProjectNameSpan").innerHTML = data.d[23];
                                document.getElementById('dvProjectNameSpan').style.display = "block";
                            }
                            if (data.d[24] != "N/A") {
                                document.getElementById("ContractNameSpan").innerHTML = data.d[24];
                                document.getElementById('dvContractNameSpan').style.display = "block";
                            }
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
            return output;
        }
        function getTasklistItems(id) {
            document.getElementById("taskItemsList").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTaskListData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        document.getElementById("taskItemsnameSpan").innerHTML = data.d[0];
                        for (var i = 1; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("taskItemsList");
                            var li = document.createElement("li");
                            li.innerHTML = '<a href="#taskDocument"  data-toggle="modal"   class="capitalize-text" onclick="showTaskDocument(&apos;' + res[1] + '&apos;)">' + res[0] + '</a>';
                            //li.appendChild(document.createTextNode(data.d[i]));
                            ul.appendChild(li);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        var firstpresstask = false;
        var updatepoligon;
        var myTraceBackMarkers = new Array();
        Object.size = function (obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
        function updateTaskMarker(obj)
        {
            try
            {
                var poligonCoords = [];
                var first = true;
                var tbcounter = 0;
                for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                    if (myTraceBackMarkers[i] != null) {
                        myTraceBackMarkers[i].setMap(null);
                    }
                }
                if (typeof updatepoligon === 'undefined') {
                    // your code here.
                }
                else {
                    updatepoligon.setMap(null);
                }
                //if(typeof myMarkersTasksLocation["Pending"] === 'undefined'){
                //    // your code here.
                //}
                //else
                //{  
                //    myMarkersTasksLocation["Pending"].setMap(null);
                //}
                //if(typeof myMarkersTasksLocation["Completed"] === 'undefined'){
                //    // your code here.
                //}
                //else
                //{  
                //    myMarkersTasksLocation["Completed"].setMap(null);
                //}
                //if(typeof myMarkersTasksLocation["Accepted"] === 'undefined'){
                //    // your code here.
                //}
                //else
                //{  
                //    myMarkersTasksLocation["Accepted"].setMap(null);
                //}
                //if(typeof myMarkersTasksLocation["InProgress"] === 'undefined'){
                //    // your code here.
                //}
                //else
                //{  
                //    myMarkersTasksLocation["InProgress"].setMap(null);
                //}
                //if(typeof myMarkersTasksLocation["Cancelled"] === 'undefined'){
                //    // your code here.
                //}
                //else
                //{  
                //    myMarkersTasksLocation["Cancelled"].setMap(null);
                //}
                //if (typeof myMarkersTasksLocation["Accepted"] === 'undefined') {
                //    // your code here.
                //}
                //else {
                //    myMarkersTasksLocation["Accepted"].setMap(null);
                //}
                for (var i = 0; i < Object.size(myMarkersTasksLocation) ; i++) {
                    if (myMarkersTasksLocation[i] != null) {
                        myMarkersTasksLocation[i].setMap(null);
                    }
                }
                var pincounter = 0;
                for (var i = 0; i < obj.length; i++) {
                    var contentString = '<div id="content">' + obj[i].Username + '<br/></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    mapTaskLocation.setCenter(new google.maps.LatLng(obj[i].Lat, obj[i].Long));
                    mapTaskLocation.setZoom(9);
                    if (obj[i].Username == "Pending") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username + "\n" + obj[i].LastLog});
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        //myMarkersTasksLocation[obj[i].Username] = marker;
                        myMarkersTasksLocation[pincounter] = marker;
                        pincounter++;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "InProgress") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username + "\n" + obj[i].LastLog});
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                        //myMarkersTasksLocation[obj[i].Username] = marker;
                        myMarkersTasksLocation[pincounter] = marker;
                        pincounter++;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "OnRoute") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username + "\n" + obj[i].LastLog});
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
                        //myMarkersTasksLocation[obj[i].Username] = marker;
                        myMarkersTasksLocation[pincounter] = marker;
                        pincounter++;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "Completed") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username + "\n" + obj[i].LastLog});
                        if (obj[i].State == "GREEN") {
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                        }
                        else {
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerX.png');
                        }
                        //myMarkersTasksLocation[obj[i].Username] = marker;
                        myMarkersTasksLocation[pincounter] = marker;
                        pincounter++;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "Accepted") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username + "\n" + obj[i].LastLog});
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                        //myMarkersTasksLocation[obj[i].Username] = marker;
                        myMarkersTasksLocation[pincounter] = marker;
                        pincounter++;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].State == "RED") {
                        if (first) {
                            first = false;
                            document.getElementById('previousTaskUser').text = obj[i].Username;
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);
                        }
                        else {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].LastLog });
                            marker.setIcon(obj[i].Marker);
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                        }
                    }
                    else {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        //myMarkersTasksLocation[obj[i].Username+obj[i].Id] = marker;
                        myMarkersTasksLocation[pincounter] = marker;
                        pincounter++;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                }
                if (poligonCoords.length > 0) {
                    updatepoligon = new google.maps.Polyline({
                        path: poligonCoords,
                        geodesic: true,
                        strokeColor: '#1b93c0',
                        strokeOpacity: 1.0,
                        strokeWeight: 7,
                        icons: [{
                            icon: iconsetngs, 
                            offset: '100%'
                        }]
                    });
                    updatepoligon.setMap(mapTaskLocation);
                    animateCircle(updatepoligon);
                }
            }
            catch(err)
            {
                showAlert(err);
            }
        }

        function getSubTasklistItems(id) {
            document.getElementById("taskSubList").innerHTML = "";
            document.getElementById('taskSubDIV').style.display = 'none';
            document.getElementById('legendDIV').style.display = 'none';
            document.getElementById('backLinkDisplay1').style.display = 'none';
            document.getElementById('backLinkDisplay2').style.display = 'none';
            document.getElementById('backLinkDisplay3').style.display = 'none';
            document.getElementById('backLinkDisplay4').style.display = 'none'; 
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getSubTaskListData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        document.getElementById('taskSubDIV').style.display = 'block'; 
                        document.getElementById('legendDIV').style.display = 'block'; 
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("taskSubList");
                        var li = document.createElement("li");
                        var colorRet = 'green';
                        if (res[3] == "Pending") {
                            colorRet = 'red';
                        }
                        else if (res[3] == "InProgress") {
                            colorRet = 'yellow';
                        }
                        var c = (i + 1);
                        if (c == 1) {
                            li.innerHTML = c + '. <i class="fa fa-circle  ' + colorRet + '-color"></i><a style="margin-left:5px;" href="#"  href="#"   class="capitalize-text" onclick="showTaskDocument(&apos;' + res[2] + '&apos;);linkChoiceView(&apos;' + id + '&apos;);" >' + res[0] + '</a>';
                        }
                        else
                            li.innerHTML = c + '.<i style="margin-left:1px;" class="fa fa-circle  ' + colorRet + '-color"></i><a style="margin-left:5px;" href="#"  href="#"  class="capitalize-text" onclick="showTaskDocument(&apos;' + res[2] + '&apos;);linkChoiceView(&apos;' + id + '&apos;);" >' + res[0] + '</a>';

                        ul.appendChild(li);
                    }
                }
            });
        }
        function linkChoiceView(id) {
            document.getElementById('backLinkDisplay1').style.display = 'block';
            document.getElementById('backLinkDisplay2').style.display = 'block';
            document.getElementById('backLinkDisplay3').style.display = 'block';
            document.getElementById('backLinkDisplay4').style.display = 'block';
            document.getElementById("rowChoiceTasksLink").value = id;

            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTaskLocationData",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d)
                        updateTaskMarker(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        function gettaskRemarks(id) {
            jQuery('#taskRemarksList div').html('');
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTaskRemarksData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('taskRemarksList').appendChild(div);
                    }
                }
            });
        }
        function hideAllRemarks() {

            document.getElementById("rotationDIV1").style.display = "block";
            document.getElementById("rotationDIV2").style.display = "block";

            var el2 = document.getElementById('tasklocation-tab');
            if (el2) {
                el2.className = 'tab-pane fade active in';
            }
            var el = document.getElementById('tremarks-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
        }
        function showAllRemarks(id) {

            document.getElementById("rotationDIV1").style.display = "none";
            document.getElementById("rotationDIV2").style.display = "none";

            if (document.getElementById('taskrejection-tab').className == "tab-pane fade active in") {

            }
            else {
                var el2 = document.getElementById('tasklocation-tab');
                if (el2) {
                    el2.className = 'tab-pane fade';
                }
                var el = document.getElementById('tremarks-tab');
                if (el) {
                    el.className = 'tab-pane fade active in';
                }

                for (var i = 0; i < divArray.length; i++) {
                    var el2 = document.getElementById(divArray[i]);
                    el2.className = 'tab-pane fade';
                }

                jQuery('#taskRemarksList2 div').html('');
                jQuery.ajax({
                    type: "POST",
                    url: "TaskDash.aspx/getTaskRemarksData2",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('taskRemarksList2').appendChild(div);
                        }
                    }
                });
            }
        }

        function showTaskDocument(name) {
            try{
                startRot();
            
                document.getElementById("rowtasktracebackUser").style.display = "none";
                document.getElementById('rowChoiceTasks').value = name;

                document.getElementById("taskinitialOptionsDiv").style.display = "block";
                 
                document.getElementById("taskhandleOptionsDiv").style.display = "none";
                document.getElementById("taskrejectOptionsDiv").style.display = "none";
                document.getElementById("myOptionsDiv").style.display = "none";
                document.getElementById("myOptionsDiv2").style.display = "none";
                document.getElementById("acceptedOptionsDiv").style.display = "none";
                document.getElementById("pdfloadingAccept").style.display = "none";
                var el = document.getElementById('tasklocation-tab');
                if (el) {
                    el.className = 'tab-pane fade active in';
                }
                var el2 = document.getElementById('taskrejection-tab');
                if (el2) {
                    el2.className = 'tab-pane fade';
                }
                var el3 = document.getElementById('tremarks-tab');
                if (el3) {
                    el3.className = 'tab-pane fade';
                }
                oldDivContainers();
                incidentOldDivContainers();
                var retVal = assignrowData(name);

                if (retVal == "Completed" || retVal == "Follow Up")
                    TaskIsCompleted();
                else if (retVal == "RejectedSaved")
                    rejectionSelect();
                else if (retVal == "Accepted")
                    acceptedCompleted();
                //else if (retVal == "MyTask")
                //    myTaskVIew();

                if ("<%=cuserDisplay%>" == "style='display:none;'") {
                    acceptedCompleted();
                    if (retVal == "Completed" || retVal == "Follow Up" || retVal == "Accepted") {
                        document.getElementById('acceptPDFDIV').style.display = 'block';
                    }
                    else {
                        document.getElementById('acceptPDFDIV').style.display = 'none';
                    }
            
                }
                taskHistoryData(name);
                insertAttachmentIcons(name);
                insertAttachmentTabData(name);
                insertAttachmentData(name);
                getChecklistItems(name);
                getChecklistItemsNotes(name);
                getCanvasNotes(name);
                infotabDefault();
                getSubTasklistItems(name);
                gettaskRemarks(name);
                hideAllRemarks();
            }
            catch(err){
                alert(err)
            }
        }
 
        function myTaskVIew() {
            document.getElementById("taskinitialOptionsDiv").style.display = "none";
            document.getElementById("taskhandleOptionsDiv").style.display = "none";
            document.getElementById("taskrejectOptionsDiv").style.display = "none";
            document.getElementById("acceptedOptionsDiv").style.display = "none";
            document.getElementById("myOptionsDiv").style.display = "block";
            document.getElementById("myOptionsDiv2").style.display = "block";
        }
        function rejectionSelect() {
            document.getElementById("acceptedOptionsDiv").style.display = "none";
            document.getElementById("taskinitialOptionsDiv").style.display = "none";
            document.getElementById("taskhandleOptionsDiv").style.display = "none";
            document.getElementById("taskrejectOptionsDiv").style.display = "block";
            document.getElementById("myOptionsDiv").style.display = "none";
            document.getElementById("myOptionsDiv2").style.display = "none";
            var el2 = document.getElementById('tasklocation-tab');
            if (el2) {
                el2.className = 'tab-pane fade';
            }
            var el = document.getElementById('taskrejection-tab');
            if (el) {
                el.className = 'tab-pane fade active in';
            }
        }
        function acceptedCompleted() {
            document.getElementById("taskinitialOptionsDiv").style.display = "none";
            document.getElementById("taskhandleOptionsDiv").style.display = "none";
            document.getElementById("acceptedOptionsDiv").style.display = "block";
            document.getElementById("myOptionsDiv").style.display = "none";
            document.getElementById("taskrejectOptionsDiv").style.display = "none";
            document.getElementById("myOptionsDiv2").style.display = "none";
        }
        function getCanvasNotes(id) {
            document.getElementById("canvasItemsListNotes").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getCanvasNotesData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        if (data.d.length > 0) {
                            document.getElementById("pCanvasNotes").style.display = "block";
                            for (var i = 0; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("canvasItemsListNotes");
                                var li = document.createElement("li");
                                li.innerHTML = '<i style="margin-left:-15px;" ></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                                ul.appendChild(li);
                                if (res.length > 1) {
                                    if (res[1] != '') {
                                        var li2 = document.createElement("li");
                                        li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                        ul.appendChild(li2);
                                    }
                                }
                            }
                        }
                        else {
                            document.getElementById("pCanvasNotes").style.display = "none";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });

        }
        function TaskIsCompleted() {
            document.getElementById("taskinitialOptionsDiv").style.display = "none";
            document.getElementById("taskhandleOptionsDiv").style.display = "block";
            document.getElementById("taskrejectOptionsDiv").style.display = "none";
        }
        function taskinsertAttachmentData(id) {
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/taskgetAttachmentData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        for (var i = 0; i < data.d.length; i++) {
                            if (data.d[i].indexOf("video") >= 0) {
                                var div = document.createElement('div');
                                div.className = 'tab-pane fade';
                                div.innerHTML = data.d[i];
                                div.id = 'video-' + (i + 1) + '-tab';
                                document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                divArray[i] = 'video-' + (i + 1) + '-tab';
                            }
                            else {
                                var div = document.createElement('div');
                                div.className = 'tab-pane fade';
                                div.align = 'center';
                                div.style.height = '380px';
                                div.innerHTML = data.d[i];
                                div.id = 'image-' + (i + 1) + '-tab';
                                document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                divArray[i] = 'image-' + (i + 1) + '-tab';
                            }
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function taskinsertAttachmentTabData(id) {
            jQuery('#taskattachments-info-tab div').html('');

            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/taskgetAttachmentDataTab",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        document.getElementById("taskattachments-info-tab").innerHTML = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function taskinsertAttachmentIcons(id) {
            jQuery('#taskdivAttachment div').html('');
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/taskgetAttachmentDataIcons",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        document.getElementById("taskdivAttachment").innerHTML = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function getChecklistItemsNotes(id) {
            document.getElementById("checklistItemsListNotes").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getChecklistNotesData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        if (data.d.length > 0) {
                            document.getElementById("pchecklistItemsListNotes").style.display = "block";
                            for (var i = 0; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("checklistItemsListNotes");
                                var li = document.createElement("li");
                                li.innerHTML = '<i style="margin-left:-15px;" class="fa fa-square-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                                ul.appendChild(li);
                                if (res.length > 1) {
                                    if(res[1] != '')
                                    {
                                        var li2 = document.createElement("li");
                                        li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                        ul.appendChild(li2);
                                    }
                                }
                            }
                        }
                        else
                        {
                            document.getElementById("pchecklistItemsListNotes").style.display = "none";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });

        }
        function getChecklistItems(id)
        {
            document.getElementById("checklistItemsList").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getChecklistData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        if (data.d[0] == "False") {
                            var el = document.getElementById('checklistnamespanFA');
                            if (el) {
                                el.className = "fa fa-square-o";
                            }
                        }
                        else {
                            var el = document.getElementById('checklistnamespanFA');
                            if (el) {
                                el.className = "fa fa-check-square-o";
                            }
                        }
                        for (var i = 1; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("checklistItemsList");
                            var li = document.createElement("li");
                            var marginLeft = '';
                            if (res[2] == 'True') {
                                marginLeft = 'style = "margin-left:-15px;"';
                            }

                            if (res[2] == '3') {

                                if (res[1] == "Checked")
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];
                                else
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];

                                ul.appendChild(li);

                                var li2 = document.createElement("li");
                                li2.innerHTML = '<a style="margin-left:5px;" href="#"  class="capitalize-text" >Notes: ' + res[3] + '</a>';
                                ul.appendChild(li2);
                            }
                            else {
                                if (res[1] == "Checked")
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];
                                else
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];

                                ul.appendChild(li);
                            }
                            //li.appendChild(document.createTextNode(data.d[i]));

                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });

        }
        function assignrowDataTask(id) {
            var output = "";
            $.ajax({
                type: "POST",
                url: "TaskDash.aspx/getTableRowDataTask",
                data: "{'id':'" + id+ "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        document.getElementById("taskusernameSpan").innerHTML = data.d[0];
                        document.getElementById("tasktimeSpan").innerHTML = data.d[1];
                        document.getElementById("tasktypeSpan").innerHTML = data.d[2];
                        document.getElementById("taskstatusSpan").innerHTML = data.d[3];
                        document.getElementById("tasklocSpan").innerHTML = data.d[4];
                        document.getElementById("taskdescriptionSpan").innerHTML = data.d[8];
                        document.getElementById("taskinstructionSpan").innerHTML = data.d[9];
                        document.getElementById("taskincidentNameHeader").innerHTML = data.d[10];
                        document.getElementById("assignedTimeSpan").innerHTML = data.d[11];
                        document.getElementById("checklistNotesSpan").innerHTML = data.d[12];
                        document.getElementById("checklistnameSpan").innerHTML = data.d[14];

                        var el = document.getElementById('headerImageClass');
                        if (el) {
                            el.className = data.d[13];
                        }
                        output = data.d[3];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
            return output;
        }
        function infotabDefault() {
            var el = document.getElementById('taskactivity-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
            var el3 = document.getElementById('taskinfo-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            var el4 = document.getElementById('taskattachments-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }
            var el2 = document.getElementById('taskliInfo');
            if (el2) {
                el2.className = 'active';
            }
            var el5 = document.getElementById('taskliActi');
            if (el5) {
                el5.className = ' ';
            }
            var el6 = document.getElementById('taskliAtta');
            if (el6) {
                el6.className = ' ';
            }
            var el7 = document.getElementById('taskliNotes');
            if (el7) {
                el7.className = ' ';
            }
            var el8 = document.getElementById('notes-tab');
            if (el8) {
                el8.className = 'tab-pane fade';
            }
        } 
        function userchoice(id, name) {
            var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
            if (exists == false) {
                var myOption;
                myOption = document.createElement("Option");
                myOption.text = name; //Textbox's value
                myOption.value = id; //Textbox's value
                sendToListBox.add(myOption);
                document.getElementById("<%=tbUserID.ClientID%>").value = document.getElementById("<%=tbUserID.ClientID%>").value + '-' + id;
                document.getElementById("<%=tbUserName.ClientID%>").value = document.getElementById("<%=tbUserName.ClientID%>").value + '-' + name;
            }
        }
        function addreminders() {

            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getReminders",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'help-block mb-4x';

                            div.innerHTML = data.d[i];

                            document.getElementById('divReminder').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });

        } 
        function recentActivity2() {

            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getRecentActivity2",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row';

                            div.innerHTML = data.d[i];

                            document.getElementById('divrecentActivity2').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }    
        function getonlineuserspercentage() {
            var output = "";
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getusershealtcheck",
                data: "{'id':'0','onlinecount':'" + <%=onlinecount%> + "','offlinecount':'" + <%=offlinecount%> + "','idlecount':'" + <%=idlecount%> + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d.onlineuserspercentage[0] == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }else{
					
                        // getonlineuserspercentage
                        document.getElementById('divOnlineCount').style.width = data.d.onlineuserspercentage[0];
                        document.getElementById('divOfflineCount').style.width = data.d.onlineuserspercentage[1];
                        document.getElementById('divIdleCount').style.width = data.d.onlineuserspercentage[2];

                        //addreminders
                        for (var i = 0; i < data.d.reminders.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'help-block mb-4x';

                            div.innerHTML = data.d.reminders[i];

                            document.getElementById('divReminder').appendChild(div);
                        }

                        //getReminderSelectorDates
                        var select1 = document.getElementById('fromReminderDate2');
                        var select2 = document.getElementById('toReminderDate');
                        for (var i = 0; i < data.d.ReminderSelectorDates.length; i++) {
                            var opt = document.createElement('option');
                            opt.value = data.d.ReminderSelectorDates[i];
                            opt.innerHTML = data.d.ReminderSelectorDates[i];
                            select1.appendChild(opt);
                            var opt2 = document.createElement('option');
                            opt2.value = data.d.ReminderSelectorDates[i];
                            opt2.innerHTML = data.d.ReminderSelectorDates[i];
                            select2.appendChild(opt2);
                        }
                        //addrowtoTableTeamTasks
                        jQuery("#teamtasksTable tbody").empty();
                        jQuery("#teamtasksTable").dataTable().fnClearTable();
                        jQuery("#teamtasksTable").dataTable().fnDraw();
                        jQuery("#teamtasksTable").dataTable().fnDestroy();
                        for (var i = 0; i < data.d.TeamTasks.length; i++) {
                            $("#teamtasksTable tbody").append(data.d.TeamTasks[i]);
                        }
                        jQuery("#teamtasksTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 10,
                            "order": [[3, "desc"]]
                        });


                        //recentActivity2
                        for (var i = 0; i < data.d.recentActivity.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row';

                            div.innerHTML = data.d.recentActivity[i];

                            document.getElementById('divrecentActivity2').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
            return output;
        } 
        function searchMapFunction (){
            try{
                var foundMatch= false;
                var text = document.getElementById("tbMapSearch").value; 
                if(!isSpecialChar(text)){
                    var options = document.getElementById('usermapListBox').options; 
                    for (var i = 0; i < options.length; i++) {
                        var option = options[i]; 
                        var optionText = option.text; 
                        var lowerOptionText = optionText.toLowerCase();
                        var lowerText = text.toLowerCase(); 
                        var regex = new RegExp("^" + text, "i");
                        var match = optionText.match(regex); 
                        var contains = lowerOptionText.indexOf(lowerText) != -1;
                        if(lowerOptionText == lowerText)
                        {
                            var res = option.value.split("-");
                            map.setCenter(new google.maps.LatLng(res[0], res[1]));
                            map.setZoom(16);
                            foundMatch = true;
                            return;
                        }
                    }
                    if(!foundMatch){
                        showAlert('Couldnt find what your looking for.');
                    }
                }
            }catch(ex)
            {
                alert(ex)
            }
        }
        function mapCBClick() {
            var locCB = document.getElementById('maplocationsCB').checked;
            var eventsCB = document.getElementById('mapeventsCB').checked;
            var peopleCB = document.getElementById('mappeopleCB').checked;
            for (var key in myMarkers) {
                var obj = myMarkers[key];
                obj.setMap(null);
                obj = null; 
            }
            myMarkers = new Array();
		
            for (var i = 0; i < Object.size(polylinesArray) ; i++) {
                if (polylinesArray[i] != null) {
                    polylinesArray[i].setMap(null);
                }
            }
		
            if (locCB || eventsCB || peopleCB) {
                jQuery.ajax({
                    type: "POST",
                    url: "TaskDash.aspx/getGPSDataFull",
                    data: "{'id':'0','locCB':'" + locCB + "','eventsCB':'" + eventsCB
                        + "','peopleCB':'" + peopleCB + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d == "]") {

                        }
                        else {
                            var obj = jQuery.parseJSON(data.d)
                            updateGPSMapMarker(obj);
                            autocomplete(document.getElementById("tbMapSearch"), countries);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }

        function createInfoWindow(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent(popupContent);
                infoWindow.open(map, this);
            });
        }
        function getLocation(obj) {
            //navigator.geolocation.getCurrentPosition(function (position) {
            //sourceLat = position.coords.latitude;
            //sourceLon = position.coords.longitude;

            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(sourceLat,sourceLon);//(sourceLat, sourceLon);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 8,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            var poligonCoords = [];
            var subpoligonCoords = [];
            var first = true;
            var secondfirst = true;
            var previousColor = "";
            var polyCounter = 0;

            countries = [];

            for (var i = 0; i < obj.length; i++) {
                var myOption;
                if(obj[i].Logs == "User")
                {
                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-user pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#user-profile-tab"  data-toggle="tab" onclick="assignUserProfileData(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                }
                else if (obj[i].Logs == "Task")
                {
                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#taskDocument"  data-toggle="modal" onclick="showTaskDocument(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                }
                else if (obj[i].Logs == "Verify")
                {
                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#verificationDocument"  data-toggle="modal" onclick="assignVerifierId(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                }
                else
                {
                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">'+ obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" data-target="#viewDocument1"  data-toggle="modal" onclick="rowchoice(&apos;' + obj[i].Id + '&apos;)"><i class="fa fa-eye red-color"></i>VIEW</a></div>';
                }
                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                if (obj[i].State == "YELLOW") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                    myMarkers[obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);

                    myOption = document.createElement("Option");
                    myOption.text = obj[i].DName; //Textbox's value
                    myOption.value = obj[i].Lat + "-" + obj[i].Long;
                    usermapListBox.add(myOption);
                    countries.push(obj[i].DName);
                }
                else if (obj[i].State == "GREEN") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                    myMarkers[obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);

                    myOption = document.createElement("Option");
                    myOption.text = obj[i].DName; //Textbox's value
                    myOption.value = obj[i].Lat + "-" + obj[i].Long;
                    usermapListBox.add(myOption);
                    countries.push(obj[i].DName);
                }
                else if (obj[i].State == "BLUE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                    myMarkers[obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);

                    myOption = document.createElement("Option");
                    myOption.text = obj[i].DName; //Textbox's value
                    myOption.value = obj[i].Lat + "-" + obj[i].Long;
                    usermapListBox.add(myOption);
                    countries.push(obj[i].DName);
                }
                else if (obj[i].State == "OFFUSER") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                    myMarkers[obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);

                    myOption = document.createElement("Option");
                    myOption.text = obj[i].DName; //Textbox's value
                    myOption.value = obj[i].Lat + "-" + obj[i].Long;
                    usermapListBox.add(myOption);
                    countries.push(obj[i].DName);
                }
                else if (obj[i].State == "OFFCLIENT") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                    myMarkers[obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);

                    myOption = document.createElement("Option");
                    myOption.text = obj[i].DName; //Textbox's value
                    myOption.value = obj[i].Lat + "-" + obj[i].Long;
                    usermapListBox.add(myOption);
                    countries.push(obj[i].DName);
                }
                else if (obj[i].State == "PURPLE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                    myMarkers[obj[i].Id] = marker;
                    createInfoWindow(marker, contentString);

                    myOption = document.createElement("Option");
                    myOption.text = obj[i].DName; //Textbox's value
                    myOption.value = obj[i].Lat + "-" + obj[i].Long;
                    usermapListBox.add(myOption);
                    countries.push(obj[i].DName);
                }
                else {
                    if (secondfirst) {
                        currentSubLocation = obj[i].State;
                        var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                        if (obj[i].Logs == '#17ff33') {
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');

                            myOption = document.createElement("Option");
                            myOption.text = obj[i].Username; //Textbox's value
                            myOption.value = obj[i].Lat + "-" + obj[i].Long;
                            usermapListBox.add(myOption);
                            countries.push(obj[i].Username);
                        }
                        else {
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');

                            myOption = document.createElement("Option");
                            myOption.text = obj[i].Username; //Textbox's value
                            myOption.value = obj[i].Lat + "-" + obj[i].Long;
                            usermapListBox.add(myOption);
                            countries.push(obj[i].Username);
                        }
                        previousColor = obj[i].Logs;
                        myMarkers[obj[i].Id] = marker;
                        createInfoWindow(marker, contentString);
                        secondfirst = false;
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        subpoligonCoords.push(point);
                    }
                    else {
                        if (currentSubLocation == obj[i].State) {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                        else {
                            if (subpoligonCoords.length > 0) {
                                var subpoligon = new google.maps.Polyline({
                                    path: subpoligonCoords,
                                    geodesic: true,
                                    strokeColor: previousColor,
                                    strokeOpacity: 1.0,
                                    strokeWeight: 2
                                });
                                subpoligon.setMap(map);
                                polylinesArray[polyCounter]= subpoligon;
                                polyCounter++;
                            }
                            subpoligonCoords = [];
                            currentSubLocation = obj[i].State;
                            var marker = new google.maps.Marker({ position: myLatlng, map: map, title: obj[i].Username + "\n" + obj[i].LastLog });
                            if (obj[i].Logs == '#17ff33') {
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png');

                                myOption = document.createElement("Option");
                                myOption.text = obj[i].Username; //Textbox's value
                                myOption.value = obj[i].Lat + "-" + obj[i].Long;
                                usermapListBox.add(myOption);
                                countries.push(obj[i].Username);
                            }
                            else {
                                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');

                                myOption = document.createElement("Option");
                                myOption.text = obj[i].Username; //Textbox's value
                                myOption.value = obj[i].Lat + "-" + obj[i].Long;
                                usermapListBox.add(myOption);
                                countries.push(obj[i].Username);
                            }
                            previousColor = obj[i].Logs;
                            myMarkers[obj[i].Id] = marker;
                            createInfoWindow(marker, contentString);
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                    }
                }
            }

            if (subpoligonCoords.length > 0) {
                var subpoligon = new google.maps.Polyline({
                    path: subpoligonCoords,
                    geodesic: true,
                    strokeColor: '#0000FF',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                subpoligon.setMap(map);
                polylinesArray[polyCounter]= subpoligon;
                polyCounter++;
            }

            //});
        }
        function getLocationNoOnline() {

            locationAllowed = true;
            setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

            }, 1000);
            // });
        }
        function Initialize() {
            jQuery.ajax({
                type: "POST",
                url: "TaskDash.aspx/getGPSData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "LOGOUT"){
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "]") {
                        getLocationNoOnline();
                    }
                    else {
                        var obj = jQuery.parseJSON(data.d)
                        getLocation(obj);
                        autocomplete(document.getElementById("tbMapSearch"), countries);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);  
                }
            });
        }
        function autocomplete(inp, arr) {
            /*the autocomplete function takes two arguments,
            the text field element and an array of possible autocompleted values:*/
            var currentFocus;
            /*execute a function when someone writes in the text field:*/
            inp.addEventListener("input", function (e) {
                var a, b, i, val = this.value;
                /*close any already open lists of autocompleted values*/
                closeAllLists();
                if (!val) { return false; }
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }
        var countries = [];//["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
        
        </script>
        <!-- ============================================
    MAIN CONTENT SECTION
    =============================================== -->
        <section class="content-wrapper" role="main" >
            <div class="content" >
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">Dashboards</span></h3>
                                </div>
                                <div class="col-md-7 ">
                                    <div class="panel-control" >
                                        <ul class="nav nav-tabs nav-main">
                                            <li id="dash1Li" style="display:<%=incidentDisplay%>"><a href="Default.aspx"  onclick="showLoader();">Incidents</a>
                                            </li>
                                            <li id="dash2Li" class="active"><a data-toggle="tab" href="#home-tab" onclick="showLoader();location.reload();">Tasking</a>
                                            </li>
                                            <li id="dash3Li" style="display:<%=otDisplay%>"><a href="OthersDash.aspx"  onclick="showLoader();">Ticketing</a>
                                            </li>
                                            <li id="dash4Li" style="display:<%=msbDisplay%>"><a href="MessageBoard.aspx"  onclick="showLoader();">Message Board</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div role="group" class="pull-right">
                                        <%=siteName%>
                                        <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignManagerUserProfileData(<%=loggedInId%>)'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                    <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                   <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                         </div>
                                </div>								
                            </div>
                        </div>
                        <div class="panel-body">
							<div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-9">
										<div class="row mb-4x">
											<div class="col-md-12"> 
                                                <div class="row">
                                                    <div class="col-md-7"> 
												<h4 id="headerincidentsDiv"></h4>
                                                        </div> 
                                                    <div class="col-md-5">
                                                                                                                                                                   <div class="col-md-4"  >    
                                          <div class="nice-checkbox inline-block no-vmargine" style="display:none;">
                                            <input type="checkbox"  id="maplocationsCB" name="niceCheck" onclick="mapCBClick()">
                                            <label for="maplocationsCB">Location</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 

  <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="mapeventsCB" checked="checked" name="niceCheck" onclick="mapCBClick()">
                                            <label for="mapeventsCB">Tasks</label>
                                          </div><!--/nice-checkbox-->       
                                        </div> 
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="mappeopleCB" name="niceCheck" onclick="mapCBClick()">
                                            <label for="mappeopleCB">People</label>
                                          </div><!--/nice-checkbox-->       
                                        </div> 
                                                          
												</div>
                                                    </div>
                                                <div class="row">
                                                    <!-- This is the div that will contain the Google Map -->
												<div id="map_canvas" style="width:100%;height:362px;"></div>
												</div>
                                                <div id="userSearchDIV" class="overlapping-map-searchbox horizontal-navigation" style="right:20px">
                                                    <div class="col-md-10">
                                                        <div class="autocomplete" > 
                                                           <input placeholder="Search" id="tbMapSearch" name="tbMapSearch"" type="text" class="form-control" autocomplete="off"> 
                                                        </div> 
                                                    </div>
                                                    <div class="col-md-2" style="margin-left:-30px;">
                                                    <a onclick="searchMapFunction()" id="searchMAP" href="#"><i class="fa fa-search red-color fa-2x"></i></a>
                                                    </div>
                                                </div>
                                            </div>
										</div>
                                                                                                                        <div class="row">
                                            <div class="col-md-3">
<div class="form-group">
                                          <div class="input-group input-group-in">
																	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>      
												<input placeholder="Pick a date" class="form-control" data-input="daterangepicker" id="fromStatusDatePicker" data-show-dropdowns="true" data-single-date-picker="true">
																									
															   </div>
															   <!-- /input-group-in -->
															</div>
                                            </div>
                                            <div class="col-md-3">
<div class="form-group">
                                          <div class="input-group input-group-in">
																	<span class="input-group-addon "><i class="fa fa-calendar"></i></span>      
												<input placeholder="Pick a date" class="form-control " data-input="daterangepicker" id="toStatusDatePicker" data-show-dropdowns="true" data-single-date-picker="true">
																										
															   </div>
															   <!-- /input-group-in -->
															</div>
                                            </div>
                                            <div class="col-md-6">
                                            <div class="row horizontal-navigation" style="margin-top:8px;">
							                    <div class="panel-control">
								                    <ul class="nav nav-tabs">
                                                        <li ><a style="padding:5px;" href="#" class="capitalize-text" onclick="getEventStatusRetrieve();">RETRIEVE</a>
									                    </li>
                                                        <li style="display: block;"><a style="padding:5px;" href="#" class="capitalize-text" onclick="getEventStatusTotal()">RESET</a>
									                    </li>
								                    </ul>
								                    <!-- /.nav -->
							                    </div>
						                    </div>
                                          </div>
                                        </div>
                                        <div id="taskeventStatusLastHeader" class="row horizontal-chart">
                                            <div class="col-md-2 text-center panel-seperator panel-heading">
                                                <h3 class="panel-title capitalize-text">STATUS</h3>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PENDING</p>
                                                </div>
                                                <div class="inline-block">
                                                   <div class="easyPieChart" id="statusPendingPie" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f44e4b">
                                                        <span class="percentage text-dark fa fa-1x">
														<span class="data-percent" id="statusPendingPercent"></span>%
                                                        </span>
                                                   </div>
                                                </div>
                                                <div class="inline-block ">
                                                    <h3 id="statusPending"></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PROGRESS</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" id="statusInprogressPie"  data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f2c400">
                                                        <span class="percentage text-dark fa fa-1x">
														  <span class="data-percent"  id="statusInprogressPercent"></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3 id="statusInprogress"></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">COMPLETED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" id="statusCompletePie"  data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#3ebb64">
                                                        <span class="percentage text-dark fa fa-1x">
																		<span class="data-percent" id="statusCompletePercent"></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3 id="statusComplete"></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text" >ACCEPTED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" id="statusAcceptedPie"  data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#1b93c0">
                                                        <span class="percentage text-dark fa fa-1x">
																		<span class="data-percent" id="statusAcceptPercent"></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3 id="statusAccept"></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block text-center">
                                                    <p class="capitalize-text">TOTAL</p>
                                                </div>
                                                <div class="text-center" >
                                                    <h2 class="capitalize-text"  style="color: #B2163B;" ID="statusTotal"></h2>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>								
                                        <div class="col-md-3" >
                                        <div data-context="success" class="panel fade in panel-transparent" data-init-panel="true" >
                                            <div class="panel-heading">
                                                <a onclick="showLoader()" href="<%= Page.ResolveClientUrl("~/Pages/Messages.aspx") %>">
                                                <h3 class="panel-title capitalize-text">HIGHLIGHTS FOR TODAY</h3>
                                                <p style="color:gray" class="text-center"><%=datetoday%></p>
                                                </a>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div id="divReminder">
                                                </div>
                                                <div class="help-block text-center mt-8x mb-3x">
													<a class="capitalize-text button-inactive" data-toggle="modal" data-target="#newReminder" href="#">+ NEW REMINDER</a>
                                                </div>		
                                            </div>

                                            <!-- /.panel-body -->
                                        </div>									
                                    </div>
                                </div>		
                                <div class="row mb-4x">
                                    <div class="col-md-12" id="bigChartDemo">
                                    <div data-context="success" class="panel fade in panel-MIMS" data-init-panel="true">
                                            <div class="panel-heading">
                                                <h3 class="panel-title capitalize-text">TASKS COMPLETED BY WEEK</h3>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body no-hpadding">
                                                <div class="help-block text-center">
                                                    <div id="hero-area" class="morris-chart"></div>
                                                </div>
                                                <div class="help-block chart-map-block">
                                                    <div class="inline-block">
                                                        <p><span class="chart-map chart-color-blue"></span> <small id="week1t">WEEK 1 TOTAL <%=week1total%></small> </p>
                                                    </div>
                                                    <div class="inline-block">
                                                        <p><span class="chart-map chart-color-green"></span> <small id="week2t">WEEK 2 TOTAL <%=week2total%></small> </p>
                                                    </div>
                                                    <div class="inline-block">
                                                        <p><span class="chart-map chart-color-yellow"></span> <small id="week3t">WEEK 3 TOTAL <%=week3total%></small> </p>
                                                    </div>    
                                                    <div class="inline-block">
                                                        <p><span class="chart-map chart-color-light-red"></span> <small id="week4t">WEEK 4 TOTAL <%=week4total%></small> </p>
                                                    </div>                                                      
                                                </div>    
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4x">
                                    <div class="col-md-15 col-sm-15">
                                        <div id="divChartDemo1"  data-context="success"  class="panel fade in panel-MIMS" data-init-panel="true">
                                                <div class="panel-heading" onclick="showLoader();window.location.href = 'Pages/Tasks.aspx';" style="cursor: pointer">
                                                    <h3 class="panel-title capitalize-text">AVG TASK COMPLETED</h3>
                                                </div>
                                                <!-- /.panel-heading -->
                                                <div class="panel-body">

                                                    <div class="help-block text-center">
                                                      <div id="hero-bar" class="morris-chart"></div>
                                                    </div>                                                     
                                                    <div class="help-block chart-map-block">
                                                        <div class="inline-block">
                                                            <p id="zaTotal">Average Total : <%=AvgTotal%></p>
                                                        </div>                                                 
                                                    </div>
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                    </div>
                                   <div class="col-md-15 col-sm-15">
                                        <div  data-context="success" id="divChartDemo2" class="panel fade in panel-MIMS" data-init-panel="true">
                                            <div class="panel-heading" onclick="showLoader();window.location.href = 'Pages/Tasks.aspx';" style="cursor: pointer">
                                                <h3 class="panel-title capitalize-text">STATUS</h3>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">

                                                <div class="help-block text-center">
                                                   <canvas id="chartjs-doughnutDemo" height="250" width="200" style="width: 200px; height: 250px;"></canvas>
                                                </div>
                                                <div style="display:none;" class="help-block text-center circle-chart-title">
													<p><b><%=PlannedDemoCount%></b><br /><b>Tasks</b></p>
                                                </div>														
												<%--<div class="help-block chart-map-block">
													<div class="inline-block">
														<p><span class="chart-map chart-color-green"></span> <small>Actual <%=ActualDemoCount%></small> </p>
													</div>
													<div class="inline-block"> 
														<p><span class="chart-map chart-color-yellow"></span> <small>Planned <%=PlannedDemoCount%></small> </p>
													</div>													
												</div>--%>
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-15">
                                        <div id="divChartDemo3" data-context="success" class="panel fade in panel-MIMS" data-init-panel="true">
                                            <div class="panel-heading" onclick="showLoader();window.location.href = 'Pages/UsersDB.aspx';" style="cursor: pointer">
                                                <h3 class="panel-title capitalize-text">TOP 5 USERS</h3>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">

                                                <div class="help-block">
												  <div class="help-block" >
													<span class="pull-right" id="top1inscount"><%=Top1InsCount%></span>
													<span  id="top1insname"><%=Top1InsName%></span>
													  <div class="progress progress-sm">
														<div id="top1ins" class="progress-bar progress-bar-primary chart-color-blue" style="width:<%=Top1Ins%>%;"></div>
													  </div>													
												  </div> 


												  <div class="help-block" >
													<span class="pull-right" id="top2inscount"><%=Top2InsCount%></span>
													<span  id="top2insname"><%=Top2InsName%></span>
												  <div class="progress progress-sm">
													<div id="top2ins" class="progress-bar progress-bar-primary chart-color-green" style="width:<%=Top2Ins%>%;"></div>
												  </div>													
												  </div>


												  <div class="help-block" >
													<span class="pull-right" id="top3inscount"><%=Top3InsCount%></span>
													<span  id="top3insname"><%=Top3InsName%></span>
												  <div class="progress progress-sm">
													<div id="top3ins" class="progress-bar progress-bar-primary chart-color-yellow" style="width:<%=Top3Ins%>%;"></div>
												  </div>													
												  </div>


												  <div class="help-block" >
													<span class="pull-right" id="top4inscount"><%=Top4InsCount%></span>
													<span  id="top4insname"><%=Top4InsName%></span>
												  <div class="progress progress-sm">
													<div id="top4ins" class="progress-bar progress-bar-primary chart-color-light-red" style="width:<%=Top4Ins%>%;"></div>
												  </div>													
												  </div>
												  <div class="help-block" >
													<span class="pull-right" id="top5inscount"><%=Top5InsCount%></span>
													<span id="top5insname"><%=Top5InsName%></span>
												  <div class="progress progress-sm">
													<div id="top5ins" class="progress-bar progress-bar-primary chart-color-dark-red" style="width:<%=Top5Ins%>%;"></div>
												  </div>													
												  </div>
												  
                                                </div>

                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-15">
                                        <div id="divChartDemo4" data-context="success" class="panel fade in panel-MIMS" data-init-panel="true">
                                            <div class="panel-heading" onclick="showLoader();window.location.href = 'Pages/Tasks.aspx';" style="cursor: pointer">
                                                <h3 class="panel-title capitalize-text">COMPLETE CHECKLIST</h3>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <h4 class="capitalize-text text-center" id="dmonth"><%=demoMonth%></h4>
                                                <div class="help-block">
                                                    <canvas id="chartjs-lineDemo" width="200" height="249" style="width: 200px; height: 249px;"></canvas>
                                                </div>										
												<%--<div class="help-block chart-map-block">
													<div class="inline-block">
														<p><span class="chart-map chart-color-blue"></span> <small><%=SkillSet1Name%></small> </p>
													</div>
                                                    <div class="inline-block">
														<p><span class="chart-map chart-color-green"></span> <small><%=SkillSet2Name%></small> </p>
													</div>	
													<div class="inline-block">
														<p><span class="chart-map chart-color-yellow"></span> <small><%=SkillSet3Name%></small> </p>
													</div>	
                                                    <div class="inline-block">
														<p><span class="chart-map chart-color-light-red"></span> <small><%=SkillSet4Name%></small> </p>
													</div>
													<div class="inline-block">
														<p><span class="chart-map chart-color-dark-red"></span> <small><%=SkillSet5Name%></small> </p>
													</div>													
												</div>--%>												
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-15">
                                        <div data-context="success" class="panel fade in panel-MIMS" data-init-panel="true">
                                            <div class="panel-heading" onclick="showLoader();window.location.href = 'Pages/UsersDB.aspx';" style="cursor: pointer">
                                                <h3 class="panel-title capitalize-text">USERS STATUS</h3>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">

                                                <div class="help-block chart-extra-margin">
                                                  <div class="help-block">
													<span class="pull-right"><asp:Label runat="server" ID="lbOnlineCount"></asp:Label></span>
													<span>Online</span>
												  <div class="progress progress-sm">
													<div id="divOnlineCount" class="progress-bar progress-bar-primary chart-color-green" style="width:1%"></div>
												  </div>													
												  </div>


												  <div class="help-block">
													<span class="pull-right"><asp:Label runat="server" ID="lbOfflineCount"></asp:Label></span>
													<span>Offline</span>
												  <div class="progress progress-sm">
													<div id="divOfflineCount" class="progress-bar progress-bar-primary chart-color-light-red" style="width:1%"></div>
												  </div>													
												  </div>


												  <div class="help-block">
													<span class="pull-right"><asp:Label runat="server" ID="lbIdleCount"></asp:Label></span>
													<span>Idle</span>
												  <div class="progress progress-sm">
													<div id="divIdleCount" class="progress-bar progress-bar-primary chart-color-yellow" style="width:1%"></div>
												  </div>													
												  </div>

                                                </div>

                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4x">
                                    <div class="col-md-16 col-sm-12">
	                                    <div id="taskssDivContainer" data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">TASKS</h3>
														<div class="row">
															<div class="col-md-8">
																<div class="progress">
																	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <%=handledTeamTasks%>%">
																	</div>
																</div>															
															</div>
															<div class="col-md-4">
																<p class="white-color progress-bar-title"><%=handledTeamTasks%>% Handled</p>
															</div>
														</div>
                                                    </div>
                                                    <div class="col-md-8 task-table">
													   <div class="col-md-4 mt-3x">
														  
                                                    <!--<div class="form-group">
                                                      <button id="reportrange2" data-drops="down" class="btn btn-block btn-default btn-lg recurring-style">
					                                  <i class="fa fa-refresh"></i>
                                                     Recurring
                                                      </button>
                                                    </div><!--/form-group-->				
														  <div class="clearfx"></div>
													   </div>												   
													   <div class="col-md-4 mt-3x">
															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg">
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>      
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker" id="teamtaskDatepicker" onchange="pickdateTeamTask()" data-show-dropdowns="true" data-single-date-picker="true">
															   </div>
															   <!-- /input-group-in -->
															</div>
													   </div>												   
													   <div class="col-md-4 mt-2x">
                                                          <input id="teamtaskSearchBox" type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" order-of-rows="desc" order-of-column="3" id="teamtasksTable" role="grid">
                                                        <thead>
                                                            <tr role="row"> 
                                                                <th  class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY">ID
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="TIME" aria-sort="descending">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" style="width:90px;" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                                                           </div>
                                    <div class="col-md-15 col-sm-12">
                                        <div data-fill-color="true" style="margin-top:0px;"  class="panel fade in panel-default panel-fill" data-init-panel="true">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">RECENT ACTIVITY</h3>
                                            </div>
                                            <div id="divrecentActivity2DIV" class="panel-body">
                                                    <div id="divrecentActivity2" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:678px">												
                                                    
                                                    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                    </div>
                                </div>
                            </div>
							</div>
                                                        <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div id="changePWDIV" class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>  
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                  			<label class="select select-o" >
                                                                                     <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>       
         <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                                      <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Contract</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                   <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                         <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                        
                                            </div>
                                                         <div class="row" style="display:none;">
                                            <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                           
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                            <!-- /tab-content -->
                        </div>
                        <!-- /panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.content-body -->
            </div>
            <!-- /.content -->
            <div aria-hidden="true" aria-labelledby="newReminder" role="dialog" tabindex="-1" id="newReminder" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">NEW REMINDER</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Reminder Name" id="tbReminderName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
                                          <div class="input-group input-group-in">
                                            <input id="tbReminderDate" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">
                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                    </div>
								</div>
                                                                <div class="row">
									<div class="col-md-6">
                                      <p class="font-bold red-color no-vmargin" >From:</p>
                                        <label class="select select-o">
                                         <select id="fromReminderDate2" onchange="fromReminderDateChange()" >
                                         </select>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                      <p class="font-bold red-color no-vmargin" >To:</p>
                                        <label class="select select-o">
                                             <select id="toReminderDate" >
                                             </select>
                                         <//label>
                                    </div>
								</div>
		                           <div class="row">
                              <div class="col-md-4">
                                 <p>Reminder Color</p>
                              </div>
                              <div class="col-md-8">
                                 <div class="nice-radio filled-color blue-radio nice-radio-inline inline-block no-vmargin">
                                    <input type="radio" name="niceRadioAlt" id="cbRemBlue" checked="checked" class="hidden radio-o"> 
                                    <label for="cbRemBlue" class="vertical-align-top"></label>
                                 </div>
                                 <div class="nice-radio filled-color green-radio nice-radio-inline inline-block">
                                    <input type="radio" name="niceRadioAlt" id="cbRemGreen" class="hidden radio-o">
                                    <label for="cbRemGreen" class="vertical-align-top"></label>
                                 </div>
                                 <div class="nice-radio filled-color yellow-radio nice-radio-inline inline-block">
                                    <input type="radio" name="niceRadioAlt" id="cbRemYellow" class="hidden radio-o">
                                    <label for="cbRemYellow" class="vertical-align-top"></label>
                                 </div>
                                 <div class="nice-radio filled-color lightred-radio nice-radio-inline inline-block">
                                    <input type="radio" name="niceRadioAlt" id="cbRemRed" class="hidden radio-o">
                                    <label for="cbRemRed" class="vertical-align-top"></label>
                                 </div>
                              </div>
                           </div>
								<div class="row">
									<div class="col-md-12">
										<textarea placeholder="Notes" id="tbReminderNotes" class="form-control" rows="3"></textarea>
									</div>
								</div>								
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
									<li><a href="#" data-dismiss="modal" class="capitalize-text">CANCEL</a>
									</li>
                                    <li class="active"><a href="#" class="capitalize-text" onclick="newReminderInsert()">CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>  
            <div aria-hidden="true" aria-labelledby="errorMessageModal" role="dialog" tabindex="-1" id="errorMessageModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
<%--					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					</div>--%>
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h4 class="red-color text-center" id="errormsgTXT">Please provide all needed information</h4>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CLOSE</a>
									    </li>		
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
                        <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                                                                                        <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li class="active"><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                         <form style="display:none" enctype="multipart/form-data" id="dz-postticket" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </form>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
                      <div aria-hidden="true" aria-labelledby="taskDocument" role="dialog" tabindex="-1" id="taskDocument" class="modal fade videoModal" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="headerImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="taskincidentNameHeader"></h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Status: <span id="taskstatusSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created by: <span id="taskusernameSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Assigned to: <span id="tasktypeSpan"></span></p>
						</div>				
                        <div class="col-md-3">
							<p>Task Type: <span id="ttypeSpan"></span></p>
						</div>					
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Location: <span id="tasklocSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created on: <span id="tasktimeSpan"></span></p>
						</div>	
                         <div class="col-md-3">
							<p>Assigned on: <span id="assignedTimeSpan"></span></p>
						</div>	
                        <div class="col-md-3" id="incidentItemsListDIV" style="display:none;">
							<p id="incidentItemsList"></p>
						</div>	
                                                  <div class="col-md-3" id="linkparentDIV" style="display:none;">
							<p id="linkparent"></p>
						</div>									
					  </div>			
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5" style="border-right: 1px #bbbbbb solid;">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" id="demo3-tabs">
                                            <li class="active" id="taskliInfo"><a href="#taskinfo-tab" data-toggle="tab" class="capitalize-text" onclick="document.getElementById('rowtasktracebackUser').style.display = 'none'">INFO</a>
                                            </li>
                                            <li id="taskliNotes"><a href="#notes-tab" data-toggle="tab" class="capitalize-text" onclick="document.getElementById('rowtasktracebackUser').style.display = 'none'">NOTES</a>
                                            </li>
                                            <li id="taskliActi"><a href="#taskactivity-tab" data-toggle="tab" onclick="tracebackOn()" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="taskliAtta"><a href="#taskattachments-tab" data-toggle="tab" class="capitalize-text" onclick="document.getElementById('rowtasktracebackUser').style.display = 'none'">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
                                        <div class="tab-content">
										<div class="tab-pane fade active in" id="taskinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Description:</b></p>
													<p id="taskdescriptionSpan"></p>
												</div>
                                                  <div class="col-md-12" id="dvCustomerNameSpan">
													<p class="red-color"><b>Account Name:</b></p>
													<p id="CustomerNameSpan"></p>
												</div>
                                                <div class="col-md-12" id="dvContractNameSpan">
													<p class="red-color"><b>Contract Name:</b></p>
													<p id="ContractNameSpan"></p>
												</div>
                                                 <div class="col-md-12" id="dvProjectNameSpan">
													<p class="red-color"><b>Project Name:</b></p>
													<p id="ProjectNameSpan"></p>
												</div>
                                            
                                                <div class="col-md-12" id="dvSystemTypeSpan">
													<p class="red-color"><b>System Type:</b></p>
													<p id="SystemTypeSpan"></p>
												</div>
                                              
                                                <div class="col-md-12">
													<p class="red-color"><b>Checklist Name : </b><b id="checklistnameSpan"></b><i id="checklistnamespanFA" style="margin-left:5px;" class="fa fa-check-square-o"></i></p>
                                                    <ul id="checklistItemsList" style="list-style-type: none;">

                                                    </ul> 
												</div>
                                                <div class="row" id="taskSubDIV" style="display:none;"> 
                                                  <div class="col-md-12">
													<p class="red-color"><b>Sub-Task : </b></p>
                                                    <ul id="taskSubList" style="list-style-type: none;margin-left:-30px;">

                                                    </ul>
												</div>
                                                </div>
                                                <div class="col-md-12" id="verificationDIV" style="display:none">
                                                    <p class="red-color"><b>Verification:</b></p>
                                                       <div class="row">
                                                         <div class="col-md-6">
                                                            <div class="help-block text-center">
                                                               <img id="veriResultIMG2" style="height:135px;width:135px;border-radius:8px;" src="">
                                                               <h4 class="gray-color">Comparison Image</h4>
                                                               </div>                                    
                                                         </div>
                                                         <div class="col-md-6">
                                                            <div class="help-block text-center">
                                                               <img id="veriSentIMG2" style="height:135px;width:135px;border-radius:8px;" src="">
                                                               <h4 class="gray-color">Sent Image</h4>
                                                               </div>
                                                         </div>                                 
                                                      </div>
                                                    <div class="text-center">
                                                    <a href="#" style="text-decoration:underline" onclick="assignVerifierRequestData()">View All</a>
					                                </div>
                                                </div>
											</div>		
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
                                             <div class="row" id="legendDIV" style="display:none">
                                                <div class="col-md-4"><i class="fa fa-circle  red-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Pending</a></div>
                                                <div class="col-md-4"><i class="fa fa-circle  yellow-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Inprogress</a></div>
                                                <div class="col-md-4"><i class="fa fa-circle  green-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Complete</a></div>
                                            </div>
										</div>
                                        <div class="tab-pane fade" id="notes-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x" style="display:none;">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
													<p id="taskinstructionSpan"></p>
												</div>
											</div>	
                                            <div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
                                                    <div id="taskRemarksList" >
												    </div>
												</div>
											</div>	
											<div class="row">
												<div class="col-md-12">
													<p class="red-color"><b>Checklist Notes:</b></p>
													<p id="checklistNotesSpan"></p>
												</div>
											</div>	
											<div id="pchecklistItemsListNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Unchecked Items:</b></p>
													 <ul id="checklistItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
                                            <div id="pCanvasNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Canvas Notes:</b></p>
													 <ul id="canvasItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	          
                                            <div id="pRejectionNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Rejection Notes:</b></p>
                                                    <p id="rejectionListNotes"></p>
												</div>
											</div>	                                       
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
										<div class="tab-pane fade" id="taskactivity-tab">
                                                    <div id="taskdivIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div>
										<div class="tab-pane fade" id="taskattachments-tab" onclick="startRot();nextbackTask();">	
                                              <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="taskattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>								
										</div>
							            </div>
									</div>
								</div>
							</div>

                                                        <div class="col-md-1"  style="width:40px;">

                                <i id="rotationDIV1" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb" class="fa fa-angle-double-left  fa-2x" onclick="backImg()"></i>

                            </div>

							<div class="col-md-5" id="taskdivAttachmentHolder" style="width:440px;margin-top:4px;border-right: 1px #bbbbbb solid;border-left: 1px #bbbbbb solid;">
                                                                                                 										<div class="tab-pane fade" id="tremarks-tab">
                                                                            								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li><a href="#tasklocation-tab" data-toggle="tab" onclick="hideAllRemarks()" class="capitalize-text">BACK</a>
                                            </li> 										
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                                    <div id="taskRemarksList2"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div> 
								<div class="tab-pane fade active in" id="tasklocation-tab">
                                    <div id="taskmap_canvasIncidentLocation" style="width:100%;height:380px;"></div>
									<div id="taskdivAttachment" onclick="startRot();" class="overlapping-map-image">
									</div>
								</div>		
                                <div class="tab-pane fade" id="taskrejection-tab">
	                                <p class="red-color text-center"><b>Rejection Notes:</b></p>
									<div class="col-md-12" style="height:410px;">
										<textarea placeholder="Rejection Notes" id="taskrejectionTextarea" class="form-control" rows="3"></textarea>

                                        <div class="row">
                                            <p class="red-color text-center"><b>Assign To Type:</b></p>
                                          <label class="select select-o">
                                           <select id="rejecttaskSelectAssigneeType" onchange="rejecttaskselectAssigneeTypeChange()">
											  <option>User</option>
											  <option style="display:<%=grpOptionView%>">Group</option>
											</select>
										 </label>
                                        </div>

                                        <div class="row" id="rejectUsersDiv">
                                             <p class="red-color text-center"><b>Assign To User:</b></p>
                                            <select id="rejectusersearchselect" class="selectpicker form-control"  data-live-search="true" runat="server">
                                            </select>

									    </div>
                                         <div class="row" id="rejectGroupDiv" style="display:none">
                                             <p class="red-color text-center"><b>Assign To Group:</b></p>
                                            <select id="rejectgroupsearchselect" class="selectpicker form-control"  data-live-search="true" runat="server">
                                            </select>
                                         </div>                       
                                    </div>
                                </div>									
							</div>

                                                         <div class="col-md-1"  style="width:40px;">
                                 <i id="rotationDIV2" class="fa fa-angle-double-right  fa-2x" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb;"  onclick="nextImg()"></i>

                             </div>

						</div>
                        <div class="row">
                                                        <div class="col-md-5">

                            </div>
                                    <div class="col-md-7" style="display:none;border-left:0px;" id="taskAudioDIV"> 
                                        <audio id="taskAudio" style="width: 100%;" width="100%" controls><source id="taskAudioSrc" type="audio/mpeg"/></audio>
                                    </div>
                        </div>
					</div>
					<div class="modal-footer">
                      <div class="row" id="rowtasktracebackUser" style="display:none;">
                        <div class="col-md-5">
                            </div>
                               <div class="col-md-7" style="text-align:left;margin-top:-8px;">
                                    <a style="font-size:16px;" href="#" id="playpausefilter" onclick="playpauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1)"><i id="taskfilter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5)"><i id="taskfilter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30)"><i id="taskfilter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60)"><i id="taskfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                            </div>
                        </div>
                      <div class="row" id="pdfloadingAcceptTask"" style="padding-bottom:10px;display:none;">
                            <div class="col-md-5">

                            </div>
                            <div class="col-md-6" style="margin-bottom:-50px">
                                <p class="red-color text-center" style="font-size:12px"><b id="gnNoteTask">GENERATING REPORT</b></p>
                                                                            <div id="myProgress">
                                              <div id="myBar"></div>
                                            </div>
                            </div>
                        </div>
						<div class="row horizontal-navigation">
                            
							<div class="panel-control">
                                <ul class="nav nav-tabs" id="taskinitialOptionsDiv">
                                      <li id="backLinkDisplay1" style="display:none;"><a href="#" onclick="showTaskDocument(document.getElementById('rowChoiceTasksLink').value)">BACK</a>
									</li> 
									<li><a href="#" data-dismiss="modal" >CLOSE</a>
									</li>
								</ul>
                                <ul class="nav nav-tabs" id="acceptedOptionsDiv">
                                    <li id="backLinkDisplay2" style="display:none;"><a href="#"  onclick="showTaskDocument(document.getElementById('rowChoiceTasksLink').value)">BACK</a>
									</li>
									<li><a href="#" data-dismiss="modal" >CLOSE</a>
									</li>
                                    <li style="display:none;" class="pull-right"><a href="#" class="capitalize-text " onclick="generateTaskExcel()">EXCEL</a>
                                    </li>	
                                    <li id="acceptPDFDIV" class="pull-right"><a href="#" class="capitalize-text " onclick="generateTaskPDF()">PDF</a>
                                    </li>	
								</ul>
								<ul class="nav nav-tabs" id="taskhandleOptionsDiv" style="display:none;">
                                    <li id="backLinkDisplay3" style="display:none;"><a href="#" onclick="showTaskDocument(document.getElementById('rowChoiceTasksLink').value)" >BACK</a>
									</li>
                                    <li><a href="#" data-dismiss="modal" >CLOSE</a>
									</li>		
                                    <li style="display:none;" id="acceptLiHandle"><a href="#" onclick="taskStateChange('Accept')" data-dismiss="modal">ACCEPT</a>
									</li>
									<li style="display:none;"><a href="#" data-target="#taskrejection-tab" data-toggle="tab"  onclick="rejectionSelect();">REJECT</a>
									</li>	
                                    <li style="display:none;" class="pull-right"><a href="#" class="capitalize-text " onclick="generateTaskExcel()">EXCEL</a>
                                    </li>		
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateTaskPDF()">PDF</a>
                                    </li>		
			
								</ul>
                                <ul class="nav nav-tabs" id="taskrejectOptionsDiv" style="display:none;">
                                    <li><a href="#" onclick="userTaskAssign('assign')">ASSIGN</a>
									</li>
									<li><a href="#"  onclick="userTaskAssign('reassign')">REASSIGN</a>
									</li>
								</ul>
                                <div class="col-md-3">
                                <ul class="nav nav-tabs" id="myOptionsDiv" style="display:none;">
                                    <li id="backLinkDisplay4" style="display:none;"><a href="#" onclick="showTaskDocument(document.getElementById('rowChoiceTasksLink').value)" >BACK</a>
									</li>
                                    <li><a href="#" data-dismiss="modal" >CLOSE</a>
									</li>	
                                    <li id="myAssignLi"><a href="#" data-dismiss="modal" onclick="myUserTaskAssign()">ASSIGN</a>
									</li>
								</ul>
                                </div>
                                <div class="col-md-9" id="myOptionsDiv2">
                                    <div class="col-md-3" style="margin-top:10px">
                                    <p class="red-color text-center"><b>Assign To:</b></p>
                                        </div>
                                    <div class="col-md-3" style="margin-top:5px">
                                                                             <label class="select select-o">
                                           <select id="myAssignType" onchange="myAssignTypeChange()">
											  <option>User</option>
											  <option style="display:<%=grpOptionView%>">Group</option>
											</select>
										 </label>
                                        </div>
                                    <div id="myusersearchselectLi" class="col-md-6">
                                                                                                                    <select id="myusersearchselect" style="border:none;" class="selectpicker form-control"  data-live-search="true" runat="server">
                                        </select>
                                        </div>
                                    <div id="mygrpsearchselectLi" style="display:none;" class="col-md-6">
                                                                               <select id="mygrpsearchselect" style="border:none;" class="selectpicker form-control"  data-live-search="true" runat="server">
                                        </select>
                                    </div>
                                </div>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                        <div aria-hidden="true" aria-labelledby="verificationDocument" role="dialog" tabindex="-1" id="verificationDocument" class="modal fade videoModal" style="display: none;">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <div class="row">
                           <div class="col-md-11">
                              <h4 class="modal-title capitalize-text">VERIFICATION</h4>
                           </div>
                           <div class="col-md-1">
                              <button aria-hidden="true" data-dismiss="modal" class="close"  type="button"><i class="icon_close fa-lg"></i></button>
                           </div>
                        </div>

                     </div>
                     <div class="modal-body pt-4x">
                        <div class="row border-bottom pb-4x selected-user container-match-user-1">
                           <div class="col-md-15 text-center border-right match-user match-user-1" onclick="moveImageToComparison('1')"> 
                              <div class="help-block">
                                 <img id="veriResult1IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font green-color" id="resultPercent1">
                                    
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-2" onclick="moveImageToComparison('2')"> 
                              <div class="help-block">
                                 <img id="veriResult2IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font yellow-color" id="resultPercent2">

                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-3" onclick="moveImageToComparison('3')"> 
                              <div class="help-block">
                                 <img id="veriResult3IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent3">
                                
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-4" onclick="moveImageToComparison('4')"> 
                              <div class="help-block">
                                 <img id="veriResult4IMG"  style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent4">
                                   
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center match-user match-user-5" onclick="moveImageToComparison('5')"> 
                              <div class="help-block">
                                 <img id="veriResult5IMG" style="height:61px;width:61px;"  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent5">
                                    
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>                                                                                                            
                        </div>
                        <div class="row">
                           <div class="col-md-17 border-right mt-2x mb-2x">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="help-block text-center">
                                       <img id="veriResultIMG" style="height:135px;width:135px;border-radius:8px;" src="">
                                       <h4 class="gray-color">Comparison Image</h4>
                                       <a href="#enlargeImageModal" class="red-color" data-dismiss="modal" data-toggle="modal" onclick="enlargeDisplayImage('Comparison')">Enlarge Image</a>
                                    </div>                                    
                                 </div>
                                 <div class="col-md-6">
                                    <div class="help-block text-center">
                                       <img id="veriSentIMG" style="height:135px;width:135px;border-radius:8px;" src="">
                                       <h4 class="gray-color">Sent Image</h4>
                                       <a href="#enlargeImageModal" class="red-color" data-dismiss="modal" data-toggle="modal" onclick="enlargeDisplayImage('Sent')">Enlarge Image</a>
                                    </div>
                                 </div>                                 
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                     <div id="map_verLocation" style="width:100%;height:174px;"></div>
                                 </div>                                 
                              </div>                              
                           </div>
                           <div class="col-md-18">
                              <div class="row">
                                 <div class="col-md-12">
                                    <h4 class="main-header" id="verifyHeaderSpan" style="color:#b2163b;"></h4>
                                 </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Created by:
                                       </p>
                                       <span class="red-color" id="verifyUserSpan">
                                             
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Time:
                                       </p>
                                       <span class="red-color" id="verifyTimeSpan">
                                       </span>                                       
                                    </div>
                              </div>
   
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Type:
                                       </p>
                                       <span class="red-color" id="verifyTypeSpan">
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Location:
                                       </p>
                                       <span class="red-color" id="verifyLocSpan">
                                       </span>                                       
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Case ID:
                                       </p>
                                       <span class="red-color" id="verifyCaseSpan">
                                             
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             PID
                                       </p>
                                       <span class="red-color" id="verifyPIDSpan">
                                       </span>                                       
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Birth Year:
                                       </p>
                                       <span class="red-color" id="verifyBYearSpan">
                                       </span>                                       
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Gender
                                       </p>
                                       <span class="red-color" id="verifyGenderSpan">
                                       </span>
                                    </div>                                    
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Ethnicity: 
                                       </p>
                                       <span class="red-color" id="verifyEthniSpan">
                                       </span>                                       
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           List:
                                       </p>
                                       <span class="red-color" id="verifyListTypeSpan">
                                       </span>
                                    </div>                                    
                              </div>
                              <div class="row">

                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Reason:
                                       </p>
                                       <span class="red-color" id="verifyReasonSpan">
                                       </span>                                       
                                    </div>

                                    <div class="col-md-6">
                                       <p class="inline-block">

                                       </p>
                                       <span class="red-color">

                                       </span>                                       
                                    </div>                                    
                              </div>  


                              <div class="row mt-4x">
                                 <div class="col-md-6">
                                    <p class="no-vmargin">
                                       Match Score
                                    </p>
                                 </div>

                                 <div class="col-md-6 text-right">
                                    <span class="green-color" id="resultPercentMain">
                                          
                                    </span>
                                 </div>
                                 
                              </div>
                              <div class="row">
                                    <div class="col-md-12">
                                                <div class="progress rounded-edge full-proogress-bar">
                                                   <div id="progressbarMainDisplay" class="progress-bar progress-bar-green rounded-edge full-proogress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                                   </div>
                                                </div>                                        
                                    </div>                                    
                              </div>                                                                                                                        
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs" style="display:none;" id="verifyDIV">
                                 <li class="active"><a href="#" onclick="runVerification()">VERIFY</a>
                                 </li>
                                 <li class="active"><a href="#" data-dismiss="modal">CLOSE</a>
                                 </li>
                              </ul>
                              <ul class="nav nav-tabs" id="nextVerifyDIV" style="display:none;">
                                 <li class="active"><a href="#" onclick="runVerification()">VERIFY</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="enlargeImageModal" role="dialog" tabindex="-1" id="enlargeImageModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
                       <div class="modal-header">
                            <div class="row"></div>
                        </div>
					<div class="modal-body" >
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                            <img id="enlargeIMG" class="user-profile-image" src=""> 
                                </div>
                            <div class="col-md-1"></div>
                        </div>
					</div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs text-center">
                                    <li><a href="#" data-dismiss="modal" onclick="jQuery('#verificationDocument').modal('show');" class="capitalize-text">Close</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="deleteUserModal" role="dialog" tabindex="-1" id="deleteUserModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x'></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                         <div class="row">
                            <h4 class="text-center" id="todeleteUser"></h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: This action cannot be undone!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a data-dismiss="modal" onclick="deleteUser()" ><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
                        <div aria-hidden="true" aria-labelledby="greenreminderboxs" role="dialog" tabindex="-1" id="greenreminderboxs" class="modal fade reminder-boxs greenModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="greenRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color " id="greenRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                          
                           </div>
                           <div class="row border-top">
                           </div>
                         
                           <div class="row mb-2x">
                              <p id="greenRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                                                          <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="bluereminderboxs" role="dialog" tabindex="-1" id="bluereminderboxs" class="modal fade reminder-boxs blueModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="blueRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="blueRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                            
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="blueRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                                                          <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="redreminderboxs" role="dialog" tabindex="-1" id="redreminderboxs" class="modal fade reminder-boxs redModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="redRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="redRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="redRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                                                          <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="yellowreminderboxs" role="dialog" tabindex="-1" id="yellowreminderboxs" class="modal fade reminder-boxs yellowModal" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title" id="yellowRemName">Reminder name</h4>
                     </div>
                     <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <i class="fa fa-clock-o"></i>
                                 <time class="black-color" id="yellowRemDate">
                                    7:00 am - 8:00 am October 16 2015
                                 </time>
                              </div>                            
                           </div>
                           
                           <div class="row border-top">
                           </div>

                           <div class="row mb-2x">
                              <p id="yellowRemDesc">
Change is the law of life. And those who look only to the past or present are certain to miss the future.
                              </p>
                           </div>
                        <div class="row">
                                                          <a href="#" data-dismiss="modal" class="markcompleted mr-3x" onclick="reminderRead()">
                                 <i class="fa fa-check-square-o"></i>
                                 MARK AS COMPLETED
                              </a>
                              <a href="#" data-dismiss="modal" class="delete-reminder mt-2x" onclick="reminderDelete()">
                                 <i class="fa fa-2x fa-trash"></i>
                              </a>                              
                        </div>
                     </div>

                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
                        <IncidentCard:MyIncidentCard id="IncidentCardControl" runat="server" />
<%--                        <div aria-hidden="true" aria-labelledby="incidentDocument" role="dialog" tabindex="-1" id="incidentDocument" class="modal fade videoModal" style="display: none;">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <div class="row">
                        <div class="col-md-11">
                            <span class="circle-point-container pull-left mt-2x mr-1x"><span id="incidentheaderImageClass" class="circle-point circle-point-orange"></span></span>
                            <h4 class="modal-title capitalize-text" id="inciincidentNameHeader"></h4>
                        </div>
                        <div class="col-md-1">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>                      
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <p>Created by: <span id="incidentusernameSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Time: <span id="incidenttimeSpan"></span></p> 
                        </div>  
                        <div class="col-md-4">
                            <p>Type: <span id="incidenttypeSpan"></span></p>
                        </div>                          
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <p>Status: <span id="incidentstatusSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Location: <span id="incidentlocSpan"></span></p>
                        </div>                          
                      </div>                      
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" id="incidentdemo3-tabs">
                                            <li class="active" id="incidentliInfo"><a href="#incidentinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>  
                                            <li id="incidentliActi"><a href="#incidentactivity-tab" onclick="tracebackOn('incident')" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="incidentliAtta"><a href="#incidentattachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>                                           
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tab-pane fade active in" id="incidentinfo-tab">
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Received by:</b> <span class="red-color" id="receivedBySpan"></span></p>                                              
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Phone number:</b> <span class="red-color" id="phonenumberSpan"></span></p>                                                
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Email:</b> <span class="red-color" id="emailSpan"></span></p>                                             
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Description:</b></p>
                                                    <p id="incidentdescriptionSpan"></p> 
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Instruction:</b></p>
                                                    <p id="incidentinstructionSpan"></p>
                                                </div>
                                            </div>  
                                            <div id="escalateionSpanDiv" class="row" style="display:none">  
                                                <div class="col-md-12">
                                                    <p id="escalationHead" class="red-color"><b>Escalation Notes:</b></p>
                                                    <p id="escalatedSpan"></p>
                                                </div>
                                            </div>   
                                                <div class="row" id="checklistDIV" style="display:none;">
                                                  <div class="col-md-12">
													<p class="red-color"><b>Dispatched Task : </b><b id="taskItemsnameSpan"></b></p>
                                                      <div class="row horizontal-navigation">
                                                        <div class="panel-control">
                                                                <ul class="nav nav-tabs" id="taskItemsList">

                                                                </ul>
                                                            <!-- /.nav -->
                                                        </div>
                                                    </div>
												</div>
                                                </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                        </div>
                                        </div>
                                        <div class="tab-pane fade" id="incidentactivity-tab">
                                                    <div id="incidentdivIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                                    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                      
                                        </div>
                                        <div class="tab-pane fade" id="incidentattachments-tab" onclick="startRot()">        
                                             <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="incidentattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                            </div>                      
                                        </div>                                      
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8" id="incidentdivAttachmentHolder">
                                <div class="row">
                                    <div class="horizontal-navigation">
                                        <div class="panel-control">
                                                <ul class="nav nav-tabs" id="incidentUsersToDispatchList">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade active in" id="incidentlocation-tab">
                                    <div id="incidentmap_canvasIncidentLocation" style="width:100%;height:378px;"></div>
                                    <div id="incidentdivAttachment" onclick="startRot()" class="overlapping-map-image">
                                    </div>
                                </div>
                                <div class="tab-pane fade " id="incidentAssign-user-tab">
                                        <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MY USERS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="assignUsersTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">USER
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">ACTION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="tab-pane fade " id="incidentNext-user-tab">
                                     <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:378px;">
                                    <p class="red-color"><b>Dispatch Options:</b></p>
                                    <div class="row" <%=dispatchSurv%>>
                                       <div class="col-md-4">
                                           <div class='row nice-checkbox'>
                                               <input onclick="showViewCamDuration()" id='cameraCheck' type='checkbox' name='niceCheck'>
                                               <label for='cameraCheck'>Camera</label>
                                           </div>

                                       </div>
                                       <div class="col-md-8">
                                           <div class="row mb-1x" id="cameraselectDiv">
                                         <label class="select select-o">
                                            <select id="cameraSelect" runat="server">
                                            </select>
                                         </label>
                                               </div>

                                    </div>
                                    </div>
                                                                                                            <div id="camViewDurationDiv" style="display:none;" class="row">
                                       <div class="col-md-4">
                                           Access Duration
                                       </div>
                                       <div class="col-md-8">
                                     <div  style="margin-top:0px;margin-bottom:7px;" class="row horizontal-navigation">
                                                <a style="font-size:14px;" href="#" onclick="viewdurationselect(15)"><i id="camviewfilter1" class="fa fa-square-o"></i>15 MIN</a>
                                                |<a style="font-size:14px;" href="#" onclick="viewdurationselect(30)"><i id="camviewfilter2" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:14px;" href="#" onclick="viewdurationselect(45)"><i id="camviewfilter3" class="fa fa-square-o"></i>45 MIN</a>
									            |<a style="font-size:14px;" href="#" onclick="viewdurationselect(60)"><i id="camviewfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                |<a style="font-size:14px;" href="#" onclick="viewdurationselect(120)"><i id="camviewfilter5" class="fa fa-square-o"></i>2 HOUR</a>
                                </div>
                                    </div>
                                    </div>

                                                                        <div class="row">
                                       <div class="col-md-4">
                                                                                      <div class='row nice-checkbox' >
                                               <input id='TaskCheck' type='checkbox' name='niceCheck'>
                                               <label for='TaskCheck'>Task</label>
                                           </div>
                                       </div>
                                       <div class="col-md-8">
                                                                                      <div class="row">
                                        <label class="select select-o" >
                                            <select id="selectTaskTemplate" runat="server">
                                            </select>
                                         </label>
                                               </div>
                                    </div>
                                    </div>
                                                                    <div class="row"  style="display:none;"> 

                                               <div class="text-center">Attachment Photo</div>

                                    </div>
                                <div class="row">
                                                      <div style="height:50px;" enctype="multipart/form-data" id="dz-upload" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                                        <div class="dz-message">
                                                          <h1>BROWSE</h1>
                                                        </div>
                                                      </div>
                                         </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="Instructions" id="selectinstructionTextarea" class="form-control" rows="6"></textarea>
                                    </div>
                                </div>       
                                                                                                                                 <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                        </div>                       
                                </div>
                        </div>
                            <div class="row">
                                                        <div class="col-md-4">

                            </div>
                                    <div class="col-md-8" style="display:none;border-left:0px;" id="incidentAudioDIV"> 
                                        <audio id="incidentAudio" style="width: 100%;" width="100%" controls><source id="incidentAudioSrc" type="audio/mpeg"/></audio>
                                    </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row" id="rowtracebackUser" style="display:none;">
                        <div class="col-md-4">
                            </div>
                               <div class="col-md-8" style="text-align:left;margin-top:-8px;">
                                    <a style="font-size:16px;" href="#" id="incidentplaypausefilter" onclick="incidentplaypauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1,'incident')"><i id="filter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5,'incident')"><i id="filter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30,'incident')"><i id="filter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60,'incident')"><i id="filter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                                    User:
							        <div style="margin-top:10px;" class="panel-control" id="tracebackUserFilter">
							        </div>
                            </div>
                        </div>
                        <div class="row" id="incidentpdfloadingAccept" style="padding-bottom:10px;display:none;">
                            <div class="col-md-5">

                            </div>
                            <div class="col-md-5" style="margin-bottom:-50px">
                                <p class="red-color text-center" style="font-size:12px"><b>GENERATING REPORT</b></p>
                                                                            <div id="incidentmyProgress">
                                              <div id="incidentmyBar"></div>
                                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                 <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs" id="incidentinitialOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">UNHANDLE</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="handledClick()">HANDLE</a>
                                    </li>
                                    <li <%=escalatedView%>><a href="#" class="capitalize-text" onclick="escalateOptionClick()">ESCALATE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="escalateOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="escalateClick()">ESCALATE</a>
                                    </li>
                                    <li>

                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="dispatchOptionsDiv" style="display:none;">
                                    <li><a href="#"   onclick="IncidentStateChange('Release')">RELEASE</a>
                                    </li>
                                    <li id="disResolveLi"><a href="#" class="capitalize-text" onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="incidenthandleOptionsDiv" style="display:none;">
                                    <li id="parkLi"><a href="#"  onclick="IncidentStateChange('Park')" >PARK</a>
                                    </li>
                                    <li><a  class="capitalize-text" data-target="#incidentAssign-user-tab" data-toggle="tab" onclick="nextLiClick()">DISPATCH FROM USERS</a>
                                    </li>
                                    <li><a  class="capitalize-text" data-target="#location-tab" data-toggle="tab" onclick="nextMapLiClick()">DISPATCH FROM MAP</a>
                                    </li>
                                    <li id="nextLi" style="display:none"><a  class="capitalize-text" data-target="#incidentNext-user-tab" onclick="finishLiClick()" data-toggle="tab" >NEXT</a>
                                    </li>
                                    <li id="finishLi" style="display:none"><a  class="capitalize-text" onclick="dispatchIncident()">DISPATCH</a>
                                    </li>
                                    <li id="resolveLi"><a onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="completedOptionsDiv2" style="display:none;">
                                    <li><a href="#"   onclick="IncidentStateChange('Reject')">REJECT</a>
                                    </li>
                                    <li id="compResolveLi2"><a href="#" class="capitalize-text"  onclick="IncidentStateChange('Resolve')">RESOLVE</a>
                                    </li>
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="completedOptionsDiv" style="display:none;">
                                    <li><a href="#"    onclick="IncidentStateChange('Reject')">REJECT</a>
                                    </li>
                                    <li id="compResolveLi"><a href="#" class="capitalize-text"  onclick="IncidentStateChange('Resolve')">RESOLVE</a>
                                    </li>
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="incidentrejectOptionsDiv" style="display:none;">
                                    <li style="display:none;"><a href="#"  onclick="IncidentStateChange('Reject')">SAVE</a>
                                    </li>
                                    <li><a href="#" onclick="handledClick()">DISPATCH</a>
                                    </li>
                                    <li><a href="#" onclick="redispatch()">RE-DISPATCH</a>
                                    </li>
                                    <li style="display:none;" id="rejResolveLi"><a href="#" class="capitalize-text" onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="resolvedDiv" style="display:none;">
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="escalatedDIV" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>
                                    <li><a href="#" onclick="handledClick()">DISPATCH</a>
                                    </li>
                                </ul> 
                                <textarea placeholder="Rejection Notes" style="display:none;" id="incidentrejectionTextarea" class="form-control" rows="3"></textarea>
                                <textarea placeholder="Notes" style="display:none;" id="resolutionTextarea" class="form-control" rows="3"></textarea>
                                 <textarea placeholder="Escalation Notes" style="display:none;" id="escalateTextarea" class="form-control" rows="3"></textarea>
                                <!-- /.nav -->
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            </div>--%>


                        <div aria-hidden="true" aria-labelledby="successfulReport" role="dialog" tabindex="-1" id="successfulReport" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" id="successfulReportScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
                                     <div aria-hidden="true" aria-labelledby="deleteAttachModal" role="dialog" tabindex="-1" id="deleteAttachModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#taskDocument').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#taskDocument').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachment()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="deleteAttachTicketModal" role="dialog" tabindex="-1" id="deleteAttachTicketModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#ticketingViewCard').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#ticketingViewCard').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachmentTicket()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
                                <TicketingCard:MyTicketingCard id="TicketingCardControl" runat="server" />
            <input id="rowChoiceTasksLink" style="display:none;">  
            <input style="display:none;" id="rowidReminderChoice" type="text"/>
            <input style="display:none;" id="verCaseID1" type="text"/>
            <input style="display:none;" id="verCaseID2" type="text"/>
            <input style="display:none;" id="verCaseID3" type="text"/>
            <input style="display:none;" id="verCaseID4" type="text"/>
            <input style="display:none;" id="verCaseID5" type="text"/>
            <input style="display:none;" id="verCaseName1" type="text"/>
            <input style="display:none;" id="verCaseName2" type="text"/>
            <input style="display:none;" id="verCaseName3" type="text"/>
            <input style="display:none;" id="verCaseName4" type="text"/>
            <input style="display:none;" id="verCaseName5" type="text"/>
              <select style="color:white;background-color: #5D5D5D;width:125px;height:105px;margin-top:-15px;display:none;" multiple="multiple" id="sendToListBox" ></select>				  
            <input style="display:none;" id="previousTaskUser" type="text"/>   
            <input style="display:none;" id="rowidChoice" type="text"/>  
                        <input style="display:none;" id="rowidChoiceTicket" type="text"/>  
                        <input style="display:none;" id="imagePostAttachment" type="text"/>
              <input style="display:none;" id="rowLongitude" type="text"/>  
              <input style="display:none;" id="rowLatitude" type="text"/>  
            <input style="display:none;" id="rowIncidentName" type="text"/>  
            <input style="display:none;" id="rowTaskName" type="text"/> 
            <input style="display:none;" id="rowChoiceTasks" type="text"/>
                         <select style=" display:none;" multiple="multiple" id="usermapListBox" ></select>	
              <asp:HiddenField runat="server" ID="tbUserID" />
              <asp:HiddenField runat="server" ID="tbUserName" />
            <asp:HiddenField runat="server" ID="tbincidentName" />
             <input style="display:none;" id="imagePath" type="text"/>
            <input style="display:none;" id="mobimagePath2" type="text"/>
                        <input style="display:none;" id="rowidChoiceAttachment" type="text"/> 
            <input id="verifierID" style="display:none;">  
                                                     <input id="ticketcontractid" style="display:none;"> 
                <input id="ticketcustomerid" style="display:none;">
              </section>
        <!-- /MAIN -->
</asp:Content>

                    