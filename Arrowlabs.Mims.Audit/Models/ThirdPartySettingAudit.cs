﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ThirdPartySettingAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string UserName { get; set; }
        public string IPAddress { get; set; }
        public string Password { get; set; }
        public string DeviceMacAddress { get; set; }
        public string ServerIP { get; set; }
        public int Port { get; set; }
        public ThirdPartySettingTypeAudit VMSType { get; set; }
        public int ServerPort { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Name { get; set; }
        public int SiteId { get; set; }
        public static bool InsertorUpdateThirdPartySeting(ThirdPartySettingAudit thirdPartySeting, string dbConnection)
        {
            if (thirdPartySeting != null)
            {
                var baseAuditId = BaseAudit.InsertBaseAudit(thirdPartySeting.BaseAudit, dbConnection);

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateThirdPartySetting", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = thirdPartySeting.Id;

                    if (thirdPartySeting.VMSType != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                        cmd.Parameters["@Type"].Value = thirdPartySeting.VMSType.Id;
                    }
                    cmd.Parameters.Add(new SqlParameter("@IPAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@IPAddress"].Value = thirdPartySeting.IPAddress;

                    cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                    cmd.Parameters["@Password"].Value = thirdPartySeting.Password;

                    cmd.Parameters.Add(new SqlParameter("@DeviceMacAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceMacAddress"].Value = thirdPartySeting.DeviceMacAddress;

                    cmd.Parameters.Add(new SqlParameter("@ServerIP", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerIP"].Value = thirdPartySeting.ServerIP;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = thirdPartySeting.Port;

                    cmd.Parameters.Add(new SqlParameter("@ServerPort", SqlDbType.Int));
                    cmd.Parameters["@ServerPort"].Value = thirdPartySeting.ServerPort;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = thirdPartySeting.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = thirdPartySeting.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = thirdPartySeting.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = thirdPartySeting.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = thirdPartySeting.Name;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = thirdPartySeting.UserName;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = thirdPartySeting.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
