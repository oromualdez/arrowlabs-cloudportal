﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class GroupUser
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string imageURL { get; set; }
        public string MACAddress { get; set; }
        public string PCName { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public int SiteId { get; set; }
        public int RoleId { get; set; }

        public string CustomerUName { get; set; }
        public static bool InsertorUpdateGroupUser(GroupUser groupUser, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)

        {
            if (groupUser != null)
            {
                int id = groupUser.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateGroupUser", connection);

                    cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                    cmd.Parameters["@GroupId"].Value = groupUser.GroupId;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = groupUser.UserId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = groupUser.CreateDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = groupUser.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = groupUser.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                    cmd.Parameters["@PCName"].Value = groupUser.PCName;

                    cmd.Parameters.Add(new SqlParameter("@MACAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MACAddress"].Value = groupUser.MACAddress;
                    
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = groupUser.SiteId;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            groupUser.Id = id;
                            var audit = new GroupUserAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, groupUser.UpdatedBy);
                            Reflection.CopyProperties(groupUser, audit);
                            GroupUserAudit.InsertGroupUserAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateGroupUser", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static bool DeleteGroupUserByGroupId(int groupId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteGroupUserByGroupId", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static List<GroupUser> GetAllGroupUserByGroupId(int groupId, string dbConnection)
        {
            List<GroupUser> allGroupUsers = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetAllGroupUserByGroupId", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupUser = new GroupUser();
                    groupUser.Id = Convert.ToInt32(resultSet["Id"].ToString());
                    groupUser.CreateDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());
                    groupUser.UpdatedDate = Convert.ToDateTime(resultSet["Updateddate"].ToString());
                    groupUser.CreatedBy = resultSet["CreatedBy"].ToString();
                    //groupUser.Username = resultSet["Username"].ToString();
                    groupUser.GroupId = Convert.ToInt32(resultSet["GroupId"].ToString());
                    groupUser.UserId = Convert.ToInt32(resultSet["UserId"].ToString());
                    groupUser.PCName = resultSet["PCName"].ToString();
                    groupUser.MACAddress = resultSet["MACAddress"].ToString();
                    allGroupUsers.Add(groupUser);
                }
                connection.Close();
                resultSet.Close();
                return allGroupUsers;
            }
        }
        public static List<GroupUser> GetGroupUsersByGroupId(int groupId, string dbConnection)
        {
            List<GroupUser> allGroupUsers = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetGroupUserByGroupId", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupUser = new GroupUser();
                    groupUser.Id = Convert.ToInt32(resultSet["Id"].ToString());
                    groupUser.CreateDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());
                    groupUser.UpdatedDate = Convert.ToDateTime(resultSet["Updateddate"].ToString());
                    groupUser.CreatedBy = resultSet["CreatedBy"].ToString();
                    groupUser.Username = resultSet["Username"].ToString();
                    groupUser.GroupId = Convert.ToInt32(resultSet["GroupId"].ToString());
                    groupUser.UserId = Convert.ToInt32(resultSet["UserId"].ToString());
                    groupUser.PCName = resultSet["PCName"].ToString();
                    groupUser.MACAddress = resultSet["MACAddress"].ToString();
                    groupUser.RoleId = Convert.ToInt32(resultSet["RoleId"].ToString());
                    allGroupUsers.Add(groupUser);
                }
                connection.Close();
                resultSet.Close();
                return allGroupUsers;
            }
        }

        public static List<GroupUser> GetGroupUsersByUserId(int userId, string dbConnection)
        {
            List<GroupUser> allGroupUsers = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetGroupUserByUserId", connection);

                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = userId;

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupUser = new GroupUser();

                    groupUser.Username = resultSet["Username"].ToString();
                    groupUser.UserId = Convert.ToInt32(resultSet["Id"].ToString());

                    //groupUser.Id = Convert.ToInt32(resultSet["Id"].ToString());
                    //groupUser.GroupId = Convert.ToInt32(resultSet["GroupId"].ToString());
                    //groupUser.UserId = Convert.ToInt32(resultSet["UserId"].ToString());

                    allGroupUsers.Add(groupUser);
                }
                connection.Close();
                resultSet.Close();
                return allGroupUsers;
            }
        }

        public static List<GroupUser> GetGroupsByUserId(int userId, string dbConnection)
        {
            List<GroupUser> allGroups = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetGroupsByUserId", connection);

                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = userId;

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupUser = new GroupUser();

                    groupUser.GroupId = Convert.ToInt32(resultSet["GroupId"].ToString());
                    groupUser.Username = resultSet["PCName"].ToString();
                    groupUser.UserId = Convert.ToInt32(resultSet["GroupId"].ToString());

                    allGroups.Add(groupUser);
                }
                connection.Close();
                resultSet.Close();
                return allGroups;
            }
        }

        public static List<GroupUser> GetGroupUserForChatByUserId(int userId, string dbConnection)
        {
            List<GroupUser> allGroupUsers = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetGroupUserForChatByUserId", connection);

                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = userId;

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupUser = new GroupUser();

                    groupUser.Username = resultSet["Username"].ToString();
                    groupUser.CustomerUName = resultSet["CustomerUName"].ToString();
                    groupUser.UserId = Convert.ToInt32(resultSet["Id"].ToString());
                    if (!string.IsNullOrEmpty(resultSet["Active"].ToString()))
                    {
                        groupUser.IsActive = (Convert.ToInt32(resultSet["Active"]) == 0 || Convert.ToInt32(resultSet["Active"]) == 2) ? false : true;  
                    }
                    allGroupUsers.Add(groupUser);
                }
                connection.Close();
                resultSet.Close();
                return allGroupUsers;
            }
        }

        public static bool DeleteGroupByGroupIdAndUserId(int groupId, int userId, string dbConnection)
        {
            List<GroupUser> allGroupUsers = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteGroupUserByGroupIdAndUserId", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = userId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteGroupUserByUsername(string userId, string dbConnection)
        {
            List<GroupUser> allGroupUsers = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteGroupUserByUsername", connection);

                cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                cmd.Parameters["@Username"].Value = userId; 

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteGroupUserByGroupIdAndMACAddress(int groupId, string macaddress, string dbConnection)
        {
            List<GroupUser> allGroupUsers = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteGroupUserByGroupIdAndMACAddress", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.Parameters.Add(new SqlParameter("@MACAddress", SqlDbType.NVarChar));
                cmd.Parameters["@MACAddress"].Value = macaddress;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
