﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Devices.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.Devices" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
         <style>
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }
        var loggedinUsername = '<%=senderName2%>';

        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;

        jQuery(document).ready(function () {
            localStorage.removeItem("activeTabInci");
            localStorage.removeItem("activeTabMessage");
            localStorage.removeItem("activeTabTask");
            localStorage.removeItem("activeTabTick");
            localStorage.removeItem("activeTabUB");
            localStorage.removeItem("activeTabVer");
            localStorage.removeItem("activeTabLost");
            try {
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    if ($(e.target).attr('href') == "#home-tab" || $(e.target).attr('href') == "#settings-tab" || $(e.target).attr('href') == "#whitelist-tab" || $(e.target).attr('href') == "#thirdparty-tab" || $(e.target).attr('href') == "#system-tab")
                        localStorage.setItem('activeTabDev', $(e.target).attr('href'));
                    //alert($(e.target).attr('href')+'----------NOW Selected');
                });
                var activeTab = localStorage.getItem('activeTabDev');
                if (activeTab) {
                    //alert(activeTab +'--------Selected');
                    //$('#myTab a[href="' + activeTab + '"]').tab('show'); 
                    jQuery('a[data-toggle="tab"][href="' + activeTab + '"]').tab('show');
					
					if(activeTab == "#settings-tab"){
					dropzoneUp();
					} 
					
                }

                localStorage.removeItem("activeTab");

                $('input[type=password]').keyup(function () {
                    // keyup event code here
                    var pswd = $(this).val();
                    if (pswd.length < 8) {
                        $('#length').removeClass('valid').addClass('invalid');
                        lengthGood = false;
                    } else {
                        $('#length').removeClass('invalid').addClass('valid');
                        lengthGood = true;
                    }
                    //validate letter
                    if (pswd.match(/[A-z]/)) {
                        $('#letter').removeClass('invalid').addClass('valid');
                        letterGood = true;

                    } else {
                        $('#letter').removeClass('valid').addClass('invalid');
                        letterGood = false;
                    }

                    //validate capital letter
                    if (pswd.match(/[A-Z]/)) {
                        $('#capital').removeClass('invalid').addClass('valid');
                        capitalGood = true;
                    } else {
                        $('#capital').removeClass('valid').addClass('invalid');

                        capitalGood = false;
                    }

                    //validate number
                    if (pswd.match(/\d/)) {
                        $('#number').removeClass('invalid').addClass('valid');
                        numGood = true;

                    } else {
                        $('#number').removeClass('valid').addClass('invalid');
                        numGood = false;
                    }
                });
                $('input[type=password]').focus(function () {
                    // focus code here
                    $('#pswd_info').show();
                });
                $('input[type=password]').blur(function () {
                    // blur code here
                    $('#pswd_info').hide();
                });

            } catch (err)
            {
                //alert(err);
            }
            addrowtoDevicesTable();
            addrowtoUsersTable();
            addrowtoWhitelistTable();
            getsettings();
            addrowtoThirdParty();
            getserverInfo();
            Dropzone.autoDiscover = false;
            

        });
        function assigneDeviceInfo(macaddress,userid)
        {
            document.getElementById('tbDeviceMac').value = macaddress;
            document.getElementById('MainContent_assignManager').value = userid;
            getCamerasClient();
        }
        function getCamerasClient()
        {
            try{
                var select = document.getElementById("assignedCamera");
                jQuery('#assignedCamera')
                .find('option')
                .remove()
                .end()
                ;
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/getCameras",
                    data: "{'MAC':'" + document.getElementById('tbDeviceMac').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            for (var i = 0; i < data.d.length; i++) {
                                var opt = document.createElement('option');
                                opt.value = data.d[i];
                                opt.innerHTML = data.d[i];
                                select.appendChild(opt);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            catch(err)
            {alert(err)}
        }
        function addCamToDevice() 
        {
            if (document.getElementById('MainContent_milestoneCamera').value != "") {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/addCam",
                    data: "{'MAC':'" + document.getElementById('tbDeviceMac').value + "','CAM':'" + document.getElementById('MainContent_milestoneCamera').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            showAlert("Successfully added camera to MIMS Client");
                            getCamerasClient();
                            try {
                                jQuery("#MainContent_milestoneCamera option[value='" + document.getElementById('MainContent_milestoneCamera').value + "']").remove();
                                jQuery("#MainContent_milestoneCamera").val(jQuery("#MainContent_milestoneCamera option:first").val());
                            }
                            catch (er) {
                             //   alert(er);
                            }
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                showAlert("No Camera to add")
            }
        }
        function removeCamDevice() {
            if (document.getElementById('assignedCamera').value != "") {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/removeCam",
                data: "{'MAC':'" + document.getElementById('tbDeviceMac').value + "','CAM':'" + document.getElementById('assignedCamera').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successMessage').innerHTML = "Successfully removed camera to MIMS Client";
                        jQuery('#successfulModal').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
            }
            else {
                showAlert("No Camera to remove")
            }
        }
        function assignMangToDevice() {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/assignMang",
                data: "{'MAC':'" + document.getElementById('tbDeviceMac').value + "','ID':'" + document.getElementById('MainContent_assignManager').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successMessage').innerHTML = "Successfully assigned MIMS Client to a manager";
                        jQuery('#successfulModal').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        var dropzoneload = false;
        function dropzoneUp()
        {
            try
            {
                if (dropzoneload == false) {
                    var myDropzone = new Dropzone("#dz-test");
                    myDropzone.on("addedfile", function (file) {
                        /* Maybe display some more file information on your page */
                        file.previewElement.addEventListener("click", function () {
                            myDropzone.removeFile(file);
                        });
                            if (file.type != "image/jpeg" && file.type != "image/png") {
                                showAlert("Kindly provided a JPEG or PNG Image for upload");
                                myDropzone.removeFile(file);
                            }
                            else {
                                var data = new FormData();
                                data.append(file.name, file);
                                data.append('Data', JSON.stringify({ username: loggedinUsername }));
                                jQuery.ajax({
                                    url: "../Handlers/DashMobileUpload.ashx",
                                    type: "POST",
                                    data: data,
                                    contentType: false,
                                    processData: false,
                                    success: function (result) {
                                        if (result == "Success") {
                                            document.getElementById('successMessage').innerHTML = "Successfully changed dashboard image.";
                                            jQuery('#successfulModal').modal('show');
                                        }
                                        else if (result == "ImageFail") {
                                            showAlert('Image dimensions do not meet the requirement.(1920x1080)!');
                                            myDropzone.removeFile(file);
                                        }
                                        else {
                                            showAlert(result);
                                            myDropzone.removeFile(file);
                                        }
                                    },
                                    error: function (err) {
                                        myDropzone.removeFile(file);
                                    }
                                });
                            }

                    });
                    dropzoneload = true;
                }
            }
            catch(err)
            {
                showAlert('Error 39: Problem occured while trying to load dropzone control.-'+err);
            }
        }
        function uploaddropzoneUp() {
            try {
                if (dropzoneload == false) {
                    var myDropzone2 = new Dropzone("#license-upload");
                    myDropzone2.on("addedfile", function (file) {

                        file.previewElement.addEventListener("click", function () {
                            myDropzone2.removeFile(file);
                        });

                        /* Maybe display some more file information on your page */
                        var data = new FormData();
                        data.append(file.name, file);
                        jQuery.ajax({
                            url: "../Handlers/LicenseHandler.ashx",
                            type: "POST",
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                if (result == "Success") {
                                    document.getElementById('successMessage').innerHTML = "Successfully uploaded licence.";
                                    jQuery('#successfulModal').modal('show');
                                }
                                else {
                                    showAlert(result);
                                }
                            },
                            error: function (err) {
                            }
                        });
                    });
                    dropzoneload = true;
                }
            }
            catch (err) {
                showAlert('Error 39: Problem occured while trying to load dropzone control.-' + err);
            }
        }

        function getserverInfo() {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/getServerData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('serverClientAddress').value = data.d[0];
                        document.getElementById('serverClientID').value = data.d[1];
                        document.getElementById('serverClientVersion').value = data.d[2];
                        document.getElementById('serverClientName').value = data.d[3];
                        document.getElementById('serverClientPhone').value = data.d[4];
                        document.getElementById('serverClientSerial').value = data.d[5];

                        document.getElementById('clientRemaining').value = data.d[6];
                        document.getElementById('clientTotal').value = data.d[7];
                        document.getElementById('clientUsed').value = data.d[8];

                        document.getElementById('mobileRemaining').value = data.d[9];
                        document.getElementById('mobileTotal').value = data.d[10];
                        document.getElementById('mobileUsed').value = data.d[11];


                        document.getElementById('surveillanceCheck').checked = false;
                        document.getElementById('notificationCheck').checked = false;
                        document.getElementById('locationCheck').checked = false;
                        document.getElementById('ticketingCheck').checked = false;
                        document.getElementById('taskCheck').checked = false;
                        document.getElementById('incidentCheck').checked = false;
                        document.getElementById('warehouseCheck').checked = false;
                        document.getElementById('chatCheck').checked = false;
                        document.getElementById('collaborationCheck').checked = false;
                        document.getElementById('lfCheck').checked = false;
                        document.getElementById('dutyrosterCheck').checked = false;
                        document.getElementById('postorderCheck').checked = false;
                        document.getElementById('verificationCheck').checked = false;
                        document.getElementById('requestCheck').checked = false;
                        document.getElementById('dispatchCheck').checked = false;
                        if (data.d[12] == "true")
                            document.getElementById('surveillanceCheck').checked = true;
                        if (data.d[13] == "true")
                            document.getElementById('notificationCheck').checked = true;
                        if (data.d[14] == "true")
                            document.getElementById('locationCheck').checked = true;
                        if (data.d[15] == "true")
                            document.getElementById('ticketingCheck').checked = true;
                        if (data.d[16] == "true")
                            document.getElementById('taskCheck').checked = true;
                        if (data.d[17] == "true")
                            document.getElementById('incidentCheck').checked = true;
                        if (data.d[18] == "true")
                            document.getElementById('warehouseCheck').checked = true;
                        if (data.d[19] == "true")
                            document.getElementById('chatCheck').checked = true;
                        if (data.d[20] == "true")
                            document.getElementById('collaborationCheck').checked = true;
                        if (data.d[21] == "true")
                            document.getElementById('lfCheck').checked = true;
                        if (data.d[22] == "true")
                            document.getElementById('dutyrosterCheck').checked = true;
                        if (data.d[23] == "true")
                            document.getElementById('postorderCheck').checked = true;
                        if (data.d[24] == "true")
                            document.getElementById('verificationCheck').checked = true;
                        if (data.d[25] == "true")
                            document.getElementById('requestCheck').checked = true;
                        if (data.d[26] == "true")
                            document.getElementById('dispatchCheck').checked = true;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function hideSystemDiv()
        {
            document.getElementById("selectTypeNewDiv").style.display = "none";
            document.getElementById("selectTypeDiv").style.display = "block";
        }
        function showSystemDiv() {
            document.getElementById("selectTypeNewDiv").style.display = "block";
            document.getElementById("selectTypeDiv").style.display = "none";
        }
        //User-profile
        function changePassword() {
            try {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;
                var isErr = false;
                if (!isErr) { 
                    if (!letterGood) {
                        showAlert('Password does not contain letter');
                        isErr = true;
                    }
                    if (!isErr) {
                        if (!capitalGood) {
                            showAlert('Password does not contain capital letter');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!numGood) {
                            showAlert('Password does not contain number');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!lengthGood) {
                            showAlert('Password length not enough');
                            isErr = true;
                        }
                    }
                }
                if (!isErr) {
                    if (newPw == confPw && newPw != "" && confPw != "") {
                        jQuery.ajax({
                            type: "POST",
                            url: "Devices.aspx/changePW",
                            data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d != "LOGOUT") {
                                    jQuery('#changePasswordModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                    jQuery('#successfulDispatch').modal('show');
                                    document.getElementById("newPwInput").value = "";
                                    document.getElementById("confirmPwInput").value = "";
                                    document.getElementById("oldPwInput").value = confPw;
                                }
                                else {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        showAlert("Kindly match new password with confirm password.")
                    }
                }
            }
            catch (ex)
            { showAlert('Error 60: Problem loading page element.-'+ex) }
        }
        function editUnlock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
            document.getElementById("editProfileA").style.display = "none";
            document.getElementById("saveProfileA").style.display = "block";
            document.getElementById("profileEmailAddDIV").style.display = "none";
            document.getElementById("profileEmailAddEditDIV").style.display = "block";
            document.getElementById("userFullnameSpanDIV").style.display = "none";
            document.getElementById("userFullnameSpanEditDIV").style.display = "block";
            if (document.getElementById('profileRoleName').innerHTML == "User") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "block";
                document.getElementById("dirInfoDIV").style.display = "none";


            }
            else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "none";
                document.getElementById("dirInfoDIV").style.display = "block";
            }
        }
        function editJustLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        function editLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
            var role = document.getElementById('UserRoleSelector').value;
            var roleid = 0;
            var supervisor = 0;
            var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
            if (retVal > 0) {
                assignUserProfileData();
            }
            else {
                ShowAlert('Error 61: Problem occured while trying to update user profile.');
            }

        }
        function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
            var output = 0;
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/addUserProfile",
                data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                }
            });
            return output;
        }
        function assignUserProfileData() {
            try {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/getUserProfileData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            try {
                                document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                                document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                                document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                                document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                                document.getElementById('profileLastLocation').innerHTML = data.d[4];
                                document.getElementById('profileRoleName').innerHTML = data.d[5];
                                document.getElementById('profileManagerName').innerHTML = data.d[6];
                                document.getElementById('userStatusSpan').innerHTML = data.d[8];
                                var el = document.getElementById('userStatusIconSpan');
                                if (el) {
                                    el.className = data.d[9];
                                }
                                document.getElementById('userprofileImgSrc').src = data.d[10];
                                document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                                document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                                document.getElementById('userFirstnameSpan').value = data.d[13];
                                document.getElementById('userLastnameSpan').value = data.d[14];
                                document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                                document.getElementById('profileEmailAddEdit').value = data.d[3];

                                document.getElementById('oldPwInput').value = data.d[16];

                                document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                                document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                                document.getElementById('profileGender').innerHTML = data.d[20];

                                if (document.getElementById('profileRoleName').innerHTML != "Level 7") {
                                    document.getElementById('deviceTypesDiv').innerHTML = "<i class='fa fa-mobile fa-2x mr-2x'></i><i style='color:lime;' class='fa fa-laptop fa-2x mr-2x'></i>";//data.d[11];

                                }
                            }
                            catch (err) {
                                //alert(err)
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            catch (err)
            { showAlert('Error 60: Problem loading page element.-'+err) }
        }
        function forceLogout() {
            document.getElementById('<%= closingbtn.ClientID %>').click();
        }
        function saveWhitelistEdit() {
            var isErr = false;
            if (isEmptyOrSpaces(document.getElementById('tbEditWhitelistMacAddress').value)) {
                isErr = true;
                showAlert("Kindly provide mac address")
            }
            else {
                if (isSpecialChar(document.getElementById('tbEditWhitelistMacAddress').value)) {
                    isErr = true;
                }
            }
            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/editWhitelist",
                    data: "{'id':'" + document.getElementById('tbEditWhitelistMacAddress').value + "','oldMac':'" + document.getElementById('tbWhitelistMacAddressHolder').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newEditWhiteListModal').modal('hide');
                            document.getElementById('successMessage').innerHTML = "Entry has successfully been edited";
                            jQuery('#successfulModal').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function saveWhitelist() {
            var isErr = false;
            if (isEmptyOrSpaces(document.getElementById('tbWhitelistMacAddress').value)) {
                isErr = true;
                showAlert("Kindly provide mac address")
            }
            else {
                if (isSpecialChar(document.getElementById('tbWhitelistMacAddress').value)) {
                    isErr = true;
                }
            }
            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/saveWhitelist",
                    data: "{'id':'" + document.getElementById('tbWhitelistMacAddress').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newWhiteListModal').modal('hide');
                            document.getElementById('successMessage').innerHTML = "New entry has successfully been added";
                            jQuery('#successfulModal').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function saveNew3rdParty()
        {
           var name = document.getElementById('tbnew3rdPartyName').value;
           var ip = document.getElementById('tbnew3rdPartyIP').value;
           var port = document.getElementById('tbnew3rdPartyPORT').value;
           var username = document.getElementById('tbnew3rdPartyUsername').value;
           var password = document.getElementById('tbnew3rdPartyPassword').value;
           var type = document.getElementById('MainContent_thirdPartytypeSelect').value;
           var isErr = false;
           if (isEmptyOrSpaces(password)) {
               isErr = true;
               showAlert("Kindly provide password")
           }
           if (isEmptyOrSpaces(username)) {
               isErr = true;
               showAlert("Kindly provide username")
           }
           else {
               if (isSpecialChar(username)) {
                   isErr = true;
               }
           }
           if (isEmptyOrSpaces(name)) {
               isErr = true;
               showAlert("Kindly provide name")
           }
           else {
               if (isSpecialChar(name)) {
                   isErr = true;
               }
           }
            //ip
           if (!ValidateIPaddress(ip)) {
               isErr = true;
           }
            //port
           if (isNumeric(port)) {
               if (!isPort(port)) {
                   isErr = true;
               }
           }
           else {
               isErr = true;
               showAlert("Kindly enter valid port numeric value")
           }


           if (!isErr) {
               if (document.getElementById('newSystemRadio').checked) {
                   var id2 = document.getElementById('tbthirdNewChoice').value;
                   if (id2 == "")
                       id2 = 0;
                   var newType = document.getElementById('tbSystemTypeToAdd').value;
                   if (isEmptyOrSpaces(newType)) {
                       isErr = true;
                       showAlert("Kindly provide type")
                   }
                   else {
                       if (isSpecialChar(newType)) {
                           isErr = true;
                       }
                   }
                   if (!isErr) {
                       jQuery.ajax({
                           type: "POST",
                           url: "Devices.aspx/saveNewSystemData",
                           data: "{'id':'" + id2 + "','name':'" + newType + "','uname':'" + loggedinUsername + "'}",
                           async: false,
                           dataType: "json",
                           contentType: "application/json; charset=utf-8",
                           success: function (data) {
                               if (data.d != "LOGOUT") {
                                   type = data.d;
                               }
                               else {
                                   showError("Session has expired. Kindly login again.");
                                   setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                               }
                           },
                           error: function () {
                               showError("Session timeout. Kindly login again.");
                               setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                           }
                       });
                   }
               }
               if (!isErr) {
                   var id = document.getElementById('tbthirdChoice').value;
                   if (id == "")
                       id = 0;
                   jQuery.ajax({
                       type: "POST",
                       url: "Devices.aspx/save3rdPartyData",
                       data: "{'id':'" + id + "','name':'" + name + "','ip':'" + ip + "','port':'" + port + "','username':'" + username + "','password':'" + password + "','type':'" + type + "','uname':'" + loggedinUsername + "'}",
                       async: false,
                       dataType: "json",
                       contentType: "application/json; charset=utf-8",
                       success: function (data) {
                           if (data.d == "SUCCESS") {
                               jQuery('#new3rdPartySystemModal').modal('hide');
                               document.getElementById('successMessage').innerHTML = name + " has successfully been added";
                               jQuery('#successfulModal').modal('show');
                           }
                           else if (data.d == "LOGOUT") {
                               showError("Session has expired. Kindly login again.");
                               setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                           }
                           else {
                               showError(data.d)
                           }
                       },
                       error: function () {
                           showError("Session timeout. Kindly login again.");
                           setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                       }
                   });
               }
           }
        }
        function saveVerifierSettings() {
            var ANPR = document.getElementById('mimsVerANPR').value;
            var FRS = document.getElementById('mimsVerFRS').value;
            var isErr = false;
            //con

            if (!isUrl(ANPR)) {
                isErr = true;
            }
            if (!isUrl(FRS)) {
                isErr = true;
            }
            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/saveVerifierSettings",
                    data: "{'ANPR':'" + ANPR + "','FRS':'" + FRS + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successMessage').innerHTML = "Verifier settings has successfully been saved";
                            jQuery('#successfulModal').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function saveServerSettings() {

            var port = document.getElementById('mimsServerPort').value;
            var ip = document.getElementById('mimsServerIP').value;
            var con = document.getElementById('mimsServerCon').value;
            var rtsp = document.getElementById('mimsServerRTSP').value;
            var video = document.getElementById('mimsServerVideo').value;
            var signalR = document.getElementById('mimsServerSignalR').value;
            var mPort = document.getElementById('mimsServerAlarmPort').value;
            var protocol = false;
            var isErr = false;
            //con
            if (!isUrl(con)) {
                isErr = true;
            }
            if (!isUrl(signalR)) {
                isErr = true;
            }
            //ip
            if (!ValidateIPaddress(ip)) {
                isErr = true;
            }
            //port
            if (isNumeric(port)) {
                if (!isPort(port)) {
                    isErr = true;
                }
            }
            else {
                isErr = true;
                showAlert("Kindly enter valid port numeric value")
            }
            //rtsp
            if (isNumeric(rtsp)) {
                if (!isPort(rtsp)) {
                    isErr = true;
                }
            }
            else {
                isErr = true;
                showAlert("Kindly enter valid rtsp port numeric value")
            }
            //video
            if (isNumeric(video)) {
                if (!isPort(video)) {
                    isErr = true;
                }
            }
            else {
                isErr = true;
                showAlert("Kindly enter valid video port numeric value")
            }
            //moc
            if (isNumeric(mPort)) {
                if (!isPort(mPort)) {
                    isErr = true;
                }
            }
            else {
                isErr = true;
                showAlert("Kindly enter valid moc port numeric value")
            }
            if (document.getElementById('serverProtocolHttps').checked) {
                protocol = true;
            }
            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/saveServerSettings",
                    data: "{'port':'" + port + "','ip':'" + ip + "','con':'" + con + "','rtsp':'" + rtsp + "','video':'" + video + "','signalR':'" + signalR + "','mPort':'" + mPort + "','protocol':'" + protocol + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successMessage').innerHTML = "Server settings has successfully been saved";
                            jQuery('#successfulModal').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function saveClientSettings() {

            var vidclip = document.getElementById('mimsClientVidClip').value;
            var vidState = document.getElementById('mimsClientVidClipState').checked;
            var passState = document.getElementById('mimsClientPassState').checked;
            var pass = document.getElementById('mimsClientPass').value;
            var images = document.getElementById('mimsClientNoImages').value;
            var imagesState = document.getElementById('mimsClientNoImagesState').checked;
            var portal = document.getElementById('mimsClient3rdWeb').value;
            var dbpath = document.getElementById('mimsClient3rdDB').value;
            var dbpathState = document.getElementById('mimsClient3rdDBState').checked;
            var gps = document.getElementById('mimsClient3rdGPSServ').value;
            var gpsURL = document.getElementById('mimsClient3rdGPSApp').value;
            var gpsI = document.getElementById('mimsClientGPSInt').value;
            var gpsIState = document.getElementById('mimsClientGPSIntState').checked;
            var interval = document.getElementById('mimsClientTransferInt').value;
            var intervalState = document.getElementById('mimsClientTransferIntState').checked;
            var isErr = false;

            if (!isNumeric(vidclip)) {
                isErr = true;
                showAlert("Kindly enter valid clip length numeric value")
            }
            if (!isNumeric(images)) {
                isErr = true;
                showAlert("Kindly enter numeric value for images")
            }
            if (!isNumeric(gpsI)) {
                isErr = true;
                showAlert("Kindly enter numeric value for gps interval")
            }
            if (!isNumeric(interval)) {
                isErr = true;
                showAlert("Kindly enter numeric value for file transfer interval")
            }
            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/saveClientSettings",
                    data: "{'vidclip':'" + vidclip + "','vidState':'" + vidState + "','passState':'" + passState + "','pass':'" + pass +
                        "','images':'" + images + "','imagesState':'" + imagesState + "','portal':'" + portal + "','dbpath':'" + dbpath +
                        "','dbpathState':'" + dbpathState + "','gps':'" + gps + "','gpsURL':'" + gpsURL + "','gpsI':'" + gpsI +
                        "','gpsIState':'" + gpsIState + "','interval':'" + interval + "','intervalState':'" + intervalState + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successMessage').innerHTML = "Client settings has successfully been saved";
                            jQuery('#successfulModal').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }

        }
        function saveMobileSettings() {
            var maxU = document.getElementById('mimsMobileMaxUp').value;
            var maxA = document.getElementById('mimsMobileMaxAttachments').value;
            var mobileAddress = document.getElementById('mimsMobileURLLink').value;
            var mobileGPS = document.getElementById('mimsMobileGPSInt').value;
            var isErr = false;

            if(!isNumeric(maxU))
            {
                isErr = true;
                showAlert("Kindly enter numeric value for max upload")
            }

            if (!isNumeric(maxA)) {
                isErr = true;
                showAlert("Kindly enter numeric value for max attachment")
            }
            if (!isNumeric(mobileGPS)) {
                isErr = true;
                showAlert("Kindly enter numeric value for gps")
            }
            if (!isUrl(mobileAddress)) {
                isErr = true;
            }
            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/saveMobileSettings",
                    data: "{'mobileGPS':'" + mobileGPS + "','mobileAddress':'" + mobileAddress + "','maxA':'" + maxA + "','maxU':'" + maxU + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successMessage').innerHTML = "Mobile settings has successfully been saved";
                            jQuery('#successfulModal').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function saveMOCSettings() {

            var hkState = document.getElementById('mimsMOCHealthCheckIntState').checked;
            var hk = document.getElementById('mimsMOCHealthCheckInt').value;
            var mocport = document.getElementById('mimsMOCPort').value;
            var isErr = false;
            if (!isNumeric(hk)) {
                isErr = true;
                showAlert("Kindly enter numeric value for interval")
            }
            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Devices.aspx/saveMOCSettings",
                    data: "{'mocport':'" + mocport + "','hk':'" + hk + "','hkState':'" + hkState + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successMessage').innerHTML = "MOC settings has successfully been saved";
                            jQuery('#successfulModal').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function saveAuditSettings() {
            try {
                var interval = "0";

                var Isasset = document.getElementById('mimsADAssetState').checked;
                var Isuser = document.getElementById('mimsADUsersState').checked;
                var Iscusev = document.getElementById('mimsADAlarmsState').checked;
                var Istask = document.getElementById('mimsADTasksState').checked;
                var Isverifier = document.getElementById('mimsADVerifierState').checked;
                var Isrem = document.getElementById('mimsADRemindersState').checked;
                var Isgroup = document.getElementById('mimsADGroupsState').checked;
                var Islocation = document.getElementById('mimsADLocationsState').checked;
                var Ischecklist = document.getElementById('mimsADChecklistsState').checked;
                var IstaskA = document.getElementById('mimsADFilesState').checked;
                var Ismlog = document.getElementById('mimsADLogsState').checked;
                var Isnoti = document.getElementById('mimsADNotiState').checked;
                var Isoffence = document.getElementById('mimsADTBState').checked;
                var IspOrder = document.getElementById('mimsADPOState').checked;
                var IsdRoster = document.getElementById('mimsADDRState').checked;
                var IstransLog = document.getElementById('mimsADTLState').checked;

                var asset = document.getElementById('mimsADAsset').value;
                var user = document.getElementById('mimsADUsers').value;
                var cusev = document.getElementById('mimsADAlarms').value;
                var task = document.getElementById('mimsADTasks').value;
                var verifier = document.getElementById('mimsADVerifier').value;
                var rem = document.getElementById('mimsADReminders').value;
                var group = document.getElementById('mimsADGroups').value;
                var location = document.getElementById('mimsADLocations').value;
                var checklist = document.getElementById('mimsADChecklists').value;
                var taskA = document.getElementById('mimsADFiles').value;
                var mlog = document.getElementById('mimsADLog').value;
                var noti = document.getElementById('mimsADNoti').value;
                var offence = document.getElementById('mimsADTB').value;
                var pOrder = document.getElementById('mimsADPO').value;
                var dRoster = document.getElementById('mimsADDR').value;
                var transLog = document.getElementById('mimsADTL').value;

                var Isware = document.getElementById('mimsADWarehouseState').checked;
                var ware = document.getElementById('mimsADWarehouse').value;

                var isErr = false;
                if (!isNumeric(ware)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for logs")
                }
                if (!isNumeric(transLog)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for logs")
                }
                if (!isNumeric(dRoster)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for duty roster")
                }
                if (!isNumeric(pOrder)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for post order")
                }
                if (!isNumeric(offence)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for offences")
                }
                if (!isNumeric(noti)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for notifications")
                }
                if (!isNumeric(noti)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for notifications")
                }
                if (!isNumeric(mlog)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for logs")
                }
                if (!isNumeric(taskA)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for attachments")
                }
                if (!isNumeric(checklist)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for checklist")
                }
                if (!isNumeric(location)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for location")
                }
                if (!isNumeric(asset)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for asset")
                }
                if (!isNumeric(user)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for user")
                }
                if (!isNumeric(cusev)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for custom event")
                }
                if (!isNumeric(task)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for task")
                }
                if (!isNumeric(verifier)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for verifier")
                }
                if (!isNumeric(rem)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for reminder")
                }
                if (!isNumeric(group)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for group")
                }
                if (!isErr) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Devices.aspx/saveAuditSettings",
                        data: "{'interval':'" + interval + "','asset':'" + asset + "','cusev':'" + cusev + "','taskA':'" + taskA +
                            "','checklist':'" + checklist + "','dRoster':'" + dRoster + "','group':'" + group + "','location':'" + location +
                            "','mlog':'" + mlog + "','noti':'" + noti + "','pOrder':'" + pOrder + "','rem':'" + rem +
                            "','task':'" + task + "','offence':'" + offence + "','transLog':'" + transLog + "','user':'" + user +
                            "','verifier':'" + verifier + "','Isinterval':'0','Isasset':'" + Isasset + "','Iscusev':'" + Iscusev +
                            "','IstaskA':'" + IstaskA + "','Ischecklist':'" + Ischecklist + "','IsdRoster':'" + IsdRoster + "','Isgroup':'" + Isgroup +
                            "','Islocation':'" + Islocation + "','Ismlog':'" + Ismlog + "','Isnoti':'" + Isnoti + "','IspOrder':'" + IspOrder +
                            "','Isrem':'" + Isrem + "','Istask':'" + Istask + "','Isoffence':'" + Isoffence + "','IstransLog':'" + IstransLog +
                            "','Isuser':'" + Isuser + "','Isverifier':'" + Isverifier + "','uname':'" + loggedinUsername + "','Iswarehouse':'" + Isware + "','warehouse':'" + ware + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "SUCCESS") {
                                document.getElementById('successMessage').innerHTML = "Audit settings has successfully been saved";
                                jQuery('#successfulModal').modal('show');
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            } catch (err)
            { showAlert('Error 60: Problem loading page element.-' + err) }

        }
        function saveHouseKeepingSettings() {
            try{
                var interval = document.getElementById('mimsHKInterval').value;            
                var asset = document.getElementById('mimsHKAsset').value;
                var user = document.getElementById('mimsHKUsers').value;
                var cusev = document.getElementById('mimsHKAlarms').value;
                var task = document.getElementById('mimsHKTasks').value;
                var verifier = document.getElementById('mimsHKVerifier').value;
                var rem = document.getElementById('mimsHKReminders').value;
                var group = document.getElementById('mimsHKGroups').value;
                var location = document.getElementById('mimsHKLocations').value;
                var checklist = document.getElementById('mimsHKChecklists').value;
                var taskA = document.getElementById('mimsHKFiles').value;
                var mlog = document.getElementById('mimsHKLog').value;
                var noti = document.getElementById('mimsHKNoti').value;
                var offence = document.getElementById('mimsHKTB').value;
                var pOrder = document.getElementById('mimsHKPO').value;
                var dRoster = document.getElementById('mimsHKDR').value;
                var transLog = document.getElementById('mimsHKTL').value;

                var Isasset = document.getElementById('mimsHKAssetState').checked;
                var Isuser = document.getElementById('mimsHKUsersState').checked;
                var Iscusev = document.getElementById('mimsHKAlarmsState').checked;
                var Istask = document.getElementById('mimsHKTasksState').checked;
                var Isverifier = document.getElementById('mimsHKVerifierState').checked;
                var Isrem = document.getElementById('mimsHKRemindersState').checked;
                var Isgroup = document.getElementById('mimsHKGroupsState').checked;
                var Islocation = document.getElementById('mimsHKLocationsState').checked;
                var Ischecklist = document.getElementById('mimsHKChecklistsState').checked;
                var IstaskA = document.getElementById('mimsHKFilesState').checked;
                var Ismlog = document.getElementById('mimsHKLogsState').checked;
                var Isnoti = document.getElementById('mimsHKNotiState').checked;
                var Isoffence = document.getElementById('mimsHKTBState').checked;
                var IspOrder = document.getElementById('mimsHKPOState').checked;
                var IsdRoster = document.getElementById('mimsHKDRState').checked;
                var IstransLog = document.getElementById('mimsHKTLState').checked;

                var Isware = document.getElementById('mimsHKWarehouseState').checked;
                var ware = document.getElementById('mimsHKWarehouse').value;

                var isErr = false;
                if (!isNumeric(ware)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for logs")
                }
                if (!isNumeric(transLog)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for logs")
                }
                if (!isNumeric(dRoster)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for duty roster")
                }
                if (!isNumeric(pOrder)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for post order")
                }
                if (!isNumeric(offence)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for offences")
                }
                if (!isNumeric(noti)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for notifications")
                }
                if (!isNumeric(noti)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for notifications")
                }
                if (!isNumeric(mlog)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for logs")
                }
                if (!isNumeric(taskA)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for attachments")
                }
                if (!isNumeric(checklist)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for checklist")
                }
                if (!isNumeric(location)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for location")
                }
                if (!isNumeric(asset)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for asset")
                }
                if (!isNumeric(user)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for user")
                }
                if (!isNumeric(cusev)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for custom event")
                }
                if (!isNumeric(task)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for task")
                }
                if (!isNumeric(verifier)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for verifier")
                }
                if (!isNumeric(rem)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for reminder")
                }
                if (!isNumeric(group)) {
                    isErr = true;
                    showAlert("Kindly enter numeric value for group")
                }
                if (!isErr) {

                    jQuery.ajax({
                        type: "POST",
                        url: "Devices.aspx/saveHouseKeepingSettings",
                        data: "{'interval':'" + interval + "','asset':'" + asset + "','cusev':'" + cusev + "','taskA':'" + taskA +
                            "','checklist':'" + checklist + "','dRoster':'" + dRoster + "','group':'" + group + "','location':'" + location +
                            "','mlog':'" + mlog + "','noti':'" + noti + "','pOrder':'" + pOrder + "','rem':'" + rem +
                            "','task':'" + task + "','offence':'" + offence + "','transLog':'" + transLog + "','user':'" + user +
                            "','verifier':'" + verifier + "','Isinterval':'0','Isasset':'" + Isasset + "','Iscusev':'" + Iscusev +
                            "','IstaskA':'" + IstaskA + "','Ischecklist':'" + Ischecklist + "','IsdRoster':'" + IsdRoster + "','Isgroup':'" + Isgroup +
                            "','Islocation':'" + Islocation + "','Ismlog':'" + Ismlog + "','Isnoti':'" + Isnoti + "','IspOrder':'" + IspOrder +
                            "','Isrem':'" + Isrem + "','Istask':'" + Istask + "','Isoffence':'" + Isoffence + "','IstransLog':'" + IstransLog +
                            "','Isuser':'" + Isuser + "','Isverifier':'" + Isverifier + "','uname':'" + loggedinUsername + "','Iswarehouse':'" + Isware + "','warehouse':'" + ware + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "SUCCESS") {
                                document.getElementById('successMessage').innerHTML = "House keeping settings has successfully been saved";
                                jQuery('#successfulModal').modal('show');
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }catch(err)
            { showAlert('Error 60: Problem loading page element.-'+err) }

        }
        function userChangeStatus(id, state) {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/changeUserState",
                data: "{'id':'" + id + "','status':'" + state + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 1)
                        document.getElementById(data.d[0]).innerHTML = data.d[1];
                    else if (data.d[0] == "LOGOUT")
                    {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function devChangeStatus(id, state) {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/changeDevState",
                data: "{'id':'" + id + "','status':'" + state + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 1)
                        document.getElementById(data.d[0]).innerHTML = data.d[1];
                    else if (data.d[0] == "LOGOUT")
                    {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function deleteWhitelist()
        {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/deleteWhitelist",
                data: "{'id':'" + document.getElementById('tbWhitelistMacAddressHolder').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successMessage').innerHTML = "Entry has successfully been deleted";
                        jQuery('#successfulModal').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function deleteThird() {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/deleteThirdParty",
                data: "{'id':'" + document.getElementById('tbthirdChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successMessage').innerHTML = "Entry has successfully been deleted";
                        jQuery('#successfulModal').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function deleteDeviceData() {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/deleteDevice",
                data: "{'id':'" + document.getElementById('tbDeviceMac').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if(data.d == "SUCCESS")
                    {
                        document.getElementById('successMessage').innerHTML = "Device has successfully been deleted";
                        jQuery('#successfulModal').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function deleteUserData() {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/deleteUser",
                data: "{'id':'" + document.getElementById('tbUserId').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successMessage').innerHTML = "User has successfully been deleted";
                        jQuery('#successfulModal').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function getsettings() {
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/getSettingsData",
                data: "{'id':'1','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById('mimsServerPort').value = data.d[1];
                    document.getElementById('mimsServerCon').value = data.d[2];
                    document.getElementById('mimsServerIP').value = data.d[0];
                    document.getElementById('mimsServerRTSP').value = data.d[3];
                    document.getElementById('mimsServerVideo').value = data.d[4];
                    document.getElementById('mimsServerSignalR').value = data.d[5];
                    document.getElementById('mimsServerAlarmPort').value = data.d[6];

                    document.getElementById('mimsClientVidClip').value = data.d[7];
                    if (data.d[8] == "True") {
                        jQuery('#mimsClientVidClipState').click();
                    }
                    document.getElementById('mimsClientPass').value = data.d[9];
                    if (data.d[10] == "True") {
                        jQuery('#mimsClientPassState').click();
                    }
                    document.getElementById('mimsClientNoImages').value = data.d[11];
                    if (data.d[12] == "True") {
                        jQuery('#mimsClientNoImagesState').click();
                    }
                    document.getElementById('mimsClient3rdWeb').value = data.d[13];
                    document.getElementById('mimsClient3rdDB').value = data.d[14];
                    if (data.d[15] == "True") {
                        jQuery('#mimsClient3rdDBState').click();
                    }
                    document.getElementById('mimsClient3rdGPSServ').value = data.d[16];
                    document.getElementById('mimsClient3rdGPSApp').value = data.d[17];
                    document.getElementById('mimsClientGPSInt').value = data.d[18];
                    if (data.d[19] == "True") {
                        jQuery('#mimsClientGPSIntState').click();
                    }
                    document.getElementById('mimsClientTransferInt').value = data.d[20];
                    if (data.d[21] == "True") {
                        jQuery('#mimsClientTransferIntState').click();
                    }
                    document.getElementById('mimsMobileGPSInt').value = data.d[22];
                    document.getElementById('mimsMobileURLLink').value = data.d[23];
                    document.getElementById('mimsMobileMaxAttachments').value = data.d[24];
                    document.getElementById('mimsMobileMaxUp').value = data.d[25];

                    document.getElementById('mimsMOCPort').value = data.d[26];
                    document.getElementById('mimsMOCHealthCheckInt').value = data.d[27];
                    if (data.d[28] == "True") {
                        jQuery('#mimsMOCHealthCheckIntState').click();
                    }
                    document.getElementById('mimsHKInterval').value = data.d[29];
                    document.getElementById('mimsHKAsset').value = data.d[30];
                    document.getElementById('mimsHKAlarms').value = data.d[31];
                    document.getElementById('mimsHKFiles').value = data.d[32];
                    document.getElementById('mimsHKChecklists').value = data.d[33];
                    document.getElementById('mimsHKDR').value = data.d[34];
                    document.getElementById('mimsHKGroups').value = data.d[35];
                    document.getElementById('mimsHKLocations').value = data.d[36];
                    document.getElementById('mimsHKLog').value = data.d[37];
                    document.getElementById('mimsHKNoti').value = data.d[38];
                    document.getElementById('mimsHKPO').value = data.d[39];
                    document.getElementById('mimsHKReminders').value = data.d[40];
                    document.getElementById('mimsHKTasks').value = data.d[41];
                    document.getElementById('mimsHKTB').value = data.d[42];
                    document.getElementById('mimsHKTL').value = data.d[43];
                    document.getElementById('mimsHKUsers').value = data.d[44];
                    document.getElementById('mimsHKVerifier').value = data.d[45];

                    document.getElementById('mimsADAsset').value = data.d[62];
                    document.getElementById('mimsADAlarms').value = data.d[63];
                    document.getElementById('mimsADFiles').value = data.d[64];
                    document.getElementById('mimsADChecklists').value = data.d[65];
                    document.getElementById('mimsADDR').value = data.d[66];
                    document.getElementById('mimsADGroups').value = data.d[67];
                    document.getElementById('mimsADLocations').value = data.d[68];
                    document.getElementById('mimsADLog').value = data.d[69];
                    document.getElementById('mimsADNoti').value = data.d[70];
                    document.getElementById('mimsADPO').value = data.d[71];
                    document.getElementById('mimsADReminders').value = data.d[72];
                    document.getElementById('mimsADTasks').value = data.d[73];
                    document.getElementById('mimsADTB').value = data.d[74];
                    document.getElementById('mimsADTL').value = data.d[75];
                    document.getElementById('mimsADUsers').value = data.d[76];
                    document.getElementById('mimsADVerifier').value = data.d[77];

                    if (data.d[46] == "True") {
                        jQuery('#mimsHKAssetState').click();
                    }
                    if (data.d[47] == "True") {
                        jQuery('#mimsHKAlarmsState').click();
                    }
                    if (data.d[48] == "True") {
                        jQuery('#mimsHKFilesState').click();
                    }
                    if (data.d[49] == "True") {
                        jQuery('#mimsHKChecklistsState').click();
                    }
                    if (data.d[50] == "True") {
                        jQuery('#mimsHKDRState').click();
                    }
                    if (data.d[51] == "True") {
                        jQuery('#mimsHKGroupsState').click();
                    }
                    if (data.d[52] == "True") {
                        jQuery('#mimsHKLocationsState').click();
                    }
                    if (data.d[53] == "True") {
                        jQuery('#mimsHKLogState').click();
                    }
                    if (data.d[54] == "True") {
                        jQuery('#mimsHKNotiState').click();
                    }
                    if (data.d[55] == "True") {
                        jQuery('#mimsHKPOState').click();
                    }
                    if (data.d[56] == "True") {
                        jQuery('#mimsHKRemindersState').click();
                    }
                    if (data.d[57] == "True") {
                        jQuery('#mimsHKTasksState').click();
                    }
                    if (data.d[58] == "True") {
                        jQuery('#mimsHKTBState').click();
                    }
                    if (data.d[59] == "True") {
                        jQuery('#mimsHKTLState').click();
                    }
                    if (data.d[60] == "True") {
                        jQuery('#mimsHKUsersState').click();
                    }
                    if (data.d[61] == "True") {
                        jQuery('#mimsHKVerifierState').click();
                    }

                    if (data.d[78] == "True") {
                        jQuery('#mimsADAssetState').click();
                    }
                    if (data.d[79] == "True") {
                        jQuery('#mimsADAlarmsState').click();
                    }
                    if (data.d[80] == "True") {
                        jQuery('#mimsADFilesState').click();
                    }
                    if (data.d[81] == "True") {
                        jQuery('#mimsADChecklistsState').click();
                    }
                    if (data.d[82] == "True") {
                        jQuery('#mimsADDRState').click();
                    }
                    if (data.d[83] == "True") {
                        jQuery('#mimsADGroupsState').click();
                    }
                    if (data.d[84] == "True") {
                        jQuery('#mimsADLocationsState').click();
                    }
                    if (data.d[85] == "True") {
                        jQuery('#mimsADLogState').click();
                    }
                    if (data.d[86] == "True") {
                        jQuery('#mimsADNotiState').click();
                    }
                    if (data.d[87] == "True") {
                        jQuery('#mimsADPOState').click();
                    }
                    if (data.d[88] == "True") {
                        jQuery('#mimsADRemindersState').click();
                    }
                    if (data.d[89] == "True") {
                        jQuery('#mimsADTasksState').click();
                    }
                    if (data.d[90] == "True") {
                        jQuery('#mimsADTBState').click();
                    }
                    if (data.d[91] == "True") {
                        jQuery('#mimsADTLState').click();
                    }
                    if (data.d[92] == "True") {
                        jQuery('#mimsADUsersState').click();
                    }
                    if (data.d[93] == "True") {
                        jQuery('#mimsADVerifierState').click();
                    }

                    document.getElementById('mimsVerFRS').value = data.d[94];
                    document.getElementById('mimsVerANPR').value = data.d[95];
                    if (data.d[96] == "true") {
                        document.getElementById('serverProtocolHttps').checked = true;
                    }
                    else {
                        document.getElementById('serverProtocolHttp').checked = true;
                    }

                    document.getElementById('mimsHKWarehouse').value = data.d[97];
                    if (data.d[98] == "True") {
                        jQuery('#mimsHKWarehouseState').click();
                    }
                    document.getElementById('mimsADWarehouse').value = data.d[99];
                    if (data.d[100] == "True") {
                        jQuery('#mimsADWarehouseState').click();
                    }
                }
            });
        }
        function whitelistrowchoice(macadd)
        {
            document.getElementById('tbEditWhitelistMacAddress').value = macadd;
            document.getElementById('tbWhitelistMacAddressHolder').value = macadd;
        }
        function thirdrowchoice(macadd) {
            document.getElementById('tbthirdChoice').value = macadd;
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/getThirdPartyDataInfo",
                data: "{'id':'" + macadd + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('tbnew3rdPartyName').value = data.d[0];
                        document.getElementById('tbnew3rdPartyIP').value = data.d[1];
                        document.getElementById('tbnew3rdPartyPORT').value = data.d[2];
                        document.getElementById('tbnew3rdPartyUsername').value = data.d[3];
                        document.getElementById('tbnew3rdPartyPassword').value = data.d[4];
                        document.getElementById('MainContent_thirdPartytypeSelect').value = data.d[5];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function rowchoiceUser(macadd) {
            document.getElementById('tbUserId').value = macadd;
        }
        function rowchoiceDevice(macadd) {
            document.getElementById('tbDeviceMac').value = macadd;
        }
        function addrowtoDevicesTable() {
            jQuery("#healthcheckdevicesTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/getTableDataDevices",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#devicesTable tbody").append(data.d[i]);
                            jQuery("#healthcheckdevicesTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoUsersTable() {
            jQuery("#healthcheckusersTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/getTableDataUsers",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#usersTable tbody").append(data.d[i]);
                            jQuery("#healthcheckusersTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoWhitelistTable() {
            jQuery("#whitelistdevicesTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/getTableDataWhitelist",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#whitelistdevicesTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoThirdParty() {
            jQuery("#thirdpartysystemstable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Devices.aspx/getTableDataThirdParty",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#thirdpartysystemstable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        
    </script>
            <section class="content-wrapper" role="main">
            <div class="content">
                <divz class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">DEVICES</span></h3>
                                </div>
                                <div class="col-md-8">
                                    <div class="panel-control">
                                        <ul class="nav nav-tabs nav-main">
                                            <li class="active"><a data-toggle="tab" href="#home-tab" onclick="jQuery('.show-component').show();">Device List</a>
                                            </li>
                                            <li><a data-toggle="tab" href="#settings-tab" onclick="dropzoneUp(),jQuery('.show-component').show();">Settings</a>
                                            </li>
                                            <li ><a data-toggle="tab" href="#whitelist-tab" onclick="jQuery('.show-component').show();">Whitelist</a>
                                            </li>
                                            <li style="display:<%=userinfoDisplay%>"><a data-toggle="tab" href="#thirdparty-tab" onclick="jQuery('.show-component').show();">3rd Party Systems</a>
                                            </li>
                                            <li style="display:<%=userinfoDisplay%>"><a data-toggle="tab" href="#system-tab" onclick="uploaddropzoneUp(),jQuery('.show-component').show();">Information</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div role="group" class="pull-right">
                                        <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                    <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                               </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            		<div class="tab-content">
							<div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li ><a href="#component-clientdevices" data-toggle="tab">MIMS Client</a>
                                                    </li>
                                                    <li><a href="#component-mobiledevices" data-toggle="tab">MIMS Mobile</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-clientdevices">
                                            <div class="col-md-12" >
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MIMS CLIENT DEVICES</h3>
                                                           <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="devicesTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">PC NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">MAC ADDRESS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">LAST LOGIN<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-mobiledevices">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MIMS MOBILE USERS</h3>
                                                         <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" order-of-rows="desc" order-of-column="0" id="usersTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">USER NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">DEVICE TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">LAST LOGIN<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
							</div>
                           <div class="tab-pane fade" id="settings-tab">
                           <div class="tab-content">
                              <div class="row mb-4x">
                                 <div class="col-md-2">
                                    <div class="row vertical-navigation vertical-components-show">
                                       <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#component-mimsserver" data-toggle="tab">MIMS Server</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#component-mimsclient" data-toggle="tab">MIMS Client</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#component-mimsmobile" data-toggle="tab">MIMS Mobile</a>
                                                    </li>
                                                    <li style="display:none;"><a href="#component-moc" data-toggle="tab">MOC</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#component-housekeeping" data-toggle="tab">Housekeeping</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#component-audit" data-toggle="tab">Audit</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#component-verifier" data-toggle="tab">Verifier</a>
                                                    </li>
                                                </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-10">
                                    <div class="row show-component component-mimsserver">
                                       <div class="col-md-12" style="display:<%=userinfoDisplay%>">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">MIMS SERVER SETTING</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">SERVER PROTOCOL:</h3>
                                                      </div>
                                                      <div class="col-md-9" style="margin-top:10px;">
                                  <div class="nice-radio inline-block no-vmargin">
                                    <input type="radio"  id="serverProtocolHttp"  name="niceRadio">
                                    <label for="serverProtocolHttp" class="vertical-align-top">HTTP</label>
                                 </div>
                                  <div class="nice-radio inline-block no-vmargin">
                                 <input type="radio" id="serverProtocolHttps"  name="niceRadio">
                                    <label for="serverProtocolHttps" class="vertical-align-top">HTTPS</label>
                                 </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">SERVER PORT:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsServerPort">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">SERVER IP:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsServerIP">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MIMS API CONNECTION:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsServerCon">
                                                              </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">RTSP PORT:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsServerRTSP">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">VIDEO STREAM PORT:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsServerVideo">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MESSAGING SERVICE:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsServerSignalR">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MOC PORT:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsServerAlarmPort">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row horizontal-navigation mt-4x pull-right">
                                                   <div class="panel-control">
                                                      <ul class="nav nav-tabs">
                                                         <li><a href="#" onclick="getsettings()">RESET</a>
                                                         </li>
                                                         <li class="active"><a href="#" onclick="saveServerSettings()">SAVE CHANGES</a>
                                                         </li>
                                                      </ul>
                                                      <!-- /.nav -->
                                                   </div>
                                                </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div class="row show-component component-mimsclient">
                                       <div class="col-md-12" style="display:<%=userinfoDisplay%>">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">MIMS CLIENT SETTING</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">VIDEO CLIP LENGTH(s):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClientVidClip">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsClientVidClipState" data-color="#3ebb64" >
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">SETTINGS PASSWORD:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClientPass">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsClientPassState"  data-color="#3ebb64"   >
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">RECORDING IMAGES:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClientNoImages">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsClientNoImagesState" data-color="#3ebb64" >
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">3RD PARTY WEBPORTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClient3rdWeb">
                                                             </div>
                                                      </div>
                                                </div>
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">3RD PARTY DATABASE:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClient3rdDB">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsClient3rdDBState" data-color="#3ebb64" >
                                                         </div>
                                                      </div>
                                                </div>
                                                                                                  <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">3RD PARTY GPS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClient3rdGPSServ">
                                                              </div>
                                                      </div>
                                                </div>
                                                    <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">3RD APP LAUNCH:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClient3rdGPSApp">
                                                                  </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">GPS UPDATE INTERVAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClientGPSInt">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsClientGPSIntState" data-color="#3ebb64"  >
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">FILE TRANSFER INTERVAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsClientTransferInt">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsClientTransferIntState" data-color="#3ebb64"  >
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row horizontal-navigation mt-4x pull-right">
                                                   <div class="panel-control">
                                                      <ul class="nav nav-tabs">
                                                         <li><a href="#" onclick="getsettings()">RESET</a>
                                                         </li>
                                                         <li class="active"><a href="#" onclick="saveClientSettings()">SAVE CHANGES</a>
                                                         </li>
                                                      </ul>
                                                      <!-- /.nav -->
                                                   </div>
                                                </div>
                                                                                                                                                
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div class="row show-component component-mimsmobile">
                                       <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">MIMS MOBILE SETTING</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="display:<%=userinfoDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">GPS UPDATE INTERVAL(s):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsMobileGPSInt">
                                                            </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=userinfoDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">URL LINK:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div  class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsMobileURLLink">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=userinfoDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MAX ATTACHMENTS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsMobileMaxAttachments">
                                                              </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=userinfoDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MAX UPLOAD SIZE(mb):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsMobileMaxUp">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">DASHBOARD IMAGE (1920x1080):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                      <div style="height:50px;" enctype="multipart/form-data" id="dz-test" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                                        <div class="dz-message">
                                                          <h1>DRAG & DROP</h1>
                                                        </div>
                                                      </div>
                                                      </div>
                                                </div>
                                                <div class="row horizontal-navigation mt-4x pull-right" style="display:<%=userinfoDisplay%>">
                                                   <div class="panel-control">
                                                      <ul class="nav nav-tabs">
                                                         <li><a href="#" onclick="getsettings()">RESET</a>
                                                         </li>
                                                         <li class="active"><a href="#" onclick="saveMobileSettings()">SAVE CHANGES</a>
                                                         </li>
                                                      </ul>
                                                      <!-- /.nav -->
                                                   </div>
                                                </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div style="display:none" class="row show-component component-moc">
                                       <div class="col-md-12" style="display:none;">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">MOC SETTING</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div style="display:none;" class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MOC PORT:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsMOCPort">
                                                             </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">HEALTH CHECK INTERVAL(h):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsMOCHealthCheckInt">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsMOCHealthCheckIntState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row horizontal-navigation mt-4x pull-right">
                                                   <div class="panel-control">
                                                      <ul class="nav nav-tabs">
                                                         <li><a href="#" onclick="getsettings()">RESET</a>
                                                         </li>
                                                         <li class="active"><a href="#" onclick="saveMOCSettings()">SAVE CHANGES</a>
                                                         </li>
                                                      </ul>
                                                      <!-- /.nav -->
                                                   </div>
                                                </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div class="row show-component component-housekeeping">
                                       <div class="col-md-12" style="display:<%=userinfoDisplay%>">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">HOUSE KEEPING SETTINGS</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">HOUSE KEEPING INTERVAL(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKInterval">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=ASSETSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">ASSETS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKAsset">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKAssetState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USERS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKUsers">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKUsersState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=ALARMSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">ALARMS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKAlarms">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKAlarmsState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=TASKSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TASKS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKTasks">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKTasksState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=VERIFIERDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">VERIFIER:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKVerifier">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKVerifierState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMINDERS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKReminders">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKRemindersState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=GROUPSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">GROUPS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKGroups">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKGroupsState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=LOCATIONSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">LOCATIONS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKLocations">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKLocationsState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=TASKSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">CHECKLISTS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKChecklists">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKChecklistsState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">ATTACHMENT FILES:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKFiles">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKFilesState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">ERROR LOG:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKLog">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKLogsState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=NOTIFICATIONDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">NOTIFICATION:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKNoti">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKNotiState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=TICKETINGDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TICKETING DATABASE:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKTB">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsHKTBState" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TRANSACTION LOGS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKTL">
                                                             <input type="checkbox" class="js-switch" id="mimsHKTLState" data-class-name="switchery switchery-alt" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=DUTYDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">DUTY ROSTERS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKDR">
                                                             <input type="checkbox" class="js-switch" id="mimsHKDRState" data-class-name="switchery switchery-alt" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=POSTDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">POST ORDERS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKPO">
                                                             <input type="checkbox" class="js-switch" id="mimsHKPOState" data-class-name="switchery switchery-alt" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=WAREHOUSEDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">WAREHOUSE:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsHKWarehouse">
                                                             <input type="checkbox" class="js-switch" id="mimsHKWarehouseState" data-class-name="switchery switchery-alt" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row horizontal-navigation mt-4x pull-right">
                                                   <div class="panel-control">
                                                      <ul class="nav nav-tabs">
                                                         <li><a href="#" onclick="getsettings()">RESET</a>
                                                         </li>
                                                         <li class="active"><a href="#" onclick="saveHouseKeepingSettings()">SAVE CHANGES</a>
                                                         </li>
                                                      </ul>
                                                      <!-- /.nav -->
                                                   </div>
                                                </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div class="row show-component component-audit">
                                       <div class="col-md-12" style="display:<%=userinfoDisplay%>">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">AUDIT SETTINGS</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="display:<%=ASSETSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">ASSETS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADAsset">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADAssetState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USERS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADUsers">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADUsersState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=ALARMSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">ALARMS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADAlarms">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADAlarmsState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=TASKSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TASKS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADTasks">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADTasksState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=VERIFIERDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">VERIFIER(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADVerifier">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADVerifierState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMINDERS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADReminders">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADRemindersState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=GROUPSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">GROUPS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADGroups">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADGroupsState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=LOCATIONSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">LOCATIONS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADLocations">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADLocationsState" data-color="#3ebb64"  >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=TASKSDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">CHECKLISTS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADChecklists">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADChecklistsState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">ATTACHMENT FILES(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADFiles">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADFilesState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MIMS LOG(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADLog">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADLogsState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=NOTIFICATIONDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">NOTIFICATION(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADNoti">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADNotiState" data-color="#3ebb64" >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=TICKETINGDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TICKETING DATABASE(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADTB">
                                                             <input type="checkbox" class="js-switch" data-class-name="switchery switchery-alt" id="mimsADTBState" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TRANSACTION LOGS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADTL">
                                                             <input type="checkbox" class="js-switch" id="mimsADTLState" data-class-name="switchery switchery-alt" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=DUTYDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">DUTY ROSTERS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADDR">
                                                             <input type="checkbox" class="js-switch" id="mimsADDRState" data-class-name="switchery switchery-alt" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=POSTDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">POST ORDERS(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADPO">
                                                             <input type="checkbox" class="js-switch" id="mimsADPOState" data-class-name="switchery switchery-alt" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x" style="display:<%=WAREHOUSEDisplay%>">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">WAREHOUSE(days):</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsADWarehouse">
                                                             <input type="checkbox" class="js-switch" id="mimsADWarehouseState" data-class-name="switchery switchery-alt" data-color="#3ebb64"   >
                                                         
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row horizontal-navigation mt-4x pull-right">
                                                   <div class="panel-control">
                                                      <ul class="nav nav-tabs">
                                                         <li><a href="#" onclick="getsettings()">RESET</a>
                                                         </li>
                                                         <li class="active"><a href="#" onclick="saveAuditSettings()">SAVE CHANGES</a>
                                                         </li>
                                                      </ul>
                                                      <!-- /.nav -->
                                                   </div>
                                                </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div class="row show-component component-verifier">
                                       <div class="col-md-12" style="display:<%=userinfoDisplay%>">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">VERIFIER SETTING</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">FRS Service URL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsVerFRS">
                                                             </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">ANPR Service URL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mimsVerANPR">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row horizontal-navigation mt-4x pull-right">
                                                   <div class="panel-control">
                                                      <ul class="nav nav-tabs">
                                                         <li><a href="#" onclick="getsettings()">RESET</a>
                                                         </li>
                                                         <li class="active"><a href="#" onclick="saveVerifierSettings()">SAVE CHANGES</a>
                                                         </li>
                                                      </ul>
                                                      <!-- /.nav -->
                                                   </div>
                                                </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                            <div class="tab-pane fade" id="whitelist-tab">
                               <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                          <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active" style="display:<%=userinfoDisplay%>"><a href="#" id="firstModal" data-target="#newWhiteListModal" data-toggle="modal" class="capitalize-text">+ NEW DEVICE</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>   
                                    </div>
                                    <div class="col-md-10">
                                       <div class="row show-component component-number-1">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">DEVICE WHITELIST</h3>
                                                                  <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="whitelistdevicesTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">MACADDRESS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">CREATED DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="thirdparty-tab">
                               <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                          <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#" data-target="#new3rdPartySystemModal" data-toggle="modal" class="capitalize-text">+ NEW SYSTEM</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>   
                                    </div>
                                    <div class="col-md-10">
                                       <div class="row show-component component-number-1">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">THIRD PARTY SYSTEMS</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="thirdpartysystemstable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">IP<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">PORT<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">USERNAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
							</div>
                                                                                    <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                            <div class="tab-pane fade" id="system-tab">
                            <div class="tab-content">
                              <div class="row mb-4x">
                                 <div class="col-md-2">
                                    <div class="row vertical-navigation vertical-components-show">
                                       <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-serverinfo" data-toggle="tab">Server</a>
                                                    </li>
                                                    <li style="display:none;"><a href="#component-cllicenseinfo" data-toggle="tab">MIMS Client</a>
                                                    </li>
                                                    <li><a href="#component-moblicenseinfo" data-toggle="tab">MIMS User License</a>
                                                    </li>
                                                    <li><a href="#component-licence " data-toggle="tab">Licence Upload</a>
                                                    </li>
                                                </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-10">
                                    <div class="row show-component component-serverinfo">
                                       <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">SERVER</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">CLIENT ID:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="serverClientID" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">CLIENT NAME:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="serverClientName" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">CLIENT PHONE:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="serverClientPhone" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">CLIENT ADDRESS:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="serverClientAddress" readonly="readonly">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">SERIAL NUMBER:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="serverClientSerial" readonly="readonly">
                                                             </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">VERSION:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="serverClientVersion" readonly="readonly">
                                                             </div>
                                                      </div>
                                                </div>       
                                                 <div class="row mb-2x">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                     <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">Notification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Location</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                                     </div>
                                                 </div>                                                                        
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div class="row show-component component-cllicenseinfo" >
                                       <div class="col-md-12" style="display:none;">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">MIMS CLIENT LICENSE</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="clientTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="clientRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="clientUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>                                                                           
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div class="row show-component component-moblicenseinfo">
                                       <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">MIMS USER LICENSE</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>                                                                           
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                    </div>
                                    <div class="row show-component component-licence">
                                       <div class="col-md-12">
                                             <div style="height:50px;" enctype="multipart/form-data" id="license-upload" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                                        <div class="dz-message">
                                                          <h1>DRAG & DROP</h1>
                                                        </div>
                                             </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                                        </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </divz>
                <!-- /panel-body -->
            </div>
            <!-- /.panel -->
                <div aria-hidden="true" aria-labelledby="successfulModal" role="dialog" tabindex="-1" id="successfulModal" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" id="successMessage"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>
			<div aria-hidden="true" aria-labelledby="newWhiteListModal" role="dialog" tabindex="-1" id="newWhiteListModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">ADD MACADDRESS TO WHITELIST</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="MAC ADDRESS" id="tbWhitelistMacAddress" class="form-control">
									</div>
								</div>		
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li class="active"><a href="#" onclick="saveWhitelist()" class="capitalize-text" >ADD</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>		
            <div aria-hidden="true" aria-labelledby="new3rdPartySystemModal" role="dialog" tabindex="-1" id="new3rdPartySystemModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">3RD PARTY SYSTEM</h4>
					</div>
					<div class="modal-body">
                        <div class="row  mb-2x" id="typeSystemDiv">
                          <div class="col-md-12">
                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h3 class="mr-3x capitalize-text no-margin">TYPE</h3>
                                                </div>
                                                <div class="col-md-4">
                                                      <div class="nice-radio inline-block no-vmargin">
                                                        <input type="radio" checked="checked" id="existingSystemRadio" onclick="hideSystemDiv()"  name="niceRadio">
                                                        <label for="existingSystemRadio">Existing System Type</label>
                                                      </div><!--/nice-radio-->                                              
                                                </div>
                                                <div class="col-md-4">
                                                  <div class="nice-radio inline-block no-vmargin">
                                                    <input type="radio" id="newSystemRadio" onclick="showSystemDiv()" name="niceRadio">
                                                    <label for="newSystemRadio">New System Type</label>
                                                  </div><!--/nice-radio-->                                              
                                                </div>  
                                            </div>
                             </div>
                          </div>
                        </div>
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbnew3rdPartyName" class="form-control">
									</div>
								</div>		
                                <div class="row">
									<div class="col-md-12">
										<input placeholder="IP" id="tbnew3rdPartyIP" class="form-control">
									</div>
								</div>	
                                <div class="row">
									<div class="col-md-12">
										<input placeholder="Port" id="tbnew3rdPartyPORT" class="form-control">
									</div>
								</div>	
                                <div class="row">
									<div class="col-md-12">
										<input placeholder="Username" id="tbnew3rdPartyUsername" class="form-control">
									</div>
								</div>	
                                <div class="row">
									<div class="col-md-12">
										<input placeholder="Password" id="tbnew3rdPartyPassword" class="form-control">
									</div>
								</div>	
                                <div class="row">
									<div class="col-md-12" id="selectTypeDiv">
										  <label class="select select-o">
                                           <select runat="server" id="thirdPartytypeSelect">

											</select>
										 </label>
									</div>
                                    <div class="col-md-12" id="selectTypeNewDiv" style="display:none;">
	                                    <input placeholder="System Type" id="tbSystemTypeToAdd" class="form-control">
									</div>
								</div>	
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li class="active"><a href="#"  onclick="saveNew3rdParty()" class="capitalize-text" id="thirdpartysystemsave">ADD</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
			<div aria-hidden="true" aria-labelledby="newEditWhiteListModal" role="dialog" tabindex="-1" id="newEditWhiteListModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT MACADDRESS</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="MAC ADDRESS" id="tbEditWhitelistMacAddress" class="form-control">
									</div>
								</div>		
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li class="active"><a href="#"   onclick="saveWhitelistEdit()" class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	 
             <div aria-hidden="true" aria-labelledby="deleteModal" role="dialog" tabindex="-1" id="deleteModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a href="#" data-dismiss="modal" onclick="deleteWhitelist()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
             <div aria-hidden="true" aria-labelledby="deleteUserModal" role="dialog" tabindex="-1" id="deleteUserModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a href="#" data-dismiss="modal" onclick="deleteUserData()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
             <div aria-hidden="true" aria-labelledby="deleteDevModal" role="dialog" tabindex="-1" id="deleteDevModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a href="#" data-dismiss="modal" onclick="deleteDeviceData()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
             <div aria-hidden="true" aria-labelledby="deleteThirdModal" role="dialog" tabindex="-1" id="deleteThirdModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a href="#" data-dismiss="modal" onclick="deleteThird()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" style="color:gray" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
             <div aria-hidden="true" aria-labelledby="deviceProfileModal" role="dialog" tabindex="-1" id="deviceProfileModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">DEVICE INFO</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" <%=dispatchSurv%>>
                              <div class="col-md-9">
                                  <p class="font-bold red-color no-vmargin" >Assigned Cameras</p>
                                  
                                        <label class="select select-o">
                                         <select id="assignedCamera" >
                                         </select>
                                        </label>
                              </div>
                               <div class="col-md-3">
                                    <div style="margin-top:20px;" class="row horizontal-navigation">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li class="active"><a href="#" data-dismiss="modal" onclick="removeCamDevice()" >REMOVE</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                               </div>
                           </div>
                                                       <div class="row" <%=dispatchSurv%>>
                              <div class="col-md-9">
                                   <p class="font-bold red-color no-vmargin" >Add Cameras</p>
                                                                          <label class="select select-o">
                                         <select id="milestoneCamera" runat="server">
                                         </select>
                                        </label>
                              </div>
                                                              <div class="col-md-3">
                                    <div style="margin-top:20px;" class="row horizontal-navigation">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li class="active"><a href="#" onclick="addCamToDevice()" >ADD</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                               </div>
                           </div>       
                           <div class="row" style="display:none;">
                              <div class="col-md-9">
                                   <p class="font-bold red-color no-vmargin" >Assigned Supervisor</p>

                                                                                                            <label class="select select-o">
                                         <select id="assignManager" runat="server">
                                         </select>
                                        </label>
                              </div>
                                                              <div class="col-md-3">
                                    <div style="margin-top:20px;" class="row horizontal-navigation">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li class="active"><a href="#" data-dismiss="modal" onclick="assignMangToDevice()" >ASSIGN</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                               </div>
                           </div>     
    
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
                            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            		                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li class="active"><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <select style="color:white;background-color: #5D5D5D;width:125px;height:105px;margin-top:-15px;display:none;" multiple="multiple" id="sendToListBox" ></select>				  
              <input id="tbWhitelistMacAddressHolder" style="display:none;">
                <input id="tbthirdNewChoice" style="display:none;">
                <input id="tbthirdChoice" style="display:none;">
                <input id="tbDeviceMac" style="display:none;">
                <input id="tbUserId" style="display:none;">
                <asp:HiddenField runat="server" ID="tbUserID" />
                <asp:HiddenField runat="server" ID="tbUserName" />
                <input style="display:none;" id="imagePath" type="text"/>
             </section>
</asp:Content>
