﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class VehicleTypeAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int ID { get; set; }
        public string VehicleTypeDesc { get; set; }
        public string VehicleTypeDesc_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }

        public static VehicleTypeAudit GetVehicleTypeAuditFromSqlReader(SqlDataReader reader)
        {
            var vehicleTypeAudit = new VehicleTypeAudit();
            vehicleTypeAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                vehicleTypeAudit.ID = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("VehicleTypeDESC") && reader["VehicleTypeDESC"] != DBNull.Value)
                vehicleTypeAudit.VehicleTypeDesc = reader["VehicleTypeDESC"].ToString();
            if (columns.Contains("VehicleTypeDESC_AR") && reader["VehicleTypeDESC_AR"] != DBNull.Value)
                vehicleTypeAudit.VehicleTypeDesc_AR = reader["VehicleTypeDESC_AR"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                vehicleTypeAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                vehicleTypeAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                vehicleTypeAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return vehicleTypeAudit;
        }
        public static List<VehicleTypeAudit> GetAllVehicleTypeAudit(string dbConnection)
        {
            var vehicleTypeAuditList = new List<VehicleTypeAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleTypeAudit", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var vehicleTypeAudit = GetVehicleTypeAuditFromSqlReader(reader);
                    vehicleTypeAuditList.Add(vehicleTypeAudit);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleTypeAuditList;
        }
        public static bool InsertVehicleTypeAudit(VehicleTypeAudit vehicleTypeAudit, string dbConnection)
        {
            if (vehicleTypeAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertVehicleTypeAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(vehicleTypeAudit.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = vehicleTypeAudit.ID;

                    cmd.Parameters.Add("@VehicleTypeDESC", SqlDbType.NVarChar).Value = vehicleTypeAudit.VehicleTypeDesc;

                    cmd.Parameters.Add("@VehicleTypeDESC_AR", SqlDbType.NVarChar).Value = vehicleTypeAudit.VehicleTypeDesc_AR;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = vehicleTypeAudit.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = vehicleTypeAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = vehicleTypeAudit.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
