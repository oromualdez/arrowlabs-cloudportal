﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
  public  class PlateCode
  {
      public int ID { get; set; }
      public int Code { get; set; }
      public string Description { get; set; }
      public string Description_AR { get; set; }
      public string PlateSource { get; set; }
      public DateTime? CreatedDate { get; set; }
      public string UpdatedBy { get; set; }
      public DateTime? UpdatedDate { get; set; }
      public string CreatedBy { get; set; }
      public int CustomerId { get; set; }

      public int SiteId { get; set; }

      public static List<PlateCode> GetAllPlateCode(string dbConnection)
      {
          var plateCodes = new List<PlateCode>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetAllPlateCode", connection);


              sqlCommand.CommandText = "GetAllPlateCode";

              var reader = sqlCommand.ExecuteReader();
              while (reader.Read())
              {
                  var plateCode = new PlateCode();

                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32( reader["CODE"].ToString());
                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                  plateCodes.Add(plateCode);
              }
              connection.Close();
              reader.Close();
          }
          return plateCodes;
      }

      public static List<PlateCode> GetAllPlateCodeByPlateSource(int id, string dbConnection)
      {
          var plateCodes = new List<PlateCode>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetAllPlateCodeByPlateSource", connection);
              sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
              sqlCommand.Parameters["@Id"].Value = id;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              sqlCommand.CommandText = "GetAllPlateCodeByPlateSource";

              var reader = sqlCommand.ExecuteReader();
              while (reader.Read())
              {
                  var plateCode = new PlateCode();

                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());
                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                  plateCodes.Add(plateCode);
              }
              connection.Close();
              reader.Close();
          }
          return plateCodes;
      }

      public static List<PlateCode> GetAllPlateCodeByCId(int id,string dbConnection)
      {
          var plateCodes = new List<PlateCode>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetAllPlateCodeByCId", connection);
              sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
              sqlCommand.Parameters["@Id"].Value = id;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              sqlCommand.CommandText = "GetAllPlateCodeByCId";

              var reader = sqlCommand.ExecuteReader();
              while (reader.Read())
              {
                  var plateCode = new PlateCode();

                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());
                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                  plateCodes.Add(plateCode);
              }
              connection.Close();
              reader.Close();
          }
          return plateCodes;
      }

      public static List<PlateCode> GetAllPlateCodeBySiteId(int id, string dbConnection)
      {
          var plateCodes = new List<PlateCode>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetAllPlateCodeBySiteId", connection);
              sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
              sqlCommand.Parameters["@Id"].Value = id;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              sqlCommand.CommandText = "GetAllPlateCodeBySiteId";

              var reader = sqlCommand.ExecuteReader();
              while (reader.Read())
              {
                  var plateCode = new PlateCode();

                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());
                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                  plateCodes.Add(plateCode);
              }
              connection.Close();
              reader.Close();
          }
          return plateCodes;
      }

      public static List<PlateCode> GetAllPlateCodeByLevel5(int id, string dbConnection)
      {
          var plateCodes = new List<PlateCode>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetAllPlateCodeByLevel5", connection);
              sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
              sqlCommand.Parameters["@Id"].Value = id;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              sqlCommand.CommandText = "GetAllPlateCodeByLevel5";

              var reader = sqlCommand.ExecuteReader();
              while (reader.Read())
              {
                  var plateCode = new PlateCode();

                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());
                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                  plateCodes.Add(plateCode);
              }
              connection.Close();
              reader.Close();
          }
          return plateCodes;
      }

      public static List<PlateCode> GetAllPlateCodeByDate(DateTime updatedDate, string dbConnection)
      {
          var plateCodes = new List<PlateCode>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetAllPlateCodeByDate", connection);
              sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
              sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
              sqlCommand.CommandType = CommandType.StoredProcedure;

              sqlCommand.CommandText = "GetAllPlateCodeByDate";

              var reader = sqlCommand.ExecuteReader();
              while (reader.Read())
              {
                  var plateCode = new PlateCode();

                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());
                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                  plateCodes.Add(plateCode);
              }
              connection.Close();
              reader.Close();
          }
          return plateCodes;
      }

      public static int InsertorUpdatePlateCode(PlateCode plateCode, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
      {
          int id = 0;
          if (plateCode != null)
          {

              id = plateCode.ID;
              var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

              using (var connection = new SqlConnection(dbConnection))
              {
                  connection.Open();
                  var cmd = new SqlCommand("InsertOrUpdatePlateCode", connection);

                  cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                  cmd.Parameters["@Id"].Value = plateCode.ID;

                  cmd.Parameters.Add(new SqlParameter("@CODE", SqlDbType.Int));
                  cmd.Parameters["@CODE"].Value = plateCode.Code;

                  cmd.Parameters.Add(new SqlParameter("@PLATESOURCE", SqlDbType.NVarChar));
                  cmd.Parameters["@PLATESOURCE"].Value = plateCode.PlateSource;

                  cmd.Parameters.Add(new SqlParameter("@DESCRIPTION", SqlDbType.NVarChar));
                  cmd.Parameters["@DESCRIPTION"].Value = plateCode.Description;

                  cmd.Parameters.Add(new SqlParameter("@DESCRIPTION_AR", SqlDbType.NVarChar));
                  cmd.Parameters["@DESCRIPTION_AR"].Value = plateCode.Description_AR;

                  cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                  cmd.Parameters["@CreatedDate"].Value = plateCode.CreatedDate;

                  cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                  cmd.Parameters["@UpdatedDate"].Value = plateCode.UpdatedDate;

                  cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                  cmd.Parameters["@CreatedBy"].Value = plateCode.CreatedBy.ToLower();
                  cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                  cmd.Parameters["@CustomerId"].Value = plateCode.CustomerId;

                  cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                  cmd.Parameters["@SiteId"].Value = plateCode.SiteId;

                  var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                  returnParameter.Direction = ParameterDirection.ReturnValue;

                  cmd.CommandType = CommandType.StoredProcedure;

                  cmd.CommandText = "InsertOrUpdatePlateCode";

                  cmd.ExecuteNonQuery();

                  if (id == 0)
                      id = (int)returnParameter.Value;

                  try
                  {
                      if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                      {
                          plateCode.ID = id;
                          var audit = new PlateCodeAudit();
                          
                          audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, plateCode.UpdatedBy);
                          Reflection.CopyProperties(plateCode, audit);
                          PlateCodeAudit.InsertPlateCodeAudit(audit, auditDbConnection);
                      }
                  }
                  catch (Exception ex)
                  {
                      MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdatePlateCode", ex, auditDbConnection);
                  }

              }
          }
          return id;
      }

      public static bool DeletePlateCodeById(int colorCode, string dbConnection)
      {
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var cmd = new SqlCommand("DeletePlateCodeById", connection);

              cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
              cmd.Parameters["@Id"].Value = colorCode;

              cmd.CommandType = CommandType.StoredProcedure;

              cmd.CommandText = "DeletePlateCodeById";

              cmd.ExecuteNonQuery();
          }
          return true;
      }

      public static PlateCode GetPlateCodeByName(string name, string dbConnection)
      {
          PlateCode plateCode = null;
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetPlateCodeByName", connection);

              sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
              sqlCommand.Parameters["@Name"].Value = name;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              var reader = sqlCommand.ExecuteReader();

              while (reader.Read())
              {
                  plateCode = new PlateCode();
                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());
                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

              }
              connection.Close();
              reader.Close();
          }
          return plateCode;
      }

      public static PlateCode GetPlateCodeById(int id, string dbConnection)
      {
          PlateCode plateCode = null;
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetPlateCodeById", connection);

              sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
              sqlCommand.Parameters["@Id"].Value = id;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              var reader = sqlCommand.ExecuteReader();

              while (reader.Read())
              {
                  plateCode = new PlateCode();
                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());

                  plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

                  plateCode.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

              }
              connection.Close();
              reader.Close();
          }
          return plateCode;
      }

      public static PlateCode GetPlateCodeByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
      {
          PlateCode plateCode = null;
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetPlateCodeByNameAndCIdAndSiteId", connection);

              sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
              sqlCommand.Parameters["@Name"].Value = name;

              sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
              sqlCommand.Parameters["@Id"].Value = id;

              sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
              sqlCommand.Parameters["@SiteId"].Value = siteid;

              sqlCommand.CommandType = CommandType.StoredProcedure;
              var reader = sqlCommand.ExecuteReader();
              var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
              while (reader.Read())
              {
                  plateCode = new PlateCode();
                  plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
                  plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());
                  plateCode.Description = reader["DESCRIPTION"].ToString();
                  plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
                  plateCode.PlateSource = reader["PLATESOURCE"].ToString();
                  plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                  plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                  plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
              }
              connection.Close();
              reader.Close();
          }
          return plateCode;
      }

    }
}
