﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class LocationTypeAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int LocationTypeId { get; set; }
        public string LocationTypeDesc { get; set; }
        public string LocationTypeDesc_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }
        private static LocationTypeAudit GetLocationTypeAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var locationTypeAudit = new LocationTypeAudit();
            locationTypeAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("LOCATIONTYPEID") && reader["LOCATIONTYPEID"] != DBNull.Value)
                locationTypeAudit.LocationTypeId = Convert.ToInt16(reader["LOCATIONTYPEID"].ToString());
            if (columns.Contains("LOCATIONTYPEDESC") && reader["LOCATIONTYPEDESC"] != DBNull.Value)
                locationTypeAudit.LocationTypeDesc = reader["LOCATIONTYPEDESC"].ToString();
            if (columns.Contains("LOCATIONTYPEDESC_AR") && reader["LOCATIONTYPEDESC_AR"] != DBNull.Value)
                locationTypeAudit.LocationTypeDesc_AR = reader["LOCATIONTYPEDESC_AR"].ToString();
            if (columns.Contains("CreateDate") && reader["CreateDate"] != DBNull.Value)
                locationTypeAudit.CreatedDate = Convert.ToDateTime(reader["CreateDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                locationTypeAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                locationTypeAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            return locationTypeAudit;
        }
        public static List<LocationTypeAudit> GetAllLocationTypeAudit(string dbConnection)
        {
            var locationTypeAudits = new List<LocationTypeAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationType", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var locType = GetLocationTypeAuditFromSqlReader( reader);
                    locationTypeAudits.Add(locType);
                }
                connection.Close();
                reader.Close();
            }
            return locationTypeAudits;
        }

        public static bool InsertLocationTypeAudit(LocationTypeAudit locationType, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(locationType.BaseAudit, dbConnection);

            if (locationType != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertLocationTypeAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@LocationTypeId", SqlDbType.Int).Value = locationType.LocationTypeId;
                    cmd.Parameters.Add("@LocationTypeDesc", SqlDbType.NVarChar).Value = locationType.LocationTypeDesc;
                    cmd.Parameters.Add("@LocationTypeDesc_AR", SqlDbType.NVarChar).Value = locationType.LocationTypeDesc_AR;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = locationType.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = locationType.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = locationType.SiteId;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
