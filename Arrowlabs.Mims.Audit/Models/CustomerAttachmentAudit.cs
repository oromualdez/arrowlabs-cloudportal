﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class CustomerAttachmentAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string AttachmentPath { get; set; }

        public string DocumentName { get; set; }
        
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerInfoId { get; set; }

        public static bool InsertCustomerAttachmentAudit(CustomerAttachmentAudit customerAttachmentAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(customerAttachmentAudit.BaseAudit, dbConnection);

            if (customerAttachmentAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("InsertCustomerAttachmentAudit", connection);

                    cmd.Parameters.Add("@AuditBaseId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = customerAttachmentAudit.Id;

                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = customerAttachmentAudit.CustomerId;
                    cmd.Parameters.Add("@CustomerInfoId", SqlDbType.Int).Value = customerAttachmentAudit.CustomerInfoId;
                    
                    cmd.Parameters.Add("@AttachmentPath", SqlDbType.NVarChar).Value = customerAttachmentAudit.AttachmentPath;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = customerAttachmentAudit.SiteId;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = customerAttachmentAudit.CreatedBy;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = customerAttachmentAudit.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@DocumentName", SqlDbType.NVarChar));
                    cmd.Parameters["@DocumentName"].Value = customerAttachmentAudit.DocumentName;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            
            }
            return true;
        }
    }
}
