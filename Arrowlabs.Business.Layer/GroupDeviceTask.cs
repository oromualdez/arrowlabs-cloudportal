﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
  public class GroupDeviceTask
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public string DeviceName { get; set; }

        public string UpdatedBy { get; set; }
        public string GroupName { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int SiteId { get; set; }


        public static bool InsertOrUpdateGroupDeviceTask(GroupDeviceTask grpDevTask, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)

        {
            if (grpDevTask != null)
            {
                int id = grpDevTask.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateGroupDeviceTask", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = grpDevTask.Id;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = grpDevTask.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@DeviceName", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceName"].Value = grpDevTask.DeviceName;

                    cmd.Parameters.Add(new SqlParameter("@GroupName", SqlDbType.NVarChar));
                    cmd.Parameters["@GroupName"].Value = grpDevTask.GroupName;

                    cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreateDate"].Value = grpDevTask.CreateDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = grpDevTask.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = grpDevTask.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = grpDevTask.SiteId;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateGroupDeviceTask";

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            grpDevTask.Id = id;
                            var audit = new GroupDeviceAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, grpDevTask.UpdatedBy);
                            Reflection.CopyProperties(grpDevTask, audit);
                            GroupDeviceAudit.InsertGroupDeviceAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateGroupDeviceTask", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static bool DeleteGroupTaskByGroupName(int taskId,string groupName, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteGroupTaskByGroupName", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupName", SqlDbType.NVarChar));
                cmd.Parameters["@GroupName"].Value = groupName;

                cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                cmd.Parameters["@TaskId"].Value = taskId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteGroupTaskByGroupName";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteDeviceTaskByDeviceName(int taskId, string deviceName, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteDeviceTaskByDeviceName", connection);

                cmd.Parameters.Add(new SqlParameter("@DeviceName", SqlDbType.NVarChar));
                cmd.Parameters["@DeviceName"].Value = deviceName;

                cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                cmd.Parameters["@TaskId"].Value = taskId;


                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDeviceTaskByDeviceName";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteGroupDeviceTaskByTaskId(int taskId,  string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteGroupDeviceTaskByTaskId", connection);
           
                cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                cmd.Parameters["@TaskId"].Value = taskId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteGroupDeviceTaskByTaskId";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static List<GroupDeviceTask> GetAllGroupDeviceTaskByTaskId(int taskId, string dbConnection)
        {
            var groupDeviceTasks = new List<GroupDeviceTask>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllGroupDeviceTaskByTaskId", connection);
                sqlCommand.CommandText = "GetAllGroupDeviceTaskByTaskId";
                sqlCommand.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                sqlCommand.Parameters["@TaskId"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var groupDeviceTask = new GroupDeviceTask();

                    groupDeviceTask.Id = Convert.ToInt32(reader["Id"].ToString());
                    groupDeviceTask.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
                    if (!String.IsNullOrWhiteSpace(reader["DeviceName"].ToString()))
                    {
                        groupDeviceTask.Name = reader["DeviceName"].ToString();
                        groupDeviceTask.DeviceName = reader["DeviceName"].ToString();
                    }
                    else if (!String.IsNullOrWhiteSpace(reader["GroupName"].ToString()))
                    {
                        groupDeviceTask.Name = reader["GroupName"].ToString();
                        groupDeviceTask.GroupName = reader["GroupName"].ToString();
                    }
                    
                    groupDeviceTask.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                    groupDeviceTask.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    groupDeviceTask.CreatedBy = reader["CreatedBy"].ToString();
                    groupDeviceTasks.Add(groupDeviceTask);
                }
                connection.Close();
                reader.Close();
            }
            return groupDeviceTasks;
        }
    }
}
