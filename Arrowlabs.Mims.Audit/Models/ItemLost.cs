﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public static class DataRecordExtensions
    {
        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
    }
    public class ItemLost
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Comments { get; set; }
        public int AssetCatId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string LocationDesc { get; set; }
        public string UpdatedBy { get; set; }
        public int LocationId { get; set; }
        public int Lost { get; set; }
        public Double Longitude { get; set; }
        public Double Latitude { get; set; }
        public string OwnerName { get; set; }

        public string CheckedinBy { get; set; }

        public string SerialNo { get; set; }

        public string Barcode { get; set; }
         
        public int CustomerId { get; set; }

        public int SiteId { get; set; }

        public bool isKey { get; set; }

        public int MainAssetCatId { get; set; }

        public bool MainLocationId { get; set; }

        //NEW PARAMTERS
        public string Remarks { get; set; }

        public DateTime? LastVisitDate { get; set; }

        public string AssetMake { get; set; }

        public string AssetModel { get; set; }

        public string Contractor { get; set; }

        public int AccountId { get; set; }
        public string AccountName { get; set; }

        public int ProjectId { get; set; }
        public string ProjectName { get; set; }

        public int MaintenancePlan { get; set; }
        public string Usage { get; set; }

        public int Status { get; set; }

        public int SparePartParentId { get; set; }

        public bool isSparePart { get; set; }
        public enum LostStatus : int
        {
            Unknown = -1,
            CheckedIn = 0,
            CheckedOut = 1,
            Transfered = 2
        }
        public static int InsertOrUpdateItemLost(ItemLost usersClass, string dbConnection)
        {

            int id = usersClass.Id;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (usersClass != null)
            {
                var baseAuditId = BaseAudit.InsertBaseAudit(usersClass.BaseAudit, dbConnection);

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateItemLostAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = usersClass.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = usersClass.Name;

                    cmd.Parameters.Add(new SqlParameter("@Comments", SqlDbType.NVarChar));
                    cmd.Parameters["@Comments"].Value = usersClass.Comments;

                    cmd.Parameters.Add(new SqlParameter("@AssetCatId", SqlDbType.Int));
                    cmd.Parameters["@AssetCatId"].Value = usersClass.AssetCatId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = usersClass.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = usersClass.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = usersClass.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = usersClass.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@OwnerName", SqlDbType.NVarChar));
                    cmd.Parameters["@OwnerName"].Value = usersClass.OwnerName;

                    cmd.Parameters.Add(new SqlParameter("@CheckedinBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CheckedinBy"].Value = usersClass.CheckedinBy;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = usersClass.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@Lost", SqlDbType.Int));
                    cmd.Parameters["@Lost"].Value = usersClass.Lost;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = usersClass.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = usersClass.CustomerId;


                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = usersClass.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = usersClass.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@IsKey", SqlDbType.Bit));
                    cmd.Parameters["@IsKey"].Value = usersClass.isKey;

                    cmd.Parameters.Add(new SqlParameter("@MainAssetCatId", SqlDbType.Int));
                    cmd.Parameters["@MainAssetCatId"].Value = usersClass.MainAssetCatId;

                    cmd.Parameters.Add(new SqlParameter("@MainLocationId", SqlDbType.Int));
                    cmd.Parameters["@MainLocationId"].Value = usersClass.MainLocationId;

                    cmd.Parameters.Add(new SqlParameter("@SerialNo", SqlDbType.NVarChar));
                    cmd.Parameters["@SerialNo"].Value = usersClass.SerialNo;

                    cmd.Parameters.Add(new SqlParameter("@Barcode", SqlDbType.NVarChar));
                    cmd.Parameters["@Barcode"].Value = usersClass.Barcode;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = usersClass.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@LastVisitDate", SqlDbType.DateTime));
                    cmd.Parameters["@LastVisitDate"].Value = usersClass.LastVisitDate;

                    cmd.Parameters.Add(new SqlParameter("@AssetMake", SqlDbType.NVarChar));
                    cmd.Parameters["@AssetMake"].Value = usersClass.AssetMake;

                    cmd.Parameters.Add(new SqlParameter("@AssetModel", SqlDbType.NVarChar));
                    cmd.Parameters["@AssetModel"].Value = usersClass.AssetModel;

                    cmd.Parameters.Add(new SqlParameter("@Contractor", SqlDbType.NVarChar));
                    cmd.Parameters["@Contractor"].Value = usersClass.Contractor;

                    cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                    cmd.Parameters["@AccountId"].Value = usersClass.AccountId;

                    cmd.Parameters.Add(new SqlParameter("@ProjectId", SqlDbType.Int));
                    cmd.Parameters["@ProjectId"].Value = usersClass.ProjectId;

                    cmd.Parameters.Add(new SqlParameter("@MaintenancePlan", SqlDbType.Int));
                    cmd.Parameters["@MaintenancePlan"].Value = usersClass.MaintenancePlan;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = usersClass.Status;

                    cmd.Parameters.Add(new SqlParameter("@SparePartParentId", SqlDbType.Int));
                    cmd.Parameters["@SparePartParentId"].Value = usersClass.SparePartParentId;

                    cmd.Parameters.Add(new SqlParameter("@isSparePart", SqlDbType.NVarChar));
                    cmd.Parameters["@isSparePart"].Value = usersClass.isSparePart;

                    cmd.Parameters.Add(new SqlParameter("@Usage", SqlDbType.NVarChar));
                    cmd.Parameters["@Usage"].Value = usersClass.Usage;


                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                }
            }
            return id;
        }
        public static ItemLost GetFullItemLostFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var itemLostClass = new ItemLost();

            if (!string.IsNullOrEmpty(reader["Id"].ToString()))
                itemLostClass.Id = Convert.ToInt32(reader["Id"].ToString());
            itemLostClass.Name = reader["Name"].ToString();
            itemLostClass.Comments = reader["Comments"].ToString();
            itemLostClass.CreatedBy = reader["CreatedBy"].ToString();
            if (!string.IsNullOrEmpty(reader["AssetCatId"].ToString()))
                itemLostClass.AssetCatId = Convert.ToInt32(reader["AssetCatId"].ToString());



            if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                itemLostClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (!string.IsNullOrEmpty(reader["UpdatedDate"].ToString()))
                itemLostClass.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (!string.IsNullOrEmpty(reader["UpdatedBy"].ToString()))
                itemLostClass.UpdatedBy = reader["UpdatedBy"].ToString();

            if (!string.IsNullOrEmpty(reader["OwnerName"].ToString()))
                itemLostClass.OwnerName = reader["OwnerName"].ToString();



            if (!string.IsNullOrEmpty(reader["LocationId"].ToString()))
                itemLostClass.LocationId = Convert.ToInt32(reader["LocationId"].ToString());

            if (!string.IsNullOrEmpty(reader["Lost"].ToString()))
                itemLostClass.Lost = Convert.ToInt32(reader["Lost"].ToString());

            if (!string.IsNullOrEmpty(reader["Latitude"].ToString()))
                itemLostClass.Latitude = Convert.ToDouble(reader["Latitude"].ToString());

            if (!string.IsNullOrEmpty(reader["Longitude"].ToString()))
                itemLostClass.Longitude = Convert.ToDouble(reader["Longitude"].ToString());

            if (DataRecordExtensions.HasColumn(reader, "LocationDesc"))
                itemLostClass.LocationDesc = reader["LocationDesc"].ToString();

            return itemLostClass;
        }

        public static List<ItemLost> GetAllItemLost(string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllItemLost", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }
        public static bool DeleteItemLostById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteItemLostById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static ItemLost GetItemLostById(int id, string dbConnection)
        {
            ItemLost itemLostList = new ItemLost();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetItemLostById", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    itemLostList = GetFullItemLostFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }

        public static List<ItemLost> GetAllItemLostByLocationIdAndAssetCatId(int locationId, int assetCategoryId, string dbConnection)
        {
            var assetList = new List<ItemLost>();
            ItemLost asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllItemLostByLocationIdAndAssetCatId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                sqlCommand.Parameters["@LocationId"].Value = locationId;

                sqlCommand.Parameters.Add(new SqlParameter("@AssetCatId", SqlDbType.Int));
                sqlCommand.Parameters["@AssetCatId"].Value = assetCategoryId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetFullItemLostFromSqlReader(reader);
                    assetList.Add(asset);
                }

                connection.Close();
                reader.Close();
            }
            return assetList;
        }
    }
}
