﻿<%@ Page EnableEventValidation="false"  Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tasks.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.Tasks" %>
<%@ Register TagPrefix="NewTask" TagName="MyNewTask" Src="~/Controls/Modals/NewTask.ascx" %>
<%@ Register TagPrefix="TicketingCard" TagName="MyTicketingCard" Src="~/Controls/Modals/TicketingCard.ascx" %>
<%@ Register TagPrefix="IncidentCard" TagName="MyIncidentCard" Src="~/Controls/Modals/IncidentCard.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
         <style>
                              .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 172px;
        margin-top:6px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 185px;
        height:23px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
             sup {
    vertical-align: super;
    font-size: smaller;
    color:orange
}
      .loaderspinner { 
          border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #b2163b;
  border-bottom: 16px solid #b2163b;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}
      .loaderspinner2 { 
          border: 4px solid #f3f3f3;
  border-radius: 50%;
  border-top: 4px solid #b2163b;
  border-bottom: 4px solid #b2163b;
  width: 25px;
  height: 25px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
  margin-top:-45px;
  margin-left:240px;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}                               


.labelSchedule{
    font-family: "Montserrat-Regular","Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 12px;
    background-color: transparent;
    border: 0; 
    margin: 10px 0;
    width: 100%;
    color:#b2163b;
}

.labelSchedule2{
    font-family: "Montserrat-Regular","Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 15px;
    background-color: transparent;
    border: 0; 
    margin: 10px 0;
    margin-left: 5px;
    width: 100%;
    color:black;
}

#newDocument {
    overflow-y:scroll;
} 

 #projDocument {
    overflow-y:scroll;
} 
  #editChecklistType4 {
    overflow-y:scroll;
} 
    #editChecklistType5 {
    overflow-y:scroll;
} 
 #taskDocument {
    overflow-y:scroll;
} 
   .line-center{
    margin:0;padding:0 10px;
    background:#FFF;
    display:inline-block;
}
h5{

    text-align:center;
    position:relative;
    z-index:2;
    color:gray;
    margin-left:-20px;
}
h5:after{
    content:"";
    position:absolute;
    top:50%;
    left:0;
    right:0;
    border-top:solid 1px gray;
    z-index:-1;
}  
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

          <style type="text/css" media="screen">
		
		#wrapper {
			
		}
        .aui-scheduler-base {
			width: 100%;
            margin:inherit;
		}

		</style>

    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connectionc
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        var loggedinUsername = '<%=senderName2%>';
        var mapUrlLocation;
        var locationAllowed = false;
        var rtime = new Date(1, 1, 2000, 12, 00, 00);
        var timeout = false;
        var delta = 200;
        var mapLocation;
        var incidentmapIncidentLocation;
        var mapIncidentLocation;
        var mapticketLocation;
        var mapIncidentLocationTemplate;
        var incidentMyMarkersIncidentLocation = new Array();
        var myMarkersIncidentLocation = new Array();
        var myMarkersIncidentLocationTemplate = new Array();
        var myMarkersLocation = new Array();
        var myTraceBackMarkers = new Array();
        var incidentmyTraceBackMarkers = new Array();
        var divArray = new Array();
        var incidentdivArray = new Array();
        var rotate_factor = 0;
        var rotated = false;



        var incidentinfoWindowLocation;
        var infoWindowLocation;
        var infoWindowIncidentLocation;
        var infoWindowIncidentLocationTemplate;
        var divArrayDutyRoster = new Array();
        //Schedule
        function addUsersToGroupDuty() {
            document.getElementById('dutyUserCollection').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getGroupUserDataDuty",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {

                            var div = document.createElement('div');

                            div.className = 'help-block round-border padding-2x mb-2x drag-element';

                            div.innerHTML = data.d[i];

                            div.id = data.d[i + 1];
                            divArrayDutyRoster[i] = div.id;
                            div.onclick = function () {
                                try {

                                    jQuery('#myScheduler div').remove();
                                    var div1 = document.createElement("div");
                                    div1.id = 'myScheduler';


                                    document.getElementById("wrapper").appendChild(div1);

                                    for (var i = 0; i < divArrayDutyRoster.length; i++) {
                                        if (typeof divArrayDutyRoster[i] === 'undefined') {
                                            // your code here.
                                        }
                                        else {
                                            var el = document.getElementById(divArrayDutyRoster[i]);
                                            if (el)
                                                el.style = "white";
                                        }
                                    }
                                    document.getElementById(this.id).style.backgroundColor = "lightgray";//"#b2163b";
                                    var splitid = this.id.split('_');

                                    LoadAlloyUIScheduleByUser(splitid[0]);
                                }
                                catch (err) {
                                    //alert(err)
                                }
                            };

                            document.getElementById('dutyUserCollection').appendChild(div);

                            i++;
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function searchDutyUserNameClick() {
            try {
                var isErr = false;
                if (isEmptyOrSpaces(document.getElementById('searchDutyUserGroup').value)) {
                    isErr = true;
                    showAlert("Kindly provide search input")
                    addUsersToGroupDuty();
                }
                else {
                    if (isSpecialChar(document.getElementById('searchDutyUserGroup').value)) {
                        isErr = true;
                    }
                }
                if (!isErr) {
                    getUsersByNameDuty(document.getElementById("searchDutyUserGroup").value);
                }
            } catch (err) {
                // alert(err)
            }
        }
        function getUsersByNameDuty(name) {
            document.getElementById('dutyUserCollection').innerHTML = "";

            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getUsersByNameDutyRoster",
                data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {

                            var div = document.createElement('div');

                            div.className = 'help-block round-border padding-2x mb-2x drag-element';

                            div.innerHTML = data.d[i];

                            if (data.d[i + 1].split('|').length > 1) {
                                div.id = data.d[i + 1].split('|')[0];
                                div.style.display = "none";
                            }
                            else
                                div.id = data.d[i + 1];

                            divArrayDutyRoster[i] = div.id;
                            div.onclick = function () {
                                try {

                                    jQuery('#myScheduler div').remove();
                                    var div1 = document.createElement("div");
                                    div1.id = 'myScheduler';


                                    document.getElementById("wrapper").appendChild(div1);

                                    for (var i = 0; i < divArrayDutyRoster.length; i++) {
                                        if (typeof divArrayDutyRoster[i] === 'undefined') {
                                            // your code here.
                                        }
                                        else {
                                            var el = document.getElementById(divArrayDutyRoster[i]);
                                            if (el)
                                                el.style = "white";
                                        }
                                    }
                                    document.getElementById(this.id).style.backgroundColor = "lightgray";//"#b2163b";
                                    var splitid = this.id.split('_');

                                    LoadAlloyUIScheduleByUser(splitid[0]);
                                }
                                catch (err) {
                                    //alert(err)
                                }
                            };

                            document.getElementById('dutyUserCollection').appendChild(div);

                            i++;
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        var gantt_chart;
        var tasks;
        function LoadAlloyUISchedule() {

            try {
                addUsersToGroupDuty();
                jQuery('#myScheduler div').remove();
                var div = document.createElement("div");
                div.id = 'myScheduler';

                console.log('deleted:myScheduler');

                document.getElementById("wrapper").appendChild(div);
				
				//jQuery('.yui3-widget.yui3-overlay.yui3-widget-positioned.yui3-widget-stacked.aui-scheduler-event-recorder-overlay.yui3-overlay-focused.yui3-overlay-hidden').remove();
				
				//jQuery('.yui3-widget.yui3-overlay.yui3-widget-positioned.yui3-widget-stacked.aui-scheduler-event-recorder-overlay.yui3-overlay-hidden').remove();
            }
            catch (err) {
                console.log(err);
            }
           


    jQuery.ajax({
        type: "POST",
        url: "Tasks.aspx/getganntTasks",
        data: "{'id':'0','uname':'" + loggedinUsername + "','view':'Month'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d.length > 0) {
                //var dayview = new A.SchedulerDayView({
                //    boundingBox: '#myScheduler .yui3-widget-bd .aui-scheduler-view-day',
                //    srcNode: '#myScheduler .yui3-widget-bd .aui-scheduler-view-day-content',

                //    headerView: true
                //});

                //var weekview = new A.SchedulerWeekView({
                //    boundingBox: '#myScheduler .yui3-widget-bd .aui-scheduler-view-week',
                //    srcNode: '#myScheduler .yui3-widget-bd .aui-scheduler-view-week-content',
                //    isoTime: true,

                //    headerView: true
                //});

                //var monthview = new A.SchedulerMonthView({
                //    boundingBox: '#myScheduler .yui3-widget-bd .aui-scheduler-view-month',
                //    srcNode: '#myScheduler .yui3-widget-bd .aui-scheduler-view-month-content'
                //});
                 dayview = new A.SchedulerDayView({
                   	scrollable:false,
                    height: 1320
                });

                 weekview = new A.SchedulerWeekView({
                    scrollable: false,
                    height: 1320
                });

                 monthview = new A.SchedulerMonthView({
                    displayRows: 3
                });
                 jsonData = JSON.parse(data.d[0]);
                var task = [];


                for (var i = 0; i < jsonData.length; i++) {
                    var entry = jsonData[i];
                    entry.startDate = new Date(entry._startDate);
                    entry.endDate = new Date(entry._endDate);
                    task.push(entry);
                }

                var johnCalendar = new A.SchedulerCalendar({
                    events: task
                });

                var events1 = [
                    johnCalendar
                ];
                /*var eventRecorder = new A.SchedulerEventRecorder({
                    duration: 15
                });*/

                var schedulerBase1 = new A.Scheduler({
                    boundingBox: '#myScheduler',
                    events: events1,
                    eventRecorder: eventRecorder,
                    draggable: false,
                    // srcNode: '#content',
                    render: true,
                    firstDayOfWeek: 7,
                    views: [dayview, weekview, monthview],
                    currentDate: new Date()
                });

                schedulerBase1.on('scheduler-event-recorder:save', function (event) {
                    //alert('Save' + event.id);
                    console.log('scheduler-event-recorder', event);
                });

                schedulerBase1.on('scheduler-event-recorder:delete', function (event) {
                    alert('delete' + event.get('taskid'));
                    console.log('scheduler-event-recorder:delete', event);
                });
                schedulerBase1.on('scheduler-event-recorder:gotoday', function (event) {
                    alert('gotoday' + event.get('taskid'));
                    console.log('scheduler-event-recorder:gotoday', event);
                });

                schedulerBase1.on('scheduler-event-recorder:edit', function (event) {
                    console.log('scheduler-event-recorder:edit', event);

                    var taskid = event.newSchedulerEvent.get('taskid');
                    if (taskid != null && taskid != '') { 
                        document.getElementById("ls").style.display = "block";
                        setTimeout(loadTask(taskid), 500);
                    }
                });
            }
        }
    });

        }
        function loadTask(taskid) {
            jQuery('#taskDocument').modal('show');
            showTaskDocument(parseInt(taskid));
        }
        function LoadAlloyUIScheduleByUser(uid) {

            try {
                jQuery('#myScheduler div').remove();
                var div = document.createElement("div");
                div.id = 'myScheduler';

                console.log('deleted:myScheduler');

                document.getElementById("wrapper").appendChild(div);

				//jQuery('.yui3-widget.yui3-overlay.yui3-widget-positioned.yui3-widget-stacked.aui-scheduler-event-recorder-overlay.yui3-overlay-focused.yui3-overlay-hidden').remove();
				
								//jQuery('.yui3-widget.yui3-overlay.yui3-widget-positioned.yui3-widget-stacked.aui-scheduler-event-recorder-overlay.yui3-overlay-hidden').remove();
				
            }
            catch (err) {
                console.log(err);
            }
           


    jQuery.ajax({
        type: "POST",
        url: "Tasks.aspx/getganntTasksByUser",
        data: "{'id':'" + uid + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d.length > 0) {
                 dayview = new A.SchedulerDayView({
                    boundingBox: '#myScheduler .yui3-widget-bd .aui-scheduler-view-day',
                    srcNode: '#myScheduler .yui3-widget-bd .aui-scheduler-view-day-content',
                    scrollable:false,
                    height: 1320,
                    headerView: true
                });

                 weekview = new A.SchedulerWeekView({
                    boundingBox: '#myScheduler .yui3-widget-bd .aui-scheduler-view-week',
                    srcNode: '#myScheduler .yui3-widget-bd .aui-scheduler-view-week-content',
                    isoTime: true,
                    scrollable:false,
                    height: 1320,
                    headerView: true
                });

                 monthview = new A.SchedulerMonthView({
                    boundingBox: '#myScheduler .yui3-widget-bd .aui-scheduler-view-month',
                    srcNode: '#myScheduler .yui3-widget-bd .aui-scheduler-view-month-content',
					displayRows : 3
                });

                var jsonData = JSON.parse(data.d[0]);
                var task = [];


                for (var i = 0; i < jsonData.length; i++) {
                    var entry = jsonData[i];
                    entry.startDate = new Date(entry._startDate);
                    entry.endDate = new Date(entry._endDate);
                    task.push(entry);
                }

                var johnCalendar = new A.SchedulerCalendar({
                    events: task
                });

                var events1 = [
                    johnCalendar
                ];
               

                var schedulerBase1 = new A.Scheduler({
                    boundingBox: '#myScheduler',
                    events: events1,
                    eventRecorder: eventRecorder,
                    draggable: false,
                    // srcNode: '#content',
                    render: true,
                    firstDayOfWeek: 7,
                    views: [dayview, weekview, monthview],
                    currentDate: new Date()
                });

                schedulerBase1.on('scheduler-event-recorder:save', function (event) {
                    //alert('Save' + event.id);
                    console.log('scheduler-event-recorder', event);
                });

                schedulerBase1.on('scheduler-event-recorder:delete', function (event) {
                    alert('delete' + event.get('taskid'));
                    console.log('scheduler-event-recorder:delete', event);
                });
                schedulerBase1.on('scheduler-event-recorder:gotoday', function (event) {
                    alert('gotoday' + event.get('taskid'));
                    console.log('scheduler-event-recorder:gotoday', event);
                });

                schedulerBase1.on('scheduler-event-recorder:edit', function (event) {
                    console.log('scheduler-event-recorder:edit', event);

                    var taskid = event.newSchedulerEvent.get('taskid');
                    if (taskid != null && taskid != '') {
                        jQuery('#taskDocument').modal('show');
                        showTaskDocument(parseInt(taskid));
                    }
                });


            }
        }
    });

        }

        //User-profile
        function changePassword() {
            try {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;
                var isErr = false;
                if (!isErr) {
                    if (!letterGood) {
                        showAlert('Password does not contain letter');
                        isErr = true;
                    }
                    if (!isErr) {
                        if (!capitalGood) {
                            showAlert('Password does not contain capital letter');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!numGood) {
                            showAlert('Password does not contain number');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!lengthGood) {
                            showAlert('Password length not enough');
                            isErr = true;
                        }
                    }
                }
                if (!isErr) {
                    if (newPw == confPw && newPw != "" && confPw != "") {
                        $.ajax({
                            type: "POST",
                            url: "Tasks.aspx/changePW",
                            data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d != "LOGOUT") {
                                    jQuery('#changePasswordModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                    jQuery('#successfulDispatch').modal('show');
                                    document.getElementById("newPwInput").value = "";
                                    document.getElementById("confirmPwInput").value = "";
                                    document.getElementById("oldPwInput").value = confPw;
                                }
                                else {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        showAlert("Kindly match new password with confirm password.")
                    }
                }
            }
            catch (ex)
            { showAlert('Error 60: Problem loading page element.-' + ex) }
        }
        function editUnlock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
            document.getElementById("editProfileA").style.display = "none";
            document.getElementById("saveProfileA").style.display = "block";
            document.getElementById("profileEmailAddDIV").style.display = "none";
            document.getElementById("profileEmailAddEditDIV").style.display = "block";
            document.getElementById("userFullnameSpanDIV").style.display = "none";
            document.getElementById("userFullnameSpanEditDIV").style.display = "block";
            if (document.getElementById('profileRoleName').innerHTML == "User") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "block";
                document.getElementById("dirInfoDIV").style.display = "none";


            }
            else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "none";
                document.getElementById("dirInfoDIV").style.display = "block";
            }
        }
        function editJustLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        function editLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
            var role = document.getElementById('UserRoleSelector').value;
            var roleid = 0;
            var supervisor = 0;
            var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
            if (retVal > 0) {
                assignUserProfileData();
            }
            else {
                showAlert("Error 61: Problem occured while trying to update user profile.")
            }

        }
        function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
            var output = 0;
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/addUserProfile",
                data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }

    function saveTZ() {
        var scountr = $("#countrySelect option:selected").text();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/saveTZ",
            data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                        jQuery('#successfulDispatch').modal('show');
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getserverInfo() {
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getServerData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById('mobileRemaining').value = data.d[9];
                    document.getElementById('mobileTotal').value = data.d[10];
                    document.getElementById('mobileUsed').value = data.d[11];
                    document.getElementById("countrySelect").value = data.d[28];
                    jQuery('#countrySelect').selectpicker('val', data.d[28]);

                    document.getElementById('surveillanceCheck').checked = false;
                    document.getElementById('notificationCheck').checked = false;
                    document.getElementById('locationCheck').checked = false;
                    document.getElementById('ticketingCheck').checked = false;
                    document.getElementById('taskCheck').checked = false;
                    document.getElementById('incidentCheck').checked = false;
                    document.getElementById('warehouseCheck').checked = false;
                    document.getElementById('chatCheck').checked = false;
                    document.getElementById('collaborationCheck').checked = false;
                    document.getElementById('lfCheck').checked = false;
                    document.getElementById('dutyrosterCheck').checked = false;
                    document.getElementById('postorderCheck').checked = false;
                    document.getElementById('verificationCheck').checked = false;
                    document.getElementById('requestCheck').checked = false;
                    document.getElementById('dispatchCheck').checked = false;
                    document.getElementById('activityCheck').checked = false;

                    if (data.d[12] == "true")
                        document.getElementById('surveillanceCheck').checked = true;
                    if (data.d[13] == "true")
                        document.getElementById('notificationCheck').checked = true;
                    if (data.d[14] == "true")
                        document.getElementById('locationCheck').checked = true;
                    if (data.d[15] == "true")
                        document.getElementById('ticketingCheck').checked = true;
                    if (data.d[16] == "true")
                        document.getElementById('taskCheck').checked = true;
                    if (data.d[17] == "true")
                        document.getElementById('incidentCheck').checked = true;
                    if (data.d[18] == "true")
                        document.getElementById('warehouseCheck').checked = true;
                    if (data.d[19] == "true")
                        document.getElementById('chatCheck').checked = true;
                    if (data.d[20] == "true")
                        document.getElementById('collaborationCheck').checked = true;
                    if (data.d[21] == "true")
                        document.getElementById('lfCheck').checked = true;
                    if (data.d[22] == "true")
                        document.getElementById('dutyrosterCheck').checked = true;
                    if (data.d[23] == "true")
                        document.getElementById('postorderCheck').checked = true;
                    if (data.d[24] == "true")
                        document.getElementById('verificationCheck').checked = true;
                    if (data.d[25] == "true")
                        document.getElementById('requestCheck').checked = true;
                    if (data.d[26] == "true")
                        document.getElementById('dispatchCheck').checked = true;
                    if (data.d[27] == "true")
                        document.getElementById('activityCheck').checked = true;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function assignUserProfileData() {
        try {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getUserProfileData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        try {
                            document.getElementById('containerDiv2').style.display = "none";
                            document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                            document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                            document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                            document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                            document.getElementById('profileLastLocation').innerHTML = data.d[4];
                            document.getElementById('profileRoleName').innerHTML = data.d[5];
                            document.getElementById('profileManagerName').innerHTML = data.d[6];
                            document.getElementById('userStatusSpan').innerHTML = data.d[8];

                            if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                document.getElementById('containerDiv2').style.display = "block";
                                document.getElementById('defaultGenderDiv').style.display = "none";
                                getserverInfo();
                            }

                            var el = document.getElementById('userStatusIconSpan');
                            if (el) {
                                el.className = data.d[9];
                            }
                            document.getElementById('userprofileImgSrc').src = data.d[10];
                            document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                            document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                            document.getElementById('userFirstnameSpan').value = data.d[13];
                            document.getElementById('userLastnameSpan').value = data.d[14];
                            document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                            document.getElementById('profileEmailAddEdit').value = data.d[3];

                            document.getElementById('oldPwInput').value = data.d[16];

                            document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                            document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                            document.getElementById('profileGender').innerHTML = data.d[20];

                            if (document.getElementById('profileRoleName').innerHTML != "Level 7") {
                                document.getElementById('deviceTypesDiv').innerHTML = "<i class='fa fa-mobile fa-2x mr-2x'></i><i style='color:lime;' class='fa fa-laptop fa-2x mr-2x'></i>";//data.d[11];

                            }
                        }
                        catch (err) { alert(err) }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (err)
        { alert('Error 60: Problem loading page element.-' + err) }
    }
    function forceLogout() {
        document.getElementById('<%= closingbtn.ClientID %>').click();
    }
    function assignUserTableData() {
        $("#assignUsersTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getAssignUserTableData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#assignUsersTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function cleardispatchList() {
        try {
            var elSel = document.getElementById('sendToListBox');
            var i;
            for (i = elSel.length - 1; i >= 0; i--) {

                var split = elSel.options[i].text.split('+');
                var element2 = document.getElementById(elSel.options[i].value + "-" + split[1]);
                if (element2) {
                    element2.style.color = "#f44e4b";
                    element2.className = "red-color";
                    element2.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                }
                var element = document.getElementById("li-" + elSel.options[i].text);
                if (element) {
                    element.parentNode.removeChild(element);
                }
                elSel.remove(i);
            }
        }
        catch (err) {
            alert('Error 42: Failed to clear dispatch list-' + err);
        }
    }
    function removeFromList(name) {
        var elSel = document.getElementById('sendToListBox');
        var i;
        for (i = elSel.length - 1; i >= 0; i--) {
            if (elSel.options[i].text == name) {
                elSel.remove(i);
            }
        }
    }
    function usersliOnclickRemove(name, id, gtype) {
        //removeFromList(name+"+"+gtype);
        dispatchUserchoiceTable(id, name, gtype);
    }
    function addnametoUserDispatchList(name, id, gtype) {
        var ul = document.getElementById("UsersToDispatchList");
        var li = document.createElement("li");
        li.setAttribute("id", "li-" + name.split(" ")[0] + "+" + gtype);
        li.innerHTML = '<a href="#"  class="capitalize-text" >' + name + '<i class="fa fa-close" onclick="usersliOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;,&apos;' + gtype + '&apos;)"></i></a>';
        ul.appendChild(li);
    }
    function removenameFromDispatchList(name, gtype) {
        try {
            var element = document.getElementById("li-" + name.split(" ")[0] + "+" + gtype);
            element.parentNode.removeChild(element);
        }
        catch (err) {
            alert(err + 'rfd')
        }
    }
    function dispatchUserchoiceTable(id, name, gtype) {
        try {
            var element = document.getElementById(id + "-" + gtype);
            var result = element.innerHTML.indexOf("ADDED");
            if (result < 0) {

                var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                if (exists == false) {
                    var myOption;
                    myOption = document.createElement("Option");
                    myOption.text = name + "+" + gtype; //Textbox's value
                    myOption.value = id; //Textbox's value
                    sendToListBox.add(myOption);
                    addnametoUserDispatchList(name, id, gtype);

                    element.style.color = "#3ebb64";
                    element.className = "green-color";
                    element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
                }
            }
            else {
                var elSel = document.getElementById('sendToListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {
                    if (elSel.options[i].value == id) {
                        var split = elSel.options[i].text.split('+');
                        removenameFromDispatchList(split[0], split[1]);
                        elSel.remove(i);
                    }
                }

                element.style.color = "#b2163b";
                element.className = "red-color";
                element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
            }
        }
        catch (ex)
        { alert(ex) }
    }
    function deleteAttachmentChoiceTicket(id) {
        jQuery('#deleteAttachTicketModal').modal('show');
        document.getElementById('rowidChoiceAttachment').value = id;
        jQuery('#ticketingViewCard').modal('hide');
    }

    function deleteAttachmentTicket() {
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/deleteAttachmentDataTicket",
            data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d != "LOGOUT") {
                    showAlert(data.d);
                    rowchoiceTicket(document.getElementById('rowidChoiceTicket').value);
                    jQuery('#ticketingViewCard').modal('show');
                }
                else {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function deleteAttachmentChoiceAsset(id) {
        jQuery('#deleteAttachModal').modal('show');
        document.getElementById('rowidChoiceAttachment').value = id;
        jQuery('#taskDocument').modal('hide');
    }
    function deleteAttachment() {
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/deleteAttachmentData",
            data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','taskid':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d != "LOGOUT") {
                    showAlert(data.d);
                    showTaskDocument(document.getElementById('rowChoiceTasks').value);
                    jQuery('#taskDocument').modal('show');
                }
                else {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    var firstOpenNewTask = false;
    var firstOpenNewTemplate = false;
    var firstopenVerifier = false;
    function getEventStatusTotal() {
        try {
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getEventStatusTotal",
                data: "{'uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else if (data.d[0] != "ERROR") {
                        document.getElementById('statusAccept').innerHTML = data.d[0];
                        document.getElementById('statusAcceptPercent').innerHTML = data.d[1];

                        document.getElementById('statusComplete').innerHTML = data.d[2];
                        document.getElementById('statusCompletePercent').innerHTML = data.d[3];


                        document.getElementById('statusInprogress').innerHTML = data.d[4];
                        document.getElementById('statusInprogressPercent').innerHTML = data.d[5];

                        document.getElementById('statusPending').innerHTML = data.d[6];
                        document.getElementById('statusPendingPercent').innerHTML = data.d[7];
                        document.getElementById('statusTotal').innerHTML = data.d[8];

                        $('#statusAcceptedPie').data('easyPieChart').update(parseInt(data.d[1]));
                        $('#statusCompletePie').data('easyPieChart').update(parseInt(data.d[3]));
                        $('#statusInprogressPie').data('easyPieChart').update(parseInt(data.d[5]));
                        $('#statusPendingPie').data('easyPieChart').update(parseInt(data.d[7]));
                    }
                    else {
                        showError("Kindly select valid date range.");
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (err) {
            alert(err);
        }
    }
    function getEventStatusRetrieve() {
        try {
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getEventStatusRetrieve",
                data: "{'fromdate':'" + document.getElementById('fromStatusDatePicker').value + "','todate':'" + document.getElementById('toStatusDatePicker').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d[0] != "ERROR") {
                            document.getElementById('statusAccept').innerHTML = data.d[0];
                            document.getElementById('statusAcceptPercent').innerHTML = data.d[1];

                            document.getElementById('statusComplete').innerHTML = data.d[2];
                            document.getElementById('statusCompletePercent').innerHTML = data.d[3];


                            document.getElementById('statusInprogress').innerHTML = data.d[4];
                            document.getElementById('statusInprogressPercent').innerHTML = data.d[5];

                            document.getElementById('statusPending').innerHTML = data.d[6];
                            document.getElementById('statusPendingPercent').innerHTML = data.d[7];
                            document.getElementById('statusTotal').innerHTML = data.d[8];

                            $('#statusAcceptedPie').data('easyPieChart').update(parseInt(data.d[1]));
                            $('#statusCompletePie').data('easyPieChart').update(parseInt(data.d[3]));
                            $('#statusInprogressPie').data('easyPieChart').update(parseInt(data.d[5]));
                            $('#statusPendingPie').data('easyPieChart').update(parseInt(data.d[7]));
                        }
                        else {
                            showError("Kindly select valid date range.");
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (err) {
            alert(err);
        }
    }
        function deleteClickOnCheckbox(id) {
            try {
                var val = document.getElementById("task" + id);
                if (val.checked) {
                    var myOption;
                    myOption = document.createElement("Option");
                    myOption.text = "task" + id; //Textbox's value
                    myOption.value = id; //Textbox's value
                    deleteListBox.add(myOption);
                }
                else {
                    var elSel = document.getElementById('deleteListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) {
                        if (elSel.options[i].text == "task" + id) {
                            elSel.remove(i);
                        }
                    }
                }
            }
            catch (err) {

            }
            try {
                var val2 = document.getElementById("alltask" + id);
                if (val2.checked) {
                    var myOption2;
                    myOption2 = document.createElement("Option");
                    myOption2.text = "alltask" + id; //Textbox's value
                    myOption2.value = id; //Textbox's value
                    deleteListBox.add(myOption2);
                }
                else {
                    var elSel2 = document.getElementById('deleteListBox');
                    var i;
                    for (i = elSel2.length - 1; i >= 0; i--) {
                        if (elSel2.options[i].text == "alltask" + id) {
                            elSel2.remove(i);
                        }
                    }
                }
            }
            catch (err) {

            }
        }
    function deleteTaskType() {
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/deleteTaskType",
            data: "{'id':'" + document.getElementById("rowChoiceTaskType").value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {
                    document.getElementById('successincidentScenario').innerHTML = "Task Type successfully deleted";
                    jQuery('#successfulDispatch').modal('show');
                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showAlert('Error 40: Failed to delete task type.-' + data.d);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function editTaskTypeChoice(id, name) {
        document.getElementById("rowChoiceTaskType").value = id;
        document.getElementById("editTaskTypeinput").value = name;
    }
    function enablechklist() {

        document.getElementById('chknamep').innerHTML = "Checklist Name:<span><a href='#' onclick='unlockchcklistname()'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' onclick='savechklistname()'><i class='fa fa-save mr-1x'></i>Save</a></span>";

        document.getElementById('tbnewchecklistmain').disabled = "";
        document.getElementById('tbnewchecklistdesc').disabled = "";
        document.getElementById('tbnewchecklistqt').disabled = "";
        document.getElementById('tbnewchecklistloc').disabled = "";
        document.getElementById('photochecklistinfo').disabled = "";
        document.getElementById('keepchecklistinfo').disabled = "";

        document.getElementById('tbnewchecklistname').disabled = true;


        //document.getElementById('chkREM').style.display = 'block';
        document.getElementById('chkADD').style.display = 'block';
        document.getElementById('chkSAV').style.display = 'none';
    }
    var savedclicked = false;
    function refreshchktable() {

        if (!savedclicked) {
            document.getElementById('tbnewchecklistmain').value = "";
            document.getElementById('tbnewchecklistdesc').value = "";
            document.getElementById('tbnewchecklistqt').value = "";
            document.getElementById('tbnewchecklistloc').value = "";

            document.getElementById("keepchecklistinfo").checked = "";
            document.getElementById("photochecklistinfo").checked = "";

            jQuery("#type5Table").dataTable().fnClearTable();
            jQuery("#type5Table").dataTable().fnDraw();
            jQuery("#type5Table").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getTableDataType5",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    taskfilter = true;
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            $("#type5Table tbody").append(data.d[i]);
                        }
                        jQuery("#type5Table").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 10,
                            "order": [[0, "asc"]]
                        });
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        } else {
            showLoader();
            location.reload();
        }
    }
    function disablechklist() {

        document.getElementById('chknamep').innerHTML = "Checklist Name:";
        document.getElementById('newChecklistModal2Header').innerHTML = "CREATE CHECKLIST";
        document.getElementById('tbnewchecklistname').value = "";
        jQuery("#chklistitemsTable").dataTable().fnClearTable();
        jQuery("#chklistitemsTable").dataTable().fnDraw();
        jQuery("#chklistitemsTable").dataTable().fnDestroy();

        jQuery("#chklistitemsTable").DataTable({
            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
            'iDisplayLength': 5,
            "order": [[0, "asc"]]
        });

        document.getElementById('tbnewchecklistmain').disabled = true;
        document.getElementById('tbnewchecklistdesc').disabled = true;
        document.getElementById('tbnewchecklistqt').disabled = true;
        document.getElementById('tbnewchecklistloc').disabled = true;
        document.getElementById('photochecklistinfo').disabled = true;
        document.getElementById('keepchecklistinfo').disabled = true;
        document.getElementById('chkREM').style.display = 'none';
        document.getElementById('chkADD').style.display = 'none';
        document.getElementById('chkSAV').style.display = 'block';

        document.getElementById('tbnewchecklistname').disabled = "";

        document.getElementById('newChecklistID').value = "0";
        document.getElementById('parentChecklistID').value = "0";

    }
    function gettablechecklisttable(id) {
        jQuery("#chklistitemsTable").dataTable().fnClearTable();
        jQuery("#chklistitemsTable").dataTable().fnDraw();
        jQuery("#chklistitemsTable").dataTable().fnDestroy();

        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getChkItemsTable",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#chklistitemsTable tbody").append(data.d[i]);
                    }
                    jQuery("#chklistitemsTable").DataTable({
                        "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                        'iDisplayLength': 5,
                        "order": [[0, "asc"]]
                    });
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function editchecklistitem(id, type, main, desc, qt, loc, ischeck) {
        document.getElementById('chkEDIT').style.display = 'block';
        document.getElementById('rowChoiceCHKEdit').value = id;
        if (type == "5") {
            document.getElementById('tbnewchecklistmain').value = main;
            document.getElementById('tbnewchecklistdesc').value = desc;
            document.getElementById('tbnewchecklistloc').value = loc;
            document.getElementById('tbnewchecklistqt').value = qt;
        }
        if (ischeck == "true") {
            document.getElementById("photochecklistinfo").checked = "true";
        } else {
            document.getElementById("photochecklistinfo").checked = "";
        }
    }

    function maineditchecklistitem(id, type, main, desc, qt, loc, ischeck) {
        document.getElementById('chkEDIT').style.display = 'block';
        document.getElementById('rowChoiceCHKEdit').value = id;
        if (type == "5") {
            document.getElementById('tbnewchecklistmain').value = main;
            document.getElementById('tbnewchecklistdesc').value = desc;
            document.getElementById('tbnewchecklistloc').value = loc;
            document.getElementById('tbnewchecklistqt').value = qt;
        }
        if (ischeck == "true") {
            document.getElementById("photochecklistinfo").checked = "true";
        } else {
            document.getElementById("photochecklistinfo").checked = "";
        }
    }

    function deletechecklistitem(tid) {
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/delChecklistItemData",
            data: "{'id':'" + tid + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {
                    showAlert('Successfully deleted checklist item.')
                    document.getElementById('tbnewchecklistmain').value = "";
                    document.getElementById('tbnewchecklistdesc').value = "";
                    document.getElementById('tbnewchecklistqt').value = "";
                    document.getElementById('tbnewchecklistloc').value = "";
                    document.getElementById('chkEDIT').style.display = 'none';
                    document.getElementById("keepchecklistinfo").checked = "";
                    document.getElementById("photochecklistinfo").checked = "";
                    try {
                        gettablechecklisttable(document.getElementById('newChecklistID').value);
                        showAlert("Successfully Deleted");
                    }
                    catch (exx) {

                    }
                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function savechklistname() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbnewchecklistname').value)) {
                isPass = false;
                showAlert("Kindly provide checklist name");
            }
            else {
                if (isSpecialChar(document.getElementById('tbnewchecklistname').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character checklist name");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/saveCHKListName",
                    data: "{'id':'" + document.getElementById('newChecklistID').value + "','name':'" + document.getElementById('tbnewchecklistname').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (isNumeric(data.d)) {
                            document.getElementById('newChecklistID').value = data.d;
                            enablechklist();
                            savedclicked = true;
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function unlockchcklistname() {
            document.getElementById('tbnewchecklistname').disabled = "";
        }
        function viewnewChecklistModal2(id, name, isedit) {
            document.getElementById('newChecklistID').value = id;
            //document.getElementById('parentChecklistID').value = id; 
            document.getElementById('tbnewchecklistname').value = name;
            gettablechecklisttable(document.getElementById('newChecklistID').value);
            document.getElementById('newChecklistModal2Header').innerHTML = "EDIT CHECKLIST";
            if (isedit == "true") {
                enablechklist();
                document.getElementById('chknamep').innerHTML = "Checklist Name:<span><a href='#' onclick='unlockchcklistname()'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' onclick='savechklistname()'><i class='fa fa-save mr-1x'></i>Save</a></span>";

            }
            else {
                document.getElementById('chkADD').style.display = 'none';
                document.getElementById('chkSAV').style.display = 'none';
                document.getElementById('chknamep').innerHTML = "Checklist Name:";
            }
        }
        function savechklist() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbnewchecklistname').value)) {
                isPass = false;
                showAlert("Kindly provide checklist name");
            }
            else {
                if (isSpecialChar(document.getElementById('tbnewchecklistname').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character checklist name");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/saveCHKList",
                    data: "{'id':'" + document.getElementById('tbnewchecklistname').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (isNumeric(data.d)) {
                        document.getElementById('newChecklistID').value = data.d;
                        enablechklist();
                    }
                    else {
                        showError(data.d);
                    }
                },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                        });
        }
    }
    function savechklistItem() {
        var isPass = true;
        document.getElementById('chkEDIT').style.display = 'none';
        document.getElementById('rowChoiceCHKEdit').value = "0";

        if (isEmptyOrSpaces(document.getElementById('tbnewchecklistmain').value)) {
            isPass = false;
            showAlert("Kindly provide main item name");
        }
        else {
            if (isSpecialChar(document.getElementById('tbnewchecklistmain').value)) {
                isPass = false;
                showAlert("Kindly remove special character main item name");
            } else {
                if (isEmptyOrSpaces(document.getElementById('tbnewchecklistdesc').value)) {

                }
                else {
                    if (isSpecialChar(document.getElementById('tbnewchecklistdesc').value)) {
                        isPass = false;
                        showAlert("Kindly remove special character sub item name");
                    }
                }
                if (isEmptyOrSpaces(document.getElementById('tbnewchecklistqt').value)) {

                }
                else {
                    if (!isNumeric(document.getElementById('tbnewchecklistqt').value)) {
                        isPass = false;
                        showAlert("Kindly provide numeric character for target value");
                    }
                }
            }
        }
        if (isPass) {

            document.getElementById('parentChecklistID').value = 0;

            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/saveCHKListItem",
                data: "{'id':'" + document.getElementById('newChecklistID').value
                    + "','pid':'" + document.getElementById("parentChecklistID").value
                    + "','name':'" + document.getElementById("tbnewchecklistmain").value
                    + "','desc':'" + document.getElementById("tbnewchecklistdesc").value
                    + "','qt':'" + document.getElementById("tbnewchecklistqt").value
                    + "','location':'" + document.getElementById("tbnewchecklistloc").value
                    + "','iscamera':'" + document.getElementById("photochecklistinfo").checked
                    + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (isNumeric(data.d[0])) {

                            //if (data.d[1] == "1") {
                            //    document.getElementById('tbnewchecklistdesc').disabled = true; 
                            //}
                            //else if (data.d[1] == "3") {
                            //    document.getElementById('tbnewchecklistdesc').disabled = true;
                            //    document.getElementById('tbnewchecklistqt').disabled = true;
                            //}
                            //else if (data.d[1] == "5") {
                            //    document.getElementById('tbnewchecklistmain').disabled = true;
                            //    document.getElementById('tbnewchecklistqt').disabled = true;
                            document.getElementById('parentChecklistID').value = data.d[0];
                            document.getElementById('previousChkName').value = data.d[2];

                            //}
                            if (document.getElementById('keepchecklistinfo').checked == false) {
                                if (data.d[1] == "5") {

                                }
                                else {
                                    document.getElementById('tbnewchecklistmain').value = "";
                                }
                                document.getElementById('tbnewchecklistdesc').value = "";
                                document.getElementById('tbnewchecklistqt').value = "";
                                document.getElementById('tbnewchecklistloc').value = "";
                                document.getElementById('photochecklistinfo').checked = false;
                            }
                        }
                        else {
                            showError(data.d[0]);
                        }
                        gettablechecklisttable(document.getElementById('newChecklistID').value);
                        showAlert("Successfully Added");
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function editCHKListItem() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbnewchecklistmain').value)) {
                isPass = false;
                showAlert("Kindly provide main item name");
            }
            else {
                if (isSpecialChar(document.getElementById('tbnewchecklistmain').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character main item name");
                } else {
                    if (isEmptyOrSpaces(document.getElementById('tbnewchecklistdesc').value)) {

                    }
                    else {
                        if (isSpecialChar(document.getElementById('tbnewchecklistdesc').value)) {
                            isPass = false;
                            showAlert("Kindly remove special character sub item name");
                        }
                    }
                    if (isEmptyOrSpaces(document.getElementById('tbnewchecklistqt').value)) {

                    }
                    else {
                        if (!isNumeric(document.getElementById('tbnewchecklistqt').value)) {
                            isPass = false;
                            showAlert("Kindly provide numeric character sub item name");
                        }
                    }
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/editCHKListItem",
                    data: "{'id':'" + document.getElementById('rowChoiceCHKEdit').value
                        + "','pid':'" + document.getElementById("newChecklistID").value
                        + "','name':'" + document.getElementById("tbnewchecklistmain").value
                        + "','desc':'" + document.getElementById("tbnewchecklistdesc").value
                        + "','qt':'" + document.getElementById("tbnewchecklistqt").value
                        + "','location':'" + document.getElementById("tbnewchecklistloc").value
                        + "','iscamera':'" + document.getElementById("photochecklistinfo").checked
                        + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (isNumeric(data.d[0])) {

                            // if (data.d[1] == "1") {
                            //    document.getElementById('tbnewchecklistdesc').disabled = true;
                            // }
                            //  else if (data.d[1] == "3") {
                            //     document.getElementById('tbnewchecklistdesc').disabled = true;
                            //     document.getElementById('tbnewchecklistqt').disabled = true;
                            // }
                            // else if (data.d[1] == "5") {
                            //      document.getElementById('tbnewchecklistmain').disabled = true;
                            //      document.getElementById('tbnewchecklistqt').disabled = true;
                            document.getElementById('parentChecklistID').value = data.d[0];
                            //  }
                            if (document.getElementById('keepchecklistinfo').checked == false) {
                                if (data.d[1] == "5") {

                                }
                                else {
                                    document.getElementById('tbnewchecklistmain').value = "";
                                }
                                document.getElementById('tbnewchecklistdesc').value = "";
                                document.getElementById('tbnewchecklistqt').value = "";
                                document.getElementById('tbnewchecklistloc').value = "";
                                document.getElementById('photochecklistinfo').checked = false;
                            }
                        }
                        else {
                            showError(data.d[0]);
                        }
                        gettablechecklisttable(document.getElementById('newChecklistID').value);
                        document.getElementById('chkEDIT').style.display = 'none';
                        document.getElementById('rowChoiceCHKEdit').value = "0";
                        showAlert("Successfully Edited");
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function saveeditTaskType() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('editTaskTypeinput').value)) {
                isPass = false;
                showAlert("Kindly provide task type");
            }
            else {
                if (isSpecialChar(document.getElementById('editTaskTypeinput').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character task type");
                }
            }
            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/editTaskTypeSave",
                    data: "{'id':'" + document.getElementById("rowChoiceTaskType").value
                        + "','type':'" + document.getElementById("editTaskTypeinput").value
                        + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successincidentScenario').innerHTML = "Task Type successfully edited";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert("Error 37: Failed to update task type. -" + data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    function savenewTaskType() {
        var newPw = document.getElementById("newTaskTypeinput").value;
        if (!isEmptyOrSpaces(newPw)) {
            if (!isSpecialChar(newPw)) {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/newTaskTypeSave",
                    data: "{'newtype':'" + newPw + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newTaskType').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Task Type successfully added";
                            jQuery('#successfulDispatch').modal('show');
                            document.getElementById("newTaskTypeinput").value = "";
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showAlert('Failed to save task type. ' + data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        else {
            showAlert("Kindly provide task type");
        }
    }
    function addrowtoTableTaskType() {
        $("#taskTypeTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataTaskType",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#taskTypeTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    //INCIDENT START
    function assignrowDataIncident(id) {
        var output = "";
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableRowDataIncident",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById('escalateionSpanDiv').style.display = 'none';
                    document.getElementById("usernameSpan").innerHTML = data.d[0];
                    document.getElementById("timeSpan").innerHTML = data.d[1];
                    document.getElementById("typeSpan").innerHTML = data.d[2];

                    var ret = data.d[3];
                    var splitRetval = ret.split('-');
                    if (splitRetval.length > 0) {
                        ret = splitRetval[0];
                    }
                    document.getElementById("statusSpan").innerHTML = ret;
                    document.getElementById("locSpan").innerHTML = data.d[4];
                    document.getElementById("receivedBySpan").innerHTML = data.d[5];
                    document.getElementById("phonenumberSpan").innerHTML = data.d[6];
                    document.getElementById("emailSpan").innerHTML = data.d[7];
                    document.getElementById("descriptionSpan").innerHTML = data.d[8];
                    document.getElementById("instructionSpan").innerHTML = data.d[9];
                    document.getElementById("incidentNameHeader").innerHTML = data.d[10];
                    var el = document.getElementById('headerImageClass');
                    if (el) {
                        el.className = data.d[11];
                    }
                    document.getElementById("rowLongitude").text = data.d[12];
                    document.getElementById("rowLatitude").text = data.d[13];
                    output = data.d[3];
                    if (data.d[14] == "TASK") {
                        document.getElementById('checklistDIV').style.display = 'none';
                        getTasklistItems(id);

                        if (data.d[3] == "Escalated" || data.d[3] == "Reject" || data.d[3] == "Resolve" || data.d[3] == "Resolve-MyIncident") {
                            document.getElementById('escalateionSpanDiv').style.display = 'block';
                            document.getElementById("escalationHead").innerHTML = data.d[3] + " Notes";

                            if (data.d[3] == "Resolve-MyIncident")
                                document.getElementById("escalationHead").innerHTML = "Resolve Notes";

                            document.getElementById("escalatedSpan").innerHTML = data.d[15];

                        }
                        document.getElementById('resolveLi').style.display = data.d[16];
                        document.getElementById('disResolveLi').style.display = data.d[16];
                        document.getElementById('compResolveLi').style.display = data.d[16];
                        document.getElementById('compResolveLi2').style.display = data.d[16];
                    }
                    else {
                        if (data.d[3] == "Escalated" || data.d[3] == "Reject" || data.d[3] == "Resolve" || data.d[3] == "Resolve-MyIncident") {
                            document.getElementById('escalateionSpanDiv').style.display = 'block';
                            document.getElementById("escalationHead").innerHTML = data.d[3] + " Notes";

                            if (data.d[3] == "Resolve-MyIncident")
                                document.getElementById("escalationHead").innerHTML = "Resolve Notes";

                            document.getElementById("escalatedSpan").innerHTML = data.d[14];
                        }
                        document.getElementById('resolveLi').style.display = data.d[15];
                        document.getElementById('disResolveLi').style.display = data.d[15];
                        document.getElementById('compResolveLi').style.display = data.d[15];
                        document.getElementById('compResolveLi2').style.display = data.d[15];
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
    function clearIncidentTabDefault() {
        var el = document.getElementById('activity-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('info-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('attachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }

        var el2 = document.getElementById('liInfo');
        if (el2) {
            el2.className = 'active';
        }
        var el5 = document.getElementById('liActi');
        if (el5) {
            el5.className = ' ';
        }
        var el6 = document.getElementById('liAtta');
        if (el6) {
            el6.className = ' ';
        }
        var ell = document.getElementById('taskfilter1');
        if (ell) {
            ell.className = 'fa fa-square-o';
        }
        var ell2 = document.getElementById('taskfilter2');
        if (ell2) {
            ell2.className = 'fa fa-square-o';
        }
        var ell3 = document.getElementById('taskfilter3');
        if (ell3) {
            ell3.className = 'fa fa-square-o';
        }
        var ell4 = document.getElementById('taskfilter4');
        if (ell4) {
            ell4.className = 'fa fa-square-o';
        }
        var elll = document.getElementById('filter1');
        if (elll) {
            elll.className = 'fa fa-square-o';
        }
        var elll2 = document.getElementById('filter2');
        if (elll2) {
            elll2.className = 'fa fa-square-o';
        }
        var elll3 = document.getElementById('filter3');
        if (elll3) {
            elll3.className = 'fa fa-square-o';
        }
        var elll4 = document.getElementById('filter4');
        if (elll4) {
            elll4.className = 'fa fa-square-o';
        }
    }
    function incidentOldDivContainers() {
        try {
            for (var i = 0; i < incidentdivArray.length; i++) {
                var el = document.getElementById(incidentdivArray[i]);
                el.parentNode.removeChild(el);
            }
            incidentdivArray = new Array();
        }
        catch (ex) {
            //alert(ex);
        }
    }
    function incidentHistoryData(id) {
        jQuery('#divIncidentHistoryActivity div').html('');
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getEventHistoryDataIncident",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('divIncidentHistoryActivity').appendChild(div);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }

    function nextbackTask() {
        document.getElementById("rotationDIV1").style.display = "block";
        document.getElementById("rotationDIV2").style.display = "block";
    }

    function showIncidentDocument(name) {
        startRot();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getGPSDataUsers",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    var obj = jQuery.parseJSON(data.d)
                    setMapOnAll(obj, incidentMyMarkersIncidentLocation);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        clearIncidentTabDefault();
        document.getElementById("resolutionTextarea").style.display = "none";
        document.getElementById("escalateionSpanDiv").style.display = "none";
        document.getElementById('rejectionTextarea').value = "";
        document.getElementById('resolutionTextarea').value = "";
        document.getElementById('escalateTextarea').value = "";
        document.getElementById("rowtasktracebackUser").style.display = "none";
        document.getElementById("rowtracebackUser").style.display = "none";
        document.getElementById('checklistDIV').style.display = 'none';
        document.getElementById("finishLi").style.display = "none";
        document.getElementById("nextLi").style.display = "none";
        document.getElementById("rowidChoice").text = name;
        document.getElementById("handleOptionsDiv").style.display = "none";
        incidentOldDivContainers();
        oldDivContainers();
        var retval = assignrowDataIncident(name);

        var splitRetval = retval.split('-');
        if (splitRetval.length > 0) {
            retval = splitRetval[0];
        }
        if (retval == "Complete") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "block";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            //document.getElementById("resolutionTextarea").style.display = "block";
            document.getElementById("completedOptionsDiv2").style.display = "none";

            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Dispatch") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "block";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Pending") {
            document.getElementById("initialOptionsDiv").style.display = "block";
            document.getElementById("parkLi").style.display = "block";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Release") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "block";
            document.getElementById("parkLi").style.display = "block";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Engage") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "block";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Park") {
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "block";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Reject") {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("resolveLi").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "block";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Resolve") {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "block";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";
        }
        else if (retval == "Escalated") {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("initialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("handleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("rejectOptionsDiv").style.display = "none";
            document.getElementById("rejectionTextarea").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "block";

        }
        if (splitRetval.length > 0) {

            if (splitRetval[1] == "Resolve") {
                document.getElementById("resolutionTextarea").style.display = "none";
                document.getElementById("completedOptionsDiv2").style.display = "none";
                document.getElementById("resolvedDiv").style.display = "block";
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("parkLi").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "none";
                document.getElementById("completedOptionsDiv").style.display = "none";
                document.getElementById("dispatchOptionsDiv").style.display = "none";
                document.getElementById("rejectOptionsDiv").style.display = "none";
                document.getElementById("rejectionTextarea").style.display = "none";
                document.getElementById("escalateOptionsDiv").style.display = "none";
                document.getElementById("escalateTextarea").style.display = "none";
                document.getElementById("escalatedDIV").style.display = "none";
            }
            else if (splitRetval[1] == "MyIncident") {
                document.getElementById("resolutionTextarea").style.display = "none";
                document.getElementById("completedOptionsDiv2").style.display = "none";
                document.getElementById("resolvedDiv").style.display = "none";
                document.getElementById("initialOptionsDiv").style.display = "none";
                document.getElementById("parkLi").style.display = "none";
                document.getElementById("handleOptionsDiv").style.display = "none";
                document.getElementById("completedOptionsDiv").style.display = "none";
                document.getElementById("dispatchOptionsDiv").style.display = "none";
                document.getElementById("rejectOptionsDiv").style.display = "none";
                document.getElementById("rejectionTextarea").style.display = "none";
                document.getElementById("escalateOptionsDiv").style.display = "none";
                document.getElementById("escalateTextarea").style.display = "none";
                document.getElementById("escalatedDIV").style.display = "block";
                if (retval == "Resolve") {
                    document.getElementById("escalatedDIV").style.display = "none";
                    document.getElementById("resolvedDiv").style.display = "block";
                }

            }
        }
        incidentHistoryData(name);
        incidentinsertAttachmentIcons(name);
        incidentinsertAttachmentTabData(name);
        incidentinsertAttachmentData(name);
        dispatchAssignMapTab();
        cleardispatchList();
        incidentinfotabDefault();
    }

    function incinextImg() {
        var found = false;
        for (var i = 0; i < incidentdivArray.length; i++) {
            var el = document.getElementById(incidentdivArray[i]);
            if (el.classList.contains("active")) {
                el.className = 'tab-pane fade ';
                if (i + 1 < imgcount) {
                    var el3 = document.getElementById(incidentdivArray[i + 1]);
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                else {
                    var el3 = document.getElementById("location-tab");
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            if (incidentdivArray.length > 0) {
                var ell = document.getElementById(incidentdivArray[0]);
                if (ell) {
                    ell.className = 'tab-pane fade active in';
                }
                var ell1 = document.getElementById("location-tab");
                if (ell1) {
                    ell1.className = 'tab-pane fade';
                }
            }
        }
    }
    function incibackImg() {
        var found = false;
        for (var i = 0; i < incidentdivArray.length; i++) {
            var el = document.getElementById(incidentdivArray[i]);
            if (el.classList.contains("active")) {
                el.className = 'tab-pane fade ';
                if (i == 0) {
                    var elz = document.getElementById("location-tab");
                    if (elz) {
                        elz.className = 'tab-pane fade active in';
                    }
                }
                else if (i - imgcount < imgcount) {
                    var el3 = document.getElementById(incidentdivArray[i - 1]);
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                else {
                    var el3 = document.getElementById("location-tab");
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            if (incidentdivArray.length > 0) {
                var ell = document.getElementById(incidentdivArray[imgcount - 1]);
                if (ell) {
                    ell.className = 'tab-pane fade active in';
                }
                var ell1 = document.getElementById("location-tab");
                if (ell1) {
                    ell1.className = 'tab-pane fade';
                }
            }
        }
    }

    function incidentinfotabDefault() {
        var el = document.getElementById('activity-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('info-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('attachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
    }
    function dispatchAssignMapTab() {
        var el = document.getElementById('incidentAssign-user-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('location-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('incidentNext-user-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
    }
    function incidentinsertAttachmentData(id) {
        document.getElementById('incirotationDIV1').style.display = "none";
        document.getElementById('incirotationDIV2').style.display = "none";
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentDataIncident",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    imgcount = 0;
                    document.getElementById('incidentAudioDIV').style.display = "none";
                    for (var i = 0; i < data.d.length; i++) {
                        if (data.d[i].indexOf("video") >= 0) {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.innerHTML = data.d[i];
                            div.id = 'video-' + (i + 1) + '-tab';
                            document.getElementById('divAttachmentHolder').appendChild(div);
                            incidentdivArray[i] = 'video-' + (i + 1) + '-tab';
                            imgcount++;
                        }
                        else {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.align = 'center';
                            div.style.height = '380px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i + 1) + '-tab';
                            document.getElementById('divAttachmentHolder').appendChild(div);
                            incidentdivArray[i] = 'image-' + (i + 1) + '-tab';
                            imgcount++;
                        }
                    }
                    if (imgcount > 0) {
                        document.getElementById('incirotationDIV1').style.display = "block";
                        document.getElementById('incirotationDIV2').style.display = "block";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function incidentinsertAttachmentIcons(id) {
        jQuery('#divAttachment div').html('');
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentDataIconsIncident",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("divAttachment").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function incidentinsertAttachmentTabData(id) {
        jQuery('#attachments-info-tab div').html('');

        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentDataTabIncident",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("attachments-info-tab").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function viewdurationselect(dur) {
        try {
            //camfilter1
            document.getElementById('cameraDuration').text = dur;
            if (dur == 15) {
                var el = document.getElementById('camviewfilter1');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }

            }
            else if (dur == 30) {
                var el = document.getElementById('camviewfilter2');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 45) {
                var el = document.getElementById('camviewfilter3');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 60) {
                var el = document.getElementById('camviewfilter4');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter3');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter5');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
            else if (dur == 120) {
                var el = document.getElementById('camviewfilter5');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('camviewfilter1');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('camviewfilter2');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('camviewfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
                var ell5 = document.getElementById('camviewfilter3');
                if (ell5) {
                    ell5.className = 'fa fa-square-o';
                }
            }
        }
        catch (err)
        { alert(err) }
    }
    function getTasklistItems(id) {
        document.getElementById("taskItemsList").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getTaskListData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskItemsnameSpan").innerHTML = data.d[0];
                    for (var i = 1; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("taskItemsList");
                        var li = document.createElement("li");
                        li.innerHTML = '<a href="#taskDocument"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="showTaskDocument(&apos;' + res[1] + '&apos;)">' + res[0] + '</a>';
                        //li.appendChild(document.createTextNode(data.d[i]));
                        ul.appendChild(li);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function getTicketLocationMarkers(obj) {


        locationAllowed = true;
        //setTimeout(function () {
        google.maps.visualRefresh = true;
        var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 15,
            center: Liverpool,
            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
        };

        // This makes the div with id "map_canvas" a google map
        mapticketLocation = new google.maps.Map(document.getElementById("map_canvasTicketLocation"), mapOptions);

        for (var i = 0; i < obj.length; i++) {

            var contentString = '<div id="content">' + obj[i].Username +
            '<br/></div>';

            var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

            var marker = new google.maps.Marker({ position: myLatlng, map: mapticketLocation, title: obj[i].Username });
            marker.setIcon('../Images/marker.png')
            myMarkersIncidentLocation[obj[i].Username] = marker;
            createInfoWindowIncidentLocation(marker, contentString);
        }
    }
    function getIncidentLocationMarkers(obj) {


        locationAllowed = true;
        //setTimeout(function () {
        google.maps.visualRefresh = true;
        var firstRed = false;
        var Liverpool;
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].State == "PURPLE") {
                Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
            }
            else if (obj[i].State == "RED") {
                if (!firstRed) {
                    Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    firstRed = true;
                }
            }
        }
        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 10,
            center: Liverpool,
            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
        };
        // This makes the div with id "map_canvas" a google map
        incidentmapIncidentLocation = new google.maps.Map(document.getElementById("map_canvasIncidentLocation"), mapOptions);
        var poligonCoords = [];
        var first = true;
        for (var i = 0; i < obj.length; i++) {

            var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '"  onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>'
            //'<div id="content">' +'Name:' + obj[i].Username +'<br/><input class="specialTaskbutton tasksfont"  type="button" id="' + obj[i].Id + obj[i].Username + '" style="width:100px;" value="ADD" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)" />''</div>';

            var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);


            if (obj[i].State == "YELLOW") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "GREEN") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "BLUE") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "OFFUSER") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "OFFCLIENT") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
            }
            else if (obj[i].State == "PURPLE") {
                var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                incidentcreateInfoWindowIncidentLocation(marker, contentString);
                document.getElementById("rowIncidentName").text = obj[i].Username;
            }
            else if (obj[i].State == "RED") {
                if (first) {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    contentString = '<div class="help-block text-center pt-2x"><p style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="inline-block red-color">' + obj[i].Username + '</p></div><div class="help-block text-center"></div>';
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                    document.getElementById("rowIncidentName").text = obj[i].Username;
                    first = false;
                }
                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                poligonCoords.push(point);
            }

        }
        if (poligonCoords.length > 0) {
            updatepoligon = new google.maps.Polyline({
                path: poligonCoords,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            updatepoligon.setMap(incidentmapIncidentLocation);
        }
    }
    function incidentcreateInfoWindowIncidentLocation(marker, popupContent) {
        google.maps.event.addListener(marker, 'click', function () {
            incidentinfoWindowLocation.setContent(popupContent);
            incidentinfoWindowLocation.open(incidentmapIncidentLocation, this);
        });
    }
    function updateIncidentMarker(obj) {
        try {
            var poligonCoords = [];
            var tracepoligonCoords = [];
            var first = true;
            var first2 = true;
            var currentSubLocation;
            var secondfirst = true;
            var previousColor = "";
            var subpoligonCoords = [];
            var previousMarker = document.getElementById("rowIncidentName").text;
            for (var i = 0; i < Object.size(incidentmyTraceBackMarkers) ; i++) {
                if (incidentmyTraceBackMarkers[i] != null) {
                    incidentmyTraceBackMarkers[i].setMap(null);
                }
            }

            if (typeof tracebackpoligon === 'undefined') {
                // your code here.
            }
            else {
                tracebackpoligon.setMap(null);
            }
            if (typeof previousMarker === 'undefined') {
                // your code here.
            }
            else {
                incidentMyMarkersIncidentLocation[previousMarker].setMap(null);
                if (typeof updatepoligon === 'undefined') {
                    // your code here.
                }
                else {
                    updatepoligon.setMap(null);
                }
            }
            var tbcounter = 0;
            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '"  onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>'

                //'<div id="content">' +'Name:' + obj[i].Username +'<br/><input class="specialTaskbutton tasksfont"  type="button" id="' + obj[i].Id + obj[i].Username + '" style="width:100px;" value="ADD" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)" />''</div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                incidentmapIncidentLocation.setCenter(new google.maps.LatLng(obj[i].Lat, obj[i].Long));
                incidentmapIncidentLocation.setZoom(9);
                if (obj[i].State == "YELLOW") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "GREEN") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOnline.png')
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "BLUE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/freeclient.png')
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "OFFUSER") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerOffline.png')
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "OFFCLIENT") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/offlineclient.png')
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "PURPLE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                    contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                    incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                    document.getElementById("rowIncidentName").text = obj[i].Username;
                }
                else if (obj[i].State == "ENG") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png')
                    contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                    incidentmyTraceBackMarkers[tbcounter] = marker;
                    tbcounter++;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "COM") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png')
                    contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                    incidentmyTraceBackMarkers[tbcounter] = marker;
                    tbcounter++;
                    incidentcreateInfoWindowIncidentLocation(marker, contentString);
                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    subpoligonCoords.push(point);
                    if (subpoligonCoords.length > 0) {
                        var subpoligon = new google.maps.Polyline({
                            path: subpoligonCoords,
                            geodesic: true,
                            strokeColor: previousColor,
                            strokeOpacity: 1.0,
                            strokeWeight: 7,
                            icons: [{
                                icon: iconsetngs,
                                offset: '100%'
                            }]
                        });
                        subpoligon.setMap(incidentmapIncidentLocation);
                        animateCircle(subpoligon);
                        incidentmyTraceBackMarkers[tbcounter] = subpoligon;
                        tbcounter++;
                    }
                }
                else if (obj[i].State == "RED") {
                    if (first) {
                        contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                        var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                        incidentcreateInfoWindowIncidentLocation(marker, contentString);
                        document.getElementById("rowIncidentName").text = obj[i].Username;
                        first = false;
                    }
                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    poligonCoords.push(point);
                }
                else {
                    if (secondfirst) {
                        currentSubLocation = obj[i].State;
                        var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                        incidentmyTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                        previousColor = obj[i].Logs;
                        incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                        incidentcreateInfoWindowIncidentLocation(marker, contentString);
                        secondfirst = false;
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        subpoligonCoords.push(point);

                    }
                    else {
                        if (currentSubLocation == obj[i].State) {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                            var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                            incidentmyTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                        }
                        else {
                            if (subpoligonCoords.length > 0) {
                                var subpoligon = new google.maps.Polyline({
                                    path: subpoligonCoords,
                                    geodesic: true,
                                    strokeColor: previousColor,
                                    strokeOpacity: 1.0,
                                    strokeWeight: 7,
                                    icons: [{
                                        icon: iconsetngs,
                                        offset: '100%'
                                    }]
                                });
                                subpoligon.setMap(incidentmapIncidentLocation);
                                animateCircle(subpoligon);
                                incidentmyTraceBackMarkers[tbcounter] = subpoligon;
                                tbcounter++;
                            }
                            subpoligonCoords = [];
                            currentSubLocation = obj[i].State;
                            var marker = new google.maps.Marker({ position: myLatlng, map: incidentmapIncidentLocation, title: obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                            incidentmyTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                            previousColor = obj[i].Logs;
                            incidentMyMarkersIncidentLocation[obj[i].Username] = marker;
                            incidentcreateInfoWindowIncidentLocation(marker, contentString);
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);
                        }
                    }
                }
            }
            if (poligonCoords.length > 0) {
                updatepoligon = new google.maps.Polyline({
                    path: poligonCoords,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                updatepoligon.setMap(incidentmapIncidentLocation);
            }
            if (tracepoligonCoords.length > 0) {
                tracebackpoligon = new google.maps.Polyline({
                    path: tracepoligonCoords,
                    geodesic: true,
                    strokeColor: '#1b93c0',
                    strokeOpacity: 1.0,
                    strokeWeight: 7,
                    icons: [{
                        icon: iconsetngs,
                        offset: '100%'
                    }]
                });
                tracebackpoligon.setMap(incidentmapIncidentLocation);
                animateCircle(tracebackpoligon);
            }
            if (subpoligonCoords.length > 0) {
                var subpoligon = new google.maps.Polyline({
                    path: subpoligonCoords,
                    geodesic: true,
                    strokeColor: previousColor,
                    strokeOpacity: 1.0,
                    strokeWeight: 7,
                    icons: [{
                        icon: iconsetngs,
                        offset: '100%'
                    }]
                });
                subpoligon.setMap(incidentmapIncidentLocation);
                animateCircle(subpoligon);
                incidentmyTraceBackMarkers[tbcounter] = subpoligon;
                tbcounter++;
            }
        }
        catch (err) {
            //showAlert(err);
        }
    }
    function escalateClick() {
        var id = document.getElementById("rowidChoice").text;
        var notes = document.getElementById("escalateTextarea").value;
        var isErr = false;
        if (!isEmptyOrSpaces(document.getElementById('escalateTextarea').value)) {
            if (isSpecialChar(document.getElementById('escalateTextarea').value)) {
                isErr = true;
                showAlert("Kindly remove special character from text area");
            }
        }
        if (!isErr) {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/escalateIncident",
                data: "{'id':'" + id + "','ins':'" + notes + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length == 0) {
                        showAlert("Error 55: Problem face when trying to escalate incident.");
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        jQuery('#viewDocument1').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = "Incident has been escalated!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                }
            });
    }
}
function handleTicket() {
    var id = document.getElementById('rowidChoiceTicket').value;
    jQuery.ajax({
        type: "POST",
        url: "Tasks.aspx/handleTicket",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "SUCCESS") {
                jQuery('#ticketingViewCard').modal('hide');
                document.getElementById('successincidentScenario').innerHTML = "Ticket has successfully been resolved";
                jQuery('#successfulDispatch').modal('show');
            }
            else if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d[0]);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function handledClick() {
            document.getElementById("incidentinitialOptionsDiv").style.display = "none";
            document.getElementById("incidenthandleOptionsDiv").style.display = "block";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("incidentrejectOptionsDiv").style.display = "none";
            document.getElementById("escalateOptionsDiv").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";

            document.getElementById("incidentrejectOptionsDiv").style.display = "none";
            document.getElementById("incidentrejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("escalatedDIV").style.display = "none";

        }
        function escalateOptionClick() {
            document.getElementById("resolutionTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("incidentinitialOptionsDiv").style.display = "none";
            document.getElementById("parkLi").style.display = "none";
            document.getElementById("incidenthandleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("incidentrejectOptionsDiv").style.display = "none";
            document.getElementById("incidentrejectionTextarea").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "block";
            document.getElementById("escalateOptionsDiv").style.display = "block";
        }
        function resolveClick() {
            document.getElementById("incidentinitialOptionsDiv").style.display = "none";
            document.getElementById("incidenthandleOptionsDiv").style.display = "none";
            document.getElementById("completedOptionsDiv").style.display = "none";
            document.getElementById("dispatchOptionsDiv").style.display = "none";
            document.getElementById("incidentrejectOptionsDiv").style.display = "none";
            document.getElementById("incidentrejectionTextarea").style.display = "none";
            document.getElementById("resolvedDiv").style.display = "none";
            document.getElementById("escalateTextarea").style.display = "none";
            document.getElementById("completedOptionsDiv2").style.display = "block";
            document.getElementById("resolutionTextarea").style.display = "block";
            document.getElementById("escalateOptionsDiv").style.display = "none";
        }
        function uploadattachment(id, imgpath) {
            if (typeof imgpath === 'undefined') {

            }
            else {
                var res = imgpath.replace(/\\/g, "|")
                try {
                    jQuery.ajax({
                        type: "POST",
                        url: "Tasks.aspx/uploadMobileAttachment",
                        data: "{'id':'" + id + "','imgpath':'" + res + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (err) {
            alert(err);
        }
    }
}
function IncidentStateChange(state) {

    var id = document.getElementById("rowidChoice").text;
    var ins = document.getElementById("selectinstructionTextarea").value;
    var isErr = false;
    if (state == "Reject") {
        ins = document.getElementById("incidentrejectionTextarea").value;

        if (isEmptyOrSpaces(ins)) {
            ins = document.getElementById("resolutionTextarea").value;
        }
    }
    if (state == "Resolve") {
        ins = document.getElementById("resolutionTextarea").value;
    }
    if (!isEmptyOrSpaces(ins)) {
        if (isSpecialChar(ins)) {
            isErr = true;
            showAlert("Kindly remove special character from text area");
        }
    }
    if (!isErr) {
        var imgPath = document.getElementById("mobimagePath2").text;
        uploadattachment(id, imgPath);
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/changeIncidentState",
            data: "{'id':'" + id + "','state':'" + state + "','ins':'" + ins + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "") {
                    showAlert("Error changing incident state");
                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (state != "Dispatch") {
                    jQuery('#viewDocument1').modal('hide');
                    document.getElementById('successincidentScenario').innerHTML = data.d + " status has been changed to " + state;
                    jQuery('#successfulDispatch').modal('show');
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
}

var imgcount = 0;
function nextImg() {
    var found = false;
    hideTaskplay();
    for (var i = 0; i < divArray.length; i++) {
        var el = document.getElementById(divArray[i]);
        if (el.classList.contains("active")) {
            el.className = 'tab-pane fade ';
            if (i + 1 < imgcount) {
                var el3 = document.getElementById(divArray[i + 1]);
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
            }
            else {
                var el3 = document.getElementById("tasklocation-tab");
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
            }
            found = true;
            break;
        }
    }
    if (!found) {
        if (divArray.length > 0) {
            var ell = document.getElementById(divArray[0]);
            if (ell) {
                ell.className = 'tab-pane fade active in';
            }
            var ell1 = document.getElementById("tasklocation-tab");
            if (ell1) {
                ell1.className = 'tab-pane fade';
            }
        }
    }
}
function backImg() {
    hideTaskplay();
    var found = false;
    for (var i = 0; i < divArray.length; i++) {
        var el = document.getElementById(divArray[i]);
        if (el.classList.contains("active")) {
            el.className = 'tab-pane fade ';
            if (i == 0) {
                var elz = document.getElementById("tasklocation-tab");
                if (elz) {
                    elz.className = 'tab-pane fade active in';
                }
            }
            else if (i - imgcount < imgcount) {
                var el3 = document.getElementById(divArray[i - 1]);
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
            }
            else {
                var el3 = document.getElementById("tasklocation-tab");
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
            }
            found = true;
            break;
        }
    }
    if (!found) {
        if (divArray.length > 0) {
            var ell = document.getElementById(divArray[imgcount - 1]);
            if (ell) {
                ell.className = 'tab-pane fade active in';
            }
            var ell1 = document.getElementById("tasklocation-tab");
            if (ell1) {
                ell1.className = 'tab-pane fade';
            }
        }
    }
}

function ticketnextImg() {
    var found = false;
    for (var i = 0; i < divArray.length; i++) {
        var el = document.getElementById(divArray[i]);
        if (el.classList.contains("active")) {
            el.className = 'tab-pane fade ';
            if (i + 1 < imgcount) {
                var el3 = document.getElementById(divArray[i + 1]);
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
            }
            else {
                var el3 = document.getElementById("ticketlocation-tab");
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
            }
            found = true;
            break;
        }
    }
    if (!found) {
        if (divArray.length > 0) {
            var ell = document.getElementById(divArray[0]);
            if (ell) {
                ell.className = 'tab-pane fade active in';
            }
            var ell1 = document.getElementById("ticketlocation-tab");
            if (ell1) {
                ell1.className = 'tab-pane fade';
            }
        }
    }
}
function ticketbackImg() {
    var found = false;
    for (var i = 0; i < divArray.length; i++) {
        var el = document.getElementById(divArray[i]);
        if (el.classList.contains("active")) {
            el.className = 'tab-pane fade ';
            if (i == 0) {
                var elz = document.getElementById("ticketlocation-tab");
                if (elz) {
                    elz.className = 'tab-pane fade active in';
                }
            }
            else if (i - imgcount < imgcount) {
                var el3 = document.getElementById(divArray[i - 1]);
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
            }
            else {
                var el3 = document.getElementById("ticketlocation-tab");
                if (el3) {
                    el3.className = 'tab-pane fade active in';
                }
            }
            found = true;
            break;
        }
    }
    if (!found) {
        if (divArray.length > 0) {
            var ell = document.getElementById(divArray[imgcount - 1]);
            if (ell) {
                ell.className = 'tab-pane fade active in';
            }
            var ell1 = document.getElementById("ticketlocation-tab");
            if (ell1) {
                ell1.className = 'tab-pane fade';
            }
        }
    }
}

function nextLiClick() {
    document.getElementById("nextLi").style.display = "block";
    document.getElementById("finishLi").style.display = "none";
}
function finishLiClick() {
    document.getElementById("finishLi").style.display = "block";
    document.getElementById("nextLi").style.display = "none";
}
function nextMapLiClick() {
    document.getElementById("nextLi").style.display = "block";
    document.getElementById("finishLi").style.display = "none";
    var name = document.getElementById("rowidChoice").text;
    jQuery.ajax({
        type: "POST",
        url: "Tasks.aspx/getGPSDataUsers",
        data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                var obj = jQuery.parseJSON(data.d)
                updateIncidentMarker(obj);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function checkLiveVideo(camera) {
    var output = "";
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/checkLiveVideo",
        data: "{'camera':'" + camera + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                output = data.d;
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
    return output;
}
function getAssigneeType(stringVal) {
    var output = "";
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/getAssigneeType",
        data: "{'id':'" + stringVal + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                output = data.d;
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
    return output;
}
function sendpushMultipleNotification(selectedUserIds, messages) {
    jQuery.ajax({
        type: "POST",
        url: "Tasks.aspx/sendpushMultipleNotification",
        data: JSON.stringify({ userIds: selectedUserIds, msg: messages, uname: loggedinUsername }),
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function insertNewTask(id, assigneetype, assigneename, assigneeid, templatename, longi, lati, incidentId) {
    var output = "";
    jQuery.ajax({
        type: "POST",
        url: "Tasks.aspx/inserttask",
        data: "{'id':'" + id + "','assigneetype':'" + assigneetype + "','assigneename':'" + assigneename + "','assigneeid':'" + assigneeid + "','templatename':'" + templatename + "','longi':'" + longi + "','lati':'" + lati + "','incidentId':'" + incidentId + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            output = data.d;
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
    return output;
}
function dispatchIncident() {
    try {
        var inciName = document.getElementById("incidentNameHeader").innerHTML;
        var id = document.getElementById("rowidChoice").text;
        var list = document.getElementById("sendToListBox");
        var isCameraAttached = document.getElementById("cameraCheck").checked;
        var isTaskAttached = document.getElementById("TaskCheck").checked;
        var longi = document.getElementById("rowLongitude").text;
        var lati = document.getElementById("rowLatitude").text;
        var instruction = document.getElementById("selectinstructionTextarea").value;
        var camDetails = "";
        var cameraValue;
        var taskValue;
        var isErr = false;
        if (isSpecialChar(instruction)) {
            isErr = true;
        }
        if (isTaskAttached) {
            var msgtemplate2 = document.getElementById("MainContent_IncidentCardControl_selectTaskTemplate").options[document.getElementById("MainContent_IncidentCardControl_selectTaskTemplate").selectedIndex].value;
            if (msgtemplate2 > 0) {
                taskValue = msgtemplate2;
            }
            else {
                isErr = true;
                showAlert("Please provide a task you wish to attach");
            }
        }
        if (isCameraAttached) {
            if (document.getElementById("MainContent_IncidentCardControl_cameraSelect").selectedIndex >= 0) {
                var msgtemplate = document.getElementById("MainContent_IncidentCardControl_cameraSelect").options[document.getElementById("MainContent_IncidentCardControl_cameraSelect").selectedIndex].value;
                if (msgtemplate == "Select Camera") {
                    isErr = true;
                    showAlert("Please provide a camera you wish to attach");
                }
                else {
                    var camName = document.getElementById("MainContent_IncidentCardControl_cameraSelect").options[document.getElementById("MainContent_IncidentCardControl_cameraSelect").selectedIndex].text;
                    var checkVal = checkLiveVideo(camName);
                    if (checkVal == "SUCCESS") {
                        cameraValue = msgtemplate;
                        camDetails = cameraValue;
                        if (typeof document.getElementById("cameraDuration").text === 'undefined') {
                            isErr = true;
                            showAlert("Please select the camera duration");
                        }
                        else {
                            instruction = instruction + "|" + document.getElementById('cameraDuration').text;
                        }

                    }
                    else {
                        isErr = true;
                        showAlert(checkVal);
                    }
                }
            }
            else {
                isErr = true;
                showAlert("No Camera available kindly unselect adding camera to dispatch.");
            }
        }
        document.getElementById("<%=tbincidentName2.ClientID%>").value = inciName;
                if (!isErr) {
                    var selected = [];
                    if (list.length > 0) {
                        IncidentStateChange('Dispatch');
                        var i;
                        for (i = 0; i < list.length; i++) {

                            if (isTaskAttached) {
                                insertNewTask(isTaskAttached, getAssigneeType(list.options[i].value), list.options[i].text, list.options[i].value, taskValue, longi, lati, id);
                            }
                            if (getAssigneeType(list.options[i].value) == "User") {

                                chat.server.sendIncidenttoUser(btoa(list.options[i].text), "ARL┴" + inciName + "┴" + instruction + "┴" + lati + "┴" + longi + "≈" + camDetails, list.options[i].value, id);
                                selected.push(list.options[i].text);
                            }
                            else {
                                chat.server.sendIncidenttoDevice(btoa(list.options[i].text), "ARL┴" + inciName + "┴" + instruction + "┴" + lati + "┴" + longi + "≈" + camDetails, id);
                            }
                        }
                    }
                    else {
                        isErr = true;
                        showAlert("Please provide assignees");
                    }

                    if (selected.length > 0)
                        sendpushMultipleNotification(selected, inciName);

                    if (!isErr) {
                        jQuery('#viewDocument1').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = inciName + " has been successfully Dispatch";
                        jQuery('#successfulDispatch').modal('show');
                    }
                }
            }
            catch (err) {
                showAlert('Error 56: Problem faced when trying to dispatch incident.-' + err);
            }
        }
        function setMapOnAll(obj, Markers) {
            try {

                for (var i = 0; i < obj.length; i++) {
                    if (Markers[obj[i].Username] != null) {
                        Markers[obj[i].Username].setMap(null);
                    }
                    else {

                    }
                }
            }
            catch (err) {
                //showAlert('');
            }
        }
        function generateIncidentPDF() {
            widthincident = 1;
            moveincident();
            document.getElementById("incidentpdfloadingAccept").style.display = "block";
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/CreatePDFIncident",
                data: "{'id':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d != "Failed to generate report!") {
                        // jQuery('#viewDocument1').modal('hide');
                        window.open(data.d);
                        // document.getElementById('successfulReportScenario').innerHTML = "Report successfully created!";
                        //  jQuery('#successfulReport').modal('show');
                        showAlert("Report successfully created!");
                        document.getElementById("incidentpdfloadingAccept").style.display = "none";
                    }
                    else {
                        showError(data.d);
                        document.getElementById("incidentpdfloadingAccept").style.display = "none";
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function clearChecklistBox() {
            document.getElementById('newChecklistID').value = "";
            document.getElementById('tbType1Description').value = "";
            document.getElementById('tbType1Quantity').value = "";
            document.getElementById('tbType1Name').value = "";
            document.getElementById('tbType1Name').readOnly = false;
            document.getElementById('tbType2Description').value = "";
            document.getElementById('tbType2EntryQuantity').value = "";
            document.getElementById('tbType2ExitQuantity').value = "";
            document.getElementById('tbType2Name').value = "";
            document.getElementById('tbType2Name').readOnly = false;
            document.getElementById('tbType3Description').value = "";
            document.getElementById('tbType3Name').value = "";
            document.getElementById('tbType3Name').readOnly = false;
            document.getElementById('tbType4Sub').value = "";
            document.getElementById('tbType4Main').value = "";
            document.getElementById('tbType4Name').value = "";
            document.getElementById('tbType4Main').readOnly = false;
            document.getElementById('tbType4Name').readOnly = false;

            document.getElementById('tbType5Sub').value = "";
            document.getElementById('tbType5Main').value = "";
            document.getElementById('tbType5Name').value = "";
            document.getElementById('tbType5Name').readOnly = false;
            document.getElementById('tbType5Main').readOnly = false;

            document.getElementById('tbType1').style.display = "block";
            document.getElementById('tbType3').style.display = "block";
            document.getElementById('tbType5').style.display = "block";

            $("#newType1Table tbody").empty();
            $("#newType3Table tbody").empty();
            $("#newType5Table tbody").empty();
        }
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }
        //INCIDENT END
        var incidentfirstpressTaskChoice = false;
        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
        function readySchedule() {
            try {
                var delay = 500;

                setTimeout(schedulerclickdelay, delay)
                
                //getUsersByNameDuty(loggedinUsername);
                  //LoadAlloyUISchedule(loggedinUsername);
            } catch (err) {
            }
        }
        function schedulerclickdelay() {
            try {
                document.querySelector('.aui-scheduler-base-today').click();
            } catch (err) { }
        }
        var A,dayview, weekview, monthview, eventRecorder;

        jQuery(document).ajaxStart(function () {
           
            showLoader();

            jQuery("#ls").css("display", "block");
        }); 
        jQuery(document).ready(function () {
            try {
                A = YUI().use(
  'aui-scheduler', 'aui',
  function (A) {
      // code goes here

      eventRecorder = new A.SchedulerEventRecorder({
          destroyed: true,
          duration: 1
      });
      LoadAlloyUISchedule();
      //finish loading gant
      document.getElementById("mySchedulerStart").style.display = "none";
      document.getElementById("mySchedulerFinish").style.display = "block";
      document.querySelector('.aui-scheduler-base-today').click();
  }
);
                 
                incidentinfoWindowLocation = new google.maps.InfoWindow();
                infoWindowLocation = new google.maps.InfoWindow();
                infoWindowIncidentLocation = new google.maps.InfoWindow();
                infoWindowIncidentLocationTemplate = new google.maps.InfoWindow();
                infoVerWindowLocation = new google.maps.InfoWindow();
                getEventStatusTotal();
            }
            catch (err) { }

            try {
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    if ($(e.target).attr('href') == "#home-tab" || $(e.target).attr('href') == "#checklist-tab" || $(e.target).attr('href') == "#templates-tab" || $(e.target).attr('href') == "#settings-tab" || $(e.target).attr('href') == "#projects-tab")//|| $(e.target).attr('href') == "#schedule-tab"
                        localStorage.setItem('activeTabTask', $(e.target).attr('href'));
                    //alert($(e.target).attr('href')+'----------NOW Selected');

                    //if ($(e.target).attr('href') == "#schedule-tab") {
                    //    readySchedule();
                    //}

                });
                var activeTab = localStorage.getItem('activeTabTask');
                if (activeTab) {
                    //alert(activeTab +'--------Selected');
                    //$('#myTab a[href="' + activeTab + '"]').tab('show'); 
                    jQuery('a[data-toggle="tab"][href="' + activeTab + '"]').tab('show');
                }
                localStorage.removeItem("activeTabDev");
                localStorage.removeItem("activeTabInci");
                localStorage.removeItem("activeTabMessage");
                localStorage.removeItem("activeTabTick");
                localStorage.removeItem("activeTabUB");
                localStorage.removeItem("activeTabVer");
                localStorage.removeItem("activeTabLost");

                $('input[type=password]').keyup(function () {
                    // keyup event code here
                    var pswd = $(this).val();
                    if (pswd.length < 8) {
                        $('#length').removeClass('valid').addClass('invalid');
                        lengthGood = false;
                    } else {
                        $('#length').removeClass('invalid').addClass('valid');
                        lengthGood = true;
                    }
                    //validate letter
                    if (pswd.match(/[A-z]/)) {
                        $('#letter').removeClass('invalid').addClass('valid');
                        letterGood = true;

                    } else {
                        $('#letter').removeClass('valid').addClass('invalid');
                        letterGood = false;
                    }

                    //validate capital letter
                    if (pswd.match(/[A-Z]/)) {
                        $('#capital').removeClass('invalid').addClass('valid');
                        capitalGood = true;
                    } else {
                        $('#capital').removeClass('valid').addClass('invalid');

                        capitalGood = false;
                    }

                    //validate number
                    if (pswd.match(/\d/)) {
                        $('#number').removeClass('invalid').addClass('valid');
                        numGood = true;

                    } else {
                        $('#number').removeClass('valid').addClass('invalid');
                        numGood = false;
                    }
                });
                $('input[type=password]').focus(function () {
                    // focus code here
                    $('#pswd_info').show();
                });
                $('input[type=password]').blur(function () {
                    // blur code here
                    $('#pswd_info').hide();
                });

            } catch (err)
            { alert(err); }

            var myDropzone2 = new Dropzone("#dz-upload");
            myDropzone2.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */
                file.previewElement.addEventListener("click", function () {
                    myDropzone2.removeFile(file);
                    document.getElementById("mobimagePath2").text = '';
                });
                if (typeof document.getElementById("mobimagePath2").text === 'undefined' || document.getElementById("mobimagePath2").text == '') {
                    if (file.type != "image/jpeg" && file.type != "image/png") {
                        showAlert("Kindly provided a JPEG or PNG Image for upload");
                        this.removeFile(file);
                    }
                    else {
                        var data = new FormData();
                        data.append(file.name, file);
                        jQuery.ajax({
                            url: "../Handlers/MobileIncidentUpload.ashx",
                            type: "POST",
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                document.getElementById("mobimagePath2").text = result;
                                //myDropzone.removeAllFiles(true);
                            },
                            error: function (err) {
                            }
                        });
                    }
                }
                else {
                    showAlert("You can only add one image at a time kindly delete previous image");
                    this.removeFile(file);
                }
            });
            var myDropzoneTicket = new Dropzone("#dz-postticket");
            myDropzoneTicket.on("addedfile", function (file) {

                width = 1;
                moveTick();
                document.getElementById("pdfloadingAcceptTick").style.display = "block";
                document.getElementById("gnNoteTick").innerHTML = "ATTACHING FILE";

                var data = new FormData();
                data.append(file.name, file);
                jQuery.ajax({
                    url: "../Handlers/MobileIncidentUpload.ashx",
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|")
                        jQuery.ajax({
                            type: "POST",
                            url: "Tasks.aspx/attachFileToTicket",
                            data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','filepath':'" + document.getElementById("imagePostAttachment").text + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showAlert(data.d);
                                rowchoiceTicket(document.getElementById('rowidChoiceTicket').value);
                                document.getElementById("pdfloadingAcceptTick").style.display = "none";
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                },
                error: function (err) {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            })
        });


        var myDropzonePost2 = new Dropzone("#dz-post");
        myDropzonePost2.on("addedfile", function (file) {
            /* Maybe display some more file information on your page */
            width = 1;
            move();
            document.getElementById("pdfloadingAccept").style.display = "block";
            document.getElementById("gnNote").innerHTML = "ATTACHING FILE";
            if (file.type != "image/jpeg" && file.type != "image/png" && file.type != "application/pdf") {
                showAlert("Kindly provided a JPEG , PNG Image or PDF for upload");
                this.removeFile(file);
                document.getElementById("pdfloadingAccept").style.display = "none";
            }
            else {
                var data = new FormData();
                data.append(file.name, file);
                jQuery.ajax({
                    url: "../Handlers/MobileIncidentUpload.ashx",
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|")
                        jQuery.ajax({
                            type: "POST",
                            url: "Tasks.aspx/attachFileToInci",
                            data: "{'id':'" + document.getElementById('rowidChoice').text + "','filepath':'" + document.getElementById("imagePostAttachment").text + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                                else {
                                    showAlert(data.d);
                                    showIncidentDocument(document.getElementById('rowidChoice').text);
                                    document.getElementById("pdfloadingAccept").style.display = "none";
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    },
                    error: function (err) {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                })
            }
        });

        var myDropzonePost = new Dropzone("#dz-postt");
        myDropzonePost.on("addedfile", function (file) {
            /* Maybe display some more file information on your page */
            width = 1;
            moveTask();
            document.getElementById("pdfloadingAcceptTask").style.display = "block";
            document.getElementById("gnNoteTask").innerHTML = "ATTACHING FILE";
            if (file.type != "image/jpeg" && file.type != "image/png" && file.type != "application/pdf") {
                showAlert("Kindly provided a JPEG , PNG Image or PDF for upload");
                this.removeFile(file);
                document.getElementById("pdfloadingAcceptTask").style.display = "none";
            }
            else {
                var data = new FormData();
                data.append(file.name, file);
                jQuery.ajax({
                    url: "../Handlers/MobileIncidentUpload.ashx",
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|")
                        jQuery.ajax({
                            type: "POST",
                            url: "Tasks.aspx/attachFileToTask",
                            data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','filepath':'" + document.getElementById("imagePostAttachment").text + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                } else {
                                    showAlert(data.d);
                                    showTaskDocument(document.getElementById('rowChoiceTasks').value);
                                    document.getElementById("pdfloadingAcceptTask").style.display = "none";
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });

                    },
                    error: function (err) {
                    }
                })
            }
        });

        var myDropzoneImport = new Dropzone("#dz-import");
        myDropzoneImport.on("addedfile", function (file) {
            /* Maybe display some more file information on your page */
            showLoader();
            if (file.type != "text/csv" && file.type != "application/vnd.ms-excel") {
                showAlert("Kindly upload correct csv file");
                this.removeFile(file);
                hideLoader();
            }
            else {
                var data = new FormData();
                data.append(file.name, file);
                jQuery.ajax({
                    url: "../Handlers/MobileIncidentUpload.ashx",
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        document.getElementById("imageImportAttachment").text = result.replace(/\\/g, "|")
                        jQuery.ajax({
                            type: "POST",
                            url: "Tasks.aspx/importChecklist",
                            data: "{'id':'0','filepath':'" + document.getElementById("imageImportAttachment").text + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                } else if (data.d == "SUCCESS") {
                                    document.getElementById('successincidentScenario').innerHTML = "Successfully imported checklist";
                                    jQuery('#successfulDispatch').modal('show');
                                    hideLoader();
                                }
                                else {
                                    showAlert(data.d);
                                    hideLoader();
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });

                    },
                    error: function (err) {
                        hideLoader();

                    }
                })
            }
        });

        var chartjsPie1;
        var chartjsPie2;
        jQuery('#projDocument').on('shown.bs.modal', function () {
            try {
                document.getElementById("chartjs-pie1").style.display = "none";
                document.getElementById("chartjs-pie2").style.display = "none";
                document.getElementById("dvchartjs-pie1").style.display = "none";
                document.getElementById("dvchartjs-pie2").style.display = "none";

                var el2 = document.getElementById('pprogress-tab');
                if (el2) {
                    el2.className = 'tab-pane fade active in';
                }
                var el = document.getElementById('ptlist-tab');
                if (el) {
                    el.className = 'tab-pane fade ';
                }

                var el3 = document.getElementById('liprg');
                if (el3) {
                    el3.className = 'active';
                }
                var el5 = document.getElementById('litlist');
                if (el5) {
                    el5.className = ' ';
                }



                try {
                    chartjsPie1.destroy();
                    chartjsPie2.destroy();
                }
                catch (err) {

                }
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getProjChartData1",
                    data: "{'id':'" + document.getElementById("rowChoiceProject").value + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d.length > 0) {
                            document.getElementById("chartjs-pie1").style.display = "block";
                            document.getElementById("chartjs-pie2").style.display = "block";
                            document.getElementById("dvchartjs-pie1").style.display = "block";
                            document.getElementById("dvchartjs-pie2").style.display = "block";
                            var res = data.d[0].split("|");

                            var pieCtx1 = document.getElementById('chartjs-pie1').getContext('2d');
                            var pieData = {
                                datasets: [{
                                    data: [res[0], res[1], res[2], res[3]], backgroundColor: [
                    '#b2163b',
                    '#f2c400',
                    '#3ebb64',
                    '#1b93c0'
                                    ]
                                }],
                                labels: [
                                'Pending',
                                'Inprogress',
                                'Completed',
                                'Accepted'
                                ]
                                //{ value: res[0], color: '#b2163b', highlight: '#b2163b', label: 'Pending' },
                                //{ value: res[1], color: '#f2c400', highlight: '#f2c400', label: 'Inprogress' },
                                //{ value: res[2], color: '#3ebb64', highlight: '#3ebb64', label: 'Completed' },
                                //{ value: res[3], color: '#1b93c0', highlight: '#1b93c0', label: 'Accepted' }
                            }
                            chartjsPie1 = new Chart(pieCtx1, {
                                type: 'pie',
                                data: pieData,
                                options: {
                                    legend: {
                                        labels: {
                                            fontSize: 9,
                                            boxWidth: 20
                                        }
                                    }
                                },
                                responsive: false,
                                title: {
                                    position: 'top',
                                    fontSize: 12,
                                    fontFamily: 'Arial',
                                    fontColor: '#666',
                                    fontStyle: 'bold',
                                    padding: 10,
                                    lineHeight: 1.2,
                                    display: true,
                                    text: 'By Status'
                                },
                                maintainAspectRatio: false,
                                legend: {
                                    display: true,
                                    enabled: true,
                                    position: 'top'
                                }
                            });
                            //var pData = [];
                            var dataA = [];
                            var dataC = [];
                            var dataL = [];
                            for (var i = 1; i < data.d.length; i++) {
                                var res2 = data.d[i].split("|");
                                var iC = 0;
                                iC = res2[0];
                                var cc = getRandomColor();
                                dataA.push(iC);
                                dataC.push(cc);
                                dataL.push(res2[3]);
                                //pData[i - 1] = {
                                //    value: iC, color: cc, highlight: cc, label: res2[3] };
                            }

                            var pData = {
                                datasets: [{
                                    data: dataA, backgroundColor: dataC
                                }], labels: dataL
                            }

                            var pieCtx2 = document.getElementById('chartjs-pie2').getContext('2d');

                            chartjsPie2 = new Chart(pieCtx2, {
                                type: 'pie',
                                data: pData,
                                options: {
                                    legend: {
                                        labels: {
                                            fontSize: 9,
                                            boxWidth: 20
                                        }
                                    }
                                },
                                responsive: false,
                                title: {
                                    position: 'top',
                                    fontSize: 12,
                                    fontFamily: 'Arial',
                                    fontColor: '#666',
                                    fontStyle: 'bold',
                                    padding: 10,
                                    lineHeight: 1.2,
                                    display: true,
                                    text: 'By Status'
                                },
                                maintainAspectRatio: false,
                                legend: {
                                    display: true,
                                    enabled: true,
                                    position: 'top'
                                }
                            });
                        }
                        else {
                            document.getElementById("chartjs-pie1").style.display = "none";
                            document.getElementById("chartjs-pie2").style.display = "none";
                            document.getElementById("dvchartjs-pie1").style.display = "none";
                            document.getElementById("dvchartjs-pie2").style.display = "none";
                        }
                    }
                });
            }
            catch (err) { alert(err) }
        });

        jQuery('#newDocument').on('shown.bs.modal', function () {
            if (firstOpenNewTask == false) {
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getLocationData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            if (data.d != "]") {
                                var obj = jQuery.parseJSON(data.d)
                                getLocationMarkers(obj);
                            }
                            else {
                                getLocationNoMarkers(obj);
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
                firstOpenNewTask = true;
            }
        });

        jQuery('#ticketingViewCard').on('shown.bs.modal', function () {
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getIncidentLocationData",
                data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d)
                        getTicketLocationMarkers(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        });

        jQuery('#viewDocument1').on('shown.bs.modal', function () {
            if (incidentfirstpressTaskChoice == false) {
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getIncidentLocationData",
                    data: "{'id':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var obj = jQuery.parseJSON(data.d)
                            getIncidentLocationMarkers(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
                incidentfirstpressTaskChoice = true;
            }
            else {
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getIncidentLocationData",
                    data: "{'id':'" + document.getElementById('rowidChoice').text + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var obj = jQuery.parseJSON(data.d)
                            updateIncidentMarker(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        });
        jQuery('#taskDocument').on('shown.bs.modal', function () {
            if (firstpressTaskChoice == false) {
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getTaskLocationData",
                    data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var obj = jQuery.parseJSON(data.d)
                            getTaskLocationMarkers(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
                firstpressTaskChoice = true;
            }
            else {
                jQuery.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getTaskLocationData",
                    data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var obj = jQuery.parseJSON(data.d)
                            updateTaskMarker(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        });

        jQuery('#verificationDocument').on('shown.bs.modal', function () {
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getVerifyLocationData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d)
                        getVerLocationMarkers(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        });
        addrowtoTableProject();
        addrowtoTableTaskType();
        assignUserTableData();
        initializetables();
        addrowtoTableAllTasks();


        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getProjectListByCustomerId",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var options = $("#projectlst");
                for (var i = 0; i < data.d.length; i++) {
                    options.append(
                         $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                     );
                }
            }
        });

        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getCustomerByProjectId",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var options = $("#customerslst");
                for (var i = 0; i < data.d.length; i++) {
                    options.append(
                         $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                     );
                }
            }
        });
    });

    function addProjTask() {
        jQuery('#newDocument').modal('show');
        jQuery('#projDocument').modal('hide');

        newTaskClick();

        $('#projectlst option').remove();

        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getProjectListByCustomerId",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var options = $("#projectlst");
                for (var i = 0; i < data.d.length; i++) {
                    options.append(
                         $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                     );
                }
            }
        });

        $("#projectlst").val(document.getElementById("rowChoiceProject").value);
        $('#contractslst option').remove();
        var contractoption = $("#contractslst");
        contractoption.append($('<option></option>').val(0).html('<%=selectContractPlaceholder%>'));
        $('#customerslst option').remove();
        var cid = 0;

        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getCustomerByProjectId",
            data: "{'id':'" + document.getElementById("rowChoiceProject").value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var options = $("#customerslst");
                for (var i = 0; i < data.d.length; i++) {
                    cid = data.d[i].Id;
                    options.append(
                         $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                     );
                }
            }
        });
        $("#customerslst").val(cid);
        document.getElementById('tbCustomerId').value = cid;
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getContractListByCustomerId",
            data: "{'id':'" + cid + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var options = $("#contractslst");
                for (var i = 0; i < data.d.length; i++) {
                    options.append(
                         $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                     );
                }
            }
        });
        document.getElementById('projectlst').disabled = true;
        document.getElementById('customerslst').disabled = true;
    }

    //EDI DEMO VERIFICATION
    function enlargeDisplayImage(type) {
        if (type == "Sent") {
            document.getElementById("enlargeIMG").src = document.getElementById("veriSentIMG").src;
        }
        else {
            document.getElementById("enlargeIMG").src = document.getElementById("veriResultIMG").src;
        }
    }
    function enlargeDisplayImage2(type) {
        if (type == "Sent") {
            document.getElementById("enlargeIMG").src = document.getElementById("veriSentIMG2").src;
        }
        else {
            document.getElementById("enlargeIMG").src = document.getElementById("veriResultIMG2").src;
        }
    }



    var infoVerWindowLocation;
    var myVerMarkersLocation = new Array();
    var mapVerLocation;
    function getVerLocationMarkers(obj) {


        locationAllowed = true;
        google.maps.visualRefresh = true;

        var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 12,
            center: Liverpool,
            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
        };

        mapVerLocation = new google.maps.Map(document.getElementById("map_verLocation"), mapOptions);
        for (var i = 0; i < obj.length; i++) {

            var contentString = '<div id="content">' + obj[i].Username +
            '<br/></div>';

            var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

            var marker = new google.maps.Marker({ position: myLatlng, map: mapVerLocation, title: obj[i].Username });
            marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
            myVerMarkersLocation[obj[i].Username] = marker;
            createverInfoWindowLocation(marker, contentString);
        }
    }
    function createverInfoWindowLocation(marker, popupContent) {
        google.maps.event.addListener(marker, 'click', function () {
            infoVerWindowLocation.setContent(popupContent);
            infoVerWindowLocation.open(mapVerLocation, this);
        });
    }
    function assignVerifierRequestData() {
        jQuery('#verificationDocument').modal('show');
        jQuery('#taskDocument').modal('hide');
        var id = document.getElementById('verifierID').value;
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getVerifyLocationData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var obj = jQuery.parseJSON(data.d)
                getVerLocationMarkers(obj);
            }
        });
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getVerifierRequestData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                document.getElementById("verifyUserSpan").innerHTML = data.d[0];
                document.getElementById("verifyTypeSpan").innerHTML = data.d[1];
                document.getElementById("verifyCaseSpan").innerHTML = data.d[2];
                document.getElementById("verifyTimeSpan").innerHTML = data.d[3];
                document.getElementById("verifyLocSpan").innerHTML = data.d[4];
                document.getElementById("verifyReasonSpan").innerHTML = data.d[5];
                document.getElementById("verifyBYearSpan").innerHTML = data.d[6];
                document.getElementById("verifyEthniSpan").innerHTML = data.d[7];
                document.getElementById("verifyPIDSpan").innerHTML = data.d[8];
                document.getElementById("verifyGenderSpan").innerHTML = data.d[9];
                document.getElementById("verifyListTypeSpan").innerHTML = data.d[10];
                document.getElementById("veriResultIMG").src = data.d[12];
                document.getElementById("veriSentIMG").src = data.d[11];
                if (data.d.length > 14) {
                    document.getElementById("verifyHeaderSpan").innerHTML = data.d[23];
                    document.getElementById("veriResultIMG").src = data.d[13];
                    document.getElementById("veriResult1IMG").src = data.d[13];
                    document.getElementById("veriResult2IMG").src = data.d[15];
                    document.getElementById("veriResult3IMG").src = data.d[17];
                    document.getElementById("veriResult4IMG").src = data.d[19];
                    document.getElementById("veriResult5IMG").src = data.d[21];

                    document.getElementById("resultPercent1").innerHTML = data.d[14] + "%";
                    document.getElementById("resultPercent2").innerHTML = data.d[16] + "%";
                    document.getElementById("resultPercent3").innerHTML = data.d[18] + "%";
                    document.getElementById("resultPercent4").innerHTML = data.d[20] + "%";
                    document.getElementById("resultPercent5").innerHTML = data.d[22] + "%";
                    document.getElementById("resultPercentMain").innerHTML = data.d[14] + "%";
                    document.getElementById('progressbarMainDisplay').setAttribute("style", "width:" + data.d[14] + "%");

                    document.getElementById("verCaseName1").text = data.d[24];
                    document.getElementById("verCaseName2").text = data.d[26];
                    document.getElementById("verCaseName3").text = data.d[28];
                    document.getElementById("verCaseName4").text = data.d[30];
                    document.getElementById("verCaseName5").text = data.d[32];

                    document.getElementById("verifyCaseSpan").innerHTML = data.d[25];
                    document.getElementById("verCaseID1").text = data.d[25];
                    document.getElementById("verCaseID2").text = data.d[27];
                    document.getElementById("verCaseID3").text = data.d[29];
                    document.getElementById("verCaseID4").text = data.d[31];
                    document.getElementById("verCaseID5").text = data.d[33];

                }
                else {
                    jQuery('#verificationFail').modal('show');
                    document.getElementById("veriResult1IMG").src = "";
                    document.getElementById("veriResult2IMG").src = "";
                    document.getElementById("veriResult3IMG").src = "";
                    document.getElementById("veriResult4IMG").src = "";
                    document.getElementById("veriResult5IMG").src = "";

                    document.getElementById("resultPercent1").innerHTML = "";
                    document.getElementById("resultPercent2").innerHTML = "";
                    document.getElementById("resultPercent3").innerHTML = "";
                    document.getElementById("resultPercent4").innerHTML = "";
                    document.getElementById("resultPercent5").innerHTML = "";
                    document.getElementById("resultPercentMain").innerHTML = "";
                    document.getElementById('progressbarMainDisplay').setAttribute("style", "width:1%");
                }
            }
        });
    }
    //After Completed Functions
    function taskStateChange(state) {
        var isErr = false;
        if (!isEmptyOrSpaces(document.getElementById('taskrejectionTextarea').value)) {
            if (isSpecialChar(document.getElementById('taskrejectionTextarea').value)) {
                isErr = true;
                showAlert("Kindly remove special character from text area");
            }
        }
        if (!isErr) {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/ChangeTaskState",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','state':'" + state + "','rejection':'" + document.getElementById('rejectionTextarea').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Task successfully updated";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert('Error 47: Error occured trying to change state of task.-' + data.d);
                    }
                }
            });
    }
}

function myUserTaskAssign() {
    try {

        var assignType = document.getElementById("myAssignType").value;
        if (assignType == "User") {
            var assignId = document.getElementById("MainContent_myusersearchselect").value;
            var assigneeName = getSelectedText('MainContent_myusersearchselect');
        }
        else {
            var assignId = document.getElementById("MainContent_mygrpsearchselect").value;
            var assigneeName = getSelectedText('MainContent_mygrpsearchselect');
        }
        var isErr = false;
        if (!isEmptyOrSpaces(document.getElementById('taskrejectionTextarea').value)) {
            if (isSpecialChar(document.getElementById('taskrejectionTextarea').value)) {
                isErr = true;
                showAlert("Kindly remove special character from text area");
            }
        }

        if (!assigneeName) {
            isErr = true;
            showAlert("Kindly provide assignee for the task");
        }
        if (assigneeName == "Select Assignee") {
            isErr = true;
            showAlert("Kindly provide assignee for the task");
        }
        if (!isErr) {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/assignToNewUserTask",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','userid':'" + assignId + "','rejection':'" + document.getElementById('taskrejectionTextarea').value + "','username':'" + assigneeName + "','uname':'" + loggedinUsername + "','type':'" + assignType + "','ttype':'assign'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "SUCCESS") {
                        if (assignType == "User") {
                            chat.server.sendtoUser(btoa(data.d[2]), "TASK┴" + document.getElementById("taskincidentNameHeader").innerHTML + "┴" + document.getElementById("taskdescriptionSpan").innerHTML + "┴0┴0", assignId);
                        }
                        else {
                            chat.server.sendtoGroup(btoa(data.d[2]), assignId, "TASK┴" + document.getElementById("taskincidentNameHeader").innerHTML + "┴" + document.getElementById("taskdescriptionSpan").innerHTML + "┴0┴0");
                        }
                        jQuery('#taskDocument').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + assigneeName;
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                        }
                    }
                });
        }
    }
    catch (ex)
    { showAlert('Error 49: Error occured trying to get task information-' + data.d); }
}
function userTaskAssign(val) {
    try {
        if (val == "assign") {
            var assignType = document.getElementById("rejecttaskSelectAssigneeType").value;
            if (assignType == "User") {
                var assignId = document.getElementById("MainContent_rejectusersearchselect").value;
                var assigneeName = getSelectedText('MainContent_rejectusersearchselect');
            }
            else {
                var assignId = document.getElementById("MainContent_rejectgroupsearchselect").value;
                var assigneeName = getSelectedText('MainContent_rejectgroupsearchselect');
            }
        }
        else {
            var assignType = "User";
            var dd = document.getElementById('MainContent_rejectusersearchselect');
            for (var i = 0; i < dd.options.length; i++) {
                if (dd.options[i].text === document.getElementById("tasktypeSpan").innerHTML) {
                    dd.selectedIndex = i;
                    break;
                }
            }
            var assignId = document.getElementById("MainContent_rejectusersearchselect").value;
            var assigneeName = getSelectedText('MainContent_rejectusersearchselect');
        }
        var isErr = false;
        if (!isEmptyOrSpaces(document.getElementById('taskrejectionTextarea').value)) {
            if (isSpecialChar(document.getElementById('taskrejectionTextarea').value)) {
                isErr = true;
                showAlert("Kindly remove special character from text area");
            }
        }
        else {
            isErr = true;
            showAlert("Kindly provide rejection notes");
        }

        if (!assigneeName && val == "assign") {
            isErr = true;
            showAlert("Kindly provide assignee for the task");
        }
        if (assigneeName == "Select Assignee" && val == "assign") {
            isErr = true;
            showAlert("Kindly provide assignee for the task");
        }
        if (!isErr) {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/assignToNewUserTask",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','userid':'" + assignId + "','rejection':'" + document.getElementById('taskrejectionTextarea').value + "','username':'" + assigneeName + "','uname':'" + loggedinUsername + "','type':'" + assignType + "','ttype':'" + val + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "SUCCESS") {
                        if (val == "assign") {
                            if (assignType == "User") {
                                chat.server.sendtoUser(btoa(data.d[2]), "TASK┴" + document.getElementById("taskincidentNameHeader").innerHTML + "┴" + document.getElementById("taskdescriptionSpan").innerHTML + "┴0┴0", assignId);
                                jQuery('#taskDocument').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + assigneeName;
                                jQuery('#successfulDispatch').modal('show');
                            }
                            else {
                                chat.server.sendtoGroup(btoa(data.d[2]), assignId, "TASK┴" + document.getElementById("taskincidentNameHeader").innerHTML + "┴" + document.getElementById("taskdescriptionSpan").innerHTML + "┴0┴0");
                                jQuery('#taskDocument').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + assigneeName;
                                jQuery('#successfulDispatch').modal('show');
                            }
                        }
                        else if (val == "reassign") {
                            if (data.d[1] == "User") {
                                chat.server.sendtoUser(btoa(data.d[2]), "TASK┴" + document.getElementById("taskincidentNameHeader").innerHTML + "┴" + document.getElementById("taskdescriptionSpan").innerHTML + "┴0┴0", assignId);
                                jQuery('#taskDocument').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + assigneeName;
                                jQuery('#successfulDispatch').modal('show');
                            }
                            else {
                                chat.server.sendtoGroup(btoa(data.d[2]), data.d[3], "TASK┴" + document.getElementById("taskincidentNameHeader").innerHTML + "┴" + document.getElementById("taskdescriptionSpan").innerHTML + "┴0┴0");
                                jQuery('#taskDocument').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Task successfully assigned to " + data.d[2];
                                jQuery('#successfulDispatch').modal('show');
                            }
                        }
                    }
                    else if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                            }
                        }
                    });
            }
        }
        catch (ex)
        { showAlert('Error 49: Error occured trying to get task information'); }
    }
    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    function deleteChoiceChkItem(type) {
        if (type == '4') {
            var pId = document.getElementById('t4newChecklistID').value;
            if (pId == "") {
                pId = 0;
            }
            newdeletechecklistchoice(4, pId, 0);
            NextChecklistType4();
        }
        else if (type == '5') {
            var pId = document.getElementById('t5newChecklistID').value;
            if (pId == "") {
                pId = 0;
            }
            newdeletechecklistchoice(5, pId, 0);
            NextChecklistType5();
        }
    }
    //CHecklists
    function AddChecklistParent(type) {
        var name = "";
        var desc = "";
        var qt1 = "";
        var qt2 = "";
        var isPassed = true;
        document.getElementById('deleteChoiceChkItem5').style.display = "none";
        if (type == "1") {
            name = document.getElementById('tbType1Name').value;
            desc = document.getElementById('tbType1Description').value;
            qt1 = document.getElementById('tbType1Quantity').value;
            qt2 = "0";
        }
        else if (type == "2") {
            name = document.getElementById('tbType2Name').value;
            desc = document.getElementById('tbType2Description').value;
            qt1 = document.getElementById('tbType2EntryQuantity').value;
            qt2 = document.getElementById('tbType2ExitQuantity').value;
        }
        else if (type == "3") {
            name = document.getElementById('tbType3Name').value;
            desc = document.getElementById('tbType3Description').value;
            qt1 = "0";
            qt2 = "0";
        }
        else if (type == "4") {
            name = document.getElementById('tbType4Name').value;
            desc = document.getElementById('tbType4Main').value;
            qt1 = document.getElementById('tbType4Sub').value;
            qt2 = "0";
        }
        else if (type == "5") {
            name = document.getElementById('tbType5Name').value;
            desc = document.getElementById('tbType5Main').value;
            qt1 = document.getElementById('tbType5Sub').value;
            qt2 = "0";
        }
        if (isEmptyOrSpaces(name)) {
            showAlert("Please provide name for your checklist");
            isPassed = false;
        }
        else if (isEmptyOrSpaces(desc)) {
            showAlert("Please provide missing field for checklist item");
            isPassed = false;
        }
        else if (isEmptyOrSpaces(qt1)) {
            showAlert("Please provide missing field for checklist item");
            isPassed = false;
        }
        else if (isEmptyOrSpaces(qt2)) {
            showAlert("Please provide missing field for checklist item");
            isPassed = false;
        }
        else {

            if (isSpecialChar(name)) {
                isPassed = false;
            }
            if (isSpecialChar(desc)) {
                isPassed = false;
            }
            if (isSpecialChar(qt1)) {
                isPassed = false;
            }
            if (isSpecialChar(qt2)) {
                isPassed = false;
            }
            if (type != "5" && type != "4") {
                if (!isNumeric(qt1)) {
                    showAlert("Please provide numeric entry quantity for checklist item");
                    isPassed = false;
                }
                else if (!isNumeric(qt2)) {
                    showAlert("Please provide numeric exit quantity for checklist item");
                    isPassed = false;
                }
            }
            if (isPassed) {
                if (document.getElementById('newChecklistID').value == "") {
                    $.ajax({
                        type: "POST",
                        url: "Tasks.aspx/AddChecklistParent",
                        data: "{'name':'" + name + "','type':'" + type + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            if (data.d > 0) {
                                document.getElementById('newChecklistID').value = data.d;
                                document.getElementById('editChoiceChecklist').value = data.d;
                                if (type == "1") {
                                    type1AddChecklistItem(document.getElementById('newChecklistID').value, desc, qt1);
                                    document.getElementById('tbType1Description').value = "";
                                    document.getElementById('tbType1Quantity').value = "";
                                    document.getElementById('tbType1Name').readOnly = true;

                                    document.getElementById('tbType3').style.display = "none";
                                    document.getElementById('tbType5').style.display = "none";
                                }
                                else if (type == "2") {
                                    type2AddChecklistItem(document.getElementById('newChecklistID').value, desc, qt1, qt2);
                                    document.getElementById('tbType2Description').value = "";
                                    document.getElementById('tbType2EntryQuantity').value = "";
                                    document.getElementById('tbType2ExitQuantity').value = "";
                                    document.getElementById('tbType2Name').readOnly = true;

                                    document.getElementById('tbType1').style.display = "none";
                                    document.getElementById('tbType3').style.display = "none";
                                    document.getElementById('tbType5').style.display = "none";
                                }
                                else if (type == "3") {
                                    type3AddChecklistItem(document.getElementById('newChecklistID').value, desc);
                                    document.getElementById('tbType3Description').value = "";
                                    document.getElementById('tbType3Name').readOnly = true;

                                    document.getElementById('tbType1').style.display = "none";
                                    document.getElementById('tbType5').style.display = "none";
                                }
                                else if (type == "4") {
                                    type4AddChecklistItem(document.getElementById('newChecklistID').value, desc, qt1, 0);
                                    document.getElementById('tbType4Sub').value = "";
                                    document.getElementById('tbType4Main').readOnly = true;
                                    document.getElementById('tbType4Name').readOnly = true;

                                    document.getElementById('tbType1').style.display = "none";
                                    document.getElementById('tbType3').style.display = "none";
                                    document.getElementById('tbType5').style.display = "none";
                                }
                                else if (type == "5") {
                                    type5AddChecklistItem(document.getElementById('newChecklistID').value, desc, qt1, 0);
                                    document.getElementById('tbType5Sub').value = "";
                                    document.getElementById('tbType5Main').readOnly = true;
                                    document.getElementById('tbType5Name').readOnly = true;

                                    document.getElementById('tbType1').style.display = "none";
                                    document.getElementById('tbType3').style.display = "none";

                                    document.getElementById('deleteChoiceChkItem5').style.display = "block";

                                }
                                checklistwasadded = true;
                            }
                            else {
                                showAlert("Error 50: Error occured trying to create checklist");
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                if (type == "1") {
                    type1AddChecklistItem(document.getElementById('newChecklistID').value, desc, qt1);
                    document.getElementById('tbType1Description').value = "";
                    document.getElementById('tbType1Quantity').value = "";
                }
                else if (type == "2") {
                    type2AddChecklistItem(document.getElementById('newChecklistID').value, desc, qt1, qt2);
                    document.getElementById('tbType2Description').value = "";
                    document.getElementById('tbType2EntryQuantity').value = "";
                    document.getElementById('tbType2ExitQuantity').value = "";
                }
                else if (type == "3") {
                    type3AddChecklistItem(document.getElementById('newChecklistID').value, desc);
                    document.getElementById('tbType3Description').value = "";
                }
                else if (type == "4") {
                    var pId = document.getElementById('t4newChecklistID').value;
                    if (pId == "") {
                        pId = 0;
                    }
                    type4AddChecklistItem(document.getElementById('newChecklistID').value, desc, qt1, pId);
                    document.getElementById('tbType4Sub').value = "";
                    document.getElementById('tbType4Main').readOnly = true;
                    document.getElementById('tbType4Name').readOnly = true;
                }
                else if (type == "5") {
                    var pId = document.getElementById('t5newChecklistID').value;
                    if (pId == "") {
                        pId = 0;
                    }
                    type5AddChecklistItem(document.getElementById('newChecklistID').value, desc, qt1, pId);
                    document.getElementById('tbType5Sub').value = "";
                    document.getElementById('tbType5Main').readOnly = true;
                    document.getElementById('tbType5Name').readOnly = true;
                }
            }

        }
    }
}
function type1AddChecklistItem(id, desc, qt) {
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/type1AddChecklistItem",
        data: "{'id':'" + id + "','desc':'" + desc + "','qt':'" + qt + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d != "") {
                $("#newType1Table tbody").append(data.d);
            }
            else {
                showAlert("Error 51: Error occured trying to insert item into checklist");
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function type2AddChecklistItem(id, desc, qt1, qt2) {

    $.ajax({
        type: "POST",
        url: "Tasks.aspx/type2AddChecklistItem",
        data: "{'id':'" + id + "','desc':'" + desc + "','qt1':'" + qt1 + "','qt2':'" + qt2 + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d != "") {
                $("#newType2Table tbody").append(data.d);
            }
            else {
                showAlert("Error 51: Error occured trying to insert item into checklist");
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });

}
function type3AddChecklistItem(id, desc) {

    $.ajax({
        type: "POST",
        url: "Tasks.aspx/type3AddChecklistItem",
        data: "{'id':'" + id + "','desc':'" + desc + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d != "") {
                $("#newType3Table tbody").append(data.d);
            }
            else {
                showAlert("Error 51: Error occured trying to insert item into checklist");
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });

}
function type4AddChecklistItem(id, desc, qt, pId) {

    $.ajax({
        type: "POST",
        url: "Tasks.aspx/type4AddChecklistItem",
        data: "{'id':'" + id + "','desc':'" + desc + "','qt':'" + qt + "','pId':'" + pId + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d[0] != "") {
                $("#newType4Table tbody").append(data.d[0]);
                document.getElementById('t4newChecklistID').value = data.d[1];
            }
            else {
                showAlert("Error 51: Error occured trying to insert item into checklist");
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });

}
function type5AddChecklistItem(id, desc, qt, pId) {

    $.ajax({
        type: "POST",
        url: "Tasks.aspx/type5AddChecklistItem",
        data: "{'id':'" + id + "','desc':'" + desc + "','qt':'" + qt + "','pId':'" + pId + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d[0] != "") {
                $("#newType5Table tbody").append(data.d[0]);
                document.getElementById('t5newChecklistID').value = data.d[1];
            }
            else {
                showAlert("Error 51: Error occured trying to insert item into checklist");
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });

}
function NextChecklistType4() {
    document.getElementById('tbType4Main').value = "";
    document.getElementById('tbType4Main').readOnly = false;
    document.getElementById('t4newChecklistID').value = "";
}
function NextChecklistType5() {
    document.getElementById('tbType5Main').value = "";
    document.getElementById('tbType5Main').readOnly = false;
    document.getElementById('t5newChecklistID').value = "";
}
function checklistCancel() {
    if (document.getElementById('newChecklistID').value != "") {
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/delChecklistData",
            data: "{'id':'" + document.getElementById('newChecklistID').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {
                    document.getElementById('newChecklistID').value = "";
                    document.getElementById('tbType1Description').value = "";
                    document.getElementById('tbType1Quantity').value = "";
                    document.getElementById('tbType1Name').value = "";
                    document.getElementById('tbType1Name').readOnly = false;
                    document.getElementById('tbType2Description').value = "";
                    document.getElementById('tbType2EntryQuantity').value = "";
                    document.getElementById('tbType2ExitQuantity').value = "";
                    document.getElementById('tbType2Name').value = "";
                    document.getElementById('tbType2Name').readOnly = false;
                    document.getElementById('tbType3Description').value = "";
                    document.getElementById('tbType3Name').value = "";
                    document.getElementById('tbType3Name').readOnly = false;
                    document.getElementById('tbType4Sub').value = "";
                    document.getElementById('tbType4Main').value = "";
                    document.getElementById('tbType4Name').value = "";
                    document.getElementById('tbType4Main').readOnly = false;
                    document.getElementById('tbType4Name').readOnly = false;

                    document.getElementById('tbType5Sub').value = "";
                    document.getElementById('tbType5Main').value = "";
                    document.getElementById('tbType5Name').value = "";
                    document.getElementById('tbType5Name').readOnly = false;
                    document.getElementById('tbType5Main').readOnly = false;

                    document.getElementById('tbType1').style.display = "block";
                    document.getElementById('tbType3').style.display = "block";
                    document.getElementById('tbType5').style.display = "block";

                    $("#newType1Table tbody").empty();
                    $("#newType3Table tbody").empty();
                    $("#newType5Table tbody").empty();

                    checklistwasadded = false;
                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else {
                showError(data.d);
            }
        },
    error: function () {
        showError("Session timeout. Kindly login again.");
        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
});
    }
}
var checklistwasadded = false;
function checklistCreate() {
    if (checklistwasadded) {
        jQuery('#newChecklist').modal('hide');
        document.getElementById('successincidentScenario').innerHTML = "Checklist has been created";
        jQuery('#successfulDispatch').modal('show');
    }
    else {
        showAlert("No checklist was made.");
    }
}



function newdeletechecklistchoice(ttype, tid, id) {
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/delChecklistItemData",
        data: "{'id':'" + tid + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "SUCCESS") {
                showAlert('Successfully deleted checklist item.')
                try {
                    var pid = document.getElementById('editChoiceChecklist').value;

                    newgetChecklistEditTable(ttype, pid);
                }
                catch (exx) {
                    alert(exx)
                }
            }
            else if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else {
                showError(data.d);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function deletechecklistchoice(ttype, id) {
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/delChecklistItemData",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "SUCCESS") {
                showAlert('Successfully deleted checklist item.')
                try {
                    var pid = document.getElementById('editChoiceChecklist').value;

                    if (ttype == "4")
                        rowchoicetype4sub(document.getElementById('t4SubChecklistID').value, document.getElementById('tbChecklistItemType4').value);
                    else if (ttype == "5")
                        rowchoicetype5sub(document.getElementById('t5SubChecklistID').value, document.getElementById('tbChecklistItemType5').value);
                    else
                        getChecklistEditTable(ttype, pid);
                }
                catch (exx) {
                    alert(exx)
                }
            }
            else if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getChecklistRowData(id) {
            document.getElementById('editChoiceChecklist').value = id;
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getChecklistRowData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                if (data.d[0] == "1") {
                    document.getElementById("editType1Table").style.display = "block";
                    document.getElementById("editType2Table").style.display = "none";
                    document.getElementById("editType3Table").style.display = "none";
                    document.getElementById("editType4Table").style.display = "none";

                    document.getElementById("tbEditChecklistDescriptionDIV").style.display = "block";
                    document.getElementById("tbEditChecklistQuantityDIV").style.display = "block";
                    document.getElementById("tbEditChecklistExitQuantityDIV").style.display = "none";
                    document.getElementById("tbEditChecklistMainDIV").style.display = "none";
                    document.getElementById("tbEditChecklistSubDIV").style.display = "none";
                }
                else if (data.d[0] == "2") {
                    document.getElementById("editType1Table").style.display = "none";
                    document.getElementById("editType2Table").style.display = "block";
                    document.getElementById("editType3Table").style.display = "none";
                    document.getElementById("editType4Table").style.display = "none";

                    document.getElementById("tbEditChecklistDescriptionDIV").style.display = "block";
                    document.getElementById("tbEditChecklistQuantityDIV").style.display = "block";
                    document.getElementById("tbEditChecklistExitQuantityDIV").style.display = "block";
                    document.getElementById("tbEditChecklistMainDIV").style.display = "none";
                    document.getElementById("tbEditChecklistSubDIV").style.display = "none";
                }
                else if (data.d[0] == "3") {
                    document.getElementById("editType1Table").style.display = "none";
                    document.getElementById("editType2Table").style.display = "none";
                    document.getElementById("editType3Table").style.display = "block";
                    document.getElementById("editType4Table").style.display = "none";

                    document.getElementById("tbEditChecklistDescriptionDIV").style.display = "block";
                    document.getElementById("tbEditChecklistQuantityDIV").style.display = "none";
                    document.getElementById("tbEditChecklistExitQuantityDIV").style.display = "none";
                    document.getElementById("tbEditChecklistMainDIV").style.display = "none";
                    document.getElementById("tbEditChecklistSubDIV").style.display = "none";
                }
                else if (data.d[0] == "4") {
                    document.getElementById("editType1Table").style.display = "none";
                    document.getElementById("editType2Table").style.display = "none";
                    document.getElementById("editType3Table").style.display = "none";
                    document.getElementById("editType4Table").style.display = "block";

                    document.getElementById("tbEditChecklistDescriptionDIV").style.display = "none";
                    document.getElementById("tbEditChecklistQuantityDIV").style.display = "none";
                    document.getElementById("tbEditChecklistExitQuantityDIV").style.display = "none";
                    document.getElementById("tbEditChecklistMainDIV").style.display = "block";
                    document.getElementById("tbEditChecklistSubDIV").style.display = "block";
                }
                else if (data.d[0] == "5") {
                    document.getElementById("editType1Table").style.display = "none";
                    document.getElementById("editType2Table").style.display = "none";
                    document.getElementById("editType3Table").style.display = "none";
                    document.getElementById("editType4Table").style.display = "block";

                    document.getElementById("tbEditChecklistDescriptionDIV").style.display = "none";
                    document.getElementById("tbEditChecklistQuantityDIV").style.display = "none";
                    document.getElementById("tbEditChecklistExitQuantityDIV").style.display = "none";
                    document.getElementById("tbEditChecklistMainDIV").style.display = "block";
                    document.getElementById("tbEditChecklistSubDIV").style.display = "block";
                }
                document.getElementById('tbEditChecklistName').value = data.d[1];
                getChecklistEditTable(data.d[0], id);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function newgetChecklistEditTable(tabletype, id) {
    try {
        document.getElementById('rowchoiceChecklistType').value = tabletype;

        $("#newType1Table tbody").empty();
        $("#newType2Table tbody").empty();
        $("#newType3Table tbody").empty();
        $("#newType4Table tbody").empty();
        $("#newType5Table tbody").empty();
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getNewChecklistRows",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    if (data.d.length > 0) {
                        for (var i = 0; i < data.d.length; i++) {
                            if (tabletype == "1") {
                                $("#newType1Table tbody").append(data.d[i]);
                            }
                            else if (tabletype == "2") {
                                $("#newType2Table tbody").append(data.d[i]);
                            }
                            else if (tabletype == "3") {
                                $("#newType3Table tbody").append(data.d[i]);
                            }
                            else if (tabletype == "4") {
                                $("#newType4Table tbody").append(data.d[i]);
                            }
                            else if (tabletype == "5") {
                                $("#newType5Table tbody").append(data.d[i]);
                            }
                        }
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    catch (err) {
        alert(err)
    }
}
function getChecklistEditTable(tabletype, id) {
    try {
        document.getElementById('rowchoiceChecklistType').value = tabletype;
        $("#editType1Table tbody").empty();
        $("#editType2Table tbody").empty();
        $("#editType3Table tbody").empty();
        $("#editType4Table tbody").empty();

        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getEditChecklistItem",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    if (data.d.length > 0) {
                        for (var i = 0; i < data.d.length; i++) {
                            if (tabletype == "1") {
                                $("#editType1Table tbody").append(data.d[i]);

                            }
                            else if (tabletype == "2") {
                                $("#editType2Table tbody").append(data.d[i]);

                            }
                            else if (tabletype == "3") {
                                $("#editType3Table tbody").append(data.d[i]);

                            }
                            else if (tabletype == "4") {
                                $("#editType4Table tbody").append(data.d[i]);

                            }
                            else if (tabletype == "5") {
                                $("#editType4Table tbody").append(data.d[i]);

                            }
                        }
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    catch (err) {
        alert(err)
    }
}
function addToChecklistItem() {
    try {
        var id = document.getElementById('editChoiceChecklist').value;
        var ttype = document.getElementById('rowchoiceChecklistType').value;
        var desc = document.getElementById('tbEditChecklistDescription').value;
        var qt = document.getElementById('tbEditChecklistQuantity').value;
        var qt2 = document.getElementById('tbEditChecklistExitQuantity').value;
        var main = document.getElementById('tbEditChecklistMain').value;
        var sub = document.getElementById('tbEditChecklistSub').value;
        var isPass = true;
        if (ttype != "4" && ttype != "5") {
            if (isEmptyOrSpaces(desc)) {
                isPass = false;
                showAlert("Kindly provide checklist description");
            }
            else if (isSpecialChar(desc)) {
                isPass = false;
            }
        }
        if (isPass) {
            if (ttype == "1") {
                if (isEmptyOrSpaces(qt)) {
                    isPass = false;
                    showAlert("Kindly provide checklist quantity");
                }
                else if (isSpecialChar(qt)) {
                    isPass = false;
                }
                if (isPass) {
                    if (isNumeric(qt))
                        type1AddChecklistItem(id, desc, qt);
                    else {
                        isPass = false;
                        showAlert("Kindly provide numeric quantity");
                    }
                }
            }
            else if (ttype == "2") {
                if (isEmptyOrSpaces(qt)) {
                    isPass = false;
                    showAlert("Kindly provide checklist quantity");
                }
                else if (isSpecialChar(qt)) {
                    isPass = false;
                }
                if (isEmptyOrSpaces(qt2)) {
                    isPass = false;
                    showAlert("Kindly provide checklist quantity");
                }
                else if (isSpecialChar(qt2)) {
                    isPass = false;
                }
                if (isPass) {
                    if (isNumeric(qt) && isNumeric(qt2))
                        type2AddChecklistItem(id, desc, qt, qt2);
                    else {
                        isPass = false;
                        showAlert("Kindly provide numeric quantity");
                    }
                }
            }
            else if (ttype == "3") {
                type3AddChecklistItem(id, desc);
            }
            else if (ttype == "4") {
                if (isEmptyOrSpaces(main)) {
                    isPass = false;
                    showAlert("Kindly provide checklist main");
                }
                else if (isSpecialChar(main)) {
                    isPass = false;
                }
                if (isEmptyOrSpaces(sub)) {
                    isPass = false;
                    showAlert("Kindly provide checklist sub");
                }
                else if (isSpecialChar(sub)) {
                    isPass = false;
                }
                if (isPass) {
                    type4AddChecklistItem(id, main, sub, 0);
                }
            }
            else if (ttype == "5") {
                if (isEmptyOrSpaces(main)) {
                    isPass = false;
                    showAlert("Kindly provide checklist main");
                }
                else if (isSpecialChar(main)) {
                    isPass = false;
                }
                if (isEmptyOrSpaces(sub)) {
                    isPass = false;
                    showAlert("Kindly provide checklist sub");
                }
                else if (isSpecialChar(sub)) {
                    isPass = false;
                }
                if (isPass) {
                    type5AddChecklistItem(id, main, sub, 0);
                }
            }
            document.getElementById('tbEditChecklistDescription').value = "";
            document.getElementById('tbEditChecklistQuantity').value = "";
            document.getElementById('tbEditChecklistExitQuantity').value = "";
            document.getElementById('tbEditChecklistMain').value = "";
            document.getElementById('tbEditChecklistSub').value = "";
            getChecklistEditTable(ttype, id);
        }
    }
    catch (err) {
        alert('Error 60: Problem loading page element.-' + err);
    }
}
function rowchoicetype4sub(id, name) {
    jQuery('#editChecklist').modal('hide');
    document.getElementById('tbChecklistItemType4').value = name;
    document.getElementById('t4SubChecklistID').value = id;
    $("#checklisttabletype4sub tbody").empty();
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/getChecklistItemType4Sub",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#checklisttabletype4sub tbody").append(data.d[i]);
                    }
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}

function rowchoicetype5sub(id, name) {
    jQuery('#editChecklist').modal('hide');
    document.getElementById('tbChecklistItemType5').value = name;
    document.getElementById('t5SubChecklistID').value = id;

    document.getElementById('btneditChecklistType5').style.display = "block";
    document.getElementById('tbChecklistItemTypeSub5').style.display = "block";

    $("#checklisttabletype5sub tbody").empty();
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/getChecklistItemType4Sub",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#checklisttabletype5sub tbody").append(data.d[i]);
                    }
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}

function viewrowchoicetype5sub(id, name) {
    jQuery('#editChecklist').modal('hide');
    document.getElementById('tbChecklistItemType5').value = name;
    document.getElementById('t5SubChecklistID').value = id;

    document.getElementById('btneditChecklistType5').style.display = "none";
    document.getElementById('tbChecklistItemTypeSub5').style.display = "none";

    $("#checklisttabletype5sub tbody").empty();
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/getChecklistItemType4Sub",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#checklisttabletype5sub tbody").append(data.d[i]);
                    }
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}

//Tasks
function myAssignTypeChange() {
    var assignType = document.getElementById("myAssignType").value;
    if (assignType == "User") {
        document.getElementById("myusersearchselectLi").style.display = "block";
        document.getElementById("mygrpsearchselectLi").style.display = "none";
    }
    else if (assignType == "Group") {
        document.getElementById("myusersearchselectLi").style.display = "none";
        document.getElementById("mygrpsearchselectLi").style.display = "block";
    }
}
function rejecttaskselectAssigneeTypeChange() {
    var assignType = document.getElementById("rejecttaskSelectAssigneeType").value;
    if (assignType == "User") {
        document.getElementById("rejectUsersDiv").style.display = "block";
        document.getElementById("rejectGroupDiv").style.display = "none";
    }
    else if (assignType == "Group") {
        document.getElementById("rejectUsersDiv").style.display = "none";
        document.getElementById("rejectGroupDiv").style.display = "block";
    }
}
function taskselectAssigneeTypeChange() {
    var assignType = document.getElementById("taskSelectAssigneeType").value;
    if (document.getElementById("taskSelectAssigneeType").selectedIndex == 0)//assignType == "User") 
    {
        document.getElementById("usersearchSelectDIV").style.display = "block";
        document.getElementById("groupsearchSelectDIV").style.display = "none";
        document.getElementById("devicesearchSelectDIV").style.display = "none";
        document.getElementById("map_canvasLocationDIV").style.display = "block";
    }
    else if (document.getElementById("taskSelectAssigneeType").selectedIndex == 1)//(assignType == "Group") 
    {
        document.getElementById("usersearchSelectDIV").style.display = "none";
        document.getElementById("groupsearchSelectDIV").style.display = "block";
        document.getElementById("devicesearchSelectDIV").style.display = "none";
        document.getElementById("map_canvasLocationDIV").style.display = "block";
    }
    else if (document.getElementById("taskSelectAssigneeType").selectedIndex == 2)//(assignType == "Multiple") 
    {
        document.getElementById("usersearchSelectDIV").style.display = "none";
        document.getElementById("groupsearchSelectDIV").style.display = "none";
        document.getElementById("devicesearchSelectDIV").style.display = "block";
        document.getElementById("map_canvasLocationDIV").style.display = "none";
    }
}
function testchange() {
    var assignType = document.getElementById("testselect").value;

    if (assignType == "User") {
        document.getElementById("usersearchSelectTemplateDIV").style.display = "block";
        document.getElementById("devicesearchSelectTemplateDIV").style.display = "none";
        document.getElementById("groupsearchSelectTemplateDIV").style.display = "none";
        document.getElementById("map_canvasTemplateLocationDIV").style.display = "block";
    }
    else if (assignType == "Group") {
        document.getElementById("usersearchSelectTemplateDIV").style.display = "none";
        document.getElementById("devicesearchSelectTemplateDIV").style.display = "none";
        document.getElementById("groupsearchSelectTemplateDIV").style.display = "block";
        document.getElementById("map_canvasTemplateLocationDIV").style.display = "block";
    }
    else if (assignType == "Multiple") {
        document.getElementById("usersearchSelectTemplateDIV").style.display = "none";
        document.getElementById("devicesearchSelectTemplateDIV").style.display = "block";
        document.getElementById("groupsearchSelectTemplateDIV").style.display = "none";
        document.getElementById("map_canvasTemplateLocationDIV").style.display = "none";
    }
}

function sendTemplateTask() {
    try {
        var name = document.getElementById("tbTemplateName").value;
        if (!isEmptyOrSpaces(name)) {
            var desc = document.getElementById("tbTemplateDescription").value;
            var assignType = document.getElementById("testselect").value;
            var assignId = "0";
            var assigneeName = "";
            if (assignType == "User") {
                assignId = document.getElementById("MainContent_usersearchSelectTemplate").value;
                assigneeName = getSelectedText('MainContent_usersearchSelectTemplate');
            }
            else if (assignType == "Group") {
                assignId = document.getElementById("MainContent_groupsearchSelectTemplate").value;
                assigneeName = getSelectedText('MainContent_groupsearchSelectTemplate');
            }
            else if (assignType == "Multiple") {
                //assignId = document.getElementById("MainContent_devicesearchSelectTemplate").value;
                //assigneeName = getSelectedText('MainContent_devicesearchSelectTemplate');
            }
            var tasktype = document.getElementById("MainContent_NewTaskControl_taskTypeSelect").value;
            var longi = document.getElementById("tbTemplateLongitude").value;
            var lati = document.getElementById("tbTemplateLatitude").value;
            var startdate = document.getElementById("templatetaskCalendar").value;
            var priority = document.getElementById("priorityTemplateSelect").value;
            var checklistid = document.getElementById("MainContent_checklistTemplateSelect").value;
            var lbrecurring = document.getElementById("recurringTemplateSelect").value;
            var recurring = false;
            if (lbrecurring == "Recurring") {
                recurring = false;
            }
            else {
                recurring = true;
            }
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/InsertTaskOnly",
                data: "{'name':'" + name
                    + "','desc':'" + desc
                    + "','assignType':'" + assignType
                    + "','assignId':'" + assignId
                    + "','assigneeName':'" + assigneeName
                    + "','longi':'" + longi
                    + "','lati':'" + lati
                    + "','startdate':'" + startdate
                    + "','priority':'" + priority
                    + "','checklistid':'" + checklistid
                    + "','recurring':'" + recurring
                    + "','lbrecurring':'" + lbrecurring
                    + "','uname':'" + loggedinUsername
                    + "','tasktype':'" + tasktype + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        hideLoader();

                        if (assignType == "User") {
                            chat.server.sendtoUser(btoa(assigneeName), "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi, assignId);
                        }
                        else if (assignType == "Group") {
                            chat.server.sendtoGroup(btoa(assigneeName), assignId, "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi);
                        }

                        document.getElementById('successincidentScenario').innerHTML = "Task has been sent.";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        hideLoader();
                        showAlert('Error 52: Error occured trying to send task template-' + data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        else {
            hideLoader();
            showAlert("Kindly provide task name");
        } sendTask()
    }
    catch (err) {
        hideLoader();
        showAlert('Error 53: Failed to get template information to send-' + err);
    }
}
var isMyself = false;
function assignMySelf() {
    if (!isMyself) {
        document.getElementById("assigneeDIV").style.display = "none";
        isMyself = true;
    }
    else {
        document.getElementById("assigneeDIV").style.display = "block";
        isMyself = false;
    }
}
function sendTask() {
    try {

        var name = document.getElementById("tbNewTaskName").value;

        var linkId = '0';
        if (isLink) {
            linkId = document.getElementById("rowChoiceTasks").value;
        }

        if (!isEmptyOrSpaces(name)) {
            if (!isSpecialChar(name)) {
                var isErr = false;
                var desc = document.getElementById("tbNewDescription").value;
                if (!isEmptyOrSpaces(desc)) {
                    if (isSpecialChar(desc)) {
                        isErr = true;
                    }
                }
                var longi = document.getElementById("tbNewLongitude").value;
                var lati = document.getElementById("tbNewLatitude").value;


                if (!isEmptyOrSpaces(longi)) {
                    if (isInvalidDouble(longi)) {
                        isErr = true;
                    }
                }

                if (!isEmptyOrSpaces(longi)) {
                    if (isInvalidDouble(lati)) {
                        isErr = true;
                    }
                }
                var tasktype = document.getElementById("MainContent_NewTaskControl_taskTypeSelect").value;
                var assignType = document.getElementById("taskSelectAssigneeType").value;
                var assignId = "0";
                var assigneeName = "";
                var isMultiple = false;
                if (assignType == "User") {
                    assignId = document.getElementById("MainContent_NewTaskControl_usersearchSelect").value;
                    assigneeName = getSelectedText('MainContent_NewTaskControl_usersearchSelect');
                }
                else if (assignType == "Group") {
                    assignId = document.getElementById("MainContent_NewTaskControl_groupsearchSelect").value;
                    assigneeName = getSelectedText('MainContent_NewTaskControl_groupsearchSelect');
                }
                else if (assignType == "Multiple") {
                    isMultiple = true;
                }
                if (!isMultiple && assignId == "0" && document.getElementById('myselfCheck').checked == false) {
                    isErr = true;
                    showAlert("Kindly provide assignee for the task");
                }
                var startdate = document.getElementById("newtaskCalendar").value;
                var startT = document.getElementById("newtaskTime").value + ":" + document.getElementById("newtaskTimeMinute").value;
                var s1 = startdate + " " + startT;
                var d1 = new Date(s1);

                var q = new Date();
                var m = q.getMonth();
                var d = q.getDate();
                var y = q.getFullYear();

                var date = new Date(y, m, d);
                var dd = dates.compare(date, d1);

                if (dd == 1) {
                    isErr = true;
                    showAlert("Date should be set to today or future date.")
                }

                var priority = document.getElementById("prioritySelect").value;
                var checklistid = document.getElementById("MainContent_NewTaskControl_checklistSelect").value;
                var lbrecurring = document.getElementById("newrecurringSelect").value;
                var issig = document.getElementById("requiresignatureCheck").checked;
                if (!checklistid) {
                    isErr = true;
                    showAlert("Kindly provide checklist for the task");
                }
                if (!isErr) {
                    var recurring = false;
                    if (lbrecurring == "Recurring") {
                        recurring = false;
                    }
                    else {
                        recurring = true;
                        showAlert("Starting Task Creation for recurring this might take some time. Kindly click ok and wait for success message.")
                    }
                    if (!isMultiple) {
                        $.ajax({
                            type: "POST",
                            url: "Tasks.aspx/InsertTaskOnly",
                            data: "{'name':'" + name
                                + "','desc':'" + desc
                                + "','assignType':'" + assignType
                                + "','assignId':'" + assignId
                                + "','assigneeName':'" + assigneeName
                                + "','longi':'" + longi
                                + "','lati':'" + lati
                                + "','startdate':'" + startdate
                                + "','priority':'" + priority
                                + "','checklistid':'" + checklistid
                                + "','recurring':'" + recurring
                                + "','lbrecurring':'" + lbrecurring
                                + "','uname':'" + loggedinUsername
                                + "','tasktype':'" + tasktype
							    + "','linkId':'" + linkId
							    + "','projectid':'" + document.getElementById('projectlst').value
                                + "','contractId':'" + document.getElementById('contractslst').value
                                + "','customerId':'" + document.getElementById('tbCustomerId').value
                                + "','startT':'" + startT
                                + "','issig':'" + issig
                                + "','myself':'" + document.getElementById('myselfCheck').checked
                                + "'}",

                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d[0] == "SUCCESS") {
                                    hideLoader();


                                    var ss1 = startdate + " 00:00";
                                    var dd1 = new Date(ss1);

                                    var q1 = new Date();
                                    var m2 = q1.getMonth();
                                    var d2 = q1.getDate();
                                    var y2 = q1.getFullYear();

                                    var date12 = new Date(y2, m2, d2);
                                    var dd12 = dates.compare(date12, dd1);

                                    if (dd12 == 0) {
                                        if (assignType == "User") {
                                            chat.server.sendtoUser(btoa(data.d[1]), "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi, assignId);
                                        }
                                        else if (assignType == "Group") {
                                            chat.server.sendtoGroup(btoa(data.d[1]), assignId, "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi);
                                        }
                                    }

                                    jQuery('#newDocument').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Task has been sent.";
                                    jQuery('#successfulDispatch').modal('show');
                                }
                                else if (data.d[0] == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                                else {
                                    hideLoader();
                                    showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        var list = document.getElementById("sendToListBox");
                        var noError = false;
                        if (list.length > 0) {
                            for (i = 0; i < list.length; i++) {
                                var split = list.options[i].text.split('+');
                                var aType = "Multiple";
                                var aName = "empty";
                                if (split.length > 0) {
                                    aType = split[1];
                                    aName = split[0];
                                }
                                $.ajax({
                                    type: "POST",
                                    url: "Tasks.aspx/InsertTaskOnly",
                                    data: "{'name':'" + name
                                + "','desc':'" + desc
                                + "','assignType':'" + aType
                                + "','assignId':'" + list.options[i].value
                                + "','assigneeName':'" + aName
                                + "','longi':'" + longi
                                + "','lati':'" + lati
                                + "','startdate':'" + startdate
                                + "','priority':'" + priority
                                + "','checklistid':'" + checklistid
                                + "','recurring':'" + recurring
                                + "','lbrecurring':'" + lbrecurring
                                + "','uname':'" + loggedinUsername
                                + "','tasktype':'" + tasktype
							    + "','linkId':'" + linkId
							    + "','projectid':'" + document.getElementById('projectlst').value
                                + "','contractId':'" + document.getElementById('contractslst').value
                                + "','customerId':'" + document.getElementById('tbCustomerId').value
                                + "','startT':'" + startT
                                + "','issig':'" + issig
                                + "','myself':'" + document.getElementById('myselfCheck').checked
                                + "'}",
                                    async: false,
                                    dataType: "json",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        if (data.d[0] == "LOGOUT") {
                                            showError("Session has expired. Kindly login again.");
                                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                        } else if (data.d[0] == "SUCCESS") {

                                            var ss1 = startdate + " 00:00";
                                            var dd1 = new Date(ss1);

                                            var q1 = new Date();
                                            var m2 = q1.getMonth();
                                            var d2 = q1.getDate();
                                            var y2 = q1.getFullYear();

                                            var date12 = new Date(y2, m2, d2);
                                            var dd12 = dates.compare(date12, dd1);

                                            if (dd12 == 0) {
                                                if (aType == "User") {
                                                    chat.server.sendtoUser(btoa(data.d[1]), "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi, list.options[i].value);
                                                }
                                                else if (assignType == "Group") {
                                                    chat.server.sendtoGroup(btoa(data.d[1]), list.options[i].value, "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi);
                                                }
                                            }
                                        }
                                        else {
                                            noError = true;
                                            hideLoader();
                                            showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                                        }
                                    },
                                    error: function () {
                                        showError("Session timeout. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    }
                                });
                            }
                            if (!noError) {
                                hideLoader();
                                jQuery('#newDocument').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Task has been sent.";
                                jQuery('#successfulDispatch').modal('show');
                            }
                        }
                        else {
                            showAlert("There are no selected users to send task to.");
                        }
                    }
                }
            }
        }
        else {
            hideLoader();
            showAlert("Kindly provide task name");
        }
    }
    catch (err) {
        hideLoader();
        showAlert('Error 49: Error occured trying to get task information-' + data.d);
    }
}
function saveTemplateTask() {
    var name = document.getElementById("tbNewTaskName").value;
    if (!isEmptyOrSpaces(name)) {
        if (!isSpecialChar(name)) {
            var isErr = false;
            var desc = document.getElementById("tbNewDescription").value;
            if (!isEmptyOrSpaces(desc)) {
                if (isSpecialChar(desc)) {
                    isErr = true;
                }
            }
            var longi = document.getElementById("tbNewLongitude").value;
            var lati = document.getElementById("tbNewLatitude").value;

            if (!isEmptyOrSpaces(longi)) {
                if (isInvalidDouble(longi)) {
                    isErr = true;
                }
            }

            if (!isEmptyOrSpaces(longi)) {
                if (isInvalidDouble(lati)) {
                    isErr = true;
                }
            }
            var tasktype = document.getElementById("MainContent_NewTaskControl_taskTypeSelect").value;
            var assignType = document.getElementById("taskSelectAssigneeType").value;
            var assignId = "0";
            var assigneeName = "";
            //if (assignType == "User") {
            //    assignId = document.getElementById("MainContent_usersearchSelect").value;
            //    assigneeName = getSelectedText('MainContent_usersearchSelect');
            //}
            //else if (assignType == "Group") {
            //    assignId = document.getElementById("MainContent_groupsearchSelect").value;
            //    assigneeName = getSelectedText('MainContent_groupsearchSelect');
            //}
            //if (!assigneeName) {
            //    isErr = true;
            //    showAlert("Kindly provide assignee for the template");
            //}
            var startdate = document.getElementById("newtaskCalendar").value;
            var priority = document.getElementById("prioritySelect").value;
            var checklistid = document.getElementById("MainContent_NewTaskControl_checklistSelect").value;
            var issig = document.getElementById("requiresignatureCheck").checked;
            if (!isErr) {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/InsertTemplateOnly",
                    data: "{'name':'" + name
                        + "','desc':'" + desc
                        + "','assignType':'" + assignType
                        + "','assignId':'" + assignId
                        + "','assigneeName':'" + assigneeName
                        + "','longi':'" + longi
                        + "','lati':'" + lati
                        + "','startdate':'" + startdate
                        + "','priority':'" + priority
                        + "','checklistid':'" + checklistid
                        + "','uname':'" + loggedinUsername
                        + "','tasktype':'" + tasktype
                        + "','projectId':'" + document.getElementById('projectlst').value
                        + "','contractId':'" + document.getElementById('contractslst').value
                        + "','customerId':'" + document.getElementById('tbCustomerId').value
                        + "','issig':'" + issig + "'}",

                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newDocument').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Task Template has been saved.";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            hideLoader();
                            showAlert('Error 54: Error occured while trying to save task information into template-' + data.d);
                        }
                    },
                    error: function (err) {
                        console.log(err)
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
    }
    else {
        hideLoader();
        showAlert("Kindly provide task name");
    }
}
function deleteTask() {
    var isError = false;
    var list = document.getElementById("deleteListBox");
    if (list.length > 0) {
        for (i = 0; i < list.length; i++) {
            if (!isError) {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/delTaskData",
                    data: "{'id':'" + list.options[i].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {

                        }
                        else if (data.d == "LOGOUT") {
                            isError = true;
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                                else {
                                    isError = true;
                                    showError(data.d);
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                }
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/delTaskData",
                    data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                        }
                        else if (data.d == "LOGOUT") {
                            isError = true;
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            isError = true;
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            if (!isError) {
                document.getElementById('successincidentScenario').innerHTML = "Task has been deleted";
                jQuery('#successfulDispatch').modal('show');
            }
        }
        function deleteChecklist() {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/delChecklistData",
                data: "{'id':'" + document.getElementById('rowChoiceChecklist').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Checklist has been deleted";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        var sourceLat = '<%=sourceLat%>';
        var sourceLon = '<%=sourceLon%>';
        function getTemplateLocationMarkers(obj) {


            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 8,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapIncidentLocationTemplate = new google.maps.Map(document.getElementById("map_canvasTemplateLocation"), mapOptions);
            google.maps.event.addListener(mapIncidentLocationTemplate, 'click', function (event) {
                alldeletemarker();
                var location = event.latLng;
                //Create a marker and placed it on the map.
                var marker2 = new google.maps.Marker({
                    position: location,
                    map: mapIncidentLocationTemplate
                });
                marker2.id = uniqueId;
                marker2.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                uniqueId++;
                markers2.push(marker2);
                //Attach click event handler to the marker.
                google.maps.event.addListener(marker2, "click", function (e) {
                    var infoWindow = new google.maps.InfoWindow({
                        content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocationTemplate(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                    });
                    infoWindow.open(mapIncidentLocationTemplate, marker2);
                });
            });
            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="locationTemplateChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocationTemplate, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                myMarkersIncidentLocationTemplate[obj[i].Username] = marker;
                createInfoWindowTemplateLocation(marker, contentString);
            }
            //}, 1000);

        }
        function createInfoWindowTemplateLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindowIncidentLocationTemplate.setContent(popupContent);
                infoWindowIncidentLocationTemplate.open(mapIncidentLocationTemplate, this);
            });
        }
        var updatepoligon;
        function getTaskLocationMarkers(obj) {

            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;

            var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 8,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapIncidentLocation = new google.maps.Map(document.getElementById("taskmap_canvasIncidentLocation"), mapOptions);
            var first = true;
            var poligonCoords = [];
            var tbcounter = 0;
            var pincounter = 0;
            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div id="content">' + obj[i].Username +
                '<br/></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);



                if (obj[i].Username == "Pending") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].Username == "InProgress") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].Username == "Completed") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    if (obj[i].State == "GREEN") {
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                    }
                    else {
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerX.png');
                    }
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].Username == "OnRoute") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].Username == "Accepted") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "RED") {
                    if (first) {
                        first = false;
                        document.getElementById('previousTaskUser').text = obj[i].Username;
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        poligonCoords.push(point);

                    }
                    else {
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        poligonCoords.push(point);
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].LastLog });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                        myTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                    }
                }
                else {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
            }
            if (poligonCoords.length > 0) {
                updatepoligon = new google.maps.Polyline({
                    path: poligonCoords,
                    geodesic: true,
                    strokeColor: '#1b93c0',
                    strokeOpacity: 1.0,
                    strokeWeight: 7,
                    icons: [{
                        icon: iconsetngs,
                        offset: '100%'
                    }]
                });
                updatepoligon.setMap(mapIncidentLocation);
                animateCircle(updatepoligon);
            }
            //}, 1000);
        }
        function createInfoWindowIncidentLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindowIncidentLocation.setContent(popupContent);
                infoWindowIncidentLocation.open(mapIncidentLocation, this);
            });
        }
        function play(i) {
            try {
                var player = document.getElementById('Video' + (i + 1));
                player.play();
            } catch (error) {
            }
        }
        function getChecklistItemsTicket(id) {
            document.getElementById("offencesItemsList").innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getChecklistDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                for (var i = 0; i < data.d.length; i++) {
                    var ul = document.getElementById("offencesItemsList");
                    var li = document.createElement("li");
                    li.appendChild(document.createTextNode(data.d[i]));
                    ul.appendChild(li);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });

}
function getChecklistItems(id) {
    document.getElementById("checklistItemsList").innerHTML = "";
    $.ajax({
        type: "POST",
        url: "Tasks.aspx/getChecklistData",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else if (data.d[0] == "False") {
                        var el = document.getElementById('checklistnamespanFA');
                        if (el) {
                            el.className = "fa fa-square-o";
                        }
                    }
                    else {
                        var el = document.getElementById('checklistnamespanFA');
                        if (el) {
                            el.className = "fa fa-check-square-o";
                        }
                    }
                    for (var i = 1; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("checklistItemsList");
                        var li = document.createElement("li");
                        var marginLeft = '';
                        if (res[2] == 'True') {
                            marginLeft = 'style = "margin-left:-15px;"';
                        }

                        if (res[2] == '3') {

                            if (res[1] == "Checked")
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];
                            else
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];

                            ul.appendChild(li);

                            var li2 = document.createElement("li");
                            li2.innerHTML = '<a style="margin-left:5px;" href="#"  class="capitalize-text" >Notes: ' + res[3] + '</a>';
                            ul.appendChild(li2);
                        }
                        else {
                            if (res[1] == "Checked")
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];
                            else
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];

                            ul.appendChild(li);
                        }
                        //li.appendChild(document.createTextNode(data.d[i]));

                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }

        function getChecklistItemsEdit(id) {
            document.getElementById("checklistItemsList").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getChecklistDataEdit",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else if (data.d[0] == "False") {
                        var el = document.getElementById('checklistnamespanFA');
                        if (el) {
                            el.className = "fa fa-square-o";
                        }
                    }
                    else {
                        var el = document.getElementById('checklistnamespanFA');
                        if (el) {
                            el.className = "fa fa-check-square-o";
                        }
                    }
                    divArrayColor = new Array();
                    for (var i = 1; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("checklistItemsList");
                        var li = document.createElement("li");
                        var marginLeft = '';
                        if (res[2] == 'True') {
                            marginLeft = 'style = "margin-left:-15px;"';
                        }

                        if (res[1] == "Checked")
                            li.innerHTML = '<div  ' + marginLeft + ' class="nice-checkbox inline-block no-vmargin"><input checked type="checkbox" onclick="checkChecklist(&apos;' + res[3] + '&apos;,&apos;' + id + '&apos;)"  id="editMobileCheck' + res[3] + '" name="niceCheck"><label for="editMobileCheck' + res[3] + '">' + res[0] + '</label><i style="margin-left:5px;color:#bbbbbb;" id="comment' + res[3] + '" onclick="selectChecklistItem(&apos;' + res[3] + '&apos;)" class="fa fa-sticky-note"></i></div>' + res[6];
                        else
                            li.innerHTML = '<div ' + marginLeft + ' class="nice-checkbox inline-block no-vmargin"><input type="checkbox" onclick="checkChecklist(&apos;' + res[3] + '&apos;,&apos;' + id + '&apos;)" id="editMobileCheck' + res[3] + '" name="niceCheck"><label for="editMobileCheck' + res[3] + '">' + res[0] + '</label><i style="margin-left:5px;color:#bbbbbb;" id="comment' + res[3] + '" onclick="selectChecklistItem(&apos;' + res[3] + '&apos;)" class="fa fa-sticky-note"></i></div>' + res[6];

                        divArrayColor[i - 1] = 'comment' + res[3];

                        ul.appendChild(li);

                        if (res[4] == '3') {
                            var li2 = document.createElement("li");
                            li2.innerHTML = '<a style="margin-left:5px;" href="#"  class="capitalize-text" >Notes: ' + res[5] + '</a>';
                            ul.appendChild(li2);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        var divArrayColor = new Array();
        function selectChecklistItem(id) {
            if (document.getElementById('comment' + id).style.color == "#b2163b" || document.getElementById('comment' + id).style.color == "rgb(178, 22, 59)") {
                document.getElementById('comment' + id).style.color = "#bbbbbb";
                document.getElementById('commentsId').value = "0";
                document.getElementById('addtaskNotesTA').style.display = 'none';
            }
            else {
                for (var i = 0; i < divArrayColor.length; i++) {
                    var el = document.getElementById(divArrayColor[i]);
                    el.style.color = "#bbbbbb";
                }
                document.getElementById('comment' + id).style.color = "#b2163b";
                document.getElementById('commentsId').value = id;
                document.getElementById('addtaskNotesTA').style.display = 'block';
            }
        }
        function addNotesToTask() {
            if (document.getElementById('addtaskNotesTA').style.display != 'block') {
                document.getElementById('addtaskNotesTA').style.display = 'block';
            }
            else {
                saveTaskRemarks("false");
            }
        }
        function saveTaskRemarks(comp) {
            var projn = document.getElementById("addtaskNotesTA").value;
            var id = document.getElementById('rowChoiceTasks').value;
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('addtaskNotesTA').value)) {
                isPass = false;
                showAlert("Kindly provide notes to be added");
            }
            else {
                if (isSpecialChar(document.getElementById('addtaskNotesTA').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character from notes");
                }
            }
            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/addNewTaskRemarks",
                    data: "{'id':'" + id + "','notes':'" + projn + "','uname':'" + loggedinUsername + "','comp':'" + comp + "','chklistid':'" + document.getElementById('commentsId').value + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            if (comp == "inprogress" || comp == "complete") {
                                showAlert("Task successfully updated");
                                document.getElementById("addtaskNotesTA").value = "";
                                showTaskDocument(id);
                            }
                            else {
                                showAlert("Successfully added notes");
                                document.getElementById("addtaskNotesTA").value = "";
                                showTaskDocument(id);
                            }
                        }
                        else if (data.d == "LOGOUT") {
                            document.getElementById('<%= logoutbtn.ClientID %>').click();
                        }
                        else {
                            if (comp == "inprogress" || comp == "complete") {
                                showError(data.d);
                            }
                            else {
                                showAlert('Failed to save notes. ' + data.d);
                            }
                        }
                    }
                });
        }
    }
    function checkChecklist(id, taskid) {
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/checkChecklistItem",
            data: "{'id':'" + id + "','taskid':'" + taskid + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {

                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        var width = 1;
        function move() {
            var elem = document.getElementById("myBar");
            width = 1;
            var id = setInterval(frame, 100);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        function moveTask() {
            var elem = document.getElementById("myBarTask");
            width = 1;
            var id = setInterval(frame, 100);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }

        function moveTick() {
            var elem = document.getElementById("myBarTick");
            width = 1;
            var id = setInterval(frame, 100);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }

        var widthincident = 1;
        function moveincident() {
            var elem = document.getElementById("incidentmyBar");
            width = 1;
            var id = setInterval(frame, 100);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }

        function generateTaskPDF() {
            width = 1;
            moveTask();
            document.getElementById("pdfloadingAcceptTask").style.display = "block";
            document.getElementById("gnNoteTask").innerHTML = "GENERATING REPORT";
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/CreatePDFTask",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d != "Failed to generate report!") {
                        // jQuery('#taskDocument').modal('hide');
                        window.open(data.d);
                        //document.getElementById('successfulReportScenario').innerHTML = "Report successfully created!";
                        //jQuery('#successfulReport').modal('show');
                        showAlert("Report successfully created!");
                        document.getElementById("pdfloadingAcceptTask").style.display = "none";
                    }
                    else {
                        showError(data.d);
                        document.getElementById("pdfloadingAcceptTask").style.display = "none";
                    }
                },
                error: function (err) {
                    //console.log(err);
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getChecklistItemsNotes(id) {
            document.getElementById("checklistItemsListNotes").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getChecklistNotesData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else if (data.d.length > 0) {
                    document.getElementById("pchecklistItemsListNotes").style.display = "block";
                    for (var i = 0; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("checklistItemsListNotes");
                        var li = document.createElement("li");
                        li.innerHTML = '<i style="margin-left:-15px;" class="fa fa-square-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                        ul.appendChild(li);
                        if (res.length > 1) {
                            if (res[1] != '') {
                                var li2 = document.createElement("li");
                                li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                ul.appendChild(li2);
                            }
                        }
                    }
                }
                else {
                    document.getElementById("pchecklistItemsListNotes").style.display = "none";
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
    function getCanvasNotes(id) {
        document.getElementById("canvasItemsListNotes").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getCanvasNotesData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else if (data.d.length > 0) {
                    document.getElementById("pCanvasNotes").style.display = "block";
                    for (var i = 0; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("canvasItemsListNotes");
                        var li = document.createElement("li");
                        li.innerHTML = '<i style="margin-left:-15px;" ></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                        ul.appendChild(li);
                        if (res.length > 1) {
                            if (res[1] != '') {
                                var li2 = document.createElement("li");
                                li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                ul.appendChild(li2);
                            }
                        }
                    }
                }
                else {
                    document.getElementById("pCanvasNotes").style.display = "none";
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
    function TaskIsCompleted() {
        document.getElementById("taskinitialOptionsDiv").style.display = "none";
        document.getElementById("taskhandleOptionsDiv").style.display = "block";
        document.getElementById("taskrejectOptionsDiv").style.display = "none";
        document.getElementById("acceptedOptionsDiv").style.display = "none";
        document.getElementById("myOptionsDiv").style.display = "none";
        document.getElementById("myOptionsDiv2").style.display = "none";
    }
    function acceptedCompleted() {
        document.getElementById("taskinitialOptionsDiv").style.display = "none";
        document.getElementById("taskhandleOptionsDiv").style.display = "none";
        document.getElementById("acceptedOptionsDiv").style.display = "block";
        document.getElementById("myOptionsDiv").style.display = "none";
        document.getElementById("taskrejectOptionsDiv").style.display = "none";
        document.getElementById("myOptionsDiv2").style.display = "none";
    }

    function rejectionSelect() {

        document.getElementById("rotationDIV1").style.display = "none";
        document.getElementById("rotationDIV2").style.display = "none";


        for (var i = 0; i < divArray.length; i++) {
            var el2 = document.getElementById(divArray[i]);
            el2.className = 'tab-pane fade';
        }

        document.getElementById("addtaskNotesTA").style.display = "none";
        document.getElementById("acceptedOptionsDiv").style.display = "none";
        document.getElementById("taskinitialOptionsDiv").style.display = "none";
        document.getElementById("taskhandleOptionsDiv").style.display = "none";
        document.getElementById("taskrejectOptionsDiv").style.display = "block";
        document.getElementById("myOptionsDiv").style.display = "none";
        document.getElementById("myOptionsDiv2").style.display = "none";

        document.getElementById('rowtasktracebackUser').style.display = "none";

        var el2 = document.getElementById('tasklocation-tab');
        if (el2) {
            el2.className = 'tab-pane fade';
        }
        var el = document.getElementById('rejection-tab');
        if (el) {
            el.className = 'tab-pane fade active in';
        }


    }
    function infotabDefault() {
        var el = document.getElementById('taskactivity-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('taskinfo-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('taskattachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
        var el2 = document.getElementById('taskliInfo');
        if (el2) {
            el2.className = 'active';
        }
        var el5 = document.getElementById('taskliActi');
        if (el5) {
            el5.className = ' ';
        }
        var el6 = document.getElementById('taskliAtta');
        if (el6) {
            el6.className = ' ';
        }
        var el7 = document.getElementById('taskliNotes');
        if (el7) {
            el7.className = ' ';
        }
        var el8 = document.getElementById('notes-tab');
        if (el8) {
            el8.className = 'tab-pane fade';
        }
    }
    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    function updateTaskMarker(obj) {
        var first = true;
        var poligonCoords = [];
        try {
            for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                if (myTraceBackMarkers[i] != null) {
                    myTraceBackMarkers[i].setMap(null);
                }
            }

            if (typeof updatepoligon === 'undefined') {
                // your code here.
            }
            else {
                updatepoligon.setMap(null);
            }
            //if (typeof myMarkersIncidentLocation["Pending"] === 'undefined') {
            //    // your code here.
            //}
            //else {
            //    myMarkersIncidentLocation["Pending"].setMap(null);
            //}
            //if (typeof myMarkersIncidentLocation["Completed"] === 'undefined') {
            //    // your code here.
            //}
            //else {
            //    myMarkersIncidentLocation["Completed"].setMap(null);
            //}
            //if (typeof myMarkersIncidentLocation["InProgress"] === 'undefined') {
            //    // your code here.
            //}
            //else {
            //    myMarkersIncidentLocation["InProgress"].setMap(null);
            //}
            //if (typeof myMarkersIncidentLocation["Accepted"] === 'undefined') {
            //    // your code here.
            //}
            //else {
            //    myMarkersIncidentLocation["Accepted"].setMap(null);
            //}
            //if (typeof myMarkersIncidentLocation["Cancelled"] === 'undefined') {
            //    // your code here.
            //}
            //else {
            //    myMarkersIncidentLocation["Cancelled"].setMap(null);
            //}

            for (var i = 0; i < Object.size(myMarkersIncidentLocation) ; i++) {
                if (myMarkersIncidentLocation[i] != null) {
                    myMarkersIncidentLocation[i].setMap(null);
                }
            }
            var tbcounter = 0;
            var pincounter = 0;
            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div id="content">' + obj[i].Username +
                '<br/></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                mapIncidentLocation.setCenter(myLatlng);
                mapIncidentLocation.setZoom(9);
                if (obj[i].Username == "Pending") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');

                    //myMarkersIncidentLocation[obj[i].Username] = marker;

                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;

                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].Username == "InProgress") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].Username == "OnRoute") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].Username == "Completed") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    if (obj[i].State == "GREEN") {
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                    }
                    else {
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerX.png');
                    }
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].Username == "Accepted") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                    // myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "RED") {
                    if (first) {
                        first = false;
                        document.getElementById('previousTaskUser').text = obj[i].Username;
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        poligonCoords.push(point);

                    }
                    else {
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        poligonCoords.push(point);
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].LastLog });
                        marker.setIcon(obj[i].Marker);
                        myTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                    }
                }
                else {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                    //myMarkersIncidentLocation[obj[i].Username] = marker;
                    myMarkersIncidentLocation[pincounter] = marker;
                    pincounter++;
                    createInfoWindowIncidentLocation(marker, contentString);
                }
            }
            if (poligonCoords.length > 0) {
                updatepoligon = new google.maps.Polyline({
                    path: poligonCoords,
                    geodesic: true,
                    strokeColor: '#1b93c0',
                    strokeOpacity: 1.0,
                    strokeWeight: 7,
                    icons: [{
                        icon: iconsetngs,
                        offset: '100%'
                    }]
                });
                updatepoligon.setMap(mapIncidentLocation);
                animateCircle(updatepoligon);
            }
        }
        catch (err) {
            //showAlert('Error 43: Problem faced during getting markers-' + err);
        }
    }
    function tracebackOn() {
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getTracebackLocationData",
            data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    var obj = jQuery.parseJSON(data.d)
                    if (obj.length > 1) {
                        updateTaskMarker(obj);
                        document.getElementById("rowtasktracebackUser").style.display = "block";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function tracebackOnFilter(duration) {

        if (duration == "1") {
            var el = document.getElementById('taskfilter1');
            if (el) {
                if (el.className == 'fa fa-check-square-o') {
                    el.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el.className = 'fa fa-check-square-o';
            }
            var ell2 = document.getElementById('taskfilter2');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('taskfilter3');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('taskfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "5") {
            var el2 = document.getElementById('taskfilter2');
            if (el2) {
                if (el2.className == 'fa fa-check-square-o') {
                    el2.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el2.className = 'fa fa-check-square-o';
            }
            var ell = document.getElementById('taskfilter1');
            if (ell) {
                ell.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('taskfilter3');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('taskfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "30") {
            var el3 = document.getElementById('taskfilter3');
            if (el3) {
                if (el3.className == 'fa fa-check-square-o') {
                    el3.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el3.className = 'fa fa-check-square-o';
            }
            var ell = document.getElementById('taskfilter1');
            if (ell) {
                ell.className = 'fa fa-square-o';
            }
            var ell2 = document.getElementById('taskfilter2');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('taskfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "60") {
            var el4 = document.getElementById('taskfilter4');
            if (el4) {
                if (el4.className == 'fa fa-check-square-o') {
                    el4.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el4.className = 'fa fa-check-square-o';
            }
            var ell = document.getElementById('taskfilter1');
            if (ell) {
                ell.className = 'fa fa-square-o';
            }
            var ell2 = document.getElementById('taskfilter2');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('taskfilter3');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
        }

        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getTracebackLocationByDurationData",
            data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','duration':'" + duration + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d)
                        updateTaskMarker(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        var firstpressTaskChoice = false;
        function showTaskDocument(name) {
            fromProj = false;
            startRot();
            document.getElementById('addtaskNotesTA').style.display = 'none';
            document.getElementById('taskrejectionTextarea').value = "";

            document.getElementById('commentsId').value = "0";

            document.getElementById('inprogressIODiv').style.display = 'none';
            document.getElementById('completeIODiv').style.display = 'none';
            document.getElementById('inprogressIODiv2').style.display = 'none';
            document.getElementById('completeIODiv2').style.display = 'none';

            document.getElementById("rowtasktracebackUser").style.display = "none";
            document.getElementById('rowChoiceTasks').value = name;
            document.getElementById("taskinitialOptionsDiv").style.display = "block";
            document.getElementById("taskhandleOptionsDiv").style.display = "none";
            document.getElementById("taskrejectOptionsDiv").style.display = "none";
            document.getElementById("myOptionsDiv").style.display = "none";
            document.getElementById("myOptionsDiv2").style.display = "none";
            document.getElementById("acceptedOptionsDiv").style.display = "none";
            document.getElementById("pdfloadingAcceptTask").style.display = "none";



            var el = document.getElementById('tasklocation-tab');
            if (el) {
                el.className = 'tab-pane fade active in';
            }
            var el2 = document.getElementById('rejection-tab');
            if (el2) {
                el2.className = 'tab-pane fade';
            }
            var el3 = document.getElementById('tremarks-tab');
            if (el3) {
                el3.className = 'tab-pane fade';
            }

            oldDivContainers();
            incidentOldDivContainers();
            getChecklistItems(name);
            var retVal = assignrowData(name);
            if (retVal == "Completed" || retVal == "Follow Up")
                TaskIsCompleted();
            else if (retVal == "RejectedSaved")
                rejectionSelect();
            else if (retVal == "Accepted")
                acceptedCompleted();
            else if (retVal == "MyTask" || retVal == "Pending") {
                myTaskVIew();
                document.getElementById('inprogressIODiv').style.display = 'block';
                document.getElementById('completeIODiv').style.display = 'none';
                document.getElementById('inprogressIODiv2').style.display = 'block';
                document.getElementById('completeIODiv2').style.display = 'none';
            }
            else if (retVal == "InProgress") {
                getChecklistItemsEdit(name);
                document.getElementById('inprogressIODiv').style.display = 'none';
                document.getElementById('completeIODiv').style.display = 'block';
                document.getElementById('inprogressIODiv2').style.display = 'none';
                document.getElementById('completeIODiv2').style.display = 'block';
            }
            if ("<%=cuserDisplay%>" == "style='display:none;'") {
            acceptedCompleted();
            if (retVal == "Completed" || retVal == "Follow Up" || retVal == "Accepted") {
                document.getElementById('acceptPDFDIV').style.display = 'block';
            }
            else {
                document.getElementById('acceptPDFDIV').style.display = 'none';
            }
                
        }
        taskHistoryData(name);
        insertAttachmentIcons(name);
        insertAttachmentTabData(name);
        insertAttachmentData(name);
        getChecklistItemsNotes(name);
        getCanvasNotes(name);
        infotabDefault();
        getSubTasklistItems(name);
        gettaskRemarks(name);
        hideAllRemarks();

            hideLoader();
    }
    function updateStatus(status) {
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/UpdateTicketStatus",
            data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','status':'" + status + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert(data.d);
                        //if (status == "START") {
                        //    handledClickTicket();
                        //    document.getElementById("liTicketBack").style.display = "none";
                        //    document.getElementById("liTicketTask").style.display = "block";
                        //    document.getElementById("liTicketStart").style.display = "none";
                        //    document.getElementById("liTicketEnd").style.display = "block";
                        //}
                        //else if (status == "END") {
                        //    handledClickTicket();
                        //    document.getElementById("liTicketBack").style.display = "none";
                        //    document.getElementById("liTicketTask").style.display = "none";
                        //    document.getElementById("liTicketStart").style.display = "none";
                        //    document.getElementById("liTicketEnd").style.display = "none";
                        //}
                        rowchoiceTicket(document.getElementById('rowidChoiceTicket').value);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function rowchoiceTicket(name) {
            startRot();
            document.getElementById('rowidChoiceTicket').value = name;
            document.getElementById('addticketNotesTA').style.display = 'none';
            document.getElementById('backTicketCardLi').style.display = 'block';
            document.getElementById('saveAsTemplateLi').style.display = 'none';

            document.getElementById('backTicketCardClose').style.display = 'block';
            document.getElementById('backTaskClose').style.display = 'none';

            assignrowDataTicket(name);
            getticketRemarks(name);
            getChecklistItemsTicket(name);
            ticketHistoryData(name);
            oldDivContainers();
            ticketinsertAttachmentIcons(name);
            ticketinsertAttachmentTabData(name);
            ticketinsertAttachmentData(name);
            ticketinfotabDefault();
            ticketdispatchAssignMapTab();
            getTasklistItemsTicket(name);
            hideAllTicketRemarks();
        }

        function generateTicketPDF() {
            width = 1;
            moveTick();
            document.getElementById("pdfloadingAcceptTick").style.display = "block";
            document.getElementById("gnNoteTick").innerHTML = "GENERATING REPORT";

            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/CreatePDFTicket",
                data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d != "Failed to generate report!") {
                        //jQuery('#ticketingViewCard').modal('hide');
                        //document.getElementById('successincidentScenario').innerHTML = "Report successfully created!";
                        //jQuery('#successfulDispatch').modal('show');
                        showAlert("Report successfully created!");
                        document.getElementById("pdfloadingAcceptTick").style.display = "none";
                        window.open(data.d);
                    }
                    else {
                        showError(data.d);
                        document.getElementById("pdfloadingAcceptTick").style.display = "none";
                    }
                }
            });
        }

        function getTasklistItemsTicket(id) {
            document.getElementById("tickettaskItemsList").innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getTaskListDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("tickettaskItemsList");
                            var li = document.createElement("li");
                            var colorRet = 'green';
                            if (res[3] == "Pending") {
                                colorRet = 'red';
                            }
                            else if (res[3] == "InProgress") {
                                colorRet = 'yellow';
                            }

                            var action = '';

                            if (res[4] == "true")
                                action = 'href="#taskDocument"  data-toggle="modal" data-dismiss="modal" onclick="showTaskDocument(&apos;' + res[2] + '&apos;);"';

                            li.innerHTML = '<i class="fa fa-circle  ' + colorRet + '-color"></i><a style="margin-left:5px;" class="capitalize-text"  ' + action + '>' + res[0] + '</a>';

                            ul.appendChild(li);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function ticketdispatchAssignMapTab() {
            var el = document.getElementById('video-0-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
            var el3 = document.getElementById('ticketlocation-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            var el4 = document.getElementById('image-1-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }
            var el5 = document.getElementById('image-2-tab');
            if (el5) {
                el5.className = 'tab-pane fade';
            }
            var el6 = document.getElementById('image-3-tab');
            if (el6) {
                el6.className = 'tab-pane fade';
            }
            var el7 = document.getElementById('image-4-tab');
            if (el7) {
                el7.className = 'tab-pane fade';
            }
            var el8 = document.getElementById('image-5-tab');
            if (el8) {
                el8.className = 'tab-pane fade';
            }
            var el9 = document.getElementById('image-6-tab');
            if (el9) {
                el9.className = 'tab-pane fade';
            }
            var el10 = document.getElementById('image-0-tab');
            if (el10) {
                el10.className = 'tab-pane fade';
            }

        }
        function ticketinfotabDefault() {
            var el3 = document.getElementById('ticketinfo-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            var el4 = document.getElementById('ticketattachments-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }
            var el42 = document.getElementById('ticketactivity-tab');
            if (el42) {
                el42.className = 'tab-pane fade';
            }
            var el43 = document.getElementById('ticketnotes-tab');
            if (el43) {
                el43.className = 'tab-pane fade';
            }
            var el44 = document.getElementById('tickettasks-tab');
            if (el44) {
                el44.className = 'tab-pane fade';
            }
            var el2 = document.getElementById('liTicketInfo');
            if (el2) {
                el2.className = 'active';
            }
            var el6 = document.getElementById('liTicketAtta');
            if (el6) {
                el6.className = ' ';
            }
            var el61 = document.getElementById('liTicketAct');
            if (el61) {
                el61.className = ' ';
            }
            var el62 = document.getElementById('liTicketNotes');
            if (el62) {
                el62.className = ' ';
            }
            var el63 = document.getElementById('liTicketTasks');
            if (el63) {
                el63.className = ' ';
            }
        }
        function addnewtask() {
            jQuery('#ticketingViewCard').modal('hide');
            jQuery("#customerslst").val(document.getElementById("ticketcustomerid").value);
            var contractoption = $("#contractslst");
            contractoption.append($('<option></option>').val(0).html('<%=selectContractPlaceholder%>'));


            if (document.getElementById("ticketcustomerid").value > 0) {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getContractListByCustomerId",
                    data: "{'id':'" + document.getElementById("ticketcustomerid").value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#contractslst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                             );
                        }
                        jQuery("#contractslst").val(document.getElementById("ticketcontractid").value);
                    }
                });
            }
            jQuery("#contractslst").val(document.getElementById("ticketcontractid").value);
        }

        function assignrowDataTicket(id) {
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getTableRowDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        // document.getElementById("platenumberSpan").innerHTML = data.d[0];
                        // document.getElementById("platesourceSpan").innerHTML = data.d[1];
                        //  document.getElementById("plateCodeSpan").innerHTML = data.d[2];
                        //  document.getElementById("vehicleMakeSpan").innerHTML = data.d[3];
                        //  document.getElementById("vehicleColorSpan").innerHTML = data.d[4];
                        document.getElementById("ticketusernameSpan").innerHTML = data.d[5];
                        document.getElementById("ticketlocSpan").innerHTML = data.d[6];
                        document.getElementById("tickettimeSpan").innerHTML = data.d[7];
                        document.getElementById("offcatSpan").innerHTML = data.d[8];
                        document.getElementById("offtypeSpan").innerHTML = data.d[9];
                        document.getElementById("liTicketReport").style.display = "none";
                        document.getElementById("liTicketResolve").style.display = "none";
                        document.getElementById("liTicketOpen").style.display = "none";
                        if (data.d[10] == "0") {
                            unhandledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                            document.getElementById("liTicketOpen").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "block";

                        } else if (data.d[10] == "3") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";

                            document.getElementById("liTicketTask").style.display = "block";

                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "block";
                            document.getElementById("liTicketOpen").style.display = "none";

                        } else if (data.d[10] == "4") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                            document.getElementById("liTicketReport").style.display = "block";
                            document.getElementById("liTicketOpen").style.display = "block";
                            if (data.d[16] == "0") {
                                document.getElementById("liTicketResolve").style.display = "none";
                            }
                            else {
                                document.getElementById("liTicketResolve").style.display = "block";
                            }
                            //liTicketResolve
                        }
                        else if (data.d[10] == "5") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                            document.getElementById("liTicketOpen").style.display = "none";
                            document.getElementById("liTicketResolve").style.display = "none";
                            document.getElementById("liTicketReport").style.display = "block";

                            //
                        }
                        else if (data.d[10] == "6") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                            document.getElementById("liTicketOpen").style.display = "none";
                            document.getElementById("liTicketResolve").style.display = "none";
                            document.getElementById("liTicketReport").style.display = "none";
                        }
                        document.getElementById("ticketCommentSpan").innerHTML = data.d[11];
                        document.getElementById('dvticketAccountSpan').style.display = "none";
                        document.getElementById('dvticketContract').style.display = "none";
                        if (data.d[12] != "N/A") {
                            document.getElementById("ticketAccountSpan").innerHTML = data.d[12];
                            document.getElementById('dvticketAccountSpan').style.display = "block";
                        }
                        if (data.d[13] != "N/A") {
                            document.getElementById("ticketContract").innerHTML = data.d[13];
                            document.getElementById('dvticketContract').style.display = "block";
                        }
                        document.getElementById("ticketcontractid").value = data.d[14];
                        document.getElementById("ticketcustomerid").value = data.d[15];
                        document.getElementById("ticketstatusSpan").innerHTML = data.d[17];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function unhandledClickTicket() {
            document.getElementById("ticketinitialOptionsDiv").style.display = "block";
            document.getElementById("tickethandleOptionsDiv").style.display = "none";
        }
        function handledClickTicket() {
            document.getElementById("ticketinitialOptionsDiv").style.display = "none";
            document.getElementById("tickethandleOptionsDiv").style.display = "block";
        }
        function saveTicketingRemarks() {
            var projn = document.getElementById("addticketNotesTA").value;
            var id = document.getElementById('rowidChoiceTicket').value;
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('addticketNotesTA').value)) {
                isPass = false;
                showAlert("Kindly provide notes to be added");
            }
            else {
                if (isSpecialChar(document.getElementById('addticketNotesTA').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character from notes");
                }
            }
            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/addNewTicketingRemarks",
                    data: "{'id':'" + id + "','notes':'" + projn + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            showAlert("Successfully added notes");
                            document.getElementById("addticketNotesTA").value = "";
                            getticketRemarks(id);
                            document.getElementById('addticketNotesTA').style.display = 'none';
                        }
                        else if (data.d == "LOGOUT") {
                            document.getElementById('<%= logoutbtn.ClientID %>').click();
                        }
                        else {
                            showAlert('Failed to save notes. ' + data.d);
                        }
                    }
                });
        }
    }
    function addNotesToTicket() {
        if (document.getElementById('addticketNotesTA').style.display != 'block') {
            document.getElementById('addticketNotesTA').style.display = 'block';
        }
        else {
            saveTicketingRemarks();
        }
    }

    function showAllTicketingRemarks(id) {

        var el2 = document.getElementById('ticketlocation-tab');
        if (el2) {
            el2.className = 'tab-pane fade';
        }
        var el = document.getElementById('ticketremarks-tab');
        if (el) {
            el.className = 'tab-pane fade active in';
        }

        for (var i = 0; i < divArray.length; i++) {
            var el2 = document.getElementById(divArray[i]);
            el2.className = 'tab-pane fade';
        }

        document.getElementById("ticketrotationDIV1").style.display = "none";
        document.getElementById("ticketrotationDIV2").style.display = "none";

        jQuery('#ticketRemarksList2 div').html('');
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTicketRemarksData2",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                for (var i = 0; i < data.d.length; i++) {
                    var div = document.createElement('div');

                    div.className = 'row activity-block-container';

                    div.innerHTML = data.d[i];

                    document.getElementById('ticketRemarksList2').appendChild(div);
                }
            }
        });

    }
    function hideAllTicketRemarks() {

        document.getElementById("ticketrotationDIV1").style.display = "block";
        document.getElementById("ticketrotationDIV2").style.display = "block";

        var el2 = document.getElementById('ticketlocation-tab');
        if (el2) {
            el2.className = 'tab-pane fade active in';
        }
        var el = document.getElementById('ticketremarks-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }

        for (var i = 0; i < divArray.length; i++) {
            var el2 = document.getElementById(divArray[i]);
            el2.className = 'tab-pane fade';
        }
    }
    function getticketRemarks(id) {
        jQuery('#ticketRemarksList div').html('');
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getTicketRemarksData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                for (var i = 0; i < data.d.length; i++) {
                    var div = document.createElement('div');

                    div.className = 'row activity-block-container';

                    div.innerHTML = data.d[i];

                    document.getElementById('ticketRemarksList').appendChild(div);
                }
            }
        });
    }
    function ticketHistoryData(id) {
        jQuery('#divTicketActivity div').html('');
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/geTicketHistoryData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('divTicketActivity').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function rowchoice2(name) {
            fromProj = true;
            startRot();

            document.getElementById('addtaskNotesTA').style.display = 'none';
            document.getElementById('taskrejectionTextarea').value = "";

            document.getElementById('inprogressIODiv').style.display = 'none';
            document.getElementById('completeIODiv').style.display = 'none';
            document.getElementById('inprogressIODiv2').style.display = 'none';
            document.getElementById('completeIODiv2').style.display = 'none';

            document.getElementById("rowtasktracebackUser").style.display = "none";
            document.getElementById('rowChoiceTasks').value = name;
            document.getElementById("taskinitialOptionsDiv").style.display = "block";
            document.getElementById("taskhandleOptionsDiv").style.display = "none";
            document.getElementById("taskrejectOptionsDiv").style.display = "none";
            document.getElementById("myOptionsDiv").style.display = "none";
            document.getElementById("myOptionsDiv2").style.display = "none";
            document.getElementById("acceptedOptionsDiv").style.display = "none";
            document.getElementById("pdfloadingAccept").style.display = "none";

            var el = document.getElementById('tasklocation-tab');
            if (el) {
                el.className = 'tab-pane fade active in';
            }
            var el2 = document.getElementById('rejection-tab');
            if (el2) {
                el2.className = 'tab-pane fade';
            }
            oldDivContainers();
            incidentOldDivContainers();
            var retVal = assignrowData(name);
            if (retVal == "Completed" || retVal == "Follow Up")
                TaskIsCompleted();
            else if (retVal == "RejectedSaved")
                rejectionSelect();
            else if (retVal == "Accepted")
                acceptedCompleted();
            else if (retVal == "MyTask" || retVal == "Pending") {
                myTaskVIew();
                document.getElementById('inprogressIODiv').style.display = 'block';
                document.getElementById('completeIODiv').style.display = 'none';
                document.getElementById('inprogressIODiv2').style.display = 'block';
                document.getElementById('completeIODiv2').style.display = 'none';
            }
            else if (retVal == "InProgress") {
                getChecklistItemsEdit(name);
                document.getElementById('inprogressIODiv').style.display = 'none';
                document.getElementById('completeIODiv').style.display = 'block';
                document.getElementById('inprogressIODiv2').style.display = 'none';
                document.getElementById('completeIODiv2').style.display = 'block';
            }

            if ("<%=cuserDisplay%>" == "style='display:none;'") {
            acceptedCompleted();
            if (retVal == "Completed" || retVal == "Follow Up" || retVal == "Accepted") {
                document.getElementById('acceptPDFDIV').style.display = 'block';
            }
            else {
                document.getElementById('acceptPDFDIV').style.display = 'none';
            }

        }

        taskHistoryData(name);
        insertAttachmentIcons(name);
        insertAttachmentTabData(name);
        insertAttachmentData(name);
        getChecklistItems(name);
        getChecklistItemsNotes(name);
        getCanvasNotes(name);
        infotabDefault();
        getSubTasklistItems(name);
        gettaskRemarks(name);
    }

    function myTaskVIew() {
        document.getElementById("taskinitialOptionsDiv").style.display = "none";
        document.getElementById("taskhandleOptionsDiv").style.display = "none";
        document.getElementById("taskrejectOptionsDiv").style.display = "none";
        document.getElementById("acceptedOptionsDiv").style.display = "none";
        document.getElementById("myOptionsDiv").style.display = "block";
        document.getElementById("myOptionsDiv2").style.display = "block";
    }
    function rowchoiceTemplate(name) {
        document.getElementById('rowChoiceTasks').value = name;
        assignrowDataTemplate(name);
    }

    function rowchoiceChecklist(name) {

        document.getElementById("btnUpdateChecklistAdd").style.display = "none";

        document.getElementById('tbChecklistItemTypeSub').value = "";
        document.getElementById('tbChecklistItemTypeSub5').value = "";
        document.getElementById('tbEditChecklistDescription').value = "";
        document.getElementById('tbEditChecklistQuantity').value = "";
        document.getElementById('tbEditChecklistExitQuantity').value = "";

        document.getElementById('rowChoiceChecklist').value = name;
        getChecklistRowData(name);
        document.getElementById("btnEditChecklistAdd").style.display = "block";
        document.getElementById("btneditChecklistType4").style.display = "block";
        document.getElementById("tbChecklistItemTypeSub").style.display = "block";

        document.getElementById("tbEditChecklistDescription").style.display = "block";
        document.getElementById("tbEditChecklistQuantity").style.display = "block";
        document.getElementById("tbEditChecklistExitQuantity").style.display = "block";
        document.getElementById("tbEditChecklistMain").style.display = "block";
        document.getElementById("tbEditChecklistSub").style.display = "block";

        document.getElementById('btnEditChecklistAdd').style.display = "block";
        document.getElementById('btnUpdateChecklistAdd').style.display = "none";
        document.getElementById('btnUpdateChecklistAdd45').style.display = "none";

        document.getElementById('btneditChecklistType5').style.display = "block";
        document.getElementById('btnupdateChecklistType5').style.display = "none";


        document.getElementById('btneditChecklistType4').style.display = "block";
        document.getElementById('btnupdateChecklistType4').style.display = "none";

        document.getElementById('tbEditChecklistDescription').value = "";
        document.getElementById('tbEditChecklistQuantity').value = "";
        document.getElementById('tbEditChecklistExitQuantity').value = "";
        document.getElementById('tbChecklistItemTypeSub').value = "";
        document.getElementById('tbChecklistItemTypeSub5').value = "";

        document.getElementById('tbEditChecklistMain').value = "";
        document.getElementById('tbEditChecklistSub').disabled = "";
    }
    function viewrowchoiceChecklist(name) {


        document.getElementById("btnupdateChecklistType5").style.display = "none";
        document.getElementById("btnUpdateChecklistAdd45").style.display = "none";
        document.getElementById("btnUpdateChecklistAdd").style.display = "none";
        document.getElementById('rowChoiceChecklist').value = name;
        getChecklistRowData(name);
        document.getElementById("btnEditChecklistAdd").style.display = "none";
        document.getElementById("btneditChecklistType4").style.display = "none";
        document.getElementById("tbChecklistItemTypeSub").style.display = "none";
        document.getElementById("tbEditChecklistDescription").style.display = "none";
        document.getElementById("tbEditChecklistQuantity").style.display = "none";
        document.getElementById("tbEditChecklistExitQuantity").style.display = "none";
        document.getElementById("tbEditChecklistMain").style.display = "none";
        document.getElementById("tbEditChecklistSub").style.display = "none";


    }
    function taskHistoryData(id) {
        jQuery('#taskdivIncidentHistoryActivity div').html('');
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getEventHistoryData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('taskdivIncidentHistoryActivity').appendChild(div);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function assignrowDataTemplate(id) {
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableRowDataTemplate",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById('tbNewLongitude').value = data.d[0];
                    document.getElementById('tbNewLatitude').value = data.d[1];
                    document.getElementById('MainContent_NewTaskControl_checklistSelect').value = data.d[2];
                    document.getElementById('tbNewTaskName').value = data.d[3];
                    document.getElementById('tbNewDescription').value = data.d[4];
                    document.getElementById('prioritySelect').value = data.d[5];
                    document.getElementById('newtaskHeader').innerHTML = "TASKS TEMPLATE";
                    document.getElementById("saveAsTemplateLi").style.display = "none";
                    document.getElementById('MainContent_NewTaskControl_taskTypeSelect').value = data.d[10];



                    //Ts
                    $("#projectlst").val(data.d[7]);

                    $('#customerslst option').remove();
                    $.ajax({
                        type: "POST",
                        url: "Tasks.aspx/getCustomerByProjectId",
                        data: "{'id':'" + data.d[7] + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#customerslst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                                 );
                            }
                        }
                    });
                    $("#customerslst").val(data.d[8]);
                    document.getElementById('tbCustomerId').value = data.d[8];
                    $('#contractslst option').remove();
                    var contractoption = $("#contractslst");
                    contractoption.append($('<option></option>').val(0).html('<%=selectContractPlaceholder%>'));
                    $.ajax({
                        type: "POST",
                        url: "Tasks.aspx/getContractListByCustomerId",
                        data: "{'id':'" + data.d[8] + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#contractslst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                                 );
                            }
                        }
                    });
                    $("#contractslst").val(data.d[9]);

                    if (data.d[11] == "true") {
                        document.getElementById("requiresignatureCheck").checked = true;
                    }

                    cleardispatchList();
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function newTaskClick() {
        document.getElementById('newtaskHeader').innerHTML = "CREATE NEW TASK";
        document.getElementById('tbNewLongitude').value = "";
        document.getElementById('tbNewLatitude').value = "";
        document.getElementById('tbNewTaskName').value = "";
        document.getElementById('tbNewDescription').value = "";
        cleardispatchList();
        document.getElementById("saveAsTemplateLi").style.display = "block";
        document.getElementById('projectlst').disabled = "";
        document.getElementById('customerslst').disabled = "";
        document.getElementById("requiresignatureCheck").checked = false;
    }
    function ticketinsertAttachmentIcons(id) {
        jQuery('#ticketdivAttachment div').html('');
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentDataIconsTicket",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("ticketdivAttachment").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function ticketinsertAttachmentTabData(id) {
        jQuery('#ticketattachments-info-tab div').html('');

        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentDataTabTicket",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("ticketattachments-info-tab").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function ticketinsertAttachmentData(id) {
        document.getElementById('ticketrotationDIV1').style.display = "none";
        document.getElementById('ticketrotationDIV2').style.display = "none";

        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentDataTicket",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    imgcount = 0;
                    //var div = document.createElement('div');
                    //div.className = 'tab-pane fade';
                    //div.innerHTML = data.d[0];
                    //div.id = 'video-0-tab';
                    //document.getElementById('ticketdivAttachmentHolder').appendChild(div);
                    //divArray[0] = 'video-0-tab';
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');
                        div.className = 'tab-pane fade';
                        div.align = 'center';
                        div.style.height = '380px';
                        div.innerHTML = data.d[i];
                        div.id = 'image-' + (i) + '-tab';
                        document.getElementById('ticketdivAttachmentHolder').appendChild(div);
                        divArray[i] = 'image-' + (i) + '-tab';
                        imgcount++;
                    }

                    if (imgcount > 0) {
                        document.getElementById('ticketrotationDIV1').style.display = "block";
                        document.getElementById('ticketrotationDIV2').style.display = "block";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function insertAttachmentIcons(id) {
        jQuery('#taskdivAttachment div').html('');
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentDataIcons",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    //for (var i = 0; i < data.d.length; i++) {
                    document.getElementById("taskdivAttachment").innerHTML = data.d;
                    //}
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function insertAttachmentTabData(id) {
        jQuery('#taskattachments-info-tab div').html('');

        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentDataTab",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskattachments-info-tab").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function insertAttachmentData(id) {
        document.getElementById("rotationDIV1").style.display = "none";
        document.getElementById("rotationDIV2").style.display = "none";
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getAttachmentData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    imgcount = 0;
                    document.getElementById('taskAudioDIV').style.display = "none";
                    for (var i = 0; i < data.d.length; i++) {
                        if (data.d[i].indexOf("video") >= 0) {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.innerHTML = data.d[i];
                            div.id = 'video-' + (i + 1) + '-tab';
                            document.getElementById('taskdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'video-' + (i + 1) + '-tab';
                            imgcount++;
                        }
                        else {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.align = 'center';
                            div.style.height = '380px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i + 1) + '-tab';
                            document.getElementById('taskdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'image-' + (i + 1) + '-tab';
                            imgcount++;
                        }
                    }
                    if (imgcount > 0) {
                        document.getElementById("rotationDIV1").style.display = "block";
                        document.getElementById("rotationDIV2").style.display = "block";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function audioincidentplay(sr) {
        document.getElementById('incidentAudioDIV').style.display = "block";
        document.getElementById('incidentAudioSrc').src = sr;
        document.getElementById('incidentAudio').load(); //call this to just preload the audio without playing

    }
    function hideIncidentplay() {
        document.getElementById('incidentAudioDIV').style.display = "none";
    }
    function audiotaskplay(sr) {
        document.getElementById('taskAudioDIV').style.display = "block";
        document.getElementById('taskAudioSrc').src = sr;
        document.getElementById('taskAudio').load(); //call this to just preload the audio without playing

    }
    function hideTaskplay() {
        document.getElementById('taskAudioDIV').style.display = "none";
    }
    function oldDivContainers() {
        try {
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                el.parentNode.removeChild(el);
            }
            divArray = new Array();
        }
        catch (ex) {
            //alert(ex);
        }
    }

    function assignrowData(id) {
        var output = "";
        document.getElementById('taskrejectionTextarea').innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableRowData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskusernameSpan").innerHTML = data.d[1];
                    document.getElementById("tasktimeSpan").innerHTML = data.d[2];
                    document.getElementById("tasktypeSpan").innerHTML = data.d[3];
                    document.getElementById("taskstatusSpan").innerHTML = data.d[4];
                    document.getElementById("tasklocSpan").innerHTML = data.d[5];
                    document.getElementById("taskdescriptionSpan").innerHTML = data.d[9];
                    document.getElementById("taskinstructionSpan").innerHTML = data.d[10];
                    document.getElementById("taskincidentNameHeader").innerHTML = data.d[11];
                    document.getElementById("assignedTimeSpan").innerHTML = data.d[12];
                    document.getElementById("checklistNotesSpan").innerHTML = data.d[13];
                    document.getElementById("checklistnameSpan").innerHTML = data.d[15];

                    var el = document.getElementById('headerImageClass');
                    if (el) {
                        el.className = data.d[14];
                    }
                    output = data.d[0];
                    document.getElementById('pRejectionNotes').style.display = "none";
                    document.getElementById('acceptLiHandle').style.display = data.d[16];

                    if (data.d[17] != "N-A") {
                        document.getElementById('pRejectionNotes').style.display = "block";
                        document.getElementById('rejectionListNotes').innerHTML = data.d[17];

                    }
                    document.getElementById('ttypeSpan').innerHTML = data.d[18];

                    document.getElementById("incidentItemsList").innerHTML = "";
                    document.getElementById('incidentItemsListDIV').style.display = "none";

                    var res = data.d[19].split("|");
                    if (res.length > 0) {
                        if (res[0] != "None") {
                            if (res[0] == "Incident")
                                document.getElementById("incidentItemsList").innerHTML = res[0] + ': <a style="color:#b2163b;" href="#viewDocument1"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="showIncidentDocument(&apos;' + res[2] + '&apos;)">' + res[1] + '</a>';
                            else
                                document.getElementById("incidentItemsList").innerHTML = res[0] + ': <a style="color:#b2163b;" href="#ticketingViewCard"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="rowchoiceTicket(&apos;' + res[2] + '&apos;)">' + res[1] + '</a>';
                            document.getElementById('incidentItemsListDIV').style.display = "block";
                        }
                    }

                    var res2 = data.d[20].split("|");
                    document.getElementById("linkparent").innerHTML = "";
                    document.getElementById('linkparentDIV').style.display = "none";
                    document.getElementById('subLinkDisplay2').style.display = 'block';
                    document.getElementById('rejectDisplay1').style.display = 'block';

                    if (res2.length > 0) {
                        if (res2[0] != "None") {
                            document.getElementById("linkparent").innerHTML = 'Main-Task: <a style="color:#b2163b;" href="#"  href="#taskDocument" data-toggle="modal" data-dismiss="modal" class="capitalize-text" onclick="showTaskDocument(&apos;' + res2[1] + '&apos;);linkChoiceView(&apos;' + id + '&apos;);">' + res2[0] + '</a>';
                            document.getElementById('linkparentDIV').style.display = "block";
                            document.getElementById('subLinkDisplay2').style.display = 'none';
                        }
                    }
                    document.getElementById("CustomerNameSpan").innerHTML = "";
                    document.getElementById("SystemTypeSpan").innerHTML = "";
                    document.getElementById("ProjectNameSpan").innerHTML = "";
                    document.getElementById("ContractNameSpan").innerHTML = "";

                    document.getElementById('dvCustomerNameSpan').style.display = "none";
                    document.getElementById('dvSystemTypeSpan').style.display = "none";
                    document.getElementById('dvProjectNameSpan').style.display = "none";
                    document.getElementById('dvContractNameSpan').style.display = "none";

                    //NEWCUSTOMER
                    if (data.d.length > 20) {
                        if (data.d[21] != "N/A") {
                            document.getElementById("CustomerNameSpan").innerHTML = data.d[21];
                            document.getElementById('dvCustomerNameSpan').style.display = "block";
                        }
                        if (data.d[22] != "N/A") {
                            document.getElementById("SystemTypeSpan").innerHTML = data.d[22];
                            document.getElementById('dvSystemTypeSpan').style.display = "block";
                        }
                        if (data.d[23] != "N/A") {
                            document.getElementById("ProjectNameSpan").innerHTML = data.d[23];
                            document.getElementById('dvProjectNameSpan').style.display = "block";
                        }
                        if (data.d[24] != "N/A") {
                            document.getElementById("ContractNameSpan").innerHTML = data.d[24];
                            document.getElementById('dvContractNameSpan').style.display = "block";
                        }
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
    function initializetables() {
        addrowtoTableMyTasks();
        //addrowtoTableTeamTasks();
        //addrowtoTableCompTeamTasks();
        //addrowtoTableType1();
        //addrowtoTableType2();
        //addrowtoTableType3();
        //addrowtoTableType4();
        //addrowtoTableType5();
        //addrowtoTableTemplates();
        //addrowtoTableRecurrintTasks();
    }

    function addrowtoTableMyTasks() {
        jQuery("#mytasksTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataMyTasks",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                taskfilter = true;
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.MyTasks.length; i++) {
                        $("#mytasksTable tbody").append(data.d.MyTasks[i]);
                    }
                    //for (var i = 0; i < data.d.TeamTasks.length; i++) {
                    $("#teamtasksTable tbody").append(data.d.TeamTasks);
                    // }
                    for (var i = 0; i < data.d.CompTeamTasks.length; i++) {
                        $("#completedteamtasksTable tbody").append(data.d.CompTeamTasks[i]);
                    }

                    for (var i = 0; i < data.d.Type5.length; i++) {
                        $("#type5Table tbody").append(data.d.Type5[i]);
                    }
                    for (var i = 0; i < data.d.Templates.length; i++) {
                        $("#templateTable tbody").append(data.d.Templates[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableTeamTasks() {
        jQuery("#teamtasksTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataTeamTasks",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                taskfilter = true;
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#teamtasksTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableCompTeamTasks() {
        jQuery("#completedteamtasksTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataCompTeamTasks",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#completedteamtasksTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }


    function addrowtoTableRecurrintTasks() {
        jQuery("#recurringTasksTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataRecurring",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#recurringTasksTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableType1() {
        jQuery("#type1Table tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataType1",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#type1Table tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableType2() {
        jQuery("#type2Table tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataType2",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#type2Table tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableType3() {
        jQuery("#type3Table tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataType3",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#type3Table tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableType4() {
        jQuery("#type4Table tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataType4",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#type4Table tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableType5() {
        jQuery("#type5Table tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataType5",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#type5Table tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableTemplates() {
        jQuery("#templateTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataTemplates",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#templateTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function useLocation(lat, long) {
        document.getElementById("tbNewLatitude").value = lat;
        document.getElementById("tbNewLongitude").value = long;
        document.getElementById("pac-input").value = ""; 
    }
    function useLocationTemplate(lat, long) {
        document.getElementById("tbTemplateLatitude").value = lat;
        document.getElementById("tbTemplateLongitude").value = long;

    }
    function alldeletemarker() {
        try {
            for (var i = 0; i < markers2.length; i++) {
                markers2[i].setMap(null);
                markers2[i] = null;
            }
            markers2 = [];
        }
        catch (err) {
            alert('Error 54: Javascript error occured while trying to delete markers-' + err);
        }
    }
    var markers2 = [];
    var uniqueId = 1;
    function getLocationMarkers(obj) {

        try {
            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 8,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapLocation = new google.maps.Map(document.getElementById("map_canvasLocation"), mapOptions);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            mapLocation.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            mapLocation.addListener('bounds_changed', function () {
                searchBox.setBounds(mapLocation.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                 

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {

                    alldeletemarker(); 

                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    var marker2 = new google.maps.Marker({
                        position: place.geometry.location,
                        title: place.name,
                        map: mapLocation
                    });
                    marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                    //Attach click event handler to the marker.
                    google.maps.event.addListener(marker2, "click", function (e) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + place.geometry.location.lat() + '&apos;,&apos;' + place.geometry.location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                        });
                        infoWindow.open(mapLocation, marker2);
                    });

                    // Create a marker for each place.
                    //markers.push(new google.maps.Marker({
                    //    map: mapLocation,
                    //    icon: icon,
                    //    title: place.name,
                    //    position: place.geometry.location
                    //}));

                    markers2.push(marker2);

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                     
                });
                mapLocation.fitBounds(bounds);
            });


            google.maps.event.addListener(mapLocation, 'click', function (event) {
                alldeletemarker();
                var location = event.latLng;
                //Create a marker and placed it on the map.
                var marker2 = new google.maps.Marker({
                    position: location,
                    map: mapLocation
                });
                marker2.id = uniqueId;
                marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                uniqueId++;
                markers2.push(marker2);
                //Attach click event handler to the marker.
                google.maps.event.addListener(marker2, "click", function (e) {
                    var infoWindow = new google.maps.InfoWindow({
                        content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                    });
                    infoWindow.open(mapLocation, marker2);
                });
            });
            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="locationChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png')
                myMarkersLocation[obj[i].Username] = marker;
                createInfoWindowLocation(marker, contentString);
            }
        }
        catch (err) {
            alert(err)
        }
    }
    function getLocationNoMarkers() {

        try {
            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 8,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapLocation = new google.maps.Map(document.getElementById("map_canvasLocation"), mapOptions);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            mapLocation.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            mapLocation.addListener('bounds_changed', function () {
                searchBox.setBounds(mapLocation.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                 
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {

                    alldeletemarker(); 

                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    var marker2 = new google.maps.Marker({
                        position: place.geometry.location,
                        title: place.name,
                        map: mapLocation
                    });
                    marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                    //Attach click event handler to the marker.
                    google.maps.event.addListener(marker2, "click", function (e) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + place.geometry.location.lat() + '&apos;,&apos;' + place.geometry.location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                        });
                        infoWindow.open(mapLocation, marker2);
                    });
                      
                    markers2.push(marker2);

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                     
                });
                mapLocation.fitBounds(bounds);
            });


            google.maps.event.addListener(mapLocation, 'click', function (event) {
                alldeletemarker();
                var location = event.latLng;
                //Create a marker and placed it on the map.
                var marker2 = new google.maps.Marker({
                    position: location,
                    map: mapLocation
                });
                marker2.id = uniqueId;
                marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                uniqueId++;
                markers2.push(marker2);
                //Attach click event handler to the marker.
                google.maps.event.addListener(marker2, "click", function (e) {
                    var infoWindow = new google.maps.InfoWindow({
                        content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                    });
                    infoWindow.open(mapLocation, marker2);
                });
            });
        }
        catch (err) {
        }
    }
    function createInfoWindowLocation(marker, popupContent) {
        google.maps.event.addListener(marker, 'click', function () {
            infoWindowLocation.setContent(popupContent);
            infoWindowLocation.open(mapLocation, this);
        });
    }
    function locationChoice(id, name) {
        var element = document.getElementById('MainContent_NewTaskControl_locationSelect');
        element.value = id;
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getLocationById",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById('tbNewLongitude').value = data.d[0];
                    document.getElementById('tbNewLatitude').value = data.d[1];
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function locationTemplateChoice(id, name) {
        var element = document.getElementById('MainContent_locationSelectTemplate');
        element.value = id;
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getLocationById",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById('tbTemplateLongitude').value = data.d[0];
                    document.getElementById('tbTemplateLatitude').value = data.d[1];
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function locationTemplateOnChange(e) {
        //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getLocationById",
            data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById('tbTemplateLongitude').value = data.d[0];
                    document.getElementById('tbTemplateLatitude').value = data.d[1];
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function locationOnChange(e) {
        //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
        if (e.options[e.selectedIndex].value > 0) {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getLocationById",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('tbNewLongitude').value = data.d[0];
                        document.getElementById('tbNewLatitude').value = data.d[1];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        else {
            document.getElementById('tbNewLongitude').value = "";
            document.getElementById('tbNewLatitude').value = "";
        }
    }
    function pickdatecompTeamTask() {
        //showAlert(document.getElementById('teamtaskDatepicker').value);

        $("#completedteamtasksTable tbody").empty();
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableDataCompTeamTasksByDate",
            data: "{'date':'" + document.getElementById('compteamtaskDatepicker').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        $("#completedteamtasksTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    var taskfilter = false;

    function pickdateProjectTask() {
        jQuery("#projectsTaskTable tbody").empty();
        jQuery("#projectsTaskTable").dataTable().fnClearTable();
        jQuery("#projectsTaskTable").dataTable().fnDraw();
        jQuery("#projectsTaskTable").dataTable().fnDestroy();
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/projViewTableByDate",
            data: "{'date':'" + document.getElementById('projecttaskDatepicker').value + "','id':'" + document.getElementById("rowChoiceProject").value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            $("#projectsTaskTable tbody").append(data.d[i]);
                        }
                        jQuery("#projectsTaskTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 5,
                            "order": [[0, "desc"]]
                        });
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function pickdateTeamTask() {
            //showAlert(document.getElementById('teamtaskDatepicker').value);
            if (taskfilter) {

                jQuery("#teamtasksTable tbody").empty();
                jQuery("#teamtasksTable").dataTable().fnClearTable();
                jQuery("#teamtasksTable").dataTable().fnDraw();
                jQuery("#teamtasksTable").dataTable().fnDestroy();
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getTableDataTeamTasksByDate",
                    data: "{'date':'" + document.getElementById('teamtaskDatepicker').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            $("#teamtasksTable tbody").append(data.d[i]);
                        }
                        jQuery("#teamtasksTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 10,
                            "order": [[4, "desc"]]
                        });
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    function addToChecklistItemType4() {
        var id = document.getElementById('editChoiceChecklist').value;
        var desc = document.getElementById('tbChecklistItemTypeSub').value;
        var parentid = document.getElementById('t4SubChecklistID').value;
        var isPass = true;
        if (isEmptyOrSpaces(desc)) {
            isPass = false;
            showAlert("Kindly provide checklist description");
        }
        else if (isSpecialChar(desc)) {
            isPass = false;
        }
        if (isPass) {
            type4AddChecklistItem(id, "", desc, parentid);
            rowchoicetype4sub(parentid, document.getElementById('tbChecklistItemType4').value);
            document.getElementById('tbChecklistItemTypeSub').value = "";
        }
    }
    function addToChecklistItemType5() {
        var id = document.getElementById('editChoiceChecklist').value;
        var desc = document.getElementById('tbChecklistItemTypeSub5').value;
        var parentid = document.getElementById('t5SubChecklistID').value;
        var isPass = true;
        if (isEmptyOrSpaces(desc)) {
            isPass = false;
            showAlert("Kindly provide checklist description");
        }
        else if (isSpecialChar(desc)) {
            isPass = false;
        }
        if (isPass) {
            type5AddChecklistItem(id, "", desc, parentid);
            rowchoicetype5sub(parentid, document.getElementById('tbChecklistItemType5').value);
            document.getElementById('tbChecklistItemTypeSub5').value = "";
        }
    }
    function updateChecklistItem() {
        var isPass = true;
        var ttype = document.getElementById('rowChoiceChecklistItemType').value;
        var desc = document.getElementById('tbEditChecklistDescription').value;
        var qt = document.getElementById('tbEditChecklistQuantity').value;
        var qt2 = document.getElementById('tbEditChecklistExitQuantity').value;
        var main = document.getElementById('tbEditChecklistMain').value;
        var sub = document.getElementById('tbEditChecklistSub').value;
        if (ttype == "4")
            desc = document.getElementById('tbChecklistItemTypeSub').value;
        else if (ttype == "5")
            desc = document.getElementById('tbChecklistItemTypeSub5').value;
        if (ttype != "4" && ttype != "5") {
            if (isEmptyOrSpaces(desc)) {
                isPass = false;
                showAlert("Kindly provide checklist description");
            }
            else if (isSpecialChar(desc)) {
                isPass = false;
            }
        }
        if (isPass) {
            if (ttype == "1") {
                if (isEmptyOrSpaces(qt)) {
                    isPass = false;
                    showAlert("Kindly provide checklist quantity");
                }
                else if (isSpecialChar(qt)) {
                    isPass = false;
                }
                if (isPass) {
                    if (isNumeric(qt))
                    { }
                    else {
                        isPass = false;
                        showAlert("Kindly provide numeric quantity");
                    }
                }
            }
            else if (ttype == "2") {
                if (isEmptyOrSpaces(qt)) {
                    isPass = false;
                    showAlert("Kindly provide checklist quantity");
                }
                else if (isSpecialChar(qt)) {
                    isPass = false;
                }
                if (isEmptyOrSpaces(qt2)) {
                    isPass = false;
                    showAlert("Kindly provide checklist quantity");
                }
                else if (isSpecialChar(qt2)) {
                    isPass = false;
                }
                if (isPass) {
                    if (isNumeric(qt) && isNumeric(qt2))
                    { }
                    else {
                        isPass = false;
                        showAlert("Kindly provide numeric quantity");
                    }
                }
            }

            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/updateChecklistItem",
                    data: "{'id':'" + document.getElementById('rowChoiceChecklistItem').value + "','desc':'" + desc + "','qt':'" + document.getElementById('tbEditChecklistQuantity').value + "','qt2':'" + document.getElementById('tbEditChecklistExitQuantity').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else if (data.d == "SUCCESS") {

                            document.getElementById('btnEditChecklistAdd').style.display = "block";
                            document.getElementById('btnUpdateChecklistAdd').style.display = "none";
                            document.getElementById('btnUpdateChecklistAdd45').style.display = "none";

                            document.getElementById('btneditChecklistType5').style.display = "block";
                            document.getElementById('btnupdateChecklistType5').style.display = "none";


                            document.getElementById('btneditChecklistType4').style.display = "block";
                            document.getElementById('btnupdateChecklistType4').style.display = "none";

                            document.getElementById('tbEditChecklistDescription').value = "";
                            document.getElementById('tbEditChecklistQuantity').value = "";
                            document.getElementById('tbEditChecklistExitQuantity').value = "";
                            document.getElementById('tbChecklistItemTypeSub').value = "";
                            document.getElementById('tbChecklistItemTypeSub5').value = "";


                            if (ttype == "4")
                                rowchoicetype4sub(document.getElementById('t4SubChecklistID').value, document.getElementById('tbChecklistItemType4').value);
                            else if (ttype == "5")
                                rowchoicetype5sub(document.getElementById('t5SubChecklistID').value, document.getElementById('tbChecklistItemType5').value);
                            else
                                getChecklistEditTable(ttype, document.getElementById('editChoiceChecklist').value);

                            showAlert("Successful Edit");
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
    }
    function updateChecklistItem45() {
        var isPass = true;
        var ttype = document.getElementById('rowChoiceChecklistItemType').value;
        var desc = document.getElementById('tbEditChecklistMain').value;


        if (isEmptyOrSpaces(desc)) {
            isPass = false;
            showAlert("Kindly provide checklist description");
        }
        else if (isSpecialChar(desc)) {
            isPass = false;
        }

        if (isPass) {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/updateChecklistItem",
                data: "{'id':'" + document.getElementById('rowChoiceChecklistItem').value + "','desc':'" + desc + "','qt':'" + document.getElementById('tbEditChecklistQuantity').value + "','qt2':'" + document.getElementById('tbEditChecklistExitQuantity').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "SUCCESS") {

                        document.getElementById('btnEditChecklistAdd').style.display = "block";
                        document.getElementById('btnUpdateChecklistAdd').style.display = "none";
                        document.getElementById('btnUpdateChecklistAdd45').style.display = "none";

                        document.getElementById('btneditChecklistType5').style.display = "block";
                        document.getElementById('btnupdateChecklistType5').style.display = "none";


                        document.getElementById('btneditChecklistType4').style.display = "block";
                        document.getElementById('btnupdateChecklistType4').style.display = "none";

                        document.getElementById('tbEditChecklistDescription').value = "";
                        document.getElementById('tbEditChecklistQuantity').value = "";
                        document.getElementById('tbEditChecklistExitQuantity').value = "";
                        document.getElementById('tbChecklistItemTypeSub').value = "";
                        document.getElementById('tbChecklistItemTypeSub5').value = "";

                        document.getElementById('tbEditChecklistMain').value = "";
                        document.getElementById('tbEditChecklistSub').disabled = "";

                        getChecklistEditTable(ttype, document.getElementById('editChoiceChecklist').value);

                        showAlert("Successful Edit");
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    function editChecklistItemChoice45(cId, cType, cDesc) {
        document.getElementById('btnEditChecklistAdd').style.display = "none";
        document.getElementById('btnUpdateChecklistAdd').style.display = "none";
        document.getElementById('btnUpdateChecklistAdd45').style.display = "block";
        document.getElementById('rowChoiceChecklistItem').value = cId;
        document.getElementById('rowChoiceChecklistItemType').value = cType;
        document.getElementById('tbEditChecklistMain').value = cDesc;
        document.getElementById('tbEditChecklistSub').disabled = "disable";

        document.getElementById('btneditChecklistType5').style.display = "block";
        document.getElementById('tbChecklistItemTypeSub5').style.display = "block";
    }
    function editChecklistItemChoice(cId, cType, cDesc, cQt, cEnQ, cExQ) {

        document.getElementById('btneditChecklistType5').style.display = "none";
        document.getElementById('btnupdateChecklistType5').style.display = "block";

        document.getElementById('btneditChecklistType4').style.display = "none";
        document.getElementById('btnupdateChecklistType4').style.display = "block";

        document.getElementById('btnEditChecklistAdd').style.display = "none";
        document.getElementById('btnUpdateChecklistAdd').style.display = "block";
        document.getElementById('btnUpdateChecklistAdd45').style.display = "none";

        document.getElementById('rowChoiceChecklistItem').value = cId;
        document.getElementById('rowChoiceChecklistItemType').value = cType;
        if (cType == 1) {

            document.getElementById('tbEditChecklistDescription').value = cDesc;
            document.getElementById('tbEditChecklistQuantity').value = cQt;
        }
        else if (cType == 2) {

            document.getElementById('tbEditChecklistDescription').value = cDesc;
            document.getElementById('tbEditChecklistQuantity').value = cEnQ;
            document.getElementById('tbEditChecklistExitQuantity').value = cExQ;
        }
        else if (cType == 3) {

            document.getElementById('tbEditChecklistDescription').value = cDesc;
        }
        else if (cType == 4) {

            document.getElementById('tbChecklistItemTypeSub').value = cDesc;
        }

        else if (cType == 5) {

            document.getElementById('tbChecklistItemTypeSub5').value = cDesc;
        }

    }
    function pickdateMyTask() {
        if (taskfilter) {
            try {
                $("#mytasksTable tbody").empty();
                jQuery("#mytasksTable").dataTable().fnClearTable();
                jQuery("#mytasksTable").dataTable().fnDraw();
                jQuery("#mytasksTable").dataTable().fnDestroy();
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getTableDataMyTasksByDate",
                    data: "{'date':'" + document.getElementById('mytaskDatepicker').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            for (var i = 0; i < data.d.length; i++) {
                                $("#mytasksTable tbody").append(data.d[i]);
                            }
                            jQuery("#mytasksTable").DataTable({
                                "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                                'iDisplayLength': 10,
                                "order": [[4, "desc"]]
                            });
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            } catch (err) {
                alert(err);
            }
        }
    }

    function pickdateAllTask() {

        if (taskfilter) {
            try {
                $("#alltasksTable tbody").empty();
                jQuery("#alltasksTable").dataTable().fnClearTable();
                jQuery("#alltasksTable").dataTable().fnDraw();
                jQuery("#alltasksTable").dataTable().fnDestroy();
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getTableDataAllTasksByDate",
                    data: "{'date':'" + document.getElementById('alltaskDatepicker').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                for (var i = 0; i < data.d.length; i++) {
                                    $("#alltasksTable tbody").append(data.d[i]);
                                }
                                jQuery("#alltasksTable").DataTable({
                                    "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                                    'iDisplayLength': 10,
                                    "order": [[4, "desc"]]
                                });
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                } catch (err) {
                    alert(err);
                }
            }
        }

        //NEW STUFF

        function addrowtoTableAllTasks() {
            jQuery("#alltasksTable tbody").empty();
            jQuery("#alltasksTable").dataTable().fnClearTable();
            jQuery("#alltasksTable").dataTable().fnDraw();
            jQuery("#alltasksTable").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getTableDataAllTasks",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    taskfilter = true;
                    for (var i = 0; i < data.d.length; i++) {
                        $("#alltasksTable tbody").append(data.d[i]);
                    }
                    jQuery("#alltasksTable").DataTable({
                        "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                        'iDisplayLength': 10,
                        "order": [[4, "desc"]]
                    });
                }
            });
        }

        function showAllTask() {
             document.getElementById('t1d').style.display = "none";
            document.getElementById('t2d').style.display = "none";
            document.getElementById('t3d').style.display = "none";
            document.getElementById('t4d').style.display = "none";
        }
        function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function linkChoiceView(id) {
            document.getElementById('backLinkDisplay1').style.display = 'block';
            document.getElementById('backLinkDisplay2').style.display = 'block';
            document.getElementById('backLinkDisplay3').style.display = 'block';
            document.getElementById('backLinkDisplay4').style.display = 'block';
            document.getElementById("rowChoiceTasksLink").value = id;

            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getTaskLocationData",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d)
                        updateTaskMarker(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getSubTasklistItems(id) {
            document.getElementById("taskSubList").innerHTML = "";
            document.getElementById('taskSubDIV').style.display = 'none';
            document.getElementById('legendDIV').style.display = 'none';
            document.getElementById('backLinkDisplay1').style.display = 'none';
            document.getElementById('backLinkDisplay2').style.display = 'none';
            document.getElementById('backLinkDisplay3').style.display = 'none';
            document.getElementById('backLinkDisplay4').style.display = 'none';
            document.getElementById('subLinkDisplay1').style.display = 'none';
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getSubTaskListData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        document.getElementById('taskSubDIV').style.display = 'block';
                        document.getElementById('subLinkDisplay1').style.display = 'block';
                        document.getElementById('legendDIV').style.display = 'block';
                        document.getElementById('rejectDisplay1').style.display = 'none';
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("taskSubList");
                        var li = document.createElement("li");
                        var colorRet = 'green';
                        if (res[3] == "Pending") {
                            colorRet = 'red';
                        }
                        else if (res[3] == "InProgress") {
                            colorRet = 'yellow';
                        }
                        var c = (i + 1);
                        if (c == 1) {
                            li.innerHTML = c + '. <i class="fa fa-circle  ' + colorRet + '-color"></i><a style="margin-left:5px;" href="#"   class="capitalize-text" onclick="showTaskDocument(&apos;' + res[2] + '&apos;);linkChoiceView(&apos;' + id + '&apos;);" >' + res[0] + '</a>';
                        }
                        else
                            li.innerHTML = c + '.<i style="margin-left:1px;" class="fa fa-circle  ' + colorRet + '-color"></i><a style="margin-left:5px;" href="#"   class="capitalize-text" onclick="showTaskDocument(&apos;' + res[2] + '&apos;);linkChoiceView(&apos;' + id + '&apos;);" >' + res[0] + '</a>';

                        ul.appendChild(li);
                    }
                }
            });
        }

        var isLink = false;
        function linktoTask() {
            isLink = true;
            jQuery('#taskDocument').modal('hide');

            document.getElementById("saveAsTemplateLi").style.display = "none";

            document.getElementById("backTaskLi").style.display = "block";

            document.getElementById('newtaskHeader').innerHTML = "SUB-TASK FOR " + document.getElementById('taskincidentNameHeader').innerHTML;

            document.getElementById("customerslst").disabled = "";

            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getTableRowDataTemplate",
                data: "{'id':'" + document.getElementById("rowChoiceTasks").value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        //Ts
                        document.getElementById('tbNewLongitude').value = data.d[0];
                        document.getElementById('tbNewLatitude').value = data.d[1];

                        $('#projectlst option').remove();
                        var projectoption = $("#projectlst");
                        projectoption.append($('<option></option>').val(0).html('<%=selectProjectPlaceholder%>'));
                        $.ajax({
                            type: "POST",
                            url: "Tasks.aspx/getProjectListByCustomerId",
                            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var options = $("#projectlst");
                                for (var i = 0; i < data.d.length; i++) {
                                    options.append(
                                         $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                                     );
                                }
                            }
                        });

                        $("#projectlst").val(data.d[7]);

                        $('#customerslst option').remove();
                        $.ajax({
                            type: "POST",
                            url: "Tasks.aspx/getCustomerByProjectId",
                            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var options = $("#customerslst");
                                for (var i = 0; i < data.d.length; i++) {
                                    options.append(
                                         $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                                     );
                                }
                            }
                        });
                        if (data.d[8] > 0) {
                            document.getElementById("customerslst").disabled = true;
                        }
                        $("#customerslst").val(data.d[8]);
                        document.getElementById('tbCustomerId').value = data.d[8];
                        $('#contractslst option').remove();
                        var contractoption = $("#contractslst");
                        contractoption.append($('<option></option>').val(0).html('<%=selectContractPlaceholder%>'));
                        $.ajax({
                            type: "POST",
                            url: "Tasks.aspx/getContractListByCustomerId",
                            data: "{'id':'" + data.d[8] + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var options = $("#contractslst");
                                for (var i = 0; i < data.d.length; i++) {
                                    options.append(
                                         $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                                     );
                                }
                            }
                        });
                        $("#contractslst").val(data.d[9]);
                        cleardispatchList();
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });


        }

        function deleteProject() {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/deleteProject",
                data: "{'id':'" + document.getElementById("rowChoiceProject").value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Project successfully deleted";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        document.getElementById('<%= logoutbtn.ClientID %>').click();
                    }
                    else {
                        showAlert('Error 40: Failed to delete Project.-' + data.d);
                    }
                }
            });
    }
    function addrowtoTableProject() {
        $("#projectsTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/getTableProject",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                for (var i = 0; i < data.d.length; i++) {
                    jQuery("#projectsTable tbody").append(data.d[i]);
                }
            }
        });
    }

    function customerOnChange(e) {
        //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display

        $('#systemtypelst option').remove();
        var options = $("#systemtypelst");
        options.append($('<option></option>').val(0).html('Select Type'));

        if (e.options[e.selectedIndex].value > 0) {
            document.getElementById('tbsystemtypeId').value = e.options[e.selectedIndex].value;
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getSystemTypeByCustomerId",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#systemtypelst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].Description)
                         );
                    }
                }
            });
        }
        else {
            document.getElementById('tbsystemtypeId').value = "0";
        }
    }
    function saveeditProject() {
        var isPass = true;
        if (isEmptyOrSpaces(document.getElementById('editProjectinput').value)) {
            isPass = false;
            showAlert("Kindly provide name");
        }
        else {
            if (isSpecialChar(document.getElementById('editProjectinput').value)) {
                isPass = false;
                showAlert("Kindly remove special character name");
            }
        }

        var startDate = document.getElementById("projectEditStartCalendar").value;
        var endDate = document.getElementById("projectEditEndCalendar").value;

        var q = new Date();
        var m = q.getMonth();
        var d = q.getDate();
        var y = q.getFullYear();

        var date = new Date(y, m, d);
        var dd = dates.compare(date, startDate);
        var isErr = false;
        if (document.getElementById("projectEditStartCalendar").value != document.getElementById("startDateCheck").value) {
            if (dd == 1) {
                isPass = false;
                showAlert("Start Date should be set to today or future date.")
            }
        }
        var ddd = dates.compare(date, endDate);

        if (ddd == 1) {
            isPass = false;
            showAlert("End Date should be set to today or future date.")
        }

        var dddd = dates.compare(startDate, endDate);
        if (dddd == 1 || dddd == 0) {
            isPass = false;
            showAlert("End Date should be set to date greater than Start Date.")
        }


        if (isPass) {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/editProjectSave",
                data: "{'id':'" + document.getElementById("rowChoiceProject").value + "','type':'" + document.getElementById("editProjectinput").value + "','startDate':'" + document.getElementById("projectEditStartCalendar").value + "','endDate':'" + document.getElementById("projectEditEndCalendar").value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        jQuery('#editProject').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = "Project successfully edited";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        document.getElementById('<%= logoutbtn.ClientID %>').click();
                        }
                        else {
                            showAlert("Error 37: Failed to update Project. -" + data.d);
                        }
                    }
                });
        }
    }

    function savenewProject() {

        var newPw = document.getElementById("newProjectinput").value;
        var custId = document.getElementById("tbsystemtypeId").value;
        var systypeId = document.getElementById("systemtypelst").value;
        var projUId = document.getElementById("MainContent_projectUser").value;
        var startDate = document.getElementById("projectStartCalendar").value;
        var endDate = document.getElementById("projectEndCalendar").value;
        var mychecked = document.getElementById('MyselfCheck').checked;


        var q = new Date();
        var m = q.getMonth();
        var d = q.getDate();
        var y = q.getFullYear();

        var date = new Date(y, m, d);
        var dd = dates.compare(date, startDate);
        var isErr = false;
        if (dd == 1) {
            isErr = true;
            showAlert("Start Date should be set to today or future date.")
        }

        var ddd = dates.compare(date, endDate);

        if (ddd == 1) {
            isErr = true;
            showAlert("End Date should be set to today or future date.")
        }

        var dddd = dates.compare(startDate, endDate);
        if (dddd == 1 || dddd == 0) {
            isErr = true;
            showAlert("End Date should be set to date greater than Start Date.")
        }
        if (!isErr) {
            if (!isEmptyOrSpaces(newPw)) {
                if (!isSpecialChar(newPw)) {
                    $.ajax({
                        type: "POST",
                        url: "Tasks.aspx/newProjectSave",
                        data: "{'newtype':'" + newPw
                            + "','custId':'" + custId
                            + "','systypeId':'" + systypeId
                            + "','startDate':'" + startDate
                            + "','endDate':'" + endDate
                            + "','projUId':'" + projUId
                            + "','mychecked':'" + mychecked
                            + "','uname':'" + loggedinUsername + "'}",

                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "SUCCESS") {
                                jQuery('#newProjects').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Project successfully added";
                                jQuery('#successfulDispatch').modal('show');
                                document.getElementById("newProjectinput").value = "";
                            }
                            else if (data.d == "LOGOUT") {
                                document.getElementById('<%= logoutbtn.ClientID %>').click();
                                }
                                else {
                                    showAlert('Failed to save Project ' + data.d);
                                }
                            }
                        });
                }
            }
            else {
                showAlert("Kindly provide project name");
            }
        }
    }
    function getprojRemarks(id) {
        jQuery('#projectRemarksList div').html('');
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getProjRemarksData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                for (var i = 0; i < data.d.length; i++) {
                    var div = document.createElement('div');

                    div.className = 'row activity-block-container';

                    div.innerHTML = data.d[i];

                    document.getElementById('projectRemarksList').appendChild(div);
                }
            }
        });
    }
    function backToProject() {
        if (fromProj) {

            var el2 = document.getElementById('pprogress-tab');
            if (el2) {
                el2.className = 'tab-pane fade active in';
            }
            var el = document.getElementById('ptlist-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }

            var el3 = document.getElementById('liprg');
            if (el3) {
                el3.className = 'active';
            }
            var el5 = document.getElementById('litlist');
            if (el5) {
                el5.className = ' ';
            }
            jQuery('#taskDocument').modal('hide');
            jQuery('#projDocument').modal('show');



        }
    }
    function hideAllRemarks() {
        document.getElementById("rotationDIV1").style.display = "block";
        document.getElementById("rotationDIV2").style.display = "block";
        var el2 = document.getElementById('tasklocation-tab');
        if (el2) {
            el2.className = 'tab-pane fade active in';
        }
        var el = document.getElementById('tremarks-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }

        for (var i = 0; i < divArray.length; i++) {
            var el2 = document.getElementById(divArray[i]);
            el2.className = 'tab-pane fade';
        }
    }
    var fromProj = false;
    function projView(id, name, start, end, cname, oname, lname, createdby, isowner) {
        try {
            fromProj = true;

            document.getElementById("addtaskprojLi").style.display = "none";
            var el2 = document.getElementById('pprogress-tab');
            if (el2) {
                el2.className = 'tab-pane fade active in';
            }
            var el = document.getElementById('ptlist-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }

            var el3 = document.getElementById('liprg');
            if (el3) {
                el3.className = 'active';
            }
            var el5 = document.getElementById('litlist');
            if (el5) {
                el5.className = ' ';
            }

            document.getElementById("projCustomerNameSpan").innerHTML = cname;
            document.getElementById("projNameSpan").innerHTML = name;
            document.getElementById("projSDateSpan").innerHTML = start;
            document.getElementById("projownerNameSpan").innerHTML = oname;
            document.getElementById("projEDateSpan").innerHTML = end;
            document.getElementById("projCreatedBy").innerHTML = createdby;
            document.getElementById("rowChoiceProject").value = id;
            if (isowner == "true") {
                document.getElementById("addtaskprojLi").style.display = "block";
            }
            projViewTables(id)
            getprojRemarks(id)
        }
        catch (err) {
            //alert(err)
        }
    }

    function showAllRemarks(id) {
        document.getElementById("rotationDIV1").style.display = "none";
        document.getElementById("rotationDIV2").style.display = "none";
        if (document.getElementById('rejection-tab').className == "tab-pane fade active in") {

        }
        else {
            var el2 = document.getElementById('tasklocation-tab');
            if (el2) {
                el2.className = 'tab-pane fade';
            }
            var el = document.getElementById('tremarks-tab');
            if (el) {
                el.className = 'tab-pane fade active in';
            }

            for (var i = 0; i < divArray.length; i++) {
                var el2 = document.getElementById(divArray[i]);
                el2.className = 'tab-pane fade';
            }



            jQuery('#taskRemarksList2 div').html('');
            jQuery.ajax({
                type: "POST",
                url: "Tasks.aspx/getTaskRemarksData2",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('taskRemarksList2').appendChild(div);
                    }
                }
            });
        }
    }
    function gettaskRemarks(id) {
        jQuery('#taskRemarksList div').html('');
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/getTaskRemarksData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                for (var i = 0; i < data.d.length; i++) {
                    var div = document.createElement('div');

                    div.className = 'row activity-block-container';

                    div.innerHTML = data.d[i];

                    document.getElementById('taskRemarksList').appendChild(div);
                }
            }
        });
    }



    function CompleteTaskWithNotes(status) {
        var projn = document.getElementById("addtaskNotesTA").value;
        var id = document.getElementById('rowChoiceTasks').value;
        $.ajax({
            type: "POST",
            url: "Tasks.aspx/addNewTaskRemarks",
            data: "{'id':'" + id + "','notes':'" + projn + "','uname':'" + loggedinUsername + "','comp':'" + status + "','chklistid':'0'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {
                    if (status == "inprogress" || status == "complete") {
                        showAlert("Task successfully updated");
                        document.getElementById("addtaskNotesTA").value = "";
                        showTaskDocument(id);
                    }
                    else {
                        showAlert("Successfully added notes");
                        document.getElementById("addtaskNotesTA").value = "";
                        showTaskDocument(id);
                    }
                }
                else if (data.d == "LOGOUT") {
                    document.getElementById('<%= logoutbtn.ClientID %>').click();
                    }
                    else {
                        if (status == "inprogress" || status == "complete") {
                            showError(data.d);
                        }
                        else {
                            showAlert('Failed to save notes. ' + data.d);
                        }
                    }
                }
            });
    }


    function saveProjectRemarks() {
        var projn = document.getElementById("projectRemarksTextarea").value;
        var id = document.getElementById("rowChoiceProject").value;
        var isPass = true;
        if (isEmptyOrSpaces(document.getElementById('projectRemarksTextarea').value)) {
            isPass = false;
            showAlert("Kindly provide remarks to be added");
        }
        else {
            if (isSpecialChar(document.getElementById('projectRemarksTextarea').value)) {
                isPass = false;
                showAlert("Kindly remove special character from remarks");
            }
        }
        if (isPass) {
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/addNewProjRemarks",
                data: "{'id':'" + id + "','notes':'" + projn + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        showAlert("Successfully added remarks");
                        document.getElementById("projectRemarksTextarea").value = "";
                        getprojRemarks(id);
                    }
                    else if (data.d == "LOGOUT") {
                        document.getElementById('<%= logoutbtn.ClientID %>').click();
                        }
                        else {
                            showAlert('Failed to save remarks. ' + data.d);
                        }
                    }
                });
        }
    }


    function projViewTables(id) {
        jQuery("#projectsTaskTable tbody").empty();
        jQuery("#projectsTaskTable").dataTable().fnClearTable();
        jQuery("#projectsTaskTable").dataTable().fnDraw();
        jQuery("#projectsTaskTable").dataTable().fnDestroy();
        jQuery.ajax({
            type: "POST",
            url: "Tasks.aspx/projViewTable",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#projectsTaskTable tbody").append(data.d[i]);
                    }
                    jQuery("#projectsTaskTable").DataTable({
                        "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                        'iDisplayLength': 5,
                        "order": [[0, "asc"]]
                    });
                }
                else {
                    jQuery("#projectsTaskTable").DataTable({
                        "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                        'iDisplayLength': 5,
                        "order": [[0, "asc"]]
                    });
                }
            }
        });
    }
    function editProjectChoice(id, name, start, end) {
        document.getElementById("rowChoiceProject").value = id;
        document.getElementById("editProjectinput").value = name;
        var datesplit = start.split(' ');
        var dt = datesplit[0].split('/');
        var month = dt[0];
        if (dt[0].length < 2)
            month = "0" + dt[0];
        var daymonth = dt[1];
        if (dt[1].length < 2)
            daymonth = "0" + dt[1];
        var dayyear = dt[2];

        document.getElementById("projectEditStartCalendar").value = month + "/" + daymonth + "/" + dayyear;

        document.getElementById("startDateCheck").value = month + "/" + daymonth + "/" + dayyear;

        var datesplit2 = end.split(' ');
        var dt2 = datesplit2[0].split('/');
        var month2 = dt2[0];
        if (dt2[0].length < 2)
            month2 = "0" + dt2[0];
        var daymonth2 = dt2[1];
        if (dt2[1].length < 2)
            daymonth2 = "0" + dt2[1];
        var dayyear2 = dt2[2];

        document.getElementById("projectEditEndCalendar").value = month2 + "/" + daymonth2 + "/" + dayyear2;
    }
    function customersOnChange(e) {
        //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display

        $('#contractslst option').remove();

        var contractoption = $("#contractslst");
        contractoption.append($('<option></option>').val(0).html('<%=selectContractPlaceholder%>'));


        if (e.options[e.selectedIndex].value > 0) {
            document.getElementById('tbCustomerId').value = e.options[e.selectedIndex].value;
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getContractListByCustomerId",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#contractslst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                         );
                    }
                }
            });
            if (document.getElementById('projectlst').value > 0) {

            }
            else {
                $('#projectlst option').remove();
                var projectoption = $("#projectlst");
                projectoption.append($('<option></option>').val(0).html('<%=selectProjectPlaceholder%>'));
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getProjectListByCustomerId",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#projectlst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                             );
                        }
                    }
                });
            }

        }
        else {


            document.getElementById('tbCustomerId').value = "0";

            $('#customerslst option').remove();
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getCustomerByProjectId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#customerslst");
                    for (var i = 0; i < data.d.length; i++) {
                        cid = data.d[i].Id;
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                         );
                    }
                }
            });

            $('#projectlst option').remove();
            var projectoption = $("#projectlst");
            projectoption.append($('<option></option>').val(0).html('<%=selectProjectPlaceholder%>'));
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getProjectListByCustomerId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#projectlst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                         );
                    }
                }
            });
        }

    }


    function projectlstOnChange(e) {
        //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
        if (document.getElementById('customerslst').value > 0) {

        }
        else {
            $('#customerslst option').remove();
            if (e.options[e.selectedIndex].value == 0) {
                $('#contractslst option').remove();
                $('#projectlst option').remove();
                var contractoption = $("#contractslst");
                contractoption.append($('<option></option>').val(0).html('<%=selectContractPlaceholder%>'));
                var projectoption = $("#projectlst");
                projectoption.append($('<option></option>').val(0).html('<%=selectProjectPlaceholder%>'));
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getContractListByCustomerId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#contractslst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                             );
                        }
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getProjectListByCustomerId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#projectlst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                             );
                        }
                    }
                });
            }
            var cid = 0;
            $.ajax({
                type: "POST",
                url: "Tasks.aspx/getCustomerByProjectId",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#customerslst");
                    for (var i = 0; i < data.d.length; i++) {
                        cid = data.d[i].Id;
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                         );
                    }
                }
            });
            if (cid > 0) {
                $('#contractslst option').remove();
                var contractoption = $("#contractslst");
                contractoption.append($('<option></option>').val(0).html('<%=selectContractPlaceholder%>'));
                $.ajax({
                    type: "POST",
                    url: "Tasks.aspx/getContractListByCustomerId",
                    data: "{'id':'" + cid + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#contractslst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                             );
                        }
                    }
                });
            }
        }
    }

    function projectOnChange(e) {
        //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
        if (e.options[e.selectedIndex].value > 0) {
            document.getElementById('tbProjectId').value = e.options[e.selectedIndex].value;
        }

    }
    </script>
        <section class="content-wrapper" role="main">
            <div class="content">
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">TASKS</span></h3>

                                </div>
                                <div class="col-md-7">
                                    <div class="panel-control">
                                        <ul  class="nav nav-tabs nav-main"> 
                                            <li class="active"><a data-toggle="tab" href="#home-tab" onclick="showAllTask()">Tasks</a>
                                            </li>
                                            <li><a data-toggle="tab" href="#schedule-tab" onclick="jQuery('.show-component').show();readySchedule();">Schedule<sup>Beta</sup></a>
                                            </li>
                                            <li <%=cuserDisplay%>><a data-toggle="tab" href="#checklist-tab" onclick="jQuery('.show-component').show();">Checklist</a>
                                            </li>
                                            <li <%=cuserDisplay%>><a data-toggle="tab" href="#templates-tab" onclick="jQuery('.show-component').show();">Templates</a>
                                            </li>
                                            <li <%=cuserDisplay%>><a data-toggle="tab" href="#settings-tab" onclick="jQuery('.show-component').show();">Settings</a>
                                            </li>
                                            <li><a data-toggle="tab" href="#projects-tab" onclick="jQuery('.show-component').show();">Projects</a>
                                            </li> 
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div role="group" class="pull-right">
                                        <%=siteName%>
                                        <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                        <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
							<div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                     <li class="active"><a href="#component-alltask" data-toggle="tab" onclick="addrowtoTableAllTasks()">All</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#component-number-1" data-toggle="tab">My Tasks</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#component-number-2" data-toggle="tab">Team Tasks</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#component-number-4" data-toggle="tab">Accepted</a>
                                                    </li>
                                                    <li style="display:none;"><a href="#component-number-3" data-toggle="tab">Recurring</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li <%=cuserDisplay%> ><a href="#" id="firstModal" data-target="#newDocument" onclick="newTaskClick()" data-toggle="modal" class="capitalize-text">+ NEW TASKS</a>
                                                    </li>
<%--                                                    <li <%=cuserDisplay%>><a href="#" data-target="#newChecklist" data-toggle="modal" class="capitalize-text" onclick="clearChecklistBox() ">+ NEW CHECKLIST</a>
                                                    </li>--%>
                                                    <li <%=cuserDisplay%>><a href="#" data-target="#newChecklistModal2" data-toggle="modal" class="capitalize-text" onclick="disablechklist()">+ NEW CHECKLIST</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#" class="capitalize-text" onclick="window.open('https://livemimslob.blob.core.windows.net/0blob/Checklist-Upload-Form.xlsx')">DOWNLOAD FORM</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#" class="capitalize-text" onclick="document.getElementById('dz-import').click()">UPLOAD FORM</a>
                                                    </li> 
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                                                                <div class="row">
                                            <div class="col-md-3">
<div class="form-group">
                                          <div class="input-group input-group-in">
																	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>      
												<input placeholder="Pick a date" class="form-control" data-input="daterangepicker" id="fromStatusDatePicker" data-show-dropdowns="true" data-single-date-picker="true">
																									
															   </div>
															   <!-- /input-group-in -->
															</div>
                                            </div>
                                            <div class="col-md-3">
<div class="form-group">
                                          <div class="input-group input-group-in">
																	<span class="input-group-addon "><i class="fa fa-calendar"></i></span>      
												<input placeholder="Pick a date" class="form-control " data-input="daterangepicker" id="toStatusDatePicker" data-show-dropdowns="true" data-single-date-picker="true">
																										
															   </div>
															   <!-- /input-group-in -->
															</div>
                                            </div>
                                            <div class="col-md-6">
                                            <div class="row horizontal-navigation" style="margin-top:8px;">
							                    <div class="panel-control">
								                    <ul class="nav nav-tabs">
                                                        <li ><a style="padding:5px;" href="#" class="capitalize-text" onclick="getEventStatusRetrieve();">RETRIEVE</a>
									                    </li>
                                                        <li style="display: block;"><a style="padding:5px;" href="#" class="capitalize-text" onclick="getEventStatusTotal()">RESET</a>
									                    </li>
								                    </ul>
								                    <!-- /.nav -->
							                    </div>
						                    </div>
                                          </div>
                                        </div>
                                        <div  class="row horizontal-chart">
                                            <div class="col-md-2 text-center panel-seperator panel-heading">
                                                <h3 class="panel-title capitalize-text">STATUS</h3>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PENDING</p>
                                                </div>
                                                <div class="inline-block">
                                                   <div class="easyPieChart" id="statusPendingPie" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f44e4b">
                                                        <span class="percentage text-dark fa fa-1x">
														<span class="data-percent" id="statusPendingPercent"></span>%
                                                        </span>
                                                   </div>
                                                </div>
                                                <div class="inline-block ">
                                                    <h3 id="statusPending"></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PROGRESS</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" id="statusInprogressPie"  data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f2c400">
                                                        <span class="percentage text-dark fa fa-1x">
														  <span class="data-percent"  id="statusInprogressPercent"></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3 id="statusInprogress"></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">COMPLETED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" id="statusCompletePie"  data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#3ebb64">
                                                        <span class="percentage text-dark fa fa-1x">
																		<span class="data-percent" id="statusCompletePercent"></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3 id="statusComplete"></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text" >ACCEPTED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" id="statusAcceptedPie"  data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#1b93c0">
                                                        <span class="percentage text-dark fa fa-1x">
																		<span class="data-percent" id="statusAcceptPercent"></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3 id="statusAccept"></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 chart-total">
                                                <p><span class="capitalize-text" id="statusTotal"></span>TOTAL TASKS</p>
                                            </div>
                                        </div>

                                         <div class="row show-component component-alltask">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">ALL TASKS</h3>
                                                         <div class="row">
															<div class="col-md-8">
																<div class="progress" style="display:none;">
																	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0">
																	</div>
																</div>															
															</div>
															<div class="col-md-4">
																<p style="display:none;" class="white-color progress-bar-title">0% Handled</p>
															</div>
														</div>
                                                    </div>
                                                    <div class="col-md-8 task-table">
													   <div class="col-md-4 mt-3x"> 			
														  <div class="clearfx"></div>
													   </div>												   
													   <div class="col-md-4 mt-3x">
															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg">
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>       
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker"  onchange="pickdateAllTask()" id="alltaskDatepicker" data-show-dropdowns="true" data-single-date-picker="true">
																										
															   </div>
															   <!-- /input-group-in -->
															</div>
													   </div>												   
													   <div class="col-md-4 mt-2x">
                                                         <input id="alltaskDatepickerSearch" type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" order-of-rows="desc" order-of-column="4" id="alltasksTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY">
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">ID
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div> 
                                        


                                        <div id="t1d" class="row show-component component-number-1" style="display:none">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">MY TASKS</h3>
                                                         <div class="row" style="display:none;">
															<div class="col-md-8">
																<div class="progress">
																	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0">
																	</div>
																</div>															
															</div>
															<div class="col-md-4">
																<p   class="white-color progress-bar-title">0% Handled</p>
															</div>
														</div>
                                                    </div>
                                                    <div class="col-md-8 task-table">
													   <div class="col-md-4 mt-3x">
														  
                                                    <!--<div class="form-group">
                                                      <button id="reportrange" data-drops="down" class="btn btn-block btn-default btn-lg recurring-style">
					                                  <i class="fa fa-refresh"></i>
                                                     Recurring
                                                      </button>
                                                    </div>/form-group-->				
														  <div class="clearfx"></div>
													   </div>												   
													   <div class="col-md-4 mt-3x">
															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg">
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>      
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker" id="mytaskDatepicker" onchange="pickdateMyTask()" data-show-dropdowns="true" data-single-date-picker="true">
																										
															   </div>
															   <!-- /input-group-in -->
															</div>
													   </div>												   
													   <div class="col-md-4 mt-2x">
                                                         <input id="mytaskDatepickerSearch" type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="mytasksTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">
                                                                </th>
																<th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="ID" aria-sort="ascending">ID
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
																<th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                         <div id="t2d" class="row show-component component-number-2" style="display:none">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">TEAM TASKS</h3>
														<div class="row">
															<div class="col-md-8">
																<div class="progress">
																	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <%=handledTeamTasks%>%">
																	</div>
																</div>															
															</div>
															<div class="col-md-4">
																<p class="white-color progress-bar-title"><%=handledTeamTasks%>% Handled</p>
															</div>
														</div>
                                                    </div>
                                                    <div class="col-md-8 task-table">
													   <div class="col-md-4 mt-3x">
														  
                                                    <!--<div class="form-group">
                                                      <button id="reportrange2" data-drops="down" class="btn btn-block btn-default btn-lg recurring-style">
					                                  <i class="fa fa-refresh"></i>
                                                     Recurring
                                                      </button>
                                                    </div>/form-group-->				
														  <div class="clearfx"></div>
													   </div>												   
													   <div class="col-md-4 mt-3x">
															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg">
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>      
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker" id="teamtaskDatepicker" onchange="pickdateTeamTask()" data-show-dropdowns="true" data-single-date-picker="true">
															   </div>
															   <!-- /input-group-in -->
															</div>
													   </div>												   
													   <div class="col-md-4 mt-2x">
                                                          <input id="teamtaskSearchBox" type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" order-of-rows="desc" order-of-column="4" id="teamtasksTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY">
                                                                </th>
                                                                <th  class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY">ID
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="TIME" aria-sort="descending">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" style="width:90px;" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <div id="t3d" class="row show-component component-number-4" style="display:none">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">ACCEPTED TASKS</h3>
														<div class="row">
															<div class="col-md-8">
																<div class="progress" style="display:none;">
																	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" >
																	</div>
																</div>															
															</div>
															<div class="col-md-4">
																<p class="white-color progress-bar-title" style="display:none;"></p>
															</div>
														</div>
                                                    </div>
                                                    <div class="col-md-8 task-table">
													   <div class="col-md-4 mt-3x">			
														  <div class="clearfx"></div>
													   </div>												   
													   <div class="col-md-4 mt-3x">
<%--															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg">
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>     
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker" id="compteamtaskDatepicker" onchange="pickdatecompTeamTask()" data-show-dropdowns="true" data-single-date-picker="true">
															   </div>
															   <!-- /input-group-in -->
															</div>--%>
													   </div>												   
													   <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                          <input  type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="completedteamtasksTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">
                                                                </th>
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                         <div id="t4d" class="row show-component component-number-3" style="display:none">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">RECURRING</h3>
														<div class="row">
															<div class="col-md-8">
																<div class="progress" style="display:none;">
																	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" >
																	</div>
																</div>															
															</div>
															<div class="col-md-4">
																<p class="white-color progress-bar-title" style="display:none;"></p>
															</div>
														</div>
                                                    </div>
                                                    <div class="col-md-8 task-table">
													   <div class="col-md-4 mt-3x">			
														  <div class="clearfx"></div>
													   </div>												   
													   <div class="col-md-4 mt-3x">
<%--															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg">
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>     
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker" id="compteamtaskDatepicker" onchange="pickdatecompTeamTask()" data-show-dropdowns="true" data-single-date-picker="true">
															   </div>
															   <!-- /input-group-in -->
															</div>--%>
													   </div>												   
													   <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                          <input  type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="recurringTasksTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">RECURRING<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->

                                    </div>
                                </div>





                            </div>
							</div>

                            <div class="tab-pane fade " id="schedule-tab">
              <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li <%=cuserDisplay%> ><a href="#" id="firstModalz" data-target="#newDocument" onclick="newTaskClick()" data-toggle="modal" class="capitalize-text">+ NEW TASKS</a>
                                                    </li>
                                                    <li <%=cuserDisplay%> ><a href="#" id="firstModalz1" onclick="LoadAlloyUISchedule()" class="capitalize-text">RESET</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                       <input style="margin-top:5px;" id="searchDutyUserGroup" type="search" class="form-control" placeholder="Search User"><i onclick="searchDutyUserNameClick()" class="fa fa-search fa-1x red-color right-inline-icon " style="margin-top:124px;right:30px;"></i>
                                        <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:705px;"> <!--style="height:1205px;"> -->
                                        <div class="drag-elements" id="dutyUserCollection">
                                        </div>                                                                    
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-number-5">
                                        <div class="col-md-12" id="mySchedulerStart" style="margin-top:200px;">
                                            <div class="row">
                                                <div class="col-md-5"></div>
                                                <div class="col-md-2">
                                                    <h3>&nbsp;Initializing...</h3>
<div class="loaderspinner"></div>          
                 </div>                               <div class="col-md-5"></div>                                  
                                      </div>      
                                            </div>
                                            <div class="col-md-12" style="display:none;height:5000px;" id="mySchedulerFinish">
                                                
                                                   
                                            <!-- /.panel-heading -->
                                                <div id="wrapper">
	

		                                                <div id="myScheduler"></div>
                                                </div>
                                                <div >
                                                    

                                                </div>
                                                <div class="clearfix"></div>
                                                 </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                              
							</div>
                                

							<div class="tab-pane fade" id="checklist-tab">
                               <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>  
                                                     <li><a href="#component-number-type5" data-toggle="tab">Checklists</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li <%=cuserDisplay%> ><a href="#" data-target="#newDocument" onclick="newTaskClick()" data-toggle="modal" class="capitalize-text">+ NEW TASKS</a>
                                                    </li>
                                                    <%--<li <%=cuserDisplay%>><a href="#" data-target="#newChecklist" data-toggle="modal" class="capitalize-text" onclick="clearChecklistBox() ">+ NEW CHECKLIST</a>
                                                    </li> --%>
                                                                                                        <li <%=cuserDisplay%>><a href="#" data-target="#newChecklistModal2" data-toggle="modal" class="capitalize-text" onclick="disablechklist()">+ NEW CHECKLIST</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#" class="capitalize-text" onclick="window.open('https://livemimslob.blob.core.windows.net/0blob/Checklist-Upload-Form.xlsx')">DOWNLOAD FORM</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#" class="capitalize-text" onclick="document.getElementById('dz-import').click()">UPLOAD FORM</a>
                                                    </li> 
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10"> 
                                        <div class="row show-component component-number-type5">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">Checklist</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="type5Table" role="grid">
                                                        <thead>
                                                            <tr role="row"> 
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="templates-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li <%=cuserDisplay%> ><a href="#" data-target="#newDocument" onclick="newTaskClick()" data-toggle="modal" class="capitalize-text">+ NEW TASKS</a>
                                                    </li>
                                                    <%--<li <%=cuserDisplay%>><a href="#" data-target="#newChecklist" data-toggle="modal" class="capitalize-text" onclick="clearChecklistBox() ">+ NEW CHECKLIST</a>
                                                    </li>--%>
                                                                                                        <li <%=cuserDisplay%>><a href="#" data-target="#newChecklistModal2" data-toggle="modal" class="capitalize-text" onclick="disablechklist()">+ NEW CHECKLIST</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#" class="capitalize-text" onclick="window.open('https://livemimslob.blob.core.windows.net/0blob/Checklist-Upload-Form.xlsx')">DOWNLOAD FORM</a>
                                                    </li>
                                                    <li <%=cuserDisplay%>><a href="#" class="capitalize-text" onclick="document.getElementById('dz-import').click()">UPLOAD FORM</a>
                                                    </li> 
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                                                                <div class="row show-component component-number-1">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">TEMPLATES</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="templateTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div  class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
                                                            <div class="tab-content">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                             
                                                                             </div><div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>  
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                  			<label class="select select-o" >
                                           <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>       
                                                            <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                                      <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Contract</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                   <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                         <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                        
                                            </div>
                                                         <div class="row" style="display:none;">
                                            <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                                  
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                            <div class="tab-pane fade" id="settings-tab">
                              <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                       <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-number-5"  data-toggle="tab">Task Type</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li style="display:<%=newTypeDisplay%>" ><a href="#" data-target="#newTaskType" data-toggle="modal" class="capitalize-text">+ NEW TYPE</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-number-5">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">TASK TYPE</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="taskTypeTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="tab-pane fade" id="projects-tab">
                              <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                       <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-number-5"  data-toggle="tab">Projects</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li <%=cuserDisplay%>  ><a href="#" data-target="#newProjects" data-toggle="modal" class="capitalize-text">+ NEW PROJECT</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-number-5">

                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">PROJECTS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="projectsTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                  <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">ACCOUNT<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                  <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">MANAGER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">START<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">END<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
                            </div>
   
                                                                         
                            </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </div>
                <!-- /panel-body -->
            </div>
            <!-- /.panel -->

            <div aria-hidden="true" aria-labelledby="newTaskType" role="dialog" tabindex="-1" id="newTaskType" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">NEW TASK TYPE</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="New Task Type" id="newTaskTypeinput"/>
                              </div>
                           </div>
                        </form> 
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#"  onclick="savenewTaskType()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="editTaskType" role="dialog" tabindex="-1" id="editTaskType" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">EDIT TASK TYPE</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Edit Task Type" id="editTaskTypeinput"/>
                              </div>
                           </div>
                        </form> 
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" data-dismiss="modal" onclick="saveeditTaskType()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="deleteTypeModal" role="dialog" tabindex="-1" id="deleteTypeModal" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
                            <div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal">CANCEL</a>
                                        </li>   
                                        <li><a href="#" data-dismiss="modal" onclick="deleteTaskType()"><i class='fa fa-trash'></i>DELETE</a>
                                        </li>   
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
                    <NewTask:MyNewTask id="NewTaskControl" runat="server" />
            <TicketingCard:MyTicketingCard id="TicketingCardControl" runat="server" />
            <div aria-hidden="true" aria-labelledby="taskDocument" role="dialog" tabindex="-1" id="taskDocument" class="modal fade videoModal" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="headerImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="taskincidentNameHeader"></h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Status: <span id="taskstatusSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created by: <span id="taskusernameSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Assigned to: <span id="tasktypeSpan"></span></p>
						</div>	
                        <div class="col-md-3">
							<p>Task Type: <span id="ttypeSpan"></span></p>
						</div>								
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Location: <span id="tasklocSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created on: <span id="tasktimeSpan"></span></p>
						</div>	
                         <div class="col-md-3">
							<p>Assigned on: <span id="assignedTimeSpan"></span></p>
						</div>	
                        <div class="col-md-3" id="incidentItemsListDIV" style="display:none;">
							<p id="incidentItemsList"></p>
						</div>	
                        <div class="col-md-3" id="linkparentDIV" style="display:none;">
							<p id="linkparent"></p>
						</div>								
					  </div>			
					</div>
					<div class="modal-body">
						<div class="row">

							<div class="col-md-5" style="border-right: 1px #bbbbbb solid;">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" id="demo3-tabs">
                                            <li class="active" id="taskliInfo"><a href="#taskinfo-tab" data-toggle="tab" class="capitalize-text" onclick="document.getElementById('rowtasktracebackUser').style.display = 'none'">INFO</a>
                                            </li>
                                            <li id="taskliNotes"><a href="#notes-tab" data-toggle="tab" class="capitalize-text" onclick="document.getElementById('rowtasktracebackUser').style.display = 'none'">NOTES</a>
                                            </li>
                                            <li id="taskliActi"><a href="#taskactivity-tab" data-toggle="tab" onclick="tracebackOn()" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="taskliAtta"><a href="#taskattachments-tab" data-toggle="tab" class="capitalize-text" onclick="document.getElementById('rowtasktracebackUser').style.display = 'none'">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
                                        <div class="tab-content">
										<div class="tab-pane fade active in" id="taskinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Description:</b></p>
													<p id="taskdescriptionSpan"></p>
												</div>
                                                  <div class="col-md-12" id="dvCustomerNameSpan">
													<p class="red-color"><b>Account Name:</b></p>
													<p id="CustomerNameSpan"></p>
												</div>
                                                <div class="col-md-12" id="dvContractNameSpan">
													<p class="red-color"><b>Contract Name:</b></p>
													<p id="ContractNameSpan"></p>
												</div>
                                                 <div class="col-md-12" id="dvProjectNameSpan">
													<p class="red-color"><b>Project Name:</b></p>
													<p id="ProjectNameSpan"></p>
												</div>
                                            
                                                <div class="col-md-12" id="dvSystemTypeSpan">
													<p class="red-color"><b>System Type:</b></p>
													<p id="SystemTypeSpan"></p>
												</div>
                                              
                                                <div class="col-md-12">
													<p class="red-color"><b>Checklist Name : </b><b id="checklistnameSpan"></b><i id="checklistnamespanFA" style="margin-left:5px;" class="fa fa-check-square-o"></i></p>
                                                    <ul id="checklistItemsList" style="list-style-type: none;">

                                                    </ul> 
												</div>
                                                <div class="row" id="taskSubDIV" style="display:none;"> 
                                                  <div class="col-md-12">
													<p class="red-color"><b>Sub-Task : </b></p>
                                                    <ul id="taskSubList" style="list-style-type: none;margin-left:-30px;">

                                                    </ul>
												</div>
                                                </div>
                                                <div class="col-md-12" id="verificationDIV" style="display:none">
                                                    <p class="red-color"><b>Verification:</b></p>
                                                       <div class="row">
                                                         <div class="col-md-6">
                                                            <div class="help-block text-center">
                                                               <img id="veriResultIMG2" style="height:135px;width:135px;border-radius:8px;" src="">
                                                               <h4 class="gray-color">Comparison Image</h4>
                                                               </div>                                    
                                                         </div>
                                                         <div class="col-md-6">
                                                            <div class="help-block text-center">
                                                               <img id="veriSentIMG2" style="height:135px;width:135px;border-radius:8px;" src="">
                                                               <h4 class="gray-color">Sent Image</h4>
                                                               </div>
                                                         </div>                                 
                                                      </div>
                                                    <div class="text-center">
                                                    <a href="#" style="text-decoration:underline" onclick="assignVerifierRequestData()">View All</a>
					                                </div>
                                                </div>
											</div>		
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
                                             <div class="row" id="legendDIV" style="display:none">
                                                <div class="col-md-4"><i class="fa fa-circle  red-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Pending</a></div>
                                                <div class="col-md-4"><i class="fa fa-circle  yellow-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Inprogress</a></div>
                                                <div class="col-md-4"><i class="fa fa-circle  green-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Complete</a></div>
                                            </div>
										</div>
                                        <div class="tab-pane fade" id="notes-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x" style="display:none;">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
													<p id="taskinstructionSpan"></p>
												</div>
											</div>	
                                            <div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
                                                    <div id="taskRemarksList" >
												    </div>
												</div>
											</div>	
											<div class="row">
												<div class="col-md-12">
													<p class="red-color"><b>Checklist Notes:</b></p>
													<p id="checklistNotesSpan"></p>
												</div>
											</div>	
											<div id="pchecklistItemsListNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Unchecked Items:</b></p>
													 <ul id="checklistItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
                                            <div id="pCanvasNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Canvas Notes:</b></p>
													 <ul id="canvasItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	          
                                            <div id="pRejectionNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Rejection Notes:</b></p>
                                                    <p id="rejectionListNotes"></p>
												</div>
											</div>	                                       
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
										<div class="tab-pane fade" id="taskactivity-tab">
                                                    <div id="taskdivIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div>
										<div class="tab-pane fade" id="taskattachments-tab" onclick="startRot();nextbackTask();">	
                                              <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="taskattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>								
										</div>
							            </div>
									</div>
								</div>
							</div>
							
                            <div class="col-md-1"  style="width:40px;">

                                <i id="rotationDIV1" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb" class="fa fa-angle-double-left  fa-2x" onclick="backImg()"></i>

                            </div>
                            
                            <div class="col-md-5" id="taskdivAttachmentHolder" style="width:440px;margin-top:4px;border-right: 1px #bbbbbb solid;border-left: 1px #bbbbbb solid;">
                                                                										<div class="tab-pane fade" id="tremarks-tab">
                                                                            								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li><a href="#tasklocation-tab" data-toggle="tab" onclick="hideAllRemarks()" class="capitalize-text">BACK</a>
                                            </li> 										
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                                    <div id="taskRemarksList2"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div> 
								<div class="tab-pane fade active in" id="tasklocation-tab">
                                    <div id="taskmap_canvasIncidentLocation" style="width:100%;height:380px;"></div>
									<div id="taskdivAttachment" onclick="startRot()" class="overlapping-map-image">
									</div>
								</div>		
                                <div class="tab-pane fade" id="rejection-tab">
	                                <p class="red-color text-center"><b>Rejection Notes:</b></p>
									<div class="col-md-12" style="height:350px;">
										<textarea placeholder="Rejection Notes" id="taskrejectionTextarea" class="form-control" rows="3"></textarea>

                                        <div class="row">
                                            <p class="red-color text-center"><b>Assign To Type:</b></p>
                                          <label class="select select-o">
                                           <select id="rejecttaskSelectAssigneeType" onchange="rejecttaskselectAssigneeTypeChange()">
											  <option>User</option>
											  <option style="display:<%=grpOptionView%>">Group</option>
											</select>
										 </label>
                                        </div>

                                        <div class="row" id="rejectUsersDiv">
                                             <p class="red-color text-center"><b>Assign To User:</b></p>
                                            <select id="rejectusersearchselect" class="selectpicker form-control"  data-live-search="true" runat="server">
                                            </select>

									    </div>
                                         <div class="row" id="rejectGroupDiv" style="display:none">
                                             <p class="red-color text-center"><b>Assign To Group:</b></p>
                                            <select id="rejectgroupsearchselect" class="selectpicker form-control"  data-live-search="true" runat="server">
                                            </select>
                                         </div>                       
                                    </div>
                                </div>									
							</div>

                             <div class="col-md-1"  style="width:40px;">
                                 <i id="rotationDIV2" class="fa fa-angle-double-right  fa-2x" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb;"  onclick="nextImg()"></i>

                             </div>

						</div> 
                        <div class="row">
                                                        <div class="col-md-5">

                            </div>
                                    <div class="col-md-7" style="display:none;border-left:0px;" id="taskAudioDIV"> 
                                        <audio id="taskAudio" style="width: 100%;" width="100%" controls><source id="taskAudioSrc" type="audio/mpeg"/></audio>
                                    </div>
                        </div>
					</div>
					<div class="modal-footer">
                      <div class="row" id="rowtasktracebackUser" style="display:none;">
                        <div class="col-md-5">
                            </div>
                               <div class="col-md-7" style="text-align:left;margin-top:-8px;margin-bottom:20px;">
                                  <a style="font-size:16px;" href="#" id="playpausefilter" onclick="playpauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1)"><i id="taskfilter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5)"><i id="taskfilter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30)"><i id="taskfilter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60)"><i id="taskfilter4" class="fa fa-square-o"></i>1 HOUR</a>

						                </div>
                            </div>
                        </div>
                      <div class="row" id="pdfloadingAcceptTask" style="padding-bottom:75px;display:none;">
                            <div class="col-md-5">

                            </div>
                            <div class="col-md-6" style="margin-bottom:-50px">
                                <p class="red-color text-center" style="font-size:12px"><b id="gnNoteTask">GENERATING REPORT</b></p>
                                                                            <div id="myProgressTask">
                                              <div id="myBarTask"></div>
                                            </div>
                            </div>
                        </div>
						<div class="row horizontal-navigation">
							<div class="panel-control">
                                <ul class="nav nav-tabs" id="taskinitialOptionsDiv">
                                    <li id="backLinkDisplay1" style="display:none;"><a href="#" onclick="showTaskDocument(document.getElementById('rowChoiceTasksLink').value)">BACK</a>
									</li> 
									<li><a href="#" data-dismiss="modal" onclick="backToProject()" >CLOSE</a>
									</li>  
                                    <li>
                                    <a href="#" onclick="addNotesToTask()">ADD NOTES</a>
                                    </li>	
                                    <li>
<a href="#" id="initialAttach" onclick="document.getElementById('dz-postt').click()">ATTACH</a>

                                    </li>	
                                    <li id="subLinkDisplay1" style="display:none;"><a href="#"  data-target="#newDocument" data-toggle="modal"  onclick="newTaskClick();linktoTask();">ADD SUB-TASK</a>
									</li>	 
                                    <li id="inprogressIODiv"> 
                                    <a href="#" onclick="CompleteTaskWithNotes('inprogress')">START</a>
                                    </li>
                                    <li id="completeIODiv">
                                    <a href="#" onclick="CompleteTaskWithNotes('complete')">COMPLETE</a>
                                    </li>	
								</ul>
                                <ul class="nav nav-tabs" id="acceptedOptionsDiv">
                                     <li id="backLinkDisplay2" style="display:none;"><a href="#"  onclick="showTaskDocument(document.getElementById('rowChoiceTasksLink').value)">BACK</a>
									</li>
									<li><a href="#" data-dismiss="modal" onclick="backToProject()">CLOSE</a>
									</li>
                                    <li>
                                    <a href="#" onclick="addNotesToTask()">ADD NOTES</a>
                                    </li>	 
                                    <li id="acceptPDFDIV" class="pull-right"><a href="#" class="capitalize-text " onclick="generateTaskPDF()">PDF</a>
                                    </li>	
								</ul>
								<ul class="nav nav-tabs" id="taskhandleOptionsDiv" style="display:none;">
                                    <li id="backLinkDisplay3" style="display:none;"><a href="#" onclick="showTaskDocument(document.getElementById('rowChoiceTasksLink').value)" >BACK</a>
									</li>
                                    <li><a href="#" data-dismiss="modal" onclick="backToProject()">CLOSE</a>
									</li>	
                                        <li>
                                    <a href="#" onclick="addNotesToTask()">ADD NOTES</a>
                                    </li>		
                                                                        <li>
<a href="#" id="handleAttach" onclick="document.getElementById('dz-postt').click()">ATTACH</a>

                                    </li>	
                                    <li id="acceptLiHandle"><a href="#" onclick="taskStateChange('Accept')" data-dismiss="modal">ACCEPT</a>
									</li>
                                    <li id="rejectDisplay1"><a href="#" data-target="#rejection-tab" data-toggle="tab"  onclick="rejectionSelect();">REJECT</a>
									</li>
                                    <li id="subLinkDisplay2"><a href="#"  data-target="#newDocument" data-toggle="modal"  onclick="newTaskClick();linktoTask();">ADD SUB-TASK</a>
									</li>		
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateTaskPDF()">PDF</a>
                                    </li>		
			
								</ul>
                                <ul class="nav nav-tabs" id="taskrejectOptionsDiv" style="display:none;">
                                    <li><a href="#"  onclick="TaskIsCompleted()">BACK</a>
									</li>
                                    <li><a href="#"  onclick="userTaskAssign('assign')">ASSIGN</a>
									</li>
									<li><a href="#"  onclick="userTaskAssign('reassign')">REASSIGN</a>
									</li>
								</ul>
                                <div class="col-md-8">
                                <ul class="nav nav-tabs" id="myOptionsDiv" style="display:none;">
                                    <li id="backLinkDisplay4" style="display:none;"><a href="#" onclick="showTaskDocument(document.getElementById('rowChoiceTasksLink').value)" >BACK</a>
									</li>
                                    <li><a href="#" data-dismiss="modal" onclick="backToProject()">CLOSE</a>
									</li>	
                                    <li>
                                    <a href="#" onclick="addNotesToTask()">ADD NOTES</a>
                                    </li>	
                                    <li>
                                        <a href="#" id="myOptionsAttach" onclick="document.getElementById('dz-postt').click()">ATTACH</a>
                                    </li>	
                                    <li id="inprogressIODiv2"> 
                                    <a href="#" onclick="CompleteTaskWithNotes('inprogress')">START</a>
                                    </li>	
                                    <li id="completeIODiv2">
                                    <a href="#" onclick="CompleteTaskWithNotes('complete')">COMPLETE</a>
                                    </li>
                                    <li id="myAssignLi"><a href="#" onclick="myUserTaskAssign()">ASSIGN</a>
									</li>
								</ul>
                                <textarea placeholder="Add Notes" style="display:none;" id="addtaskNotesTA" class="form-control" rows="3"></textarea>
                                
                                <form style="display:none" enctype="multipart/form-data" id="dz-postt" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </form>

                                </div>
                                <div class="col-md-4" id="myOptionsDiv2" style="margin-left: -20px;padding-right: 0px;padding-left: 0px;">
                                    <div class="col-md-2" style="margin-top:10px;padding: 0px;">
                                    <p class="red-color text-center"><b>Assign:</b></p>
                                        </div>
                                    <div class="col-md-4" style="margin-top:5px;padding-right: 0px;">
                                                                             <label class="select select-o">
                                           <select id="myAssignType" onchange="myAssignTypeChange()">
											  <option>User</option>
											  <option style="display:<%=grpOptionView%>" >Group</option>
											</select>
										 </label>
                                        </div>
                                    <div id="myusersearchselectLi" style="margin-top:3px;
    padding-right: 0px;
    width: 144px;
    margin-left: 0px;" class="col-md-6">
                                                                                                                    <select id="myusersearchselect" style="border:none;" class="selectpicker form-control"  data-live-search="true" runat="server">
                                        </select>
                                        </div>
                                    <div id="mygrpsearchselectLi" style="display:none;margin-top:3px;
    padding-right: 0px;
    width: 144px;
    margin-left: 0px;" class="col-md-6">
                                                                               <select id="mygrpsearchselect" style="border:none;" class="selectpicker form-control"  data-live-search="true" runat="server">
                                        </select>
                                    </div>
                                </div>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="deleteIncidentModal" role="dialog" tabindex="-1" id="deleteIncidentModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteTask()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="deleteChecklistModal" role="dialog" tabindex="-1" id="deleteChecklistModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteChecklist()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	   
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="successfulReport" role="dialog" tabindex="-1" id="successfulReport" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" id="successfulReportScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            		                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                                                      <form style="display:none" enctype="multipart/form-data" id="dz-postticket" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </form>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
            <div aria-hidden="true" aria-labelledby="newChecklist" role="dialog" tabindex="-1" id="newChecklist" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="checklistCancel()"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CREATE NEW CHECKLIST</h4>
                     </div>
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-md-4">
                              <ul class="nav panel-filled">
                                 <li class="active" id="tbType1">
                                    <a href="#tab-1" data-toggle="tab"> 
                                    <h3 class="capitalize-text no-vmargin">Singular with Quantity</h3>
                                    </a>
                                 </li> 
                                 <li id="tbType3">
                                    <a href="#tab-3" data-toggle="tab">
                                    <h3 class="capitalize-text no-vmargin">Singular with Description</h3>
                                    </a>
                                 </li> 
                                <li id="tbType5">
                                    <a href="#tab-5" data-toggle="tab">
                                    <h3 class="capitalize-text no-vmargin">Nested checklist with notes</h3>
                                    </a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                           <div class="col-md-8">
                              <div class="tab-content">
                                 <div class="tab-pane fade in active" id="tab-1">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Name" id="tbType1Name" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Enter New Item" id="tbType1Description" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Quantity" id="tbType1Quantity" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row horizontal-navigation mt-2x">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li><a href="#" onclick="AddChecklistParent(1)">ADD</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                    <div class="row mt-2x">
                                        <div class="table-responsive">
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="newType1Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DESCRIPTION<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USERNAME">QUANTITY<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                        </div>
                                   </div>
                                </div>                              
                                 <div class="tab-pane fade" id="tab-2">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Name" id="tbType2Name" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Enter New Item" id="tbType2Description" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Entry Quantity" id="tbType2EntryQuantity" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Exit Quantity" id="tbType2ExitQuantity" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row horizontal-navigation mt-2x">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li><a href="#" onclick="AddChecklistParent(2)">ADD</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                    <div class="row mt-2x">
                                        <div class="table-responsive">
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="newType2Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DESCRIPTION<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USERNAME">ENTRY<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USERNAME">EXIT<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                        </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane fade" id="tab-3">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Name" id="tbType3Name" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Enter New Item" id="tbType3Description" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row horizontal-navigation mt-2x">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li><a href="#" onclick="AddChecklistParent(3)">ADD</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                    <div class="row mt-2x">
                                        <div class="table-responsive">
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="newType3Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DESCRIPTION<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                        </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane fade" id="tab-4">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Name" id="tbType4Name" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Main" id="tbType4Main" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Sub" id="tbType4Sub" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row horizontal-navigation mt-2x">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li><a href="#" onclick="AddChecklistParent(4)">ADD</a>
                                             </li>
                                             <li><a href="#" onclick="NextChecklistType4()">NEXT</a>
                                             </li>
                                             <li><a href="#" onclick="deleteChoiceChkItem('4')">DELETE</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                    <div class="row mt-2x">
                                        <div class="table-responsive">
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="newType4Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">MAIN<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USERNAME">SUB<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>
                                              </tbody>
                                           </table>
                                        </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane fade" id="tab-5">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Name" id="tbType5Name" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Main" id="tbType5Main" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Sub" id="tbType5Sub" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row horizontal-navigation mt-2x">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li><a href="#" onclick="AddChecklistParent(5)">ADD</a>
                                             </li>
                                             <li><a href="#" onclick="NextChecklistType5()">NEXT</a>
                                             </li>
                                             <li style="display:none;" id="deleteChoiceChkItem5"><a href="#" onclick="deleteChoiceChkItem('5')">DELETE</a>
                                             </li> 
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                    <div class="row mt-2x">
                                        <div class="table-responsive">
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="newType5Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">MAIN<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USERNAME">SUB<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>
                                              </tbody>
                                           </table>
                                        </div>
                                    </div>
                                 </div>
                              </div>
                                                                                                   <form style="display:none" enctype="multipart/form-data" id="dz-import" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </form>
                          </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li ><a href="#" onclick="checklistCreate()">CREATE</a>
                                 </li>
                                 <li><a href="#" onclick="checklistCancel()" data-dismiss="modal">CANCEL</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>                     
                  </div>
                  <!-- /.modal-content -->
                    </div>
               <!-- /.modal-dialog -->
                </div>
            </div>
             
            <div aria-hidden="true" aria-labelledby="newChecklistModal2" role="dialog" tabindex="-1" id="newChecklistModal2" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-lgx">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="refreshchktable()"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text" id="newChecklistModal2Header">CREATE CHECKLIST</h4> 
                     </div>
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-md-3"> 
                               <div class="row" style="margin-top:10px;">
                                   <p id="chknamep">Checklist Name:<span><a href="#" onclick="unlockchcklistname()"><i class="fa fa-pencil mr-1x"></i>Edit</a><a href="#" onclick="savechklistname()"><i class="fa fa-save mr-1x"></i>Save</a></span></p>
                                  <input placeholder="Checklist Name" id="tbnewchecklistname" class="form-control">
                               </div>
                               <div class="row" style="margin-top:10px;">
                                   <p>Checklist Details:</p>
                             <input placeholder="Main Item" id="tbnewchecklistmain" class="form-control">
                                   </div>
                               <div class="row" style="margin-top:10px;">
                             <input placeholder="Description" id="tbnewchecklistdesc" class="form-control">
                                   </div>
                               <div class="row" style="margin-top:10px;">
                             <input placeholder="? or Target Value 1. Target Value 2" id="tbnewchecklistqt" class="form-control">
                                   </div>
                                <div class="row" style="margin-top:10px;">
                                <input placeholder="Location" id="tbnewchecklistloc" class="form-control">
									
                                        </div>                      <div class="row" style="margin-top:10px;">
                                   <div class="col-md-11">
                                       <p style="padding-top:10px;">Requires Photo Proof</p>
                                   </div>
                                   <div class="col-md-1">
                                       <div class='nice-checkbox'>
                                               <input id='photochecklistinfo' type='checkbox' name='niceCheck'>
                                               <label for='photochecklistinfo'></label>
                                           </div>
                                   </div>
                                   </div>
                               <div class="row" style="margin-top:10px;">
                                   <div class="col-md-11">
                                       <p style="padding-top:10px;">Keep same details for next item</p>
                                   </div>
                                   <div class="col-md-1">
                                       <div class='nice-checkbox'>
                                               <input id='keepchecklistinfo' type='checkbox' name='niceCheck'>
                                               <label for='keepchecklistinfo'></label>
                                           </div>
                                   </div>
                                   </div>
                                   
                           </div>
                           <div class="col-md-9">
                                                                                               <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ITEMS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="chklistitemsTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="MAIN" aria-sort="ascending">MAIN
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="SUB">SUB<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="LOCATION">VALUE<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="LOCATION">LOCATION<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="PHOTO">PHOTO<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">ACTION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                          </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li  id="chkREM" style="display:none"><a href="#" onclick="checklistCreate()">REMOVE</a>
                                 </li> 
                                 <li  id="chkCANC"><a href="#"  onclick="refreshchktable()" data-dismiss="modal">CLOSE</a>
                                 </li>
                                 <li  id="chkADD"><a href="#" onclick="savechklistItem()">ADD</a>
                                 </li>
                                 <li  id="chkSAV"><a href="#" onclick="savechklist()">SAVE</a>
                                 </li> 
                                 <li  style="display:none;" id="chkEDIT"><a href="#" onclick="editCHKListItem()">SAVE EDIT</a>
                                 </li> 
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>                     
                  </div>
                  <!-- /.modal-content -->
                    </div>
               <!-- /.modal-dialog -->
                </div>
            </div>
             <div aria-hidden="true" aria-labelledby="editChecklist" role="dialog" tabindex="-1" id="editChecklist" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" ><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text" id="checklistHeader">CHECKLIST</h4>
                     </div>
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="tab-content">
                                 <div class="tab-pane fade in active" >
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Name" disabled="disabled" id="tbEditChecklistName" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row" id="tbEditChecklistDescriptionDIV">
                                       <div class="col-md-12">
                                          <input placeholder="Description" id="tbEditChecklistDescription" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row" id="tbEditChecklistQuantityDIV">
                                       <div class="col-md-12">
                                          <input placeholder="Quantity" id="tbEditChecklistQuantity" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row" id="tbEditChecklistExitQuantityDIV">
                                       <div class="col-md-12">
                                          <input placeholder="Quantity" id="tbEditChecklistExitQuantity" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row" id="tbEditChecklistMainDIV">
                                       <div class="col-md-12">
                                          <input placeholder="Main" id="tbEditChecklistMain" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row" id="tbEditChecklistSubDIV">
                                       <div class="col-md-12">
                                          <input placeholder="Sub" id="tbEditChecklistSub" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row horizontal-navigation mt-2x">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li id="btnEditChecklistAdd"><a href="#" onclick="addToChecklistItem()">ADD</a>
                                             </li> 
                                              <li style="display:none;" id="btnUpdateChecklistAdd"><a href="#" onclick="updateChecklistItem()">SAVE</a>
                                             </li>
                                              <li style="display:none;" id="btnUpdateChecklistAdd45"><a href="#" onclick="updateChecklistItem45()">SAVE</a>
                                             </li>
                                              
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                    <div class="row mt-2x">
                                        <div style="max-height:380px;" class="table-responsive">
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="editType1Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" style="width:5%;" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DESCRIPTION<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USERNAME">QUANTITY<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="editType2Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" style="width:5%;" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DESCRIPTION<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USERNAME">ENTRY<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USERNAME">EXIT<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="editType3Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" style="width:5%;" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DESCRIPTION<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="editType4Table" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" style="width:5%;" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">MAIN<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                        </div>
                                   </div>
                                </div>                              
                              </div>
                          </div>
                     </div>
                     <div class="modal-footer">
                     </div>                     
                  </div>
                  <!-- /.modal-content -->
                    </div>
               <!-- /.modal-dialog -->
                </div>
            </div>
            <div aria-hidden="true" aria-labelledby="editChecklistType4" role="dialog" tabindex="-1" id="editChecklistType4" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#editChecklist').modal('show');"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">NESTED CHECKLIST</h4>
                     </div>
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="tab-content">
                                 <div class="tab-pane fade in active" >
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Name" readonly="readonly" id="tbChecklistItemType4" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Enter New Item" id="tbChecklistItemTypeSub" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row horizontal-navigation mt-2x">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li id="btneditChecklistType4"><a href="#" onclick="addToChecklistItemType4()">ADD</a>
                                             </li> 
                                              <li id="btnupdateChecklistType4" style="display:none"><a href="#" onclick="updateChecklistItem()">SAVE</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                    <div class="row mt-2x">
                                        <div style="max-height:380px" class="table-responsive">
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="checklisttabletype4sub" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DESCRIPTION<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                        </div>
                                   </div>
                                </div>                              
                              </div>
                          </div>
                     </div>
                     <div class="modal-footer">
                     </div>                     
                  </div>
                  <!-- /.modal-content -->
                    </div>
               <!-- /.modal-dialog -->
                </div>
            </div>
            <div aria-hidden="true" aria-labelledby="editChecklistType5" role="dialog" tabindex="-1" id="editChecklistType5" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#editChecklist').modal('show');"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">NESTED CHECKLIST NOTES</h4>
                     </div>
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="tab-content">
                                 <div class="tab-pane fade in active" >
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Name" readonly="readonly" id="tbChecklistItemType5" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <input placeholder="Enter New Item" id="tbChecklistItemTypeSub5" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row horizontal-navigation mt-2x">
                                       <div class="panel-control">
                                          <ul class="nav nav-tabs">
                                             <li id="btneditChecklistType5"><a href="#" onclick="addToChecklistItemType5()">ADD</a>
                                             </li> 
                                              <li id="btnupdateChecklistType5" style="display:none;"><a href="#" onclick="updateChecklistItem()">SAVE</a>
                                             </li>
                                          </ul>
                                          <!-- /.nav -->
                                       </div>
                                    </div>
                                    <div class="row mt-2x">
                                        <div style="max-height:380px" class="table-responsive">
                                           <table class="table table-condensed table-noborder table-striped bordered-top app-modal-checkbox" id="checklisttabletype5sub" role="grid">
                                              <thead>
                                                 <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DESCRIPTION<a href="#"><i class="fa fa-sort ml-2x"></i></a>
                                                    </th>
                                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION<a href="#"></a>
                                                    </th>
                                                 </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                           </table>
                                        </div>
                                   </div>
                                </div>                              
                              </div>
                          </div>
                     </div>
                     <div class="modal-footer">
                     </div>                     
                  </div>
                  <!-- /.modal-content -->
                    </div>
               <!-- /.modal-dialog -->
                </div>
            </div>
            <!-- /EID-DEMO-->
            <div aria-hidden="true" aria-labelledby="verificationDocument" role="dialog" tabindex="-1" id="verificationDocument" class="modal fade videoModal" style="display: none;">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <div class="row">
                           <div class="col-md-11">
                              <h4 class="modal-title capitalize-text">VERIFICATION</h4>
                           </div>
                           <div class="col-md-1">
                              <button aria-hidden="true" data-dismiss="modal" class="close" onclick="jQuery('#taskDocument').modal('show');" type="button"><i class="icon_close fa-lg"></i></button>
                           </div>
                        </div>

                     </div>
                     <div class="modal-body pt-4x">
                        <div class="row border-bottom pb-4x selected-user container-match-user-1">
                           <div class="col-md-15 text-center border-right match-user match-user-1" onclick="moveImageToComparison('1')"> 
                              <div class="help-block">
                                 <img id="veriResult1IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font green-color" id="resultPercent1">
                                    
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-2" onclick="moveImageToComparison('2')"> 
                              <div class="help-block">
                                 <img id="veriResult2IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font yellow-color" id="resultPercent2">

                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-3" onclick="moveImageToComparison('3')"> 
                              <div class="help-block">
                                 <img id="veriResult3IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent3">
                                
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-4" onclick="moveImageToComparison('4')"> 
                              <div class="help-block">
                                 <img id="veriResult4IMG"  style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent4">
                                   
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center match-user match-user-5" onclick="moveImageToComparison('5')"> 
                              <div class="help-block">
                                 <img id="veriResult5IMG" style="height:61px;width:61px;"  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent5">
                                    
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>                                                                                                            
                        </div>
                        <div class="row">
                           <div class="col-md-17 border-right mt-2x mb-2x">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="help-block text-center">
                                       <img id="veriResultIMG" style="height:135px;width:135px;border-radius:8px;" src="">
                                       <h4 class="gray-color">Comparison Image</h4>
                                       <a href="#enlargeImageModal" class="red-color" data-dismiss="modal" data-toggle="modal" onclick="enlargeDisplayImage('Comparison')">Enlarge Image</a>
                                    </div>                                    
                                 </div>
                                 <div class="col-md-6">
                                    <div class="help-block text-center">
                                       <img id="veriSentIMG" style="height:135px;width:135px;border-radius:8px;" src="">
                                       <h4 class="gray-color">Sent Image</h4>
                                       <a href="#enlargeImageModal" class="red-color" data-dismiss="modal" data-toggle="modal" onclick="enlargeDisplayImage('Sent')">Enlarge Image</a>
                                    </div>
                                 </div>                                 
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                     <div id="map_verLocation" style="width:100%;height:174px;"></div>
                                 </div>                                 
                              </div>                              
                           </div>
                           <div class="col-md-18">
                              <div class="row">
                                 <div class="col-md-12">
                                    <h4 class="main-header" id="verifyHeaderSpan" style="color:#b2163b;"></h4>
                                 </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Created by:
                                       </p>
                                       <span class="red-color" id="verifyUserSpan">
                                             
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Time:
                                       </p>
                                       <span class="red-color" id="verifyTimeSpan">
                                       </span>                                       
                                    </div>
                              </div>
   
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Type:
                                       </p>
                                       <span class="red-color" id="verifyTypeSpan">
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Location:
                                       </p>
                                       <span class="red-color" id="verifyLocSpan">
                                       </span>                                       
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Case ID:
                                       </p>
                                       <span class="red-color" id="verifyCaseSpan">
                                             
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             PID
                                       </p>
                                       <span class="red-color" id="verifyPIDSpan">
                                       </span>                                       
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Birth Year:
                                       </p>
                                       <span class="red-color" id="verifyBYearSpan">
                                       </span>                                       
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Gender
                                       </p>
                                       <span class="red-color" id="verifyGenderSpan">
                                       </span>
                                    </div>                                    
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Ethnicity: 
                                       </p>
                                       <span class="red-color" id="verifyEthniSpan">
                                       </span>                                       
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           List:
                                       </p>
                                       <span class="red-color" id="verifyListTypeSpan">
                                       </span>
                                    </div>                                    
                              </div>
                              <div class="row">

                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Reason:
                                       </p>
                                       <span class="red-color" id="verifyReasonSpan">
                                       </span>                                       
                                    </div>

                                    <div class="col-md-6">
                                       <p class="inline-block">

                                       </p>
                                       <span class="red-color">

                                       </span>                                       
                                    </div>                                    
                              </div>  


                              <div class="row mt-4x">
                                 <div class="col-md-6">
                                    <p class="no-vmargin">
                                       Match Score
                                    </p>
                                 </div>

                                 <div class="col-md-6 text-right">
                                    <span class="green-color" id="resultPercentMain">
                                          
                                    </span>
                                 </div>
                                 
                              </div>
                              <div class="row">
                                    <div class="col-md-12">
                                                <div class="progress rounded-edge full-proogress-bar">
                                                   <div id="progressbarMainDisplay" class="progress-bar progress-bar-green rounded-edge full-proogress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                                   </div>
                                                </div>                                        
                                    </div>                                    
                              </div>                                                                                                                        
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs" id="verifyDIV">
                                 <li ><a href="#" onclick="runVerification()">VERIFY</a>
                                 </li>
                                 <li ><a href="#" data-dismiss="modal">CLOSE</a>
                                 </li>
                              </ul>
                              <ul class="nav nav-tabs" id="nextVerifyDIV" style="display:none;">
                                 <li ><a href="#" onclick="runVerification()">VERIFY</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="enlargeImageModal" role="dialog" tabindex="-1" id="enlargeImageModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
                       <div class="modal-header">
                            <div class="row"></div>
                        </div>
					<div class="modal-body" >
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                            <img id="enlargeIMG" class="user-profile-image" src=""> 
                                </div>
                            <div class="col-md-1"></div>
                        </div>
					</div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs text-center">
                                    <li><a href="#" data-dismiss="modal" onclick="jQuery('#verificationDocument').modal('show');" class="capitalize-text">Close</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	              
            <IncidentCard:MyIncidentCard id="IncidentCardControl" runat="server" />
<%--            <div aria-hidden="true" aria-labelledby="incidentDocument" role="dialog" tabindex="-1" id="incidentDocument" class="modal fade videoModal" style="display: none;">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <div class="row">
                        <div class="col-md-11">
                            <span class="circle-point-container pull-left mt-2x mr-1x"><span id="incidentheaderImageClass" class="circle-point circle-point-orange"></span></span>
                            <h4 class="modal-title capitalize-text" id="incidentNameHeader"></h4>
                        </div>
                        <div class="col-md-1">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>                      
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <p>Created by: <span id="usernameSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Time: <span id="timeSpan"></span></p> 
                        </div>  
                        <div class="col-md-4">
                            <p>Type: <span id="incidenttypeSpan"></span></p>
                        </div>                          
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <p>Status: <span id="incidentstatusSpan"></span></p>
                        </div>  
                        <div class="col-md-4">
                            <p>Location: <span id="incidentlocSpan"></span></p>
                        </div>                          
                      </div>                      
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" id="incidentdemo3-tabs">
                                            <li class="active" id="incidentliInfo"><a href="#incidentinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>  
                                            <li id="incidentliActi"><a href="#incidentactivity-tab" onclick="tracebackOn('incident')" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="incidentliAtta"><a href="#incidentattachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>                                           
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tab-content">
                                        <div class="tab-pane fade active in" id="incidentinfo-tab">
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Received by:</b> <span class="red-color" id="receivedBySpan"></span></p>                                              
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Phone number:</b> <span class="red-color" id="phonenumberSpan"></span></p>                                                
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p><b>Email:</b> <span class="red-color" id="emailSpan"></span></p>                                             
                                                </div>
                                            </div>  
                                            <div class="row mb-2x">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Description:</b></p>
                                                    <p id="incidentdescriptionSpan"></p> 
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="red-color"><b>Instruction:</b></p>
                                                    <p id="incidentinstructionSpan"></p>
                                                </div>
                                            </div>  
                                            <div id="escalateionSpanDiv" class="row" style="display:none">  
                                                <div class="col-md-12">
                                                    <p id="escalationHead" class="red-color"><b>Escalation Notes:</b></p>
                                                    <p id="escalatedSpan"></p>
                                                </div>
                                            </div>   
                                                <div class="row" id="checklistDIV" style="display:none;">
                                                  <div class="col-md-12">
													<p class="red-color"><b>Dispatched Task : </b><b id="taskItemsnameSpan"></b></p>
                                                      <div class="row horizontal-navigation">
                                                        <div class="panel-control">
                                                                <ul class="nav nav-tabs" id="taskItemsList">

                                                                </ul>
                                                            <!-- /.nav -->
                                                        </div>
                                                    </div>
												</div>
                                                </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                        </div>
                                        </div>
                                        <div class="tab-pane fade" id="incidentactivity-tab">
                                                    <div id="incidentdivIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                                    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                      
                                        </div>
                                        <div class="tab-pane fade" id="incidentattachments-tab" onclick="startRot()">        
                                             <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="incidentattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                            </div>                      
                                        </div>    
                                        </div>                                  
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8" id="incidentdivAttachmentHolder">
                                <div class="row">
                                    <div class="horizontal-navigation">
                                        <div class="panel-control">
                                                <ul class="nav nav-tabs" id="incidentUsersToDispatchList">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade active in" id="incidentlocation-tab">
                                    <div id="incidentmap_canvasIncidentLocation" style="width:100%;height:378px;"></div>
                                    <div id="incidentdivAttachment" onclick="startRot()" class="overlapping-map-image">
                                    </div>
                                </div>
                                <div class="tab-pane fade " id="incidentAssign-user-tab">
                                        <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">MY USERS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="assignUsersTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">USER
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">ACTION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="tab-pane fade " id="incidentNext-user-tab">
                                     <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:378px;">
                                    <p class="red-color"><b>Dispatch Options:</b></p>
                                    <div class="row" <%=dispatchSurv%>>
                                       <div class="col-md-4">
                                           <div class='row nice-checkbox'>
                                               <input onclick="showViewCamDuration()" id='cameraCheck' type='checkbox' name='niceCheck'>
                                               <label for='cameraCheck'>Camera</label>
                                           </div>

                                       </div>
                                       <div class="col-md-8">
                                           <div class="row mb-1x" id="cameraselectDiv">
                                         <label class="select select-o">
                                            <select id="cameraSelect" runat="server">
                                            </select>
                                         </label>
                                               </div>

                                    </div>
                                    </div>
                                                                                                            <div id="camViewDurationDiv" style="display:none;" class="row">
                                       <div class="col-md-4">
                                           Access Duration
                                       </div>
                                       <div class="col-md-8">
                                     <div  style="margin-top:0px;margin-bottom:7px;" class="row horizontal-navigation">
                                                <a style="font-size:14px;" href="#" onclick="viewdurationselect(15)"><i id="camviewfilter1" class="fa fa-square-o"></i>15 MIN</a>
                                                |<a style="font-size:14px;" href="#" onclick="viewdurationselect(30)"><i id="camviewfilter2" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:14px;" href="#" onclick="viewdurationselect(45)"><i id="camviewfilter3" class="fa fa-square-o"></i>45 MIN</a>
									            |<a style="font-size:14px;" href="#" onclick="viewdurationselect(60)"><i id="camviewfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                |<a style="font-size:14px;" href="#" onclick="viewdurationselect(120)"><i id="camviewfilter5" class="fa fa-square-o"></i>2 HOUR</a>
                                </div>
                                    </div>
                                    </div>

                                                                        <div class="row">
                                       <div class="col-md-4">
                                                                                      <div class='row nice-checkbox' >
                                               <input id='TaskCheck' type='checkbox' name='niceCheck'>
                                               <label for='TaskCheck'>Task</label>
                                           </div>
                                       </div>
                                       <div class="col-md-8">
                                                                                      <div class="row">
                                        <label class="select select-o" >
                                            <select id="selectTaskTemplate" runat="server">
                                            </select>
                                         </label>
                                               </div>
                                    </div>
                                    </div>
                                                                    <div class="row"  style="display:none;"> 

                                               <div class="text-center">Attachment Photo</div>

                                    </div>
                                <div class="row">
                                                      <div style="height:50px;" enctype="multipart/form-data" id="dz-upload" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                                        <div class="dz-message">
                                                          <h1>BROWSE</h1>
                                                        </div>
                                                      </div>
                                         </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="Instructions" id="selectinstructionTextarea" class="form-control" rows="6"></textarea>
                                    </div>
                                </div>       
                                                                                                                                 <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>                                                                                  
                                        </div>                       
                                </div>
                        </div>
                            <div class="row">
                                                        <div class="col-md-4">

                            </div>
                                    <div class="col-md-8" style="display:none;border-left:0px;" id="incidentAudioDIV"> 
                                        <audio id="incidentAudio" style="width: 100%;" width="100%" controls><source id="incidentAudioSrc" type="audio/mpeg"/></audio>
                                    </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row" id="rowtracebackUser" style="display:none;">
                        <div class="col-md-4">
                            </div>
                               <div class="col-md-8" style="text-align:left;margin-top:-25px;">
 <a style="font-size:16px;" href="#" id="incidentplaypausefilter" onclick="incidentplaypauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1,'incident')"><i id="filter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5,'incident')"><i id="filter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30,'incident')"><i id="filter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60,'incident')"><i id="filter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                                    User:
							        <div style="margin-top:10px;" class="panel-control" id="tracebackUserFilter">
							        </div>
                            </div>
                        </div>
                        <div class="row" id="incidentpdfloadingAccept" style="padding-bottom:10px;display:none;">
                            <div class="col-md-5">

                            </div>
                            <div class="col-md-5" style="margin-bottom:-50px">
                                <p class="red-color text-center" style="font-size:12px"><b>GENERATING REPORT</b></p>
                                                                            <div id="incidentmyProgress">
                                              <div id="incidentmyBar"></div>
                                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                 <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs" id="incidentinitialOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">UNHANDLE</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="handledClick()">HANDLE</a>
                                    </li>
                                    <li <%=escalatedView%>><a href="#" class="capitalize-text" onclick="escalateOptionClick()">ESCALATE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="escalateOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="escalateClick()">ESCALATE</a>
                                    </li>
                                    <li>

                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="dispatchOptionsDiv" style="display:none;">
                                    <li><a href="#"   onclick="IncidentStateChange('Release')">RELEASE</a>
                                    </li>
                                    <li id="disResolveLi"><a href="#" class="capitalize-text" onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="incidenthandleOptionsDiv" style="display:none;">
                                    <li id="parkLi"><a href="#"  onclick="IncidentStateChange('Park')" >PARK</a>
                                    </li>
                                    <li><a  class="capitalize-text" data-target="#incidentAssign-user-tab" data-toggle="tab" onclick="nextLiClick()">DISPATCH FROM USERS</a>
                                    </li>
                                    <li><a  class="capitalize-text" data-target="#location-tab" data-toggle="tab" onclick="nextMapLiClick()">DISPATCH FROM MAP</a>
                                    </li>
                                    <li id="nextLi" style="display:none"><a  class="capitalize-text" data-target="#incidentNext-user-tab" onclick="finishLiClick()" data-toggle="tab" >NEXT</a>
                                    </li>
                                    <li id="finishLi" style="display:none"><a  class="capitalize-text" onclick="dispatchIncident()">DISPATCH</a>
                                    </li>
                                    <li id="resolveLi"><a onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="completedOptionsDiv2" style="display:none;">
                                    <li><a href="#"   onclick="IncidentStateChange('Reject')">REJECT</a>
                                    </li>
                                    <li id="compResolveLi2"><a href="#" class="capitalize-text"  onclick="IncidentStateChange('Resolve')">RESOLVE</a>
                                    </li>
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="completedOptionsDiv" style="display:none;">
                                    <li><a href="#"    onclick="IncidentStateChange('Reject')">REJECT</a>
                                    </li>
                                    <li id="compResolveLi"><a href="#" class="capitalize-text"  onclick="IncidentStateChange('Resolve')">RESOLVE</a>
                                    </li>
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="incidentrejectOptionsDiv" style="display:none;">
                                    <li style="display:none;"><a href="#"  onclick="IncidentStateChange('Reject')">SAVE</a>
                                    </li>
                                    <li><a href="#" onclick="handledClick()">DISPATCH</a>
                                    </li>
                                    <li><a href="#" onclick="redispatch()">RE-DISPATCH</a>
                                    </li>
                                    <li style="display:none;" id="rejResolveLi"><a href="#" class="capitalize-text" onclick="resolveClick()">RESOLVE</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="resolvedDiv" style="display:none;">
                                    <li class="pull-right"><a href="#" class="capitalize-text " onclick="generateIncidentPDF()">PDF</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs" id="escalatedDIV" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CANCEL</a>
                                    </li>
                                    <li><a href="#" onclick="handledClick()">DISPATCH</a>
                                    </li>
                                </ul> 
                                <textarea placeholder="Rejection Notes" style="display:none;" id="incidentrejectionTextarea" class="form-control" rows="3"></textarea>
                                <textarea placeholder="Notes" style="display:none;" id="resolutionTextarea" class="form-control" rows="3"></textarea>
                                 <textarea placeholder="Escalation Notes" style="display:none;" id="escalateTextarea" class="form-control" rows="3"></textarea>
                                <!-- /.nav -->
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            </div>--%>


             <div aria-hidden="true" aria-labelledby="deleteAttachModal" role="dialog" tabindex="-1" id="deleteAttachModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#taskDocument').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#taskDocument').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachment()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <!-- //NEW STUFF-->
             <div aria-hidden="true" aria-labelledby="newProjects" role="dialog" tabindex="-1" id="newProjects" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">NEW PROJECT</h4>
                     </div>
            
                     <div class="modal-body">
                          <form role="form">
                           <div class="row">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Project Name" id="newProjectinput"/>
                              </div>
                           </div>
                               <div  class="row" style="display:<%=customersDisplay%>;">
									<div class="col-md-12">
										<label class="select select-o">
                                           <select id="customerlst" runat="server" onchange="customerOnChange(this);">
											</select>
										 </label>
									</div>
                                    <div  style="display:none;">
                                        <label class="select select-o">
                                           <select id="systemtypelst" >
                                               <option value="0">Select Type</option>
											</select>
										 </label>
                                    </div>
								</div>
                                <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">    
                                          <div class="input-group input-group-in">

                                            <input id="projectStartCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                              </div>
                                    <div class="col-md-6">
                               <div class="form-group">    
                                          <div class="input-group input-group-in">

                                            <input id="projectEndCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                              </div>
                           </div>
                                                         <div class="row" style="display:<%=projectDisplay%>">
                              <div class="col-md-4">
                                  <p class="font-bold red-color no-margin" style="margin-top:7px;margin-left:2px;">
                                                                Project Manager
                                 </p>
                              </div>
                                                             <div class="col-md-6">
                                                                 										<label class="select select-o">
                                           <select id="projectUser" runat="server">
											</select>
										 </label>
                                                             </div>
                                                             <div class="col-md-2">
                                                                 <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin"> 
                                                                    <input type="checkbox" id="MyselfCheck" name="niceCheck">
                                                                    <label for="MyselfCheck">Myself</label>
                                                                  </div><!--/nice-checkbox-->       
                                                             </div>
                           </div>
                                                     </form>   
                     </div>
             
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#"  onclick="savenewProject()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                   </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            <div aria-hidden="true" aria-labelledby="editProject" role="dialog" tabindex="-1" id="editProject" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">EDIT PROJECT</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Name" id="editProjectinput"/>
                              </div>
                               <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">    
                                          <div class="input-group input-group-in">

                                            <input id="projectEditStartCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                              </div>
                                    <div class="col-md-6">
                               <div class="form-group">    
                                          <div class="input-group input-group-in">

                                            <input id="projectEditEndCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                              </div>
                           </div>
                           </div>
                        </form> 
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#"   onclick="saveeditProject()" >SAVE</a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                </div>
               </div> 
           
             <div aria-hidden="true" aria-labelledby="projDocument" role="dialog" tabindex="-1" id="projDocument" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-lgx">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text" style="display:inline">PROJECT NAME : </h4><p class="modal-title capitalize-text" style="color:gray;display:inline" id="projNameSpan"></p>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4" style="border-right: 1px solid #bbbbbb;">
                                <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li class="active"><a href="#pinfo-tab" data-toggle="tab" class="capitalize-text" onclick="document.getElementById('projectRemarksTextarea').style.display = 'none';document.getElementById('adLi').style.display = 'none'">INFO</a>
                                            </li>
                                            <li  ><a href="#premarks-tab" data-toggle="tab" class="capitalize-text" onclick="document.getElementById('projectRemarksTextarea').style.display = 'block';document.getElementById('adLi').style.display = 'block'">REMARKS</a> 
                                            </li> 										
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                   <div class="tab-content">
										<div class="tab-pane fade active in" id="pinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:550px;">	
 	 <div class="row mb-2x">
 
                                                <div class="col-md-12">
													<p class="red-color"><b>Account Name:</b></p>
													<p id="projCustomerNameSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Project Manager:</b></p>
													<p id="projownerNameSpan"></p>
												</div>
                                                 <div class="col-md-12">
													<p class="red-color"><b>Start Date:</b></p>
													<p id="projSDateSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>End Date:</b></p>
													<p id="projEDateSpan"></p>
												</div>
                                                          <div class="col-md-12">
													<p class="red-color"><b>Project Owner:</b></p>
													<p id="projCreatedBy"></p>
												</div>
                                </div>
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div> 
										</div> 
										<div class="tab-pane fade" id="premarks-tab">
                                                    <div id="projectRemarksList"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:550px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div> 
							            </div>
							</div>
							<div class="col-md-8" style="border-left:none"> 
                                                                <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                                                                        <li id="liprg"  class="active"><a   href="#pprogress-tab" data-toggle="tab" class="capitalize-text">PROGRESS</a>
                                            </li>
                                            <li id="litlist" ><a href="#ptlist-tab" data-toggle="tab" class="capitalize-text">TASK LIST</a> 
                                            </li>
										
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                                                   <div class="tab-content">
										<div class="tab-pane fade " id="ptlist-tab">
  <div style="margin-top:10px;" data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">PROJECT TASKS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-3x">
															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg">
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>      
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker" id="projecttaskDatepicker" onchange="pickdateProjectTask()" data-show-dropdowns="true" data-single-date-picker="true">
															   </div>
															   <!-- /input-group-in -->
															</div>
													   </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="projectsTaskTable" role="grid" number-of-rows="5">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                  <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                   <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">TASK TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
						    
										</div> 
										<div class="tab-pane fade active in" id="pprogress-tab">
                                                                                                                        <div style="margin-left:60px;margin-top:140px;" class="row">
                                                                                         <div class="col-md-6">
                                                                                             <label id="dvchartjs-pie1" >
                                                    <p class="text-center">By Status</p>
<%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;By Status--%>
    <canvas id="chartjs-pie1" class="text-center" width="225" height="225"></canvas>    
                                                                                                 </label>                                     
 </div>
                                            <div class="col-md-6">
                                                <label id="dvchartjs-pie2" >
                                                    <p class="text-center">By Type</p>
                                                    <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;By Type--%>
 <canvas id="chartjs-pie2" class="text-center" width="225" height="225"></canvas>
                                                    </label>
                                            </div>
                                                </div>
                                                    
										</div> 
							            </div>

                                                              </div> 
						</div>
					</div>
					<div class="modal-footer">
                        <textarea style="display:none;" placeholder="Remarks Notes" id="projectRemarksTextarea" class="form-control" rows="3"></textarea>
						<div style="margin-top:15px;" class="row horizontal-navigation">
							<div class="panel-control"> 
								<ul class="nav nav-tabs">
									<li id="adLi" style="display:none;"><a href="#" class="capitalize-text" onclick="saveProjectRemarks()">ADD REMARKS</a>
									</li>
									<li id="addtaskprojLi" style="display:none"><a href="#" class="capitalize-text" onclick="addProjTask()">ADD TASK</a>
									</li>
								</ul> 
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            	
             <div aria-hidden="true" aria-labelledby="deleteProjectModal" role="dialog" tabindex="-1" id="deleteProjectModal" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
                            <div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal">CANCEL</a>
                                        </li>   
                                        <li><a href="#" data-dismiss="modal" onclick="deleteProject()"><i class='fa fa-trash'>DELETE</i></a>
                                        </li>   
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="deleteAttachTicketModal" role="dialog" tabindex="-1" id="deleteAttachTicketModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#ticketingViewCard').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#ticketingViewCard').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachmentTicket()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
			
		

            <input style="display:none;" id="verCaseID1" type="text"/>
            <input style="display:none;" id="verCaseID2" type="text"/>
            <input style="display:none;" id="verCaseID3" type="text"/>
            <input style="display:none;" id="verCaseID4" type="text"/>
            <input style="display:none;" id="verCaseID5" type="text"/>
            <input style="display:none;" id="verCaseName1" type="text"/>
            <input style="display:none;" id="verCaseName2" type="text"/>
            <input style="display:none;" id="verCaseName3" type="text"/>
            <input style="display:none;" id="verCaseName4" type="text"/>
            <input style="display:none;" id="verCaseName5" type="text"/>
            <input id="verifierID" style="display:none;">  
             <input style="display:none;" id="startDateCheck" type="text"/>
            <!-- ./EID-DEMO-->
                          <input style="display:none;" id="rowIncidentName" type="text"/>
            <select style="display:none;" multiple="multiple" id="sendToListBox" ></select>				  
            <select style="display:none;" multiple="multiple" id="deleteListBox" ></select>		
            <input id="rowChoiceTasks" style="display:none;">  
                        <input id="rowChoiceTasksLink" style="display:none;">  
            <input style="display:none;" id="rowLongitude" type="text"/>  
            <input style="display:none;" id="rowLatitude" type="text"/>  
            <input style="display:none;" id="previousTaskUser" type="text"/> 
            <input id="rowChoiceTaskType" style="display:none;" value=""> 
                  <input id="rowidChoiceTicket" style="display:none;" value=""> 
            
            <input id="rowChoiceCHKEdit" style="display:none;" value=""> 
            <input id="previousChkName" style="display:none;" value=""> 
            <input id="rowChoiceProject" style="display:none;" value="">   
            <input style="display:none;" id="rowidChoice" type="text"/>
            <input id="newChecklistID" style="display:none;" value="0">  
            <input id="parentChecklistID"  style="display:none;" value="0">  
            <input id="rowchoiceChecklistType" style="display:none;" value="">  
            <input id="t4newChecklistID" style="display:none;" value="">  
            <input id="t4SubChecklistID" style="display:none;" value=""> 
            <input id="t5newChecklistID" style="display:none;" value="">  
            <input id="t5SubChecklistID" style="display:none;" value="">  
            <input id="rowChoiceChecklist" style="display:none;">
            <input id="rowChoiceChecklistItem" style="display:none;">
            <input id="rowChoiceChecklistItemType" style="display:none;">
            <input style="display:none;" id="imagePostAttachment" type="text"/>
             <input style="display:none;" id="imageImportAttachment" type="text"/>
            <input id="rowidChoiceAttachment" value="0" style="display:none;">
                            <input id="ticketcontractid" style="display:none;"> 
                <input id="ticketcustomerid" style="display:none;">
            <asp:HiddenField runat="server" ID="tbincidentName2" />
            <input style="display:none;" id="cameraDuration" type="text"/>
            <input id="editChoiceChecklist" style="display:none;">
            <input id="commentsId" style="display:none;">
            <input style="display:none;" id="imagePath" type="text"/>
                        <input style="display:none;" id="mobimagePath2" type="text"/>
                        <input style="display:none;" id="tbProjectId" type="text"/>
            <input style="display:none;" id="tbsystemtypeId" type="text"/>
            <input style="display:none;" id="tbCustomerId" type="text"/>
             </section>
</asp:Content>
