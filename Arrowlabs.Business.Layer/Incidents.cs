﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class Incidents
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
         [DataMember]
        public string Description { get; set; }
         [DataMember]
         public int LocationId { get; set; }
         [DataMember]
         public string Longitude { get; set; }
         [DataMember]
         public string Latitude { get; set; }
         [DataMember]
         public int TaskId { get; set; }
         [DataMember]
         public int NotificationId { get; set; }
         [DataMember]
         public int IncidentType { get; set; }
         [DataMember]
         public string IncidentTypeName { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string StatusName { get; set; }
        [DataMember]
        public string Instructions { get; set; }
        [DataMember]
        public int CustomEventId { get; set; }
        public enum IncidentTypes
        {
            MobileIncident = 0,
            ThirdParty = 1,
            SystemCreated= 2
        }
        public enum IncidentActionStatus
        {
            Park = 0,
            Dispatch = 1,
            Engage = 2,
            Complete = 3,
            Release = 4
        }
        public static List<string> GetIncidentActionStatus()
        {
            return Enum.GetNames(typeof(IncidentActionStatus)).ToList();
        }
        public static List<string> GetIncidentTypes()
        {
            return Enum.GetNames(typeof(IncidentTypes)).ToList();
        }
        private static Incidents GetIncidentFromSqlReader(SqlDataReader reader, string dbConnection)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var notification = new Incidents();

            if (reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Status"] != DBNull.Value)
                notification.Status = Convert.ToInt32(reader["Status"].ToString());
            if (reader["CustomEventId"] != DBNull.Value)
                notification.CustomEventId = Convert.ToInt32(reader["CustomEventId"].ToString());

            if (notification.Status == (int)IncidentActionStatus.Dispatch)
                notification.StatusName = "Dispatch";
            else if (notification.Status == (int)IncidentActionStatus.Park)
                notification.StatusName = "Park";

            if (reader["IncidentType"] != DBNull.Value)
                notification.IncidentType = Convert.ToInt32(reader["IncidentType"].ToString());

            if (notification.IncidentType == (int)IncidentTypes.MobileIncident)
                notification.IncidentTypeName = "MobileIncident";
            else if (notification.IncidentType == (int)IncidentTypes.SystemCreated)
                notification.IncidentTypeName = "SystemCreated";
            else if (notification.IncidentType == (int)IncidentTypes.ThirdParty)
                notification.IncidentTypeName = "ThirdParty";

            if (reader["LocationId"] != DBNull.Value)
                notification.LocationId = Convert.ToInt32(reader["LocationId"].ToString());
            if (reader["TaskId"] != DBNull.Value)
                notification.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
            if (reader["NotificationId"] != DBNull.Value)
                notification.NotificationId = Convert.ToInt32(reader["NotificationId"].ToString());
            if (reader["Instructions"] != DBNull.Value)
                notification.Instructions = reader["Instructions"].ToString();

            if (reader["Description"] != DBNull.Value)
                notification.Description = reader["Description"].ToString();
            if (reader["ReceivedBy"] != DBNull.Value)
                notification.ReceivedBy = reader["ReceivedBy"].ToString();
            if (reader["Name"] != DBNull.Value)
                notification.Name = reader["Name"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                notification.Latitude = reader["Latitude"].ToString();
            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                notification.Longitude = reader["Longitude"].ToString();

            return notification;
        }
        public static List<Incidents> GetAllIncidents(string dbConnection)
        {
            var notifications = new List<Incidents>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllIncidents", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetIncidentFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        /// <summary>
        /// It returns all notifications
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<Incidents> GetAllIncidentsByCreator(string name, string dbConnection)
        {
            var notifications = new List<Incidents>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllIncidentsByCreator", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetIncidentFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        
        //public static bool InsertorUpdateIncident(Incidents notification, string dbConnection)
        //{
        //    if (notification != null)
        //    {
        //        using (var connection = new SqlConnection(dbConnection))
        //        {
        //            connection.Open();
        //            var cmd = new SqlCommand("InsertorUpdateIncident", connection);

        //            cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
        //            cmd.Parameters["@Id"].Value = notification.Id;

        //            cmd.Parameters.Add(new SqlParameter("@CustomEventId", SqlDbType.Int));
        //                                cmd.Parameters["@CustomEventId"].Value = notification.CustomEventId;
                    

        //            cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
        //            cmd.Parameters["@Description"].Value = notification.Description;

        //            cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
        //            cmd.Parameters["@LocationId"].Value = notification.LocationId;

        //            cmd.Parameters.Add(new SqlParameter("@IncidentType", SqlDbType.Int));
        //            cmd.Parameters["@IncidentType"].Value = notification.IncidentType;

        //            cmd.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.Int));
        //            cmd.Parameters["@NotificationId"].Value = notification.NotificationId;

        //            cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
        //            cmd.Parameters["@TaskId"].Value = notification.TaskId;

        //            cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
        //            cmd.Parameters["@Status"].Value = notification.Status;

        //            cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
        //            cmd.Parameters["@Name"].Value = notification.Name;

        //            cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
        //            cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

        //            cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
        //            cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

        //            cmd.Parameters.Add(new SqlParameter("@ReceivedBy", SqlDbType.NVarChar));
        //            cmd.Parameters["@ReceivedBy"].Value = notification.ReceivedBy;

        //            cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.NVarChar));
        //            cmd.Parameters["@Longitude"].Value = notification.Longitude;

        //            cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
        //            cmd.Parameters["@Latitude"].Value = notification.Latitude;

        //            cmd.Parameters.Add(new SqlParameter("@Instructions", SqlDbType.NVarChar));
        //            cmd.Parameters["@Instructions"].Value = notification.Instructions;


        //            cmd.CommandType = CommandType.StoredProcedure;

        //            cmd.ExecuteNonQuery();
        //        }
        //    }
        //    return true;
        //}

        public static bool InsertorUpdateIncident(Incidents notification, string dbConnection)
        {
            var id = 0;
            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateIncident", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomEventId", SqlDbType.Int));
                    cmd.Parameters["@CustomEventId"].Value = notification.CustomEventId;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = notification.Description;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = notification.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@IncidentType", SqlDbType.Int));
                    cmd.Parameters["@IncidentType"].Value = notification.IncidentType;

                    cmd.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.Int));
                    cmd.Parameters["@NotificationId"].Value = notification.NotificationId;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = notification.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = notification.Status;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = notification.Name;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ReceivedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@ReceivedBy"].Value = notification.ReceivedBy;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longitude"].Value = notification.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = notification.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Instructions", SqlDbType.NVarChar));
                    cmd.Parameters["@Instructions"].Value = notification.Instructions;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    if (notification.TaskId > 0)
                    {
                        var temptasks = UserTask.GetTaskTemplateById(notification.TaskId, dbConnection);
                        var newTasks = new UserTask();
                        newTasks.Name = temptasks.Name;
                        newTasks.Description = temptasks.Description;

                        var notificationInfo = Notification.GetNotificationsById(notification.NotificationId, dbConnection);
                        if (notification != null)
                        {
                            newTasks.AssigneeName = notificationInfo.AssigneeName;
                            newTasks.AssigneeId = notificationInfo.AssigneeId;//Convert.ToInt32(tbUserID.Value);
                            newTasks.AssigneeType = notificationInfo.AssigneeType;

                            newTasks.CreateDate = notification.CreatedDate;
                            newTasks.CreatedBy = notification.CreatedBy;
                            newTasks.ManagerName = notification.CreatedBy;
                            var managerinfo = Users.GetUserByName(notification.CreatedBy, dbConnection);
                            newTasks.ManagerId = managerinfo.ID;

                            newTasks.IsDeleted = false;
                            newTasks.UpdatedDate = notification.CreatedDate;
                            newTasks.Longitude = Convert.ToDouble(notification.Longitude);
                            newTasks.Latitude = Convert.ToDouble(notification.Longitude);
                            newTasks.StartDate = notification.CreatedDate.Value.Date;
                            newTasks.EndDate = notification.CreatedDate.Value.Date;
                            newTasks.Priority = temptasks.Priority;
                            newTasks.TemplateCheckListId = temptasks.TemplateCheckListId;
                            
                            var taskId1 = UserTask.InsertorUpdateTask(newTasks, dbConnection);
                            var allTemplateCheckListItems = TemplateCheckListItem.GetAllTemplateCheckListItems(dbConnection);
                            if (allTemplateCheckListItems != null && allTemplateCheckListItems.Count > 0)
                            {
                                var selectedTemplateCheckListItems = allTemplateCheckListItems.Where(i => i.ParentCheckListId == newTasks.TemplateCheckListId).ToList();
                                if (selectedTemplateCheckListItems.Count > 0)
                                {
                                    foreach (var templateCheckListItem in selectedTemplateCheckListItems)
                                    {
                                        var taskCheckList = new TaskCheckList();
                                        taskCheckList.IsChecked = templateCheckListItem.IsChecked;
                                        taskCheckList.TaskId = taskId1;
                                        taskCheckList.UpdatedDate = DateTime.Now;
                                        taskCheckList.CreatedDate = DateTime.Now;
                                        taskCheckList.CreatedBy = Environment.UserName;
                                        taskCheckList.TemplateCheckListItemId = templateCheckListItem.Id;
                 
                                        TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckList, dbConnection);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
        public static Incidents GetIncidentsById(int Id, string dbConnection)
        {
            Incidents notification = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetIncidentsById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = Id;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    notification = GetIncidentFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return notification;
        }

        public static bool DeleteIncidentById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteIncidentById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        
    }
}
