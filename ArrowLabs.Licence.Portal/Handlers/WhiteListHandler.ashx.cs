﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Configuration;

namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for WhiteListHandler
    /// </summary>
    public class WhiteListHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string savedFileName = string.Empty;
            try
            {
                context.Response.ContentType = "text/plain";
                context.Response.Expires = -1;

                foreach (string file in context.Request.Files)
                {
                    HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
                    string FileName = string.Empty;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        string[] files = hpf.FileName.Split(new char[] { '\\' });
                        FileName = files[files.Length - 1];
                    }
                    else
                    {
                        FileName = hpf.FileName;
                    }
                    if (hpf.ContentLength == 0)
                        continue;

                    savedFileName = context.Server.MapPath("~/Uploads/" + FileName);
                    using (System.IO.StreamWriter filez = new System.IO.StreamWriter(@"C:\WriteLine.txt", true))
                    {
                        filez.WriteLine(savedFileName);
                    }
                    hpf.SaveAs(savedFileName);


                    var whiteList = File.ReadLines(savedFileName).Skip(1)
                        .Select(ParseWhiteListFromLine);
                        var list = whiteList.ToList<string>();
                        foreach (var whiteItem in list)
                        {
                            var whiteClass = new Arrowlabs.Business.Layer.WhiteList();
                            whiteClass.MACAddress = whiteItem;
                            whiteClass.CreatedBy = file;
                            whiteClass.CreatedDate = DateTime.Now;
                            whiteClass.UpdatedBy = file;
                            whiteClass.UpdatedDate = DateTime.Now;
                            Arrowlabs.Business.Layer.WhiteList.InsertOrUpdateWhiteList(whiteClass, dbConnection);
                        }


                    context.Response.Write(FileName);
                    context.Response.StatusCode = 200;
                }
            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter file2 = new System.IO.StreamWriter(@"C:\WriteLine.txt", true))
                {
                    file2.WriteLine(savedFileName);
                    file2.WriteLine(ex);
                }
                context.Response.StatusCode = 400;
            }
        }
        static string dbConnection = WebConfigurationManager.ConnectionStrings["ArrowLabsPortal"].ConnectionString;
        private string ParseWhiteListFromLine(string line)
        {
            return line;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}