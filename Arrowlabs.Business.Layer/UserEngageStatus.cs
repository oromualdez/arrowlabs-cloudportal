﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class UserEngageStatus
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int Engage { get; set; }
        [DataMember]
        public string EngageIn { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int EventId { get; set; }
    }
}
