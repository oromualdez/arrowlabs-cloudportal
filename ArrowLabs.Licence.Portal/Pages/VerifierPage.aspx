﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VerifierPage.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.VerifierPage" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <script src="/scripts/jquery.validate.js"></script>
      <script src="/scripts/dropzone.js"></script>
    <script src="/scripts/fileinput.js"></script>
    <!--Add script to update the page and send messages.-->
    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });
        var loggedinUsername = '<%=senderName2%>';
        var infoWindowLocation;
        var myMarkersLocation = new Array();
        var rotate_factor = 0;
        var rotated = false;
        function startRot() {
            rotated = false;
            rotate_factor = 0;
        }
        function loadMe(e) {
            var newImageWidth = e.width;
            var newImageHeight = e.height;
            var canvasWidth = 400;
            var canvasHeight = 400;
            var imageAspectRatio = e.width / e.height;
            var canvasAspectRatio = canvasWidth / canvasHeight;
            if (imageAspectRatio < canvasAspectRatio) {
                newImageHeight = canvasHeight;
                newImageWidth = e.width * (newImageHeight / e.height);
            }
            else if (imageAspectRatio > canvasAspectRatio) {
                newImageWidth = canvasWidth
                newImageHeight = e.height * (newImageWidth / e.width);
            }
            var margins;
            if (newImageWidth == 400) {
                jQuery(e).css({
                    'width': newImageWidth,
                    'height': newImageHeight,
                    'margin-top': '10%'
                });
            }
            else {
                jQuery(e).css({
                    'width': newImageWidth,
                    'height': newImageHeight
                });
            }
        }
        function rotateMe(e) {
            try {
                rotate_factor += 1;
                var rotate_angle = (90 * rotate_factor) % 360;
                if (rotated) {

                    if (e.width == 400) {
                        jQuery(e).css({
                            'margin-top': '10%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else {
                        jQuery(e).css({
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    rotated = false;
                }
                else {
                    if (e.width == 400 && e.height == 300) {
                        jQuery(e).css({
                            'margin-top': '10%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else if (e.width == 400 && e.height < 300) {
                        jQuery(e).css({
                            'margin-top': '20%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else {
                        jQuery(e).css({
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    rotated = true;
                }
            }
            catch (err) {
                alert(err);
            }
        }

        var mapLocation;
        var myMarkersIncidentLocation = new Array();
        var infoVerWindowLocation;
        var myVerMarkersLocation = new Array();
        var mapVerLocation;
        function getIncidentLocationMarkers(obj) {


            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;
            var firstRed = false;
            var Liverpool;
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].State == "PURPLE") {
                    Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                }
                else if (obj[i].State == "RED") {
                    if (!firstRed) {
                        Liverpool = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        firstRed = true;
                    }
                }
            }
            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 10,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapIncidentLocation = new google.maps.Map(document.getElementById("map_canvasIncidentLocation"), mapOptions);
            var poligonCoords = [];
            var first = true;
            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '"  onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>'
                //'<div id="content">' +'Name:' + obj[i].Username +'<br/><input class="specialTaskbutton tasksfont"  type="button" id="' + obj[i].Id + obj[i].Username + '" style="width:100px;" value="ADD" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)" />''</div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);


                if (obj[i].State == "YELLOW") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('../Images/markerIdle.png')
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    //createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "GREEN") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('../Images/markerOnline.png')
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    //createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "BLUE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('../Images/freeclient.png')
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    ///createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "OFFUSER") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('../Images/markerOffline.png')
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    //createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "OFFCLIENT") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('../Images/offlineclient.png')
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    //createInfoWindowIncidentLocation(marker, contentString);
                }
                else if (obj[i].State == "PURPLE") {
                    var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                    marker.setIcon('../Images/marker.png')
                    contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                    myMarkersIncidentLocation[obj[i].Username] = marker;
                    //createInfoWindowIncidentLocation(marker, contentString);
                    document.getElementById("rowIncidentName").text = obj[i].Username;
                }
                else if (obj[i].State == "RED") {
                    if (first) {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('../Images/marker.png');
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        //createInfoWindowIncidentLocation(marker, contentString);
                        document.getElementById("rowIncidentName").text = obj[i].Username;
                        first = false;
                    }
                    var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    poligonCoords.push(point);
                }

            }
            if (poligonCoords.length > 0) {
                updatepoligon = new google.maps.Polyline({
                    path: poligonCoords,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                updatepoligon.setMap(mapIncidentLocation);
            }
        }
        //User-profile
        function changePassword() {
            try {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;
                if (newPw == confPw && newPw != "" && confPw != "") {
                    jQuery.ajax({
                        type: "POST",
                        url: "VerifierPage.aspx/changePW",
                        data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d != "LOGOUT") {
                                jQuery('#changePasswordModal').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                jQuery('#successfulDispatch').modal('show');
                                document.getElementById("newPwInput").value = "";
                                document.getElementById("confirmPwInput").value = "";
                                document.getElementById("oldPwInput").value = confPw;
                            }
                            else {
                                document.getElementById('<%= logoutbtn.ClientID %>').click();
                            }

                        }
                    });
                }
                else {
                    showAlert("Kindly match new password with confirm password.")
                }
            }
            catch (ex)
            { showAlert('Error 60: Problem loading page element.-' + ex) }
        }
        function processANPRTicket() {

        }
        function editUnlock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
            document.getElementById("editProfileA").style.display = "none";
            document.getElementById("saveProfileA").style.display = "block";
            document.getElementById("profileEmailAddDIV").style.display = "none";
            document.getElementById("profileEmailAddEditDIV").style.display = "block";
            document.getElementById("userFullnameSpanDIV").style.display = "none";
            document.getElementById("userFullnameSpanEditDIV").style.display = "block";
            if (document.getElementById('profileRoleName').innerHTML == "User") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "block";
                document.getElementById("dirInfoDIV").style.display = "none";
            }
            else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "none";
                document.getElementById("dirInfoDIV").style.display = "block";
            }
        }
        function editJustLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        function editLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
            var role = document.getElementById('UserRoleSelector').value;
            var roleid = 0;
            var supervisor = 0;
            var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
            if (retVal > 0) {
                assignUserProfileData();
            }
            else {
                alert('Error 62: Problem occured while trying to get user profile.');
            }

        }
        function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
            var output = 0;
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/addUserProfile",
                data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    output = data.d;
                }
            });
            return output;
        }
        function assignUserProfileData() {
            try {
                jQuery.ajax({
                    type: "POST",
                    url: "VerifierPage.aspx/getUserProfileData",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        try {
                            document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                            document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                            document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                            document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                            document.getElementById('profileLastLocation').innerHTML = data.d[4];
                            document.getElementById('profileRoleName').innerHTML = data.d[5];
                            document.getElementById('profileManagerName').innerHTML = data.d[6];
                            document.getElementById('userStatusSpan').innerHTML = data.d[8];
                            var el = document.getElementById('userStatusIconSpan');
                            if (el) {
                                el.className = data.d[9];
                            }
                            document.getElementById('userprofileImgSrc').src = data.d[10];
                            document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                            document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                            document.getElementById('userFirstnameSpan').value = data.d[13];
                            document.getElementById('userLastnameSpan').value = data.d[14];
                            document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                            document.getElementById('profileEmailAddEdit').value = data.d[3];

                            document.getElementById('oldPwInput').value = data.d[16];

                            document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                            document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                            document.getElementById('profileGender').innerHTML = data.d[20];
                        }
                        catch (err) { alert(err) }
                    }
                });
            }
            catch (err)
            { alert('Error 60: Problem loading page element.-' + err) }
        }
        function forceLogout() {
            document.getElementById('<%= closingbtn.ClientID %>').click();
        }
        function deleteANPRChoice() {
            var id = document.getElementById("rowidChoiceANPR").text;
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/delANPRData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('nameDeleted').innerHTML = "ANPR"
                        jQuery('#entryDeleted').modal('show');
                    }
                    else {
                        showAlert(data.d);
                    }
                }
            });
        }
        function insertAttachmentTabData(id) {
            jQuery('#attachments-info-tab div').html('');

            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getAttachmentDataTab",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById("attachments-info-tab").innerHTML = data.d;
                }
            });
        }

        function addrowtoEnrollANPR() {
            jQuery("#anprenrollmentTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getTableANPREnrollment",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#anprenrollmentTable tbody").append(data.d[i]);
                    }
                }
            });
        }
        function addrowtoEnrolledANPR() {
            jQuery("#anprenrolledTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getTableANPREnrolled",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#anprenrolledTable tbody").append(data.d[i]);
                    }
                }
            });
        }
        var firstincidentpress = false;
        jQuery(document).ready(function () {
            try {
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    if ($(e.target).attr('href') == "#frs-tab" || $(e.target).attr('href') == "#anpr-tab")
                        localStorage.setItem('activeTabVer', $(e.target).attr('href'));
                    //alert($(e.target).attr('href')+'----------NOW Selected');
                });
                var activeTab = localStorage.getItem('activeTabVer');
                if (activeTab) {
                    //alert(activeTab +'--------Selected');
                    //$('#myTab a[href="' + activeTab + '"]').tab('show'); 
                    jQuery('a[data-toggle="tab"][href="' + activeTab + '"]').tab('show');
                }
                localStorage.removeItem("activeTabDev");
                localStorage.removeItem("activeTabInci");
                localStorage.removeItem("activeTabMessage");
                localStorage.removeItem("activeTabTask");
                localStorage.removeItem("activeTabTick");
                localStorage.removeItem("activeTabUB");
                localStorage.removeItem("activeTabLost");

            } catch (err)
            { alert(err); }
            try {
                infoWindowLocation = new google.maps.InfoWindow();
                infoVerWindowLocation = new google.maps.InfoWindow();
            }
            catch (err) { }
            addrowtoTable();
            addrowtoTable2();
            addrowtoTable3();
            //addrowtoANPR();
            //addrowtoEnrollANPR();
            //addrowtoEnrolledANPR();
            jQuery('#viewEnrollment').on('shown.bs.modal', function () {
                jQuery.ajax({
                    type: "POST",
                    url: "VerifierPage.aspx/getVerifyLocationData",
                    data: "{'id':'" + document.getElementById("rowidChoice").text + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var obj = jQuery.parseJSON(data.d)
                        getLocationMarkers(obj);
                    }
                });
            });
            jQuery('#viewDocument1').on('shown.bs.modal', function () {
                jQuery.ajax({
                    type: "POST",
                    url: "VerifierPage.aspx/getVerifyLocationData",
                    data: "{'id':'" + document.getElementById("rowidChoice").text + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var obj = jQuery.parseJSON(data.d)
                        getVerLocationMarkers(obj);
                    }
                });
            });

            jQuery('#anprViewCard').on('shown.bs.modal', function () {
                if (firstincidentpress == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "VerifierPage.aspx/getIncidentLocationData",
                        data: "{'id':'" + document.getElementById("rowidChoiceANPR").text + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var obj = jQuery.parseJSON(data.d)
                            getIncidentLocationMarkers(obj);
                        }
                    });
                    firstincidentpress = true;
                }
                else {
                    jQuery.ajax({
                        type: "POST",
                        url: "VerifierPage.aspx/getIncidentLocationData",
                        data: "{'id':'" + document.getElementById("rowidChoiceANPR").text + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var obj = jQuery.parseJSON(data.d)
                            updateIncidentMarker(obj);
                        }
                    });
                }
            });
        });
        var updatepoligon;
        var tracebackpoligon;
        function updateIncidentMarker(obj) {
            try {
                var poligonCoords = [];
                var tracepoligonCoords = [];
                var first = true;
                var first2 = true;
                var currentSubLocation;
                var secondfirst = true;
                var previousColor = "";
                var subpoligonCoords = [];
                var previousMarker = document.getElementById("rowIncidentName").text;

                if (typeof tracebackpoligon === 'undefined') {
                    // your code here.
                }
                else {
                    tracebackpoligon.setMap(null);
                }
                if (typeof previousMarker === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersIncidentLocation[previousMarker].setMap(null);
                    if (typeof updatepoligon === 'undefined') {
                        // your code here.
                    }
                    else {
                        updatepoligon.setMap(null);
                    }
                }
                var tbcounter = 0;
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-mobile pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" id="' + obj[i].Id + obj[i].Username + '"  onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-plus red-color"></i>ADD</a></div>'

                    //'<div id="content">' +'Name:' + obj[i].Username +'<br/><input class="specialTaskbutton tasksfont"  type="button" id="' + obj[i].Id + obj[i].Username + '" style="width:100px;" value="ADD" onclick="userdispatchChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)" />''</div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                    mapIncidentLocation.setCenter(new google.maps.LatLng(obj[i].Lat, obj[i].Long));
                    mapIncidentLocation.setZoom(9);
                    if (obj[i].State == "YELLOW") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('../Images/markerIdle.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        //createInfoWindowIncidentLocation(marker, contentString);
                    }
                    else if (obj[i].State == "GREEN") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('../Images/markerOnline.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        //createInfoWindowIncidentLocation(marker, contentString);
                    }
                    else if (obj[i].State == "BLUE") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('../Images/freeclient.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        //createInfoWindowIncidentLocation(marker, contentString);
                    }
                    else if (obj[i].State == "OFFUSER") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('../Images/markerOffline.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        //createInfoWindowIncidentLocation(marker, contentString);
                    }
                    else if (obj[i].State == "OFFCLIENT") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('../Images/offlineclient.png')
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        //createInfoWindowIncidentLocation(marker, contentString);
                    }
                    else if (obj[i].State == "PURPLE") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                        marker.setIcon('../Images/marker.png')
                        contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                        myMarkersIncidentLocation[obj[i].Username] = marker;
                        //createInfoWindowIncidentLocation(marker, contentString);
                        document.getElementById("rowIncidentName").text = obj[i].Username;
                    }
                    else if (obj[i].State == "ENG") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('../Images/markerIdle.png')
                        contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                        myTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                        // createInfoWindowIncidentLocation(marker, contentString);
                    }
                    else if (obj[i].State == "COM") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username + "\n" + obj[i].LastLog });
                        marker.setIcon('../Images/markerOnline.png')
                        contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                        myTraceBackMarkers[tbcounter] = marker;
                        tbcounter++;
                        //createInfoWindowIncidentLocation(marker, contentString);
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        subpoligonCoords.push(point);
                        if (subpoligonCoords.length > 0) {
                            var subpoligon = new google.maps.Polyline({
                                path: subpoligonCoords,
                                geodesic: true,
                                strokeColor: previousColor,
                                strokeOpacity: 1.0,
                                strokeWeight: 7
                            });
                            subpoligon.setMap(mapIncidentLocation);
                            myTraceBackMarkers[tbcounter] = subpoligon;
                            tbcounter++;
                        }
                    }
                    else if (obj[i].State == "RED") {
                        if (first) {
                            contentString = '<p class="inline-block red-color" style="color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p>';
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                            marker.setIcon('../Images/marker.png');
                            myMarkersIncidentLocation[obj[i].Username] = marker;
                            //createInfoWindowIncidentLocation(marker, contentString);
                            document.getElementById("rowIncidentName").text = obj[i].Username;
                            first = false;
                        }
                        var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                        poligonCoords.push(point);
                    }
                    else {
                        if (secondfirst) {
                            currentSubLocation = obj[i].State;
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].LastLog });
                            marker.setIcon('../Images/bluesmall.png');
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                            previousColor = obj[i].Logs;
                            myMarkersIncidentLocation[obj[i].Username] = marker;
                            //createInfoWindowIncidentLocation(marker, contentString);
                            secondfirst = false;
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            subpoligonCoords.push(point);

                        }
                        else {
                            if (currentSubLocation == obj[i].State) {
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                subpoligonCoords.push(point);
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].LastLog });
                                marker.setIcon('../Images/bluesmall.png');
                                myTraceBackMarkers[tbcounter] = marker;
                                tbcounter++;
                            }
                            else {
                                if (subpoligonCoords.length > 0) {
                                    var subpoligon = new google.maps.Polyline({
                                        path: subpoligonCoords,
                                        geodesic: true,
                                        strokeColor: previousColor,
                                        strokeOpacity: 1.0,
                                        strokeWeight: 7
                                    });
                                    subpoligon.setMap(mapIncidentLocation);
                                    myTraceBackMarkers[tbcounter] = subpoligon;
                                    tbcounter++;
                                }
                                subpoligonCoords = [];
                                currentSubLocation = obj[i].State;
                                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].LastLog });
                                marker.setIcon('../Images/bluesmall.png');
                                myTraceBackMarkers[tbcounter] = marker;
                                tbcounter++;
                                previousColor = obj[i].Logs;
                                myMarkersIncidentLocation[obj[i].Username] = marker;
                                //createInfoWindowIncidentLocation(marker, contentString);
                                var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                                subpoligonCoords.push(point);
                            }
                        }
                    }
                }
                if (poligonCoords.length > 0) {
                    updatepoligon = new google.maps.Polyline({
                        path: poligonCoords,
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });
                    updatepoligon.setMap(mapIncidentLocation);
                }
                if (tracepoligonCoords.length > 0) {
                    tracebackpoligon = new google.maps.Polyline({
                        path: tracepoligonCoords,
                        geodesic: true,
                        strokeColor: '#1b93c0',
                        strokeOpacity: 1.0,
                        strokeWeight: 7
                    });
                    tracebackpoligon.setMap(mapIncidentLocation);
                }
                if (subpoligonCoords.length > 0) {
                    var subpoligon = new google.maps.Polyline({
                        path: subpoligonCoords,
                        geodesic: true,
                        strokeColor: previousColor,
                        strokeOpacity: 1.0,
                        strokeWeight: 7
                    });
                    subpoligon.setMap(mapIncidentLocation);
                    myTraceBackMarkers[tbcounter] = subpoligon;
                    tbcounter++;
                }
            }
            catch (err)
            { showAlert(err); }
        }
        jQuery(function () {
            // Now that the DOM is fully loaded, create the dropzone, and setup the
            // event listeners
            var myDropzone = new Dropzone("#dz-test");
            myDropzone.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */
                file.previewElement.addEventListener("click", function () {
                    myDropzone.removeFile(file);
                    document.getElementById("imagePath").text = '';
                });
                if (typeof document.getElementById("imagePath").text === 'undefined' || document.getElementById("imagePath").text == '') {
                    if (file.type != "image/jpeg" && file.type != "image/png") {
                        showAlert("Kindly provided a JPEG or PNG Image for upload");
                        this.removeFile(file);
                    }
                    else {
                        var data = new FormData();
                        data.append(file.name, file);

                        jQuery.ajax({
                            url: "../Handlers/EnrollmentHandler.ashx",
                            type: "POST",
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                document.getElementById("imagePath").text = result.replace(/\\/g, "|");
                            },
                            error: function (err) {
                            }
                        });
                    }
                }
                else {
                    showAlert("You can only add one image at a time kindly delete previous image");
                    this.removeFile(file);
                }
            });
        })
        function rowchoiceANPR(tID) {
            document.getElementById("rowidChoiceANPR").text = tID;
            document.getElementById("liEnrollANPR").style.pointerEvents = "none";
            document.getElementById("liEnrollANPR").style.opacity = 0.6;
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getANPRData",
                data: "{'id':'" + tID + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 0) {
                        document.getElementById("plateCatSpan").innerHTML = data.d[0];
                        document.getElementById("platesourceSpan").innerHTML = data.d[1];
                        document.getElementById("plateColorSpan").innerHTML = data.d[2];
                        document.getElementById("plateCodeSpan").innerHTML = data.d[3];
                        document.getElementById("platenumberSpan").innerHTML = data.d[4];
                        document.getElementById("plateTypeSpan").innerHTML = data.d[5];
                        document.getElementById("timeSpan").innerHTML = data.d[6];
                        document.getElementById("usernameSpan").innerHTML = data.d[7];
                        document.getElementById("locSpan").innerHTML = data.d[8];
                    }
                    else {
                        showAlert("Not able to find entry.");
                    }
                }
            });
            //jQuery.ajax({
            //    type: "POST",
            //    url: "VerifierPage.aspx/getIncidentLocationData",
            //    data: "{'id':'" + tID + "'}",
            //    dataType: "json",
            //    contentType: "application/json; charset=utf-8",
            //    success: function (data) {
            //        var obj = jQuery.parseJSON(data.d)
            //        getIncidentLocationMarkers(obj);
            //    }
            //});
            var el3 = document.getElementById('location-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            oldDivContainers();
            insertAttachmentTabData(tID);
            insertAttachmentData(tID);
            insertAttachmentIcons(tID);
        }
        function rowchoiceANPREnroll(tID, imgSrc, uName) {
            document.getElementById("rowidChoiceANPR").text = tID;
            document.getElementById("anprenrollmentIMG").src = imgSrc;
            document.getElementById("tbANPRRequestBy").value = uName;
        }
        function verifyANPR() {
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/verifyANPR",
                data: "{'id':'" + document.getElementById("rowidChoiceANPR").text + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 1) {
                        document.getElementById("tbANPRPlateEnrollment").value = data.d[0];
                        //document.getElementById("anprLaneTypeEnrollment").value = data.d[1];
                        document.getElementById("anprPlateCityEnrollment").value = data.d[2];
                        document.getElementById("MainContent_anprPlateCategoryEnrollment").value = data.d[3];
                        document.getElementById("anprPlateColorEnrollment").value = data.d[4];
                        document.getElementById("anprPlateTypeEnrollment").value = data.d[5];
                        document.getElementById("MainContent_anprPlateStatusCodeEnrollment").value = data.d[6];
                        document.getElementById("anprPlatePrefixEnrollment").value = data.d[7];

                        showAlert(data.d[8]);
                        document.getElementById("liEnrollANPR").style.pointerEvents = "all";
                        document.getElementById("liEnrollANPR").style.opacity = 1;
                    }
                    else {
                        showError(data.d[0]);
                    }
                }
            });
        }
        function enrollANPR(type) {
            try {
                if (type == 'request') {
                    var plateno = document.getElementById("tbANPRPlateEnrollment").value;
                    var platelane = document.getElementById("anprLaneTypeEnrollment").value;
                    var platecity = document.getElementById("anprPlateCityEnrollment").value;
                    var platecategory = document.getElementById("MainContent_anprPlateCategoryEnrollment").value;
                    var platecolor = document.getElementById("anprPlateColorEnrollment").value;
                    var platetype = document.getElementById("anprPlateTypeEnrollment").value;
                    var platecode = document.getElementById("MainContent_anprPlateStatusCodeEnrollment").value;
                    var plateprefix = document.getElementById("anprPlatePrefixEnrollment").value;
                    if (plateno != '' && plateprefix != '') {
                        jQuery.ajax({
                            type: "POST",
                            url: "VerifierPage.aspx/enrollANPR",
                            data: "{'id':'" + document.getElementById("rowidChoiceANPR").text + "','plateno':'" + plateno + "','platelane':'" + platelane + "','platecity':'" + platecity + "','platecategory':'" + platecategory + "','platecolor':'" + platecolor + "','platetype':'" + platetype + "','platecode':'" + platecode + "','plateprefix':'" + plateprefix + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                output = data.d;
                                if (output == 'Success') {
                                    jQuery('#anprEnrollment').modal('hide');
                                    document.getElementById("successincidentScenario").innerHTML = "ANPR has successfully been enrolled.";
                                    jQuery('#successfulDispatch').modal('show');
                                }
                                else if (data.d == "LOGOUT") {
                                    document.getElementById('<%= logoutbtn.ClientID %>').click();
                                }
                                else {
                                    showAlert(output);
                                }
                            }
                        });
                    }
                    else {
                        showAlert('Please provide plate information!');
                    }

                }
            }
            catch (err) {
                showError(err);
            }
        }
        function rejectANPR() {
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/rejectANPR",
                data: "{'id':'" + document.getElementById("rowidChoiceANPR").text + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById("successincidentScenario").innerHTML = "ANPR has successfully been rejected.";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        document.getElementById('<%= logoutbtn.ClientID %>').click();
                    }
                    else { showError(data.d); }
                }
            });
        }
        function insertAttachmentIcons(id) {
            jQuery('#divAttachment div').html('');
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getAttachmentDataIcons",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById("divAttachment").innerHTML = data.d;
                }
            });
        }
        function insertAttachmentData(id) {
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getAttachmentData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var div = document.createElement('div');
                    div.className = 'tab-pane fade';
                    div.innerHTML = data.d[0];
                    div.id = 'video-0-tab';
                    document.getElementById('divAttachmentHolder').appendChild(div);
                    divArray[0] = 'video-0-tab';
                    for (var i = 1; i < data.d.length; i++) {

                        var div = document.createElement('div');
                        div.className = 'tab-pane fade';
                        div.align = 'center';
                        div.style.height = '420px';
                        div.innerHTML = data.d[i];
                        div.id = 'image-' + (i) + '-tab';
                        document.getElementById('divAttachmentHolder').appendChild(div);
                        divArray[i] = 'image-' + (i) + '-tab';

                    }
                }
            });
        }
        function oldDivContainers() {
            try {
                for (var i = 0; i < divArray.length; i++) {
                    var el = document.getElementById(divArray[i]);
                    el.parentNode.removeChild(el);
                }
                divArray = new Array();
            }
            catch (ex) {
                //alert('oldDivContainers-' + ex);
                //alert(ex);
            }
        }
        var divArray = new Array();
        function showRejectDiv() {
            document.getElementById("enrollRequestDIV").style.display = "none";
            document.getElementById("enrollRejectDIV").style.display = "block";
            document.getElementById("rejectionoteDiv").style.display = "block";
        }
        function hideRejectDiv() {
            document.getElementById("enrollRequestDIV").style.display = "block";
            document.getElementById("enrollRejectDIV").style.display = "none";
            document.getElementById("rejectionoteDiv").style.display = "none";
        }
        function enlargeDisplayImage(type) {
            if (type == "Sent") {
                document.getElementById("enlargeIMG").src = document.getElementById("veriSentIMG").src;
            }
            else {
                document.getElementById("enlargeIMG").src = document.getElementById("veriResultIMG").src;
            }
        }
        function moveImageToComparison(place) {
            document.getElementById("veriResultIMG").src = document.getElementById("veriResult" + place + "IMG").src;
            document.getElementById("resultPercentMain").innerHTML = document.getElementById("resultPercent" + place).innerHTML;
            document.getElementById("verifyHeaderSpan").innerHTML = document.getElementById("verCaseName" + place).text;
            document.getElementById("verifyCaseSpan").innerHTML = document.getElementById("verCaseID" + place).text;
            document.getElementById('progressbarMainDisplay').setAttribute("style", "width:" + document.getElementById("resultPercent" + place).innerHTML);

            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getEnrolledData",
                data: "{'id':'" + document.getElementById("verCaseID" + place).text + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById("verifyTypeSpan").innerHTML = data.d[1];
                    document.getElementById("verifyUserSpan").innerHTML = data.d[0];
                    document.getElementById("verifyReasonSpan").innerHTML = data.d[4];
                    document.getElementById("verifyPIDSpan").innerHTML = data.d[5];
                    document.getElementById("verifyBYearSpan").innerHTML = data.d[6];
                    document.getElementById("verifyGenderSpan").innerHTML = data.d[7];
                    document.getElementById("verifyEthniSpan").innerHTML = data.d[8];
                    document.getElementById("verifyListTypeSpan").innerHTML = data.d[9];
                    //document.getElementById("tbNameEnrollment").value = data.d[11];
                }
            });

            var str = document.getElementById("resultPercent" + place).innerHTML;
            str = str.substring(0, str.length - 1);
            var perColor = "green"
            if (place == 2)
                perColor = "yellow";
            else if (place > 2)
                perColor = "red";
            var element = document.getElementById('progressbarMainDisplay');
            element.className = "progress-bar progress-bar-" + perColor + " rounded-edge full-proogress-bar";
            document.getElementById("resultPercentMain").className = perColor + "-color";
        }
        function enrollVerifier(enrolled) {
            try {
                var id = document.getElementById("rowidChoice").text;
                var reason = document.getElementById("tbReasonEnrollment").value;
                var ethnicity = document.getElementById("ethnicitySelectionEnrollment").value;
                var gender = document.getElementById("genderSelectionEnrollment").value;
                var category = document.getElementById("ListTypeEnrollment").value;
                var pid = document.getElementById("tbPIDEnrollment").value;
                var name = document.getElementById("tbNameEnrollment").value;
                var yob = document.getElementById("tbBYEnrollment").value;

                if (!isEmptyOrSpaces(name)) {
                    if (!isSpecialChar(name)) {
                        var isErr = false;
                        if (!isEmptyOrSpaces(reason)) {
                            if (isSpecialChar(reason)) {
                                isErr = true;
                            }
                        }
                        else {
                            showAlert("Kindly provide reason")
                            isErr = true;
                        }
                        if (!isEmptyOrSpaces(pid)) {
                            if (isSpecialChar(pid)) {
                                isErr = true;
                            }
                        }
                        else {
                            showAlert("Kindly provide personal id")
                            isErr = true;
                        }
                        if (!isEmptyOrSpaces(pid)) {
                            if (isSpecialChar(pid)) {
                                isErr = true;
                            }
                        }
                        else {
                            showAlert("Kindly provide personal id")
                            isErr = true;
                        }
                        if (yob.length == 4) {
                            if (!isNumeric(yob)) {
                                showAlert("Kindly provide a valid birth year")
                                isErr = true;
                            }
                        }
                        else {
                            showAlert("Kindly provide a valid birth year")
                            isErr = true;
                        }
                        if (!isErr) {
                            if (category == "BlackList")
                                category = "0";
                            else
                                category = "1";
                            if (name != '') {
                                jQuery.ajax({
                                    type: "POST",
                                    url: "VerifierPage.aspx/newEnrollmentData",
                                    data: "{'id':'" + id + "','reason':'" + reason + "','ethnicity':'" + ethnicity + "','gender':'" + gender + "','category':'" + category + "','pid':'" + pid + "','name':'" + name + "','yob':'" + yob + "','enrolled':'" + enrolled + "','uname':'" + loggedinUsername + "'}",
                                    dataType: "json",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        if (data.d == "SUCCESS") {
                                            jQuery('#viewEnrollment').modal('hide');
                                            if (enrolled == "True")
                                                document.getElementById("nameSuccessEdit").innerHTML = name + " has successfully been edited.";
                                            else
                                                document.getElementById("nameSuccessEdit").innerHTML = name + " has successfully been enrolled.";;

                                            jQuery('#successEnroll').modal('show');
                                        }
                                        else {
                                            showAlert('Error 59: Problem faced when trying to enroll entry.-' + data.d);
                                        }
                                    }
                                });
                            }
                            else {
                                showAlert('Please provide name for enrollment');
                            }
                        }
                    }
                }
                else {
                    showAlert('Please provide name for enrollment');
                }
            }
            catch (err) {
                //alert(err);
            }
        }
        function newEnrollment() {
            try {

                var reason = document.getElementById("tbNEWReasonEnrollment").value;
                var type = document.getElementById("newEnrollmentTypeSelect").value;
                var ethnicity = document.getElementById("newEthnicitySelectionEnrollment").value;
                var gender = document.getElementById("newGenderSelectionEnrollment").value;
                var category = document.getElementById("newListTypeEnrollment").value;
                var pid = document.getElementById("tbNEWPIDEnrollment").value;
                var yob = document.getElementById("tbNEWBYEnrollment").value;
                var imgP = document.getElementById("imagePath").text;

                var name = document.getElementById("tbNEWNameEnrollment").value;
                if (!isEmptyOrSpaces(name)) {
                    if (!isSpecialChar(name)) {
                        var isErr = false;
                        if (!isEmptyOrSpaces(reason)) {
                            if (isSpecialChar(reason)) {
                                isErr = true;
                            }
                        }
                        else {
                            showAlert("Kindly provide reason")
                            isErr = true;
                        }
                        if (!isEmptyOrSpaces(pid)) {
                            if (isSpecialChar(pid)) {
                                isErr = true;
                            }
                        }
                        else {
                            showAlert("Kindly provide personal id")
                            isErr = true;
                        }
                        if (!isEmptyOrSpaces(pid)) {
                            if (isSpecialChar(pid)) {
                                isErr = true;
                            }
                        }
                        else {
                            showAlert("Kindly provide personal id")
                            isErr = true;
                        }
                        if (yob.length == 4) {
                            if (!isNumeric(yob)) {
                                showAlert("Kindly provide a valid birth year")
                                isErr = true;
                            }
                        }
                        else {
                            showAlert("Kindly provide a valid birth year")
                            isErr = true;
                        }
                        if (!isErr) {
                            if (category == "BlackList")
                                category = "0";
                            else {
                                category = "1";
                            }
                            if (typeof imgP === 'undefined') {
                                showAlert('Please select image to enroll!');
                            }
                            else {
                                if (name != '') {
                                    var res = imgP.replace(/\\/g, "|")
                                    jQuery.ajax({
                                        type: "POST",
                                        url: "VerifierPage.aspx/newEnrollment",
                                        data: "{'imgPath':'" + res + "','reason':'" + reason + "','ethnicity':'" + ethnicity + "','gender':'" + gender + "','category':'" + category + "','pid':'" + pid + "','name':'" + name + "','yob':'" + yob + "','type':'" + type + "','uname':'" + loggedinUsername + "'}",
                                        dataType: "json",
                                        contentType: "application/json; charset=utf-8",
                                        success: function (data) {
                                            if (data.d == "SUCCESS") {
                                                jQuery('#newUser').modal('hide');
                                                document.getElementById("nameSuccessEdit").innerHTML = name + " has successfully been enrolled.";
                                                jQuery('#successEnroll').modal('show');
                                            }
                                            else {
                                                showAlert('Error 73: Problem faced when trying to make new enrollment.-' + data.d);
                                            }
                                        }
                                    });
                                }
                                else {
                                    showAlert('Please provide name for enrollment');
                                }
                            }
                        }
                    }
                }
                else {
                    showAlert('Please provide name for enrollment');
                }
            }
            catch (err)
            { showAlert('Error 74: Problem faced when trying to get data for enrollment.-' + err); }
        }
        function getVerLocationMarkers(obj) {
            
            sourceLat = obj[0].Lat;
            sourceLon = obj[0].Long;

                locationAllowed = true;
                google.maps.visualRefresh = true;

                var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 12,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                mapVerLocation = new google.maps.Map(document.getElementById("map_verLocation"), mapOptions);
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div id="content">' + obj[i].Username +
                    '<br/></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    var marker = new google.maps.Marker({ position: myLatlng, map: mapVerLocation, title: obj[i].Username });
                    marker.setIcon('../Images/marker.png');
                    myVerMarkersLocation[obj[i].Username] = marker;
                    createverInfoWindowLocation(marker, contentString);
                }
            
        }
        function createverInfoWindowLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoVerWindowLocation.setContent(popupContent);
                infoVerWindowLocation.open(mapVerLocation, this);
            });
        }
        function getLocationMarkers(obj) {
           
            sourceLat = obj[0].Lat;
            sourceLon = obj[0].Long;

                locationAllowed = true;
                google.maps.visualRefresh = true;

                var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 12,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                mapLocation = new google.maps.Map(document.getElementById("map_Location"), mapOptions);
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div id="content">' + obj[i].Username +
                    '<br/></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                    marker.setIcon('../Images/marker.png');
                    myMarkersLocation[obj[i].Username] = marker;
                    createInfoWindowLocation(marker, contentString);
                }
        }
        function createInfoWindowLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindowLocation.setContent(popupContent);
                infoWindowLocation.open(mapLocation, this);
            });
        }
        function runVerification() {
            var name = document.getElementById("rowidChoice").text;
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/runVerifyData",
                data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 0) {

                        document.getElementById("verifyUserSpan").innerHTML = data.d[0];
                        document.getElementById("verifyTypeSpan").innerHTML = data.d[1];
                        document.getElementById("verifyCaseSpan").innerHTML = data.d[2];
                        document.getElementById("verifyTimeSpan").innerHTML = data.d[3];
                        document.getElementById("verifyLocSpan").innerHTML = data.d[4];
                        document.getElementById("verifyReasonSpan").innerHTML = data.d[5];
                        document.getElementById("verifyBYearSpan").innerHTML = data.d[6];
                        document.getElementById("verifyEthniSpan").innerHTML = data.d[7];
                        document.getElementById("verifyPIDSpan").innerHTML = data.d[8];
                        document.getElementById("verifyGenderSpan").innerHTML = data.d[9];
                        document.getElementById("verifyListTypeSpan").innerHTML = data.d[10];
                        document.getElementById("veriResultIMG").src = data.d[12];
                        document.getElementById("veriSentIMG").src = data.d[11];
                        if (data.d.length > 14) {
                            document.getElementById("verifyHeaderSpan").innerHTML = data.d[23];
                            document.getElementById("veriResultIMG").src = data.d[13];
                            document.getElementById("veriResult1IMG").src = data.d[13];
                            document.getElementById("veriResult2IMG").src = data.d[15];
                            document.getElementById("veriResult3IMG").src = data.d[17];
                            document.getElementById("veriResult4IMG").src = data.d[19];
                            document.getElementById("veriResult5IMG").src = data.d[21];

                            document.getElementById("resultPercent1").innerHTML = data.d[14] + "%";
                            document.getElementById("resultPercent2").innerHTML = data.d[16] + "%";
                            document.getElementById("resultPercent3").innerHTML = data.d[18] + "%";
                            document.getElementById("resultPercent4").innerHTML = data.d[20] + "%";
                            document.getElementById("resultPercent5").innerHTML = data.d[22] + "%";
                            document.getElementById("resultPercentMain").innerHTML = data.d[14] + "%";
                            document.getElementById('progressbarMainDisplay').setAttribute("style", "width:" + data.d[14] + "%");

                            document.getElementById("verCaseName1").text = data.d[24];
                            document.getElementById("verCaseName2").text = data.d[26];
                            document.getElementById("verCaseName3").text = data.d[28];
                            document.getElementById("verCaseName4").text = data.d[30];
                            document.getElementById("verCaseName5").text = data.d[32];

                            document.getElementById("verifyCaseSpan").innerHTML = data.d[25];
                            document.getElementById("verCaseID1").text = data.d[25];
                            document.getElementById("verCaseID2").text = data.d[27];
                            document.getElementById("verCaseID3").text = data.d[29];
                            document.getElementById("verCaseID4").text = data.d[31];
                            document.getElementById("verCaseID5").text = data.d[33];

                        }
                        else {
                            jQuery('#verificationFail').modal('show');
                            document.getElementById("veriResult1IMG").src = "";
                            document.getElementById("veriResult2IMG").src = "";
                            document.getElementById("veriResult3IMG").src = "";
                            document.getElementById("veriResult4IMG").src = "";
                            document.getElementById("veriResult5IMG").src = "";

                            document.getElementById("resultPercent1").innerHTML = "";
                            document.getElementById("resultPercent2").innerHTML = "";
                            document.getElementById("resultPercent3").innerHTML = "";
                            document.getElementById("resultPercent4").innerHTML = "";
                            document.getElementById("resultPercent5").innerHTML = "";
                            document.getElementById("resultPercentMain").innerHTML = "";
                            document.getElementById('progressbarMainDisplay').setAttribute("style", "width:1%");
                        }
                        showAlert("Verification was successful");
                    }
                    else {
                        showAlert("Verification failed.");
                    }
                }
            });

        }
        function rowchoice(name, type) {
            document.getElementById("rowidChoice").text = name;
            if (type == "VERI") {
                assignVerifierRequestData(name);
            }
            else if (type == "EN") {
                assignEnrollRequestData(name);
            }
            else if (type == "END") {
                assignEnrolledData(name);
            }
            else {
                document.getElementById("todeleteEn").innerHTML = type;
                document.getElementById("todeleteVer").innerHTML = type;
                document.getElementById("nameDeleted").innerHTML = type;
            }
        }
        function assignEnrollRequestData(id) {
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getVerifierRequestData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById("enrollmentHeader").innerHTML = "ENROLLMENT REQUEST";
                    document.getElementById("enrollmentTypeSelect").value = data.d[1];
                    document.getElementById("tbUserEnrollment").value = data.d[0];
                    document.getElementById("tbReasonEnrollment").value = data.d[5];
                    document.getElementById("enrollmentIMG").src = data.d[11];

                    document.getElementById("enrollRequestDIV").style.display = "block";
                    document.getElementById("enrolledDIV").style.display = "none";
                    document.getElementById("map_Location").style.display = "block";
                    document.getElementById("tbNameEnrollment").value = "";//data.d[12];
                }
            });
        }
        function assignEnrolledData(id) {
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getEnrolledData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById("enrollmentHeader").innerHTML = "ENROLLED";
                    document.getElementById("enrollmentTypeSelect").value = data.d[1];
                    document.getElementById("tbUserEnrollment").value = data.d[0];
                    document.getElementById("tbReasonEnrollment").value = data.d[4];
                    document.getElementById("tbPIDEnrollment").value = data.d[5];
                    document.getElementById("tbBYEnrollment").value = data.d[6];
                    document.getElementById("genderSelectionEnrollment").value = data.d[7];
                    document.getElementById("ethnicitySelectionEnrollment").value = data.d[8];
                    document.getElementById("ListTypeEnrollment").value = data.d[9];
                    document.getElementById("enrollmentIMG").src = data.d[10];

                    document.getElementById("enrollRequestDIV").style.display = "none";
                    document.getElementById("enrolledDIV").style.display = "block";
                    document.getElementById("map_Location").style.display = "none";
                    document.getElementById("tbNameEnrollment").value = data.d[11];

                }
            });
        }
        function rejectEnrollment() {
            var id = document.getElementById("rowidChoice").text;
            var notes = document.getElementById("tbRejectionNote").value;
            if (notes != "") {
                jQuery.ajax({
                    type: "POST",
                    url: "VerifierPage.aspx/rejectEnrollmentRequest",
                    data: "{'id':'" + id + "','notes':'" + notes + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        document.getElementById("nameSuccessEdit").innerHTML = "Enrollment request has successfully been rejected.";
                        jQuery('#viewEnrollment').modal('hide');
                        jQuery('#successEnroll').modal('show');
                    }
                });
            }
            else {
                document.getElementById("enrollmentFailMessage").innerHTML = "Please provide rejection note before saving.";
                jQuery('#enrollmentFail').modal('show');
            }
        }
        function assignVerifierRequestData(id) {
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getVerifierRequestData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    document.getElementById("verifyUserSpan").innerHTML = data.d[0];
                    document.getElementById("verifyTypeSpan").innerHTML = data.d[1];
                    document.getElementById("verifyCaseSpan").innerHTML = data.d[2];
                    document.getElementById("verifyTimeSpan").innerHTML = data.d[3];
                    document.getElementById("verifyLocSpan").innerHTML = data.d[4];
                    document.getElementById("verifyReasonSpan").innerHTML = data.d[5];
                    document.getElementById("verifyBYearSpan").innerHTML = data.d[6];
                    document.getElementById("verifyEthniSpan").innerHTML = data.d[7];
                    document.getElementById("verifyPIDSpan").innerHTML = data.d[8];
                    document.getElementById("verifyGenderSpan").innerHTML = data.d[9];
                    document.getElementById("verifyListTypeSpan").innerHTML = data.d[10];
                    document.getElementById("veriResultIMG").src = data.d[12];
                    document.getElementById("veriSentIMG").src = data.d[11];
                    if (data.d.length > 14) {
                        document.getElementById("verifyHeaderSpan").innerHTML = data.d[23];
                        document.getElementById("veriResultIMG").src = data.d[13];
                        document.getElementById("veriResult1IMG").src = data.d[13];
                        document.getElementById("veriResult2IMG").src = data.d[15];
                        document.getElementById("veriResult3IMG").src = data.d[17];
                        document.getElementById("veriResult4IMG").src = data.d[19];
                        document.getElementById("veriResult5IMG").src = data.d[21];

                        document.getElementById("resultPercent1").innerHTML = data.d[14] + "%";
                        document.getElementById("resultPercent2").innerHTML = data.d[16] + "%";
                        document.getElementById("resultPercent3").innerHTML = data.d[18] + "%";
                        document.getElementById("resultPercent4").innerHTML = data.d[20] + "%";
                        document.getElementById("resultPercent5").innerHTML = data.d[22] + "%";
                        document.getElementById("resultPercentMain").innerHTML = data.d[14] + "%";
                        document.getElementById('progressbarMainDisplay').setAttribute("style", "width:" + data.d[14] + "%");

                        document.getElementById("verCaseName1").text = data.d[24];
                        document.getElementById("verCaseName2").text = data.d[26];
                        document.getElementById("verCaseName3").text = data.d[28];
                        document.getElementById("verCaseName4").text = data.d[30];
                        document.getElementById("verCaseName5").text = data.d[32];

                        document.getElementById("verifyCaseSpan").innerHTML = data.d[25];
                        document.getElementById("verCaseID1").text = data.d[25];
                        document.getElementById("verCaseID2").text = data.d[27];
                        document.getElementById("verCaseID3").text = data.d[29];
                        document.getElementById("verCaseID4").text = data.d[31];
                        document.getElementById("verCaseID5").text = data.d[33];

                    }
                    else {
                        jQuery('#verificationFail').modal('show');
                        document.getElementById("veriResult1IMG").src = "";
                        document.getElementById("veriResult2IMG").src = "";
                        document.getElementById("veriResult3IMG").src = "";
                        document.getElementById("veriResult4IMG").src = "";
                        document.getElementById("veriResult5IMG").src = "";

                        document.getElementById("resultPercent1").innerHTML = "";
                        document.getElementById("resultPercent2").innerHTML = "";
                        document.getElementById("resultPercent3").innerHTML = "";
                        document.getElementById("resultPercent4").innerHTML = "";
                        document.getElementById("resultPercent5").innerHTML = "";
                        document.getElementById("resultPercentMain").innerHTML = "";
                        document.getElementById('progressbarMainDisplay').setAttribute("style", "width:1%");
                    }
                }
            });
        }
        function addrowtoTable() {
            jQuery("#verifierTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getTableData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#verifierTable tbody").append(data.d[i]);
                    }
                }
            });
        }
        function addrowtoTable2() {
            jQuery("#enrollmentTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getTableData2",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#enrollmentTable tbody").append(data.d[i]);
                    }
                }
            });
        }
        function addrowtoTable3() {
            jQuery("#enrolledTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getTableData3",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#enrolledTable tbody").append(data.d[i]);
                    }
                }
            });
        }
        function addrowtoANPR() {
            jQuery("#anprTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/getTableANPR",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#anprTable tbody").append(data.d[i]);
                    }
                }
            });
        }
        function deleteVeriChoice() {
            var id = document.getElementById("rowidChoice").text;
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/delVerifierData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    jQuery('#entryDeleted').modal('show');

                }
            });
        }
        function deleteEnChoice() {
            var id = document.getElementById("rowidChoice").text;
            jQuery.ajax({
                type: "POST",
                url: "VerifierPage.aspx/delEnrolledData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    jQuery('#entryDeleted').modal('show');
                }
            });
        }
    </script>
    <!-- ============================================
    MAIN CONTENT SECTION
    =============================================== -->
        <section class="content-wrapper" role="main">
            <div class="content">
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">Verifier</span></h3>
                                </div>
                                <div class="col-md-8">
                                    <div class="panel-control">
                                        <ul id="demo3-tabs" class="nav nav-tabs nav-main">
                                            <li class="active"><a data-toggle="tab" href="#frs-tab">FRS</a>
                                            </li>
                                            <li ><a data-toggle="tab" href="#anpr-tab">ANPR</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div role="group" class="pull-right">
                                       <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                    <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            	<div class="tab-content">
							<div class="tab-pane fade active in" id="frs-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-verifier" data-toggle="tab">Verify</a>
                                                    </li>
                                                    <li><a href="#component-enrollment" data-toggle="tab">Requests</a>
                                                    </li>
                                                    <li><a href="#component-enrolled" data-toggle="tab">Enrolled</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#"  data-target="#newUser" data-toggle="modal" class="capitalize-text">+ NEW ENROLLMENT</a>
                                                    </li>                                                    
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row horizontal-chart">
                                            <div class="col-md-15 text-center panel-seperator panel-heading">
                                                <h3 class="panel-title capitalize-text">STATUS</h3>
                                            </div>

                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">REQUESTS</p>
                                                </div>
                                                <div class="inline-block">
                                                   <div class="easyPieChart" data-percent="<%=pendingPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f44e4b">
                                                        <span class="percentage text-dark fa fa-1x">
														<span class="data-percent"><asp:Label ID="lbPendingpercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                   </div>
                                                </div>
                                                <div class="inline-block ">
                                                    <h3><asp:Label ID="lbPending" runat="server"></asp:Label></h3>

                                                </div>
                                            </div>
                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">VERIFY</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart"  data-percent="<%=inprogressPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f2c400">
                                                        <span class="percentage text-dark fa fa-1x">
														  <span class="data-percent"><asp:Label ID="lbInprogresspercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbInprogress" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">ENROLLED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" data-percent="<%=completedPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#3ebb64">
                                                        <span class="percentage text-dark fa fa-1x">
																		<span class="data-percent"><asp:Label ID="lbCompletedpercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbCompleted" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-15 chart-total">
                                                <p><span class="capitalize-text"><asp:Label ID="lbTotalAlarms" runat="server"></asp:Label></span>TOTAL VERIFICATIONS</p>
                                            </div>
                                        </div>
                                        <div class="row show-component component-verifier">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">VERIFICATION</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="verifierTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-enrollment">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ENROLLMENT REQUEST</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="enrollmentTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-enrolled">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ENROLLED</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="enrolledTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="anpr-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-anpr" data-toggle="tab">Entries</a>
                                                    </li>
                                                    <li><a href="#component-anprenrollment" data-toggle="tab">Enrollment</a>
                                                    </li>
                                                <%--    <li><a href="#component-anprenrolled" data-toggle="tab">Enrolled</a>
                                                    </li>--%>
                                                    
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">                                              
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-anpr">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ANPR ENTRIES</h3>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="anprTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="TransactionID" aria-sort="ascending">TransactionID
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">Plate Number<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">Plate City<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">Plate Category<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-anprenrollment">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ANPR REQUESTS</h3>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="anprenrollmentTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                       <%-- <div class="row show-component component-anprenrolled">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ANPR ENROLLED</h3>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="anprenrolledTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">TIME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->--%>
                                        
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div style="display:<%=userinfoDisplay%>" class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                                    </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </div>
                <!-- /panel-body -->
            </div>
            <!-- /.panel -->
            <div aria-hidden="true" aria-labelledby="viewDocument1" role="dialog" tabindex="-1" id="viewDocument1" class="modal fade videoModal" style="display: none;">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <div class="row">
                           <div class="col-md-11">
                              <h4 class="modal-title capitalize-text">VERIFICATION</h4>
                           </div>
                           <div class="col-md-1">
                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                           </div>
                        </div>

                     </div>
                     <div class="modal-body pt-4x">
                        <div class="row border-bottom pb-4x selected-user container-match-user-1">
                           <div class="col-md-15 text-center border-right match-user match-user-1" onclick="moveImageToComparison('1')"> 
                              <div class="help-block">
                                 <img id="veriResult1IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font green-color" id="resultPercent1">
                                    
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-2" onclick="moveImageToComparison('2')"> 
                              <div class="help-block">
                                 <img id="veriResult2IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font yellow-color" id="resultPercent2">

                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-3" onclick="moveImageToComparison('3')"> 
                              <div class="help-block">
                                 <img id="veriResult3IMG" style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent3">
                                
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center border-right match-user match-user-4" onclick="moveImageToComparison('4')"> 
                              <div class="help-block">
                                 <img id="veriResult4IMG"  style="height:61px;width:61px;" src=""  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent4">
                                   
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>
                           <div class="col-md-15 text-center match-user match-user-5" onclick="moveImageToComparison('5')"> 
                              <div class="help-block">
                                 <img id="veriResult5IMG" style="height:61px;width:61px;"  class="table-cricle-image auto-dimention" /> 
                              </div>

                              <div class="help-block">
                                 <span class="percentage large-font lightred-color" id="resultPercent5">
                                    
                                 </span>   
                                 <span class="light-gray">
                                    MATCH
                                 </span>
                              </div>
                              
                           </div>                                                                                                            
                        </div>
                        <div class="row">
                           <div class="col-md-17 border-right mt-2x mb-2x">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="help-block text-center">
                                       <img id="veriResultIMG" style="height:135px;width:135px;border-radius:8px;" src="">
                                       <h4 class="gray-color">Comparison Image</h4>
                                       <a href="#enlargeImageModal" class="red-color" data-dismiss="modal" data-toggle="modal" onclick="enlargeDisplayImage('Comparison')">Enlarge Image</a>
                                    </div>                                    
                                 </div>
                                 <div class="col-md-6">
                                    <div class="help-block text-center">
                                       <img id="veriSentIMG" style="height:135px;width:135px;border-radius:8px;" src="">
                                       <h4 class="gray-color">Sent Image</h4>
                                       <a href="#enlargeImageModal" class="red-color" data-dismiss="modal" data-toggle="modal" onclick="enlargeDisplayImage('Sent')">Enlarge Image</a>
                                    </div>
                                 </div>                                 
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                     <div id="map_verLocation" style="width:100%;height:174px;"></div>
                                 </div>                                 
                              </div>                              
                           </div>
                           <div class="col-md-18">
                              <div class="row">
                                 <div class="col-md-12">
                                    <h4 class="main-header" id="verifyHeaderSpan" style="color:#b2163b;"></h4>
                                 </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Created by:
                                       </p>
                                       <span class="red-color" id="verifyUserSpan">
                                             
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Time:
                                       </p>
                                       <span class="red-color" id="verifyTimeSpan">
                                       </span>                                       
                                    </div>
                              </div>
   
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Type:
                                       </p>
                                       <span class="red-color" id="verifyTypeSpan">
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Location:
                                       </p>
                                       <span class="red-color" id="verifyLocSpan">
                                       </span>                                       
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Case ID:
                                       </p>
                                       <span class="red-color" id="verifyCaseSpan">
                                             
                                       </span>
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             PID
                                       </p>
                                       <span class="red-color" id="verifyPIDSpan">
                                       </span>                                       
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Birth Year:
                                       </p>
                                       <span class="red-color" id="verifyBYearSpan">
                                       </span>                                       
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           Gender
                                       </p>
                                       <span class="red-color" id="verifyGenderSpan">
                                       </span>
                                    </div>                                    
                              </div>
                              <div class="row">
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Ethnicity: 
                                       </p>
                                       <span class="red-color" id="verifyEthniSpan">
                                       </span>                                       
                                    </div>
                                    <div class="col-md-6">
                                       <p class="inline-block">
                                           List:
                                       </p>
                                       <span class="red-color" id="verifyListTypeSpan">
                                       </span>
                                    </div>                                    
                              </div>
                              <div class="row">

                                    <div class="col-md-6">
                                       <p class="inline-block">
                                             Reason:
                                       </p>
                                       <span class="red-color" id="verifyReasonSpan">
                                       </span>                                       
                                    </div>

                                    <div class="col-md-6">
                                       <p class="inline-block">

                                       </p>
                                       <span class="red-color">

                                       </span>                                       
                                    </div>                                    
                              </div>  


                              <div class="row mt-4x">
                                 <div class="col-md-6">
                                    <p class="no-vmargin">
                                       Match Score
                                    </p>
                                 </div>

                                 <div class="col-md-6 text-right">
                                    <span class="green-color" id="resultPercentMain">
                                          
                                    </span>
                                 </div>
                                 
                              </div>
                              <div class="row">
                                    <div class="col-md-12">
                                                <div class="progress rounded-edge full-proogress-bar">
                                                   <div id="progressbarMainDisplay" class="progress-bar progress-bar-green rounded-edge full-proogress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                                   </div>
                                                </div>                                        
                                    </div>                                    
                              </div>                                                                                                                        
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs" id="verifyDIV">
                                 <li class="active"><a href="#" onclick="runVerification()">VERIFY</a>
                                 </li>
                                 <li class="active"><a href="#" data-dismiss="modal">CLOSE</a>
                                 </li>
                              </ul>
                              <ul class="nav nav-tabs" id="nextVerifyDIV" style="display:none;">
                                 <li class="active"><a href="#" onclick="runVerification()">VERIFY</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>	
            <div aria-hidden="true" aria-labelledby="anprViewCard" role="dialog" tabindex="-1" id="anprViewCard" class="modal fade videoModal" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="headerImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="incidentNameHeader">ANPR</h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-4">
							<p>Created by: <span id="usernameSpan"></span></p> 
						</div>		
                        <div class="col-md-4">
							<p>Location: <span id="locSpan"></span></p>
						</div>	
						<div class="col-md-4">
							<p>Created on: <span id="timeSpan"></span></p>
						</div>				
					  </div>				
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                            <li class="active" id="liInfo"><a href="#info-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="liAtta"><a href="#attachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
                                        	<div class="tab-content">
										<div class="tab-pane fade active in" id="info-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
												<div class="col-md-12">
													<p class="red-color"><b>Plate Number:</b></p>
													<p id="platenumberSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate City:</b></p>
													<p id="platesourceSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate Country:</b></p>
													<p id="plateCodeSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate Category:</b></p>
													<p id="plateCatSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate Color:</b></p>
													<p id="plateColorSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate Type:</b></p>
													<p id="plateTypeSpan"></p>
												</div>
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div>
										<div class="tab-pane fade" id="attachments-tab" onclick="startRot()">	
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="attachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>							
										</div>		
                                                </div>								
									</div>
								</div>
							</div>
							<div class="col-md-8" id="divAttachmentHolder">
								<div class="tab-pane fade active in" id="location-tab">
                                    <div id="map_canvasIncidentLocation" style="width:100%;height:378px;"></div>
									<div id="divAttachment" onclick="startRot()" class="overlapping-map-image">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
									<li><a href="#" data-dismiss="modal" onclick="processANPRTicket()">HANDLE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="viewEnrollment" role="dialog" tabindex="-1" id="viewEnrollment" class="modal fade videoModal" style="display: none;">
               <div class="modal-dialog modal-md">
                  <div class="modal-content">
                     <div class="modal-header">
                        <div class="row">
                           <div class="col-md-11">
                              <h4 class="modal-title capitalize-text" id="enrollmentHeader"></h4>
                           </div>
                           <div class="col-md-1">
                              <button aria-hidden="true" onclick="hideRejectDiv()" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                           </div>
                        </div>

                     </div>
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-md-6">
                                 <div class="row" id="divNameEnrollment">
                                     
									<div class="col-md-12">
                                        <p class="red-color"><b>Enrollment Name:</b></p>
                                        <input placeholder="NAME" id="tbNameEnrollment" class="form-control">
									</div>
								</div>	 
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Enrollment Type:</b></p>
                                        <label  id="enrollmentTypeDIV" class="select select-o">
                                           <select id="enrollmentTypeSelect" onchange="UserRoleSelectorChange(this);">
                                              <option>FRS</option>
                                              <option>ANPR</option>
                                           </select>
                                       </label>   
									</div>
								</div>	     
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Enrolled By:</b></p>
										<input placeholder="USER" id="tbUserEnrollment" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Reason:</b></p>
										<input placeholder="REASON" id="tbReasonEnrollment" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Personal ID:</b></p>
										<input placeholder="PID" id="tbPIDEnrollment" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Birth Year:</b></p>
										<input placeholder="BIRTH YEAR" id="tbBYEnrollment" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Gender:</b></p>
										<label id="genderSelectDIV" class="select select-o">
                                           <select id="genderSelectionEnrollment" onchange="UserRoleSelectorChange(this);">
                                              <option>Male</option>
                                              <option>Female</option>
                                           </select>
                                       </label>    
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Ethnicity:</b></p>

                                        <label  id="ethnicitySelectDIV" class="select select-o">
                                           <select id="ethnicitySelectionEnrollment" onchange="UserRoleSelectorChange(this);">
                                              <option>Asian</option>
                                              <option>Black</option>
                                               <option>Caucasian</option>
                                           </select>
                                       </label>    
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12 ">
                                        <p class="red-color"><b>Status:</b></p>
                                        <label id="ListTypeSelectDIV" class="select select-o">
                                           <select id="ListTypeEnrollment" onchange="UserRoleSelectorChange(this);">
                                              <option>BlackList</option>
                                              <option>WhiteList</option>
                                           </select>
                                       </label> 
									</div>
								</div>                                                                 
                           </div>
                           <div class="col-md-6">
                              <div class="row">
                                  <div class="col-md-1"></div>
                                 <div class="col-md-10 text-center">
                                       <img id="enrollmentIMG" class="user-profile-image" src=""> 
                                 </div>  
                                  <div class="col-md-1"></div>                             
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div id="map_Location" style="width:100%;height:174px;"></div>
                                 </div>                                 
                              </div>                              
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                       <div class="row" id="rejectionoteDiv" style="display:none;">
									<div class="col-md-12">
										<input placeholder="Rejection Note" id="tbRejectionNote" class="form-control">
									</div>
						</div>
                        <div class="row horizontal-navigation  mt-3x">
                           <div class="panel-control">
                              <ul class="nav nav-tabs" id="enrolledDIV" style="display:none;">
                                 <li ><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li class="active"><a href="#" data-dismiss="modal" onclick="enrollVerifier('True')">SAVE</a>
                                 </li>
                              </ul>
                              <ul class="nav nav-tabs" id="enrollRequestDIV" style="display:none;">
                                 <li ><a href="#" onclick="showRejectDiv()">REJECT</a>
                                 </li>
                                 <li class="active"><a href="#" onclick="enrollVerifier('False')">ENROLL</a>
                                 </li>
                              </ul>
                              <ul class="nav nav-tabs" id="enrollRejectDIV" style="display:none;">
                                 <li class="active"><a href="#" data-dismiss="modal" onclick="hideRejectDiv()">CANCEL</a>
                                 </li>
                                 <li ><a href="#" onclick="rejectEnrollment()">SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="anprEnrollment" role="dialog" tabindex="-1" id="anprEnrollment" class="modal fade videoModal" style="display: none;">
               <div class="modal-dialog modal-md">
                  <div class="modal-content">
                     <div class="modal-header">
                        <div class="row">
                           <div class="col-md-11">
                              <h4 class="modal-title capitalize-text">ANPR ENROLLMENT REQUEST</h4>
                           </div>
                           <div class="col-md-1">
                              <button aria-hidden="true" onclick="hideRejectDiv()" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                           </div>
                        </div>

                     </div>
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-md-6">
                               <div class="row">
									<div class="col-md-12"> 
                                        <p class="red-color"><b>Requested By:</b></p>
                                        <input type="text" id="tbANPRRequestBy" class="form-control">
									</div>
								</div>
                               
                               <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Plate Code:</b></p>
                                        <input placeholder="PLATE PREFIX" id="anprPlatePrefixEnrollment" class="form-control">
									</div>
								</div>	
                                 <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Plate Number:</b></p>
                                        <input placeholder="PLATE NUMBER" id="tbANPRPlateEnrollment" class="form-control">
									</div>
								</div>	 
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Lane:</b></p>
                                        <label   class="select select-o">
                                           <select id="anprLaneTypeEnrollment" >
                                              <option value="E">ENTRY LANE</option>
                                              <option value="X">EXIT LANE</option>
                                           </select>
                                       </label>   
									</div>
								</div>	     
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Plate Country:</b></p>
                                        <label   class="select select-o">
                                           <select id="anprPlateCityEnrollment" >
                                              <option value="SAU">SAUDI ARABIA</option>
                                              <option value="KUW">KUWAIT</option>
                                               <option value="DXB">UAE-DUBAI</option>
                                               <option value="AUH">UAE-ABU DHABI</option>
                                               <option value="AJM">UAE-AJMAN</option>
                                               <option value="FUJ">UAE-FUJ</option>
                                               <option value="SHJ">UAE-SHARJAH</option>
                                               <option value="UAQ">UAE-UAQ</option>
                                               <option value="RAK">UAE-RAK</option>
                                               <option value="FED">UAE-FED</option>
                                               <option value="UNK">UAE-UNK</option>
                                               <option value="QAT">QATAR</option>
                                               <option value="OMN">OMAN</option>
                                               <option value="BHR">BAHRAIN</option>
                                           </select>
                                       </label>   
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Plate Category:</b></p>
                                        <label   class="select select-o">
                                           <select id="anprPlateCategoryEnrollment" runat="server">
                                           </select>
                                       </label>   
									</div>
								</div>
                                <div class="row">
								    <div class="col-md-12">
                                        <p class="red-color"><b>Plate Color:</b></p>
                                        <label   class="select select-o">
                                           <select id="anprPlateColorEnrollment" >
                                               <option>White</option>
                                               <option>Green</option>
                                               <option>Red</option>
                                               <option>Blue</option>
                                               <option>Orange</option>
                                               <option>Yellow</option>
                                               <option>Cyan</option>
                                               <option>Black</option>
                                           </select>
                                       </label>   
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Plate Type:</b></p>
                                        <label   class="select select-o">
                                           <select id="anprPlateTypeEnrollment" >
                                               <option>Long</option>
                                               <option>Short</option>
                                               <option>Not Available</option>
                                           </select>
                                       </label>   
									</div>
								</div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="red-color"><b>Plate Status:</b></p>
                                        <label   class="select select-o">
                                           <select id="anprPlateStatusCodeEnrollment" runat="server">
  
                                           </select>
                                       </label>   
									</div>
								</div>
                                              </div>
                           <div class="col-md-6">
                              <div class="row">
                                  <div class="col-md-1"></div>
                                 <div class="col-md-10 text-center">
                                       <img id="anprenrollmentIMG" class="user-profile-image" src=""> 
                                 </div>  
                                  <div class="col-md-1"></div>                             
                              </div>
                              <%--<div class="row">
                                 <div class="col-md-12">
                                    <div id="map_Location" style="width:100%;height:174px;"></div>
                                 </div>                                 
                              </div>    --%>                          
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation  mt-3x">
                           <div class="panel-control">
                              <ul class="nav nav-tabs" id="anprenrollRequestDIV">
                                 <li ><a href="#" data-dismiss="modal" onclick="rejectANPR()">REJECT</a>
                                 </li>
                                 <li class="active" id="liEnrollANPR" style="opacity:0.6;pointer-events:none;"><a href="#" onclick="enrollANPR('request')">ENROLL</a>
                                 </li>
                                  <li class="active"><a onclick="verifyANPR()">VERIFY</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <div aria-hidden="true" aria-labelledby="newEnrollment" role="dialog" tabindex="-1" id="newEnrollment" class="modal fade videoModal" style="display: none;">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">

                     <div class="modal-body pt-4x">
                        <div class="row">
                                <div class="row col-md-7 border-right mt-2x mb-2x">
                                    <div class="col-md-12">
                                        <div class="panel-body">
                                          <h3>Profile Picuture</h3>
                                          <form enctype="multipart/form-data" method="post" id="dz-edittest" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              <p class="lead">your image anywhere or <span>browse</span></p>
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                                </div>                          
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs" >
                                 <li class="active"><a href="#">ENROLL</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>	
            <div aria-hidden="true" aria-labelledby="newUser" role="dialog" tabindex="-1" id="newUser" class="modal fade" style="display: none;">
                 <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <div class="row">
                           <div class="col-md-11">
                              <h4 class="modal-title capitalize-text">NEW ENROLLMENT</h4>
                           </div>
                           <div class="col-md-1">
                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                           </div>
                        </div>

                     </div>
                     <div class="modal-body pt-4x">
                        <div class="row">
                           <div class="col-md-5">
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Enrolled Name:</b></p>
										<input placeholder="NAME" id="tbNEWNameEnrollment" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Enrolled Type:</b></p>
                                        <label id="newEnrollmentTypeDIV" class="select select-o">
                                           <select id="newEnrollmentTypeSelect" onchange="UserRoleSelectorChange(this);">
                                              <option>FRS</option>
                                              <option>ANPR</option>
                                           </select>
                                       </label>   
									</div>
								</div>	     
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Reason:</b></p>
										<input placeholder="REASON" id="tbNEWReasonEnrollment" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Personal Id:</b></p>
										<input placeholder="PID" id="tbNEWPIDEnrollment" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Birth Year:</b></p>
										<input placeholder="BIRTH YEAR" id="tbNEWBYEnrollment" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Gender:</b></p>
										<label id="newGenderSelectDIV" class="select select-o">
                                           <select id="newGenderSelectionEnrollment" onchange="UserRoleSelectorChange(this);">
                                              <option>Male</option>
                                              <option>Female</option>
                                           </select>
                                       </label>    
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Ethnicity:</b></p>
                                        <label id="newEthnicitySelectDIV" class="select select-o">
                                           <select id="newEthnicitySelectionEnrollment" onchange="UserRoleSelectorChange(this);">
                                              <option>Asian</option>
                                              <option>Black</option>
                                               <option>Caucasian</option>
                                           </select>
                                       </label>    
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p class="red-color"><b>Status:</b></p>
                                        <label  id="newListTypeSelectDIV" class="select select-o">
                                           <select id="newListTypeEnrollment" onchange="UserRoleSelectorChange(this);">
                                              <option>BlackList</option>
                                              <option>WhiteList</option>
                                           </select>
                                       </label> 
									</div>
								</div>                                                                 
                           </div>
                            <div class="row col-md-7 border-right mt-2x mb-2x">
                                    <div class="col-md-12">
                                        <p class="red-color"><b>Upload Photo:</b></p>
                                        <div class="panel-body">
                                          <form enctype="multipart/form-data" method="post" id="dz-test" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              <p class="lead">your image anywhere or <span>browse</span></p>
                                       </div>
                                    </form>
                                  </div>
                               </div>
                           </div>                          
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs" >
                                 <li class="active"><a href="#" onclick="newEnrollment()">ENROLL</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
             </div>  
            <div aria-hidden="true" aria-labelledby="deleteVer" role="dialog" tabindex="-1" id="deleteVer" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                         <div class="row">
                            <h4 class="text-center" id="todeleteVer"></h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: This action cannot be undone!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a href="#" data-dismiss="modal" onclick="deleteVeriChoice()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="deleteEnroll" role="dialog" tabindex="-1" id="deleteEnroll" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                         <div class="row">
                            <h4 class="text-center" id="todeleteEn"></h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: This action cannot be undone!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a href="#" data-dismiss="modal" onclick="deleteEnChoice()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="deleteANPR" role="dialog" tabindex="-1" id="deleteANPR" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                         <div class="row">
                            <h4 class="text-center" id="todeleteANPR"></h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: This action cannot be undone!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li class="active"> <a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li class="active"><a href="#" data-dismiss="modal" onclick="deleteANPRChoice()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="entryDeleted" role="dialog" tabindex="-1" id="entryDeleted" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body text-center remove-border-bottom">
                        <div class="row">
                            <div class="col-md-12">
                                <p style="color:gray" class="capitalize-text ">
                                    GOOD JOB!
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <i class="fa fa-smile-o fa-6x green-color"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    <span class="red-color" id="nameDeleted">
                                        
                                    </span>
                                    has been successfully deleted.
                                </p>
                            </div>
                        </div>                      
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs text-center">
                                    <li><a href="#" data-dismiss="modal" class="capitalize-text" onclick="location.reload();">Close</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="verificationFail" role="dialog" tabindex="-1" id="verificationFail" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body text-center remove-border-bottom">
                        <div class="row">
                            <div class="col-md-12">
                                <i class="fa fa-warning fa-4x red-color"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    <span class="red-color" id="warningMessage">
                                        Failed to get images from service.
                                    </span>
                                    Try again later.
                                </p>
                            </div>
                        </div>                      
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs text-center">
                                    <li><a href="#" data-dismiss="modal" class="capitalize-text">Close</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="enrollmentFail" role="dialog" tabindex="-1" id="enrollmentFail" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body text-center remove-border-bottom">
                        <div class="row">
                            <div class="col-md-12">
                                <i class="fa fa-warning fa-4x red-color"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    <span class="red-color" id="enrollmentFailMessage">

                                    </span>
                                </p>
                            </div>
                        </div>                      
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs text-center">
                                    <li><a href="#" data-dismiss="modal" class="capitalize-text">Close</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="enlargeImageModal" role="dialog" tabindex="-1" id="enlargeImageModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
                       <div class="modal-header">
                            <div class="row"></div>
                        </div>
					<div class="modal-body" >
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                            <img id="enlargeIMG" class="user-profile-image" src=""> 
                                </div>
                            <div class="col-md-1"></div>
                        </div>
					</div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs text-center">
                                    <li><a href="#" data-dismiss="modal" onclick="jQuery('#viewDocument1').modal('show');" class="capitalize-text">Close</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="successEnroll" role="dialog" tabindex="-1" id="successEnroll" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body text-center remove-border-bottom">
                        <div class="row">
                            <div class="col-md-12">
                                <p style="color:gray" class="capitalize-text ">
                                    GOOD JOB!
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <i class="fa fa-smile-o fa-6x green-color"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p id="nameSuccessEdit">
                                </p>
                            </div>
                        </div>                      
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs text-center">
                                    <li><a href="#" data-dismiss="modal" class="capitalize-text" onclick="location.reload();">Close</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="successfulModal" role="dialog" tabindex="-1" id="successfulModal" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="../Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successMessage"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="../Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li class="active"><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
                          <input style="display:none;" id="rowIncidentName" type="text"/>
            <input style="display:none;" id="verCaseID1" type="text"/>
            <input style="display:none;" id="verCaseID2" type="text"/>
            <input style="display:none;" id="verCaseID3" type="text"/>
            <input style="display:none;" id="verCaseID4" type="text"/>
            <input style="display:none;" id="verCaseID5" type="text"/>
            <input style="display:none;" id="verCaseName1" type="text"/>
            <input style="display:none;" id="verCaseName2" type="text"/>
            <input style="display:none;" id="verCaseName3" type="text"/>
            <input style="display:none;" id="verCaseName4" type="text"/>
            <input style="display:none;" id="verCaseName5" type="text"/>
            
            <input style="display:none;" id="rowidChoice" type="text"/>
            <input style="display:none;" id="rowidChoiceANPR" type="text"/>
            <input style="display:none;" id="imagePath" type="text"/>
             </section>
        <!-- /MAIN -->
</asp:Content>

