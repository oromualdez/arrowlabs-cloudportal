﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class WarehouseAssetCategoryAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public int CustomerId { get; set; }

        public static bool InsertWarehouseAssetCategoryAudit(WarehouseAssetCategoryAudit assetCategory, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(assetCategory.BaseAudit, dbConnection);

            if (assetCategory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertWarehouseAssetCategoryAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = assetCategory.Id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = assetCategory.Name;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = assetCategory.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = assetCategory.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = assetCategory.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = assetCategory.CustomerId;
                     
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
