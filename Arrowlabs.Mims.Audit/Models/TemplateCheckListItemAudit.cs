﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class TemplateCheckListItemAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public int ParentCheckListId { get; set; }
        public int ChildCheckListId { get; set; }
        public int CheckListItemType { get; set; }
        public bool IsChecked { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        // Type 2
        public int EntryQuantity { get; set; }
        public int ExitQuantity { get; set; }
        public int SiteId { get; set; }

        // Mapping Attributes
        public string ParentCheckListName { get; set; }
        public string ChildCheckListName { get; set; }
        public int CustomerId { get; set; }
        public string CheckListLocation { get; set; }
        public bool IsCamera { get; set; }

        private static TemplateCheckListItemAudit GetTemplateCheckListItemAuditFromSqlReader(SqlDataReader reader)
        {
            var templateCheckListItem = new TemplateCheckListItemAudit();
            templateCheckListItem.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                templateCheckListItem.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                templateCheckListItem.Name = reader["Name"].ToString();
            if (columns.Contains("Quantity") && reader["Quantity"] != DBNull.Value)
                templateCheckListItem.Quantity = Convert.ToInt32(reader["Quantity"].ToString());
            if (columns.Contains("EntryQuantity") && reader["EntryQuantity"] != DBNull.Value)
                templateCheckListItem.EntryQuantity = (reader["EntryQuantity"] != null) ? Convert.ToInt32(reader["EntryQuantity"].ToString()) : 0;
            if (columns.Contains("ExitQuantity") && reader["ExitQuantity"] != DBNull.Value)
                templateCheckListItem.ExitQuantity = (reader["ExitQuantity"] != null) ? Convert.ToInt32(reader["ExitQuantity"].ToString()) : 0;
            if (columns.Contains("ParentCheckListId") && reader["ParentCheckListId"] != DBNull.Value)
                templateCheckListItem.ParentCheckListId = Convert.ToInt32(reader["ParentCheckListId"].ToString());
            if (columns.Contains("ChildCheckListId") && reader["ChildCheckListId"] != DBNull.Value)
                templateCheckListItem.ChildCheckListId = (!String.IsNullOrEmpty(reader["ChildCheckListId"].ToString())) ? Convert.ToInt32(reader["ChildCheckListId"].ToString()) : 0;
            if (columns.Contains("IsChecked") && reader["IsChecked"] != DBNull.Value) templateCheckListItem.CheckListItemType = Convert.ToInt32(reader["CheckListItemType"].ToString());
            templateCheckListItem.IsChecked = Convert.ToBoolean(reader["IsChecked"].ToString().ToLower());
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                templateCheckListItem.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString().ToLower());
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                templateCheckListItem.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                templateCheckListItem.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("ParentName") && reader["ParentName"] != DBNull.Value)
                templateCheckListItem.ParentCheckListName = reader["ParentName"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                templateCheckListItem.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                templateCheckListItem.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            return templateCheckListItem;
        }
        public static bool InsertTemplateCheckListItemAudit(TemplateCheckListItemAudit checkListItem, string dbConnection)
        {
            if (checkListItem != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTemplateCheckListItemAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(checkListItem.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = checkListItem.Id;

                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = checkListItem.Name;

                    cmd.Parameters.Add("@ParentCheckListId", SqlDbType.Int).Value = checkListItem.ParentCheckListId;

                    cmd.Parameters.Add("@ChildCheckListId", SqlDbType.Int).Value = checkListItem.ChildCheckListId;

                    cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = checkListItem.Quantity;

                    cmd.Parameters.Add("@EntryQuantity", SqlDbType.Int).Value = checkListItem.EntryQuantity;

                    cmd.Parameters.Add("@ExitQuantity", SqlDbType.Int).Value = checkListItem.ExitQuantity;

                    cmd.Parameters.Add("@CheckListItemType", SqlDbType.Int).Value = checkListItem.CheckListItemType;

                    cmd.Parameters.Add("@IsChecked", SqlDbType.Int).Value = checkListItem.IsChecked;

                    cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = checkListItem.IsDeleted;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = checkListItem.CreatedDate;
                    
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = checkListItem.CreatedBy;

                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = checkListItem.SiteId;

                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = checkListItem.CustomerId;

                    cmd.Parameters.Add("@IsCamera", SqlDbType.Bit).Value = checkListItem.IsCamera;

                    cmd.Parameters.Add("@CheckListLocation", SqlDbType.NVarChar).Value = checkListItem.CheckListLocation;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static List<TemplateCheckListItemAudit> GetAllTemplateCheckListItemAudit(string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckListItemAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListItemAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListItemAuditFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }

                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }
    }
}
