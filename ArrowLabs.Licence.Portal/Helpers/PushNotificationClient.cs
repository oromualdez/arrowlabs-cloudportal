﻿using Arrowlabs.Business.Layer;
using Newtonsoft.Json.Linq;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace ArrowLabs.Licence.Portal.Helpers
{
    public static class PushNotificationClient
    {
        static string appleCertificatePath = System.IO.Path.Combine(System.Web.HttpRuntime.BinDirectory, "ArrowLabs_MIMS_APNS.p12");
        static string appleCertificatePassword = "arrowlabs";

        static PushNotificationClient()
        {
            //push.OnNotificationSent += NotificationSent;
            //push.OnChannelException += ChannelException;
            //push.OnServiceException += ServiceException;
            //push.OnNotificationFailed += NotificationFailed;
            //push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            //push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            //push.OnChannelCreated += ChannelCreated;
            //push.OnChannelDestroyed += ChannelDestroyed;            

        }
        static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            Console.WriteLine("Device Registration Changed:  Old-> " + oldSubscriptionId + "  New-> " + newSubscriptionId + " -> " + notification);
        }
        static void NotificationSent(object sender, INotification notification)
        {
            //Console.WriteLine("Sent: " + sender + " -> " + notification);
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
            //{

            //    file.WriteLine("Sent: " + sender + " -> " + notification);
            //}
            if (!string.IsNullOrEmpty(dbCon))
                Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("NotificationSent-" + sender, "Sent: " + sender + " -> " + notification, new Exception(), dbCon);
        }
        static void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            if (!string.IsNullOrEmpty(dbCon))
                Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("NotificationFailed-" + sender, "Failure: " + sender + " -> " + notification, new Exception(), dbCon);

            //Console.WriteLine("Failure: " + sender + " -> " + notificationFailureException.Message + " -> " + notification);
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
            //{

            //    file.WriteLine("Failure: " + sender + " -> " + notificationFailureException.Message + " -> " + notification);
            //}
        }
        //static void ChannelException(object sender, IPushChannel channel, Exception exception)
        //{
        //    Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        //}
        static void ServiceException(object sender, Exception exception)
        {
            if (!string.IsNullOrEmpty(dbCon))
                Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("ServiceException-" + sender, "Service Exception: " + sender + " -> " + exception, new Exception(), dbCon);
            //Console.WriteLine("Service Exception: " + sender + " -> " + exception);
        }
        static void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            if (!string.IsNullOrEmpty(dbCon))
                Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("DeviceSubscriptionExpired-" + sender, "Device Subscription Expired: " + sender + " -> " + notification, new Exception(), dbCon);
            //Console.WriteLine("Device Subscription Expired: " + sender + " -> " + expiredDeviceSubscriptionId);
        }
        static void ChannelDestroyed(object sender)
        {
            //Console.WriteLine("Channel Destroyed for: " + sender);
        }
        //static void ChannelCreated(object sender, IPushChannel pushChannel)
        //{
        //    Console.WriteLine("Channel Created for: " + sender);
        //    using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
        //    {

        //        file.WriteLine("Channel Created for: " + sender);
        //    }
        //}
       static string dbCon { get; set; }
        public static void SendtoAllAppleNotification(string message, bool isManager, string loggedInUsername, string dbConnection)
        {
            try
            {
                dbCon = dbConnection;
                if (!string.IsNullOrEmpty(message))
                {
                             ApnsConfiguration config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production,
    appleCertificatePath, appleCertificatePassword);
         ApnsServiceBroker apnsBroker = new ApnsServiceBroker(config);
         apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
         {

             aggregateEx.Handle(ex =>
             {

                 // See what kind of exception it was to further diagnose
                 if (ex is ApnsNotificationException)
                 {
                     var notificationException = (ApnsNotificationException)ex;

                     // Deal with the failed notification
                     var apnsNotification = notificationException.Notification;
                     var statusCode = notificationException.ErrorStatusCode;

                     if (!string.IsNullOrEmpty(dbCon))
                         Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAllAppleNotification-" + loggedInUsername, "Apple Notification Failed: ID=: " + apnsNotification, new Exception(), dbCon);

                     //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                     //{

                     //    file.WriteLine("Apple Notification Failed: ID=" + apnsNotification + ", Code=" + statusCode);
                     //}
                 }
                 else
                 {
                     if (!string.IsNullOrEmpty(dbCon))
                         Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAllAppleNotification-" + loggedInUsername, "Apple Notification Failed for some unknown reason: ", new Exception(), dbCon);

                     // Inner exception might hold more useful information like an ApnsConnectionException           
                     //Console.WriteLine("Apple Notification Failed for some unknown reason : {ex.InnerException}");
                     //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                     //{

                     //    file.WriteLine("Apple Notification Failed for some unknown reason : {ex.InnerException}");
                     //}
                 }

                 // Mark it as handled
                 return true;
             });
         };

         apnsBroker.OnNotificationSucceeded += (notification) =>
         {
             //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
             //{

             //    file.WriteLine("Apple Notification Sent!");
             //}
             if (!string.IsNullOrEmpty(dbCon))
                 Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAllAppleNotification-" + loggedInUsername, "Apple Notification Sent!-"+DateTime.Now.ToString(), new Exception(), dbCon);
         };
                    // Start the broker
                    apnsBroker.Start();

                    //foreach (var deviceToken in MY_DEVICE_TOKENS) {
                    // Queue a notification to send
                    var devicelist = PushNotificationDevice.GetAllPushNotificationDevices(dbConnection);
                    var filteredList = new List<PushNotificationDevice>();
                    if (isManager)
                    {
                        var manager = Users.GetUserByName(loggedInUsername, dbConnection);
                        var subordinates = Users.GetAllFullUsersByManagerId(manager.ID, dbConnection);
                        var subordinateNames = subordinates.Select(user => user.Username.ToLower()).ToList();

                        foreach (var device in devicelist)
                        {
                            if (device.Username != null && subordinateNames.Contains(device.Username.ToLower()))
                            {
                                filteredList.Add(device);
                            }
                        }
                    }
                    else
                    {
                        filteredList = devicelist;
                    }
                    foreach (var dev in filteredList)
                    {
                        {
                            if (!string.IsNullOrEmpty(dev.DeviceToken) && dev.DeviceType == (int)PushNotificationDeviceType.Apple)
                            {
                                apnsBroker.QueueNotification(new ApnsNotification
                                {
                                    Tag = dev.DeviceToken,
                                    DeviceToken = dev.DeviceToken,
                                    Payload = JObject.Parse("{\"aps\":{\"badge\":0 ,\"alert\":\"" + message + "\",\"sound\":\"default\"}}")
                                });
                            }
                        }
                    }
                    apnsBroker.Stop();

                }
            }
            catch (Exception ex) { MIMSLog.MIMSLogSave("PushNofiticationClient", "SendtoAllAppleNotification", ex, CommonUtility.dbConnection); }
        }
        public static void SendtoAppleUserNotification(string message, string username, string dbConnection)
        {
            try
            {
                if (!string.IsNullOrEmpty(message))
                {
                             ApnsConfiguration config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production,
    appleCertificatePath, appleCertificatePassword);
         ApnsServiceBroker apnsBroker = new ApnsServiceBroker(config);
         apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
         {

             aggregateEx.Handle(ex =>
             {

                 // See what kind of exception it was to further diagnose
                 if (ex is ApnsNotificationException)
                 {
                     var notificationException = (ApnsNotificationException)ex;

                     // Deal with the failed notification
                     var apnsNotification = notificationException.Notification;
                     var statusCode = notificationException.ErrorStatusCode;

                     if (!string.IsNullOrEmpty(dbCon))
                         Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleUserNotification-" + username, "Apple Notification Failed: ID=: " + apnsNotification, new Exception(), dbCon);

                     //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                     //{

                     //    file.WriteLine("Apple Notification Failed: ID=" + apnsNotification + ", Code=" + statusCode);
                     //}
                 }
                 else
                 {
                     if (!string.IsNullOrEmpty(dbCon))
                         Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleUserNotification-" + username, "Apple Notification Failed for some unknown reason: ", new Exception(), dbCon);

                     // Inner exception might hold more useful information like an ApnsConnectionException           
                     //Console.WriteLine("Apple Notification Failed for some unknown reason : {ex.InnerException}");
                     //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                     //{

                     //    file.WriteLine("Apple Notification Failed for some unknown reason : {ex.InnerException}");
                     //}
                 }
                 // Mark it as handled
                 return true;
             });
         };

         apnsBroker.OnNotificationSucceeded += (notification) =>
         {
             if (!string.IsNullOrEmpty(dbCon))
                 Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleUserNotification-" + username, "Apple Notification Sent!-" + DateTime.Now.ToString(), new Exception(), dbCon);

             //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
             //{

             //    file.WriteLine("(Apple Notification Sent!) - "+DateTime.Now.ToString());
             //}
         };
                    // Start the broker
                    apnsBroker.Start();

                    //foreach (var deviceToken in MY_DEVICE_TOKENS) {
                    // Queue a notification to send
                    var devicelist = PushNotificationDevice.GetPushNotificationDeviceByUsername(username, dbConnection);

                    if (!string.IsNullOrEmpty(devicelist.DeviceToken) && devicelist.DeviceType == (int)PushNotificationDeviceType.Apple)
                    {
                        apnsBroker.QueueNotification(new ApnsNotification
                        {
                            Tag = devicelist.DeviceToken,
                            DeviceToken = devicelist.DeviceToken,
                            Payload = JObject.Parse("{\"aps\":{\"badge\":0 ,\"alert\":\"" + message + "\",\"sound\":\"default\"}}")
                        });
                    }
                    apnsBroker.Stop();
                }
            }
            catch (Exception ex)  
            {
                MIMSLog.MIMSLogSave("PushNofiticationClient", "SendtoAppleUserNotification", ex, CommonUtility.dbConnection);
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                //{

                //    file.WriteLine(ex.Message + "-SendtoAppleUserNotification");
                //}
                
            }
        }

        public static void SendtoAppleMultiUserNotification(string message, string[] usernames, string dbConnection)
        {
            try
            {
                if (!string.IsNullOrEmpty(message))
                {
                             ApnsConfiguration config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production,
    appleCertificatePath, appleCertificatePassword);
         ApnsServiceBroker apnsBroker = new ApnsServiceBroker(config);

         apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
         {

             aggregateEx.Handle(ex =>
             {

                 // See what kind of exception it was to further diagnose
                 if (ex is ApnsNotificationException)
                 {
                     var notificationException = (ApnsNotificationException)ex;

                     // Deal with the failed notification
                     var apnsNotification = notificationException.Notification;
                     var statusCode = notificationException.ErrorStatusCode;

                     if (!string.IsNullOrEmpty(dbCon))
                         Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleMultiUserNotification-" + usernames[0], "Apple Notification Failed: ID=: " + apnsNotification, new Exception(), dbCon);

                     //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                     //{

                     //    file.WriteLine("Apple Notification Failed: ID=" + apnsNotification + ", Code=" + statusCode);
                     //}
                 }
                 else
                 {
                     if (!string.IsNullOrEmpty(dbCon))
                         Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleMultiUserNotification-" + usernames[0], "Apple Notification Failed for some unknown reason: ", new Exception(), dbCon);

                     // Inner exception might hold more useful information like an ApnsConnectionException           
                     //Console.WriteLine("Apple Notification Failed for some unknown reason : {ex.InnerException}");
                     //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                     //{

                     //    file.WriteLine("Apple Notification Failed for some unknown reason : {ex.InnerException}");
                     //}
                 }

                 // Mark it as handled
                 return true;
             });
         };

         apnsBroker.OnNotificationSucceeded += (notification) =>
         {
             if (!string.IsNullOrEmpty(dbCon))
                 Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleMultiUserNotification-" + usernames[0], "Apple Notification Sent!-" + DateTime.Now.ToString(), new Exception(), dbCon);


             //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
             //{

             //    file.WriteLine("Apple Notification Sent!");
             //}
         };
                    // Start the broker
                    apnsBroker.Start();

                    //foreach (var deviceToken in MY_DEVICE_TOKENS) {
                    // Queue a notification to send
                    foreach (var username in usernames)
                    {
                        var devicelist = PushNotificationDevice.GetPushNotificationDeviceByUsername(username, dbConnection);

                        if (!string.IsNullOrEmpty(devicelist.DeviceToken) && devicelist.DeviceType == (int)PushNotificationDeviceType.Apple)
                        {
                            apnsBroker.QueueNotification(new ApnsNotification
                            {
                                Tag = devicelist.DeviceToken,
                                DeviceToken = devicelist.DeviceToken,
                                Payload = JObject.Parse("{\"aps\":{\"badge\":0 ,\"alert\":\"" + message + "\",\"sound\":\"default\"}}")
                            });
                        }
                    }
                    apnsBroker.Stop();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("PushNofiticationClient", "SendtoAppleMultiUserNotification", ex, CommonUtility.dbConnection);
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                //{

                //    file.WriteLine(ex.Message + "-SendtoAppleUserNotification");
                //}
            }
        }
        public static void SendtoAppleUsersInGroupNotification(int groupid, string message, string dbConnection)
        {
            try
            {
                if (!string.IsNullOrEmpty(message))
                {
                             ApnsConfiguration config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production,
    appleCertificatePath, appleCertificatePassword);
         ApnsServiceBroker apnsBroker = new ApnsServiceBroker(config);

         apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
         {

             aggregateEx.Handle(ex =>
             {

                 // See what kind of exception it was to further diagnose
                 if (ex is ApnsNotificationException)
                 {
                     var notificationException = (ApnsNotificationException)ex;

                     // Deal with the failed notification
                     var apnsNotification = notificationException.Notification;
                     var statusCode = notificationException.ErrorStatusCode;

                     if (!string.IsNullOrEmpty(dbCon))
                         Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleUsersInGroupNotification-" + groupid, "Apple Notification Failed: ID=: " + apnsNotification, new Exception(), dbCon);

                     //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                     //{

                     //    file.WriteLine("Apple Notification Failed: ID=" + apnsNotification + ", Code=" + statusCode);
                     //}
                 }
                 else
                 {
                     if (!string.IsNullOrEmpty(dbCon))
                         Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleUsersInGroupNotification-" + groupid, "Apple Notification Failed for some unknown reason: ", new Exception(), dbCon);

                     // Inner exception might hold more useful information like an ApnsConnectionException           
                     //Console.WriteLine("Apple Notification Failed for some unknown reason : {ex.InnerException}");
                     //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                     //{

                     //    file.WriteLine("Apple Notification Failed for some unknown reason : {ex.InnerException}");
                     //}
                 }

                 // Mark it as handled
                 return true;
             });
         };

         apnsBroker.OnNotificationSucceeded += (notification) =>
         {
             if (!string.IsNullOrEmpty(dbCon))
                 Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("SendtoAppleUsersInGroupNotification-" + groupid, "Apple Notification Sent!-" + DateTime.Now.ToString(), new Exception(), dbCon);

             //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
             //{

             //    file.WriteLine("Apple Group Notification Sent!");
             //}
         };
                    // Start the broker
                    apnsBroker.Start();

                    //foreach (var deviceToken in MY_DEVICE_TOKENS) {
                    // Queue a notification to send
                    var devicelist = PushNotificationDevice.GetAllPushNotificationDevicesByGroupId(groupid, dbConnection);
                    foreach (var dev in devicelist)
                    {
                        if (!string.IsNullOrEmpty(dev.DeviceToken) && dev.DeviceType == (int)PushNotificationDeviceType.Apple)
                        {
                            apnsBroker.QueueNotification(new ApnsNotification
                            {
                                Tag = dev.DeviceToken,
                                DeviceToken = dev.DeviceToken,
                                Payload = JObject.Parse("{\"aps\":{\"badge\":0 ,\"alert\":\"" + message + "\",\"sound\":\"default\"}}")
                            });
                        }
                    }
                    apnsBroker.Stop();
                    //var appleCert = File.ReadAllBytes(appleCertificatePath);
                    //push.RegisterAppleService(new ApplePushChannelSettings(appleCert, appleCertificatePassword));


                    //push.StopAllServices();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("PushNofiticationClient", "SendtoAppleUsersInGroupNotification", ex, CommonUtility.dbConnection);
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                //{

                //    file.WriteLine(ex.Message + "-SendtoAppleUsersInGroupNotification");
                //}
            }
        }
    }
}