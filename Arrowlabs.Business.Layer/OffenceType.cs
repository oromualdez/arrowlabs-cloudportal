﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class OffenceType
    {
        public int OffenceTypeId { get; set; }
        public string OffenceTypeDesc { get; set; }
        public string OffenceTypeDesc_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public int AccountId { get; set; }
        public int OffenceCategoryId { get; set; }
        public string AccountName { get; set; }
        public string OffenceCategory { get; set; }
        public int CustomerId { get; set; }

        public int SiteId { get; set; }

        public DatabaseOperation Operation { get; set; }

        public static List<OffenceType> GetAllOffenceType(string dbConnection)
        {
            var offenceTypes = new List<OffenceType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceType", connection);


                sqlCommand.CommandText = "GetAllOffenceType";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offenceType = new OffenceType();

                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    if (columns.Contains("AccountId") && reader["AccountId"] != DBNull.Value)
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains( "OffenceCategoryId") && reader["OffenceCategoryId"] != DBNull.Value)
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);

                    if (columns.Contains( "OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();

                    if (columns.Contains( "AccountName") && reader["AccountName"] != DBNull.Value)
                        offenceType.AccountName = reader["AccountName"].ToString();
                    offenceTypes.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offenceTypes;
        }

        public static List<OffenceType> GetAllOffenceTypeByOffenceCategoryId(int id, string dbConnection)
        {
            var offences = new List<OffenceType>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceTypeByOffenceCategoryId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offenceType = new OffenceType();

                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId") && reader["SiteId"] != DBNull.Value)
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                    if (columns.Contains("OffenceCategoryId") && reader["OffenceCategoryId"] != DBNull.Value)
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);

                    if (columns.Contains("OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();

                    if (columns.Contains("AccountName") && reader["AccountName"] != DBNull.Value)
                        offenceType.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    offences.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static List<OffenceType> GetAllOffenceTypeByCId(int id, string dbConnection)
        {
            var offences = new List<OffenceType>();
 
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceTypeByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList(); 
                while (reader.Read())
                {
                    var offenceType = new OffenceType();

                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId") && reader["SiteId"] != DBNull.Value)
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                    if (columns.Contains( "OffenceCategoryId") && reader["OffenceCategoryId"] != DBNull.Value)
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);

                    if (columns.Contains( "OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();

                    if (columns.Contains( "AccountName") && reader["AccountName"] != DBNull.Value)
                        offenceType.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    offences.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static List<OffenceType> GetAllOffenceTypeBySiteId(int id, string dbConnection)
        {
            var offences = new List<OffenceType>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceTypeBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offenceType = new OffenceType();

                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId") && reader["SiteId"] != DBNull.Value)
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                    if (columns.Contains("OffenceCategoryId") && reader["OffenceCategoryId"] != DBNull.Value)
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);

                    if (columns.Contains("OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();

                    if (columns.Contains("AccountName") && reader["AccountName"] != DBNull.Value)
                        offenceType.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    offences.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static List<OffenceType> GetAllOffenceTypeByLevel5(int id, string dbConnection)
        {
            var offences = new List<OffenceType>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceTypeByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offenceType = new OffenceType();

                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId") && reader["SiteId"] != DBNull.Value)
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                    if (columns.Contains("OffenceCategoryId") && reader["OffenceCategoryId"] != DBNull.Value)
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);

                    if (columns.Contains("OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();

                    if (columns.Contains("AccountName") && reader["AccountName"] != DBNull.Value)
                        offenceType.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    offences.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static OffenceType GetOffenceTypeByNameAndCId(string name, int id, string dbConnection)
        {
            OffenceType offenceType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceTypeByNameAndCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offenceType = new OffenceType();
                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                    if (columns.Contains( "OffenceCategoryId"))
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);
                    if (columns.Contains( "OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();
                    if (columns.Contains( "AccountName"))
                        offenceType.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                }
                connection.Close();
                reader.Close();
            }
            return offenceType;
        }

        public static OffenceType GetOffenceTypeByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            OffenceType offenceType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceTypeByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offenceType = new OffenceType();
                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                    if (columns.Contains( "OffenceCategoryId"))
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);
                    if (columns.Contains( "OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();
                    if (columns.Contains( "AccountName"))
                        offenceType.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                }
                connection.Close();
                reader.Close();
            }
            return offenceType;
        }
         
        public static OffenceType GetOffenceTypeById(int id, string dbConnection)
        {
            OffenceType offenceType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceTypeById", connection);
                 
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offenceType = new OffenceType();
                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId"))
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                    if (columns.Contains("OffenceCategoryId"))
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);
                    if (columns.Contains("OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();
                    if (columns.Contains("AccountName"))
                        offenceType.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                }
                connection.Close();
                reader.Close();
            }
            return offenceType;
        }

        public static List<OffenceType> GetAllOffenceTypeByDate(DateTime updatedDate, string dbConnection)
        {
            var offenceTypes = new List<OffenceType>();           
            var offenceTypAudits = OffenceType_Audit.GetAllOffenceType_AuditByDate(updatedDate, dbConnection);
            DataSet dataset = new DataSet();
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("OFFENCETYPEID");
            foreach (var offenceTypAudit in offenceTypAudits)
            {
                if (offenceTypAudit.Operation == "Updated" || offenceTypAudit.Operation == "Inserted")
                {
                    var dr = dataTable.NewRow();
                    dr["OFFENCETYPEID"] = offenceTypAudit.OfenceTypeId;
                    dataTable.Rows.Add(dr);
                }
                else
                {
                    var offenceType = new OffenceType();
                    offenceType.OffenceTypeId = offenceTypAudit.OfenceTypeId;
                    offenceType.Operation = DatabaseOperation.Deleted;
                    offenceType.OffenceTypeDesc = DatabaseOperation.Unknown.ToString();
                    offenceType.OffenceTypeDesc_AR = DatabaseOperation.Unknown.ToString();
                    offenceType.CreatedBy = DatabaseOperation.Unknown.ToString();
                    offenceType.CreatedDate = DateTime.Now;
                    offenceType.UpdatedDate = DateTime.Now;
                    offenceTypes.Add(offenceType);
                }
            }
            if (dataTable.Rows.Count > 0)
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllOffenceTypeByDate", connection);
                    sqlCommand.Parameters.AddWithValue("@OffenceTypes", dataTable);               

             
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "GetAllOffenceTypeByDate";

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var offenceType = new OffenceType();

                        offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                        offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                        offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                        offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                        offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());

                        offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                        offenceTypes.Add(offenceType);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            return offenceTypes;
        }

        public static int InsertorUpdateOffenceType(OffenceType offenceType, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var id = 0;
            if (offenceType != null)
            {
                id = offenceType.OffenceTypeId;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateOffenceType", connection);

                    cmd.Parameters.Add(new SqlParameter("@OFFENCETYPEID", SqlDbType.Int));
                    cmd.Parameters["@OFFENCETYPEID"].Value = offenceType.OffenceTypeId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = offenceType.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = offenceType.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@OffenceCategoryId", SqlDbType.Int));
                    cmd.Parameters["@OffenceCategoryId"].Value = offenceType.OffenceCategoryId;

                    cmd.Parameters.Add(new SqlParameter("@OFFENCETYPEDESC", SqlDbType.NVarChar));
                    cmd.Parameters["@OFFENCETYPEDESC"].Value = offenceType.OffenceTypeDesc;

                    cmd.Parameters.Add(new SqlParameter("@OFFENCETYPEDESC_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@OFFENCETYPEDESC_AR"].Value = offenceType.OffenceTypeDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = offenceType.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = offenceType.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = offenceType.CreatedBy.ToLower();

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateOffenceType";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            offenceType.OffenceTypeId = id;
                            var audit = new OffenceTypeAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, offenceType.UpdatedBy);
                            Reflection.CopyProperties(offenceType, audit);
                            OffenceTypeAudit.InsertOffenceTypeAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateOffenceType", ex, auditDbConnection);
                    }

                }
            }
            return id;
        }

        public static bool DeleteOffenceTypeById(int colorCode, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteOffenceTypeById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = colorCode;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteOffenceTypeById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static DateTime GetLastEffectiveDate(string dbConnection)
        {
            var effectiveDate = DateTime.Now;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceType_Audit_EffectiveDate", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetOffenceType_Audit_EffectiveDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["EffectiveDate"] != DBNull.Value)
                        effectiveDate = Convert.ToDateTime(reader["EffectiveDate"].ToString());
                }
                connection.Close();
                reader.Close();
            }
            return effectiveDate;
        }

        public static OffenceType GetOffenceTypeByName(string name, string dbConnection)
        {
            OffenceType offenceType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceTypeByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offenceType = new OffenceType();
                    offenceType.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offenceType.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
                    offenceType.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                    if (columns.Contains( "OffenceCategoryId"))
                        offenceType.OffenceCategoryId = Convert.ToInt32(reader["OffenceCategoryId"]);
                    if (columns.Contains( "OffenceCategory") && reader["OffenceCategory"] != DBNull.Value)
                        offenceType.OffenceCategory = reader["OffenceCategory"].ToString();
                    if (columns.Contains( "AccountName"))
                        offenceType.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                }
                connection.Close();
                reader.Close();
            }
            return offenceType;
        }
    }
    
    internal class OffenceType_Audit
    {
        public int OfenceTypeId { get; set; }
        public string Operation { get; set; }
        public DateTime EffectiveDate { get; set; }

        public static List<OffenceType_Audit> GetAllOffenceType_AuditByDate(DateTime updatedDate, string dbConnection)
        {
            var offenceType_Audits = new List<OffenceType_Audit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceType_AuditByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetAllOffenceType_AuditByDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var offenceType_Audit = new OffenceType_Audit();

                    if (reader["OFFENCETYPEID"] != DBNull.Value)
                        offenceType_Audit.OfenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());

                    offenceType_Audit.Operation = reader["Operation"].ToString();
                    offenceType_Audit.EffectiveDate = Convert.ToDateTime(reader["EffectiveDate"].ToString());
                    offenceType_Audits.Add(offenceType_Audit);
                }
                connection.Close();
                reader.Close();
            }
            return offenceType_Audits;
        }
    }
}