﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class AssetPlan
    {
        public int Id { get; set; } 
        public int AssetId { get; set; }
        public string AssetName { get; set; }
        public string  AssigneeName { get; set; }
        public int AssigneeType { get; set; }
        public int AssigneeId { get; set; }
        public int TaskType { get; set; }
        public int ChecklistId { get; set; }
        public string PlanName { get; set; }
        public string Recurring { get; set; }
        public bool isRecurring { get; set; }
        public string TaskName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdateBy { get; set; }
        public string TaskDescription { get; set; }
        public string PlanTime { get; set; }
        public int Priority { get; set; }
        public bool isSignatures { get; set; }
        public string TaskTypeName { get; set; }
        public string ChecklistName { get; set; }
        public string CustomerUName { get; set; }
        public string CustomerAName { get; set; }
        public int SiteId { get; set; }
        public bool isTemplate { get; set; }
        public int CustomerId { get; set; }
        //new added incase needed
        public int ManagerId { get; set; }

        public int IncidentId { get; set; }

        public int TaskLinkId { get; set; }

        public int ProjectId { get; set; }

        public int CustId { get; set; }

        public int ContractId { get; set; }

        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public DateTime? PlanDateTime { get; set; }
        public int ScheduleId { get; set; }
        public int CustTimeZone { get; set; }
        
        public static int InsertorUpdateAssetPlan(AssetPlan userTask, string dbConnection, string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = userTask.Id;
            try
            {
                var action = (userTask.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                if (userTask != null)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertorUpdateAssetPlan", connection);

                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = userTask.Id;
                        cmd.Parameters.Add(new SqlParameter("@AssetId", SqlDbType.Int));
                        cmd.Parameters["@AssetId"].Value = userTask.AssetId;
                        cmd.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                        cmd.Parameters["@AssigneeType"].Value = userTask.AssigneeType;
                        cmd.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                        cmd.Parameters["@AssigneeId"].Value = userTask.AssigneeId;
                        cmd.Parameters.Add(new SqlParameter("@TaskType", SqlDbType.Int));
                        cmd.Parameters["@TaskType"].Value = userTask.TaskType;
                        cmd.Parameters.Add(new SqlParameter("@ChecklistId", SqlDbType.Int));
                        cmd.Parameters["@ChecklistId"].Value = userTask.ChecklistId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = userTask.CustomerId;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = userTask.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@Priority", SqlDbType.Int));
                        cmd.Parameters["@Priority"].Value = userTask.Priority;
                          
                        cmd.Parameters.Add(new SqlParameter("@ContractId", SqlDbType.Int));
                        cmd.Parameters["@ContractId"].Value = userTask.ContractId;

                        cmd.Parameters.Add(new SqlParameter("@CustId", SqlDbType.Int));
                        cmd.Parameters["@CustId"].Value = userTask.CustId;

                        cmd.Parameters.Add(new SqlParameter("@ProjectId", SqlDbType.Int));
                        cmd.Parameters["@ProjectId"].Value = userTask.ProjectId;

                        cmd.Parameters.Add(new SqlParameter("@TaskLinkId", SqlDbType.Int));
                        cmd.Parameters["@TaskLinkId"].Value = userTask.TaskLinkId;

                        cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                        cmd.Parameters["@IncidentId"].Value = userTask.IncidentId;

                        cmd.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                        cmd.Parameters["@ManagerId"].Value = userTask.ManagerId;

                        cmd.Parameters.Add(new SqlParameter("@TaskName", SqlDbType.NVarChar));
                        cmd.Parameters["@TaskName"].Value = userTask.TaskName;

                        cmd.Parameters.Add(new SqlParameter("@PlanName", SqlDbType.NVarChar));
                        cmd.Parameters["@PlanName"].Value = userTask.PlanName;

                        cmd.Parameters.Add(new SqlParameter("@Recurring", SqlDbType.NVarChar));
                        cmd.Parameters["@Recurring"].Value = userTask.Recurring;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = userTask.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@UpdateBy", SqlDbType.NVarChar));
                        cmd.Parameters["@UpdateBy"].Value = userTask.UpdateBy;

                        cmd.Parameters.Add(new SqlParameter("@TaskDescription", SqlDbType.NVarChar));
                        cmd.Parameters["@TaskDescription"].Value = userTask.TaskDescription;

                        cmd.Parameters.Add(new SqlParameter("@PlanTime", SqlDbType.NVarChar));
                        cmd.Parameters["@PlanTime"].Value = userTask.PlanTime;

                        cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                        cmd.Parameters["@StartDate"].Value = userTask.StartDate;

                        cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                        cmd.Parameters["@EndDate"].Value = userTask.EndDate;

                        cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreateDate"].Value = userTask.CreateDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = userTask.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@isRecurring", SqlDbType.TinyInt));
                        cmd.Parameters["@isRecurring"].Value = userTask.isRecurring;

                        cmd.Parameters.Add(new SqlParameter("@isSignatures", SqlDbType.TinyInt));
                        cmd.Parameters["@isSignatures"].Value = userTask.isSignatures;

                        cmd.Parameters.Add(new SqlParameter("@isTemplate", SqlDbType.TinyInt));
                        cmd.Parameters["@isTemplate"].Value = userTask.isTemplate;

                        cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                        cmd.Parameters["@Latitude"].Value = userTask.Latitude;

                        cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                        cmd.Parameters["@Longitude"].Value = userTask.Longitude;

                        cmd.Parameters.Add(new SqlParameter("@AssigneeName", SqlDbType.NVarChar));
                        cmd.Parameters["@AssigneeName"].Value = userTask.AssigneeName;

                        
                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.CommandText = "InsertorUpdateAssetPlan";

                        cmd.ExecuteNonQuery();

                        if (id == 0)
                            id = (int)returnParameter.Value;

                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {
                                userTask.Id = id;
                                var audit = new Arrowlabs.Mims.Audit.Models.AssetPlanAudit();
                                audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, userTask.UpdateBy);
                                Reflection.CopyProperties(userTask, audit);
                                Arrowlabs.Mims.Audit.Models.AssetPlanAudit.InsertorUpdateAssetPlanAudit(audit, auditDbConnection);
                            }
                        }
                        catch (Exception ex)
                        {
                            MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateAssetPlan", ex, dbConnection);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("AssetPlan", "InsertorUpdateAssetPlan", ex, dbConnection);
            }
            return id;
        }

        public static AssetPlan GetAssetPlanFromSqlReader(SqlDataReader resultSet)
        {
            var userTask = new AssetPlan();
            var columns = Enumerable.Range(0, resultSet.FieldCount).Select(resultSet.GetName).ToList();
            if (resultSet["Id"] != DBNull.Value)
                userTask.Id = Convert.ToInt32(resultSet["Id"].ToString());
            if (resultSet["PlanName"] != DBNull.Value)
                userTask.PlanName = resultSet["PlanName"].ToString();
            if (resultSet["TaskName"] != DBNull.Value)
                userTask.TaskName = resultSet["TaskName"].ToString();

            if (columns.Contains("CustomerUName") && resultSet["CustomerUName"] != DBNull.Value)
                userTask.CustomerUName = resultSet["CustomerUName"].ToString();

            if (columns.Contains("AssetName") && resultSet["AssetName"] != DBNull.Value)
                userTask.AssetName = resultSet["AssetName"].ToString();
             
            if (columns.Contains("TaskTypeName") && resultSet["TaskTypeName"] != DBNull.Value)
                userTask.TaskTypeName = resultSet["TaskTypeName"].ToString();
            if (resultSet["TaskType"] != DBNull.Value)
                userTask.TaskType = Convert.ToInt32(resultSet["TaskType"].ToString());
            if (columns.Contains("PlanDateTime") && resultSet["PlanDateTime"] != DBNull.Value)
                userTask.PlanDateTime = Convert.ToDateTime( resultSet["PlanDateTime"].ToString());

            if (columns.Contains("ScheduleId") && resultSet["ScheduleId"] != DBNull.Value)
                userTask.ScheduleId = Convert.ToInt32(resultSet["ScheduleId"].ToString());

            if (columns.Contains("AssigneeName") && resultSet["AssigneeName"] != DBNull.Value)
                userTask.AssigneeName = resultSet["AssigneeName"].ToString();

            if (columns.Contains("ChecklistName") && resultSet["ChecklistName"] != DBNull.Value)
                userTask.ChecklistName = resultSet["ChecklistName"].ToString();

            if (columns.Contains("PlanTime") && resultSet["PlanTime"] != DBNull.Value)
                userTask.PlanTime = resultSet["PlanTime"].ToString();

            if (resultSet["CreateDate"] != DBNull.Value)
                userTask.CreateDate = Convert.ToDateTime(resultSet["CreateDate"].ToString());
            if (resultSet["UpdatedDate"] != DBNull.Value)
                userTask.UpdatedDate = Convert.ToDateTime(resultSet["UpdatedDate"].ToString());
            if (resultSet["CreatedBy"] != DBNull.Value)
                userTask.CreatedBy = resultSet["CreatedBy"].ToString();

            if (resultSet["StartDate"] != DBNull.Value)
                userTask.StartDate = Convert.ToDateTime(resultSet["StartDate"].ToString());
            if (resultSet["EndDate"] != DBNull.Value)
                userTask.EndDate = Convert.ToDateTime(resultSet["EndDate"].ToString());

            if (resultSet["TaskDescription"] != DBNull.Value)
                userTask.TaskDescription = resultSet["TaskDescription"].ToString();
            if (resultSet["AssigneeType"] != DBNull.Value)
                userTask.AssigneeType = Convert.ToInt32(resultSet["AssigneeType"].ToString());
            if (resultSet["AssigneeId"] != DBNull.Value)
                userTask.AssigneeId = Convert.ToInt32(resultSet["AssigneeId"].ToString());
            if (resultSet["isSignatures"] != DBNull.Value)
                userTask.isSignatures = Convert.ToBoolean(resultSet["isSignatures"].ToString());
            if (resultSet["Priority"] != DBNull.Value)
                userTask.Priority = Convert.ToInt32(resultSet["Priority"].ToString());
            if (resultSet["ChecklistId"] != DBNull.Value)
                userTask.ChecklistId = (!String.IsNullOrEmpty(resultSet["ChecklistId"].ToString())) ? Convert.ToInt32(resultSet["ChecklistId"].ToString()) : 0;
            if (resultSet["isRecurring"] != DBNull.Value)
                userTask.isRecurring = Convert.ToBoolean(resultSet["isRecurring"].ToString());

            if (resultSet["Longitude"] != DBNull.Value)
                userTask.Longitude = Convert.ToDouble(resultSet["Longitude"].ToString());

            if (resultSet["Latitude"] != DBNull.Value)
                userTask.Latitude = Convert.ToDouble(resultSet["Latitude"].ToString());

            if (resultSet["isTemplate"] != DBNull.Value)
                userTask.isTemplate = Convert.ToBoolean(resultSet["isTemplate"].ToString());

            if (resultSet["Recurring"] != DBNull.Value)
                userTask.Recurring = resultSet["Recurring"].ToString();

            if (resultSet["AssetId"] != DBNull.Value)
                userTask.AssetId = Convert.ToInt32(resultSet["AssetId"].ToString());

            if (resultSet["SiteId"] != DBNull.Value)
                userTask.SiteId = Convert.ToInt32(resultSet["SiteId"].ToString());

            if (resultSet["CustomerId"] != DBNull.Value)
                userTask.CustomerId = Convert.ToInt32(resultSet["CustomerId"].ToString());

            if (resultSet["ContractId"] != DBNull.Value)
                userTask.ContractId = Convert.ToInt32(resultSet["ContractId"].ToString());

            if (resultSet["CustId"] != DBNull.Value)
                userTask.CustId = Convert.ToInt32(resultSet["CustId"].ToString());

            if (resultSet["ProjectId"] != DBNull.Value)
                userTask.ProjectId = Convert.ToInt32(resultSet["ProjectId"].ToString());

            if (resultSet["TaskLinkId"] != DBNull.Value)
                userTask.TaskLinkId = Convert.ToInt32(resultSet["TaskLinkId"].ToString());

            if (resultSet["IncidentId"] != DBNull.Value)
                userTask.IncidentId = Convert.ToInt32(resultSet["IncidentId"].ToString());

            if (resultSet["ManagerId"] != DBNull.Value)
                userTask.ManagerId = Convert.ToInt32(resultSet["ManagerId"].ToString());

            if (columns.Contains("TimeZone") && resultSet["TimeZone"] != DBNull.Value)
                userTask.CustTimeZone = Convert.ToInt32(resultSet["TimeZone"].ToString());

            if (columns.Contains("CustomerAName") && resultSet["CustomerAName"] != DBNull.Value)
            {
                userTask.CustomerAName = resultSet["CustomerAName"].ToString();

                if (userTask.AssigneeType == (int)TaskAssigneeType.Group)
                {
                    userTask.CustomerAName = userTask.AssigneeName;
                } 
            }

            return userTask;
        }
        public static List<AssetPlan> GetAllAssetPlanScheduleByDate(DateTime date, string dbConnection)
        {
            var customEvents = new List<AssetPlan>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanScheduleByDate";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@date", SqlDbType.DateTime));
                sqlCommand.Parameters["@date"].Value = date;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlan> GetAllAssetPlanBySiteId(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlan>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanBySiteId";
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlan> GetAllAssetPlanByCId(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlan>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanByCId";
                sqlCommand.Parameters.Add(new SqlParameter("@CId", SqlDbType.Int));
                sqlCommand.Parameters["@CId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlan> GetAllAssetPlanByLevel5(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlan>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanByLevel5";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlan> GetAllAssetPlanByTaskId(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlan>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanByTaskId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlan> GetAllAssetPlanByAssetId(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlan>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanByAssetId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static AssetPlan GetAssetPlanById(int siteId, string dbConnection)
        {
            var customEvent = new AssetPlan();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAssetPlanById";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customEvent = GetAssetPlanFromSqlReader(reader);

                }
                connection.Close();
                reader.Close();
            }
            return customEvent;
        }

        public static List<AssetPlan> GetAllAssetPlan(string dbConnection)
        {
            var customEvents = new List<AssetPlan>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlan";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static void DeleteAssetPlanById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteAssetPlanById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteAssetPlanById";

                cmd.ExecuteNonQuery();
            }
        }
    }
}
