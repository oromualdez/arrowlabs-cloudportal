﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class TamplateDriverOffence
    {
        public Guid Identifier { get; set; }
        public string OffenceDate { get; set; }
        public string OffenceTime { get; set; }
        public string PlateNumber { get; set; }
        public string PlateSource { get; set; }
        public string PlateCode { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleColor { get; set; }
        public string Offence1 { get; set; }
        public string Offence2 { get; set; }
        public string Offence3 { get; set; }
        public string Offence4 { get; set; }
        public string UserName { get; set; }
        public byte[] VideoPath { get; set; }
        public byte[] ImagePath1 { get; set; }
        public byte[] ImagePath2 { get; set; }
        public byte[] ImagePath3 { get; set; }
        public byte[] ImagePath4 { get; set; }
        public byte[] ImagePath5 { get; set; }
        public byte[] ImagePath6 { get; set; }
        public byte[] OffenceImagePath { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ReceiveDate { get; set; }

        public int SiteId { get; set; }
    }
}
