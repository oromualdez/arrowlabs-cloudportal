﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class HealthCheckAudit
    {
        public enum DeviceStatus : int
        {
            Unknown = -1,
            Offline = 0,
            Online = 1,
            Error = 2
        }
        public BaseAudit BaseAudit { get; set; }
        public int Status { get; set; }
        public string MacAddress { get; set; }
        public string PCName { get; set; }
        public string StatusInfo { get; set; }
        public bool isOffline { get; set; }
        public bool isOnline { get; set; }
        public bool isError { get; set; }
        public int SiteId { get; set; }

        public static bool InsertHealthCheckAudit(HealthCheckAudit healthCheckAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(healthCheckAudit.BaseAudit, dbConnection);


            if (healthCheckAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertHealthCheckAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Status", SqlDbType.Int).Value = healthCheckAudit.Status;
                    cmd.Parameters.Add("@MacAddress", SqlDbType.NVarChar).Value = healthCheckAudit.MacAddress;
                    cmd.Parameters.Add("@PCName", SqlDbType.NVarChar).Value = healthCheckAudit.PCName;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = healthCheckAudit.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        private static HealthCheckAudit GetHealthCheckAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var healthCheckAudit = new HealthCheckAudit();
            healthCheckAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("MacAddress") && reader["MacAddress"] != DBNull.Value)
                healthCheckAudit.MacAddress = reader["MacAddress"].ToString();
            if (columns.Contains("PCName") && reader["PCName"] != DBNull.Value)
                healthCheckAudit.PCName = reader["PCName"].ToString();
            if (columns.Contains("Status") && reader["Status"] != DBNull.Value)
                healthCheckAudit.Status = Convert.ToInt16(reader["Status"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                healthCheckAudit.SiteId = Convert.ToInt16(reader["SiteId"].ToString());
            if (healthCheckAudit.Status == 0)
            {
                healthCheckAudit.isOffline = true;
                healthCheckAudit.StatusInfo = "Offline";
            }
            else if (healthCheckAudit.Status == 1)
            {
                healthCheckAudit.isOnline = true;
                healthCheckAudit.StatusInfo = "Online";
            }
            else if (healthCheckAudit.Status == 2)
            {
                healthCheckAudit.isError = true;
                healthCheckAudit.StatusInfo = "Warning";
            }


            return healthCheckAudit;
        }
        public static List<HealthCheckAudit> GetAllHealthCheckAudit(string dbConnection)
        {
            var healthCheckAudits = new List<HealthCheckAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllHealthCheckAudit‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var healthCheckAudit =  GetHealthCheckAuditFromSqlReader(reader);
                    healthCheckAudits.Add(healthCheckAudit);
                }
                connection.Close();
                reader.Close();
            }
            return healthCheckAudits;
        }
    }
}
