﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ChatUserAudit
    {
        public BaseAudit BaseAudit { get; set; }  
        public int Id { get; set; }
        public int UserId { get; set; }
        public int LocationId { get; set; }
        public int SiteId { get; set; }
        public string UserName { get; set; }
        public string ConnectionId { get; set; }
        public bool Active { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        public string UpdatedBy { get; set; }

        public static bool InsertChatUserAudit(ChatUserAudit chatUser, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(chatUser.BaseAudit, dbConnection);

            if (chatUser != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertChatUserAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = chatUser.Id;
                    cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = chatUser.UserName;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = chatUser.CreatedDate;
                    cmd.Parameters.Add("@LoginDate", SqlDbType.DateTime).Value = chatUser.LoginDate;
                    cmd.Parameters.Add("@LogoutDate", SqlDbType.DateTime).Value = chatUser.LogoutDate;
                    cmd.Parameters.Add("@ConnectionId", SqlDbType.NVarChar).Value = chatUser.ConnectionId;
                    cmd.Parameters.Add("@Active", SqlDbType.Bit).Value = chatUser.Active;
                    cmd.Parameters.Add("@LocationId", SqlDbType.Int).Value = chatUser.LocationId;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = chatUser.SiteId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                }
                return true;
            }
            return false;
        }
    }
}
