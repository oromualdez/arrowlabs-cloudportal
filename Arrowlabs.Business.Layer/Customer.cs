﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class Customer
    {
        public int Id { get; set; }
        public bool Status { get; set; }
        public string LocationAddress { get; set; }
        public string ContractRef { get; set; }
        public string ClientName { get; set; }
        public string SalesAccount { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        //public string LocationDesc { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int PPMVisitPerYear { get; set; }
        public int PPMTotalHour { get; set; }
        public int TotalPPMHour { get; set; }
        public int SRVisitPerYear { get; set; }
        public int SRHourVisitPer { get; set; }
        public int SRTotalHours { get; set; }
        public int SRTotal { get; set; }
        public string ScopeOfWorks { get; set; }
        public string SLA { get; set; }
        public string AnnualContractValue { get; set; }
        public string TotalContractValue { get; set; }
        public string MonthlyContractValue { get; set; }
        public string Remarks { get; set; }
        public string PaymentTerms { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int SiteId { get; set; }
        public string CustomerType { get; set; }

        public int CustomerInfoId { get; set; }

        public string CustomerUName { get; set; }
        public string CustomerFullName { get; set; }
        public static int InsertOrUpdateCustomer(Customer customer, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = customer.Id;

            var action = (customer.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (customer != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateCustomer", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customer.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = customer.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Bit));
                    cmd.Parameters["@Status"].Value = customer.Status;

                    cmd.Parameters.Add(new SqlParameter("@LocationAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationAddress"].Value = customer.LocationAddress;

                    cmd.Parameters.Add(new SqlParameter("@ContractRef", SqlDbType.NVarChar));
                    cmd.Parameters["@ContractRef"].Value = customer.ContractRef;

                    cmd.Parameters.Add(new SqlParameter("@ClientName", SqlDbType.NVarChar));
                    cmd.Parameters["@ClientName"].Value = customer.ClientName;

                    cmd.Parameters.Add(new SqlParameter("@SalesAccount", SqlDbType.NVarChar));
                    cmd.Parameters["@SalesAccount"].Value = customer.SalesAccount;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = customer.Name;

                    cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                    cmd.Parameters["@Email"].Value = customer.Email;

                    cmd.Parameters.Add(new SqlParameter("@ContactNo", SqlDbType.NVarChar));
                    cmd.Parameters["@ContactNo"].Value = customer.ContactNo;

                    cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                    cmd.Parameters["@StartDate"].Value = customer.StartDate;

                    cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                    cmd.Parameters["@EndDate"].Value = customer.EndDate;

                    cmd.Parameters.Add(new SqlParameter("@PPMVisitPerYear", SqlDbType.Int));
                    cmd.Parameters["@PPMVisitPerYear"].Value = customer.PPMVisitPerYear;

                    cmd.Parameters.Add(new SqlParameter("@PPMTotalHour", SqlDbType.Int));
                    cmd.Parameters["@PPMTotalHour"].Value = customer.PPMTotalHour;

                    cmd.Parameters.Add(new SqlParameter("@TotalPPMHour", SqlDbType.Int));
                    cmd.Parameters["@TotalPPMHour"].Value = customer.TotalPPMHour;

                    cmd.Parameters.Add(new SqlParameter("@SRVisitPerYear", SqlDbType.Int));
                    cmd.Parameters["@SRVisitPerYear"].Value = customer.SRVisitPerYear;

                    cmd.Parameters.Add(new SqlParameter("@SRHourVisitPer", SqlDbType.Int));
                    cmd.Parameters["@SRHourVisitPer"].Value = customer.SRHourVisitPer;

                    cmd.Parameters.Add(new SqlParameter("@SRTotalHours", SqlDbType.Int));
                    cmd.Parameters["@SRTotalHours"].Value = customer.SRTotalHours;

                    cmd.Parameters.Add(new SqlParameter("@SRTotal", SqlDbType.Int));
                    cmd.Parameters["@SRTotal"].Value = customer.SRTotal;

                    cmd.Parameters.Add(new SqlParameter("@ScopeOfWorks", SqlDbType.NVarChar));
                    cmd.Parameters["@ScopeOfWorks"].Value = customer.ScopeOfWorks;

                    cmd.Parameters.Add(new SqlParameter("@SLA", SqlDbType.NVarChar));
                    cmd.Parameters["@SLA"].Value = customer.SLA;

                    cmd.Parameters.Add(new SqlParameter("@AnnualContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@AnnualContractValue"].Value = customer.AnnualContractValue;

                    cmd.Parameters.Add(new SqlParameter("@TotalContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@TotalContractValue"].Value = customer.TotalContractValue;

                    cmd.Parameters.Add(new SqlParameter("@MonthlyContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@MonthlyContractValue"].Value = customer.MonthlyContractValue;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = customer.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@PaymentTerms", SqlDbType.NVarChar));
                    cmd.Parameters["@PaymentTerms"].Value = customer.PaymentTerms;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customer.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = customer.UpdatedDate;


                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customer.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customer.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customer.SiteId;
                   
                    cmd.Parameters.Add(new SqlParameter("@CustomerType", SqlDbType.NVarChar));
                    cmd.Parameters["@CustomerType"].Value = customer.CustomerType;
                    
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateCustomer";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            customer.Id = id;
                            var audit = new CustomerAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customer.UpdatedBy);
                            Reflection.CopyProperties(customer, audit);
                            CustomerAudit.InsertCustomerAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateCustomer", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static Customer GetCustomerFromSqlReader(SqlDataReader reader)
        {
            var customer = new Customer();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                customer.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Status"] != DBNull.Value)
                customer.Status = Convert.ToBoolean(reader["Status"].ToString());
            if (reader["LocationAddress"] != DBNull.Value)
                customer.LocationAddress = reader["LocationAddress"].ToString();
            if (reader["ContractRef"] != DBNull.Value)
                customer.ContractRef = reader["ContractRef"].ToString();
            if (reader["ClientName"] != DBNull.Value)
                customer.ClientName = reader["ClientName"].ToString();
            if (reader["SalesAccount"] != DBNull.Value)
                customer.SalesAccount = reader["SalesAccount"].ToString();
            if (reader["Name"] != DBNull.Value)
                customer.Name = reader["Name"].ToString();
            if (reader["Email"] != DBNull.Value)
                customer.Email = reader["Email"].ToString();

            if (reader["ContactNo"] != DBNull.Value)
                customer.ContactNo = reader["ContactNo"].ToString();
            if (reader["StartDate"] != DBNull.Value)
                customer.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());

            if (columns.Contains( "EndDate") && reader["EndDate"] != DBNull.Value)
                customer.EndDate = Convert.ToDateTime(reader["EndDate"]);

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                customer.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("CustomerFullName") && reader["CustomerFullName"] != DBNull.Value)
                customer.CustomerFullName = reader["CustomerFullName"].ToString();
            

            if (columns.Contains( "CustomerInfoId") && reader["CustomerInfoId"] != DBNull.Value)
            {
                customer.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"]);
            }

            if (columns.Contains( "PPMVisitPerYear") && reader["PPMVisitPerYear"] != DBNull.Value)
            {
                customer.PPMVisitPerYear = Convert.ToInt32(reader["PPMVisitPerYear"]);
            }
            if (columns.Contains( "LocationDesc") && reader["LocationDesc"] != DBNull.Value)
            {
              //  customer.LocationDesc = reader["LocationDesc"].ToString();
            }
            if (reader["SRTotalHours"] != DBNull.Value)
                customer.SRTotalHours = Convert.ToInt32(reader["SRTotalHours"].ToString());
            
            if (reader["PPMTotalHour"] != DBNull.Value)
                customer.PPMTotalHour = Convert.ToInt32(reader["PPMTotalHour"].ToString());
            if (reader["TotalPPMHour"] != DBNull.Value)
                customer.TotalPPMHour = Convert.ToInt32(reader["TotalPPMHour"].ToString());

            if (reader["SRVisitPerYear"] != DBNull.Value)
                customer.SRVisitPerYear = Convert.ToInt32(reader["SRVisitPerYear"].ToString());

            if (reader["SRHourVisitPer"] != DBNull.Value)
                customer.SRHourVisitPer = Convert.ToInt32(reader["SRHourVisitPer"].ToString());

            if (reader["ScopeOfWorks"] != DBNull.Value)
                customer.ScopeOfWorks = reader["ScopeOfWorks"].ToString();

            if (reader["SRTotal"] != DBNull.Value)
                customer.SRTotal = Convert.ToInt32(reader["SRTotal"].ToString());


            if (reader["SLA"] != DBNull.Value)
                customer.SLA = reader["SLA"].ToString();

            if (reader["AnnualContractValue"] != DBNull.Value)
                customer.AnnualContractValue = reader["AnnualContractValue"].ToString();

            if (reader["MonthlyContractValue"] != DBNull.Value)
                customer.MonthlyContractValue = reader["MonthlyContractValue"].ToString();

            if (reader["Remarks"] != DBNull.Value)
                customer.Remarks = reader["Remarks"].ToString();

            if (reader["PaymentTerms"] != DBNull.Value)
                customer.PaymentTerms = reader["PaymentTerms"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                customer.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            
            if (reader["UpdatedDate"] != DBNull.Value)
                customer.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            
            if (reader["CreatedBy"] != DBNull.Value)
                customer.CreatedBy = reader["CreatedBy"].ToString();
            
            if (reader["UpdatedBy"] != DBNull.Value)
                customer.UpdatedBy = reader["UpdatedBy"].ToString();
            
            if (reader["SiteId"] != DBNull.Value)
                customer.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerType"] != DBNull.Value)
                customer.CustomerType = reader["CustomerType"].ToString();
         
            return customer;
        }
        public static List<Customer> GetAllCustomers(string dbConnection)
        {
            var customers = new List<Customer>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomers";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customerSystemType = GetCustomerFromSqlReader(reader);
                    customers.Add(customerSystemType);
                }
                connection.Close();
                reader.Close();
            }
            return customers;
        }

        public static void DeleteCustomerById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteCustomerById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteCustomerById";

                cmd.ExecuteNonQuery();
            }
        }
        public static List<Customer> GetAllCustomersBySiteId(int siteId, string dbConnection)
        {
            var customers = new List<Customer>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomersBySiteId";
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                   var customer = GetCustomerFromSqlReader(reader);
                    customers.Add(customer);
                }
                connection.Close();
                reader.Close();
            }
            return customers;
        }
        public static Customer GetCustomerById(int customerId, string dbConnection)
        {
            Customer customer = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetCustomerById";
                sqlCommand.Parameters.Add(new SqlParameter("@customerId", SqlDbType.Int));
                sqlCommand.Parameters["@customerId"].Value = customerId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customer = GetCustomerFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return customer;
        }
        public static Customer GetCustomerByClientName(string clientName, string dbConnection)
        {
            Customer customer = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetCustomerByClientName";
                sqlCommand.Parameters.Add(new SqlParameter("@ClientName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ClientName"].Value = clientName;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customer = GetCustomerFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return customer;
        }

        public static Customer GetCustomerByClientNameSiteIdAndCId(string clientName,int siteid,int cid, string dbConnection)
        {
            Customer customer = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetCustomerByClientNameSiteIdAndCId";
                sqlCommand.Parameters.Add(new SqlParameter("@ClientName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ClientName"].Value = clientName;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.Parameters.Add(new SqlParameter("@CId", SqlDbType.Int));
                sqlCommand.Parameters["@CId"].Value = cid;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customer = GetCustomerFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return customer;
        }

        public static List<Customer> GetAllCustomersByCustomerInfoId(int customerInfoId, string dbConnection)
        {
            var customers = new List<Customer>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomersByCustomerInfoId";
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerInfoId"].Value = customerInfoId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customer = GetCustomerFromSqlReader(reader);
                    customers.Add(customer);
                }
                connection.Close();
                reader.Close();
            }
            return customers;
        }

        public static List<Customer> GetAllCustomersByLevel5(int customerInfoId, string dbConnection)
        {
            var customers = new List<Customer>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomersByLevel5";
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = customerInfoId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customer = GetCustomerFromSqlReader(reader);
                    customers.Add(customer);
                }
                connection.Close();
                reader.Close();
            }
            return customers;
        }

    }
}
