﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class GroupDeviceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int TaskId { get; set; }
        public string DeviceName { get; set; }
        public string GroupName { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int SiteId { get; set; }

        private static GroupDeviceAudit GetGroupDeviceAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var groupDeviceAudit = new GroupDeviceAudit();
            groupDeviceAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                groupDeviceAudit.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                groupDeviceAudit.Name = reader["Name"].ToString();
            if (columns.Contains("CreateDate") && reader["CreateDate"] != DBNull.Value)
                groupDeviceAudit.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                groupDeviceAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("TaskId") && reader["TaskId"] != DBNull.Value)
                groupDeviceAudit.TaskId = Convert.ToInt16(reader["TaskId"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                groupDeviceAudit.SiteId = Convert.ToInt16(reader["SiteId"].ToString());
   
            if (columns.Contains("DeviceName") && reader["DeviceName"] != DBNull.Value && !String.IsNullOrWhiteSpace(reader["DeviceName"].ToString()))
            {
                groupDeviceAudit.Name = reader["DeviceName"].ToString();
                groupDeviceAudit.DeviceName = reader["DeviceName"].ToString();
            }
            else if (columns.Contains("GroupName") && reader["GroupName"] != DBNull.Value && !String.IsNullOrWhiteSpace(reader["GroupName"].ToString()))
            {
                groupDeviceAudit.Name = reader["GroupName"].ToString();
                groupDeviceAudit.GroupName = reader["GroupName"].ToString();
            }

            return groupDeviceAudit;
        }
        public static bool InsertGroupDeviceAudit(GroupDeviceAudit groupDeviceAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(groupDeviceAudit.BaseAudit, dbConnection);

            if (groupDeviceAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertGroupDeviceAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = groupDeviceAudit.Id;
                    cmd.Parameters.Add("@TaskId", SqlDbType.Int).Value = groupDeviceAudit.TaskId;
                    cmd.Parameters.Add("@DeviceName", SqlDbType.NVarChar).Value = groupDeviceAudit.DeviceName;
                    cmd.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = groupDeviceAudit.GroupName;
                    cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = groupDeviceAudit.CreateDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = groupDeviceAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = groupDeviceAudit.SiteId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static List<GroupDeviceAudit> GetAllGroupDeviceAudit(string dbConnection)
        {
            var groupDeviceAudits = new List<GroupDeviceAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllGroupDeviceAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var groupDeviceTask = GetGroupDeviceAuditFromSqlReader(reader);
                    groupDeviceAudits.Add(groupDeviceTask);
                }
                connection.Close();
                reader.Close();
            }
            return groupDeviceAudits;
        }
    }
}
