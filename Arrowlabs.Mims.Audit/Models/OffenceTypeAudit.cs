﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class OffenceTypeAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int OffenceTypeId { get; set; }
        public string OffenceTypeDesc { get; set; }
        public string OffenceTypeDesc_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        
        private static OffenceTypeAudit GetOffenceTypeAuditFromSqlReader(SqlDataReader reader)
        {
            var offenceTypeAudit = new OffenceTypeAudit();
            offenceTypeAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("OFFENCETYPEID") && reader["OFFENCETYPEID"] != DBNull.Value)
                offenceTypeAudit.OffenceTypeId = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
            if (columns.Contains("OFFENCETYPEDESC") && reader["OFFENCETYPEDESC"] != DBNull.Value)
                offenceTypeAudit.OffenceTypeDesc = reader["OFFENCETYPEDESC"].ToString();
            if (columns.Contains("OFFENCETYPEDESC_AR") && reader["OFFENCETYPEDESC_AR"] != DBNull.Value)
                offenceTypeAudit.OffenceTypeDesc_AR = reader["OFFENCETYPEDESC_AR"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                offenceTypeAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                offenceTypeAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                offenceTypeAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return offenceTypeAudit;
        }
        public static List<OffenceTypeAudit> GetAllOffenceTypeAudit(string dbConnection)
        {
            var offenceTypes = new List<OffenceTypeAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceTypeAudit", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var offenceType = GetOffenceTypeAuditFromSqlReader(reader);
                    offenceTypes.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offenceTypes;
        }
        public static bool InsertOffenceTypeAudit(OffenceTypeAudit offenceTypeAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(offenceTypeAudit.BaseAudit, dbConnection);

            if (offenceTypeAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOffenceTypeAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@OFFENCETYPEID", SqlDbType.Int).Value = offenceTypeAudit.OffenceTypeId;

                    cmd.Parameters.Add("@OFFENCETYPEDESC", SqlDbType.NVarChar).Value = offenceTypeAudit.OffenceTypeDesc;

                    cmd.Parameters.Add("@OFFENCETYPEDESC_AR", SqlDbType.NVarChar).Value = offenceTypeAudit.OffenceTypeDesc_AR;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = offenceTypeAudit.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = offenceTypeAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = offenceTypeAudit.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = offenceTypeAudit.CustomerId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }

}
