﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class VehicleColorAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int ColorCode { get; set; }
        public string ColorDesc { get; set; }
        public string ColorDesc_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        
        public static VehicleColorAudit GetVehicleColorAuditFromSqlReader(SqlDataReader reader)
        {
            var vehicleColorAudit = new VehicleColorAudit();
            vehicleColorAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("COLORCODE") && reader["COLORCODE"] != DBNull.Value)
                vehicleColorAudit.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
            if (columns.Contains("COLORDESC") && reader["COLORDESC"] != DBNull.Value)
                vehicleColorAudit.ColorDesc = reader["COLORDESC"].ToString();
            if (columns.Contains("COLORDESC_AR") && reader["COLORDESC_AR"] != DBNull.Value)
                vehicleColorAudit.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                vehicleColorAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                vehicleColorAudit.CreatedBy = reader["CreatedBy"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                vehicleColorAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return vehicleColorAudit;
        }
        public static List<VehicleColorAudit> GetAllVehicleColorAudit(string dbConnection)
        {
            var vehicleColorAudits = new List<VehicleColorAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleColorAudit", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var vehicleColorAudit = GetVehicleColorAuditFromSqlReader(reader);
                    vehicleColorAudits.Add(vehicleColorAudit);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleColorAudits;
        }
        public static bool InsertVehicleColorAudit(VehicleColorAudit vehicleColorAudit, string dbConnection)
        {
            if (vehicleColorAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertVehicleColorAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(vehicleColorAudit.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@COLORCODE", SqlDbType.Int).Value = vehicleColorAudit.ColorCode;

                    cmd.Parameters.Add("@COLORDESC", SqlDbType.NVarChar).Value = vehicleColorAudit.ColorDesc;

                    cmd.Parameters.Add("@COLORDESC_AR", SqlDbType.NVarChar).Value = vehicleColorAudit.ColorDesc_AR;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = vehicleColorAudit.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = vehicleColorAudit.CreatedBy;
                   
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = vehicleColorAudit.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = vehicleColorAudit.CustomerId;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }

}
