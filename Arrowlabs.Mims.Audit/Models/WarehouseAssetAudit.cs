﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Arrowlabs.Mims.Audit.Models
{
    public class WarehouseAssetAudit
    {
        public BaseAudit BaseAudit { get; set; } 
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int AssetCategoryId { get; set; }
        public int SiteId { get; set; }
        public string AssetCategoryName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public int CustomerId { get; set; }

        public static bool InsertWarehouseAssetAudit(WarehouseAssetAudit assetAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(assetAudit.BaseAudit, dbConnection);

            if (assetAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertWarehouseAssetAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = assetAudit.Id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = assetAudit.Name;
                    cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = assetAudit.Quantity;
                    cmd.Parameters.Add("@AssetCategoryId", SqlDbType.Int).Value = assetAudit.AssetCategoryId;
                    cmd.Parameters.Add("@LocationId", SqlDbType.Int).Value = assetAudit.LocationId;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = assetAudit.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = assetAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = assetAudit.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = assetAudit.CustomerId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
