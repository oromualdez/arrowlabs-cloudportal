﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class UserRole
    {
        public int Id { get; set; }
        public string Name { get; set; }


        public static List<UserRole> GetAllUserRole(string dbConnection)
        {
            var userTasks = new List<UserRole>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserRole", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    var userRole = new UserRole();

                    userRole.Id = Convert.ToInt32(resultSet["Id"].ToString());
                    userRole.Name = resultSet["Name"].ToString();

                    userTasks.Add(userRole);
                }
                connection.Close();
                resultSet.Close();
            }
            return userTasks;
        }


        public static bool InsertorUpdateUserRole(UserRole userRole, string dbConnection)
        {
            if (userRole != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateUserRole", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = userRole.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = userRole.Name;
                    /*
                                        cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                                        cmd.Parameters["@CreateDate"].Value = userRole.CreateDate;

                                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                                        cmd.Parameters["@UpdatedDate"].Value = userRole.UpdatedDate;

                                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                                        cmd.Parameters["@CreatedBy"].Value = userRole.CreatedBy;
                    */
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        public static bool DeleteUserRoleByRoleId(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("DeleteUserRoleByRoleId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteNonQuery();
                connection.Close();

                return true;
            }
        }
    }
}
