﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class ConfigSettings
    {
        public int Id { get; set; }
        public string DeviceMAC { get; set; }
        public int Port { get; set; }
        public int Interval { get; set; }       
        public string ServerIP { get; set; }
        public string ConnectionString { get; set; }
        public string ServerConnection { get; set; }
        public string GPSURL { get; set; }
        public string SettingsPassword { get; set; }
        public string SettingsMasterPassword { get; set; }
        public string SnapFilePath { get; set; }
        public string PCName { get; set; }
        public string ClientID { get; set; }
        public string AppVersion { get; set; }
        public int RegisterState { get; set; }
        public int CurfewState { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? CurrentDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CurfewDays { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string PSDPortal { get; set; }
        public string CSVPath { get; set; }
        public string GPSPath { get; set; }
        public int ThirdPartyPort { get; set; }
        public int VideoLength { get; set; }
        public int NoImages { get; set; }
        public int HealthCheckInterval { get; set; }
        public int AlarmDeleteInterval { get; set; }
        public int GPSInterval { get; set; }
        public bool IntervalState{ get; set; }
        public bool ThirdPartyPortState{ get; set; }
        public bool VideoLengthState{ get; set; }
        public bool SettingPassState{ get; set; }
        public bool NumberOfImageState{ get; set; }
        public bool CSVPathState{ get; set; }
        public bool HealthCheckState{ get; set; }
        public bool AlarmDeleteState{ get; set; }
        public bool GPSIntervalState{ get; set; }
        public float MIMSMobileGPSInterval { get; set; }
        public int RTSPStreamPort { get; set; }
        public int VideoPort { get; set; }
        public string Language { get; set; }
        public string ChatServerUrl { get; set; }
        public int MessagePort { get; set; }

        public bool Protocol { get; set; }

        public static ConfigSettings GetConfigSettings(string dbConnection)
        {
            ConfigSettings configClass = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetConfigSettings", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    configClass = new ConfigSettings();
                    configClass.DeviceMAC = reader["DeviceMAC"].ToString();

                    if (!string.IsNullOrEmpty(reader["Port"].ToString()))
                        configClass.Port = Convert.ToInt32(reader["Port"].ToString());
                    if (!string.IsNullOrEmpty(reader["Interval"].ToString()))
                        configClass.Interval = Convert.ToInt32(reader["Interval"].ToString());
                    if (!string.IsNullOrEmpty(reader["HealthCheckInterval"].ToString()))
                        configClass.HealthCheckInterval = Convert.ToInt32(reader["HealthCheckInterval"].ToString());
                    if (!string.IsNullOrEmpty(reader["AlarmDeleteInterval"].ToString()))
                        configClass.AlarmDeleteInterval = Convert.ToInt32(reader["AlarmDeleteInterval"].ToString());
                    if (!string.IsNullOrEmpty(reader["GPSInterval"].ToString()))
                        configClass.GPSInterval = Convert.ToInt32(reader["GPSInterval"].ToString());
                   
                    
                    configClass.ServerIP = reader["ServerIP"].ToString();
                    configClass.ConnectionString = reader["ConnectionString"].ToString();
                    configClass.ServerConnection = reader["ServerConnection"].ToString();
                    configClass.GPSURL = reader["GPSURL"].ToString();
                    configClass.SettingsPassword = reader["SettingsPassword"].ToString();
                    configClass.SettingsMasterPassword = reader["SettingsMasterPassword"].ToString();
                    configClass.SnapFilePath = reader["SnapFilePath"].ToString();
                    configClass.PCName = reader["PCName"].ToString();
                    configClass.ClientID = reader["ClientID"].ToString();
                    configClass.AppVersion = reader["AppVersion"].ToString();

                    if (!string.IsNullOrEmpty(reader["RegisterState"].ToString()))
                        configClass.RegisterState = Convert.ToInt32(reader["RegisterState"].ToString());
                    if (!string.IsNullOrEmpty(reader["CurfewState"].ToString()))
                        configClass.CurfewState = Convert.ToInt32(reader["CurfewState"].ToString());

                    configClass.CurfewDays = reader["CurfewDays"].ToString();
                    configClass.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"].ToString());
                    configClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    configClass.CurrentDate = Convert.ToDateTime(reader["CurrentDate"].ToString());
                    configClass.UpdatedBy = reader["UpdatedBy"].ToString();
                    configClass.UpdatedDate = Convert.ToDateTime(reader["UpdateDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["ThirdPartyPort"].ToString()))
                        configClass.ThirdPartyPort = Convert.ToInt32(reader["ThirdPartyPort"].ToString());
                    if (!string.IsNullOrEmpty(reader["VideoLength"].ToString()))
                        configClass.VideoLength = Convert.ToInt32(reader["VideoLength"].ToString());
                    if (!string.IsNullOrEmpty(reader["NoImages"].ToString()))
                        configClass.NoImages = Convert.ToInt32(reader["NoImages"].ToString());

                    configClass.PSDPortal = reader["PSDPortal"].ToString();
                    configClass.CSVPath = reader["CSVPath"].ToString();
                    configClass.GPSPath = reader["GPSPath"].ToString();

                    if (!string.IsNullOrEmpty(reader["IntervalState"].ToString()))
                        configClass.IntervalState = Convert.ToBoolean(reader["IntervalState"]);
                    if (!string.IsNullOrEmpty(reader["ThirdPartyPortState"].ToString()))
                        configClass.ThirdPartyPortState = Convert.ToBoolean(reader["ThirdPartyPortState"]);
                    if (!string.IsNullOrEmpty(reader["VideoLengthState"].ToString()))
                        configClass.VideoLengthState = Convert.ToBoolean(reader["VideoLengthState"]);
                    if (!string.IsNullOrEmpty(reader["SettingPassState"].ToString()))
                        configClass.SettingPassState = Convert.ToBoolean(reader["SettingPassState"]);
                    if (!string.IsNullOrEmpty(reader["NumberOfImageState"].ToString()))
                        configClass.NumberOfImageState = Convert.ToBoolean(reader["NumberOfImageState"]);
                    if (!string.IsNullOrEmpty(reader["CSVPathState"].ToString()))
                        configClass.CSVPathState = Convert.ToBoolean(reader["CSVPathState"]);
                    if (!string.IsNullOrEmpty(reader["HealthCheckState"].ToString()))
                        configClass.HealthCheckState = Convert.ToBoolean(reader["HealthCheckState"]);
                    if (!string.IsNullOrEmpty(reader["AlarmDeleteState"].ToString()))
                        configClass.AlarmDeleteState = Convert.ToBoolean(reader["AlarmDeleteState"]);

                    if (!string.IsNullOrEmpty(reader["GPSIntervalState"].ToString()))
                        configClass.GPSIntervalState = Convert.ToBoolean(reader["GPSIntervalState"]);


                    try
                    {
                        if (!string.IsNullOrEmpty(reader["LanguageName"].ToString()))
                            configClass.Language = reader["LanguageName"].ToString();
                        if (!string.IsNullOrEmpty(reader["MIMSMobileGPSInterval"].ToString()))
                            configClass.MIMSMobileGPSInterval = Convert.ToSingle(reader["MIMSMobileGPSInterval"].ToString());

                        if (!string.IsNullOrEmpty(reader["RTSPStreamPort"].ToString()))
                            configClass.RTSPStreamPort = Convert.ToInt32(reader["RTSPStreamPort"].ToString());

                        if (!string.IsNullOrEmpty(reader["VideoPort"].ToString()))
                            configClass.VideoPort = Convert.ToInt32(reader["VideoPort"].ToString());
                        if (!string.IsNullOrEmpty(reader["ChatServerUrl"].ToString()))
                            configClass.ChatServerUrl = reader["ChatServerUrl"].ToString();
                        if (!string.IsNullOrEmpty(reader["MessagePort"].ToString()))
                            configClass.MessagePort = Convert.ToInt32(reader["MessagePort"].ToString());

                        if (columns.Contains( "Protocol") & reader["Protocol"] != null)
                            configClass.Protocol = Convert.ToBoolean(reader["Protocol"]);

                    }
                    catch(Exception ex) {
                        MIMSLog.MIMSLogSave("BusinessLayer", "GetConfigSettings", ex, dbConnection);
                    }
                }
                connection.Close();
                reader.Close();
            }
            return configClass;
        }
        public static ConfigSettings GetConfigSettingsClientSide(string dbConnection)
        {
            ConfigSettings configClass = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetConfigSettings", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetConfigSettings";


                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    configClass = new ConfigSettings();
                    configClass.DeviceMAC = reader["DeviceMAC"].ToString();

                    if (!string.IsNullOrEmpty(reader["Port"].ToString()))
                        configClass.Port = Convert.ToInt32(reader["Port"].ToString());
                    if (!string.IsNullOrEmpty(reader["Interval"].ToString()))
                        configClass.Interval = Convert.ToInt32(reader["Interval"].ToString());
                    if (!string.IsNullOrEmpty(reader["GPSInterval"].ToString()))
                        configClass.GPSInterval = Convert.ToInt32(reader["GPSInterval"].ToString());

                    configClass.ServerIP = reader["ServerIP"].ToString();
                    configClass.ConnectionString = reader["ConnectionString"].ToString();
                    configClass.ServerConnection = reader["ServerConnection"].ToString();
                    configClass.GPSURL = reader["GPSURL"].ToString();
                    configClass.SettingsPassword = reader["SettingsPassword"].ToString();
                    configClass.SettingsMasterPassword = reader["SettingsMasterPassword"].ToString();
                    configClass.SnapFilePath = reader["SnapFilePath"].ToString();
                    configClass.PCName = reader["PCName"].ToString();
                    configClass.ClientID = reader["ClientID"].ToString();
                    configClass.AppVersion = reader["AppVersion"].ToString();

                    if (!string.IsNullOrEmpty(reader["RegisterState"].ToString()))
                        configClass.RegisterState = Convert.ToInt32(reader["RegisterState"].ToString());
                    if (!string.IsNullOrEmpty(reader["CurfewState"].ToString()))
                        configClass.CurfewState = Convert.ToInt32(reader["CurfewState"].ToString());

                    configClass.CurfewDays = reader["CurfewDays"].ToString();
                    configClass.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"].ToString());
                    configClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    configClass.CurrentDate = Convert.ToDateTime(reader["CurrentDate"].ToString());
                    configClass.UpdatedBy = reader["UpdatedBy"].ToString();
                    configClass.UpdatedDate = Convert.ToDateTime(reader["UpdateDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["ThirdPartyPort"].ToString()))
                        configClass.ThirdPartyPort = Convert.ToInt32(reader["ThirdPartyPort"].ToString());
                    if (!string.IsNullOrEmpty(reader["VideoLength"].ToString()))
                        configClass.VideoLength = Convert.ToInt32(reader["VideoLength"].ToString());
                    if (!string.IsNullOrEmpty(reader["NoImages"].ToString()))
                        configClass.NoImages = Convert.ToInt32(reader["NoImages"].ToString());
                    configClass.PSDPortal = reader["PSDPortal"].ToString();
                    configClass.CSVPath = reader["CSVPath"].ToString();
                    configClass.GPSPath = reader["GPSPath"].ToString();

                    if (!string.IsNullOrEmpty(reader["GPSIntervalState"].ToString()))
                        configClass.GPSIntervalState = Convert.ToBoolean(reader["GPSIntervalState"]);

                    try
                    {
                        if (!string.IsNullOrEmpty(reader["LanguageName"].ToString()))
                            configClass.Language = reader["LanguageName"].ToString();

                        if (!string.IsNullOrEmpty(reader["ChatServerUrl"].ToString()))
                            configClass.ChatServerUrl = reader["ChatServerUrl"].ToString();
                        if (!string.IsNullOrEmpty(reader["VideoPort"].ToString()))
                            configClass.VideoPort = Convert.ToInt32(reader["VideoPort"].ToString());
                        //if (!string.IsNullOrEmpty(reader["MIMSMobileGPSInterval"].ToString()))
                        //    configClass.MIMSMobileGPSInterval = Convert.ToSingle(reader["MIMSMobileGPSInterval"].ToString());


                    }
                    catch { }
                }



                connection.Close();
                reader.Close();
            }
            return configClass;
        }
        public static ConfigSettings GetConfigSettingsByDate(DateTime updatedDate, string dbConnection)
        {
            var configClass = new ConfigSettings();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetConfigSettingsByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetConfigSettingsByDate";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    configClass.DeviceMAC = reader["DeviceMAC"].ToString();

                    if (!string.IsNullOrEmpty(reader["Port"].ToString()))
                        configClass.Port = Convert.ToInt32(reader["Port"].ToString());
                    if (!string.IsNullOrEmpty(reader["Interval"].ToString()))
                        configClass.Interval = Convert.ToInt32(reader["Interval"].ToString());
                    if (!string.IsNullOrEmpty(reader["HealthCheckInterval"].ToString()))
                        configClass.HealthCheckInterval = Convert.ToInt32(reader["HealthCheckInterval"].ToString());
                    if (!string.IsNullOrEmpty(reader["AlarmDeleteInterval"].ToString()))
                        configClass.AlarmDeleteInterval = Convert.ToInt32(reader["AlarmDeleteInterval"].ToString());
                    if (!string.IsNullOrEmpty(reader["GPSInterval"].ToString()))
                        configClass.GPSInterval = Convert.ToInt32(reader["GPSInterval"].ToString());
                    if (!string.IsNullOrEmpty(reader["MIMSMobileGPSInterval"].ToString()))
                        configClass.MIMSMobileGPSInterval = Convert.ToSingle(reader["MIMSMobileGPSInterval"].ToString());
                    
                    configClass.ServerIP = reader["ServerIP"].ToString();
                    configClass.ConnectionString = reader["ConnectionString"].ToString();
                    configClass.ServerConnection = reader["ServerConnection"].ToString();
                    configClass.GPSURL = reader["GPSURL"].ToString();
                    configClass.SettingsPassword = reader["SettingsPassword"].ToString();
                    configClass.SettingsMasterPassword = reader["SettingsMasterPassword"].ToString();
                    configClass.SnapFilePath = reader["SnapFilePath"].ToString();
                    configClass.PCName = reader["PCName"].ToString();
                    configClass.ClientID = reader["ClientID"].ToString();
                    configClass.AppVersion = reader["AppVersion"].ToString();

                    if (!string.IsNullOrEmpty(reader["RegisterState"].ToString()))
                        configClass.RegisterState = Convert.ToInt32(reader["RegisterState"].ToString());
                    if (!string.IsNullOrEmpty(reader["CurfewState"].ToString()))
                        configClass.CurfewState = Convert.ToInt32(reader["CurfewState"].ToString());
                    configClass.CurfewDays = reader["CurfewDays"].ToString();
                    configClass.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"].ToString());
                    configClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    configClass.CurrentDate = Convert.ToDateTime(reader["CurrentDate"].ToString());
                    configClass.UpdatedBy = reader["UpdatedBy"].ToString();
                    configClass.UpdatedDate = Convert.ToDateTime(reader["UpdateDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["ThirdPartyPort"].ToString()))
                        configClass.ThirdPartyPort = Convert.ToInt32(reader["ThirdPartyPort"].ToString());
                    if (!string.IsNullOrEmpty(reader["VideoLength"].ToString()))
                        configClass.VideoLength = Convert.ToInt32(reader["VideoLength"].ToString());
                    if (!string.IsNullOrEmpty(reader["NoImages"].ToString()))
                        configClass.NoImages = Convert.ToInt32(reader["NoImages"].ToString());

                    configClass.PSDPortal = reader["PSDPortal"].ToString();
                    configClass.CSVPath = reader["CSVPath"].ToString();
                    configClass.GPSPath = reader["GPSPath"].ToString();

                    configClass.IntervalState = Convert.ToBoolean(reader["IntervalState"]);
                    configClass.ThirdPartyPortState = Convert.ToBoolean(reader["ThirdPartyPortState"]);
                    configClass.VideoLengthState = Convert.ToBoolean(reader["VideoLengthState"]);
                    configClass.SettingPassState = Convert.ToBoolean(reader["SettingPassState"]);
                    configClass.NumberOfImageState = Convert.ToBoolean(reader["NumberOfImageState"]);
                    configClass.CSVPathState = Convert.ToBoolean(reader["CSVPathState"]);
                    configClass.HealthCheckState = Convert.ToBoolean(reader["HealthCheckState"]);
                    configClass.AlarmDeleteState = Convert.ToBoolean(reader["AlarmDeleteState"]);
                    configClass.GPSIntervalState = Convert.ToBoolean(reader["GPSIntervalState"]);
                    if (!string.IsNullOrEmpty(reader["ChatServerUrl"].ToString()))
                        configClass.ChatServerUrl = reader["ChatServerUrl"].ToString();
                    if (!string.IsNullOrEmpty(reader["MessagePort"].ToString()))
                        configClass.MessagePort = Convert.ToInt32(reader["MessagePort"].ToString());
                }
                connection.Close();
                reader.Close();
            }
            return configClass;
        }

        public static bool InsertOrUpdateConfigSettingsClientSide(ConfigSettings conClass, string dbConnection)
        {
            if (conClass != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateConfigSettings", connection);


                    cmd.Parameters.Add(new SqlParameter("@DeviceMAC", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceMAC"].Value = conClass.DeviceMAC;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = conClass.Port;

                    cmd.Parameters.Add(new SqlParameter("@Interval", SqlDbType.Int));
                    cmd.Parameters["@Interval"].Value = conClass.Interval;

                    cmd.Parameters.Add(new SqlParameter("@ServerIP", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerIP"].Value = conClass.ServerIP;

                    cmd.Parameters.Add(new SqlParameter("@ConnectionString", SqlDbType.NVarChar));
                    cmd.Parameters["@ConnectionString"].Value = conClass.ConnectionString;

                    cmd.Parameters.Add(new SqlParameter("@ServerConnection", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerConnection"].Value = conClass.ServerConnection;

                    cmd.Parameters.Add(new SqlParameter("@GPSURL", SqlDbType.NVarChar));
                    cmd.Parameters["@GPSURL"].Value = conClass.GPSURL;

                    cmd.Parameters.Add(new SqlParameter("@SettingsPassword", SqlDbType.NVarChar));
                    cmd.Parameters["@SettingsPassword"].Value = conClass.SettingsPassword;

                    cmd.Parameters.Add(new SqlParameter("@SettingsMasterPassword", SqlDbType.NVarChar));
                    cmd.Parameters["@SettingsMasterPassword"].Value = conClass.SettingsMasterPassword;

                    cmd.Parameters.Add(new SqlParameter("@SnapFilePath", SqlDbType.NVarChar));
                    cmd.Parameters["@SnapFilePath"].Value = conClass.SnapFilePath;

                    cmd.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                    cmd.Parameters["@PCName"].Value = conClass.PCName;

                    cmd.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    cmd.Parameters["@ClientID"].Value = conClass.ClientID;

                    cmd.Parameters.Add(new SqlParameter("@AppVersion", SqlDbType.NVarChar));
                    cmd.Parameters["@AppVersion"].Value = conClass.AppVersion;

                    cmd.Parameters.Add(new SqlParameter("@RegisterState", SqlDbType.Int));
                    cmd.Parameters["@RegisterState"].Value = conClass.RegisterState;

                    cmd.Parameters.Add(new SqlParameter("@CurfewState", SqlDbType.Int));
                    cmd.Parameters["@CurfewState"].Value = conClass.CurfewState;

                    cmd.Parameters.Add(new SqlParameter("@CurfewDays", SqlDbType.Int));
                    cmd.Parameters["@CurfewDays"].Value = conClass.CurfewDays;

                    cmd.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.DateTime));
                    cmd.Parameters["@ExpiryDate"].Value = conClass.ExpiryDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = conClass.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CurrentDate", SqlDbType.DateTime));
                    cmd.Parameters["@CurrentDate"].Value = conClass.CurrentDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = conClass.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdateDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdateDate"].Value = conClass.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@ThirdPartyPort", SqlDbType.Int));
                    cmd.Parameters["@ThirdPartyPort"].Value = conClass.ThirdPartyPort;

                    cmd.Parameters.Add(new SqlParameter("@VideoLength", SqlDbType.Int));
                    cmd.Parameters["@VideoLength"].Value = conClass.VideoLength;

                    cmd.Parameters.Add(new SqlParameter("@NoImages", SqlDbType.Int));
                    cmd.Parameters["@NoImages"].Value = conClass.NoImages;

                    cmd.Parameters.Add(new SqlParameter("@CSVPath", SqlDbType.NVarChar));
                    cmd.Parameters["@CSVPath"].Value = conClass.CSVPath;

                    cmd.Parameters.Add(new SqlParameter("@PSDPortal", SqlDbType.NVarChar));
                    cmd.Parameters["@PSDPortal"].Value = conClass.PSDPortal;

                    cmd.Parameters.Add(new SqlParameter("@GPSPath", SqlDbType.NVarChar));
                    cmd.Parameters["@GPSPath"].Value = conClass.GPSPath;

                    cmd.Parameters.Add(new SqlParameter("@GPSInterval", SqlDbType.Int));
                    cmd.Parameters["@GPSInterval"].Value = conClass.GPSInterval;

                    cmd.Parameters.Add(new SqlParameter("@GPSIntervalState", SqlDbType.Bit));
                    cmd.Parameters["@GPSIntervalState"].Value = conClass.GPSIntervalState;

                    if (!string.IsNullOrEmpty(conClass.Language))
                    {
                        cmd.Parameters.Add(new SqlParameter("@LanguageName", SqlDbType.NVarChar));
                        cmd.Parameters["@LanguageName"].Value = conClass.Language;
                    }
                    if (!string.IsNullOrEmpty(conClass.ChatServerUrl))
                    {
                        cmd.Parameters.Add(new SqlParameter("@ChatServerUrl", SqlDbType.NVarChar));
                        cmd.Parameters["@ChatServerUrl"].Value = conClass.ChatServerUrl;
                    }

                    cmd.Parameters.Add(new SqlParameter("@VideoPort", SqlDbType.Int));
                    cmd.Parameters["@VideoPort"].Value = conClass.VideoPort;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateConfigSettings";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static bool InsertOrUpdateConfigSettings(ConfigSettings conClass, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)

        {
            if (conClass != null)
            {
                int id = conClass.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateConfigSettings", connection);

                    cmd.Parameters.Add(new SqlParameter("@DeviceMAC", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceMAC"].Value = conClass.DeviceMAC;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = conClass.Port;

                    cmd.Parameters.Add(new SqlParameter("@Interval", SqlDbType.Int));
                    cmd.Parameters["@Interval"].Value = conClass.Interval;

                    cmd.Parameters.Add(new SqlParameter("@ServerIP", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerIP"].Value = conClass.ServerIP;

                    cmd.Parameters.Add(new SqlParameter("@ConnectionString", SqlDbType.NVarChar));
                    cmd.Parameters["@ConnectionString"].Value = conClass.ConnectionString;

                    cmd.Parameters.Add(new SqlParameter("@ServerConnection", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerConnection"].Value = conClass.ServerConnection;

                    cmd.Parameters.Add(new SqlParameter("@GPSURL", SqlDbType.NVarChar));
                    cmd.Parameters["@GPSURL"].Value = conClass.GPSURL;

                    cmd.Parameters.Add(new SqlParameter("@SettingsPassword", SqlDbType.NVarChar));
                    cmd.Parameters["@SettingsPassword"].Value = conClass.SettingsPassword;

                    cmd.Parameters.Add(new SqlParameter("@SettingsMasterPassword", SqlDbType.NVarChar));
                    cmd.Parameters["@SettingsMasterPassword"].Value = conClass.SettingsMasterPassword;

                    cmd.Parameters.Add(new SqlParameter("@SnapFilePath", SqlDbType.NVarChar));
                    cmd.Parameters["@SnapFilePath"].Value = conClass.SnapFilePath;

                    cmd.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                    cmd.Parameters["@PCName"].Value = conClass.PCName;

                    cmd.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    cmd.Parameters["@ClientID"].Value = conClass.ClientID;

                    cmd.Parameters.Add(new SqlParameter("@AppVersion", SqlDbType.NVarChar));
                    cmd.Parameters["@AppVersion"].Value = conClass.AppVersion;

                    cmd.Parameters.Add(new SqlParameter("@RegisterState", SqlDbType.Int));
                    cmd.Parameters["@RegisterState"].Value = conClass.RegisterState;

                    cmd.Parameters.Add(new SqlParameter("@CurfewState", SqlDbType.Int));
                    cmd.Parameters["@CurfewState"].Value = conClass.CurfewState;

                    cmd.Parameters.Add(new SqlParameter("@CurfewDays", SqlDbType.NVarChar));
                    cmd.Parameters["@CurfewDays"].Value = conClass.CurfewDays;

                    cmd.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.DateTime));
                    cmd.Parameters["@ExpiryDate"].Value = conClass.ExpiryDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = conClass.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CurrentDate", SqlDbType.DateTime));
                    cmd.Parameters["@CurrentDate"].Value = conClass.CurrentDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = conClass.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdateDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdateDate"].Value = conClass.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@ThirdPartyPort", SqlDbType.Int));
                    cmd.Parameters["@ThirdPartyPort"].Value = conClass.ThirdPartyPort;

                    cmd.Parameters.Add(new SqlParameter("@VideoLength", SqlDbType.Int));
                    cmd.Parameters["@VideoLength"].Value = conClass.VideoLength;

                    cmd.Parameters.Add(new SqlParameter("@NoImages", SqlDbType.Int));
                    cmd.Parameters["@NoImages"].Value = conClass.NoImages;

                    cmd.Parameters.Add(new SqlParameter("@HealthCheckInterval", SqlDbType.Int));
                    cmd.Parameters["@HealthCheckInterval"].Value = conClass.HealthCheckInterval;

                    cmd.Parameters.Add(new SqlParameter("@AlarmDeleteInterval", SqlDbType.Int));
                    cmd.Parameters["@AlarmDeleteInterval"].Value = conClass.AlarmDeleteInterval;

                    cmd.Parameters.Add(new SqlParameter("@CSVPath", SqlDbType.NVarChar));
                    cmd.Parameters["@CSVPath"].Value = conClass.CSVPath;

                    cmd.Parameters.Add(new SqlParameter("@PSDPortal", SqlDbType.NVarChar));
                    cmd.Parameters["@PSDPortal"].Value = conClass.PSDPortal;

                    cmd.Parameters.Add(new SqlParameter("@GPSPath", SqlDbType.NVarChar));
                    cmd.Parameters["@GPSPath"].Value = conClass.GPSPath;

                    cmd.Parameters.Add(new SqlParameter("@GPSInterval", SqlDbType.Int));
                    cmd.Parameters["@GPSInterval"].Value = conClass.GPSInterval;

                    cmd.Parameters.Add(new SqlParameter("@IntervalState", SqlDbType.Bit));
                    cmd.Parameters["@IntervalState"].Value = conClass.IntervalState;

                    cmd.Parameters.Add(new SqlParameter("@ThirdPartyPortState", SqlDbType.Bit));
                    cmd.Parameters["@ThirdPartyPortState"].Value = conClass.ThirdPartyPortState;

                    cmd.Parameters.Add(new SqlParameter("@VideoLengthState", SqlDbType.Bit));
                    cmd.Parameters["@VideoLengthState"].Value = conClass.VideoLengthState;

                    cmd.Parameters.Add(new SqlParameter("@SettingPassState", SqlDbType.Bit));
                    cmd.Parameters["@SettingPassState"].Value = conClass.SettingPassState;

                    cmd.Parameters.Add(new SqlParameter("@NumberOfImageState", SqlDbType.Bit));
                    cmd.Parameters["@NumberOfImageState"].Value = conClass.NumberOfImageState;

                    cmd.Parameters.Add(new SqlParameter("@CSVPathState", SqlDbType.Bit));
                    cmd.Parameters["@CSVPathState"].Value = conClass.CSVPathState;

                    cmd.Parameters.Add(new SqlParameter("@HealthCheckState", SqlDbType.Bit));
                    cmd.Parameters["@HealthCheckState"].Value = conClass.HealthCheckState;

                    cmd.Parameters.Add(new SqlParameter("@AlarmDeleteState", SqlDbType.Bit));
                    cmd.Parameters["@AlarmDeleteState"].Value = conClass.AlarmDeleteState;

                    cmd.Parameters.Add(new SqlParameter("@GPSIntervalState", SqlDbType.Bit));
                    cmd.Parameters["@GPSIntervalState"].Value = conClass.GPSIntervalState;

                    cmd.Parameters.Add(new SqlParameter("@MIMSMobileGPSInterval", SqlDbType.Float));
                    cmd.Parameters["@MIMSMobileGPSInterval"].Value = conClass.MIMSMobileGPSInterval;

                    cmd.Parameters.Add(new SqlParameter("@RTSPStreamPort", SqlDbType.Int));
                    cmd.Parameters["@RTSPStreamPort"].Value = conClass.RTSPStreamPort;

                    cmd.Parameters.Add(new SqlParameter("@VideoPort", SqlDbType.Int));
                    cmd.Parameters["@VideoPort"].Value = conClass.VideoPort;

                    cmd.Parameters.Add(new SqlParameter("@ChatServerUrl", SqlDbType.NVarChar));
                    cmd.Parameters["@ChatServerUrl"].Value = conClass.ChatServerUrl;
                    
                    cmd.Parameters.Add(new SqlParameter("@MessagePort", SqlDbType.NVarChar));
                    cmd.Parameters["@MessagePort"].Value = conClass.MessagePort;

                    cmd.Parameters.Add(new SqlParameter("@Protocol", SqlDbType.Bit));
                    cmd.Parameters["@Protocol"].Value = conClass.Protocol;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateConfigSettings";

                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new ConfigSettingsAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, conClass.UpdatedBy);
                            Reflection.CopyProperties(conClass, audit);
                            ConfigSettingsAudit.InsertConfigSettingsAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateConfigSettings", ex, dbConnection);
                    }

                }
            }
            return true;
        }

        public static bool InsertOrUpdateConfigSettingsWithLanguage(ConfigSettings conClass, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)

        {
            if (conClass != null)
            {
                int id = conClass.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateConfigSettingsWithLanguage", connection);

                    cmd.Parameters.Add(new SqlParameter("@DeviceMAC", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceMAC"].Value = conClass.DeviceMAC;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = conClass.Port;

                    cmd.Parameters.Add(new SqlParameter("@Interval", SqlDbType.Int));
                    cmd.Parameters["@Interval"].Value = conClass.Interval;

                    cmd.Parameters.Add(new SqlParameter("@ServerIP", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerIP"].Value = conClass.ServerIP;

                    cmd.Parameters.Add(new SqlParameter("@ConnectionString", SqlDbType.NVarChar));
                    cmd.Parameters["@ConnectionString"].Value = conClass.ConnectionString;

                    cmd.Parameters.Add(new SqlParameter("@ServerConnection", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerConnection"].Value = conClass.ServerConnection;

                    cmd.Parameters.Add(new SqlParameter("@GPSURL", SqlDbType.NVarChar));
                    cmd.Parameters["@GPSURL"].Value = conClass.GPSURL;

                    cmd.Parameters.Add(new SqlParameter("@SettingsPassword", SqlDbType.NVarChar));
                    cmd.Parameters["@SettingsPassword"].Value = conClass.SettingsPassword;

                    cmd.Parameters.Add(new SqlParameter("@SettingsMasterPassword", SqlDbType.NVarChar));
                    cmd.Parameters["@SettingsMasterPassword"].Value = conClass.SettingsMasterPassword;

                    cmd.Parameters.Add(new SqlParameter("@SnapFilePath", SqlDbType.NVarChar));
                    cmd.Parameters["@SnapFilePath"].Value = conClass.SnapFilePath;

                    cmd.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                    cmd.Parameters["@PCName"].Value = conClass.PCName;

                    cmd.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                    cmd.Parameters["@ClientID"].Value = conClass.ClientID;

                    cmd.Parameters.Add(new SqlParameter("@AppVersion", SqlDbType.NVarChar));
                    cmd.Parameters["@AppVersion"].Value = conClass.AppVersion;

                    cmd.Parameters.Add(new SqlParameter("@RegisterState", SqlDbType.Int));
                    cmd.Parameters["@RegisterState"].Value = conClass.RegisterState;

                    cmd.Parameters.Add(new SqlParameter("@CurfewState", SqlDbType.Int));
                    cmd.Parameters["@CurfewState"].Value = conClass.CurfewState;

                    cmd.Parameters.Add(new SqlParameter("@CurfewDays", SqlDbType.NVarChar));
                    cmd.Parameters["@CurfewDays"].Value = conClass.CurfewDays;

                    cmd.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.DateTime));
                    cmd.Parameters["@ExpiryDate"].Value = conClass.ExpiryDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = conClass.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CurrentDate", SqlDbType.DateTime));
                    cmd.Parameters["@CurrentDate"].Value = conClass.CurrentDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = conClass.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdateDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdateDate"].Value = conClass.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@ThirdPartyPort", SqlDbType.Int));
                    cmd.Parameters["@ThirdPartyPort"].Value = conClass.ThirdPartyPort;

                    cmd.Parameters.Add(new SqlParameter("@VideoLength", SqlDbType.Int));
                    cmd.Parameters["@VideoLength"].Value = conClass.VideoLength;

                    cmd.Parameters.Add(new SqlParameter("@NoImages", SqlDbType.Int));
                    cmd.Parameters["@NoImages"].Value = conClass.NoImages;

                    cmd.Parameters.Add(new SqlParameter("@HealthCheckInterval", SqlDbType.Int));
                    cmd.Parameters["@HealthCheckInterval"].Value = conClass.HealthCheckInterval;

                    cmd.Parameters.Add(new SqlParameter("@AlarmDeleteInterval", SqlDbType.Int));
                    cmd.Parameters["@AlarmDeleteInterval"].Value = conClass.AlarmDeleteInterval;

                    cmd.Parameters.Add(new SqlParameter("@CSVPath", SqlDbType.NVarChar));
                    cmd.Parameters["@CSVPath"].Value = conClass.CSVPath;

                    cmd.Parameters.Add(new SqlParameter("@PSDPortal", SqlDbType.NVarChar));
                    cmd.Parameters["@PSDPortal"].Value = conClass.PSDPortal;

                    cmd.Parameters.Add(new SqlParameter("@GPSPath", SqlDbType.NVarChar));
                    cmd.Parameters["@GPSPath"].Value = conClass.GPSPath;

                    cmd.Parameters.Add(new SqlParameter("@GPSInterval", SqlDbType.Int));
                    cmd.Parameters["@GPSInterval"].Value = conClass.GPSInterval;

                    cmd.Parameters.Add(new SqlParameter("@IntervalState", SqlDbType.Bit));
                    cmd.Parameters["@IntervalState"].Value = conClass.IntervalState;

                    cmd.Parameters.Add(new SqlParameter("@ThirdPartyPortState", SqlDbType.Bit));
                    cmd.Parameters["@ThirdPartyPortState"].Value = conClass.ThirdPartyPortState;

                    cmd.Parameters.Add(new SqlParameter("@VideoLengthState", SqlDbType.Bit));
                    cmd.Parameters["@VideoLengthState"].Value = conClass.VideoLengthState;

                    cmd.Parameters.Add(new SqlParameter("@SettingPassState", SqlDbType.Bit));
                    cmd.Parameters["@SettingPassState"].Value = conClass.SettingPassState;

                    cmd.Parameters.Add(new SqlParameter("@NumberOfImageState", SqlDbType.Bit));
                    cmd.Parameters["@NumberOfImageState"].Value = conClass.NumberOfImageState;

                    cmd.Parameters.Add(new SqlParameter("@CSVPathState", SqlDbType.Bit));
                    cmd.Parameters["@CSVPathState"].Value = conClass.CSVPathState;

                    cmd.Parameters.Add(new SqlParameter("@HealthCheckState", SqlDbType.Bit));
                    cmd.Parameters["@HealthCheckState"].Value = conClass.HealthCheckState;

                    cmd.Parameters.Add(new SqlParameter("@AlarmDeleteState", SqlDbType.Bit));
                    cmd.Parameters["@AlarmDeleteState"].Value = conClass.AlarmDeleteState;

                    cmd.Parameters.Add(new SqlParameter("@GPSIntervalState", SqlDbType.Bit));
                    cmd.Parameters["@GPSIntervalState"].Value = conClass.GPSIntervalState;

                    cmd.Parameters.Add(new SqlParameter("@MIMSMobileGPSInterval", SqlDbType.Float));
                    cmd.Parameters["@MIMSMobileGPSInterval"].Value = conClass.MIMSMobileGPSInterval;

                    cmd.Parameters.Add(new SqlParameter("@RTSPStreamPort", SqlDbType.Int));
                    cmd.Parameters["@RTSPStreamPort"].Value = conClass.RTSPStreamPort;

                    cmd.Parameters.Add(new SqlParameter("@VideoPort", SqlDbType.Int));
                    cmd.Parameters["@VideoPort"].Value = conClass.VideoPort;

                    cmd.Parameters.Add(new SqlParameter("@ChatServerUrl", SqlDbType.NVarChar));
                    cmd.Parameters["@ChatServerUrl"].Value = conClass.ChatServerUrl;
                    
                    cmd.Parameters.Add(new SqlParameter("@MessagePort", SqlDbType.NVarChar));
                    cmd.Parameters["@MessagePort"].Value = conClass.MessagePort;

                    cmd.Parameters.Add(new SqlParameter("@Language", SqlDbType.NVarChar));
                    cmd.Parameters["@Language"].Value = conClass.Language;

                    cmd.Parameters.Add(new SqlParameter("@Protocol", SqlDbType.Bit));
                    cmd.Parameters["@Protocol"].Value = conClass.Protocol;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateConfigSettingsWithLanguage";

                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new ConfigSettingsAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, conClass.UpdatedBy);
                            Reflection.CopyProperties(conClass, audit);
                            ConfigSettingsAudit.InsertConfigSettingsAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateConfigSettingsWithLanguage", ex, dbConnection);
                    }

                }
            }
            return true;
        }
  
    }
}
