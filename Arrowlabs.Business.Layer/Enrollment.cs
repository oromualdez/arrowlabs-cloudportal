﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class Enrollment
    {
        public int Id { get; set; }
        [DataMember]
        public int CaseId { get; set; }
        [DataMember]
        public string PID { get; set; }
        [DataMember]
        public string YearOfBirth { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string Ethnicity { get; set; }
        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public string ConfirmReason { get; set; }
        [DataMember]
        public int ListCategory { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string TransactionID { get; set; }
        [DataMember]
        public byte[] EnrollImage { get; set; }
        public string TypeName { get; set; }
        public string ListName { get; set; }
        public enum EnrollmentTypes
        {
            FRS = 0,
            ANPR = 1
        }
        public enum GenderTypes
        {
            Female = 0,
            Male = 1
        }
        public enum EthnicityTypes
        {
            Asian = 0,
            Black = 1,
            Caucasian = 2
        }
        public enum ListTypes
        {
            BlackList = 0,
            WhiteList = 1
        }
        public static List<string> GetListTypes()
        {
            return Enum.GetNames(typeof(ListTypes)).ToList();
        }
        public static List<string> GetEnrollmentTypes()
        {
            return Enum.GetNames(typeof(EnrollmentTypes)).ToList();
        }
        public static List<string> GetGenderTypes()
        {
            return Enum.GetNames(typeof(GenderTypes)).ToList();
        }
        public static List<string> GetEthnicityTypes()
        {
            return Enum.GetNames(typeof(EthnicityTypes)).ToList();
        }
        public static int InsertOrUpdateEnrolment(Enrollment enrollment, string dbConnection, bool isAgain,
            string auditDbConnection = "", bool isAuditEnabled = true)

        {
            int id = enrollment.Id;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

            if (enrollment != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateEnrolment", connection);

                    cmd.Parameters.Add(new SqlParameter("@CaseId", SqlDbType.Int));
                    cmd.Parameters["@CaseId"].Value = enrollment.CaseId;

                    cmd.Parameters.Add(new SqlParameter("@PID", SqlDbType.NVarChar));
                    cmd.Parameters["@PID"].Value = enrollment.PID;

                    cmd.Parameters.Add(new SqlParameter("@YearOfBirth", SqlDbType.NVarChar));
                    cmd.Parameters["@YearOfBirth"].Value = enrollment.YearOfBirth;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = enrollment.Name;

                    cmd.Parameters.Add(new SqlParameter("@Gender", SqlDbType.NVarChar));
                    cmd.Parameters["@Gender"].Value = enrollment.Gender;

                    cmd.Parameters.Add(new SqlParameter("@Ethnicity", SqlDbType.NVarChar));
                    cmd.Parameters["@Ethnicity"].Value = enrollment.Ethnicity;

                    cmd.Parameters.Add(new SqlParameter("@Reason", SqlDbType.NVarChar));
                    cmd.Parameters["@Reason"].Value = enrollment.Reason;

                    cmd.Parameters.Add(new SqlParameter("@ListCategory", SqlDbType.Int));
                    cmd.Parameters["@ListCategory"].Value = enrollment.ListCategory;

                    cmd.Parameters.Add(new SqlParameter("@ConfirmReason", SqlDbType.NVarChar));
                    cmd.Parameters["@ConfirmReason"].Value = enrollment.ConfirmReason;

                    cmd.Parameters.Add(new SqlParameter("@TransactionID", SqlDbType.NVarChar));
                    cmd.Parameters["@TransactionID"].Value = enrollment.TransactionID;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = enrollment.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = enrollment.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = enrollment.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = enrollment.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                    cmd.Parameters["@Type"].Value = enrollment.Type;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateEnrolment";

                    cmd.ExecuteNonQuery();
                    if (id == 0)
                        id = (int)returnParameter.Value;

                    if (isAgain)
                        CognitecEnrollment(id, enrollment, dbConnection);


                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            enrollment.Id = id;
                            var audit = new EnrollmentAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, enrollment.UpdatedBy);
                            Reflection.CopyProperties(enrollment, audit);
                            EnrollmentAudit.InsertEnrollmentAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateEnrolment", ex, dbConnection);
                    }




                }
            }
            return id;
        }
        private static void CognitecEnrollment(int id, Enrollment enClass, string dbConnection)
        {
            try
            {
                var fvdbScan = new FVDBScan();
                var ValidService = false;
                foreach (var vs in VerifierService.GetAllVerifierService(dbConnection))
                {
                    try
                    {
                        fvdbScan.Url = vs.Url;
                        fvdbScan.alive();
                        ValidService = true;
                        break;
                    }
                    catch { }
                }

                if (ValidService)
                {
                    Image img = new Image();
                    img.binaryImg = enClass.EnrollImage;
                    // var result = servic.service.identification(img, authName, 10000, 0.0f, 0);
                    var caseprop = new CaseProperty[7];
                    var imageset = new Image[1];
                    imageset[0] = img;

                    var case1 = new CaseProperty();
                    case1.name = "CaseID";
                    case1.value = id.ToString();
                    caseprop[0] = case1;

                    var case2 = new CaseProperty();
                    case2.name = "PID";
                    case2.value = enClass.PID;
                    caseprop[1] = case2;

                    var case3 = new CaseProperty();
                    case3.name = "Gender";
                    case3.value = enClass.Gender;
                    caseprop[2] = case3;

                    var case4 = new CaseProperty();
                    case4.name = "YearOfBirth";
                    case4.value = enClass.YearOfBirth;
                    caseprop[3] = case4;

                    var case5 = new CaseProperty();
                    case5.name = "Ethnicity";
                    case5.value = enClass.Ethnicity;
                    caseprop[4] = case5;

                    var case6 = new CaseProperty();
                    case6.name = "Name";
                    case6.value = enClass.Name;
                    caseprop[5] = case6;

                    var case7 = new CaseProperty();
                    case7.name = "AuthName";
                    case7.value = enClass.CreatedBy;
                    caseprop[6] = case7;               

                    fvdbScan.fvdbaddCase(case1.value, caseprop);
                    var enResult = fvdbScan.enrollmentset(case1.value, imageset, enClass.CreatedBy);

                    enClass.TransactionID = enResult.transactionId;
                    enClass.CaseId = id;
                   
                    InsertOrUpdateEnrolment(enClass, dbConnection, false);
                }
            }
            catch (Exception ex)
            {
            }
        }
        private static Enrollment GetEnrollmentFromSqlReader(SqlDataReader reader)
        {
            var enrollment = new Enrollment();
            if (reader["CaseId"] != DBNull.Value)
                enrollment.CaseId = Convert.ToInt16(reader["CaseId"].ToString());
            if (reader["PID"] != DBNull.Value)
                enrollment.PID = reader["PID"].ToString();
            if (reader["Type"] != DBNull.Value)
                enrollment.Type = Convert.ToInt16(reader["Type"].ToString());

            if (enrollment.Type == (int)EnrollmentTypes.ANPR)
                enrollment.TypeName = "ANPR";
            else
                enrollment.TypeName = "FRS";



            if (reader["YearOfBirth"] != DBNull.Value)
                enrollment.YearOfBirth = reader["YearOfBirth"].ToString();

            if (reader["Name"] != DBNull.Value)
                enrollment.Name = reader["Name"].ToString();
            if (reader["Gender"] != DBNull.Value)
                enrollment.Gender = reader["Gender"].ToString();
            if (reader["Reason"] != DBNull.Value)
                enrollment.Reason = reader["Reason"].ToString();

            if (reader["Ethnicity"] != DBNull.Value)
                enrollment.Ethnicity = reader["Ethnicity"].ToString();
            if (reader["ConfirmReason"] != DBNull.Value)
                enrollment.ConfirmReason = reader["ConfirmReason"].ToString();
            if (reader["ListCategory"] != DBNull.Value)
                enrollment.ListCategory = Convert.ToInt16(reader["ListCategory"].ToString());
            if (reader["ConfirmReason"] != DBNull.Value)
                enrollment.ConfirmReason = reader["ConfirmReason"].ToString();
            if (reader["CreatedBy"] != DBNull.Value)
                enrollment.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedBy"] != DBNull.Value)
                enrollment.UpdatedBy = reader["UpdatedBy"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                enrollment.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                enrollment.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["TransactionID"] != DBNull.Value)
                enrollment.TransactionID = reader["TransactionID"].ToString();

            if (enrollment.ListCategory == (int)ListTypes.BlackList)
                enrollment.ListName = "BlackList";
            else
                enrollment.ListName = "WhiteList";

            return enrollment;
        }
        public static List<Enrollment> GetAllEnrollments(string dbConnection)
        {
            var enrollments = new List<Enrollment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllEnrollment";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var enrollment = GetEnrollmentFromSqlReader(reader);
                    enrollments.Add(enrollment);
                }
                connection.Close();
                reader.Close();
            }
            return enrollments;
        }

        public static List<Enrollment> GetAllEnrollmentByType(int Type, string dbConnection)
        {
            var enrollments = new List<Enrollment>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllEnrollmentByType", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllEnrollmentByType";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var enrollment = GetEnrollmentFromSqlReader(reader);



                    enrollments.Add(enrollment);
                }
                connection.Close();
                reader.Close();
            }
            return enrollments;
        }

        public static List<Enrollment> GetAllEnrollmentBySite(int siteid, string dbConnection)
        {
            var enrollments = new List<Enrollment>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllEnrollmentBySite", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllEnrollmentBySite";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var enrollment = GetEnrollmentFromSqlReader(reader);



                    enrollments.Add(enrollment);
                }
                connection.Close();
                reader.Close();
            }
            return enrollments;
        }

        public static List<Enrollment> GetAllEnrollmentByTypeAndSite(int Type,int siteid ,string dbConnection)
        {
            var enrollments = new List<Enrollment>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllEnrollmentByTypeAndSite", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllEnrollmentByTypeAndSite";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var enrollment = GetEnrollmentFromSqlReader(reader);



                    enrollments.Add(enrollment);
                }
                connection.Close();
                reader.Close();
            }
            return enrollments;
        }
        public static List<Enrollment> GetAllEnrollmentByCreatedBy(string name, string dbConnection)
        {
            var enrollments = new List<Enrollment>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllEnrollmentByCreatedBy", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllEnrollmentByCreatedBy";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var enrollment = GetEnrollmentFromSqlReader(reader);
                    enrollments.Add(enrollment);
                }
                connection.Close();
                reader.Close();
            }
            return enrollments;
        }
        public static Enrollment GetAllEnrollmentByCaseId(int caseId, string dbConnection)
        {
            Enrollment enrollments = null;

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllEnrollmentByCaseId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@CaseId", SqlDbType.Int));
                sqlCommand.Parameters["@CaseId"].Value = caseId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllEnrollmentByCaseId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    enrollments = GetEnrollmentFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return enrollments;
        }
        public static List<Enrollment> GetAllEnrollmentByTypeAndCreatedBy(string name, int Type, string dbConnection)
        {
            var enrollments = new List<Enrollment>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllEnrollmentByTypeAndCreatedBy", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllEnrollmentByTypeAndCreatedBy";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var enrollment = GetEnrollmentFromSqlReader(reader);

                    enrollments.Add(enrollment);
                }
                connection.Close();
                reader.Close();
            }
            return enrollments;
        }

        public static bool DeleteEnrollmentById(int caseId, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("DeleteEnrollmentById", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = caseId;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteEnrollmentById";
                    sqlCommand.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}