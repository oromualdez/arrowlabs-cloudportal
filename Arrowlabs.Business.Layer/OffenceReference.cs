﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
  public  class OffenceReference
    {
        public int Id { get; set; }
        public string OffenceName { get; set; }

        public int OffenceId { get; set; }

        public int IncidentId { get; set; }

        private static OffenceReference GetOffenceReferenceFromSqlReader(SqlDataReader reader)
        {
            var data = new OffenceReference();
            if (reader["Id"] != DBNull.Value)
                data.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["OffenceName"] != DBNull.Value)
                data.OffenceName = reader["OffenceName"].ToString();
            if (reader["OffenceId"] != DBNull.Value)
                data.OffenceId = Convert.ToInt32(reader["OffenceId"].ToString());
            if (reader["IncidentId"] != DBNull.Value)
                data.IncidentId = Convert.ToInt32(reader["IncidentId"].ToString());

            return data;
        }
        public static List<OffenceReference> GetAllOffenceReference(string dbConnection)
        {
            var dataList = new List<OffenceReference>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllOffenceReference", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = GetOffenceReferenceFromSqlReader(reader);
                    dataList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return dataList;
        }
        public static List<OffenceReference> GetOffenceReferenceByIncidentId(int incidentId, string dbConnection)
        {
            var dataList = new List<OffenceReference>();
            OffenceReference data = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceReferenceByIncidentId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = incidentId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    data = GetOffenceReferenceFromSqlReader(reader);
                    dataList.Add(data);
                }
                connection.Close();
                reader.Close();
            }
            return dataList;
        }
        public static bool InsertorUpdateOffenceReference(OffenceReference chatUser, string dbConnection)
        {
            if (chatUser != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateOffenceReference", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = chatUser.Id;

                    cmd.Parameters.Add(new SqlParameter("@OffenceName", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceName"].Value = chatUser.OffenceName;

                    cmd.Parameters.Add(new SqlParameter("@OffenceId", SqlDbType.Int));
                    cmd.Parameters["@OffenceId"].Value = chatUser.OffenceId;

                    cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                    cmd.Parameters["@IncidentId"].Value = chatUser.IncidentId;


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static bool DeleteOffenceReferenceByIncidentId(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteOffenceReferenceByIncidentId", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteOffenceReferenceByIncidentId";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
