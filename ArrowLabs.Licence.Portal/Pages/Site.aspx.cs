﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class Site : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static ClientLicence getClientLic;

        protected string senderName;
        protected string ipaddress;
        protected string senderName2;
        protected string userinfoDisplay;
        protected string senderName3;
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            try
            {
                senderName = User.Identity.Name;
                senderName2 = Encrypt.EncryptData(senderName, true, dbConnection);
                var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
                ipaddress = configtSettings.ChatServerUrl + "/signalr";
                var userinfo = Users.GetUserByName(senderName, dbConnection);
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                senderName3 = userinfo.CustomerUName;
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    if (userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        System.Web.Security.FormsAuthentication.SignOut();
                        Response.Redirect("~/Default.aspx");
                    }
                }
                if (userinfo != null)
                {
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        userinfoDisplay = "none";
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            userinfoDisplay = "block";
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "Page_Load", err, dbConnection);
            }

        }
        [WebMethod]
        public static List<string> getTableData(int id,string uname)
        {
            var listy = new List<string>();
            try
            {
                var userinfo = Users.GetUserByEncryptedName(uname,dbConnection);
                var sessions = new List<Arrowlabs.Business.Layer.Site>();
                sessions = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    sessions = sessions.Where(i => i.CustomerInfoId == userinfo.CustomerInfoId).ToList();

                foreach (var item in sessions)
                {
                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td>" + item.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td><a href='#' data-target='#newSite'  data-toggle='modal'  onclick='editSite(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;,&apos;" + item.Lati + "&apos;,&apos;" + item.Long + "&apos;)'><i class='fa fa-pencil' ></i>Edit</a><a href='#' data-target='#deleteSite'  data-toggle='modal' onclick='editSite(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;,&apos;" + item.Lati + "&apos;,&apos;" + item.Long + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Site", "getTableData", err, dbConnection);
            }
            return listy;
        }

        [WebMethod]
        public static string deleteSiteData(int id, string uname)
        {
            var newAuthorize = new MyAuthorize();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {

                return "LOGOUT";
            }
            else
            {
                var listy = string.Empty;
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var s = Arrowlabs.Business.Layer.Site.GetSiteById(id, dbConnection);
                        if (s != null)
                        {
                            id = s.Id;
                            try
                            {
                                var tattach = UserTaskAttachment.GetTaskAttachmentBySiteId(id, dbConnection);
                                foreach (var t in tattach)
                                {
                                    CommonUtility.CloudDeleteFile(t.DocumentPath);
                                }
                            }
                            catch (Exception ex) { MIMSLog.MIMSLogSave("Site", "deleteSiteData GetTaskAttachmentBySiteId", ex, dbConnection); }
                            try
                            {
                                var tattach = MobileHotEventAttachment.GetMobileHotEventAttachmentBySiteId(id, dbConnection);
                                foreach (var t in tattach)
                                {
                                    CommonUtility.CloudDeleteFile(t.AttachmentPath);
                                }
                            }
                            catch (Exception ex) { MIMSLog.MIMSLogSave("Site", "deleteSiteData GetMobileHotEventAttachmentBySiteId", ex, dbConnection); }
                            try
                            {
                                var tattach = LostAndFoundAttachments.GetLostAndFoundAttachmentsBySiteId(id, dbConnection);
                                foreach (var t in tattach)
                                {
                                    CommonUtility.CloudDeleteFile(t.ImagePath);
                                }
                            }
                            catch (Exception ex) { MIMSLog.MIMSLogSave("Site", "deleteSiteData GetLostAndFoundAttachmentsBySiteId", ex, dbConnection); }
                            try
                            {
                                var tattach = ItemLostAttachment.GetItemLostAttachmentBySiteId(id, dbConnection);
                                foreach (var t in tattach)
                                {
                                    CommonUtility.CloudDeleteFile(t.AttachmentPath);
                                }
                            }
                            catch (Exception ex) { MIMSLog.MIMSLogSave("Site", "deleteSiteData GetItemLostAttachmentBySiteId", ex, dbConnection); }
                            if (Arrowlabs.Business.Layer.Site.DeleteSiteById(id, dbConnection))
                            {
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Site", id.ToString(), id.ToString(), userinfo, "Site DELETE" + id);
                                Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("Site was deleted with id=" + id, "Site Successfully", new Exception(), dbConnection);
                                listy = "SUCCESS";
                            }
                            else
                            {
                                listy = "Error occured while trying to delete site";
                            }
                        }
                        else
                        {
                            listy = "Error: site does not exist anymore";
                        }
                    }
                }
                catch (Exception ex)
                {
                    listy = ex.Message;
                    MIMSLog.MIMSLogSave("Site", "deleteSiteData", ex, dbConnection);
                }
                return listy;
            }
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static string insertSiteData(string id, string lat, string longi, string name, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    if (!string.IsNullOrEmpty(id))
                    {
                        var getSite = Arrowlabs.Business.Layer.Site.GetSiteById(Convert.ToInt32(id), dbConnection);
                        if (getSite != null)
                        {
                            var newSite = Arrowlabs.Business.Layer.Site.GetSiteByNameAndCId(name,userinfo.CustomerInfoId ,dbConnection);
                            if (newSite == null)
                            {
                                getSite.Name = name;
                                getSite.UpdatedBy = userinfo.Username;
                                getSite.UpdatedDate = DateTime.Now;

                                if (!string.IsNullOrEmpty(longi))
                                    getSite.Long = longi; 
                                else
                                    getSite.Long = "0";
                                if (!string.IsNullOrEmpty(lat))
                                    getSite.Lati = lat;
                                else
                                    getSite.Lati = "0";

                                var retval = Arrowlabs.Business.Layer.Site.InsertorUpdateSite(getSite, dbConnection, dbConnectionAudit, true);

                                listy = "SUCCESS";
                            }
                            else if (newSite != null && newSite.Id == Convert.ToInt32(id))
                            {
                                newSite.UpdatedBy = userinfo.Username;
                                newSite.UpdatedDate = DateTime.Now;

                                if (!string.IsNullOrEmpty(longi))
                                    newSite.Long = longi;
                                else
                                    newSite.Long = "0";
                                if (!string.IsNullOrEmpty(lat))
                                    newSite.Lati = lat;
                                else
                                    newSite.Lati = "0";

                                var retval = Arrowlabs.Business.Layer.Site.InsertorUpdateSite(newSite, dbConnection, dbConnectionAudit, true);

                                listy = "SUCCESS";
                            }
                            else
                            {
                                listy = "Site name already exists";
                            }
                        }
                        else
                        {
                            getSite = Arrowlabs.Business.Layer.Site.GetSiteByNameAndCId(name, userinfo.CustomerInfoId, dbConnection);
                            if (getSite == null)
                            {
                                var newSite = new Arrowlabs.Business.Layer.Site();
                                newSite.Name = name;
                                newSite.UpdatedBy = userinfo.Username;
                                newSite.CreatedBy = userinfo.Username;
                                newSite.CreatedDate = DateTime.Now;
                                newSite.UpdatedDate = DateTime.Now;
                                newSite.CustomerInfoId = userinfo.CustomerInfoId;
                                if (!string.IsNullOrEmpty(longi))
                                    newSite.Long = longi;
                                else
                                    newSite.Long = "0";
                                if (!string.IsNullOrEmpty(lat))
                                    newSite.Lati = lat;
                                else
                                    newSite.Lati = "0";

                                var retval = Arrowlabs.Business.Layer.Site.InsertorUpdateSite(newSite, dbConnection, dbConnectionAudit, true);

                                var mimcon = MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection);
                                var consplit = mimcon.FilePath.Split('\\');
                                var newstring = string.Empty;
                                var count = 0;
                                foreach (var split in consplit)
                                {
                                    if (count < 4)
                                    {
                                        newstring += split + "\\";
                                        count++;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                } 

                                if (retval > 0)
                                    listy = "SUCCESS";
                                else
                                    listy = "Error occured while creating site.";
                            }
                            else
                            {
                                listy = "Site name already exists";
                            }
                        }
                    }
                    else
                    {
                        var getSite = Arrowlabs.Business.Layer.Site.GetSiteByNameAndCId(name,userinfo.CustomerInfoId, dbConnection);
                        if (getSite == null)
                        {
                            var newSite = new Arrowlabs.Business.Layer.Site();
                            newSite.Name = name;
                            newSite.UpdatedBy = userinfo.Username;
                            newSite.CreatedBy = userinfo.Username;
                            newSite.CreatedDate = DateTime.Now;
                            newSite.UpdatedDate = DateTime.Now;
                            newSite.CustomerInfoId = userinfo.CustomerInfoId;
                            if (!string.IsNullOrEmpty(longi))
                                newSite.Long = longi;
                            else
                                newSite.Long = "0";
                            if (!string.IsNullOrEmpty(lat))
                                newSite.Lati = lat;
                            else
                                newSite.Lati = "0";

                            var retval = Arrowlabs.Business.Layer.Site.InsertorUpdateSite(newSite, dbConnection, dbConnectionAudit, true);

                            var mimcon = MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection);
                            var consplit = mimcon.FilePath.Split('\\');
                            var newstring = string.Empty;
                            var count = 0;
                            foreach (var split in consplit)
                            {
                                if (count < 4)
                                {
                                    newstring += split + "\\";
                                    count++;
                                }
                                else
                                {
                                    break;
                                }
                            } 

                            if (retval > 0)
                                listy = "SUCCESS";
                            else
                                listy = "Error occured while creating site.";
                        }
                        else
                        {
                            listy = "Site name already exists";
                        }
                    }
                }
                catch (Exception ex)
                {
                    listy = ex.Message;
                    MIMSLog.MIMSLogSave("Site", "insertSiteData", ex, dbConnection);
                }
                return listy;
            }
        }
      
        //User Profile
        [WebMethod]
        public static string changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo.ID > 0)
            {
                var getuser = userinfo;
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                            
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    return userinfo.ID;
                }
                else
                    return 0;
            }
            return userinfo.ID;
        }
        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                  //  var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    //if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                    if (customData.RoleId != (int)Role.SuperAdmin)
                    {
                      //  if (pushDev != null)
                      //  {
                       //     if (!string.IsNullOrEmpty(pushDev.Username))
                       //     {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                       //     }
                       //     else
                        //    {
                        //        monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        //        fontstyle = "";
                          //  }
                        //}
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Site", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static string getLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }
                var site = Arrowlabs.Business.Layer.Site.GetSiteById(id, dbConnection);

                json += "[";
                if (site != null)
                {
                    json += "{ \"Username\" : \"" + site.Name + "\",\"Id\" : \"" + site.Id.ToString() + "\",\"Long\" : \"" + site.Long + "\",\"Lat\" : \"" + site.Lati + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }

    }
}