﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Web.Services;
using System.Web.Configuration;
using System.Globalization;
using System.Text;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using System.Threading;
using ArrowLabs.Licence.Portal.Helpers;
using System.Net.Sockets;
using System.ServiceProcess;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class Incident : System.Web.UI.Page
    {
        static CustomEvent.EventTypes cusEvType { get; set; }
        static CustomEvent.EventTypes GetEventType(string value)
        {
            switch (value)
            {
                case "MobHot":
                    return CustomEvent.EventTypes.MobileHotEvent;
                case "All":
                    return CustomEvent.EventTypes.Unknown;
                case "HotEvent":
                    return CustomEvent.EventTypes.HotEvent;
                case "Offence":
                    return CustomEvent.EventTypes.DriverOffence;
                case "Incident":
                    return CustomEvent.EventTypes.Incident;
                default:
                    return CustomEvent.EventTypes.Unknown;
            }
        }
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static ClientLicence getClientLic;
        int completedCount;
        int inprogressCount;
        int pendingCount;
        int total;
        static List<string> VideoExtensions = new List<string> { ".AVI", ".MP4", ".MKV", ".FLV" };

        protected string incidentId;
        protected string incidentType;
        protected string longi;
        protected string lati;
        protected string alarmMSG;
        protected string senderName;
        protected string username;
        protected string ipaddress;
        protected string sendToManager;
        protected string assignedTo;
        protected string sendToTag;
        protected string sendMangId;
        protected string MobileHandledPercentage;
        protected string systemHandledPercentage;
        protected string completedSystemPercent;
        protected string inprogressSystemPercent;
        protected string pendingSystemPercent;
        protected string HotHandledPercentage;

        protected string pending3rdPartyPercent;
        protected string inprogress3rdPartyPercent;
        protected string completed3rdPartyPercent;
        protected string thirdpartyHandledPercentage;

        protected string userinfoDisplay;
        protected string NewIncident;
        protected string CreateNewIncident;
        protected string Receivedby;
        protected string PhoneNumber;
        protected string Email;
        protected string SelectLocation;
        protected string Lat;
        protected string IncidentName;
        protected string IncidentDescription;
        protected string Next;
        protected string ClearSavedLocation;
        protected string Cameras;
        protected string Task;
        protected string Instructions;
        protected string Back;
        protected string Park;
        protected string Dispatch;
        protected string DispatchFromUsers;
        protected string Longi;
        protected string direction;
        protected string dispatchoptions;
        protected string dispatchSurv;
        protected string dispatchDisplay;
        protected string taskDisplay;

        protected string pendingPercent;
        protected string inprogressPercent;
        protected string completedPercent;
        protected string RequestHandledPercentage;
        protected string escalatedView;
        static string fpath;
        protected string senderName2;
        protected string sourceLat;
        protected string sourceLon;
        protected string currentlocation;
        protected string senderName3;
        protected string siteName;
        protected string requestDisplay;
        protected void Page_Init(object sender, EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            fpath = Server.MapPath("~/Uploads/");
            dbConnection = CommonUtility.dbConnection;
            var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
            ipaddress = configtSettings.ChatServerUrl + "/signalr";
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            MobileHotEventTableControl.TableName = "mobilehoteventTable";
            //HotEventTableControl.TableName = "hoteventTable";
            userinfoDisplay = "block";
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            senderName2 = Encrypt.EncryptData(User.Identity.Name, true, dbConnection);
            senderName = User.Identity.Name;
            var userinfo = Users.GetUserByName(User.Identity.Name, dbConnection);
            sourceLat = "25.2049808502197";
            sourceLon = "55.2707939147949";
            currentlocation = "U.A.E";
            requestDisplay = "style='display:none'";
            try
            {

                if (userinfo.SiteId > 0)
                {
                    siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                }

                var CameraLists = MilestoneCamera.GetAllMilestoneCamera(dbConnection);
                newCameraSelect.DataTextField = "CameraName";
                newCameraSelect.DataValueField = "CamDetails";
                newCameraSelect.DataSource = CameraLists;
                newCameraSelect.DataBind();

                //cameraSelect.DataTextField = "CameraName";
                //cameraSelect.DataValueField = "CamDetails";
                //cameraSelect.DataSource = CameraLists;
                //cameraSelect.DataBind();
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                dispatchDisplay = "none";
				taskDisplay = "style='display:none'";
				dispatchSurv = "style='display:none'";
                senderName3 = userinfo.CustomerUName;
                if (getClientLic != null)
                {
                    if (!getClientLic.isDispatch)
                        dispatchDisplay = "none";
                    if (!getClientLic.isSurveillance)
                        dispatchSurv = "style='display:none'";
                    if (!getClientLic.isTask)
                        taskDisplay = "style='display:none'";
                }

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser || userinfo.RoleId == (int)Role.Regional)
                {
                    if (userinfo.CustomerInfoId > 0)
                    {
                        var gSite = Arrowlabs.Business.Layer.CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                        if (gSite != null)
                        {
                            sourceLat = gSite.Lati;
                            sourceLon = gSite.Long;
                            currentlocation = gSite.Country;
                        }
                    }
                }
                else
                {
                    if (userinfo.SiteId > 0)
                    {
                        var gSite = Arrowlabs.Business.Layer.Site.GetSiteById(userinfo.SiteId, dbConnection);
                        if (gSite != null)
                        {
                            sourceLat = gSite.Lati;
                            sourceLon = gSite.Long;
                            currentlocation = ReverseGeocode.RetrieveFormatedAddress(sourceLat, sourceLon, getClientLic);
                        }
                    }
                }

                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    {
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Surveillance && getClientLic.isSurveillance)
                        {
                            dispatchSurv = "style='display:block'";
                        }
                        else if (mods.ModuleId == (int)Accounts.ModuleTypes.Dispatch && getClientLic.isDispatch)
                        {
                            dispatchDisplay = "block";
                        }
                        else if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks && getClientLic.isTask)
                        {
                            taskDisplay = "style='display:block'";
                        }
                        else if (mods.ModuleId == (int)Accounts.ModuleTypes.Request)
                        {
                            requestDisplay = "style='display:block'";
                        } 
                    }
                }
                else
                {
                    if (getClientLic != null)
                    {
                        if (getClientLic.isDispatch)
                            dispatchDisplay = "block";
                        if (getClientLic.isSurveillance)
                            dispatchSurv = "style='display:block'";
                        if (getClientLic.isTask)
                            taskDisplay = "style='display:block'";
                    }
                }
                if (!string.IsNullOrEmpty(senderName))
                {

                    NewIncident = "New Incident ";
                    CreateNewIncident = "Create New Incident";
                    Receivedby = "Received by";

                    PhoneNumber = "Phone Number";
                    Email = "Email";
                    SelectLocation = "Select Location";

                    Lat = "Latitude";
                    IncidentName = "Incident Name";
                    IncidentDescription = "Incident Description";

                    Next = "Next";
                    ClearSavedLocation = "Clear Saved Location";
                    Cameras = "Cameras";

                    Task = "Task";
                    Instructions = "Instructions";
                    Back = "Back";

                    Park = "Park";
                    Dispatch = "Dispatch";
                    DispatchFromUsers = "Dispatch From Users";
                    Longi = "Longitude";
                    dispatchoptions = "Dispatch Options"; 

                        NewIncident = "New Incident ";
                        CreateNewIncident = "Create New Incident";
                        Receivedby = "Received by";

                        PhoneNumber = "Phone Number";
                        Email = "Email";
                        SelectLocation = "Select Location";

                        Lat = "Latitude";
                        IncidentName = "Incident Name";
                        IncidentDescription = "Incident Description";

                        Next = "Next";
                        ClearSavedLocation = "Clear Saved Location";
                        Cameras = "Cameras";

                        Task = "Task";
                        Instructions = "Instructions";
                        Back = "Back";

                        Park = "Park";
                        Dispatch = "Dispatch";
                        DispatchFromUsers = "Dispatch From Users";
                        Longi = "Longitude";
                        dispatchoptions = "Dispatch Options";

                    //}
                    ipaddress = configtSettings.ChatServerUrl + "/signalr";
                    var locations = new List<Location>();

                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        locations.AddRange(Location.GetAllLocation(dbConnection));
                        locations.AddRange(Location.GetAllSubLocation(dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                    {
                        locations.AddRange(Location.GetAllLocationBySiteId(userinfo.SiteId, dbConnection));
                        locations.AddRange(Location.GetAllSubLocationBySiteId(userinfo.SiteId, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                        //{
                        locations.AddRange(Location.GetAllMainLocationByLevel5(userinfo.ID, dbConnection));
                        locations.AddRange(Location.GetAllSubLocationByLevel5(userinfo.ID, dbConnection));
                        //}
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        locations.AddRange(Location.GetAllMainLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                        locations.AddRange(Location.GetAllSubLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                    }
                    var grouped = locations.GroupBy(item => item.ID);
                    locations = grouped.Select(grp => grp.OrderBy(item => item.LocationDesc).First()).ToList();
                    locations = locations.OrderBy(i => i.LocationDesc).ToList();
                    var glocations = new List<Location>();
                    var loc = new Location();
                    loc.ID = 0;
                    loc.LocationDesc = SelectLocation;
                    loc.LocationDesc_AR = SelectLocation;
                    loc.CreatedDate = CommonUtility.getDTNow();
                    loc.CreatedBy = userinfo.Username;
                    glocations.Add(loc);
                    glocations.AddRange(locations);

                    locationSelect.DataTextField = "LOCATIONDESC";
                    locationSelect.DataValueField = "Id";
                    locationSelect.DataSource = glocations;
                    locationSelect.DataBind();

                    getEventStatus(userinfo);

                    var allusers = new List<Users>();
                    var alldevices = new List<Device>();
                    var allgroups = new List<Group>();
                    var defUser = new Users();
                    defUser.Username = "Please Select";
                    defUser.CustomerUName = "Please Select";
                    defUser.ID = 0;
                    allusers.Add(defUser);
                    var defDev = new Device();
                    defDev.PCName = "Please Select";
                    defDev.MACAddress = "0";
                    alldevices.Add(defDev);
                    var defgrp = new Group();
                    defgrp.Name = "Please Select";
                    defgrp.Id = 0;
                    allgroups.Add(defgrp);

                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.Regional || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        escalatedView = "style='display:none'";
                    }

                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        escalatedView = "style='display:none'";
                        var taskslist = UserTask.GetAllTaskTemplates(dbConnection);

                        taskSelect.DataTextField = "Name";
                        taskSelect.DataValueField = "Id";
                        taskSelect.DataSource = taskslist;
                        taskSelect.DataBind();


                        //selectTaskTemplate.DataTextField = "Name";
                        //selectTaskTemplate.DataValueField = "Id";
                        //selectTaskTemplate.DataSource = taskslist;
                        //selectTaskTemplate.DataBind();

                        allusers.AddRange(Users.GetAllUsers(dbConnection));
                        alldevices.AddRange(Device.GetAllDevice(dbConnection));
                        allgroups.AddRange(Group.GetAllGroup(dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        escalatedView = "style='display:none'";
                        var taskslist = UserTask.GetAllTemplateTasksByManagerId(userinfo.ID, dbConnection);

                        taskSelect.DataTextField = "Name";
                        taskSelect.DataValueField = "Id";
                        taskSelect.DataSource = taskslist;
                        taskSelect.DataBind();


                        //selectTaskTemplate.DataTextField = "Name";
                        //selectTaskTemplate.DataValueField = "Id";
                        //selectTaskTemplate.DataSource = taskslist;
                        //selectTaskTemplate.DataBind();

                        allusers.AddRange(Users.GetAllUsers(dbConnection));

                        allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();

                        allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username,dbConnection));
                        userinfoDisplay = "none";


                    }
                    else
                    {
                        
                        //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                        //if (pushDev != null)
                        //{
                        //    if (!string.IsNullOrEmpty(pushDev.Username))
                        //    {
                        //        pushDev.IsServerPortal = true;
                        //        pushDev.SiteId = userinfo.SiteId;
                        //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection, dbConnectionAudit, true);
                        //    }
                        //}
                        PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);
                        userinfoDisplay = "none";
                        var tasktemplateList = UserTask.GetAllTemplateTasksByManagerId(userinfo.ID, dbConnection);

                        taskSelect.DataTextField = "Name";
                        taskSelect.DataValueField = "Id";
                        taskSelect.DataSource = tasktemplateList;
                        taskSelect.DataBind();

                        //selectTaskTemplate.DataTextField = "Name";
                        //selectTaskTemplate.DataValueField = "Id";
                        //selectTaskTemplate.DataSource = tasktemplateList;
                        //selectTaskTemplate.DataBind();

                        if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var allmanagers = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                            foreach (var usermanager in allmanagers)
                            {
                                allusers.Add(usermanager);
                                var allmanagerusers = Users.GetAllFullUsersByManagerIdForDirector(usermanager.ID, dbConnection);
                                allusers.AddRange(allmanagerusers);
                            }

                            var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                            unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                            foreach (var subsubuser in unassigned)
                            {
                                allusers.Add(subsubuser);
                            } 
                        }
                        else if (userinfo.RoleId == (int)Role.Manager)
                        {
                            allusers.AddRange(Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection));
                            allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            var userlist = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                            foreach(var user in userlist)
                            {
                                if (user.Username != userinfo.Username)
                                    allusers.Add(user);
                            }
                            allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));

                            userinfoDisplay = "block";
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            allusers.AddRange(Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection));
                            allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                            userinfoDisplay = "block";
                            allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                           // foreach (var site in sites)
                           // {
                                allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                           // }
                            allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                        } 
                    }
                    allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                    var searchSelectItems = new List<SearchSelectItem>();

                    var grouped2 = allusers.GroupBy(item => item.ID);
                    allusers = grouped2.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                    foreach (var searchSelectUser in allusers)
                    {
                        var newSearchSelectItem = new SearchSelectItem();
                        newSearchSelectItem.ID = searchSelectUser.ID.ToString();
                        newSearchSelectItem.Name = searchSelectUser.CustomerUName;
                        searchSelectItems.Add(newSearchSelectItem);
                    }
                    foreach (var searchSelectDev in alldevices)
                    {
                        var newSearchSelectItem = new SearchSelectItem();
                        newSearchSelectItem.ID = searchSelectDev.MACAddress;
                        newSearchSelectItem.Name = searchSelectDev.PCName;
                        searchSelectItems.Add(newSearchSelectItem);
                    }
                    foreach (var searchSelectGroup in allgroups)
                    {
                        var newSearchSelectItem = new SearchSelectItem();
                        newSearchSelectItem.ID = searchSelectGroup.Id.ToString();
                        newSearchSelectItem.Name = searchSelectGroup.Name;
                        searchSelectItems.Add(newSearchSelectItem);
                    }
                    searchSelect.DataTextField = "Name";
                    searchSelect.DataValueField = "ID";
                    searchSelect.DataSource = searchSelectItems;
                    searchSelect.DataBind();

                    sendAlarmSelect.DataTextField = "CustomerUName";
                    sendAlarmSelect.DataValueField = "ID";
                    sendAlarmSelect.DataSource = allusers;
                    sendAlarmSelect.DataBind();

                    sendDeviceSelect.DataTextField = "PCName";
                    sendDeviceSelect.DataValueField = "MACAddress";
                    sendDeviceSelect.DataSource = alldevices;
                    sendDeviceSelect.DataBind();

                    sendGroupSelect.DataTextField = "Name";
                    sendGroupSelect.DataValueField = "ID";
                    sendGroupSelect.DataSource = allgroups;
                    sendGroupSelect.DataBind();
                    cusEvType = CustomEvent.EventTypes.Unknown;
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "Page_Load", er, dbConnection, userinfo.SiteId);
            }
        }

        [WebMethod]
        public static string pushNotificationSend(string id, string assigneename, string assigneetype, string name,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (assigneetype == TaskAssigneeType.Group.ToString())
                {
                    var thread = new Thread(() => CommonUtility.sendPushNotificationGroup(Convert.ToInt32(id), "Alarm: " + name, dbConnection));
                    thread.Start();
                }
                else if (assigneetype == TaskAssigneeType.User.ToString())
                {
                    var pushDevInfo = PushNotificationDevice.GetPushNotificationDeviceByUsername(assigneename, dbConnection);
                    if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Apple)
                    {
                        Thread thread = new Thread(() => PushNotificationClient.SendtoAppleUserNotification("Alarm: " + name, assigneename, dbConnection));

                        thread.Start();
                    }
                    else if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Android)
                    {
                        Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(assigneename, CommonUtility.androidpushApplicationID, CommonUtility.androidpushSenderId, pushDevInfo.DeviceToken, "Alarm", name, dbConnection, (int)PushNotificationDevice.PushNotificationType.IncidentNotification));

                        thread.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident.aspx", "pushNotificationSend", ex, dbConnection, userinfo.SiteId);
                return "FAIL";
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static string sendpushMultipleNotification(string[] userIds, string msg,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                Thread thread = new Thread(() => CommonUtility.sendPushNotificationAlarm(userIds, msg, dbConnection));
                thread.Start();
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident.aspx", "sendpushMultipleNotification", ex, dbConnection, userinfo.SiteId);
                return "FAIL";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string sendpushMultipleNotificationAlarm(string[] userIds, string msg, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                Thread thread = new Thread(() => CommonUtility.sendPushNotificationAlarmMessage(userIds, msg, dbConnection));
                thread.Start();
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident.aspx", "sendpushMultipleNotification", ex, dbConnection, userinfo.SiteId);
                return "FAIL";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static List<string> getTableDataMyIncident(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var evHistory = EventHistory.GetEventHistoryByUser(userinfo.Username, dbConnection);
                    var incidentList = new List<int>();
                    foreach (var ev in evHistory)
                    {
                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                        {
                            if (!incidentList.Contains(ev.EventId))
                            {
                                var item = CustomEvent.GetCustomEventById(ev.EventId, dbConnection);
                                if (item != null)
                                {
                                    if (!item.Handled)
                                    {
                                        if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                            if (mobEv != null)
                                            {
                                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                                    item.Name = "None";
                                                else
                                                    item.Name = mobEv.EventTypeName;
                                            }
                                        }
                                        else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                                    item.UserName = CommonUtility.getPCName(reqEv);

                                                if (item.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    var splitName = reqEv.Name.Split('^');
                                                    if (splitName.Length > 1)
                                                        item.Name = splitName[1];
                                                }
                                            }
                                        }
                                        if (item.CreatedBy != userinfo.Username)
                                        {
                                            incidentList.Add(item.EventId);
                                            var imageclass = "circle-point-red";
                                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.HandledBy + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";

                                            var gUser = Users.GetUserByName(item.HandledBy, dbConnection);

                                            if (gUser != null)
                                                returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name  + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + gUser.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";

                                            listy.Add(returnstring);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evHistory = EventHistory.GetEventHistoryByUser(userinfo.Username, dbConnection);
                    var incidentList = new List<int>();
                    foreach (var ev in evHistory)
                    {
                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                        {
                            if (!incidentList.Contains(ev.EventId))
                            {
                                var item = CustomEvent.GetCustomEventById(ev.EventId, dbConnection);
                                if (item != null)
                                {
                                    if (!item.Handled)
                                    {
                                        if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                            if (mobEv != null)
                                            {
                                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                                    item.Name = "None";
                                                else
                                                    item.Name = mobEv.EventTypeName;
                                            }
                                        }
                                        else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                                    item.UserName = CommonUtility.getPCName(reqEv);

                                                if (item.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    var splitName = reqEv.Name.Split('^');
                                                    if (splitName.Length > 1)
                                                        item.Name = splitName[1];
                                                }
                                            }
                                        }
                                        if (item.CreatedBy != userinfo.Username)
                                        {
                                            incidentList.Add(item.EventId);
                                            var imageclass = "circle-point-red";
                                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td> <td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.HandledBy + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";


                                            var gUser = Users.GetUserByName(item.HandledBy, dbConnection);

                                            if (gUser != null)
                                                returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + gUser.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";


                                            
                                            listy.Add(returnstring);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var allcustomEvents = new List<CustomEvent>();

                    if (userinfo.RoleId == (int)Role.Regional) //Level 5
                    {
                        allcustomEvents.Clear();
                       // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                            allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusLevel5AndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.ID, true, dbConnection));
                         
                        allcustomEvents = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer) //Level 7/6
                    {
                        allcustomEvents.Clear();
                        var sites = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));

                        foreach (var site in sites)
                            allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, site.Id, true, dbConnection));

                        if (userinfo.RoleId == (int)Role.ChiefOfficer)
                            allcustomEvents = allcustomEvents.Where(i => i.Escalate == (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        allcustomEvents = CustomEvent.GetAllCustomEventByStatusCIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.CustomerInfoId, true, dbConnection);

                        allcustomEvents = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                    else
                    { // Level 3/4
                        allcustomEvents = CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection);

                        if (userinfo.RoleId == (int)Role.Admin) //Level 3
                            allcustomEvents = allcustomEvents.Where(i => i.Escalate == (int)CustomEvent.EscalateTypes.Level2).ToList();
                        else if (userinfo.RoleId == (int)Role.Director) //Level 4
                            allcustomEvents = allcustomEvents.Where(i => i.Escalate == (int)CustomEvent.EscalateTypes.Level3).ToList();

                    }



                    var grouped = allcustomEvents.GroupBy(item => item.EventId);
                    allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                    foreach (var item in allcustomEvents)
                    {

                        if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                            if (mobEv != null)
                            {
                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                    item.Name = "None";
                                else
                                    item.Name = mobEv.EventTypeName;
                            }
                        }
                        else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                        {
                            var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                            if (reqEv != null)
                            {
                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                    item.UserName = CommonUtility.getPCName(reqEv);

                                if (item.EventType == CustomEvent.EventTypes.Request)
                                {
                                    var splitName = reqEv.Name.Split('^');
                                    if (splitName.Length > 1)
                                        item.Name = splitName[1];
                                }
                            }
                        }

                        var imageclass = "circle-point-red";



                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.HandledBy + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";

                        var gUser = Users.GetUserByName(item.HandledBy, dbConnection);

                        if (gUser != null)
                            returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + gUser.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                        
                        listy.Add(returnstring);

                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableDataMyIncident", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
    
        [WebMethod]
        public static List<string> getTableDataResolved(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var allcustomEvents = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var pclist = new List<string>();
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection);
                    var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                    foreach (var custom in customeventlist)
                    {
                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                            allcustomEvents.Add(custom);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByDateAndDirectorIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), manguser, dbConnection));

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                allcustomEvents.Add(custom);
                        }
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateHandledAndSiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)),userinfo.SiteId ,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateHandledAndCId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByDateHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));

                        var glist = CustomEvent.GetAllCustomEventByDateHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(glist.Where(i=>i.HandledBy == userinfo.Username).ToList());
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                }

                var imageclass = string.Empty;
                var grouped = allcustomEvents.GroupBy(item => item.EventId);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();
                foreach (var item in allcustomEvents)
                {
                    if (item.EventType != CustomEvent.EventTypes.DriverOffence)
                    {
                        if (item.Handled)
                        {
                            if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                            {
                                if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                {
                                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                    if (mobEv != null)
                                    {
                                        if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                            item.Name = "None";
                                        else
                                            item.Name = mobEv.EventTypeName;
                                    }
                                }
                                else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                                {
                                    var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                                    if (reqEv != null)
                                    {
                                        if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                            item.UserName = CommonUtility.getPCName(reqEv);

                                        if (item.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var splitName = reqEv.Name.Split('^');
                                            if (splitName.Length > 1)
                                                item.Name = splitName[1];
                                        }
                                    }
                                }

                                imageclass = "circle-point-red";
                                var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                listy.Add(returnstring);
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableDataResolved", er, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }

        [WebMethod]
        public static List<string> getTableData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.MobileHotEvent, userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.MobileHotEvent, userinfo.CustomerInfoId, dbConnection);
                 
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var site in sites)
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByTypeByLevel5((int)CustomEvent.EventTypes.MobileHotEvent, userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.MobileHotEvent, dbConnection);
                }
                //escalated
                if (userinfo.RoleId == (int)Role.SuperAdmin) //Level 7/6
                {
                    var sites = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));

                    foreach (var site in sites)
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, site.Id, true, dbConnection));
                     
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusCIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.CustomerInfoId, true, dbConnection));

                }
                else
                { // Level 3/4


                    if (userinfo.RoleId == (int)Role.Admin) //Level 3
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEvents = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level3 && i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();

                    }
                    else if (userinfo.RoleId == (int)Role.Director) //Level 4
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEvents = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                }

                //if (userinfo.RoleId == (int)Role.Director) //Level 4
                //{
                //    allcustomEvents = allcustomEvents.Where(i => i.UserName.ToLower() != userinfo.Username).ToList();
                //}

                allcustomEvents = allcustomEvents.Where(i => i.EventType ==CustomEvent.EventTypes.MobileHotEvent).ToList();

                var grouped = allcustomEvents.GroupBy(item => item.EventId);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                var imageclass = string.Empty;
                listy.Add("MOBILE HOT EVENTS");
                foreach (var item in allcustomEvents)
                {
                    if (item.EventType == CustomEvent.EventTypes.MobileHotEvent && item.StatusName != "Resolve")
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            var dir = Accounts.GetDirectorByManagerName(userinfo.Username, dbConnection);
                            if (dir != null)
                            {
                                var isGood = false;
                                if (item.StatusName == "Pending")
                                {
                                    isGood = true;
                                }
                                else
                                {
                                    if (item.HandledBy == userinfo.Username)
                                    {
                                        isGood = true;
                                    }
                                }
                                if (isGood)
                                {
                                    if (item.StatusName != "Escalated" && item.HandledBy != dir.Username)
                                    {
                                        var getHotName = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                        if (string.IsNullOrEmpty(getHotName.EventTypeName))
                                            getHotName.EventTypeName = "None";
                                        imageclass = "circle-point-red";
                                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + getHotName.EventTypeName + "-" + item.EventId + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                        listy.Add(returnstring);
                                    }
                                }
                            }
                            else
                            {
                                if (item.StatusName != "Escalated")
                                {
                                    var isGood = false;
                                    if (item.StatusName == "Pending")
                                    {
                                        isGood = true;
                                    }
                                    else
                                    {
                                        if (item.HandledBy == userinfo.Username)
                                        {
                                            isGood = true;
                                        }
                                    }
                                    if (isGood)
                                    {
                                        var getHotName = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                        if (string.IsNullOrEmpty(getHotName.EventTypeName))
                                            getHotName.EventTypeName = "None";
                                        imageclass = "circle-point-red";
                                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + getHotName.EventTypeName + "-" + item.EventId + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                        listy.Add(returnstring);
                                    }
                                }
                            }
                        }
                        else
                        {

                            //var isGood = false;
                            //if (item.StatusName == "Pending")
                            //{
                            //    isGood = true;
                            //}
                            //else
                            //{
                            //    if (item.HandledBy == userinfo.Username)
                            //    {
                            //        isGood = true;
                            //    }
                            //    if (userinfo.RoleId != (int)Role.Admin)
                            //    {
                            //        isGood = true;
                            //    }
                            //}
                            //if (isGood)
                            //{
                            if (item.StatusName != "Escalated")
                            {
                                var getHotName = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                if (string.IsNullOrEmpty(getHotName.EventTypeName))
                                    getHotName.EventTypeName = "None";
                                imageclass = "circle-point-red";
                                var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + getHotName.EventTypeName + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                listy.Add(returnstring);
                            }
                            //}
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableData", er, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }

        [WebMethod]
        public static List<string> getTableData2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);
                 
                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<CustomEvent>();
                var vehs = ClientManager.GetAllClientManager(dbConnection);
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var pclist = new List<string>();
                    allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                    var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);

                    foreach (var veh in vehs)
                    {
                        if (veh.UserId == userinfo.ID)
                        {
                            var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                            pclist.Add(health.PCName);
                        }
                    }
                    foreach (var custom in customeventlist)
                    {
                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                            allcustomEvents.Add(custom);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                        foreach (var veh in vehs)
                        {
                            if (veh.UserId == manguser)
                            {
                                var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                                pclist.Add(health.PCName);
                            }
                        }
                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                allcustomEvents.Add(custom);
                        }
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.HotEvent, userinfo.CustomerInfoId, dbConnection);
                
                else if (userinfo.RoleId == (int)Role.Regional)
                {

                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var site in sites)
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByTypeByLevel5((int)CustomEvent.EventTypes.HotEvent, userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                }



                var grouped = allcustomEvents.GroupBy(item => item.EventId);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();
                var imageclass = string.Empty;
                listy.Add("HOT EVENTS");
                foreach (var item in allcustomEvents)
                {
                    if (item.EventType == CustomEvent.EventTypes.HotEvent && item.StatusName != "Resolve")
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            if (item.StatusName != "Escalated")
                            {

                                var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                                if (hotEv != null)
                                    item.UserName = CommonUtility.getPCName(hotEv);

                                imageclass = "circle-point-red";
                                var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name  + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                listy.Add(returnstring);
                            }
                        }
                        else
                        {
                            var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                            if (hotEv != null)
                                item.UserName = CommonUtility.getPCName(hotEv);

                            imageclass = "circle-point-red";
                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name  + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                            listy.Add(returnstring);
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableData2", er, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }

        [WebMethod]
        public static List<string> getTableData4(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<CustomEvent>();
                var vehs = ClientManager.GetAllClientManager(dbConnection);
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var pclist = new List<string>();
                    allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                    var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.Request, dbConnection);

                    foreach (var veh in vehs)
                    {
                        if (veh.UserId == userinfo.ID)
                        {
                            var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                            pclist.Add(health.PCName);
                        }
                    }
                    foreach (var custom in customeventlist)
                    {
                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                            allcustomEvents.Add(custom);
                    }

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.Request, dbConnection);
                        foreach (var veh in vehs)
                        {
                            if (veh.UserId == manguser)
                            {
                                var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                                pclist.Add(health.PCName);
                            }
                        }
                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                allcustomEvents.Add(custom);
                        }
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.Request, userinfo.CustomerInfoId, dbConnection);
                
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByTypeByLevel5((int)CustomEvent.EventTypes.Request, userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.Request, dbConnection);
                }


                if (userinfo.RoleId == (int)Role.SuperAdmin) //Level 7/6
                {
                    var sites = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));

                    foreach (var site in sites)
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, site.Id, true, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusCIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.CustomerInfoId, true, dbConnection));

                }
                else
                { // Level 3/4
                    if (userinfo.RoleId == (int)Role.Admin) //Level 3
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEvents = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level3 && i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();

                    }
                    else if (userinfo.RoleId == (int)Role.Director) //Level 4
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEvents = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                }

                //if (userinfo.RoleId == (int)Role.Director) //Level 4
                //{
                //     allcustomEvents = allcustomEvents.Where(i => i.UserName.ToLower() != userinfo.Username).ToList();
                //}

                allcustomEvents = allcustomEvents.Where(i => i.EventType == CustomEvent.EventTypes.Request).ToList();

                var grouped = allcustomEvents.GroupBy(item => item.EventId);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                var imageclass = string.Empty;
                foreach (var item in allcustomEvents)
                {
                    if (item.EventType == CustomEvent.EventTypes.Request && item.StatusName != "Resolve")
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            var dir = Accounts.GetDirectorByManagerName(userinfo.Username, dbConnection);
                            if (dir != null)
                            {
                                if (item.StatusName != "Escalated" && item.HandledBy != dir.Username)
                                {
                                    var isGood = false;
                                    if (item.StatusName == "Pending")
                                    {
                                        isGood = true;
                                    }
                                    else
                                    {
                                        if (item.HandledBy == userinfo.Username)
                                        {
                                            isGood = true;
                                        }
                                    }
                                    if (isGood)
                                    {
                                        var continuE = true;
                                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Dispatch)
                                        {
                                            if (item.HandledBy != userinfo.Username)
                                                continuE = false;
                                        }
                                        if (continuE)
                                        {
                                            var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                                            if (hotEv != null)
                                            {
                                                if (CommonUtility.getPCName(hotEv) != "MIMS MOBILE")
                                                    item.UserName = CommonUtility.getPCName(hotEv);

                                                var splitName = hotEv.Name.Split('^');
                                                if (splitName.Length > 1)
                                                    item.Name = splitName[1];

                                            }
                                            imageclass = "circle-point-blue";
                                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                            listy.Add(returnstring);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (item.StatusName != "Escalated")
                                {
                                    var isGood = false;
                                    if (item.StatusName == "Pending")
                                    {
                                        isGood = true;
                                    }
                                    else
                                    {
                                        if (item.HandledBy == userinfo.Username)
                                        {
                                            isGood = true;
                                        }
                                    }
                                    if (isGood)
                                    {
                                        var continuE = true;
                                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Dispatch)
                                        {
                                            if (item.HandledBy != userinfo.Username)
                                                continuE = false;
                                        }
                                        if (continuE)
                                        {
                                            var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                                            if (hotEv != null)
                                            {
                                                if (CommonUtility.getPCName(hotEv) != "MIMS MOBILE")
                                                    item.UserName = CommonUtility.getPCName(hotEv);

                                                var splitName = hotEv.Name.Split('^');
                                                if (splitName.Length > 1)
                                                    item.Name = splitName[1];

                                            }
                                            imageclass = "circle-point-blue";
                                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                            listy.Add(returnstring);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //var isGood = false;
                            //if (item.StatusName == "Pending")
                            //{
                            //    isGood = true;
                            //}
                            //else
                            //{
                            //    if (item.HandledBy == userinfo.Username)
                            //    {
                            //        isGood = true;
                            //    }
                            //    if (userinfo.RoleId != (int)Role.Admin)
                            //    {
                            //        isGood = true;
                            //    }
                            //}
                            //if (isGood)
                            //{
                            if (item.StatusName != "Escalated")
                            {
                                var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                                if (hotEv != null)
                                {
                                    if (CommonUtility.getPCName(hotEv) != "MIMS MOBILE")
                                        item.UserName = CommonUtility.getPCName(hotEv);

                                    var splitName = hotEv.Name.Split('^');
                                    if (splitName.Length > 1)
                                        item.Name = splitName[1];

                                }
                                imageclass = "circle-point-blue";
                                var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                listy.Add(returnstring);
                            }
                            //}
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableData4", er, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }
     
        [WebMethod]
        public static List<string> getTableData3(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director )
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection); 
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {

                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));

                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                }

                if (userinfo.RoleId == (int)Role.SuperAdmin) //Level 7/6
                {
                    var sites = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));

                    foreach (var site in sites)
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, site.Id, true, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusCIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.CustomerInfoId, true, dbConnection));

                }
                else
                { // Level 3/4
                    if (userinfo.RoleId == (int)Role.Admin) //Level 3
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEvents = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level3 && i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();

                    }
                    else if (userinfo.RoleId == (int)Role.Director) //Level 4
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEvents = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                }
                allcustomEvents = allcustomEvents.Where(i => i.IncidentType == (int)Incidents.IncidentTypes.SystemCreated).ToList();


                var imageclass = string.Empty;


                var grouped = allcustomEvents.GroupBy(item => item.EventId);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                foreach (var item in allcustomEvents)
                {
                    if (item.IncidentType == (int)Incidents.IncidentTypes.SystemCreated)
                    {
                        if (item.StatusName != "Resolve")
                        {
                            imageclass = "circle-point-yellow";
                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                            listy.Add(returnstring);
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableData3", er, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }
        [WebMethod]
        public static List<string> getTableData5(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            return listy;
        }
        [WebMethod]
        public static List<string> getTableData6(int id,string uname) //TABLE FOR INCIDENT TYPES
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var incidenttypes = new List<MobileHotEventCategory>();
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    incidenttypes = MobileHotEventCategory.GetAllMobileHotEventCategories(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                    incidenttypes = MobileHotEventCategory.GetAllMobileHotEventCategoriesBySiteId(userinfo.SiteId, dbConnection);
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    incidenttypes = MobileHotEventCategory.GetAllMobileHotEventCategoriesByCId(userinfo.CustomerInfoId, dbConnection);
                else if (userinfo.RoleId == (int)Role.Regional)
                    incidenttypes = MobileHotEventCategory.GetAllMobileHotEventCategoriesByLevel5(userinfo.ID, dbConnection);

                foreach (var item in incidenttypes)
                {
                    var action = "<a href='#' data-target='#editIncidentType'  data-toggle='modal'  onclick='editIncidentTypeChoice(&apos;" + item.CategoryId + "&apos;,&apos;" + item.Description + "&apos;,&apos;" + CommonUtility.getMobileHoteventCategoryUrlId(item.Url) + "&apos;)'><i class='fa fa-pencil' ></i>Edit</a><a href='#'  data-target='#deleteIncidentModal'  data-toggle='modal'  onclick='editIncidentTypeChoice(&apos;" + item.CategoryId + "&apos;,&apos;" + item.Description + "&apos;,&apos;" + CommonUtility.getMobileHoteventCategoryUrlId(item.Url) + "&apos;)'><i class='fa fa-trash'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)//userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                    {
                        if (item.CreatedBy != userinfo.Username)
                        {
                            action = string.Empty;
                        }
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Description + "</td><td>" + item.CustomerUName+ "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableData6", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getUserTableData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allusers = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        allusers.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            allusers.Add(subsubuser);
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    } 
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {

                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                var grouped = allusers.GroupBy(item => item.ID);
                allusers = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in allusers)
                {
                   // if (item.DeviceType == (int)Users.DeviceTypes.Mobile || item.DeviceType == (int)Users.DeviceTypes.MobileAndClient || item.RoleId == (int)Role.Manager)
                   // {
                        if (item.Username != userinfo.Username)
                        {
                            var returnstring = "<tr role='row' class='odd'><td>" + item.CustomerUName + "</td><td class='sorting_1'>" + item.Status + "</td><td><a href='#' class='red-color' id=" + item.ID + item.Username + " onclick='userchoiceTable(&apos;" + item.ID + "&apos;,&apos;" + item.Username + "&apos;,&apos;" + item.CustomerUName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                            listy.Add(returnstring);
                        }
                   // }
                    //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                }
                foreach (var dev in devices)
                {
                    if (dev.Status == 1)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Online</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='userchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                    else
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Offline</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='userchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getUserTableData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAssignUserTableData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allusers = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        allusers.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            allusers.Add(subsubuser);
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    } 
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                } 
                else if (userinfo.RoleId == (int)Role.Regional)
                {

                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                var grouped = allusers.GroupBy(item => item.ID);
                allusers = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in allusers)
                {
                    //if (item.DeviceType == (int)Users.DeviceTypes.Mobile || item.DeviceType == (int)Users.DeviceTypes.MobileAndClient || item.RoleId == (int)Role.Manager)
                    //{
                        if (item.Username != userinfo.Username)
                        {
                            var returnstring = "<tr role='row' class='odd'><td>" + item.CustomerUName + "</td><td class='sorting_1'>" + item.Status + "</td><td><a href='#' class='red-color' id=" + item.ID +"-"+item.Username + " onclick='dispatchUserchoiceTable(&apos;" + item.ID + "&apos;,&apos;" + item.Username + "&apos;,&apos;" + item.CustomerUName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                            listy.Add(returnstring);
                        }
                    //}
                }
                foreach (var dev in devices)
                {
                    if (dev.Status == 1)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Online</td><td><a href='#' class='red-color' id=" + newDev.PCName + "-" + newDev.PCName + " onclick='dispatchUserchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                    else
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Offline</td><td><a href='#' class='red-color' id=" + newDev.PCName + "-" + newDev.PCName + " onclick='dispatchUserchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getAssignUserTableData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAssigneeType(string id,string uname)
        {
 

            if (CommonUtility.isNumeric(id))
                return "User";
            else
                return "Device";
        }
        [WebMethod]
        public static List<string> getTableRowData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customEventData = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);

                if (customEventData != null)
                { 
                    if (customEventData.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        var mobEv = MobileHotEvent.GetMobileHotEventByGuid(customEventData.Identifier, dbConnection);
                        if (string.IsNullOrEmpty(mobEv.EventTypeName))
                            customEventData.Name = "None";
                        else
                            customEventData.Name = mobEv.EventTypeName;

                        customEventData.Description = mobEv.Comments;
                    }
                    else if (customEventData.EventType == CustomEvent.EventTypes.Request)
                    {
                        var reqEv = HotEvent.GetHotEventById(customEventData.Identifier, dbConnection);
                        var splitName = reqEv.Name.Split('^');
                        if (splitName.Length > 1)
                        {
                            customEventData.Description = "Request " + splitName[1];
                            customEventData.Name = splitName[1];
                        }
                        if (reqEv != null)
                        {
                            if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                customEventData.UserName = CommonUtility.getPCName(reqEv);
                        }
                    }
                    else if (customEventData.EventType == CustomEvent.EventTypes.HotEvent)
                    {
                        var reqEv = HotEvent.GetHotEventById(customEventData.Identifier, dbConnection);
                        if (reqEv != null)
                            customEventData.UserName = CommonUtility.getPCName(reqEv);
                    }
                    if (!string.IsNullOrEmpty(customEventData.UserName))
                    {
                        var getUserInfo = Users.GetUserByName(customEventData.UserName, dbConnection);
                        if (getUserInfo != null)
                        {
                            if (string.IsNullOrEmpty(customEventData.EmailAddress))
                                customEventData.EmailAddress = getUserInfo.Email;
                            if (string.IsNullOrEmpty(customEventData.PhoneNumber))
                                customEventData.PhoneNumber = getUserInfo.Telephone;
                        }
                    }


                    listy.Add(customEventData.CustomerUName);
                    listy.Add(customEventData.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customEventData.Event);
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        var details = string.Empty;
                        var EVHistory = EventHistory.GetEventHistoryByEventId(customEventData.EventId, dbConnection);
                        EVHistory = EVHistory.OrderBy(i => i.Id).ToList();
                        foreach (var rem in EVHistory)
                        {
                            if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                            {
                                if (rem.UserName == userinfo.Username)
                                {
                                    details = customEventData.StatusName + "-MyIncident";
                                }
                                else
                                {
                                    details = customEventData.StatusName;
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(details))
                            listy.Add(details);
                        else
                            listy.Add(customEventData.StatusName);
                    }
                    else
                        listy.Add(customEventData.StatusName);

                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customEventData.Latitude.ToString(), customEventData.Longtitude.ToString(), getClientLic);
                    if (!string.IsNullOrEmpty(geolocation))
                        listy.Add(geolocation);
                    else
                    {
                        var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, customEventData.EventId);
                        if (geofence.Count > 0)
                        {
                            geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        }
                        listy.Add(geolocation);
                    }
                    if (!string.IsNullOrEmpty(customEventData.ReceivedBy))
                    {
                        listy.Add(customEventData.ReceivedBy);
                    }
                    else
                    {
                        listy.Add(customEventData.CustomerUName);
                    }
                    listy.Add(customEventData.PhoneNumber);
                    listy.Add(customEventData.EmailAddress);
                    listy.Add(customEventData.Description);
                    if (!string.IsNullOrEmpty(customEventData.Instructions))
                    {
                        var splitIns = customEventData.Instructions.Split('|');
                        if (splitIns.Length > 0)
                            customEventData.Instructions = splitIns[0];
                    }
                    listy.Add(customEventData.Instructions);
                    listy.Add(customEventData.Name + "-" + customEventData.CustomerIncidentId);
                    listy.Add("circle-point " + CommonUtility.getImgIncidentPriority(customEventData.Event));
                    listy.Add(customEventData.Longtitude.ToString());
                    listy.Add(customEventData.Latitude.ToString());
                    var gettasks = UserTask.GetAllTaskByIncidentId(id, dbConnection);
                    var foundtask = false;
                    if (gettasks.Count > 0)
                    {
                        foreach (var task in gettasks)
                        {
                            if (task.Status != (int)TaskStatus.Cancelled)
                            {
                                foundtask = true;
                                break;
                            }
                        }
                    }
                    if(foundtask)
                        listy.Add("TASK");

                    var foundEsca = false;
                    var evsHistory = EventHistory.GetEventHistoryByEventId(id, dbConnection);
                    if (evsHistory.Count > 0)
                    {
                        if (customEventData.StatusName == "Escalated")//customEventData.Escalate == (int)CustomEvent.EscalateTypes.Escalate)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                        else if (customEventData.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                        else if (customEventData.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                    }
                    if(!foundEsca)
                        listy.Add("N/A");

                    if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                    {
                        if (customEventData.StatusName=="Escalated")//customEventData.Escalate == (int)CustomEvent.EscalateTypes.Escalate)
                        {
                            listy.Add("none");
                        }
                        else
                            listy.Add("block");
                    }
                    else
                    {
                        listy.Add("block");
                    }

                    listy.Add(customEventData.Notes);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableRowData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableRowDataTask(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.CustomerUName);
                    listy.Add(customData.CreateDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.ACustomerUName);
                    listy.Add(customData.StatusDescription);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geolocation);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Description);
                    listy.Add(customData.Notes);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.StartDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.CheckListNotes);
                    listy.Add("circle-point " + CommonUtility.getImgStatus(customData.StatusDescription));
                    var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(customData.TemplateCheckListId.ToString(), dbConnection);
                    if (parentTasks != null)
                        listy.Add(parentTasks.Name);
                    else
                        listy.Add("None");

                    listy.Add(customData.TaskTypeName);

                    if (customData.IncidentId > 0)
                    {
                        var getCus = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                        if (getCus != null)
                        {
                            if (getCus.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus.Identifier, dbConnection);
                                if (mobEv != null)
                                {
                                    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                    {

                                    }
                                    else
                                        getCus.Name = mobEv.EventTypeName;
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.HotEvent || getCus.EventType == CustomEvent.EventTypes.Request)
                            {
                                var reqEv = HotEvent.GetHotEventById(getCus.Identifier, dbConnection);
                                if (reqEv != null)
                                {
                                    if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                        getCus.UserName = CommonUtility.getPCName(reqEv);

                                    if (getCus.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var splitName = reqEv.Name.Split('^');
                                        if (splitName.Length > 1)
                                            getCus.Name = splitName[1];
                                    }
                                }
                            }

                            listy.Add(getCus.Name + "|" + customData.IncidentId);


                        }
                        else
                            listy.Add("None");
                    }
                    else
                        listy.Add("None");

                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableRowDataTask", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getEventHistoryData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allEventHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                foreach (var rem in allEventHistory)
                {
                    var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                    if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                        if (mobEv != null)
                        {
                            if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                gethotevent.Name = "None";
                            else
                                gethotevent.Name = mobEv.EventTypeName;
                        }
                    }
                    else if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                    {
                        var reqEv = HotEvent.GetHotEventById(gethotevent.Identifier, dbConnection);
                        var splitName = reqEv.Name.Split('^');
                        if (splitName.Length > 1)
                        {
                            gethotevent.Name = splitName[1];
                        }
                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerUName;
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "engaged ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";

                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);


                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getEventHistoryData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var eventData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);//EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(eventData.Id, dbConnection);

                var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(id), dbConnection);
                if (tasklinks.Count > 0)
                {
                    foreach (var link in tasklinks)
                    {
                        var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                        foreach (var forl in forlink)
                        {
                            forl.isSubTask = true;
                            taskeventhistory.Add(forl);
                        }
                    }
                    taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                }

                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                foreach (var task in taskeventhistory)
                {
                    var taskn = "Task";
                    if (task.isSubTask)
                    {
                        taskn = "Sub-task";
                    }
                    if (eventData.AssigneeType == (int)TaskAssigneeType.Group)
                    {
                        if (task.Action == (int)TaskAction.Update)
                        {
                            acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                                + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> updated<span class='red-color'>" + taskn
                                + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                            listy.Add(acceptedData);
                        }
                    }

                    if (task.Action == (int)TaskAction.Complete)
                    {
                        completedBy = task.CustomerUName;
                        var compLocation = string.Empty;
                        var geoLoc = ReverseGeocode.RetrieveFormatedAddress(eventData.EndLatitude.ToString(), eventData.EndLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc))
                            compLocation = " at " + geoLoc;
                        var compInfo = "completed";
                        var compstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + completedBy + "</span>" + compInfo + "<span class='red-color'>" + taskn
                            + "</span>" + compLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(compstring);
                    }
                    else if (task.Action == (int)TaskAction.InProgress)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "started";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.OnRoute)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "on route";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Pause)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "paused";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Resume)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "resumed";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Accepted)
                    {
                        acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> accepted<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(acceptedData);
                    }
                    else if (task.Action == (int)TaskAction.Rejected)
                    {
                        rejectedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> rejected<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(rejectedData);
                    }
                    else if (task.Action == (int)TaskAction.Assigned)
                    {
                        assignedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> assigned<span class='red-color'>" + taskn
                            + "</span> to " + task.ACustomerUName + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(assignedData);
                    }
                    else if (task.Action == (int)TaskAction.Pending)
                    {
                        var incidentLocation = string.Empty;
                        var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(eventData.Latitude.ToString(), eventData.Longitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc3))
                            incidentLocation = " at " + geoLoc3;
                        var actioninfo = "created";

                        var gUser = Users.GetUserByName(eventData.CreatedBy, dbConnection);

                        var dataString = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + gUser.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + taskn
                            + "</span>for " + task.ACustomerUName + " " + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(dataString);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTaskHistoryData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> taskgetAttachmentData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<video id='Video" + i + " width='100%' height='380px' muted controls ><source src='" + item.DocumentPath + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                //if (item.ChecklistId == 0)
                                //{
                                //    var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source src='" + item.DocumentPath + "' type='audio/mpeg' /></audio>";
                                //    listy.Add(retstring);
                                //    i++;
                                //}
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img onclick='rotateMe(this);' src='" + item.DocumentPath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }
                            
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "taskgetAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideTaskplay()' data-target='#tasklocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Task Location</p></div></div></div>";
                var i = 1;
                var iTab = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideTaskplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                if (item.ChecklistId == 0)
                                {
                                    //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                    listy += retstring;
                                    //iTab++;
                                }
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                                var imgstring = String.Format(item.DocumentPath);

                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideTaskplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;

                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstring = "<div class='row static-height-with-border clickable-row' onclick='hideTaskplay()' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getAttachmentDataTab", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataIcons(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;

                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {

                            if (i == 4)
                            {
                                var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#taskattachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                listy += retstring;
                                i++;
                                break;
                            }
                            else
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideTaskplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                {
                                    if (item.ChecklistId == 0)
                                    {
                                        //var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)'/>";

                                        listy += retstring;
                                        i++;
                                    }
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    var imgstring = String.Format(item.DocumentPath);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideTaskplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getAttachmentDataIcons", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataIcons(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection); 
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);
                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;

                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {

                                        var imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {
                                        imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + 0 + ")' data-target='#video-" + i + "-tab'/>";
                            listy += retstring;
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {
                                        imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' onclick='hideIncidentplay();hideTaskplay()'/>";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {
                                        imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getAttachmentDataIcons", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#location-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Incident Location</p></div></div></div>";

                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);

                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {
                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");

                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                               // iTab++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {
                                var imgstring = requiredString;

                                var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;
                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    //iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {
                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                    //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + 0 + ")' data-target='#video-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Playback</p></div></div></div>";
                            listy += retstring;
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    //iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstring = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border onclick='hideIncidentplay()' clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    //iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()' ><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "getAttachmentDataTab", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);

                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);
                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {

                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" + requiredString + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                            {
                                //var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source src='" + requiredString + "' type='audio/mpeg' /></audio>";
                                //listy.Add(retstring);
                                //i++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {

                                var imgstring = requiredString;
                                var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }

                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" +  requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    //var retstring = "<audio id='Video" + i + "' width='100%' height='380px'  controls ><source type='audio/mpeg' src='" + requiredString + "' /></audio>";
                                    //listy.Add(retstring);
                                    //i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring = requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" + hotEv.playbackURL + "' /></video>";
                            listy.Add(retstring);
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" + requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    //var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source type='audio/mpeg' src='" + requiredString + "' /></audio>";
                                    //listy.Add(retstring);
                                    //i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring = requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" + requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    //var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source type='audio/mpeg' src='" + requiredString + "' /></audio>";
                                    //listy.Add(retstring);
                                    //i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring = requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        public void getEventStatus(Users userinfo)
        {
            var cusEvents = new List<CustomEvent>();
            var mobcusEvents = new List<CustomEvent>();
            var hotcusEvents = new List<CustomEvent>();
            var reqcusEvents = new List<CustomEvent>();
            pendingCount = 0;
            inprogressCount = 0;
            completedCount = 0;
            total = 0;
            var mobileHotCount = 0;
            var HotCount = 0;
            var RequestCount = 0;
            var systemPending = 0;
            var systemInprogress = 0;
            var systemCompleted = 0;
            var systemTotal = 0;
            try
            {
                if (userinfo != null)
                { 
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                        var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);
 
                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                cusEvents.Add(custom);
                        }
                        foreach (var custom in reqcustomeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                cusEvents.Add(custom);
                        }
                       
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                        
                        foreach (var manguser in managerusers)
                        {
                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                            var pclist = new List<string>();
                            var customeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                            var reqcustomeventlist = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);
 
                            foreach (var custom in customeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    cusEvents.Add(custom);
                            }
                            foreach (var custom in reqcustomeventlist)
                            {
                                var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                                if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                    cusEvents.Add(custom);
                            }
                            
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId, dbConnection));
                        
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                       // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                       // foreach (var site in sites)
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));

                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));

                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        cusEvents.AddRange(CustomEvent.GetAllCustomEvents(dbConnection));

                    }
                    if (cusEvents.Count > 0)
                    {

                        var grouped = cusEvents.GroupBy(item => item.EventId);
                        cusEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                        var isValid = false;
                        var isMangUser = false;

                        if (userinfo.RoleId == (int)Role.Manager)
                            isMangUser = true;
                        
                        foreach (var evs in cusEvents)
                        {

                            if (isMangUser)
                            {
                                if (evs.HandledBy == "LiveMIMSAlarmService" || evs.HandledBy == userinfo.Username)
                                    isValid = true;
                                else
                                    isValid = false;
                            }
                            else
                                isValid = true;

                            if (isValid)
                            {
                                if (evs.EventType != (int)CustomEvent.EventTypes.DriverOffence)
                                {
                                    if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        mobcusEvents.Add(evs);
                                    else if (evs.EventType == CustomEvent.EventTypes.HotEvent)
                                        hotcusEvents.Add(evs);
                                    else if (evs.EventType == CustomEvent.EventTypes.Request)
                                        reqcusEvents.Add(evs);

                                    if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                    {
                                        if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent || evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            total++;
                                            pendingCount++;
                                        }
                                    }
                                    else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                    {
                                        if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                            mobileHotCount++;
                                        else if (evs.EventType == CustomEvent.EventTypes.HotEvent)
                                            HotCount++;
                                        else if (evs.EventType == CustomEvent.EventTypes.Request)
                                            RequestCount++;

                                        if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent || evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            total++;
                                            completedCount++; //complete count
                                        }
                                        else if (evs.IncidentType == (int)Incidents.IncidentTypes.SystemCreated)
                                            systemCompleted++;

                                    }
                                    else
                                    {
                                        if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                            mobileHotCount++;
                                        else if (evs.EventType == CustomEvent.EventTypes.HotEvent)
                                            HotCount++;
                                        else if (evs.EventType == CustomEvent.EventTypes.Request)
                                            RequestCount++;

                                        if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent || evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            if (evs.StatusName != "Escalated")
                                                total++;
                                        }
                                        else if (evs.IncidentType == (int)Incidents.IncidentTypes.SystemCreated)
                                        {
                                            if (evs.IncidentStatus != (int)CustomEvent.IncidentActionStatus.Resolve)
                                            {
                                                systemInprogress++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    systemTotal = systemPending + systemCompleted + systemInprogress;
                    lbTotalSystemCreated.Text = systemTotal.ToString();
                    inprogressCount = total - (pendingCount + completedCount);

                    if (systemPending > 0)
                    {
                        pendingSystemPercent = ((int)Math.Round((float)systemPending / (float)systemTotal * (float)100)).ToString();
                    }
                    else
                    {
                        pendingSystemPercent = "0";
                    }
                    if (systemCompleted > 0)
                    {
                        completedSystemPercent = ((int)Math.Round((float)systemCompleted / (float)systemTotal * (float)100)).ToString();
                    }
                    else
                    {
                        completedSystemPercent = "0";
                    }
                    if (systemInprogress > 0)
                    {
                        inprogressSystemPercent = ((int)Math.Round((float)systemInprogress / (float)systemTotal * (float)100)).ToString();
                    }
                    else
                    {
                        inprogressSystemPercent = "0";
                    }
                    if ((systemCompleted + systemInprogress) > 0)
                    {
                        systemHandledPercentage = ((int)Math.Round((float)(systemCompleted + systemInprogress) / (float)systemTotal * (float)100)).ToString();
                    }
                    else
                    {
                        systemHandledPercentage = "0";
                    }



                    lbCompletedSystemPercent.Text = systemCompleted.ToString();
                    lbInprogressSystemPercent.Text = systemInprogress.ToString();
                    lbPendingSystemPercent.Text = systemPending.ToString();

                    pending3rdPartyPercent = "0";
                    inprogress3rdPartyPercent = "0";
                    completed3rdPartyPercent = "0";
                    thirdpartyHandledPercentage = "0";
                    lbpending3rdParty.Text = "0";
                    lbcompleted3rdParty.Text = "0";
                    lbinprogress3rdParty.Text = "0";
                    lbtotal3rdParty.Text = "0";

                    var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                    var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                    var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                    var mobHotPercentage = (int)Math.Round((float)mobileHotCount / (float)mobcusEvents.Count * (float)100);
                    var hotPercentage = (int)Math.Round((float)HotCount / (float)hotcusEvents.Count * (float)100);
                    var requestPercentage = (int)Math.Round((float)RequestCount / (float)reqcusEvents.Count * (float)100);

                    if (RequestCount > 0)
                        RequestHandledPercentage = requestPercentage.ToString();
                    else
                        RequestHandledPercentage = "0";



                    if (mobileHotCount > 0)
                        MobileHotEventTableControl.MobileHandledPercentage = mobHotPercentage.ToString();
                    else
                        MobileHotEventTableControl.MobileHandledPercentage = "0";

                    //if (HotCount > 0)
                    //    HotEventTableControl.MobileHandledPercentage = hotPercentage.ToString();
                    //else
                    //    HotEventTableControl.MobileHandledPercentage = "0";

                    if (pendingCount > 0)
                    {
                        pendingPercent = pendingPercentage.ToString();
                        lbPending.Text = pendingCount.ToString();
                        lbPendingpercent.Text = pendingPercentage.ToString();
                    }
                    else
                    {
                        lbPending.Text = "0";
                        lbPendingpercent.Text = "0";
                        pendingPercent = "0";
                    }
                    if (inprogressCount > 0)
                    {
                        inprogressPercent = inprogressPercentage.ToString();
                        lbInprogress.Text = inprogressCount.ToString();
                        lbInprogresspercent.Text = inprogressPercentage.ToString();
                    }
                    else
                    {
                        inprogressPercent = "0";
                        lbInprogresspercent.Text = "0";
                        lbInprogress.Text = "0";
                    }
                    if (completedCount > 0)
                    {
                        completedPercent = completedPercentage.ToString();
                        lbCompleted.Text = completedCount.ToString();
                        lbCompletedpercent.Text = completedPercentage.ToString();
                    }
                    else
                    {
                        completedPercent = "0";
                        lbCompleted.Text = "0";
                        lbCompletedpercent.Text = "0";
                    }




                    lbTotalAlarms.Text = total.ToString();
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getEventStatus", er, dbConnection, userinfo.SiteId);
            }
        }
        [WebMethod]
        public static string changeIncidentState(string id, string state, string ins,string uname)
        {
            int taskId1 = 0;
            var retVal = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retValve = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retValve)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                 
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (!string.IsNullOrEmpty(id))
                    {
                        var oldIInfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        incidentinfo.UpdatedBy = userinfo.Username;

                        if (state != "Reject" && state != "Resolve")
                            incidentinfo.Instructions = ins;

                        incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue(state.ToLower());
                        incidentinfo.Handled = false;
                        //incidentinfo.SiteId = userinfo.SiteId;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        var returnV = CommonUtility.CreateIncident(incidentinfo);
                        if (returnV != "")
                        {
                            taskId1 = Convert.ToInt32(returnV);
                            CustomEvent.CustomEventHandledById(taskId1, false, userinfo.Username, dbConnection);
                            //taskId1 = CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection);
                            retVal = incidentinfo.Name;

                            if (incidentinfo.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                var getMobName = MobileHotEvent.GetMobileHotEventByGuid(incidentinfo.Identifier, dbConnection);
                                {
                                    if (string.IsNullOrEmpty(getMobName.EventTypeName))
                                        retVal = "None";
                                    else
                                        retVal = getMobName.EventTypeName;

                                }
                            }

                            if (taskId1 < 1)
                            {
                                taskId1 = Convert.ToInt32(id);

                            }

                            if (incidentinfo.IncidentStatus != (int)CustomEvent.IncidentActionStatus.Dispatch)
                            {
                                if (oldIInfo.IncidentStatus != incidentinfo.IncidentStatus)
                                {
                                    var EventHistoryEntry = new EventHistory();
                                    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                                    EventHistoryEntry.CreatedBy = userinfo.Username;
                                    EventHistoryEntry.EventId = taskId1;
                                    EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                                    EventHistoryEntry.UserName = userinfo.Username;
                                    EventHistoryEntry.Remarks = ins;
                                    EventHistoryEntry.SiteId = userinfo.SiteId;
                                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                                }
                            }
                            else
                            {

                                //Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                                var notifications = Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                if (notifications.Count > 0)
                                {
                                    var firstNotification = notifications.FirstOrDefault();
                                    if (firstNotification.AssigneeType == (int)Notification.NotificationAssigneeType.User)
                                    {
                                        var userNotification = notifications.Where(x => x.AssigneeId == userinfo.ID).FirstOrDefault();
                                        if (userNotification != null)
                                        {
                                            Arrowlabs.Business.Layer.Notification.UpdateNotificationStatus(userNotification.Id, true, dbConnection);
                                            //TraceBackHistory.DeleteTraceBackHistoryByIncidentId(userNotification.Id, dbConnection);
                                        }
                                    }
                                }
                                
                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    task.Status = (int)TaskStatus.Cancelled;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }
                            }
                            if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                            {
                                incidentinfo.Handled = true;
                                incidentinfo.HandledTime = CommonUtility.getDTNow();
                                incidentinfo.HandledBy = userinfo.Username;
                                incidentinfo.SiteId = userinfo.SiteId;
                                taskId1 = CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection, dbConnectionAudit, true);
                                if (taskId1 < 1)
                                {
                                    taskId1 = Convert.ToInt32(id);
                                    incidentinfo.EventId = Convert.ToInt32(id);
                                }
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    var tskhistory = new TaskEventHistory();
                                    if (task.Status == (int)TaskStatus.Completed)
                                    {
                                        task.Status = (int)TaskStatus.Accepted;
                                        tskhistory.Action = (int)TaskAction.Accepted;
                                        tskhistory.Remarks = "Incident connected to task has been resolved";
                                    }
                                    else
                                    {
                                        task.Status = (int)TaskStatus.Cancelled;
                                        tskhistory.Action = (int)TaskAction.Rejected;
                                        tskhistory.Remarks = "Incident connected to task has been resolved prior to completion";
                                    }
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }
                                CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection);
                                CustomEvent.CustomEventHandledById(incidentinfo.EventId, true, userinfo.Username, dbConnection);
                            }
                            else if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Release)
                            {
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true ,dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {

                                    var tskhistory = new TaskEventHistory();

                                    task.Status = (int)TaskStatus.Cancelled;
                                    tskhistory.Action = (int)TaskAction.Rejected;
                                    tskhistory.Remarks = "Incident connected to task has been released prior to completion";
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);

                                }
                            }
                            else if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                            {
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(incidentinfo.EventId, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, CommonUtility.dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(incidentinfo.EventId, true,dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(incidentinfo.EventId, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    var tskhistory = new TaskEventHistory();
                                    task.Status = (int)TaskStatus.Rejected;
                                    tskhistory.Action = (int)TaskAction.Rejected;
                                    tskhistory.Remarks = "Incident connected to task has been rejected prior to completion";
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }
                            }
                        }
                    }

                }
                catch (Exception er)
                {
                    MIMSLog.MIMSLogSave("Incident", "changeIncidentState", er, dbConnection, userinfo.SiteId);
                }
                return retVal;
            }
        }
        [WebMethod]
        public static List<string> rejectReDispatch(string id, string ins,string uname)
        {
            //int taskId1 = 0;
            var listy = new List<string>();
                        var newAuthorize = new MyAuthorize();
            var retValve = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retValve)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var uulist = new List<Users>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        listy.Add(incidentinfo.Name);
                        incidentinfo.UpdatedBy = userinfo.Username;
                        //incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue("reject");
                        //incidentinfo.SiteId = userinfo.SiteId;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        //var returnV = CommonUtility.CreateIncident(incidentinfo);
                        //if (returnV != "")
                        //{
                            //taskId1 = Convert.ToInt32(returnV);
                            //var retVal = incidentinfo.Name;

                            //if (incidentinfo.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            //{
                            //    var getMobName = MobileHotEvent.GetMobileHotEventByGuid(incidentinfo.Identifier, dbConnection);
                            //    {
                            //        if (string.IsNullOrEmpty(getMobName.EventTypeName))
                            //            retVal = "None";
                            //        else
                            //            retVal = getMobName.EventTypeName;

                            //    }
                            //}
                            //listy.Add(retVal);
                            //if (taskId1 < 1)
                            //{
                            //    taskId1 = Convert.ToInt32(id);
                            //}
                            var EventHistoryEntry = new EventHistory();
                            //EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            //EventHistoryEntry.CreatedBy = userinfo.Username;
                            //EventHistoryEntry.EventId = taskId1;
                            //EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                            //EventHistoryEntry.UserName = userinfo.Username;
                            //EventHistoryEntry.Remarks = ins;
                            //EventHistoryEntry.SiteId = userinfo.SiteId;
                            //EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                        //if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                        //{

                            var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(incidentinfo.EventId, dbConnection);
                            foreach (var noti in notifications)
                            {
                                if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                {
                                    var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                    taskuser.Engaged = false;
                                    taskuser.EngagedIn = string.Empty;


                                    if (!listy.Any(str => str.Contains(taskuser.Username)))
                                    {
                                        if (taskuser.Username != userinfo.Username)
                                        {
                                            //var evs = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection);
                                            //if(evs.Count > 0){
                                            //    if (evs[0].UserName == taskuser.Username)
                                            //    {
                                            //        listy.Add(taskuser.Username);
                                            //        listy.Add(noti.AssigneeId.ToString());
                                            //        uulist.Add(taskuser);
                                            //    }
                                            //}
                                            listy.Add(taskuser.Username);
                                            listy.Add(noti.AssigneeId.ToString());
                                            uulist.Add(taskuser);
                                        }

                                        Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                    }
                                    
                                }
                                else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                {
                                    var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                    foreach (var taskuser in groupusers)
                                    {
                                        var splitinfo = taskuser.EngagedIn.Split('-');
                                        if (splitinfo.Length > 1)
                                        {
                                            if (splitinfo[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                }
                            }
                            Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusPrefixByIncidentId(incidentinfo.EventId, true,"REJ" ,dbConnection);
                            var taskinfo = UserTask.GetAllTaskByIncidentId(incidentinfo.EventId, dbConnection);
                            foreach (var task in taskinfo)
                            {
                                task.Status = (int)TaskStatus.Pending;
                                task.RejectionNotes = ins;
                                task.CustomerId = userinfo.CustomerInfoId;
                                UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                var tskhistory = new TaskEventHistory();
                                tskhistory.Action = (int)TaskAction.Assigned;
                                tskhistory.CreatedBy = userinfo.Username;
                                tskhistory.Remarks = task.AssigneeName;
                                tskhistory.CreatedDate = CommonUtility.getDTNow();
                                tskhistory.TaskId = task.Id;
                                tskhistory.SiteId = userinfo.SiteId;
                                tskhistory.CustomerId = userinfo.CustomerInfoId;
                                TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                            }
                        //}
                            incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                            incidentinfo.UpdatedBy = userinfo.Username;
                            incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue("dispatch");
                            //incidentinfo.SiteId = userinfo.SiteId;
                            incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                            var returnV = CommonUtility.CreateIncident(incidentinfo);
                            if (returnV != "")
                            {
                                //foreach (var u in uulist)
                                //{
                                //    MIMSLog.MIMSLogSave(ins, id, new Exception(), dbConnection);
                                //    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                                //    EventHistoryEntry.CreatedBy = userinfo.Username;
                                //    EventHistoryEntry.EventId = Convert.ToInt32(id);
                                //    EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                                //    EventHistoryEntry.UserName = u.Username;
                                //    EventHistoryEntry.Remarks = ins;
                                //    EventHistoryEntry.SiteId = userinfo.SiteId;
                                //    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                                //}
                            }
                            else
                            {
                                listy.Clear();
                            }
                        //}
                    }
                }
                catch (Exception er)
                {
                    listy.Clear();
                    MIMSLog.MIMSLogSave("Incident", "rejectReDispatch", er, dbConnection, userinfo.SiteId);
                }
                
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getLocationById(string id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (!string.IsNullOrEmpty(id))
                {
                    if (Convert.ToInt32(id) > 0)
                    {
                        var getloc = Location.GetLocationById(Convert.ToInt32(id), dbConnection);
                        var vertices = string.Empty;
                        if (getloc != null)
                        {
                            if (getloc.LocationTypeId > 1)
                            {
                                var geolocs = GeofenceLocation.GetAllGeofenceLocationbyLocationId(dbConnection, getloc.ID);
                                foreach (var geo in geolocs)
                                {
                                    listy.Add("(" + geo.Latitude.ToString() + "," + geo.Longitude.ToString() + ")");
                                    //listy.Add(geo.Longitude.ToString());
                                }
                            }
                            else
                            {
                                listy.Add(getloc.Longitude.ToString());
                                listy.Add(getloc.Latitude.ToString());
                            }
                        }
                    }
                    else
                    {
                        listy.Add(string.Empty);
                        listy.Add(string.Empty);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getLocationById", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getTimerDuration(int id)
        {
            var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
            return settings.MIMSMobileGPSInterval.ToString();
        }
        [WebMethod]
        public static string getGPSDataUsers(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var users = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();

                if (userinfo.RoleId == (int)Role.Manager)
                {
                    users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    users = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);
                    users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        users.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            users.Add(subsubuser);
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        users.Add(subsubuser);
                    } 
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                } 
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                }
                else if(userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    users = Users.GetAllUsers(dbConnection);
                    users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    users = Users.GetAllUsers(dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                users = users.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                json += "[";
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        var grouped = users.GroupBy(item => item.ID);
                        users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                        foreach (var item in users)
                        {
                            //if (item.DeviceType == (int)Users.DeviceTypes.Mobile || item.DeviceType == (int)Users.DeviceTypes.MobileAndClient || item.RoleId == (int)Role.Manager)
                            //{
                                var newLoginDate = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString("d MMM  hh:mm tt");
                                if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"DisplayName\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                }
                                else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"DisplayName\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"Retrieve\"},";
                                }
                                else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"DisplayName\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"Retrieve\"},";
                                }
                            //}
                        }
                        foreach (var dev in devices)
                        {
                            if (dev.Status == 1)
                            {
                                var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                                json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else
                            {
                                var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                                json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"OFFCLIENT\",\"Logs\" : \"Retrieve\"},";

                            }
                        }
                        var item2 = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        if (item2 != null)
                            json += "{ \"Username\" : \"" + item2.Name + "\",\"Id\" : \"" + item2.EventId.ToString() + "\",\"Long\" : \"" + item2.Longtitude.ToString() + "\",\"Lat\" : \"" + item2.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getGPSDataUsers", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getGPSData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var users = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();

                //var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        users.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            users.Add(subsubuser);
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        users.Add(subsubuser);
                    } 
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                    // devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    users = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);
                    users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                   // devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                   // devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    users = Users.GetAllUsers(dbConnection);
                    users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    users = Users.GetAllUsers(dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                users = users.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();

                users = users.Where(i => i.Username != userinfo.Username).ToList();

                json += "[";
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        var grouped = users.GroupBy(item => item.ID);
                        users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                        foreach (var item in users)
                        {
                            //if (item.DeviceType == (int)Users.DeviceTypes.Mobile || item.DeviceType == (int)Users.DeviceTypes.MobileAndClient || item.RoleId ==(int)Role.Manager)
                            //{
                                var newLoginDate = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString("d MMM  hh:mm tt");
                                //if (item.LastLogin.Value > dt)
                                if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"DisplayName\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                }
                                else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"DisplayName\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"Retrieve\"},";
                                }
                                else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"DisplayName\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"Retrieve\"},";
                                }
                            //}
                        }
                        foreach (var dev in devices)
                        {
                            if (dev.Status == 1)
                            {
                                var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                                //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                                json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else
                            {
                                var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                                //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                                json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"OFFCLIENT\",\"Logs\" : \"Retrieve\"},";

                            }
                        }
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getGPSData", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getIncidentLocationData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                json += "[";
                if (id > 0)
                {
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            var item = CustomEvent.GetCustomEventById(id, dbConnection);
                            if (item.EventType == CustomEvent.EventTypes.Incident)
                            {
                                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                                if (geofence.Count > 0)
                                {
                                    foreach (var geo in geofence)
                                    {
                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                    }
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getIncidentLocationData", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string getTaskLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            if (item.StatusDescription == "Pending")
                            {
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                            {
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "OnRoute")
                            {
                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || item.StatusDescription == "Rejected")
                            {
                                if (item.EndLatitude > 0 && item.EndLongitude > 0)
                                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getLocationData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var locs = new List<Location>();
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    locs.AddRange(Location.GetAllLocation(dbConnection));
                    locs.AddRange(Location.GetAllSubLocation(dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                    locs.AddRange(Location.GetAllLocationBySiteId(userinfo.SiteId, dbConnection));
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    locs.AddRange(Location.GetAllLocationByLevel5(userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    locs.AddRange(Location.GetAllLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                }
                json += "[";
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        //var dt = CommonUtility.getDTNow().Subtract(new TimeSpan(0, Convert.ToInt32(settings.MIMSMobileGPSInterval), 15));
                        if (locs.Count > 0)
                        {
                            foreach (var item in locs)
                            {
                                if (item.LocationTypeId > 1)
                                {
                                    var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyLocationId(dbConnection, item.ID);
                                    foreach (var geofence in geofencelocs)
                                    {
                                        json += "{ \"Username\" : \"" + item.LocationDesc + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED" + item.ID.ToString() + "\",\"Logs\" : \"Retrieve\"},";
                                    }
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.LocationDesc + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                        }
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getLocationData", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getEmptyLocationData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            //var locs = Location.GetAllLocation(dbConnection);

            json += "[";
            //var dt = CommonUtility.getDTNow().Subtract(new TimeSpan(0, Convert.ToInt32(settings.MIMSMobileGPSInterval), 15));
            //foreach (var item in locs)
            //{
            //    json += "{ \"Username\" : \"" + item.LocationDesc + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
            //}
            //json = json.Substring(0, json.Length - 1);
            json += "]";
            return json;
        }
        [WebMethod]
        public static string InsertVertices(int id, string vertices,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var pairArray = vertices.Split(')');
                var geoLongi = string.Empty;
                var geoLatitude = string.Empty;
                for (int i = 0; i < pairArray.Length; i++)
                {
                    if (pairArray[i].StartsWith("("))
                    {
                        pairArray[i] = pairArray[i].Substring(1, pairArray[i].Length - 1);
                    }
                    if (pairArray[i].StartsWith(",("))
                    {
                        pairArray[i] = pairArray[i].Substring(2, pairArray[i].Length - 2);
                    }

                }
                foreach (var item in pairArray)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        var newGeoFence = new GeofenceLocation();
                        newGeoFence.Latitude = Convert.ToDouble(item.Split(',')[0]);
                        newGeoFence.Longitude = Convert.ToDouble(item.Split(',')[1]);
                        newGeoFence.IncidentId = id;
                        newGeoFence.CreatedBy = userinfo.Username;

                        newGeoFence.SiteId = userinfo.SiteId;
                        newGeoFence.CustomerId = userinfo.CustomerInfoId;

                        newGeoFence.CreatedDate = CommonUtility.getDTNow();
                        newGeoFence.UpdatedDate = CommonUtility.getDTNow();
                        GeofenceLocation.InsertOrUpdateGeofenceLocation(newGeoFence, dbConnection, dbConnectionAudit, true);
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "InsertVertices", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        protected string cusEvId { get; set; }
        [WebMethod]
        public static List<string> getChecklistNotesData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                foreach (var item in sessions)
                {
                    if (item.CheckListItemType == 5)
                    {
                        if (item.ChildCheckList != null)
                        {
                            foreach (var child in item.ChildCheckList)
                            {
                                if (!child.IsChecked)
                                    listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incidents", "getChecklistNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getCanvasNotesData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var sessions = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var count = 1;
                foreach (var item in sessions)
                {
                    if (!string.IsNullOrEmpty(item.DocumentPath))
                    {
                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                        {
                            listy.Add("Attachment " + count + "|" + item.ImageNote);
                        }
                        count++;    //listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incidents", "getCanvasNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getChecklistData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var chklisty = new List<string>();
                var isAllChecked = true;
                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);


                var groupedlist = new List<TaskCheckList>();
                var groupedItems = sessions.GroupBy(x => x.Id);
                var groupedlistItems = groupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                foreach (var child in groupedlistItems)
                {
                    var attachfiles = sessions.Where(i => i.Id == child.Id).ToList();
                    var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                    if (mp3files.Count > 0)
                    {
                        //Yes
                        groupedlist.Add(mp3files[0]);
                    }
                    else
                    {
                        groupedlist.Add(attachfiles[0]);
                    }
                }

                foreach (var item in groupedlist)
                {
                    var stringCheck = string.Empty;
                    if (item.IsChecked)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                        isAllChecked = false;
                    }
                    var imgsrc = string.Empty;
                    if (!string.IsNullOrEmpty(item.DocumentPath) && System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                    {

                        imgsrc = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                    }

                    if (item.CheckListItemType == 4 || item.CheckListItemType == 5)
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|True|" + imgsrc);
                    }
                    else if (item.CheckListItemType == 3)
                    {
                        if (!string.IsNullOrEmpty(item.TemplateCheckListItemNote))
                            chklisty.Add(item.Name + "|" + stringCheck + "|3|" + item.TemplateCheckListItemNote + "|" + imgsrc);
                        else
                            chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    else
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    if (item.ChildCheckList != null)
                    {
                        //var audiofiles = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                        //var glist = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() != ".MP3").ToList();

                        var childgroupedlist = new List<TaskCheckList>();
                        var childgroupedItems = item.ChildCheckList.GroupBy(x => x.Id);
                        var childgroupedlistItems = childgroupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                        foreach (var child in childgroupedlistItems)
                        {
                            var attachfiles = item.ChildCheckList.Where(i => i.Id == child.Id).ToList();
                            var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                            if (mp3files.Count > 0)
                            {
                                //Yes
                                childgroupedlist.Add(mp3files[0]);
                            }
                            else
                            {
                                childgroupedlist.Add(attachfiles[0]);
                            }
                        }

                        foreach (var child in childgroupedlist)
                        {
                            var imgsrc2 = string.Empty;
                            if (!string.IsNullOrEmpty(child.DocumentPath) && System.IO.Path.GetExtension(child.DocumentPath).ToUpperInvariant() == ".MP3")
                            {

                                imgsrc2 = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + child.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                            }

                            if (child.IsChecked)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                                isAllChecked = false;
                            }
                            chklisty.Add(child.Name + "|" + stringCheck + "|False|" + imgsrc2);
                        }
                    }
                }
                listy.Add(isAllChecked.ToString());
                listy.AddRange(chklisty);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getChecklistData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskListData(int id,string uname)
        {

            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);
                if (usertask.Count > 0)
                    listy.Add(usertask[0].Name);
                foreach (var task in usertask)
                {
                    if (task.Status != (int)TaskStatus.Cancelled)
                        listy.Add(task.ACustomerUName + "|" + task.Id);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTaskListData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string inserttask(string id, string assigneetype, string assigneename, string assigneeid, string templatename, string longi, string lati, int incidentId,string uname)
        {
            int taskId1 = 0;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var dtnow = CommonUtility.getDTNow();
                if (Convert.ToBoolean(id))
                {
                    if (Convert.ToInt32(templatename) > 0)
                    {
                        var temptasks = UserTask.GetTaskById(Convert.ToInt32(templatename), dbConnection);
                        var newTasks = new UserTask();
                        newTasks.Name = temptasks.Name;
                        newTasks.Description = temptasks.Description;
                        newTasks.IncidentId = Convert.ToInt32(incidentId);
                        if (assigneetype == TaskAssigneeType.Location.ToString())
                        {
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);
                            newTasks.AssigneeType = (int)TaskAssigneeType.Location;
                        }
                        else if (assigneetype == TaskAssigneeType.Group.ToString())
                        {
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);
                            newTasks.AssigneeType = (int)TaskAssigneeType.Group;
                        }
                        else if (assigneetype == TaskAssigneeType.User.ToString())
                        {
                            newTasks.AssigneeType = (int)TaskAssigneeType.User;
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);

                        }
                        else if (assigneetype == TaskAssigneeType.Device.ToString())
                        {
                            newTasks.AssigneeType = (int)TaskAssigneeType.Device;
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);

                        }
                        newTasks.CreateDate = dtnow;
                        newTasks.CreatedBy = userinfo.Username;
                        newTasks.ManagerName = userinfo.Username;
                        newTasks.ManagerId = userinfo.ID;
                        newTasks.TaskTypeId = temptasks.TaskTypeId;
                        newTasks.IsDeleted = false;
                        newTasks.UpdatedDate = dtnow;

                        if (!string.IsNullOrEmpty(longi))
                            newTasks.Longitude = Convert.ToDouble(longi);
                        else
                            newTasks.Longitude = 0;

                        if (!string.IsNullOrEmpty(lati))
                            newTasks.Latitude = Convert.ToDouble(lati);
                        else
                            newTasks.Latitude = 0;


                        newTasks.IsSignature = temptasks.IsSignature;



                        var selectedTaskStartDateTime = dtnow;

                        newTasks.StartDate = selectedTaskStartDateTime;
                        newTasks.EndDate = selectedTaskStartDateTime;

                        newTasks.Priority = temptasks.Priority;
                        newTasks.TemplateCheckListId = temptasks.TemplateCheckListId;
                        newTasks.SiteId = userinfo.SiteId;
                        newTasks.CustomerId = userinfo.CustomerInfoId;
                        taskId1 = UserTask.InsertorUpdateTask(newTasks, dbConnection, dbConnectionAudit, true);
                        newTasks.Id = taskId1;

                        if (newTasks.IsSignature)
                            UserTask.UpdateTaskSignature(newTasks.Id, true, dbConnection);

                        var tskhistory = new TaskEventHistory();
                        tskhistory.Action = (int)TaskAction.Pending;
                        tskhistory.CreatedBy = userinfo.Username;
                        tskhistory.CreatedDate = CommonUtility.getDTNow();
                        tskhistory.TaskId = taskId1;
                        tskhistory.SiteId = userinfo.SiteId;
                        tskhistory.CustomerId = userinfo.CustomerInfoId;
                        TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);

                        var allTemplateCheckListItems = TemplateCheckListItem.GetAllTemplateCheckListItems(dbConnection);
                        if (allTemplateCheckListItems != null && allTemplateCheckListItems.Count > 0)
                        {
                            var selectedTemplateCheckListItems = allTemplateCheckListItems.Where(i => i.ParentCheckListId == newTasks.TemplateCheckListId).ToList();
                            if (selectedTemplateCheckListItems.Count > 0)
                            {
                                foreach (var templateCheckListItem in selectedTemplateCheckListItems)
                                {
                                    var taskCheckList = new TaskCheckList();
                                    taskCheckList.IsChecked = templateCheckListItem.IsChecked;
                                    taskCheckList.TaskId = taskId1;
                                    taskCheckList.UpdatedDate = dtnow;
                                    taskCheckList.CreatedDate = dtnow;
                                    taskCheckList.CreatedBy = Environment.UserName;
                                    taskCheckList.TemplateCheckListItemId = templateCheckListItem.Id;
                                    taskCheckList.SiteId = userinfo.SiteId;
                                    taskCheckList.CustomerId = userinfo.CustomerInfoId;
                                    TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckList, dbConnection,dbConnectionAudit,true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "inserttask", er, dbConnection, userinfo.SiteId);
            }
            return taskId1.ToString();
        }
        [WebMethod]
        public static string insertNewIncident(string name, string desc, string locationid, string incidenttype, string notificationid, string taskid, string receivedby, string longi, string lati, string status, string instructions, string msgtask, string phoneNo, string emailAdd,string uname)
        {
            int taskId1 = 0;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var newincident = new CustomEvent();
                var dtnow = CommonUtility.getDTNow();
                newincident.UserName = userinfo.Username;
                newincident.Identifier = Guid.NewGuid();
                newincident.RecevieTime = dtnow;
                newincident.EventType = CustomEvent.EventTypes.Incident;
                newincident.Name = name;
                newincident.Description = desc;
                newincident.LocationId = Convert.ToInt32(locationid);
                newincident.IncidentType = Convert.ToInt32(incidenttype);
                newincident.ReceivedBy = receivedby;
                newincident.CreatedBy = userinfo.Username;
                newincident.CreatedDate = dtnow;
                newincident.Longtitude = longi;
                newincident.Latitude = lati;
                newincident.Instructions = instructions;
                newincident.UpdatedBy = userinfo.Username;
                if (Convert.ToBoolean(msgtask))
                    newincident.TemplateTaskId = Convert.ToInt32(taskid);
                else
                    newincident.TemplateTaskId = 0;

                newincident.SiteId = userinfo.SiteId;
                newincident.CustomerId = userinfo.CustomerInfoId;
                if (status == CustomEvent.IncidentActionStatus.Park.ToString())
                    newincident.IncidentStatus = (int)CustomEvent.IncidentActionStatus.Park;
                else
                    newincident.IncidentStatus = (int)CustomEvent.IncidentActionStatus.Dispatch;

                newincident.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                newincident.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);

                var retVal = CommonUtility.CreateIncident(newincident);
                if (retVal != "")
                {
                    taskId1 = Convert.ToInt32(retVal);

                    var EventHistoryEntry = new EventHistory();
                    EventHistoryEntry.CreatedDate = dtnow;
                    EventHistoryEntry.CreatedBy = userinfo.Username;
                    EventHistoryEntry.EventId = taskId1;
                    EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                    EventHistoryEntry.UserName = receivedby;
                    EventHistoryEntry.SiteId = userinfo.SiteId;
                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);

                    CustomEvent.CustomEventHandledById(taskId1, false, userinfo.Username, dbConnection);

                    if (newincident.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        var newEventHistoryEntry = new EventHistory();
                        newEventHistoryEntry.CreatedDate = dtnow;
                        newEventHistoryEntry.CreatedBy = userinfo.Username;
                        newEventHistoryEntry.EventId = taskId1;
                        newEventHistoryEntry.IncidentAction = newincident.IncidentStatus;
                        newEventHistoryEntry.UserName = userinfo.Username;
                        newEventHistoryEntry.SiteId = userinfo.SiteId;
                        newEventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                        EventHistory.InsertEventHistory(newEventHistoryEntry, dbConnection, dbConnectionAudit, true);
                    }
                    CustomEvent.UpdateCustomEventDetailsById(taskId1, phoneNo, emailAdd, dbConnection);
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "insertNewIncident", er, dbConnection, userinfo.SiteId);
            }
            return taskId1.ToString();
        }

        //User Profile
        [WebMethod]
        public static string changePW(int id, string password,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                if (userinfo.ID > 0)
                {
                    var getuser = userinfo;
                    getuser.FirstName = firstname;
                    getuser.LastName = lastname;
                    getuser.Email = emailaddress;

                    if (getuser.RoleId != role)
                    {
                        getuser.RoleId = role;
                        if (role == (int)Role.Manager)
                        {
                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                        else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                        }
                        else
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                    }
                    if (getuser.RoleId == (int)Role.Manager)
                    {

                        var dirUser = Users.GetUserById(supervisor, dbConnection);
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        if (dirUser != null)
                        {
                            if (!string.IsNullOrEmpty(dirUser.Username))
                            {
                                List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                           
                                            }};
                                DirectorManager.InsertDirectorManager(userManagerList, dbConnection,dbConnectionAudit,true);
                            }
                        }
                    }
                    else if (getuser.RoleId == (int)Role.Operator)
                    {
                        if (supervisor > 0)
                        {
                            var manUser = Users.GetUserById(supervisor, dbConnection);
                            List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                            UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                    else if (getuser.RoleId == (int)Role.UnassignedOperator)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                    {
                        return userinfo.ID;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "addUserProfile", er, dbConnection, userinfo.SiteId);
            }
            return userinfo.ID;
        }

        [WebMethod]
        public static List<string> getUserProfileData(int id,string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                // var test = Users.GetUserById(id, dbConnection);
                //var customData = Users.GetUserById(id, dbConnection);

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);

                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                 //   var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    if (customData.RoleId != (int)Role.SuperAdmin)
                    {
                      //  if (pushDev != null)
                      //  {
                       //     if (!string.IsNullOrEmpty(pushDev.Username))
                        //    {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                          //  }
                         //   else
                         //   {
                          //      monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                          //      fontstyle = "";
                         //   }
                        //}
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static string getDispatchUserList(int id, string ttype,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        json += "<a href='#' style='color:" + CommonUtility.getHexColor(0) + "' ><i  class='fa fa-check-square-o'></i>" + item.ACustomerUName + "</a>";
                    }
                    else
                    {
                        var colorCount = 0;
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        foreach (var noti in notification)
                        {
                            if (noti.Prefix == "ARL" && noti.IsHandled)
                            {
                                var tbColor = string.Empty;
                                if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                                {

                                }
                                else
                                {
                                    tbColor = CommonUtility.getHexColor(colorCount);
                                    assigneeList.Add(noti.AssigneeId);
                                    colorCount++;
                                    json += "<a href='#' style='color:" + tbColor + ";font-size:16px;' onclick='tracebackOnUser(&apos;" + id + "&apos;,&apos;" + ttype + "&apos;,&apos;" + noti.AssigneeId + "&apos;)'><i id='" + id + noti.AssigneeId + "'  class='fa fa-check-square-o'></i>" + noti.ACustomerUName + "</a>|";

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getDispatchUserList", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getTracebackLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    var geteventhistorys = TaskEventHistory.GetTaskEventHistoryByTaskId(Convert.ToInt32(id), dbConnection);
                    var rejecteds = geteventhistorys.Where(i => i.Action == (int)TaskAction.Rejected).ToList();

                    var gotrejected = false;

                    if (rejecteds.Count > 0)
                    {
                        gotrejected = true;
                    }

                    var inprohistory = new TaskEventHistory();

                    if (!gotrejected)
                    {
                        if (item.StatusDescription == "Pending")
                        {
                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        {
                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    else
                    {
                        if (item.Longitude > 0 && item.Latitude > 0)
                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        foreach (var ev in geteventhistorys)
                        {
                            if (ev.Action == (int)TaskAction.OnRoute)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (inprohistory.Id == 0)
                                {
                                    if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                                    {
                                        inprohistory = ev;
                                    }
                                }
                            }
                            else if (ev.Action == (int)TaskAction.InProgress)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (inprohistory.Id == 0)
                                {
                                    if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                                    {
                                        inprohistory = ev;
                                    }
                                }
                            }
                            else if (ev.Action == (int)TaskAction.Complete)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                {
                                    if (ev.CreatedDate.Value < item.ActualEndDate.Value)
                                        json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"X\",\"Logs\" : \"Retrieve\"},";
                                    else
                                        json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                        }
                    }
                    if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || gotrejected)
                    {
                        if (traceback.Count > 0)
                        {
                            if (gotrejected)
                            {
                                if (inprohistory.Id == 0)
                                {
                                    if (!string.IsNullOrEmpty(inprohistory.Longtitude) && !string.IsNullOrEmpty(inprohistory.Latitude))
                                        json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + inprohistory.Longtitude.ToString() + "\",\"Lat\" : \"" + inprohistory.Latitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                                else
                                {
                                    if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                        json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                            else
                            {
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                            foreach (var tb in traceback)
                            {
                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                if (tb.Longitude > 0 && tb.Latitude > 0)
                                    json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                            if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTracebackLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getTracebackLocationByDurationData(int id, int duration, string ttype,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var assigneeList = new List<int>();
                if (id > 0)
                {
                    json += "[";
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);

                        if (item.StatusDescription == "Pending")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed"  || item.StatusDescription == "Accepted")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }

                        if (item.StatusDescription == "Completed"  || item.StatusDescription == "Accepted")
                        {
                            var curTime = CommonUtility.getDTNow();
                            var firstTime = false;
                            if (traceback.Count > 0)
                            {
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                    if (!firstTime)
                                    {
                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                        firstTime = true;
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";

                                    }
                                    else
                                    {
                                        if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                        {
                                            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                            curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                        }
                                    }
                                }
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    else
                    {
                        var item = CustomEvent.GetCustomEventById(id, dbConnection);

                        if (item.EventType == CustomEvent.EventTypes.Incident)
                        {
                            var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                            if (geofence.Count > 0)
                            {
                                foreach (var geo in geofence)
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                        }
                        else
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        var colorCount = 0;
                        foreach (var noti in notification)
                        {
                            if (noti.Prefix == "ARL" && noti.IsHandled)
                            {
                                var tbColor = string.Empty;
                                if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                                {

                                }
                                else
                                {
                                    tbColor = CommonUtility.getHexColor(colorCount);
                                    assigneeList.Add(noti.AssigneeId);
                                    colorCount++;
                                }
                                var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                                var curTime = CommonUtility.getDTNow();
                                var firstTime = false;
                                var evHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection);
                                var engLongi = string.Empty;
                                var engLati = string.Empty;
                                var engDate = string.Empty;
                                var compLongi = string.Empty;
                                var compLati = string.Empty;
                                var compDate = string.Empty;
                                if (traceback.Count > 0)
                                {
                                    //json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                    var engstart = false;
                                    foreach (var tb in traceback)
                                    {
                                        var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                        if (!engstart)
                                        {
                                            engstart = true;
                                            var engfound = false;
                                            var compfound = false;
                                            foreach (var ev in evHistory)
                                            {
                                                if (ev.UserName == noti.AssigneeName)
                                                {
                                                    if (!engfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                        {
                                                            engLongi = ev.Longtitude;
                                                            engLati = ev.Latitude;
                                                            engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                            engfound = true;

                                                        }
                                                    }
                                                    if (!compfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                        {
                                                            compLongi = ev.Longtitude;
                                                            compLati = ev.Latitude;
                                                            compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            compfound = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (!firstTime)
                                        {
                                            curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                            firstTime = true;
                                            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";

                                        }
                                        else
                                        {
                                            if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                            {
                                                json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                            }
                                        }
                                    }
                                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                    //json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                        }

                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getTracebackLocationByDurationData", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getTracebackLocationDataByUser(int id, int duration, string ttype, int[] userIds,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    json += "[";
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);

                        if (item.StatusDescription == "Pending")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        {
                            //json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";
                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
 
                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }

                        if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            var curTime = CommonUtility.getDTNow();
                            var firstTime = false;
                            if (traceback.Count > 0)
                            {
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);

                                    if (!firstTime)
                                    {
                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                        firstTime = true;
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";

                                    }
                                    else
                                    {
                                        if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                        {
                                            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                            curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone).AddHours(userinfo.TimeZone);
                                        }
                                    }
                                }
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    else
                    {
                        var item = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (item.EventType == CustomEvent.EventTypes.Incident)
                        {
                            var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                            if (geofence.Count > 0)
                            {
                                foreach (var geo in geofence)
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                        }
                        else
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        var colorCount = 0;
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        var notiuserlist = new List<string>();
                        foreach (var noti in notification)
                        {
                            if (noti.Prefix == "ARL" && noti.IsHandled)
                            {
                                if (!userIds.Contains(noti.AssigneeId))//(noti.AssigneeId != userIds[0])
                                {
                                    if (!notiuserlist.Contains(noti.AssigneeName))
                                    {
                                        notiuserlist.Add(noti.AssigneeName);
                                        var tbColor = string.Empty;
                                        if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                                        {

                                        }
                                        else
                                        {
                                            tbColor = CommonUtility.getHexColor(colorCount);
                                            assigneeList.Add(noti.AssigneeId);
                                            colorCount++;
                                        }

                                        var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                                        var curTime = CommonUtility.getDTNow();
                                        var firstTime = false;
                                        var evHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection);
                                        var engLongi = string.Empty;
                                        var engLati = string.Empty;
                                        var engDate = string.Empty;
                                        var compLongi = string.Empty;
                                        var compLati = string.Empty;
                                        var compDate = string.Empty;
                                        if (traceback.Count > 0)
                                        {
                                            //json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                            var engstart = false;

                                            foreach (var tb in traceback)
                                            {
                                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                                if (!engstart)
                                                {
                                                    engstart = true;
                                                    var engfound = false;
                                                    var compfound = false;
                                                    foreach (var ev in evHistory)
                                                    {
                                                        if (ev.UserName == getUser.Username)
                                                        {
                                                            if (!engfound)
                                                            {
                                                                if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                                {
                                                                    engLongi = ev.Longtitude;
                                                                    engLati = ev.Latitude;
                                                                    engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                                    json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                                    json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                                    engfound = true;

                                                                }
                                                            }
                                                            if (!compfound)
                                                            {
                                                                if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                                {
                                                                    compLongi = ev.Longtitude;
                                                                    compLati = ev.Latitude;
                                                                    compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                                    compfound = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (!firstTime)
                                                {
                                                    curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                                    firstTime = true;
                                                    json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";

                                                }
                                                else
                                                {
                                                    if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                                    {
                                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                                    }
                                                }
                                            }
                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                            //json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                        }
                                        else
                                        {
                                            var getEventHistory = EventHistory.GetEventHistoryByEventId(id, dbConnection);
                                            var engfound = false;
                                            var compfound = false;
                                            //var engLongi = string.Empty;
                                            //var engLati = string.Empty;
                                            //var engDate = string.Empty;
                                            //var compLongi = string.Empty;
                                            //var compLati = string.Empty;
                                            //var compDate = string.Empty;
                                            foreach (var ev in getEventHistory)
                                            {
                                                if (ev.UserName == noti.AssigneeName)
                                                {
                                                    if (!engfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                        {
                                                            engLongi = ev.Longtitude;
                                                            engLati = ev.Latitude;
                                                            engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                            engfound = true;

                                                        }
                                                    }
                                                    if (!compfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                        {
                                                            compLongi = ev.Longtitude;
                                                            compLati = ev.Latitude;
                                                            compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                                            compfound = true;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //if (traceback.Count > 0)
                                        //{

                                        //    json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                        //    foreach (var tb in traceback)
                                        //    {
                                        //        var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                        //        if (!firstTime)
                                        //        {
                                        //            curTime = tb.CreatedDate.Value;
                                        //            firstTime = true;
                                        //            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";

                                        //        }
                                        //        else
                                        //        {
                                        //            if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                        //            {
                                        //                json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                        //                curTime = tb.CreatedDate.Value;
                                        //            }
                                        //        }
                                        //    }
                                        //    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                        //}
                                    }
                                }
                            }
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getTracebackLocationDataByUser", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string newIncidentTypeSave(string newtype,string img,string uname)
        {
            var json = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {

                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }
                     
                    var getData = MobileHotEventCategory.GetMobileHotEventCategoryByNameAndCIdAndSiteId(newtype, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = MobileHotEventCategory.GetMobileHotEventCategoryByNameAndCIdAndSiteId(newtype, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }

                    if (getData == null)
                    { 
                        var mobEventcat = new MobileHotEventCategory();
                        mobEventcat.CreatedBy = userinfo.Username;
                        mobEventcat.CreatedDate = CommonUtility.getDTNow();
                        mobEventcat.Description = newtype;
                        mobEventcat.IsActive = true;
                        mobEventcat.UpdatedBy = userinfo.Username;
                        mobEventcat.UpdatedDate = CommonUtility.getDTNow();
                        mobEventcat.SiteId = userinfo.SiteId;
                        mobEventcat.CustomerId = userinfo.CustomerInfoId;
                        mobEventcat.Url = CommonUtility.getMobileHoteventCategoryUrl(img);
                        MobileHotEventCategory.InsertOrUpdateMobileHotEventCategory(mobEventcat, dbConnection, dbConnectionAudit, true);
                        json = "SUCCESS";
                    }
                    else
                    {
                        json = "Name already exists";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Incident.aspx", "newIncidentTypeSave", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }
        [WebMethod]
        public static string editIncidentTypeSave(int id, string type, string img, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }


                    if (id > 0)
                    {
                        var getData = MobileHotEventCategory.GetMobileHotEventCategoryByNameAndCIdAndSiteId(type, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                        if (userinfo.RoleId == (int)Role.Director)
                        {
                            if (getData == null)
                            {
                                getData = MobileHotEventCategory.GetMobileHotEventCategoryByNameAndCIdAndSiteId(type, userinfo.CustomerInfoId, 0, dbConnection);
                            }
                        }

                        if (getData == null)
                        {
                            var mobEventcat = MobileHotEventCategory.GetAllMobileHotEventCategories(dbConnection);
                            mobEventcat = mobEventcat.Where(i => i.CategoryId == id).ToList();
                            foreach (var item in mobEventcat)
                            { 
                                if (item.Description != type)
                                {
                                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Incident", type, item.Description, userinfo, "INCIDENT TYPE NAME-" + id);
                                    item.Description = type;
                                }
                                item.IsActive = true;
                                item.UpdatedBy = userinfo.Username;
                                item.UpdatedDate = CommonUtility.getDTNow();
                                item.Url = CommonUtility.getMobileHoteventCategoryUrl(img);
                                MobileHotEventCategory.InsertOrUpdateMobileHotEventCategory(item, dbConnection, dbConnectionAudit, true);
                                json = "SUCCESS";
                                break;
                            } 
                        }
                        else
                        {
                            var mobEventcat = MobileHotEventCategory.GetAllMobileHotEventCategories(dbConnection);
                            mobEventcat = mobEventcat.Where(i => i.CategoryId == id).ToList();
                            foreach (var item in mobEventcat)
                            {
                                if (item.Description == type)
                                { 
                                    item.IsActive = true;
                                    item.UpdatedBy = userinfo.Username;
                                    item.UpdatedDate = CommonUtility.getDTNow();
                                    item.Url = CommonUtility.getMobileHoteventCategoryUrl(img);
                                    MobileHotEventCategory.InsertOrUpdateMobileHotEventCategory(item, dbConnection, dbConnectionAudit, true);
                                    json = "SUCCESS";
                                }
                                else
                                {
                                    json = "Name already exists";
                                }
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Incident.aspx", "editIncidentTypeSave", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
            }
            return json;
        }
        [WebMethod]
        public static string deleteIncidentTypeSave(int id,string uname)
        {
            var json = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }
                    if (id > 0)
                    {
                        MobileHotEventCategory.DeleteMobileHotEventCategory(id, dbConnection);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Incident", id.ToString(), id.ToString(), userinfo, "Delete MobileHotEventCategory" + id);
                        json = "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Incident.aspx", "deleteIncidentTypeSave", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }
        [WebMethod]
        public static List<string> getLongLatByUserId(string id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (CommonUtility.isNumeric(id))
                {
                    if (Convert.ToInt32(id) > 0)
                    {
                        var uinfo = Users.GetUserById(Convert.ToInt32(id), dbConnection);
                        if(uinfo != null){
                        listy.Add(uinfo.Latitude.ToString());
                        listy.Add(uinfo.Longitude.ToString());
                        listy.Add(uinfo.Username);
                        }
                    }
                }
                else
                {
                    var deviceinfo = Device.GetClientDeviceByPcName(id, dbConnection);
                    if(deviceinfo !=null)
                    {
                        listy.Add(deviceinfo.Latitude.ToString());
                        listy.Add(deviceinfo.Longitude.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident.aspx", "getLongLatByUserId", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string geofenceSuccess(int returnV,string uname)
        {
            var retVal = "FAIL";
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (returnV > 0)
                {
                    //var geofenceCheck = MIMSLog.GetAllMIMSLogsByMacAddress(returnV.ToString(), dbConnection);
                   // if (geofenceCheck.Count > 0)
                    //{
                        return "SUCCESS";
                    //}
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident.aspx", "geofenceSuccess", ex, dbConnection, userinfo.SiteId);
            }
            return retVal;
        }

        [WebMethod]
        public static string deleteIncident(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    CustomEvent.DeleteCustomEventById(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Incident", id.ToString(), id.ToString(), userinfo, "Delete Incident" + id);
                    json = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident.aspx", "deleteIncident", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
            return json;
        }

        [WebMethod]
        public static string checkLiveVideo(string camera,string uname)
        {
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                
                var ipaddress = CommonUtility.milestonemobile;
                var port = CommonUtility.milestoneport;

                if (!string.IsNullOrEmpty(ipaddress) && CommonUtility.isNumeric(port))
                {
                    using (TcpClient tcpClient = new TcpClient(ipaddress, Convert.ToInt32(port)))
                    {
                        var alarm = new ServiceController("Milestone Mobile Service");
                        if (alarm != null)
                        {
                            if (alarm.Status.Equals(ServiceControllerStatus.Running))
                            {
                                retVal = "SUCCESS";
                            }
                            else
                            {
                                retVal = "Sorry,failed to connect to mobile video service.";
                            }
                        }
                        else
                        {
                            retVal = "Sorry,failed to connect to mobile video service.";
                        }
                        tcpClient.GetStream().Close();
                        tcpClient.Close();
                    }
                }
                else
                    retVal = "Problem with milestone settings.";
                return retVal;
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident", "checkLiveVideo", ex, dbConnection, userinfo.SiteId);
                return "Sorry,failed to connect to mobile video service.";
            }
        }
        [WebMethod]
        public static string CreatePDFIncident(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                                                var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var cusevent = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (cusevent != null)
                            return GeneratePDFIncident(cusevent, userinfo);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Incident", "CreatePDFIncident", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }

        [WebMethod]
        public static string escalateIncident(int id,string ins,string uname)
        {
            var json = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        incidentinfo.UpdatedBy = userinfo.Username;
                        incidentinfo.IncidentStatus = (int)CustomEvent.IncidentActionStatus.Escalated;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        var returnV = CommonUtility.CreateIncident(incidentinfo);
                        if (returnV != "")
                        {
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = id;
                            EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                            EventHistoryEntry.UserName = userinfo.Username;
                            EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            CustomEvent.CustomEventHandledById(id, true, userinfo.Username, dbConnection);
                            var escaLevel = 0;
                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                escaLevel = (int)CustomEvent.EscalateTypes.Level2;
                            }
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {
                                escaLevel = (int)CustomEvent.EscalateTypes.Level3;
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                escaLevel = (int)CustomEvent.EscalateTypes.Level4;
                            }
                            CustomEvent.UpdateCustomEventbyEscalate(id, escaLevel, dbConnection);
                            Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(id, true, dbConnection);
                            json = "SUCCESS";
                        }

                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Incident.aspx", "escalateIncident", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }
        

        private static string GeneratePDFIncident(CustomEvent cusevent, Users userinfo)
        {
            string fileName = string.Empty;
            try
            {
                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                var path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };
                doc.Open();
                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                Font rweb = new Font(z_cn, 11, Font.NORMAL, arrowred);

                Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                Font eleventimesI = new Font(f_cn, 11, Font.ITALIC, Color.BLACK);

                Font mbtimes = new Font(h_cn, 18, Font.BOLD, arrowred);
                Font cbtimes = new Font(h_cn, 13, Font.BOLD, arrowred);

                float[] columnWidthsH = new float[] { 25, 60, 15 };

                var mainheadertable = new PdfPTable(3);
                mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainheadertable.WidthPercentage = 100;
                mainheadertable.SetWidths(columnWidthsH);
                var newTRIlist = new List<TimelineReportItems>();

                var headerMain = new PdfPCell();
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    headerMain.AddElement(new Phrase("G4S INCIDENT REPORT", mbtimes));
                }
                else
                {
                    headerMain.AddElement(new Phrase("MIMS INCIDENT REPORT", mbtimes));
                }
                headerMain.PaddingBottom = 10;
                headerMain.HorizontalAlignment = 1;
                headerMain.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell1 = new PdfPCell();
                pdfCell1.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell2 = new PdfPCell();
                pdfCell2.Border = Rectangle.NO_BORDER;
                mainheadertable.AddCell(pdfCell1);
                mainheadertable.AddCell(headerMain);
                mainheadertable.AddCell(pdfCell2);

                var taskdataTable = new PdfPTable(2);
                var taskdataTableA = new PdfPTable(2);
                var taskdataTableB = new PdfPTable(2);
                taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTable.WidthPercentage = 100;

                taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                taskdataTableA.WidthPercentage = 100;
                taskdataTableB.WidthPercentage = 100;

                float[] columnWidths2575 = new float[] { 25, 75 };
                float[] columnWidths3070 = new float[] { 30, 70 };
                float[] columnWidths3565 = new float[] { 35, 65 };
                float[] columnWidths4060 = new float[] { 40, 60 };
                taskdataTableA.SetWidths(columnWidths4060);

                taskdataTableB.SetWidths(columnWidths4060);

                //SIDE A
                var header1 = new PdfPCell();
                header1.AddElement(new Phrase("Incident Name:", beleventimes));
                header1.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1);

                var header1a = new PdfPCell();
                header1a.AddElement(new Phrase(cusevent.Name, eleventimes));
                header1a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1a);

                var header2 = new PdfPCell();
                header2.AddElement(new Phrase("Incident Type:", beleventimes));
                header2.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2);

                var header2a = new PdfPCell();
                header2a.AddElement(new Phrase(cusevent.Event, eleventimes));
                header2a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2a);

                var header3 = new PdfPCell();
                header3.AddElement(new Phrase("Created Date:", beleventimes));
                header3.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3);

                var header3a = new PdfPCell();
                header3a.AddElement(new Phrase(cusevent.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), eleventimes));
                header3a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3a);

                var header4 = new PdfPCell();
                header4.AddElement(new Phrase("Created by:", beleventimes));
                header4.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4);

                var header4a = new PdfPCell();
                header4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                header4a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4a);

                var header5 = new PdfPCell();
                header5.AddElement(new Phrase("Received by:", beleventimes));
                header5.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header5);

                if (string.IsNullOrEmpty(cusevent.ReceivedBy))
                {
                    cusevent.ReceivedBy = cusevent.CustomerFullName;
                } 

                var header5a = new PdfPCell();
                header5a.AddElement(new Phrase(cusevent.ReceivedBy, eleventimes));
                header5a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header5a);

                if (!string.IsNullOrEmpty(cusevent.UserName))
                {
                    var getUserInfo = Users.GetUserByName(cusevent.UserName, dbConnection);
                    if (getUserInfo != null)
                    {
                        if (string.IsNullOrEmpty(cusevent.EmailAddress))
                            cusevent.EmailAddress = getUserInfo.Email;
                        if (string.IsNullOrEmpty(cusevent.PhoneNumber))
                            cusevent.PhoneNumber = getUserInfo.Telephone;
                    }
                }

                var header6 = new PdfPCell();
                header6.AddElement(new Phrase("Contact Number:", beleventimes));
                header6.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header6);

                var header6a = new PdfPCell();
                header6a.AddElement(new Phrase(cusevent.PhoneNumber, eleventimes));
                header6a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header6a);

                var header7 = new PdfPCell();
                header7.AddElement(new Phrase("Contact Email:", beleventimes));
                header7.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header7);

                var header7a = new PdfPCell();
                header7a.AddElement(new Phrase(cusevent.EmailAddress, eleventimes));
                header7a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header7a);

                var header8 = new PdfPCell();
                header8.AddElement(new Phrase("Description:", beleventimes));
                header8.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8);

                if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    cusevent.Description = cusevent.MobDescription;

                var header8a = new PdfPCell();
                header8a.AddElement(new Phrase(cusevent.Description, eleventimes));
                header8a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8a);

                var header9 = new PdfPCell();
                header9.AddElement(new Phrase("Instructions:", beleventimes));
                header9.Border = Rectangle.NO_BORDER;
                header9.PaddingBottom = 20;
                taskdataTableA.AddCell(header9);

                if (!string.IsNullOrEmpty(cusevent.Instructions))
                {
                    var splitIns = cusevent.Instructions.Split('|');
                    if (splitIns.Length > 0)
                        cusevent.Instructions = splitIns[0];
                }

                var header9a = new PdfPCell();
                header9a.AddElement(new Phrase(cusevent.Instructions, eleventimes));
                header9a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header9a);

                //SIDE B

                var headerB2 = new PdfPCell();
                headerB2.AddElement(new Phrase("Incident Status:", beleventimes));
                headerB2.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2);

                var headerB2a = new PdfPCell();
                headerB2a.AddElement(new Phrase(cusevent.StatusName, eleventimes));
                headerB2a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2a);

                var headerB1 = new PdfPCell();
                headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                headerB1.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB1);


                var siteinfo = Arrowlabs.Business.Layer.Site.GetSiteById(cusevent.SiteId, dbConnection);
                if (siteinfo != null)
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase(siteinfo.Name, eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }
                else
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase("N/A", eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }


                var headerB3 = new PdfPCell();
                headerB3.AddElement(new Phrase("Dispatched By:", beleventimes));
                headerB3.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB3);

                var headerB3a = new PdfPCell();
                headerB3a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                headerB3a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB3a);


                if (cusevent.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                {
                    var headerB4 = new PdfPCell();
                    headerB4.AddElement(new Phrase("Resolved By:", beleventimes));
                    headerB4.Border = Rectangle.NO_BORDER;
                    headerB4.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB4);

                    var headerB4a = new PdfPCell();
                    headerB4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                    headerB4a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB4a);

                    var headerB5 = new PdfPCell();
                    headerB5.AddElement(new Phrase("Resolved Notes:", beleventimes));
                    headerB5.Border = Rectangle.NO_BORDER;
                    headerB5.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB5);

                    var evsHistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                    var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve);
                    if (isEsca != null)
                    {
                        foreach (var esca in isEsca)
                        {
                            var headerB5a = new PdfPCell();
                            headerB5a.AddElement(new Phrase(esca.Remarks, eleventimes));
                            headerB5a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB5a);
                            break;
                        }
                    }
                    else
                    {
                        var headerB5a = new PdfPCell();
                        headerB5a.AddElement(new Phrase("N/A", eleventimes));
                        headerB5a.Border = Rectangle.NO_BORDER;
                        taskdataTableB.AddCell(headerB5a);
                    }
                }

                taskdataTable.AddCell(taskdataTableA);
                taskdataTable.AddCell(taskdataTableB);

                var maptextdataTable = new PdfPTable(1);
                maptextdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                maptextdataTable.WidthPercentage = 100;

                var geolocation = ReverseGeocode.RetrieveFormatedAddress(cusevent.Latitude.ToString(), cusevent.Longtitude.ToString(), getClientLic);
                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, cusevent.EventId);
                if (string.IsNullOrEmpty(geolocation))
                {
                    if (geofence.Count > 0)
                    {
                        geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        cusevent.Longtitude = geofence[0].Longitude.ToString();
                        cusevent.Latitude = geofence[0].Latitude.ToString();

                    }
                }

                if (string.IsNullOrEmpty(geolocation))
                    geolocation = "Not Specified";

                var mapheader = new PdfPCell();
                mapheader.AddElement(new Phrase("Incident Location:" + geolocation, eleventimes));
                mapheader.Border = Rectangle.NO_BORDER;
                maptextdataTable.AddCell(mapheader);

                var mapdataTable = new PdfPTable(2);
                mapdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                mapdataTable.WidthPercentage = 100;
                float[] columnWidths7030 = new float[] { 70, 30 };
                mapdataTable.SetWidths(columnWidths7030);
                //MAP

                //outer.AddCell(table);
                var latComp = string.Empty;
                var longComp = string.Empty;
                var latEng = string.Empty;
                var longEng = string.Empty;
                var arrived = string.Empty;
                var vargeopath = string.Empty; //&path=color:0xff0000ff|weight:5|25.09773,55.16316|25.0978512224338,55.1634863298386
                var tracebackpath = string.Empty; //&path=color:blue|weight:3|25.09773,55.16316|25.0978512224338,55.1634863298386


                var incidenteventhistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;

                foreach (var rem in incidenteventhistory)
                {
                    var timelineItem = new TimelineReportItems();

                    if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        cusevent.Name = cusevent.EventTypeName;
                    }
                    else if (cusevent.EventType == CustomEvent.EventTypes.Request)
                    {
                        var splitName = cusevent.RequestName.Split('^');
                        if (splitName.Length > 1)
                        {
                            cusevent.Name = splitName[1];
                        }
                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerFullName;
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "engaged ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";
                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    newTRIlist.Add(timelineItem);
                }


                if (geofence.Count > 0)
                {
                    vargeopath = "&path=color:0xff0000ff|weight:5|";
                    foreach (var geo in geofence)
                    {
                        vargeopath += geo.Latitude + "," + geo.Longitude + "|";
                    }
                    vargeopath = vargeopath.Substring(0, vargeopath.Length - 1);
                }
                var assigneeList = new List<int>();
                var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(cusevent.EventId, dbConnection);

                var colorCount = 0;
                var legendTraceback = new List<string>();
                foreach (var noti in notification)
                {
                    var tbColor = string.Empty;
                    if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                    {

                    }
                    else
                    {
                        tbColor = CommonUtility.getColorName(colorCount);
                        assigneeList.Add(noti.AssigneeId);
                        legendTraceback.Add(tbColor + " Path:  " + noti.ACustomerFullName + " route");
                        colorCount++;
                    }
                    var tracebackhistory = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);
                    var tbCount = 0;
                    var tbRemovedPointsCount = 0;

                    var tbContinueCountStart = 0;
                    if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                    {
                        tbRemovedPointsCount = tracebackhistory.Count - CommonUtility.tracebackreportmaxcount;
                        tbContinueCountStart = (CommonUtility.tracebackreportmaxcount / 2) + tbRemovedPointsCount;
                    }
                    if (tracebackhistory.Count > 0)
                    {
                        tracebackpath += "&path=color:blue|weight:5|";
                        tracebackpath += latEng + "," + longEng + "|";
                        foreach (var trace in tracebackhistory)
                        {
                            if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                            {
                                tbCount++;
                                if (tbCount < (CommonUtility.tracebackreportmaxcount / 2))
                                    tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                                else if (tbCount > tbContinueCountStart)
                                    tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                            }
                            else
                                tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                        }
                        tracebackpath += latComp + "," + longComp;
                    }
                }
                var maplat = cusevent.Latitude;
                var maplong = cusevent.Longtitude;
                var taskLocation = string.Empty;
                if (cusevent.Latitude != "0" && cusevent.Longtitude != "0")
                {
                    taskLocation = "&markers=color:red%7Clabel:T%7C" + cusevent.Latitude + "," + cusevent.Longtitude;
                }
                else
                {
                    maplat = latEng;
                    maplong = longEng;
                }
                //Map style roadMap
                String mapURL = "https://maps.googleapis.com/maps/api/staticmap?" +
"center=" + cusevent.Latitude + "," + cusevent.Longtitude + "&" +
"size=700x360" + tracebackpath + vargeopath + arrived + "&markers=color:green%7Clabel:C%7C" + latComp + "," + longComp + "&markers=color:yellow%7Clabel:E%7C" + latEng + "," + longEng + taskLocation + "&zoom=17" + "&maptype=png" + "&sensor=true";
                iTextSharp.text.Image LocationImage = iTextSharp.text.Image.GetInstance(mapURL);
                LocationImage.ScaleToFit(380, 320);
                LocationImage.Alignment = iTextSharp.text.Image.UNDERLYING;
                LocationImage.SetAbsolutePosition(0, 0);
                PdfPCell locationcell = new PdfPCell(LocationImage);
                locationcell.PaddingTop = 5;
                locationcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right 
                locationcell.Border = 0;
                mapdataTable.AddCell(locationcell);


                PdfPTable legendtable = new PdfPTable(1);
                legendtable.DefaultCell.Border = Rectangle.NO_BORDER;
                legendtable.HorizontalAlignment = 0;
                Font redfont = new Font();
                redfont.Size = 11;
                Font yellowfont = new Font();
                yellowfont.Size = 11;
                Font greenfont = new Font();
                greenfont.Size = 11;
                Font bluefont = new Font();
                bluefont.Size = 11;
                redfont.Color = Color.RED;
                legendtable.AddCell(new Phrase("Legend:", ueleventimes));
                legendtable.AddCell(new Phrase("I:  Incident Location", redfont));
                yellowfont.Color = new Color(204, 204, 0);
                legendtable.AddCell(new Phrase("E:  Engaged Location", yellowfont));
                bluefont.Color = iTextSharp.text.Color.BLUE;
                legendtable.AddCell(new Phrase("A:  Arrived Location", bluefont));
                greenfont.Color = new Color(0, 204, 0);
                legendtable.AddCell(new Phrase("C:  Completed Location", greenfont));
                var pathCell = new PdfPCell(new Phrase("Red Path:  Geofence", eleventimes));
                pathCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                legendtable.AddCell(pathCell);
                foreach (var legendtrace in legendTraceback)
                {
                    var traceCell = new PdfPCell(new Phrase(legendtrace, eleventimes));
                    traceCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    legendtable.AddCell(traceCell);
                }
                mapdataTable.AddCell(legendtable);

                iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                paragraphTable.SpacingBefore = 120f;
                Paragraph p = new Paragraph();
                p.IndentationLeft = 10;

                p.Add(mainheadertable);
                p.Add(taskdataTable);
                p.Add(maptextdataTable);
                p.Add(mapdataTable);

                PdfPTable table = new PdfPTable(1);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                //Map style roadMap

                doc.Add(paragraphTable);

                doc.Add(p);

                //Attachments

                var listycanvasnotes = new List<string>();

                var attachments = new List<MobileHotEventAttachment>();


                if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusevent.Identifier, dbConnection);
                    if (mobEv != null)
                        attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection));


                }

                attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusevent.EventId, dbConnection));

                var videoCount = 0;
                var imgCount = 0;
                var pdfCount = 0;

                PdfPTable taskimagetable = new PdfPTable(2);
                taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskimagetable.WidthPercentage = 100;

                var atheader1 = new PdfPCell();
                atheader1.AddElement(new Phrase("Attachments", cbtimes));
                atheader1.Border = Rectangle.NO_BORDER;
                atheader1.PaddingTop = 10;
                atheader1.PaddingBottom = 10;
                atheader1.Colspan = 2;
                taskimagetable.AddCell(atheader1);
                taskimagetable.KeepTogether = true;
                if (attachments.Count > 0)
                {
                    var attachcount = 0;
                    var canvasattachcount = 0;
                    var attachlist = new List<MobileHotEventAttachment>();
                    var videosList = new List<MobileHotEventAttachment>();

                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                            {
                                videoCount++;
                            }
                            else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".MP3")
                            {
                                videoCount++;
                            }
                            else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                            {
                                pdfCount++;
                            }
                            else
                            {
                                attachlist.Add(item);
                                attachcount++;
                                imgCount++;
                                listycanvasnotes.Add("Image " + attachcount);
                            }
                        }
                    }
                    taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                    var countz = 0;
                    foreach (var item in attachlist)
                    {
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                        {

                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".MP3")
                        {

                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                        {

                        }
                        else
                        {
                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.AttachmentPath);

                            if (image.Height > image.Width)
                            {
                                //Maximum height is 800 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Height;
                                image.ScalePercent(percentage * 100);
                            }
                            else
                            {
                                //Maximum width is 600 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Width;
                                image.ScalePercent(percentage * 100);
                            }

                            //image.ScaleAbsolute(120f, 155.25f);
                            image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                            if (!string.IsNullOrEmpty(listycanvasnotes[countz]))
                            {
                                Paragraph pcell = new Paragraph(listycanvasnotes[countz].ToUpper());
                                pcell.Alignment = Element.ALIGN_LEFT;
                                pcell.SpacingAfter = 10;
                                imgCell1.AddElement(pcell);
                            }
                            imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                            imgCell1.AddElement(new Chunk(image, 0, 0));
                            imgCell1.PaddingTop = 10;
                            taskimagetable.AddCell(imgCell1);
                            countz++;
                        }
                    }
                    taskimagetable.CompleteRow();
                }

                var extraattachment = new PdfPTable(3);
                extraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                extraattachment.WidthPercentage = 100;
                var phrase2 = new Phrase();
                phrase2.Add(
                    new Chunk("PDF Attached:  ", btimes)
                );
                phrase2.Add(new Chunk(pdfCount.ToString(), times));

                var zheaderA3a = new PdfPCell();
                zheaderA3a.AddElement(phrase2);
                zheaderA3a.Border = Rectangle.NO_BORDER;
                zheaderA3a.PaddingTop = 20;
                zheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(zheaderA3a);

                var phrase32 = new Phrase();
                phrase32.Add(
                    new Chunk("Media Attached:  ", btimes)
                );
                phrase32.Add(new Chunk(videoCount.ToString(), times));

                var aheaderA3a = new PdfPCell();
                aheaderA3a.AddElement(phrase32);
                aheaderA3a.Border = Rectangle.NO_BORDER;
                aheaderA3a.PaddingTop = 20;
                aheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aheaderA3a);

                var phrase323 = new Phrase();
                phrase323.Add(
                    new Chunk("Images Attached:  ", btimes)
                );
                phrase323.Add(new Chunk(imgCount.ToString(), times));

                var aaheaderA3a = new PdfPCell();
                aaheaderA3a.AddElement(phrase323);
                aaheaderA3a.Border = Rectangle.NO_BORDER;
                aaheaderA3a.PaddingTop = 20;
                aaheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aaheaderA3a);

                doc.NewPage();

                doc.Add(taskimagetable);

                doc.Add(extraattachment);


                PdfPTable timetable = new PdfPTable(2);
                timetable.WidthPercentage = 100;
                timetable.DefaultCell.Border = Rectangle.NO_BORDER;
                timetable.SetWidths(columnWidths2575);

                if (newTRIlist.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Incident Timeline", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    timetable.AddCell(headeventCell);

                    foreach (var lit in newTRIlist)
                    {
                        var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(eventCell);
                    }
                }
                doc.Add(timetable);

                //TASKS LOOP

                var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    int count = 1;
                     
                    foreach (var task in utask)
                    {
                        var tskheadertable = new PdfPTable(2);
                        tskheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskheadertable.WidthPercentage = 100;
                        float[] columnWidths2080 = new float[] { 20, 80 };
                        tskheadertable.SetWidths(columnWidths2080);

                        var tskheader0 = new PdfPCell();
                        //tskheader0.AddElement(new Phrase("Task #:" + count, cbtimes));

                        tskheader0.AddElement(new Phrase("Attached Task", cbtimes));
                        tskheader0.Border = Rectangle.NO_BORDER;
                        tskheader0.Colspan = 2;
                        tskheadertable.AddCell(tskheader0);
  
                        var tskheader1 = new PdfPCell();
                        tskheader1.AddElement(new Phrase("Task Name:", beleventimes));
                        tskheader1.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1);

                        var tskheader1a = new PdfPCell();
                        tskheader1a.AddElement(new Phrase(task.Name, eleventimes));
                        tskheader1a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1a);

                        var header1z = new PdfPCell();
                        header1z.AddElement(new Phrase("Task Number:", beleventimes));
                        header1z.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1z);

                        var header1az = new PdfPCell();
                        header1az.AddElement(new Phrase(task.NewCustomerTaskId, eleventimes));
                        header1az.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1az);

                        var tskheader2 = new PdfPCell();
                        tskheader2.AddElement(new Phrase("Task Description:", beleventimes));
                        tskheader2.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2);

                        var tskheader2a = new PdfPCell();
                        tskheader2a.AddElement(new Phrase(task.Description, eleventimes));
                        tskheader2a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2a);

                        var chklistName = "None";
                        var newTCLlist = new List<TaskChecklistReportItems>();
                        var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(task.TemplateCheckListId.ToString(), dbConnection);
                        if (parentTasks != null)
                        {
                            chklistName = parentTasks.Name;

                            var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(task.Id, dbConnection);

                            foreach (var item in sessions)
                            {
                                var newTCL = new TaskChecklistReportItems();
                                newTCL.isParent = true;

                                if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, eleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                else
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, beleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                newTCL.Notes = item.TemplateCheckListItemNote;

                                var myattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(item.Id, dbConnection);
                                newTCL.Attachments = myattachments;


                                var stringCheck = string.Empty;
                                var itemNotes = string.Empty;

                                if (item.IsChecked)
                                {
                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("3", gweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;
                                }
                                else
                                {
                                    List myList = new ZapfDingbatsList(54);

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("6", rweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;

                                    //if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                    //    chkCounter++;
                                }

                                newTCLlist.Add(newTCL);

                                if (item.ChildCheckList != null)
                                {
                                    if (item.ChildCheckList.Count > 0)
                                    {
                                        foreach (var child in item.ChildCheckList)
                                        {

                                            var cnewTCL = new TaskChecklistReportItems();
                                            cnewTCL.isParent = false;

                                            var ncaheaderA = new PdfPCell();
                                            ncaheaderA.AddElement(new Phrase(child.Name, eleventimesI));
                                            ncaheaderA.Border = Rectangle.NO_BORDER;
                                            cnewTCL.itemName = ncaheaderA;

                                            cnewTCL.Notes = child.TemplateCheckListItemNote;

                                            var mycattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(child.Id, dbConnection);
                                            cnewTCL.Attachments = mycattachments;

                                            if (item.IsChecked)
                                            {
                                                //gweb rweb
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("3", gweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                            }
                                            else
                                            {
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("6", rweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                                //chkCounter++;
                                            }

                                            newTCLlist.Add(cnewTCL);
                                        }
                                    }
                                }
                            }
                        }
                        var tskheader3 = new PdfPCell();
                        tskheader3.AddElement(new Phrase("Checklist Name:", beleventimes));
                        tskheader3.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3);

                        var tskheader3a = new PdfPCell();
                        tskheader3a.AddElement(new Phrase(chklistName, eleventimes));
                        tskheader3a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3a);
                         
                        doc.Add(tskheadertable);

                        var newchecklistTable = new PdfPTable(4);
                        newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        newchecklistTable.WidthPercentage = 100;
                        float[] columnWidthsCHK = new float[] { 30, 10, 30, 30 };
                        newchecklistTable.SetWidths(columnWidthsCHK);

                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("Checklist", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.PaddingTop = 10;
                        cheader1.PaddingBottom = 10;
                        cheader1.Colspan = 4;
                        newchecklistTable.AddCell(cheader1);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Item Name", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.PaddingLeft = 5;
                        ncaheader1.PaddingBottom = 5;
                        ncaheader1.BackgroundColor = Color.DARK_GRAY;

                        newchecklistTable.AddCell(ncaheader1);

                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Status", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Notes", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("Attachments", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader4);

                        foreach (var item in newTCLlist)
                        {
                            newchecklistTable.AddCell(item.itemName);

                            newchecklistTable.AddCell(item.Status);

                            var ncaheaderAB = new PdfPCell();
                            ncaheaderAB.AddElement(new Phrase(item.Notes, eleventimes));
                            ncaheaderAB.Border = Rectangle.NO_BORDER;
                            newchecklistTable.AddCell(ncaheaderAB);

                            if (item.Attachments.Count > 0)
                            {
                                PdfPTable imgTable = new PdfPTable(1);
                                imgTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                foreach (var attach in item.Attachments)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant()))
                                    {
                                        imgTable.AddCell(new Phrase("Video Attached", eleventimes));
                                    }
                                    else if (System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant() == ".MP3")
                                    {
                                        imgTable.AddCell(new Phrase("Audio Attached", eleventimes));
                                    }
                                    else if (System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        imgTable.AddCell(new Phrase("PDF Attached", eleventimes));
                                    }
                                    else
                                    {
                                        //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + task.Id + "/{0}", System.IO.Path.GetFileName(attach.DocumentPath));

                                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(attach.DocumentPath);

                                        if (image.Height > image.Width)
                                        {
                                            //Maximum height is 800 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Height;
                                            image.ScalePercent(percentage * 100);
                                        }
                                        else
                                        {
                                            //Maximum width is 600 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Width;
                                            image.ScalePercent(percentage * 100);
                                        }

                                        //image.ScaleAbsolute(120f, 155.25f);
                                        image.Border = Rectangle.NO_BORDER;
                                        iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1l.Border = Rectangle.NO_BORDER;
                                        imgCell1l.AddElement(new Chunk(image, 0, 0));
                                        imgTable.AddCell(imgCell1l);
                                        if (!string.IsNullOrEmpty(attach.ImageNote) && attach.ImageNote != "Task Attachment")
                                            imgTable.AddCell(new Phrase("Img Note:" + attach.ImageNote, eleventimes));
                                    }
                                }
                                newchecklistTable.AddCell(imgTable);
                            }
                            else
                            {
                                newchecklistTable.AddCell(new Phrase(" ", eleventimes));
                            }
                        }

                        doc.Add(newchecklistTable);

                        var tsklistycanvasnotes = new List<string>();
                        var tskattachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(task.Id, dbConnection);
                        var tskvideoCount = 0;
                        var tskimgCount = 0;
                        var tskpdfCount = 0;

                        PdfPTable tsktaskimagetable = new PdfPTable(2);
                        tsktaskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktaskimagetable.WidthPercentage = 100;

                        var tskatheader1 = new PdfPCell();
                        tskatheader1.AddElement(new Phrase("Task Attachments", cbtimes));
                        tskatheader1.Border = Rectangle.NO_BORDER;
                        tskatheader1.PaddingTop = 10;
                        tskatheader1.PaddingBottom = 10;
                        tskatheader1.Colspan = 2;
                        tsktaskimagetable.AddCell(tskatheader1);

                        if (tskattachments.Count > 0)
                        { 
                            var attachcount = 0;
                            var canvasattachcount = 0;
                            var attachlist = new List<UserTaskAttachment>();

                            foreach (var item in tskattachments)
                            {
                                if (!string.IsNullOrEmpty(item.DocumentPath) && item.ChecklistId == 0)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        tskpdfCount++;
                                    }
                                    else
                                    {
                                        attachlist.Add(item);
                                        attachcount++;
                                        tskimgCount++;
                                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                                        {
                                            tsklistycanvasnotes.Add("Canvas Notes Image " + attachcount + ":" + item.ImageNote);
                                            canvasattachcount++;
                                        }
                                        else
                                        {
                                            tsklistycanvasnotes.Add("Image " + attachcount);
                                            canvasattachcount++;
                                        }
                                    }
                                }
                            }
          
                            var countz = 0;
                            foreach (var item in attachlist)
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {

                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                {
                                   
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.DocumentPath);

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    if (!string.IsNullOrEmpty(tsklistycanvasnotes[countz]))
                                    {
                                        Paragraph pcell = new Paragraph(tsklistycanvasnotes[countz].ToUpper());
                                        pcell.Alignment = Element.ALIGN_LEFT;
                                        pcell.SpacingAfter = 10;
                                        imgCell1.AddElement(pcell);
                                    }
                                    imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                    imgCell1.AddElement(new Chunk(image, 0, 0));
                                    imgCell1.PaddingTop = 10;
                                    tsktaskimagetable.AddCell(imgCell1);
                                    countz++;
                                }
                            }
                            tsktaskimagetable.CompleteRow();
                        }

                        var tskextraattachment = new PdfPTable(3);
                        tskextraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskextraattachment.WidthPercentage = 100;
                        var tskphrase2 = new Phrase();
                        tskphrase2.Add(
                            new Chunk("PDF Attached:  ", btimes)
                        );
                        tskphrase2.Add(new Chunk(tskpdfCount.ToString(), times));

                        var tskzheaderA3a = new PdfPCell();
                        tskzheaderA3a.AddElement(tskphrase2);
                        tskzheaderA3a.Border = Rectangle.NO_BORDER;
                        tskzheaderA3a.PaddingTop = 20;
                        tskzheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskzheaderA3a);

                        var tskphrase32 = new Phrase();
                        tskphrase32.Add(
                            new Chunk("Media Attached:  ", btimes)
                        );
                        tskphrase32.Add(new Chunk(tskvideoCount.ToString(), times));

                        var tskaheaderA3a = new PdfPCell();
                        tskaheaderA3a.AddElement(tskphrase32);
                        tskaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaheaderA3a.PaddingTop = 20;
                        tskaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaheaderA3a);

                        var tskphrase323 = new Phrase();
                        tskphrase323.Add(
                            new Chunk("Images Attached:  ", btimes)
                        );
                        tskphrase323.Add(new Chunk(tskimgCount.ToString(), times));

                        var tskaaheaderA3a = new PdfPCell();
                        tskaaheaderA3a.AddElement(tskphrase323);
                        tskaaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaaheaderA3a.PaddingTop = 20;
                        tskaaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaaheaderA3a);

                        doc.Add(tsktaskimagetable);

                        doc.Add(tskextraattachment);

                        var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(task.Id, dbConnection);

                        taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();

                        var tsknewTRIlist = new List<TimelineReportItems>();

                        foreach (var taskEv in taskeventhistory)
                        {
                            var timelineItem = new TimelineReportItems();
                            var taskn = "Task";
                            if (taskEv.isSubTask)
                            {
                                taskn = "Sub-task";
                            }

                            if (taskEv.Action == (int)TaskAction.Complete)
                            {
                                completedBy = taskEv.CustomerFullName;
                                var compLocation = string.Empty;
                                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(task.EndLatitude.ToString(), task.EndLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc))
                                    compLocation = " at " + geoLoc;
                                var compInfo = "completed";
                                latComp = task.EndLatitude.ToString();
                                longComp = task.EndLongitude.ToString();
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                                var compstring = completedBy + " " + compInfo + " " + taskn + " " + compLocation;

                                timelineItem.TimelineActivity = compstring;
                                //listy.Add(compstring);
                            }
                            else if (taskEv.Action == (int)TaskAction.InProgress)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "started";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.OnRoute)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "onroute";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Pause)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "paused";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Resume)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "resumed";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Accepted)
                            {
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                acceptedData = taskEv.CustomerFullName + " accepted " + taskn;
                                //listy.Add(acceptedData);
                                timelineItem.TimelineActivity = acceptedData;
                            }
                            else if (taskEv.Action == (int)TaskAction.Rejected)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                rejectedData = taskEv.CustomerFullName + " rejected Task " + taskn;

                                timelineItem.TimelineActivity = rejectedData;
                                //listy.Add(rejectedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Assigned)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                assignedData = taskEv.CustomerFullName + " assigned " + taskn + " to " + taskEv.ACustomerFullName;

                                timelineItem.TimelineActivity = assignedData;

                                //listy.Add(assignedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Pending)
                            {
                                var incidentLocation = string.Empty;
                                var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(task.Latitude.ToString(), task.Longitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc3))
                                    incidentLocation = " at " + geoLoc3;
                                var actioninfo = "created";
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                actioninfo = task.CustomerFullName + " " + actioninfo + " " + taskn + " for " + taskEv.ACustomerFullName + " " + incidentLocation;
                                timelineItem.TimelineActivity = actioninfo;
                                //listy.Add(actioninfo);
                            }
                            tsknewTRIlist.Add(timelineItem);
                        }

                        PdfPTable tsktimetable = new PdfPTable(2);
                        tsktimetable.WidthPercentage = 100;
                        tsktimetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktimetable.SetWidths(columnWidths2575);

                        if (tsknewTRIlist.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Task Timeline", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            tsktimetable.AddCell(headeventCell);

                            foreach (var lit in tsknewTRIlist)
                            {
                                var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(eventCell);
                            }
                        }

                        doc.Add(tsktimetable);


                        PdfPTable notestable = new PdfPTable(2);
                        notestable.WidthPercentage = 100;
                        notestable.DefaultCell.Border = Rectangle.NO_BORDER;
                        notestable.SetWidths(columnWidths2575);

                        var tremarks = TaskRemarks.GetTaskRemarksByTaskId(task.Id, dbConnection);

                        if (tremarks.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Notes", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            notestable.AddCell(headeventCell);
                            foreach (var remark in tremarks)
                            {
                                var dateCell = new PdfPCell(new Phrase(remark.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString(), redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                notestable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(remark.FName + " " + remark.LName + ":" + remark.Remarks, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                notestable.AddCell(eventCell);
                            }
                        }

                        doc.Add(notestable);
                    }
                }
                //END OF TASKS



                Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                var endTable = new PdfPTable(1);
                endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                endTable.WidthPercentage = 100;

                var endheader = new PdfPCell();
                endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                endheader.Border = Rectangle.NO_BORDER;
                endTable.AddCell(endheader);
                doc.Add(endTable);

                writer.Flush();
                doc.Close();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                        return vpath;
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Incident", "GeneratePDFIncident", exp, dbConnection, userinfo.SiteId);
                return "Failed to generate report!";
            }
            return "Failed to generate report!";
        }
         
        [WebMethod]
        public static string UpdateCustomEventView(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }
                    if (id > 0)
                    {
                        CustomEvent.UpdateCustomEventView(id, dbConnection);
                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Default", "UpdateCustomEventView", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }

        [WebMethod]
        public static string attachFileToTask(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new MobileHotEventAttachment();

                    var newimgPath = filepath.Replace('|', '\\');
                    //var mimsconfigsettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    //var savestring = mimsconfigsettings.FilePath.Remove(mimsconfigsettings.FilePath.Length - 5);
                    //savestring = savestring + "HotEvents";
                    //if (!System.IO.Directory.Exists(savestring))
                    //    System.IO.Directory.CreateDirectory(savestring);

                    //if (System.IO.Path.GetExtension(newimgPath).ToUpperInvariant() == ".PDF")
                    //{
                    //    var bytes = File.ReadAllBytes(newimgPath);
                    //    var Tsavestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + ".pdf";
                    //    if (!System.IO.File.Exists(Tsavestring))
                    //    {
                    //        savestring = Tsavestring;
                    //        File.WriteAllBytes(savestring, bytes);
                    //    }
                    //    else
                    //    {
                    //        savestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + "(1).pdf";
                    //        File.WriteAllBytes(savestring, bytes);
                    //    }
                    //}
                    //else
                    //{
                    //    var bmap = new System.Drawing.Bitmap(newimgPath);
                    //    var Tsavestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + ".jpg";
                    //    if (!System.IO.File.Exists(Tsavestring))
                    //    {
                    //        savestring = Tsavestring;
                    //        bmap.Save(savestring);
                    //    }
                    //    else
                    //    {
                    //        savestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + "(1).jpg";
                    //        bmap.Save(savestring);
                    //    }
                    //}


                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);
                            newObj.AttachmentName = System.IO.Path.GetFileName(savestring);
                            newObj.AttachmentPath = savestring;
                            //newObj.SiteId = cusEv.SiteId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            newObj.CreatedBy = userinfo.Username;
                            newObj.CreatedDate = CommonUtility.getDTNow();
                            newObj.EventId = id;
                            newObj.IsPortal = true;
                            newObj.SiteId = userinfo.SiteId;
                            newObj.CustomerId = userinfo.CustomerInfoId;
                            MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newObj, dbConnection, dbConnectionAudit, true);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Incident", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Incident", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToTask", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static string deleteAttachmentData(int id, int incidentid, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }
                    var ret = MobileHotEventAttachment.GetMobileHotEventAttachmentById(id, dbConnection);
                    if (ret != null)
                    {
                        MobileHotEventAttachment.DeleteMobileHotEventAttachmentById(id, dbConnection);

                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "UserTaskAttachment", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());

                        CommonUtility.CloudDeleteFile(ret[0].AttachmentPath);

                        listy = "Successfully deleted entry";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Tasks", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string uploadMobileAttachment(int id, string imgpath, string uname)
        {
            var retid = false;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (!string.IsNullOrEmpty(imgpath))
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var newimgPath = imgpath.Replace('|', '\\');
                    //var bmap = new System.Drawing.Bitmap(newimgPath);
                    var newmobattach = new MobileHotEventAttachment();
                    //var mimsconfigsettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    //var savestring = mimsconfigsettings.FilePath.Remove(mimsconfigsettings.FilePath.Length - 5);
                    //savestring = savestring + "HotEvents\\" + Guid.NewGuid();
                    //if (!System.IO.Directory.Exists(savestring))
                    //    System.IO.Directory.CreateDirectory(savestring);
                    //savestring = savestring + "\\" + Guid.NewGuid() + ".jpg";
                    //newmobattach.Attachment = CommonUtility.ImageToByte2(bmap);

                    //System.IO.MemoryStream data = new System.IO.MemoryStream();
                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);
                            
                            newmobattach.AttachmentPath = savestring;//string.Empty;//savestring;
                            newmobattach.CreatedBy = userinfo.Username;
                            newmobattach.CreatedDate = CommonUtility.getDTNow();
                            newmobattach.EventId = id;
                            newmobattach.SiteId = userinfo.SiteId;
                            newmobattach.CustomerId = userinfo.CustomerInfoId;
                            newmobattach.UpdatedBy = userinfo.Username;
                            newmobattach.UpdatedDate = CommonUtility.getDTNow();
                            newmobattach.IsPortal = true;
                            retid = MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newmobattach, dbConnection);
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Incident", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Incident", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Incident", "uploadMobileAttachment", ex, dbConnection, userinfo.SiteId);
                }
            }
            return retid.ToString();
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }

        [WebMethod]
        public static List<string> getTableDataAllIncident(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var completelistincident = new List<CustomEvent>();
                //My Incident
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var evHistory = EventHistory.GetEventHistoryByUser(userinfo.Username, dbConnection);

                    var incidentList = new List<int>();
                    foreach (var ev in evHistory)
                    {
                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                        {
                            if (!incidentList.Contains(ev.EventId))
                            {
                                var item = CustomEvent.GetCustomEventById(ev.EventId, dbConnection);
                                if (item != null)
                                {
                                    if (!item.Handled)
                                    {
                                        if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                            if (mobEv != null)
                                            {
                                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                                    item.Name = "None";
                                                else
                                                    item.Name = mobEv.EventTypeName;
                                            }
                                        }
                                        else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                                    item.UserName = CommonUtility.getPCName(reqEv);

                                                if (item.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    var splitName = reqEv.Name.Split('^');
                                                    if (splitName.Length > 1)
                                                        item.Name = splitName[1];
                                                }
                                            }
                                        }
                                        if (item.CreatedBy != userinfo.Username)
                                        {
                                            incidentList.Add(item.EventId);
                                            completelistincident.Add(item); 
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evHistory = EventHistory.GetEventHistoryByUser(userinfo.Username, dbConnection);
                    var incidentList = new List<int>();
                    foreach (var ev in evHistory)
                    {
                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                        {
                            if (!incidentList.Contains(ev.EventId))
                            {
                                var item = CustomEvent.GetCustomEventById(ev.EventId, dbConnection);
                                if (item != null)
                                {
                                    if (!item.Handled)
                                    {
                                        if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                            if (mobEv != null)
                                            {
                                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                                    item.Name = "None";
                                                else
                                                    item.Name = mobEv.EventTypeName;
                                            }
                                        }
                                        else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                                    item.UserName = CommonUtility.getPCName(reqEv);

                                                if (item.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    var splitName = reqEv.Name.Split('^');
                                                    if (splitName.Length > 1)
                                                        item.Name = splitName[1];
                                                }
                                            }
                                        }
                                        if (item.CreatedBy != userinfo.Username)
                                        {
                                            incidentList.Add(item.EventId);
                                            completelistincident.Add(item); 
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var allcustomEventsz = new List<CustomEvent>();

                    if (userinfo.RoleId == (int)Role.Regional) //Level 5
                    {
                        allcustomEventsz.Clear();
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                            allcustomEventsz.AddRange(CustomEvent.GetAllCustomEventByStatusLevel5AndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.ID, true, dbConnection));

                        //allcustomEventsz = allcustomEventsz.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer) //Level 7/6
                    {
                        allcustomEventsz.Clear();
                        var sites = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                        allcustomEventsz.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));

                        foreach (var site in sites)
                            allcustomEventsz.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, site.Id, true, dbConnection));

                        //if (userinfo.RoleId == (int)Role.ChiefOfficer)
                        allcustomEventsz = allcustomEventsz.Where(i => i.Escalate == (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        allcustomEventsz.AddRange(CustomEvent.GetAllCustomEventByStatusCIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.CustomerInfoId, true, dbConnection));

                        //allcustomEventsz = allcustomEventsz.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                    else
                    { // Level 3/4
                        allcustomEventsz = CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection);

                        if (userinfo.RoleId == (int)Role.Admin) //Level 3
                            allcustomEventsz = allcustomEventsz.Where(i => i.Escalate == (int)CustomEvent.EscalateTypes.Level2).ToList();
                        else if (userinfo.RoleId == (int)Role.Director) //Level 4
                            allcustomEventsz = allcustomEventsz.Where(i => i.Escalate == (int)CustomEvent.EscalateTypes.Level3).ToList();

                    }



                    var grouped = allcustomEventsz.GroupBy(item => item.EventId);
                    allcustomEventsz = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                    foreach (var item in allcustomEventsz)
                    {

                        if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                            if (mobEv != null)
                            {
                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                    item.Name = "None";
                                else
                                    item.Name = mobEv.EventTypeName;
                            }
                        }
                        else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                        {
                            var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                            if (reqEv != null)
                            {
                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                    item.UserName = CommonUtility.getPCName(reqEv);

                                if (item.EventType == CustomEvent.EventTypes.Request)
                                {
                                    var splitName = reqEv.Name.Split('^');
                                    if (splitName.Length > 1)
                                        item.Name = splitName[1];
                                }
                            }
                        }
                        completelistincident.Add(item); 

                    }
                }

                //MBHE
                var allcustomEvents = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.MobileHotEvent, userinfo.CustomerInfoId, dbConnection);

                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.MobileHotEvent, userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByTypeByLevel5((int)CustomEvent.EventTypes.MobileHotEvent, userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.MobileHotEvent, dbConnection);
                }

                //escalated
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer) //Level 7/6
                {
                    var sites = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));

                    foreach (var site in sites)
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, site.Id, true, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.Regional) //Level 5
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    var xallcustomEvents = CustomEvent.GetAllCustomEventByStatusLevel5AndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.ID, true, dbConnection);


                    xallcustomEvents = xallcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();

                    allcustomEvents.AddRange(xallcustomEvents);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    var xallcustomEvents = CustomEvent.GetAllCustomEventByStatusCIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.CustomerInfoId, true, dbConnection);

                    xallcustomEvents = xallcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();

                    allcustomEvents.AddRange(xallcustomEvents);
                }
                else // Level 3/4
                {


                    if (userinfo.RoleId == (int)Role.Admin) //Level 3
                    {
                        var xallcustomEvents = CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection);
                        xallcustomEvents = xallcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level3 && i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                        allcustomEvents.AddRange(xallcustomEvents);
                    }
                    else if (userinfo.RoleId == (int)Role.Director) //Level 4
                    {
                        var xallcustomEvents = CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection);
                        xallcustomEvents = xallcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                        allcustomEvents.AddRange(xallcustomEvents);
                    }
                }

                allcustomEvents = allcustomEvents.Where(i => i.EventType == CustomEvent.EventTypes.MobileHotEvent).ToList();

                var grouped2 = allcustomEvents.GroupBy(item => item.EventId);
                allcustomEvents = grouped2.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                foreach (var item in allcustomEvents)
                {
                    if (item.EventType == CustomEvent.EventTypes.MobileHotEvent && item.StatusName != "Resolve")
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            var dir = Accounts.GetDirectorByManagerName(userinfo.Username, dbConnection);
                            if (dir != null)
                            {
                                if (item.StatusName != "Escalated" && item.HandledBy != dir.Username)
                                {
                                    var isGood = false;
                                    if (item.StatusName == "Pending")
                                    {
                                        isGood = true;
                                    }
                                    else
                                    {
                                        if (item.HandledBy == userinfo.Username)
                                        {
                                            isGood = true;
                                        }
                                    }
                                    if (isGood)
                                    {
                                        completelistincident.Add(item);
                                    }
                                }
                            }
                            else
                            {
                                if (item.StatusName != "Escalated")
                                {
                                    var isGood = false;
                                    if (item.StatusName == "Pending")
                                    {
                                        isGood = true;
                                    }
                                    else
                                    {
                                        if (item.HandledBy == userinfo.Username)
                                        {
                                            isGood = true;
                                        }
                                    }
                                    if (isGood)
                                    {
                                        completelistincident.Add(item);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var isGood = false;
                            if (item.StatusName == "Pending")
                            {
                                isGood = true;
                            }
                            else
                            {
                                if (item.HandledBy == userinfo.Username)
                                {
                                    isGood = true;
                                }
                                if (userinfo.RoleId != (int)Role.Admin)
                                {
                                    isGood = true;
                                }
                            }
                            if (isGood)
                            {
                                completelistincident.Add(item);
                            }
                        }
                    }
                }
                //Resolved
                var allcustomEventsR = new List<CustomEvent>();

                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var pclist = new List<string>();
                    allcustomEventsR = CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection);
                    var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);

                    foreach (var custom in customeventlist)
                    {
                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                            allcustomEventsR.Add(custom);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEventsR.AddRange(CustomEvent.GetAllCustomEventByDateAndDirectorIdHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEventsR.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)), manguser, dbConnection));

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);

                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                allcustomEventsR.Add(custom);
                        }
                    }
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEventsR = CustomEvent.GetAllCustomEventByDateHandledAndCId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEventsR = CustomEvent.GetAllCustomEventByDateHandledAndSiteId(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allcustomEventsR.AddRange(CustomEvent.GetAllCustomEventByDateHandledAndLevel5(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));

                        var glist = CustomEvent.GetAllCustomEventByDateHandledAndLevel5(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection);
                    allcustomEventsR.AddRange(glist.Where(i => i.HandledBy == userinfo.Username).ToList());
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEventsR = CustomEvent.GetAllCustomEventByDateHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                }

                var imageclass = string.Empty;
                var groupedR = allcustomEventsR.GroupBy(item => item.EventId);
                allcustomEventsR = groupedR.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                foreach (var item in allcustomEventsR)
                {
                    if (item.EventType != CustomEvent.EventTypes.DriverOffence)
                    {
                        if (item.Handled)
                        {
                            if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                            {
                                if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                {
                                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                                    if (mobEv != null)
                                    {
                                        if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                            item.Name = "None";
                                        else
                                            item.Name = mobEv.EventTypeName;
                                    }
                                }
                                else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                                {
                                    var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                                    if (reqEv != null)
                                    {
                                        if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                            item.UserName = CommonUtility.getPCName(reqEv);

                                        if (item.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var splitName = reqEv.Name.Split('^');
                                            if (splitName.Length > 1)
                                                item.Name = splitName[1];
                                        }
                                    }
                                }
                                completelistincident.Add(item);
                            }
                        }
                    }
                }
                //HotEvents
                var allcustomEventsH = new List<CustomEvent>();
                var vehs = ClientManager.GetAllClientManager(dbConnection);
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var pclist = new List<string>();
                    allcustomEventsH = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                    var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);

                    foreach (var veh in vehs)
                    {
                        if (veh.UserId == userinfo.ID)
                        {
                            var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                            pclist.Add(health.PCName);
                        }
                    }
                    foreach (var custom in customeventlist)
                    {
                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                            allcustomEventsH.Add(custom);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEventsH.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEventsH.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                        foreach (var veh in vehs)
                        {
                            if (veh.UserId == manguser)
                            {
                                var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                                pclist.Add(health.PCName);
                            }
                        }
                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                allcustomEventsH.Add(custom);
                        }
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEventsH = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.HotEvent, userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {

                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                        allcustomEventsH.AddRange(CustomEvent.GetAllCustomEventByTypeByLevel5((int)CustomEvent.EventTypes.HotEvent, userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEventsH = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
                }

                //escalated
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer) //Level 7/6
                {
                    var sites = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                    allcustomEventsH.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));

                    foreach (var site in sites)
                        allcustomEventsH.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, site.Id, true, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.Regional) //Level 5
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allcustomEventsH.AddRange(CustomEvent.GetAllCustomEventByStatusLevel5AndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.ID, true, dbConnection));

                    allcustomEventsH = allcustomEventsH.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    //allcustomEventsH = allcustomEvents.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEventsH = CustomEvent.GetAllCustomEventByStatusCIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.CustomerInfoId, true, dbConnection);

                    allcustomEventsH = allcustomEventsH.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                }
                else
                { // Level 3/4


                    if (userinfo.RoleId == (int)Role.Admin) //Level 3
                    {
                        allcustomEventsH.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEventsH = allcustomEventsH.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level3 && i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();

                    }
                    else if (userinfo.RoleId == (int)Role.Director) //Level 4
                    {
                        allcustomEventsH.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEventsH = allcustomEventsH.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                }

                allcustomEventsH = allcustomEventsH.Where(i => i.EventType == CustomEvent.EventTypes.HotEvent).ToList();

                var groupedH = allcustomEventsH.GroupBy(item => item.EventId);
                allcustomEventsH = groupedH.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();
                //var imageclass = string.Empty;
                foreach (var item in allcustomEventsH)
                {
                    if (item.EventType == CustomEvent.EventTypes.HotEvent && item.StatusName != "Resolve")
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            if (item.StatusName != "Escalated")
                            {
                                var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                                if (hotEv != null)
                                    item.UserName = CommonUtility.getPCName(hotEv);

                                completelistincident.Add(item);
                            }
                        }
                        else
                        {
                            var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                            if (hotEv != null)
                                item.UserName = CommonUtility.getPCName(hotEv);

                            completelistincident.Add(item);
                        }
                    }
                }
                //REquest
                var allcustomEventsRE = new List<CustomEvent>();
                vehs = ClientManager.GetAllClientManager(dbConnection);
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var pclist = new List<string>();
                    allcustomEventsRE = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                    var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.Request, dbConnection);

                    foreach (var veh in vehs)
                    {
                        if (veh.UserId == userinfo.ID)
                        {
                            var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                            pclist.Add(health.PCName);
                        }
                    }
                    foreach (var custom in customeventlist)
                    {
                        var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                        if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                            allcustomEventsRE.Add(custom);
                    }

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEventsRE.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEventsRE.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                        var pclist = new List<string>();
                        var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.Request, dbConnection);
                        foreach (var veh in vehs)
                        {
                            if (veh.UserId == manguser)
                            {
                                var health = HealthCheck.GetDeviceHealthCheckbyMacAddress(veh.MacAddress, dbConnection);
                                pclist.Add(health.PCName);
                            }
                        }
                        foreach (var custom in customeventlist)
                        {
                            var hotEv = HotEvent.GetHotEventById(custom.Identifier, dbConnection);
                            if (pclist.Contains(CommonUtility.getPCName(hotEv)))
                                allcustomEventsRE.Add(custom);
                        }
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEventsRE = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.Request, userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    allcustomEventsRE = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.Request, userinfo.CustomerInfoId, dbConnection);

                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allcustomEventsRE.AddRange(CustomEvent.GetAllCustomEventByTypeByLevel5((int)CustomEvent.EventTypes.Request, userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEventsRE = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.Request, dbConnection);
                }


                //escalated
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer) //Level 7/6
                {
                    var sites = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);
                    allcustomEventsRE.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));

                    foreach (var site in sites)
                        allcustomEventsRE.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, site.Id, true, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.Regional) //Level 5
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    allcustomEventsRE.AddRange(CustomEvent.GetAllCustomEventByStatusLevel5AndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.ID, true, dbConnection));

                    
                    allcustomEventsRE = allcustomEventsRE.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                }
                else
                { // Level 3/4


                    if (userinfo.RoleId == (int)Role.Admin) //Level 3
                    {
                        allcustomEventsRE.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEventsRE = allcustomEventsRE.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level3 && i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();

                    }
                    else if (userinfo.RoleId == (int)Role.Director) //Level 4
                    {
                        allcustomEventsRE.AddRange(CustomEvent.GetAllCustomEventByStatusSiteIdAndHandled((int)CustomEvent.IncidentActionStatus.Escalated, userinfo.SiteId, true, dbConnection));
                        allcustomEventsRE = allcustomEventsRE.Where(i => i.Escalate != (int)CustomEvent.EscalateTypes.Level4).ToList();
                    }
                }

                allcustomEventsRE = allcustomEventsRE.Where(i => i.EventType == CustomEvent.EventTypes.Request).ToList();

                var groupedHE = allcustomEventsRE.GroupBy(item => item.EventId);
                allcustomEventsRE = groupedHE.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                //var imageclass = string.Empty;
                foreach (var item in allcustomEventsRE)
                {
                    if (item.EventType == CustomEvent.EventTypes.Request && item.StatusName != "Resolve")
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            var dir = Accounts.GetDirectorByManagerName(userinfo.Username, dbConnection);
                            if (dir != null)
                            {
                                if (item.StatusName != "Escalated" && item.HandledBy != dir.Username)
                                {
                                    var isGood = false;
                                    if (item.StatusName == "Pending")
                                    {
                                        isGood = true;
                                    }
                                    else
                                    {
                                        if (item.HandledBy == userinfo.Username)
                                        {
                                            isGood = true;
                                        }
                                    }
                                    if (isGood)
                                    {
                                        var continuE = true;
                                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Dispatch)
                                        {
                                            if (item.HandledBy != userinfo.Username)
                                                continuE = false;
                                        }
                                        if (continuE)
                                        {
                                            var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                                            if (hotEv != null)
                                            {
                                                if (CommonUtility.getPCName(hotEv) != "MIMS MOBILE")
                                                    item.UserName = CommonUtility.getPCName(hotEv);

                                                var splitName = hotEv.Name.Split('^');
                                                if (splitName.Length > 1)
                                                    item.Name = splitName[1];

                                            }
                                            completelistincident.Add(item);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (item.StatusName != "Escalated")
                                {
                                    var isGood = false;
                                    if (item.StatusName == "Pending")
                                    {
                                        isGood = true;
                                    }
                                    else
                                    {
                                        if (item.HandledBy == userinfo.Username)
                                        {
                                            isGood = true;
                                        }
                                    }
                                    if (isGood)
                                    {
                                        var continuE = true;
                                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Dispatch)
                                        {
                                            if (item.HandledBy != userinfo.Username)
                                                continuE = false;
                                        }
                                        if (continuE)
                                        {
                                            var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                                            if (hotEv != null)
                                            {
                                                if (CommonUtility.getPCName(hotEv) != "MIMS MOBILE")
                                                    item.UserName = CommonUtility.getPCName(hotEv);

                                                var splitName = hotEv.Name.Split('^');
                                                if (splitName.Length > 1)
                                                    item.Name = splitName[1];

                                            }
                                            completelistincident.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            var isGood = false;
                            if (item.StatusName == "Pending")
                            {
                                isGood = true;
                            }
                            else
                            {
                                if (item.HandledBy == userinfo.Username)
                                {
                                    isGood = true;
                                }
                                if (userinfo.RoleId != (int)Role.Admin)
                                {
                                    isGood = true;
                                }
                            }
                            if (isGood)
                            {
                                var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                                if (hotEv != null)
                                {
                                    if (CommonUtility.getPCName(hotEv) != "MIMS MOBILE")
                                        item.UserName = CommonUtility.getPCName(hotEv);

                                    var splitName = hotEv.Name.Split('^');
                                    if (splitName.Length > 1)
                                        item.Name = splitName[1];

                                }
                                completelistincident.Add(item);
                            }
                        }
                    }
                }
                if (completelistincident.Count > 0)
                {
                    var groupedCC = completelistincident.GroupBy(item => item.EventId);
                    completelistincident = groupedCC.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                    foreach (var item in completelistincident)
                    {
                        var cimageclass = string.Empty;

                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                            cimageclass = "circle-point-red";
                        else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                            cimageclass = "circle-point-green";
                        else
                            cimageclass = "circle-point-yellow";

                        //var getHotName = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                        //if (getHotName != null)
                        //{
                        //    item.Name = getHotName.EventTypeName;
                        //    if (string.IsNullOrEmpty(getHotName.EventTypeName))
                        //        item.Name = "None";
                        //}

                        if(item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            item.Name = item.EventTypeName;
                        }
                        else if (item.EventType == CustomEvent.EventTypes.Request)
                        {
                            var splitName = item.RequestName.Split('^');
                            if (splitName.Length > 1)
                                item.Name = splitName[1];
                        }
                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + cimageclass + "'></span></span></td><td>" + item.CustomerIncidentId + "</td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name  + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableDataAllIncident", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
    


    }
}