﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public enum MobileDeviceType
    {
        None = 0,
        Mobile = 1,
        Tablet = 2
    }
    public class MobileDevice
    {
        public int Id { get; set; }
        public string UpdatedBy { get; set; }
        public string Uuid { get; set; }
        public int DeviceType { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        private static MobileDevice GetMobileDeviceFromSqlReader(SqlDataReader reader, string dbConnection)
        {
            var mobileDevice = new MobileDevice();

            if (reader["Id"] != DBNull.Value)
                mobileDevice.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Uuid"] != DBNull.Value)
                mobileDevice.Uuid = Decrypt.DecryptData(reader["Uuid"].ToString(), true, dbConnection);
            if (reader["DeviceType"] != DBNull.Value)
                mobileDevice.DeviceType = Convert.ToInt32(Decrypt.DecryptData(reader["DeviceType"].ToString(), true, dbConnection));
            if (reader["IsActive"] != DBNull.Value)
                mobileDevice.IsActive = Convert.ToBoolean(Decrypt.DecryptData(reader["IsActive"].ToString(), true, dbConnection));
            if (reader["CreatedDate"] != DBNull.Value)
                mobileDevice.CreatedDate = Convert.ToDateTime(Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection));
            if (reader["Updateddate"] != DBNull.Value)
                mobileDevice.UpdatedDate = Convert.ToDateTime(Decrypt.DecryptData(reader["Updateddate"].ToString(), true, dbConnection));
            
            return mobileDevice;
        }

        public static bool InsertorUpdateMobileDevice(MobileDevice mobileDevice, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (mobileDevice != null)
            {
                int id = mobileDevice.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateMobileDevice", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = mobileDevice.Id;

                    cmd.Parameters.Add(new SqlParameter("@Uuid", SqlDbType.NVarChar));
                    cmd.Parameters["@Uuid"].Value = Encrypt.EncryptData(mobileDevice.Uuid, true, dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@DeviceType", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceType"].Value = Encrypt.EncryptData(mobileDevice.DeviceType.ToString(), true, dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@IsActive", SqlDbType.NVarChar));
                    cmd.Parameters["@IsActive"].Value = Encrypt.EncryptData(mobileDevice.IsActive.ToString(), true, dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(mobileDevice.CreatedDate.ToString(), true, dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedDate"].Value = Encrypt.EncryptData(mobileDevice.UpdatedDate.ToString(), true, dbConnection);

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            mobileDevice.Id = id;
                            var audit = new MobileDeviceAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, mobileDevice.UpdatedBy);
                            Reflection.CopyProperties(mobileDevice, audit);
                            MobileDeviceAudit.InsertMobileDeviceAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateMobileDevice", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static MobileDevice GetMobileDeviceByUuid(string uuid, string dbConnection)
        {
            var mobileDevice = new MobileDevice();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetMobileDeviceByUuid", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Uuid", SqlDbType.NVarChar)).Value = Encrypt.EncryptData(uuid, true, dbConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    mobileDevice = GetMobileDeviceFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return mobileDevice;
        }

        public static List<MobileDevice> GetAllActiveMobileDevicesByDeviceType(int deviceType , string dbConnection)
        {
            var mobileDevices = new List<MobileDevice>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllActiveMobileDevicesByDeviceType", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@DeviceType", SqlDbType.NVarChar)).Value = Encrypt.EncryptData(deviceType.ToString(), true, dbConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@IsActive", SqlDbType.NVarChar)).Value = Encrypt.EncryptData(true.ToString(), true, dbConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    mobileDevices.Add(GetMobileDeviceFromSqlReader(reader, dbConnection));
                }
                connection.Close();
                reader.Close();
            }
            return mobileDevices;
        }
    }
}
