﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class MimsSupport
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DocumentPath { get; set; }
        public string VideoPath { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Updatedby { get; set; }
        private static MimsSupport GetMimsSupportFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var mimsSupport = new MimsSupport();
            if (reader["Id"] != DBNull.Value)
                mimsSupport.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                mimsSupport.Name = reader["Name"].ToString();
            if (reader["Description"] != DBNull.Value)
                mimsSupport.Description = reader["Description"].ToString();
            if (reader["VideoPath"] != DBNull.Value)
                mimsSupport.VideoPath = reader["VideoPath"].ToString();
            if (reader["DocumentPath"] != DBNull.Value)
                mimsSupport.DocumentPath = reader["DocumentPath"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                mimsSupport.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                mimsSupport.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["Updatedby"] != DBNull.Value)
                mimsSupport.Updatedby = reader["Updatedby"].ToString();
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                mimsSupport.CreatedBy = reader["CreatedBy"].ToString();

            return mimsSupport;
        }
        public static List<MimsSupport> GetAllMimsSupport(string dbConnection)
        {
            var mimsSupports = new List<MimsSupport>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllMimsSupport";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var mimsSupport = GetMimsSupportFromSqlReader(reader);
                    mimsSupports.Add(mimsSupport);
                }
                connection.Close();
                reader.Close();
            }
            return mimsSupports;
        }
    }
}
