﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class Activity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ActivityType { get; set; }
        public int ActivityTypeId { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public string Username { get; set; }
        public int UserId { get; set; }

        public string StartLati { get; set; }
        public string EndLati { get; set; }
        public string StartLong { get; set; }
        public string EndLong { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public int Status { get; set; }
        public string StatusName { get; set; }
        public string ActivityClass { get; set; }
        public string CustomerUName { get; set; }
        private static Activity GetActivityFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();


            var notification = new Activity();

            if (reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                notification.Name = reader["Name"].ToString();
            if (reader["Description"] != DBNull.Value)
                notification.Description = reader["Description"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["ActivityType"] != DBNull.Value)
                notification.ActivityType = reader["ActivityType"].ToString();
            if (reader["ActivityTypeId"] != DBNull.Value)
                notification.ActivityTypeId = Convert.ToInt32(reader["ActivityTypeId"].ToString());


            if (reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                notification.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());


            if (reader["Username"] != DBNull.Value)
                notification.Username = reader["Username"].ToString();

            if (reader["UserId"] != DBNull.Value)
                notification.UserId = Convert.ToInt32(reader["UserId"].ToString());

            if (reader["StartTime"] != DBNull.Value)
                notification.StartTime = Convert.ToDateTime(reader["StartTime"].ToString());

            if (reader["EndTime"] != DBNull.Value)
                notification.EndTime = Convert.ToDateTime(reader["EndTime"].ToString());

            if (reader["StartLati"] != DBNull.Value)
                notification.StartLati = reader["StartLati"].ToString();

            if (reader["EndLati"] != DBNull.Value)
                notification.EndLati = reader["EndLati"].ToString();

            if (reader["StartLong"] != DBNull.Value)
                notification.StartLong = reader["StartLong"].ToString();

            if (reader["EndLong"] != DBNull.Value)
                notification.EndLong = reader["EndLong"].ToString();

            if (reader["Status"] != DBNull.Value)
                notification.Status = Convert.ToInt32(reader["Status"].ToString());

            if (columns.Contains( "CustomerUName"))
                notification.CustomerUName = reader["CustomerUName"].ToString(); 

            if (notification.Status == 0) //Pending
            {
                notification.ActivityClass = "circle-point-red";
                notification.StatusName = "START";
            }
            else if (notification.Status == 1) //Inprogress
            {
                notification.ActivityClass = "circle-point-yellow";
                notification.StatusName = "FINISH";
            }
            else if (notification.Status == 2) //Completed
            {
                notification.ActivityClass = "circle-point-green";
                notification.StatusName = "CLEAR";
            }
            return notification;
        }

        public static List<Activity> GetAllActivity(string dbConnection)
        {
            var assetList = new List<Activity>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllActivity", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var asset = GetActivityFromSqlReader(reader);
                        assetList.Add(asset);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Activity", "GetAllActivity", exp, dbConnection);

            }
            return assetList;
        }

        public static Activity GetActivityById(int id, string dbConnection)
        {
            Activity asset = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetActivityById", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        asset = GetActivityFromSqlReader(reader);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Activity", "GetActivityById", exp, dbConnection);

            }
            return asset;
        }

        public static List<Activity> GetActivityByUserId(int id, string dbConnection)
        {
            var itemFounds = new List<Activity>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetActivityByUserId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        itemFounds.Add(GetActivityFromSqlReader(reader));
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Activity", "GetActivityByUserId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<Activity> GetAllActivityBySiteId(int siteId, string dbConnection)
        {
            var itemFounds = new List<Activity>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllActivityBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetActivityFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Activity", "GetAllActivityBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<Activity> GetAllActivityByLevel5(int siteId, string dbConnection)
        {
            var itemFounds = new List<Activity>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllActivityByLevel5", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetActivityFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Activity", "GetAllActivityBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<Activity> GetAllActivityByCId(int siteId, string dbConnection)
        {
            var itemFounds = new List<Activity>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllActivityByCId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetActivityFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Activity", "GetAllActivityBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }
        public static bool DeleteActivityById(int empid, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteActivityById", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = empid;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteActivityById";

                    cmd.ExecuteNonQuery();
                }
                return true;

            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Activity", "DeleteActivityById", ex, dbConnection);
            }
            return false;
        }

        public static int InsertorUpdateActivity(Activity notification, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateActivity", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = notification.Name;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = notification.Description;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ActivityType", SqlDbType.NVarChar));
                    cmd.Parameters["@ActivityType"].Value = notification.ActivityType;

                    cmd.Parameters.Add(new SqlParameter("@ActivityTypeId", SqlDbType.Int));
                    cmd.Parameters["@ActivityTypeId"].Value = notification.ActivityTypeId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = notification.Username;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = notification.UserId;


                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = notification.Status;

                    cmd.Parameters.Add(new SqlParameter("@StartTime", SqlDbType.DateTime));
                    cmd.Parameters["@StartTime"].Value = notification.StartTime;
                    cmd.Parameters.Add(new SqlParameter("@EndTime", SqlDbType.DateTime));
                    cmd.Parameters["@EndTime"].Value = notification.EndTime;

                    cmd.Parameters.Add(new SqlParameter("@StartLati", SqlDbType.NVarChar));
                    cmd.Parameters["@StartLati"].Value = notification.StartLati;
                    cmd.Parameters.Add(new SqlParameter("@EndLati", SqlDbType.NVarChar));
                    cmd.Parameters["@EndLati"].Value = notification.EndLati;
                    cmd.Parameters.Add(new SqlParameter("@StartLong", SqlDbType.NVarChar));
                    cmd.Parameters["@StartLong"].Value = notification.StartLong;
                    cmd.Parameters.Add(new SqlParameter("@EndLong", SqlDbType.NVarChar));
                    cmd.Parameters["@EndLong"].Value = notification.EndLong;
 
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateActivity";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            notification.Id = id;
                            var audit = new ActivityAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, notification.CreatedBy);
                            Reflection.CopyProperties(notification, audit);
                            ActivityAudit.InsertActivityAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateActivity", ex, dbConnection);
                    }
                }
            }
            return id;
        }
    }
}
