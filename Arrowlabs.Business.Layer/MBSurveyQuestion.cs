﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class MBSurveyQuestion
    {
        public int Id { get; set; } 
        public bool Active { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; } 
        public string Question { get; set; } 
        public DateTime? CreatedDate { get; set; } 
        public string CreatedBy { get; set; } 
        public string CustomerUName { get; set; }
        public int Green { get; set; }
        public int Yellow { get; set; } 
        public int Red { get; set; }

        public static bool DeleteMBSurveyQuestionById(int empid, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteMBSurveyQuestionById", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = empid;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteMBSurveyQuestionById";

                    cmd.ExecuteNonQuery();
                }
                return true;

            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("MBSurveyQuestion", "DeleteMBSurveyQuestionById", ex, dbConnection);
            }
            return false;
        }

        private static MBSurveyQuestion GetMBSurveyQuestionFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();


            var notification = new MBSurveyQuestion();

            if (reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["Red"] != DBNull.Value)
                notification.Red = Convert.ToInt32(reader["Red"].ToString());

            if (reader["Yellow"] != DBNull.Value)
                notification.Yellow = Convert.ToInt32(reader["Yellow"].ToString());

            if (reader["Green"] != DBNull.Value)
                notification.Green = Convert.ToInt32(reader["Green"].ToString()); 

            if (reader["Question"] != DBNull.Value)
                notification.Question = reader["Question"].ToString(); 
            if (reader["Active"] != DBNull.Value)
                notification.Active = Convert.ToBoolean(reader["Active"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString()); 
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString(); 
            if (reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                notification.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                notification.CustomerUName = reader["CustomerUName"].ToString();
             

            return notification;
        }

        public static int InsertorUpdateMBSurveyQuestion(MBSurveyQuestion notification, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateMBSurveyQuestion", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@Question", SqlDbType.NVarChar));
                    cmd.Parameters["@Question"].Value = notification.Question;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;
                      
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy; 

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit));
                    cmd.Parameters["@Active"].Value = notification.Active;

                    cmd.Parameters.Add(new SqlParameter("@Green", SqlDbType.Int));
                    cmd.Parameters["@Green"].Value = notification.Green;

                    cmd.Parameters.Add(new SqlParameter("@Yellow", SqlDbType.Int));
                    cmd.Parameters["@Yellow"].Value = notification.Yellow;

                    cmd.Parameters.Add(new SqlParameter("@Red", SqlDbType.Int));
                    cmd.Parameters["@Red"].Value = notification.Red;
                      
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateMBSurveyQuestion";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            notification.Id = id;
                            var audit = new MBSurveyQuestionAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, notification.CreatedBy);
                            Reflection.CopyProperties(notification, audit);
                            MBSurveyQuestionAudit.InserMBSurveyQuestionAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InserMBSurveyQuestionAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }


        public static List<MBSurveyQuestion> GetAllMBSurveyQuestion(string dbConnection)
        {
            var assetList = new List<MBSurveyQuestion>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllMBSurveyQuestion", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var asset = GetMBSurveyQuestionFromSqlReader(reader);
                        assetList.Add(asset);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MBSurveyQuestion", "GetAllMBSurveyQuestion", exp, dbConnection);

            }
            return assetList;
        }

        public static MBSurveyQuestion GetMBSurveyQuestionById(int id, string dbConnection)
        {
            MBSurveyQuestion asset = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetMBSurveyQuestionById", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        asset = GetMBSurveyQuestionFromSqlReader(reader);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MBSurveyQuestion", "GetMBSurveyQuestionById", exp, dbConnection);

            }
            return asset;
        }

        public static List<MBSurveyQuestion> GetAllMBSurveyQuestionBySiteId(int siteId, string dbConnection)
        {
            var itemFounds = new List<MBSurveyQuestion>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllMBSurveyQuestionBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetMBSurveyQuestionFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MBSurveyQuestion", "GetAllMBSurveyQuestionBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<MBSurveyQuestion> GetAllMBSurveyQuestionByLevel5(int siteId, string dbConnection)
        {
            var itemFounds = new List<MBSurveyQuestion>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllMBSurveyQuestionByLevel5", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetMBSurveyQuestionFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MBSurveyQuestion", "GetAllMBSurveyQuestionByLevel5", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<MBSurveyQuestion> GetAllMBSurveyQuestionByCustomerId(int siteId, string dbConnection)
        {
            var itemFounds = new List<MBSurveyQuestion>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllMBSurveyQuestionByCustomerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetMBSurveyQuestionFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                } 
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MBSurveyQuestion", "GetAllMBSurveyQuestionByCustomerId", exp, dbConnection); 
            }
            return itemFounds;
        }
    }
}
