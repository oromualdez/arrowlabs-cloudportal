﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class TaskCanvasCategory
    {
        public int Id { get; set; }
        public string UpdatedBy { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int CustomerId { get; set; }
        public static void DeleteTaskCanvasCategory(int id,string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTaskCanvasCategory", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTaskCanvasCategory";

                cmd.ExecuteNonQuery();
            }
        }

        private static TaskCanvasCategory GetTaskCanvasCategoryFromSqlReader(SqlDataReader reader)
        {
            var hotEventCategory = new TaskCanvasCategory();

            if (reader["Id"] != DBNull.Value)
                hotEventCategory.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Description"] != DBNull.Value)
                hotEventCategory.Description = reader["Description"].ToString();
            if (reader["Color"] != DBNull.Value)
                hotEventCategory.Color = reader["Color"].ToString();
            if (reader["CreatedBy"] != DBNull.Value)
                hotEventCategory.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                hotEventCategory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (reader["UpdatedBy"] != DBNull.Value)
                hotEventCategory.UpdatedBy = reader["UpdatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                hotEventCategory.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
            if (reader["IsActive"] != DBNull.Value)
                hotEventCategory.IsActive = Convert.ToBoolean(reader["IsActive"]);
            if (reader["CustomerId"] != DBNull.Value)
                hotEventCategory.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
            

            return hotEventCategory;
        }
        public static List<TaskCanvasCategory> GetAllTaskCanvasCategories(string dbConnection)
        {
            var taskCanvasCategories = new List<TaskCanvasCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllTaskCanvasCategories";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var taskCanvasCategory = GetTaskCanvasCategoryFromSqlReader(reader);
                    taskCanvasCategories.Add(taskCanvasCategory);
                }
                connection.Close();
                reader.Close();
            }
            return taskCanvasCategories;
        }

        public static int InsertorUpdateTaskCanvasCategory(TaskCanvasCategory taskCanvasCategory, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            if (taskCanvasCategory != null)
            {
                id = taskCanvasCategory.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("InsertorUpdateTaskCanvasCategory", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskCanvasCategory.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = taskCanvasCategory.CustomerId;
                    

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = taskCanvasCategory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = taskCanvasCategory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = taskCanvasCategory.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = taskCanvasCategory.UpdatedBy;
             
                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = taskCanvasCategory.Description;

                    cmd.Parameters.Add(new SqlParameter("@Color", SqlDbType.NVarChar));
                    cmd.Parameters["@Color"].Value = taskCanvasCategory.Color;

                    cmd.Parameters.Add(new SqlParameter("@IsActive", SqlDbType.Bit));
                    cmd.Parameters["@IsActive"].Value = taskCanvasCategory.IsActive;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            taskCanvasCategory.Id = id;
                            var audit = new TaskCanvasCategoryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, taskCanvasCategory.UpdatedBy);
                            Reflection.CopyProperties(taskCanvasCategory, audit);
                            TaskCanvasCategoryAudit.InsertTaskCanvasCategoryAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateTaskCanvasCategory", ex, auditDbConnection);
                    }

                }
            }
            return id;
        }
    }
}
