﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Enums
{
    public enum Action
    {
        None = 0,
        Create = 1,
        BeforeUpdate = 2,
        AfterUpdate = 3,
        BeforeDelete = 4,
        AfterDelete = 5,
        Engage = 6
    }
}
