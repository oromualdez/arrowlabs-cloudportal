﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MobileHotEventTableUC.ascx.cs" Inherits="ArrowLabs.Licence.Portal.Controls.Tables.MobileHotEventTableUC" %>
<script>
    function addrowtoTable(pagename, methodName, tablename, loggedinUsername) {
        $.ajax({
            type: "POST",
            url: pagename + ".aspx/" + methodName,
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                for (var i = 0; i < data.d.length; i++) {
                    if (i == 0) {
                        document.getElementById(tablename+'header').innerHTML = data.d[0];
                    }
                    else
                        $("#" + tablename + " tbody").append(data.d[i]);
                }
            }
        });
    }
</script>

<div class="col-md-12">
    <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
        <div class="panel-heading">
            <div class="row no-gutter">
                <div class="col-md-8">
                    <h3 id="<%=TableName%>header" class="panel-title capitalize-text"></h3>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<%=MobileHandledPercentage%>" aria-valuemin="1" aria-valuemax="100" style="width: <%=MobileHandledPercentage%>%">
									</div>
                                </div>						
                            </div>
                            <div class="col-md-8">
                                <p class="white-color progress-bar-title"><%=MobileHandledPercentage%>% Handled</p>
                            </div>
                        </div>
                </div>
                <div class="col-md-4 mt-2x">
                    <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                    <div class="clearfx"></div>
                </div>
            </div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" order-of-rows="desc" order-of-column="4" id="<%=TableName%>" role="grid">
                    <thead>
                        <tr role="row">
                             <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-label="PRIORITY" aria-sort="ascending">PRIORITY
                                </th>
                               <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                </th>
                         
                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                </th>
                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">TIME<i class="fa fa-sort ml-2x"></i>
                                </th>
                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">USER<i class="fa fa-sort ml-2x"></i>
                                </th>
                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                </th>
                            </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            </div>
    </div>
</div>