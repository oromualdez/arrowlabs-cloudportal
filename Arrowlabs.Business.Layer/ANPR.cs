﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    [DataContract]
    public class ANPR
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string TransactionID { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string SiteName { get; set; }
        [DataMember]
        public int DPUId { get; set; }
        [DataMember]
        public string DPUName { get; set; }
        [DataMember]
        public System.DateTime TransactionTime { get; set; }
        [DataMember]
        public string PlateNumber { get; set; }
        [DataMember]
        public string PlateCity { get; set; }
        [DataMember]
        public string PlateCountry { get; set; }
        [DataMember]
        public string PlatePrefix { get; set; }
        [DataMember]
        public System.Nullable<decimal> Confidence { get; set; }
        [DataMember]
        public string PlateType { get; set; }
        [DataMember]
        public string PlateCategory { get; set; }
        [DataMember]
        public string PlateColor { get; set; }
        [DataMember]
        public string PlateImage { get; set; }
        [DataMember]
        public string OverviewImage { get; set; }
        [DataMember]
        public string AlarmStatusCode { get; set; }
        [DataMember]
        public string AlarmStatusTitle { get; set; }
        [DataMember]
        public int LaneNo { get; set; }
        [DataMember]
        public string LaneName { get; set; }
        [DataMember]
        public string PlateNumberOnly { get; set; }
        [DataMember]
        public string PlateTypeID { get; set; }
        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public double Longitude { get; set; }
        [DataMember]
        public byte[] MobileCameraCapture { get; set; }
        [DataMember]
        public Types Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }

        public enum Types : int
        {
            MIMSClient = 0,
            MIMSMobile = 1,
            ANPRSystem = 3,
            EnrollmentRequest = 4,
            Enrolled = 5,
            EnrollRejected = 6
        }
        public enum enumPlateCategory : int
        {
            Unknown = 0,
            MotorBike = 1,
            Transport = 2,
            Private = 3,
            Army = 4,
            CD = 5,
            Police = 6,
            Government = 7,
            Customs = 8,
            Testing = 9,
            Export = 10,
            CC = 11,
            Transfer = 12,
            Commercial = 13,
            Learning = 14,
            Municipality = 15,
            Diplomatic = 16,
            Protocol = 17,
            Equipment = 18,
            UN = 19,
            MOJ = 20,
            NotAvailable = 21
        }
        public static List<ANPR> GetAllANPR(string dbConnection)
        {
            var ANPRList = new List<ANPR>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllANPR", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var anpr = GetANPRFromSqlReader(reader);
                    ANPRList.Add(anpr);
                }
                connection.Close();
                reader.Close();
            }
            return ANPRList;
        }

        public static List<ANPR> GetAllANPRBySiteId(int siteid, string dbConnection)
        {
            var ANPRList = new List<ANPR>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllANPRBySiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var anpr = GetANPRFromSqlReader(reader);
                    ANPRList.Add(anpr);
                }
                connection.Close();
                reader.Close();
            }
            return ANPRList;
        }

        private static ANPR GetANPRFromSqlReader(SqlDataReader reader)
        {
            var anpr = new ANPR();
            if (reader["Id"] != DBNull.Value)
                anpr.Id = Convert.ToInt64(reader["Id"].ToString());
            if (reader["TransactionID"] != DBNull.Value)
                anpr.TransactionID = reader["TransactionID"].ToString();//Convert.ToInt64(reader["TransactionID"].ToString());
            if (reader["CreatedDated"] != DBNull.Value)
                anpr.CreatedDate = Convert.ToDateTime(reader["CreatedDated"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                anpr.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["PlateTypeID"] != DBNull.Value)
                anpr.PlateTypeID = reader["PlateTypeID"].ToString();
            if (reader["LaneName"] != DBNull.Value)
                anpr.LaneName = reader["LaneName"].ToString();
            if (reader["LaneNo"] != DBNull.Value)
                anpr.LaneNo = Convert.ToInt32(reader["LaneNo"].ToString());
            if (reader["OverviewImage"] != DBNull.Value)
                anpr.OverviewImage = reader["OverviewImage"].ToString();
            if (reader["PlateImage"] != DBNull.Value)
                anpr.PlateImage = reader["PlateImage"].ToString();

            if (reader["PlatePrefix"] != DBNull.Value)
                anpr.PlatePrefix = reader["PlatePrefix"].ToString();

            if (reader["DPUId"] != DBNull.Value)
                anpr.DPUId = Convert.ToInt32(reader["DPUId"].ToString());

            if (reader["SiteId"] != DBNull.Value)
                anpr.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (reader["TransactionTime"] != DBNull.Value)
                anpr.TransactionTime = Convert.ToDateTime(reader["TransactionTime"].ToString());
            if (reader["AlarmStatusTitle"] != DBNull.Value)
                anpr.AlarmStatusTitle = reader["AlarmStatusTitle"].ToString();
            if (reader["AlarmStatusCode"] != DBNull.Value)
                anpr.AlarmStatusCode = reader["AlarmStatusCode"].ToString();
            if (reader["PlateType"] != DBNull.Value)
                anpr.PlateType = reader["PlateType"].ToString();
            if (reader["PlateCategory"] != DBNull.Value)
                anpr.PlateCategory = reader["PlateCategory"].ToString();
            if (reader["PlateCountry"] != DBNull.Value)
                anpr.PlateCountry = reader["PlateCountry"].ToString();
            if (reader["PlateCity"] != DBNull.Value)
                anpr.PlateCity = reader["PlateCity"].ToString();
            if (reader["PlateNumber"] != DBNull.Value)
                anpr.PlateNumber = reader["PlateNumber"].ToString();
            if (reader["DPUName"] != DBNull.Value)
                anpr.DPUName = reader["DPUName"].ToString();
            if (reader["PlateColor"] != DBNull.Value)
                anpr.PlateColor = reader["PlateColor"].ToString();
            if (reader["SiteName"] != DBNull.Value)
                anpr.SiteName = reader["SiteName"].ToString();

            if (reader["Latitude"] != DBNull.Value)
                anpr.Latitude = Convert.ToDouble(reader["Latitude"].ToString());

            if (reader["Longitude"] != DBNull.Value)
                anpr.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
            if (reader["Type"] != DBNull.Value)
                anpr.Type = (Types)Convert.ToInt16(reader["Type"].ToString());
            return anpr;
        }

        public static bool DeleteANPRByTransactionID(int transactionID, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("DeleteANPRByTransactionID", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.BigInt));
                sqlCommand.Parameters["@Id"].Value = transactionID;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "DeleteANPRByTransactionID";
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            return true;
        }

        public static ANPR GetANPRByTransactionID(int transactionID, string dbConnection)
        {
            ANPR anpr = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetANPRByTransactionID", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.BigInt));
                sqlCommand.Parameters["@Id"].Value = transactionID;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    anpr = GetANPRFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return anpr;
        }

        public static List<ANPR> GetAllANPRByType(ANPR.Types type, string dbConnection)
        {
            var ANPRList = new List<ANPR>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllANPRByType", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = (int)type;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var anpr = GetANPRFromSqlReader(reader);
                    ANPRList.Add(anpr);
                }
                connection.Close();
                reader.Close();
            }
            return ANPRList;
        }
        public static ANPR GetANPRById(int id, string dbConnection)
        {
            var ANPRList = new ANPR();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetANPRById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    ANPRList = GetANPRFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return ANPRList;
        }
        
        public static int InsertOrUpdateANPR(ANPR anpr, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            try
            {
                var action = (anpr.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                if (anpr != null)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateANPR", connection);

                        cmd.Parameters.Add(new SqlParameter("@SiteName", SqlDbType.NVarChar));
                        cmd.Parameters["@SiteName"].Value = anpr.SiteName;

                        cmd.Parameters.Add(new SqlParameter("@DPUName", SqlDbType.NVarChar));
                        cmd.Parameters["@DPUName"].Value = anpr.DPUName;

                        cmd.Parameters.Add(new SqlParameter("@PlateNumber", SqlDbType.NVarChar));
                        cmd.Parameters["@PlateNumber"].Value = anpr.PlateNumber;

                        cmd.Parameters.Add(new SqlParameter("@PlateCity", SqlDbType.NVarChar));
                        cmd.Parameters["@PlateCity"].Value = anpr.PlateCity;

                        cmd.Parameters.Add(new SqlParameter("@PlateCountry", SqlDbType.NVarChar));
                        cmd.Parameters["@PlateCountry"].Value = anpr.PlateCountry;

                        cmd.Parameters.Add(new SqlParameter("@PlateCategory", SqlDbType.NVarChar));
                        cmd.Parameters["@PlateCategory"].Value = anpr.PlateCategory;

                        cmd.Parameters.Add(new SqlParameter("@PlateColor", SqlDbType.NVarChar));
                        cmd.Parameters["@PlateColor"].Value = anpr.PlateColor;

                        cmd.Parameters.Add(new SqlParameter("@PlateType", SqlDbType.NVarChar));
                        cmd.Parameters["@PlateType"].Value = anpr.PlateType;

                        cmd.Parameters.Add(new SqlParameter("@AlarmStatusCode", SqlDbType.NVarChar));
                        cmd.Parameters["@AlarmStatusCode"].Value = anpr.AlarmStatusCode;

                        cmd.Parameters.Add(new SqlParameter("@AlarmStatusTitle", SqlDbType.NVarChar));
                        cmd.Parameters["@AlarmStatusTitle"].Value = anpr.AlarmStatusTitle;

                        cmd.Parameters.Add(new SqlParameter("@TransactionTime", SqlDbType.DateTime));
                        cmd.Parameters["@TransactionTime"].Value = anpr.TransactionTime;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = anpr.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@DPUId", SqlDbType.Int));
                        cmd.Parameters["@DPUId"].Value = anpr.DPUId;

                        cmd.Parameters.Add(new SqlParameter("@PlatePrefix", SqlDbType.NVarChar));
                        cmd.Parameters["@PlatePrefix"].Value = anpr.PlatePrefix;

                        cmd.Parameters.Add(new SqlParameter("@PlateImage", SqlDbType.NVarChar));
                        cmd.Parameters["@PlateImage"].Value = anpr.PlateImage;

                        cmd.Parameters.Add(new SqlParameter("@LaneNo", SqlDbType.Int));
                        cmd.Parameters["@LaneNo"].Value = anpr.LaneNo;

                        cmd.Parameters.Add(new SqlParameter("@OverviewImage", SqlDbType.NVarChar));
                        cmd.Parameters["@OverviewImage"].Value = anpr.OverviewImage;

                        cmd.Parameters.Add(new SqlParameter("@LaneName", SqlDbType.NVarChar));
                        cmd.Parameters["@LaneName"].Value = anpr.LaneName;

                        cmd.Parameters.Add(new SqlParameter("@PlateTypeID", SqlDbType.NVarChar));
                        cmd.Parameters["@PlateTypeID"].Value = anpr.PlateTypeID;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = anpr.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@TransactionID", SqlDbType.BigInt));
                        cmd.Parameters["@TransactionID"].Value = anpr.PlateImage;

                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.BigInt));
                        cmd.Parameters["@Id"].Value = anpr.Id;
                        
                        cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                        cmd.Parameters["@Latitude"].Value = anpr.Latitude;

                        cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                        cmd.Parameters["@Longitude"].Value = anpr.Longitude;

                        cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                        cmd.Parameters["@Type"].Value = (int) anpr.Type;

                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();

                        id = (int)returnParameter.Value;


                        //if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        //{
                        //    var audit = new AssetCategoryAudit();
                        //    audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, anpr.UpdatedBy);
                        //    Reflection.CopyProperties(anpr, audit);
                        //    AssetCategoryAudit.InsertAssetCategoryAudit(audit, auditDbConnection);
                        //}

                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateANPR", ex, dbConnection);
            }
            return id;
        }
    }
}
