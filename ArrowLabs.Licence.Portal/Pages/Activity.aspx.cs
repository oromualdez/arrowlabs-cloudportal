﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class Activity : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static ClientLicence getClientLic;

        protected string senderName;
        protected string ipaddress;
        protected string senderName2;
        protected string userinfoDisplay;
        protected string userinfoDisplay2;
        protected string senderName3;
        protected string sourceLat;
        protected string sourceLon;
        protected string siteName;
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            dbConnection = CommonUtility.dbConnection;
            userinfoDisplay = "block";
            userinfoDisplay2 = "block";
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            try
            {
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                senderName = User.Identity.Name;
                senderName2 = Encrypt.EncryptData(senderName, true, dbConnection);

                var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
                ipaddress = configtSettings.ChatServerUrl + "/signalr";
                var userinfo = Users.GetUserByName(senderName, dbConnection);
                if (userinfo != null)
                {
                    if (userinfo.SiteId > 0)
                    {
                        siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                    }

                    senderName3 = userinfo.CustomerUName;
 
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        userinfoDisplay = "none";
                        userinfoDisplay2 = "none";
                    }
                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        userinfoDisplay2 = "block";
                    }
                    if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        userinfoDisplay2 = "block";
                        if (userinfo.CustomerInfoId > 0)
                        {
                            var gSite = Arrowlabs.Business.Layer.CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                            if (gSite != null)
                            {
                                sourceLat = gSite.Lati;
                                sourceLon = gSite.Long; 
                            }
                        }
                    }
                    else
                    {
                        if (userinfo.SiteId > 0)
                        {
                            var gSite = Arrowlabs.Business.Layer.Site.GetSiteById(userinfo.SiteId, dbConnection);
                            if (gSite != null)
                            {
                                sourceLat = gSite.Lati;
                                sourceLon = gSite.Long; 
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "Page_Load", err, dbConnection);
            }

        }
        [WebMethod]
        public static List<string> getTableData(int id,string uname)
        {
            var listy = new List<string>();
            try
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = new List<Arrowlabs.Business.Layer.Activity>();
                if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                {
                    sessions = Arrowlabs.Business.Layer.Activity.GetAllActivityBySiteId(userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var allusers = new List<Users>();
                        var sessionsX = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var usr in sessionsX)
                        {
                            allusers.Add(usr);
                            var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                            foreach (var subsubuser in getallUsers)
                            {
                                allusers.Add(subsubuser);
                            }
                        }
                        var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                        unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                        foreach (var subsubuser in unassigned)
                        {
                            allusers.Add(subsubuser);
                        }

                        allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                        var grouped = allusers.GroupBy(item => item.ID);
                        allusers = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                        var usersnames = new List<string>();
                        foreach (var u in allusers)
                        {
                            usersnames.Add(u.Username);
                        }
                        sessions = sessions.Where(i => usersnames.Contains(i.CreatedBy)).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.Manager)
                    {
                        var allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);

                        var usersnames = new List<string>();

                        foreach (var u in allusers)
                        {
                            usersnames.Add(u.Username);
                        }

                        sessions = sessions.Where(i => usersnames.Contains(i.CreatedBy)).ToList();
                    }
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    sessions = Arrowlabs.Business.Layer.Activity.GetAllActivityByCId(userinfo.CustomerInfoId, dbConnection);
                else if (userinfo.RoleId == (int)Role.Regional)
                    sessions = Arrowlabs.Business.Layer.Activity.GetAllActivityByLevel5(userinfo.ID, dbConnection);
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                    sessions = Arrowlabs.Business.Layer.Activity.GetAllActivity(dbConnection);


                foreach (var item in sessions)
                {
                    var status = "Pending";
                    if (item.Status == 1)
                        status = "Inprogress";
                    else if (item.Status == 2)
                        status = "Completed";
                    else if (item.Status == 3)
                        status = "Cleared";

                    var actionCheck = "<div  class='nice-checkbox inline-block no-vmargin'><input  onchange='deleteClickOnCheckbox(&apos;" + item.Id + "&apos;)' type='checkbox' id='task" + item.Id.ToString() + "' name='niceCheck'><label style='margin-top:15px' for='task" + item.Id.ToString() + "'></label></div>";
                    var delete = "<a href='#' data-target='#deleteSite2'  data-toggle='modal' onclick='rowchoice2(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    item.CreatedDate = item.CreatedDate.Value.AddHours(userinfo.TimeZone);
                    if (item.StartTime != null)
                    {
                        item.CreatedDate = item.StartTime.Value.AddHours(userinfo.TimeZone);
                        if (item.EndTime != null)
                            item.CreatedDate = item.EndTime.Value.AddHours(userinfo.TimeZone);
                    }
                    if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin )
                    {
                        actionCheck = string.Empty;
                        delete = string.Empty;
                    }
                    var returnstring = "<tr role='row' class='odd'><td>"+actionCheck+"</td><td class='sorting_1'>" + item.Name + "</td><td>" + item.CustomerUName + "</td><td>" + item.CreatedDate.ToString() + "</td><td>" + status + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='rowchoice2(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>"+delete+"</td></tr>";
                    if (item.Status != 4)
                        listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Site", "getTableData", err, dbConnection);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableRowData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = Arrowlabs.Business.Layer.Activity.GetActivityById(id, dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.Name);
                    listy.Add(customData.Description);
                    if (customData.StartTime != null)
                        listy.Add(customData.StartTime.Value.AddHours(userinfo.TimeZone).ToString());
                    else
                        listy.Add("Not started");
                    if (customData.EndTime != null)
                        listy.Add(customData.EndTime.Value.AddHours(userinfo.TimeZone).ToString());
                    else
                        listy.Add("Not finished");
                    listy.Add(customData.CustomerUName);

                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.StartLati, customData.StartLong, getClientLic);
                    if (!string.IsNullOrEmpty(geolocation))
                        listy.Add(geolocation);
                    else
                        listy.Add("Not Updated");

                    var geolocation2 = ReverseGeocode.RetrieveFormatedAddress(customData.EndLati, customData.EndLong, getClientLic);
                    if (!string.IsNullOrEmpty(geolocation2))
                        listy.Add(geolocation2);
                    else
                        listy.Add("Not Updated");

                    listy.Add(customData.ActivityType);
                    listy.Add(customData.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTableRowData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getIncidentLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            json += "[";
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        var item = Arrowlabs.Business.Layer.Activity.GetActivityById(id, dbConnection);
                        if (item != null)
                        {
                            if (!string.IsNullOrEmpty(item.StartLong))
                            {
                                json += "{ \"Username\" : \"Offence\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLong + "\",\"Lat\" : \"" + item.StartLati + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                                if (!string.IsNullOrEmpty(item.EndLong))
                                    json += "{ \"Username\" : \"Offence\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLong + "\",\"Lat\" : \"" + item.EndLati + "\",\"LastLog\" : \"\",\"State\" : \"FINISH\",\"Logs\" : \"Retrieve\"},";
                            }
                            else
                            {
                                json += "{ \"Username\" : \"Offence\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"0\",\"Lat\" : \"0\",\"LastLog\" : \"\",\"State\" : \"NONE\",\"Logs\" : \"Retrieve\"},";
                            }
                            
                            json = json.Substring(0, json.Length - 1);

                        }
                    }
                }
                json += "]";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getIncidentLocationData", err, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static List<string> getTableData2(int id, string uname)
        {
            var listy = new List<string>();
            try
            {
                var gUser = Users.GetUserByEncryptedName(uname, dbConnection);

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(gUser.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                } 
                var sessions = new List<Arrowlabs.Business.Layer.ActivityType>();
                if (gUser.RoleId == (int)Role.Admin || gUser.RoleId == (int)Role.Manager || gUser.RoleId == (int)Role.Director)
                    sessions = Arrowlabs.Business.Layer.ActivityType.GetAllActivityTypeBySiteId(gUser.SiteId, dbConnection);
                else if (gUser.RoleId == (int)Role.CustomerSuperadmin)
                    sessions = Arrowlabs.Business.Layer.ActivityType.GetAllActivityTypeByCId(gUser.CustomerInfoId, dbConnection);
                else if (gUser.RoleId == (int)Role.Regional)
                    sessions = Arrowlabs.Business.Layer.ActivityType.GetAllActivityTypeByLevel5(gUser.ID, dbConnection);
                else if (gUser.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                    sessions = Arrowlabs.Business.Layer.ActivityType.GetAllActivityType(dbConnection);


                foreach (var item in sessions)
                {
                    var action = "<a href='#' data-target='#newSite'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;) '><i class='fa fa-pencil'></i>Edit</a><a href='#' data-target='#deleteSite'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (gUser.RoleId != (int)Role.SuperAdmin && gUser.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (item.CreatedBy != gUser.Username)
                            action = string.Empty;
                    }

                    var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.Name + "</td><td>" + item.CustomerUName + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Site", "getTableData2", err, dbConnection);
            }
            return listy;
        }

        [WebMethod]
        public static string deleteSiteData(int id,string uname)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {

                return "LOGOUT";
            }
            else
            {
                var listy = string.Empty;
                try
                {
                    var gUser = Users.GetUserByEncryptedName(uname, dbConnection);
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(gUser.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }


                    if (id > 0)
                    {
                        if (Arrowlabs.Business.Layer.ActivityType.DeleteActivityTypeById(id, dbConnection))
                        {
                            //Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("Activity Type was deleted with id=" + id, "Activity Type Successfully", new Exception(), dbConnection);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Activity", id.ToString(), id.ToString(), gUser, "Activity DELETE" + id);
                            listy = "SUCCESS";
                        }
                        else
                        {
                            listy = "Error occured while trying to delete";
                        }
                    }
                }
                catch (Exception ex)
                {
                    listy = ex.Message;
                    MIMSLog.MIMSLogSave("Activity", "deleteSiteData", ex, dbConnection);
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteSiteData2(int id, string uname)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {

                return "LOGOUT";
            }
            else
            {
                var listy = string.Empty;
                try
                {
                    var gUser = Users.GetUserByEncryptedName(uname, dbConnection);

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(gUser.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    if (id > 0)
                    {
                        if (Arrowlabs.Business.Layer.Activity.DeleteActivityById(id, dbConnection))
                        {
                            //Arrowlabs.Business.Layer.MIMSLog.MIMSLogSave("Activity Type was deleted with id=" + id, "Activity Type Successfully", new Exception(), dbConnection);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Activity", id.ToString(), id.ToString(), gUser, "Activity DELETE" + id);
                            listy = "SUCCESS";
                        }
                        else
                        {
                            listy = "Error occured while trying to delete";
                        }
                    }
                }
                catch (Exception ex)
                {
                    listy = ex.Message;
                    MIMSLog.MIMSLogSave("Activity", "deleteSiteData2", ex, dbConnection);
                }
                return listy;
            }
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static string insertSiteData(string id,string uname,int sid)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var gNewType = new ActivityType();
                    if (sid > 0)
                    {
                        gNewType = ActivityType.GetActivityTypeById(sid, dbConnection);
                        if (gNewType.Name.ToLower() != id.ToLower())
                        {
                            var getData2 = ActivityType.GetActivityTypeByNameAndCIdAndSiteId(id, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = ActivityType.GetActivityTypeByNameAndCIdAndSiteId(id, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {

                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                    }
                    else
                    {
                        gNewType = ActivityType.GetActivityTypeByNameAndCIdAndSiteId(id, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                        if (userinfo.RoleId == (int)Role.Director)
                        {
                            if (gNewType == null)
                            {
                                gNewType = ActivityType.GetActivityTypeByNameAndCIdAndSiteId(id, userinfo.CustomerInfoId, 0, dbConnection);
                            }
                        }

                        if (gNewType == null)
                        {
                            if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                var getTaskCategorys = ActivityType.GetAllActivityTypeByCId(userinfo.CustomerInfoId, dbConnection);
                                if (getTaskCategorys.Count > 0)
                                {
                                    foreach (var getcat in getTaskCategorys)
                                    {
                                        if (getcat.Name.ToLower() == id.ToLower())
                                            ActivityType.DeleteActivityTypeById(getcat.Id, dbConnection);
                                    }
                                }
                            }
                            gNewType = new ActivityType();
                        }
                        else
                        {
                            return "Name already exists";
                        }
                        //gNewType = ActivityType.GetActivityTypeByName(id, dbConnection);

                        //if (gNewType != null && gNewType.CustomerId == userinfo.CustomerInfoId)
                        //{
                        //    return "Type already exists";
                        //}
                        //else
                        //{
                        //    gNewType = new ActivityType();
                        //}
                    }
                    gNewType.Name = id;
                    gNewType.SiteId = userinfo.SiteId;
                    gNewType.UpdatedBy = userinfo.Username;
                    gNewType.UpdatedDate = DateTime.UtcNow;
                    gNewType.CreatedDate = DateTime.UtcNow;
                    gNewType.CreatedBy = userinfo.Username;
                    gNewType.CustomerId = userinfo.CustomerInfoId;
                    ActivityType.InsertorUpdateActivityType(gNewType, dbConnection, dbConnectionAudit, true);

                    var gAllActs = Arrowlabs.Business.Layer.Activity.GetAllActivity(dbConnection);
                    gAllActs = gAllActs.Where(i => i.ActivityTypeId == sid).ToList();
                    foreach (var act in gAllActs)
                    {
                        act.ActivityType = id;
                        Arrowlabs.Business.Layer.Activity.InsertorUpdateActivity(act, dbConnection, dbConnectionAudit, true);
                    }

                    return "SUCCESS";
                }
                catch (Exception ex)
                {
                    listy = ex.Message;
                    MIMSLog.MIMSLogSave("Site", "insertSiteData", ex, dbConnection);
                }
                return listy;
            }
        }
        [WebMethod]
        public static List<string> handleTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    { 
                        listy.Add("LOGOUT");
                        return listy;
                    }


                    var cusData = Arrowlabs.Business.Layer.Activity.GetActivityById(id, dbConnection);
                    cusData.Status = 4;
                    Arrowlabs.Business.Layer.Activity.InsertorUpdateActivity(cusData, dbConnection, dbConnectionAudit, true);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Activity", id.ToString(), id.ToString(), userinfo, "Handled Activity" + id);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Activity", "handleTicket", ex, dbConnection, userinfo.SiteId);
                    listy.Add(ex.Message);
                }
                listy.Add("SUCCESS");

            }
            return listy;
        }
        //User Profile
        [WebMethod]
        public static string changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }
                
                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = DateTime.Now;
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return 0; 
            }

            if (userinfo.ID > 0)
            {
                var getuser = userinfo;
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = DateTime.Now,
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = DateTime.Now,
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                            
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    return userinfo.ID;
                }
                else
                    return 0;
            }
            return userinfo.ID;
        }
        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.Username);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.Username);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                //    var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    if (customData.RoleId != (int)Role.SuperAdmin)
                    {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }
                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Surveillance", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }
    }
}