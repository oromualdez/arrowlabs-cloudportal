﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class MimsConfigSettingAudit
    {
        public BaseAudit BaseAudit { get; set; }        
        public int Id { get; set; }        
        public string MIMSServerAddress { get; set; }        
        public string MIMSMobileAddress { get; set; }        
        public float MIMSMobileGPSInterval { get; set; }        
        public DateTime? CreatedDate { get; set; }        
        public DateTime? UpdatedDate { get; set; }        
        public string CreatedBy { get; set; }        
        public string UpdatedBy { get; set; }        
        public int MaxUploadSize { get; set; }        
        public int MaxNumberAttachments { get; set; }        
        public string FilePath { get; set; }        
        public int LiveVideoFrameWidth { get; set; }        
        public int LiveVideoFrameHeight { get; set; }        
        public string UploadServiceUrl { get; set; }

        public static bool InsertMimsConfigSettingAudit(MimsConfigSettingAudit configSetting, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(configSetting.BaseAudit, dbConnection);

            if (configSetting != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMimsConfigSettingAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = configSetting.Id;
                    cmd.Parameters.Add("@MIMSServerAddress", SqlDbType.NVarChar).Value = configSetting.MIMSServerAddress;
                    cmd.Parameters.Add("@MIMSMobileAddress", SqlDbType.NVarChar).Value = configSetting.MIMSMobileAddress;
                    cmd.Parameters.Add("@MIMSMobileGPSInterval", SqlDbType.Float).Value = configSetting.MIMSMobileGPSInterval;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = configSetting.CreatedDate;
                    //cmd.Parameters.Add("@UpdatedDate", SqlDbType.DateTime).Value = configSetting.UpdatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = configSetting.CreatedBy;
                    //cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar).Value = configSetting.UpdatedBy;
                    cmd.Parameters.Add("@MaxUploadSize", SqlDbType.Int).Value = configSetting.MaxUploadSize;
                    cmd.Parameters.Add("@MaxNumberAttachments", SqlDbType.Int).Value = configSetting.MaxNumberAttachments;
                    cmd.Parameters.Add("@FilePath", SqlDbType.NVarChar).Value = configSetting.FilePath;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
        
                }
                return true;
            }
            return false;
        }
    }
}
