﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for PostOrderPDFUpload
    /// </summary>
    public class PostOrderPDFUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                //string ImeTvrtke = context.Request["Data"];
                //if (!string.IsNullOrEmpty(ImeTvrtke))
                //{
                    //var uname = ImeTvrtke.Split('"')[3];
                    //var siteId = Users.GetUserByName(uname, CommonUtility.dbConnection).SiteId;
                    if (context.Request.Files.Count > 0)
                    {
                        HttpFileCollection files = context.Request.Files;
                        var mimcon = MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection);
                        var consplit = mimcon.FilePath.Split('\\');
                        var newstring = string.Empty;
                        var count = 0;
                        foreach (var split in consplit)
                        {
                            if (count < 4)
                            {
                                newstring += split + "\\";
                                count++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        HttpPostedFile file = files[0];
                        newstring = newstring + "Uploads\\PostOrders\\" + Guid.NewGuid().ToString() + ".pdf";
                        file.SaveAs(newstring);
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(newstring);
                    }
                //}
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("PostOrderPDFUpload", "ProcessRequest", ex, CommonUtility.dbConnection);
                context.Response.Write("FAIL");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}