﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
  public  class ChatUser
  {
      public static string arrowlabsKey = "ARL";
      public int Id { get; set; }
      public int UserId { get; set; }
      public int LocationId { get; set; }
      public string UserName { get; set; }
      public string ConnectionId { get; set; }
      public int Active { get; set; }
      public int DeviceType { get; set; }
      public DateTime? CreatedDate { get; set; }
      public DateTime? LoginDate { get; set; }
      public DateTime? LogoutDate { get; set; }
      public string UpdatedBy { get; set; }
      public int SiteId { get; set; }

      public static bool ValidLicense(string dbConnection)
      {
          var licenseInfo = ClientLicence.GetLicenseByClientID(arrowlabsKey, dbConnection);

          var users = GetAllChatUsersByDeviceType(dbConnection);

          if (licenseInfo != null)
          {
              if (users <= Convert.ToInt32(licenseInfo.TotalMobileUsers))
              {
                  //var remaining = (Convert.ToInt32(licenseInfo.TotalMobileUsers) - users).ToString();

                  //licenseInfo.RemainingMobileUsers = remaining;
                  //licenseInfo.UsedMobileUsers = users.ToString();

                  //ClientLicence.InsertClientLicence(licenseInfo, dbConnection);
                  return true;
              }
          }
       
          return false;
      }
      
      private static ChatUser GetChatUserFromSqlReader(SqlDataReader reader)
      {
          var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
          var chatUser = new ChatUser();
          if (reader["Id"] != DBNull.Value)
              chatUser.Id = Convert.ToInt32(reader["Id"].ToString());
          if (reader["LocationId"] != DBNull.Value)
              chatUser.LocationId = Convert.ToInt32(reader["LocationId"].ToString());
          if (reader["UserName"] != DBNull.Value)
              chatUser.UserName = reader["UserName"].ToString();
          if (reader["ConnectionId"] != DBNull.Value)
              chatUser.ConnectionId = reader["ConnectionId"].ToString();
          if (reader["Active"] != DBNull.Value)
              chatUser.Active = Convert.ToInt32(reader["Active"].ToString());
          if (reader["CreatedDate"] != DBNull.Value)
              chatUser.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
          if (reader["LoginDate"] != DBNull.Value)
              chatUser.LoginDate = Convert.ToDateTime(reader["LoginDate"].ToString());
          if (reader["LogoutDate"] != DBNull.Value)
              chatUser.LogoutDate = Convert.ToDateTime(reader["LogoutDate"].ToString());
          if (reader["Type"] != DBNull.Value)
              chatUser.DeviceType = Convert.ToInt16(reader["Type"].ToString());
          if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
              chatUser.SiteId = Convert.ToInt32(reader["SiteId"].ToString());


          

          return chatUser;
      }
      public static List<ChatUser> GetAllChatUsersByGroupId(int locationId, string dbConnection)
      {
          var chatUserList = new List<ChatUser>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();

              var sqlCommand = new SqlCommand("GetAllChatUsersByGroupId", connection);
              sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
              sqlCommand.Parameters["@LocationId"].Value = locationId;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              var reader = sqlCommand.ExecuteReader();

              while (reader.Read())
              {
                  var chatUser = GetChatUserFromSqlReader(reader);
                  chatUserList.Add(chatUser);
              }
              connection.Close();
              reader.Close();
          }
          return chatUserList;
      }


      public static List<ChatUser> GetAllChatUsers( bool active,string dbConnection)
      {
          var chatUserList = new List<ChatUser>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();

              var sqlCommand = new SqlCommand("GetAllChatUsers", connection);
              sqlCommand.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit));
              sqlCommand.Parameters["@Active"].Value = active;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              var reader = sqlCommand.ExecuteReader();

              while (reader.Read())
              {
                  var chatUser = GetChatUserFromSqlReader(reader);
                  chatUserList.Add(chatUser);
              }
              connection.Close();
              reader.Close();
          }
          return chatUserList;
      }

      public static List<ChatUser> GetAllNotificationUsers(string dbConnection)
      {
          var chatUserList = new List<ChatUser>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();

              var sqlCommand = new SqlCommand("GetAllNotificationUsers", connection);
             
              sqlCommand.CommandType = CommandType.StoredProcedure;
              var reader = sqlCommand.ExecuteReader();

              while (reader.Read())
              {
                  var chatUser = GetChatUserFromSqlReader(reader);
                  if (reader["UserId"] != DBNull.Value)
                      chatUser.UserId = Convert.ToInt32(reader["UserId"].ToString());
                  chatUserList.Add(chatUser);
              }
              connection.Close();
              reader.Close();
          }
          return chatUserList;
      }

      public static ChatUser GetChatUserByName(string name, string dbConnection)
      {
          ChatUser chatUser = null;
       
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetChatUserByName", connection);

              sqlCommand.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
              sqlCommand.Parameters["@UserName"].Value = name;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              var reader = sqlCommand.ExecuteReader();

              while (reader.Read())
              {
                  chatUser = GetChatUserFromSqlReader(reader);
              }
              connection.Close();
              reader.Close();
          }
          
          return chatUser;
      }

      public static List<Users> GetAllChatUserByStatus(int status,string dbConnection)
      {
          var chatUserList = new List<Users>();
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();

              var sqlCommand = new SqlCommand("GetAllChatUserByStatus", connection);

              sqlCommand.Parameters.Add(new SqlParameter("@status", SqlDbType.Int));
              sqlCommand.Parameters["@status"].Value = status;

              sqlCommand.CommandType = CommandType.StoredProcedure;
              var reader = sqlCommand.ExecuteReader();

              while (reader.Read())
              {
                  var user = Users.GetFullAccountUserFromSqlReader(reader, dbConnection);
                  chatUserList.Add(user);
              }
              connection.Close();
              reader.Close();
          }
          return chatUserList;
      }

      public static bool InsertorUpdateChatUser(ChatUser chatUser, string dbConnection,
      string auditDbConnection = "", bool isAuditEnabled = true)
      {
          int id = 0;

          var action = (chatUser.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

          if (chatUser != null)
          {
              using (var connection = new SqlConnection(dbConnection))
              {
                  connection.Open();
                  var cmd = new SqlCommand("InsertorUpdateChatUser", connection);

                  cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                  cmd.Parameters["@Id"].Value = chatUser.Id;

                  cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                  cmd.Parameters["@UserName"].Value = chatUser.UserName;

                  cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                  cmd.Parameters["@CreatedDate"].Value = chatUser.CreatedDate;

                  cmd.Parameters.Add(new SqlParameter("@LoginDate", SqlDbType.DateTime));
                  cmd.Parameters["@LoginDate"].Value = chatUser.LoginDate;

                  cmd.Parameters.Add(new SqlParameter("@LogoutDate", SqlDbType.DateTime));
                  cmd.Parameters["@LogoutDate"].Value = chatUser.LogoutDate;

                  cmd.Parameters.Add(new SqlParameter("@ConnectionId", SqlDbType.NVarChar));
                  cmd.Parameters["@ConnectionId"].Value = chatUser.ConnectionId;
                  
                  cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Int));
                  cmd.Parameters["@Active"].Value = chatUser.Active;

                  cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                  cmd.Parameters["@LocationId"].Value = chatUser.LocationId;

                  cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                  cmd.Parameters["@Type"].Value = chatUser.DeviceType;
                  
                  cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                  cmd.Parameters["@SiteId"].Value = chatUser.SiteId;

                  SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                  returnParameter.Direction = ParameterDirection.ReturnValue;

                  cmd.CommandType = CommandType.StoredProcedure;
                  cmd.ExecuteNonQuery();

                  id = (int)returnParameter.Value;

                  try
                  {
                      if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                      {
                          var audit = new ChatUserAudit();
                          audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, chatUser.UpdatedBy);
                          Reflection.CopyProperties(chatUser, audit);
                          ChatUserAudit.InsertChatUserAudit(audit, auditDbConnection);
                      }
                  }
                  catch (Exception ex)
                  {
                      MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateChatUser", ex, dbConnection);
                  }
              }
          }
          return true;
      }

      public static bool UpdateChatUserStatus(string userName, int active, string dbConnection)
      {

          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var cmd = new SqlCommand("UpdateChatUserStatus", connection);

              cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
              cmd.Parameters["@UserName"].Value = userName;

              cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Int));
              cmd.Parameters["@Active"].Value = active;

              cmd.CommandType = CommandType.StoredProcedure;
              cmd.ExecuteNonQuery();
          }

          return true;
      }
      public static int GetAllChatUsersByDeviceType(string dbConnection)
      {
          var onlineUsers = 0;
       
          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var sqlCommand = new SqlCommand("GetAllChatUsersByDeviceType", connection);
              sqlCommand.CommandType = CommandType.StoredProcedure;
              onlineUsers = (Int32)sqlCommand.ExecuteScalar();
              connection.Close();
          }
          return onlineUsers;
      }
      public static bool UpdateAllChatUserStatus(string dbConnection)
      {

          using (var connection = new SqlConnection(dbConnection))
          {
              connection.Open();
              var cmd = new SqlCommand("UpdateAllChatUserStatus", connection);

              cmd.CommandType = CommandType.StoredProcedure;
              cmd.ExecuteNonQuery();
          }

          return true;
      }

    }
}
