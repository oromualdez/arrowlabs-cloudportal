﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
   public class DeviceGroup
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string DeviceId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int SiteId { get; set; }

        public string UpdatedBy { get; set; }


        public static bool InsertorUpdateDeviceGroup(DeviceGroup deviceGroup, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (deviceGroup != null)
            {
                int id = deviceGroup.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateDeviceGroup", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = deviceGroup.Id;

                    cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                    cmd.Parameters["@GroupId"].Value = deviceGroup.GroupId;

                    cmd.Parameters.Add(new SqlParameter("@DeviceId", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceId"].Value = deviceGroup.DeviceId;

                    cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreateDate"].Value = deviceGroup.CreateDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = deviceGroup.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = deviceGroup.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = deviceGroup.SiteId;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateDeviceGroup";

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            deviceGroup.Id = id;
                            var audit = new DeviceGroupAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, deviceGroup.UpdatedBy);
                            Reflection.CopyProperties(deviceGroup, audit);
                            DeviceGroupAudit.InsertDeviceGroupAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertDeviceGroupAudit", ex, dbConnection);
                    }

                }
            }
            return true;
        }

        public static bool DeleteDeviceGroupByGroupIdAndDeviceId(int groupId, string deviceId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteDeviceGroupByGroupIdAndDeviceId", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.Parameters.Add(new SqlParameter("@DeviceId", SqlDbType.NVarChar));
                cmd.Parameters["@DeviceId"].Value = deviceId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDeviceGroupByGroupIdAndDeviceId";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteDeviceGroupByGroupId(int groupId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteDeviceGroupByGroupId", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDeviceGroupByGroupId";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteDeviceGroupByDeviceId(string deviceId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteDeviceGroupByDeviceId", connection);

                cmd.Parameters.Add(new SqlParameter("@DeviceId", SqlDbType.NVarChar));
                cmd.Parameters["@DeviceId"].Value = deviceId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDeviceGroupByDeviceId";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
