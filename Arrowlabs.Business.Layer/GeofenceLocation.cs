﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
  public  class GeofenceLocation
    {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public int IncidentId { get; set; }
        public double Latitude{ get; set; }
        public double Longitude { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LocationDesc { get; set; }
        public string IncidentName { get; set; }

        public int SiteId { get; set; }
        public int CustomerId { get; set; }

        private static GeofenceLocation GetGeofenceLocationFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var fence = new GeofenceLocation();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                fence.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("LocationId") && reader["LocationId"] != DBNull.Value)
                fence.LocationId = Convert.ToInt32(reader["LocationId"].ToString());
            if (columns.Contains("IncidentId") && reader["IncidentId"] != DBNull.Value)
                fence.IncidentId = Convert.ToInt32(reader["IncidentId"].ToString());
            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                fence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                fence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                fence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("UpdatedDate") && reader["UpdatedDate"] != DBNull.Value)
                fence.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                fence.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("LocationDesc") && reader["LocationDesc"] != DBNull.Value)
                fence.LocationDesc = reader["LocationDesc"].ToString();
            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                fence.IncidentName = reader["Name"].ToString();
          

            return fence;
        }
        public static bool DeleteGeofenceLocationbyLocationId(int locationId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteGeofenceLocation", connection);

                cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                cmd.Parameters["@LocationId"].Value = locationId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteGeofenceLocation";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static IList<GeofenceLocation> GetAllGeofencePointsByLocation(string dbConnection)
        {
            List<GeofenceLocation> fencetList = new List<GeofenceLocation>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("GetAllGeofencePointsByLocation", connection);
               
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "GetAllGeofencePointsByLocation";

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var fence = GetGeofenceLocationFromSqlReader(reader);
                    fencetList.Add(fence);
                }
                connection.Close();
                reader.Close();
            }
            return fencetList;
        }

        public static IList<GeofenceLocation> GetAllGeofencePointsByIncident(string dbConnection)
        {
            List<GeofenceLocation> fencetList = new List<GeofenceLocation>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("GetAllGeofencePointsByIncident", connection);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "GetAllGeofencePointsByIncident";

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var fence = GetGeofenceLocationFromSqlReader(reader);
                    fencetList.Add(fence);
                }
                connection.Close();
                reader.Close();
            }
            return fencetList;
        }


        public static bool DeleteGeofenceLocationbyIncidentId(int incidentId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteGeofenceLocationbyIncidentId", connection);

                cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                cmd.Parameters["@IncidentId"].Value = incidentId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteGeofenceLocationbyIncidentId";

               
            }
            return true;
        }
        public static List<GeofenceLocation> GetAllGeofenceLocationbyLocationId(string dbConnection, int locationId)
        {
            var fencetList = new List<GeofenceLocation>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllGeofenceLocationbyLocationId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                sqlCommand.Parameters["@LocationId"].Value = locationId;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var fence = GetGeofenceLocationFromSqlReader(reader);
                    fencetList.Add(fence);
                }
                connection.Close();
                reader.Close();
            }
            return fencetList;
        }
        public static List<GeofenceLocation> GetAllGeofenceLocationbyIncidentId(string dbConnection, int incidentId)
        {
            var fencetList = new List<GeofenceLocation>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllGeofenceLocationbyIncidentId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                sqlCommand.Parameters["@IncidentId"].Value = incidentId;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var fence = GetGeofenceLocationFromSqlReader(reader);
                    fencetList.Add(fence);
                }
                connection.Close();
                reader.Close();
            }
            return fencetList;
        }
        public static bool InsertOrUpdateGeofenceLocation(GeofenceLocation fenceLocation, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (fenceLocation.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (fenceLocation != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateGeofenceLocation", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = fenceLocation.Id;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = fenceLocation.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                    cmd.Parameters["@IncidentId"].Value = fenceLocation.IncidentId;
                    
                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = fenceLocation.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = fenceLocation.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = fenceLocation.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = fenceLocation.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = fenceLocation.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = fenceLocation.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = fenceLocation.CustomerId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            fenceLocation.Id = id;
                            var audit = new GeofenceLocationAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, fenceLocation.CreatedBy);
                            Reflection.CopyProperties(fenceLocation, audit);
                            GeofenceLocationAudit.InsertOrUpdateGeofenceLocation(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Audit Business Layer", "InsertOrUpdateGeofenceLocation", ex, dbConnection);
                    }
                }
            }
            return true;
        }
    }
}
