﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class MilestoneCameraAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public Guid Id { get; set; }
        public string CameraName { get; set; }

        public string ServerIP { get; set; }

        public int Port { get; set; }

        public string Password { get; set; }

        public string Username { get; set; }

        public string CamDetails { get; set; }

        public int AuthenticationType { get; set; }
        public int SiteId { get; set; }
        public static bool InsertorUpdateMilestoneCamera(MilestoneCameraAudit milestoneCamera, string dbConnection)
        {
            if (milestoneCamera != null)
            {
                var baseAuditId = BaseAudit.InsertBaseAudit(milestoneCamera.BaseAudit, dbConnection);

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMilestoneCamera", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Id"].Value = milestoneCamera.Id;

                    cmd.Parameters.Add(new SqlParameter("@CameraName", SqlDbType.NVarChar));
                    cmd.Parameters["@CameraName"].Value = milestoneCamera.CameraName;

                    cmd.Parameters.Add(new SqlParameter("@ServerIP", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerIP"].Value = milestoneCamera.ServerIP;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = milestoneCamera.Port;

                    cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                    cmd.Parameters["@Password"].Value = milestoneCamera.Password;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = milestoneCamera.Username;

                    cmd.Parameters.Add(new SqlParameter("@AuthenticationType", SqlDbType.Int));
                    cmd.Parameters["@AuthenticationType"].Value = milestoneCamera.AuthenticationType;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = milestoneCamera.SiteId;


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
