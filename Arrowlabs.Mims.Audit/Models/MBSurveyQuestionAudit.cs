﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Mims.Audit.Models
{
    public class MBSurveyQuestionAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public bool Active { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public string Question { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CustomerUName { get; set; }
        public int Green { get; set; }
        public int Yellow { get; set; }
        public int Red { get; set; }

        public static int InserMBSurveyQuestionAudit(MBSurveyQuestionAudit notification, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(notification.BaseAudit, dbConnection);

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InserMBSurveyQuestionAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@Question", SqlDbType.NVarChar));
                    cmd.Parameters["@Question"].Value = notification.Question;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit));
                    cmd.Parameters["@Active"].Value = notification.Active;

                    cmd.Parameters.Add(new SqlParameter("@Green", SqlDbType.Int));
                    cmd.Parameters["@Green"].Value = notification.Green;

                    cmd.Parameters.Add(new SqlParameter("@Yellow", SqlDbType.Int));
                    cmd.Parameters["@Yellow"].Value = notification.Yellow;

                    cmd.Parameters.Add(new SqlParameter("@Red", SqlDbType.Int));
                    cmd.Parameters["@Red"].Value = notification.Red;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateMBSurveyQuestion";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                }
            }
            return 0;
        } 
    }
}
