﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class MobileHotEventCategory
    {
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public string Url { get; set; }
        public string CustomerUName { get; set; }

        public static void DeleteMobileHotEventCategory(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMobileHotEventCategory", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMobileHotEventCategory";

                cmd.ExecuteNonQuery();
            }
        }

        private static MobileHotEventCategory GetMobileHotEventCategoryFromSqlReader(SqlDataReader reader)
        {
            var hotEventCategory = new MobileHotEventCategory();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["CategoryId"] != DBNull.Value)
                hotEventCategory.CategoryId = Convert.ToInt32(reader["CategoryId"].ToString());
            if (reader["Description"] != DBNull.Value)
                hotEventCategory.Description = reader["Description"].ToString();
            if (reader["CreatedBy"] != DBNull.Value)
                hotEventCategory.CreatedBy = reader["CreatedBy"].ToString().ToLower();
            if (reader["CreatedDate"] != DBNull.Value)
                hotEventCategory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (reader["UpdatedBy"] != DBNull.Value)
                hotEventCategory.UpdatedBy = reader["UpdatedBy"].ToString().ToLower();
            if (reader["UpdatedDate"] != DBNull.Value)
                hotEventCategory.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
            if (reader["IsActive"] != DBNull.Value)
                hotEventCategory.IsActive = Convert.ToBoolean(reader["IsActive"]);

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                hotEventCategory.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                hotEventCategory.CustomerId = Convert.ToInt32(reader["CustomerId"]);

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                hotEventCategory.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("Url") && reader["Url"] != DBNull.Value)
                hotEventCategory.Url = reader["Url"].ToString();
             
            return hotEventCategory;
        }
        public static List<MobileHotEventCategory> GetAllMobileHotEventCategories(string dbConnection)
        {
            var hotEventCategories = new List<MobileHotEventCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllMobileHotEventCategories";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetMobileHotEventCategoryFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }
        public static bool InsertOrUpdateMobileHotEventCategory(MobileHotEventCategory mobileHotEventCategory,
            string dbConnection, string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (mobileHotEventCategory != null)
            {
                int id = mobileHotEventCategory.CategoryId;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMobileHotEventCategory", connection);

                    cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = mobileHotEventCategory.CategoryId;
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = mobileHotEventCategory.Description;
                    cmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = mobileHotEventCategory.IsActive;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = mobileHotEventCategory.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = mobileHotEventCategory.CreatedBy;
                    cmd.Parameters.Add("@UpdatedDate", SqlDbType.DateTime).Value = mobileHotEventCategory.UpdatedDate;
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar).Value = mobileHotEventCategory.UpdatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = mobileHotEventCategory.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = mobileHotEventCategory.CustomerId;
                    cmd.Parameters.Add("@Url", SqlDbType.NVarChar).Value = mobileHotEventCategory.Url;
                     
                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            mobileHotEventCategory.CategoryId = id;
                            var audit = new MobileHotEventCategoryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, mobileHotEventCategory.UpdatedBy);
                            Reflection.CopyProperties(mobileHotEventCategory, audit);
                            MobileHotEventCategoryAudit.InsertMobileHotEventCategoryAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateMobileHotEventCategory", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static List<MobileHotEventCategory> GetAllMobileHotEventCategoriesByCId(int id,string dbConnection)
        {
            var hotEventCategories = new List<MobileHotEventCategory>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllMobileHotEventCategoriesByCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.NVarChar));

                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandText = "GetAllMobileHotEventCategoriesByCId";

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetMobileHotEventCategoryFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }


        public static List<MobileHotEventCategory> GetAllMobileHotEventCategoriesByLevel5(int id, string dbConnection)
        {
            var hotEventCategories = new List<MobileHotEventCategory>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllMobileHotEventCategoriesByLevel5", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)); 
                sqlCommand.Parameters["@UserId"].Value = id;
                sqlCommand.CommandText = "GetAllMobileHotEventCategoriesByLevel5";

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetMobileHotEventCategoryFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }

        public static List<MobileHotEventCategory> GetAllMobileHotEventCategoriesBySiteId(int id, string dbConnection)
        {
            var hotEventCategories = new List<MobileHotEventCategory>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllMobileHotEventCategoriesBySiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.NVarChar));

                sqlCommand.Parameters["@SiteId"].Value = id;
                sqlCommand.CommandText = "GetAllMobileHotEventCategoriesBySiteId";

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetMobileHotEventCategoryFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }

        public static MobileHotEventCategory GetMobileHotEventCategoryByName(string name, string dbConnection)
        {
            MobileHotEventCategory taskCheckListItem = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetMobileHotEventCategoryByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    taskCheckListItem = GetMobileHotEventCategoryFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return taskCheckListItem;
        }

        public static MobileHotEventCategory GetMobileHotEventCategoryByNameAndCId(string name,int id ,string dbConnection)
        {
            MobileHotEventCategory taskCheckListItem = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetMobileHotEventCategoryByNameAndCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    taskCheckListItem = GetMobileHotEventCategoryFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return taskCheckListItem;
        }

        public static MobileHotEventCategory GetMobileHotEventCategoryByNameAndCIdAndSiteId(string name, int id,int siteid ,string dbConnection)
        {
            MobileHotEventCategory taskCheckListItem = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetMobileHotEventCategoryByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    taskCheckListItem = GetMobileHotEventCategoryFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return taskCheckListItem;
        }
    }
}
