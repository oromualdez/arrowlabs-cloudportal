﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class Offence
    {
        public int ID { get; set; }
        public string OffenceDesc { get; set; }
        public string OffenceDesc_AR { get; set; }
        public int OffenceTypeID { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DatabaseOperation Operation { get; set; }
        public static  List<OffenceType> OffenceTypeLists { get; set; }
        public string OffenceName { get; set; }
        public List<OffenceType> OffenceTypeList { get; set; }
        public int AccountId { get; set; }
        public int CustomerId { get; set; }
        public string AccountName { get; set; }
        public string UpdatedBy { get; set; }
        public string OffenceValue { get; set; }

        public int SiteId { get; set; }

        public static  void OffenceTypes(string dbConnection)
        {
            OffenceTypeLists = OffenceType.GetAllOffenceType(dbConnection);
        }

        public static List<Offence> GetAllOffence(string dbConnection)
        {
            var offences = new List<Offence>();
            OffenceTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffence", connection);


                sqlCommand.CommandText = "GetAllOffence";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList(); 
                while (reader.Read())
                {
                    var offence = new Offence();

                    offence.ID = Convert.ToInt32(reader["Id"].ToString());
                    offence.OffenceDesc = reader["OFFENCEDESC"].ToString();
                    offence.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
                    offence.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offence.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                    offence.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offence.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    offence.OffenceName = OffenceTypeName(offence.OffenceTypeID);
                    offence.OffenceTypeList = OffenceTypeLists;

                    if (columns.Contains( "OffenceValue"))
                        offence.OffenceValue = reader["OffenceValue"].ToString();

                    if (columns.Contains( "AccountId"))
                        offence.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("CustomerId"))
                        offence.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("SiteId"))
                        offence.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains( "AccountName"))
                        offence.AccountName = reader["AccountName"].ToString();

                    offences.Add(offence);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static List<Offence> GetAllOffenceByOffenceTypeId(int id, string dbConnection)
        {
            var offences = new List<Offence>();
            OffenceTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceByOffenceTypeId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offence = new Offence();

                    offence.ID = Convert.ToInt32(reader["Id"].ToString());
                    offence.OffenceDesc = reader["OFFENCEDESC"].ToString();
                    offence.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
                    offence.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offence.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                    offence.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offence.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    offence.OffenceName = OffenceTypeName(offence.OffenceTypeID);
                    offence.OffenceTypeList = OffenceTypeLists;

                    if (columns.Contains("OffenceValue"))
                        offence.OffenceValue = reader["OffenceValue"].ToString();

                    if (columns.Contains("AccountId"))
                        offence.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("AccountName"))
                        offence.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("CustomerId"))
                        offence.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("SiteId"))
                        offence.SiteId = Convert.ToInt32(reader["SiteId"]);

                    offences.Add(offence);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static List<Offence> GetAllOffenceByCId(int id,string dbConnection)
        {
            var offences = new List<Offence>();
            OffenceTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;  
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList(); 
                while (reader.Read())
                {
                    var offence = new Offence();

                    offence.ID = Convert.ToInt32(reader["Id"].ToString());
                    offence.OffenceDesc = reader["OFFENCEDESC"].ToString();
                    offence.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
                    offence.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offence.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                    offence.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offence.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    offence.OffenceName = OffenceTypeName(offence.OffenceTypeID);
                    offence.OffenceTypeList = OffenceTypeLists;

                    if (columns.Contains( "OffenceValue"))
                        offence.OffenceValue = reader["OffenceValue"].ToString();

                    if (columns.Contains( "AccountId"))
                        offence.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        offence.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("CustomerId"))
                        offence.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("SiteId"))
                        offence.SiteId = Convert.ToInt32(reader["SiteId"]);

                    offences.Add(offence);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static List<Offence> GetAllOffenceBySiteId(int id, string dbConnection)
        {
            var offences = new List<Offence>();
            OffenceTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offence = new Offence();

                    offence.ID = Convert.ToInt32(reader["Id"].ToString());
                    offence.OffenceDesc = reader["OFFENCEDESC"].ToString();
                    offence.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
                    offence.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offence.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                    offence.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offence.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    offence.OffenceName = OffenceTypeName(offence.OffenceTypeID);
                    offence.OffenceTypeList = OffenceTypeLists;

                    if (columns.Contains("OffenceValue"))
                        offence.OffenceValue = reader["OffenceValue"].ToString();

                    if (columns.Contains("AccountId"))
                        offence.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("AccountName"))
                        offence.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("CustomerId"))
                        offence.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("SiteId"))
                        offence.SiteId = Convert.ToInt32(reader["SiteId"]);

                    offences.Add(offence);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static List<Offence> GetAllOffenceByLevel5(int id, string dbConnection)
        {
            var offences = new List<Offence>();
            OffenceTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offence = new Offence();

                    offence.ID = Convert.ToInt32(reader["Id"].ToString());
                    offence.OffenceDesc = reader["OFFENCEDESC"].ToString();
                    offence.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
                    offence.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offence.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                    offence.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offence.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    offence.OffenceName = OffenceTypeName(offence.OffenceTypeID);
                    offence.OffenceTypeList = OffenceTypeLists;

                    if (columns.Contains("OffenceValue"))
                        offence.OffenceValue = reader["OffenceValue"].ToString();

                    if (columns.Contains("AccountId"))
                        offence.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("AccountName"))
                        offence.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("CustomerId"))
                        offence.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("SiteId"))
                        offence.SiteId = Convert.ToInt32(reader["SiteId"]);

                    offences.Add(offence);
                }
                connection.Close();
                reader.Close();
            }
            return offences;
        }

        public static string OffenceTypeName(int OffenceTypeId)
        {
            foreach (var offType in OffenceTypeLists)
            {
                if (offType.OffenceTypeId == OffenceTypeId)
                    return offType.OffenceTypeDesc;
            }
            return String.Empty;
        }
        public static List<Offence> GetAllOffenceByDate(DateTime updatedDate, string dbConnection)
        {
            try
            {
                var offences = new List<Offence>();
                var vehicleColorAudits = Offence_Audit.GetAllOffence_AuditByDate(updatedDate, dbConnection);
                DataSet dataset = new DataSet();
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("Id");
                foreach (var vehicleColorAudit in vehicleColorAudits)
                {
                    if (vehicleColorAudit.Operation == "Updated" || vehicleColorAudit.Operation == "Inserted")
                    {
                        var dr = dataTable.NewRow();
                        dr["Id"] = vehicleColorAudit.Id;
                        dataTable.Rows.Add(dr);
                    }
                    else
                    {
                        var offence = new Offence();
                        offence.ID = vehicleColorAudit.Id;
                        offence.OffenceDesc = DatabaseOperation.Unknown.ToString();
                        offence.OffenceDesc_AR = DatabaseOperation.Unknown.ToString();
                        offence.OffenceTypeID = (int)DatabaseOperation.Unknown;
                        offence.CreateDate = DateTime.Now;
                        offence.UpdatedDate = DateTime.Now;
                        offence.CreatedBy = DatabaseOperation.Unknown.ToString();
                        offence.Operation = DatabaseOperation.Deleted;
                        offences.Add(offence);
                    }
                }
                if (dataTable.Rows.Count > 0)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var sqlCommand = new SqlCommand("GetAllOffenceByDate", connection);
                        sqlCommand.Parameters.AddWithValue("@Offences", dataTable);
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.CommandText = "GetAllOffenceByDate";

                        var reader = sqlCommand.ExecuteReader();
                        var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                        while (reader.Read())
                        {
                            var offence = new Offence();

                            offence.ID = Convert.ToInt32(reader["Id"].ToString());
                            offence.OffenceDesc = reader["OFFENCEDESC"].ToString();
                            offence.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
                            offence.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                            offence.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                            offence.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                            offence.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                            if (columns.Contains( "OffenceValue"))
                                offence.OffenceValue = reader["OffenceValue"].ToString();

                            offence.Operation = DatabaseOperation.Updated;

                            offences.Add(offence);
                        }
                        connection.Close();
                        reader.Close();
                    }
                }
                return offences;
            }
            catch (Exception exp)
            { }
            return null;
        }
         

        public static int InsertorUpdateOffence(Offence offence, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {

            var id = 0;

            if (offence != null)
            {
                id = offence.ID;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateOffence", connection);

                    cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int));
                    cmd.Parameters["@ID"].Value = offence.ID;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = offence.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = offence.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@OffenceDesc", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceDesc"].Value = offence.OffenceDesc;

                    cmd.Parameters.Add(new SqlParameter("@OffenceDesc_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceDesc_AR"].Value = offence.OffenceDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@OffenceTypeID", SqlDbType.Int));
                    cmd.Parameters["@OffenceTypeID"].Value = offence.OffenceTypeID;

                    cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreateDate"].Value = offence.CreateDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = offence.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = offence.CreatedBy.ToLower();

                    cmd.Parameters.Add(new SqlParameter("@OffenceValue", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceValue"].Value = offence.OffenceValue;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateOffence";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            offence.ID = id;
                            var audit = new OffenceAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, offence.UpdatedBy);
                            Reflection.CopyProperties(offence, audit);
                            OffenceAudit.InsertOffenceAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateOffence", ex, auditDbConnection);
                    }
                }
            }
            return id;
        }

        public static DateTime GetLastEffectiveDate(string dbConnection)
        {
            var effectiveDate = DateTime.Now;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffence_Audit_EffectiveDate", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetOffence_Audit_EffectiveDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["EffectiveDate"] != DBNull.Value)
                        effectiveDate = Convert.ToDateTime(reader["EffectiveDate"].ToString());
                }
                connection.Close();
                reader.Close();
            }
            return effectiveDate;
        }

        public static bool DeleteOffenceById(int colorCode, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteOffenceById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = colorCode;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteOffenceById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static Offence GetOffenceByName(string name, string dbConnection)
        {
            Offence offence = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offence = new Offence();
                    offence.ID = Convert.ToInt32(reader["Id"].ToString());
                    offence.OffenceDesc = reader["OFFENCEDESC"].ToString();
                    offence.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
                    offence.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offence.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                    offence.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offence.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    offence.OffenceName = OffenceTypeName(offence.OffenceTypeID);
                    offence.OffenceTypeList = OffenceTypeLists;

                    if (columns.Contains( "OffenceValue"))
                        offence.OffenceValue = reader["OffenceValue"].ToString();

                    if (columns.Contains( "AccountId"))
                        offence.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        offence.AccountName = reader["AccountName"].ToString();

                }
                connection.Close();
                reader.Close();
            }
            return offence;
        }

        public static Offence GetOffenceById(int id, string dbConnection)
        {
            Offence offence = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offence = new Offence();
                    offence.ID = Convert.ToInt32(reader["Id"].ToString());
                    offence.OffenceDesc = reader["OFFENCEDESC"].ToString();
                    offence.OffenceDesc_AR = reader["OFFENCEDESC_AR"].ToString();
                    offence.OffenceTypeID = Convert.ToInt32(reader["OFFENCETYPEID"].ToString());
                    offence.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
                    offence.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offence.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    offence.OffenceName = OffenceTypeName(offence.OffenceTypeID);
                    offence.OffenceTypeList = OffenceTypeLists;

                    if (columns.Contains("OffenceValue"))
                        offence.OffenceValue = reader["OffenceValue"].ToString();

                    if (columns.Contains("AccountId"))
                        offence.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("SiteId"))
                        offence.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        offence.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("AccountName"))
                        offence.AccountName = reader["AccountName"].ToString();

                }
                connection.Close();
                reader.Close();
            }
            return offence;
        }

    }

    internal class Offence_Audit
    {
        public int Id { get; set; }
        public string Operation { get; set; }
        public DateTime EffectiveDate { get; set; }

        public static List<Offence_Audit> GetAllOffence_AuditByDate(DateTime updatedDate, string dbConnection)
        {
            var offence_Audits = new List<Offence_Audit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffence_AuditByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetAllOffence_AuditByDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var offence_Audit = new Offence_Audit();

                    if (reader["Id"] != DBNull.Value)
                        offence_Audit.Id = Convert.ToInt32(reader["Id"].ToString());

                    offence_Audit.Operation = reader["Operation"].ToString();
                    offence_Audit.EffectiveDate = Convert.ToDateTime(reader["EffectiveDate"].ToString());
                    offence_Audits.Add(offence_Audit);
                }
                connection.Close();
                reader.Close();
            }
            return offence_Audits;
        }
    }
}
