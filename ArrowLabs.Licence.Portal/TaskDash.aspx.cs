﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Arrowlabs.Business.Layer;
using System.Text;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;

//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;

using System.Web.Services;
using ArrowLabs.Licence.Portal.Helpers;
using System.Threading;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ArrowLabs.Licence.Portal
{
    public partial class TaskDash : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        protected string locID;
        protected string incidentName;
        protected string incidentIns;
        protected string incidentDesc;
        protected string incidentRecBy;
        protected string incidentLong;
        protected string incidentLat;
        protected string incidentTaskId;
        protected string incidentAction;
        protected string IsTask;
        protected string incidentType;
        protected string resolvedPercent = "0";
        protected string pendingPercent = "0";
        protected string HandledPercentage = "0";
        protected string inprogressPercent = "0";
        protected string completedPercent = "0";
        protected string datetoday;
        protected string hot1Percent = "0";
        protected string hot2Percent = "0";
        protected string hot3Percent = "0";
        protected string hot4Percent = "0";
        protected string hot5Percent = "0";
        protected string day1;
        protected string day2;
        protected string day3;
        protected string day4;
        protected string day5;
        protected string day6;
        protected string day7;
        protected string msbDisplay;
        protected string week1total;
        protected string week2total;
        protected string week3total;
        protected string week4total;

        protected string loggedInId;
        protected string VeriRequestCount = "0";
        protected string VeriEnrolledCount = "0";
        protected string VeriTotalCount = "0";
        protected string VeriRequestPercent = "0";
        protected string VeriEnrolledPercent = "0";
        protected string userinfoDisplay = "none";

        protected string pendingPercentDemo;
        protected string pendingPercentDemoCount;

        protected string inprogressPercentDemo;
        protected string inprogressPercentDemoCount;

        protected string completedPercentDemo;
        protected string completedPercentDemoCount;

        protected string acceptedPercent;
        protected string acceptedPercentCount;

        protected string totalTasksCount;

        protected string handledTeamTasks;

        protected int ActualDemo = 0;
        protected int PlannedDemo = 0;

        protected string ActualDemoCount = "0";
        protected string PlannedDemoCount = "0";

        protected int taskUser1Avg = 0;
        protected int taskUser2Avg = 0;
        protected int taskUser3Avg = 0;
        protected int taskUser4Avg = 0;
        protected int taskUser5Avg = 0;

        protected string taskUser1 = " ";
        protected string taskUser2 = " ";
        protected string taskUser3 = " ";
        protected string taskUser4 = " ";
        protected string taskUser5 = " ";

        protected string AvgTotal = "0";

        protected string Top1Ins = "0";
        protected string Top2Ins = "0";
        protected string Top3Ins = "0";
        protected string Top4Ins = "0";
        protected string Top5Ins = "0";

        protected string Top1InsCount = "0";
        protected string Top2InsCount = "0";
        protected string Top3InsCount = "0";
        protected string Top4InsCount = "0";
        protected string Top5InsCount = "0";

        protected string Top1InsName = " ";
        protected string Top2InsName = " ";
        protected string Top3InsName = " ";
        protected string Top4InsName = " ";
        protected string Top5InsName = " ";

        protected string Top1TicCount = "0";
        protected string Top2TicCount = "0";
        protected string Top3TicCount = "0";
        protected string Top4TicCount = "0";
        protected string Top5TicCount = "0";

        protected string Top1TicName = "";
        protected string Top2TicName = "";
        protected string Top3TicName = "";
        protected string Top4TicName = "";
        protected string Top5TicName = "";

        protected string Top1Tic = "0";
        protected string Top2Tic = "0";
        protected string Top3Tic = "0";
        protected string Top4Tic = "0";
        protected string Top5Tic = "0";

        protected int SkillSet1Week1Count = 0;
        protected int SkillSet1Week2Count = 0;
        protected int SkillSet1Week3Count = 0;
        protected int SkillSet1Week4Count = 0;

        protected int SkillSet2Week1Count = 0;
        protected int SkillSet2Week2Count = 0;
        protected int SkillSet2Week3Count = 0;
        protected int SkillSet2Week4Count = 0;

        protected int SkillSet3Week1Count = 0;
        protected int SkillSet3Week2Count = 0;
        protected int SkillSet3Week3Count = 0;
        protected int SkillSet3Week4Count = 0;

        protected int SkillSet4Week1Count = 0;
        protected int SkillSet4Week2Count = 0;
        protected int SkillSet4Week3Count = 0;
        protected int SkillSet4Week4Count = 0;

        protected int SkillSet5Week1Count = 0;
        protected int SkillSet5Week2Count = 0;
        protected int SkillSet5Week3Count = 0;
        protected int SkillSet5Week4Count = 0;


        protected string SkillSet1Name = " ";
        protected string SkillSet2Name = " ";
        protected string SkillSet3Name = " ";
        protected string SkillSet4Name = " ";
        protected string SkillSet5Name = " ";

        protected string demoWeek1 = "1";
        protected string demoWeek2 = "2";
        protected string demoWeek3 = "3";
        protected string demoWeek4 = "4";
        protected string demoWeek5 = "5";
        protected string demoMonth = "";

        protected int day1week1;
        protected int day2week1;
        protected int day3week1;
        protected int day4week1;
        protected int day5week1;
        protected int day6week1;
        protected int day7week1;

        protected int day1week2;
        protected int day2week2;
        protected int day3week2;
        protected int day4week2;
        protected int day5week2;
        protected int day6week2;
        protected int day7week2;

        protected int day1week3;
        protected int day2week3;
        protected int day3week3;
        protected int day4week3;
        protected int day5week3;
        protected int day6week3;
        protected int day7week3;

        protected int day1week4;
        protected int day2week4;
        protected int day3week4;
        protected int day4week4;
        protected int day5week4;
        protected int day6week4;
        protected int day7week4;

        protected int ticketday1week1;
        protected int ticketday2week1;
        protected int ticketday3week1;
        protected int ticketday4week1;
        protected int ticketday5week1;
        protected int ticketday6week1;
        protected int ticketday7week1;

        protected int ticketday1week2;
        protected int ticketday2week2;
        protected int ticketday3week2;
        protected int ticketday4week2;
        protected int ticketday5week2;
        protected int ticketday6week2;
        protected int ticketday7week2;

        protected int ticketday1week3;
        protected int ticketday2week3;
        protected int ticketday3week3;
        protected int ticketday4week3;
        protected int ticketday5week3;
        protected int ticketday6week3;
        protected int ticketday7week3;

        protected int ticketday1week4;
        protected int ticketday2week4;
        protected int ticketday3week4;
        protected int ticketday4week4;
        protected int ticketday5week4;
        protected int ticketday6week4;
        protected int ticketday7week4;
        protected int notificationCount1;
        protected int notificationCount2;
        protected int notificationCount3;
        protected int notificationCount4;
        protected string cusEvId { get; set; }
        protected string incidentId;
        protected string longi;
        protected string lati;
        protected string alarmMSG;
        protected string senderName2;
        protected string senderName;
        
        protected string username;
        protected string ipaddress;
        protected string sendToManager;
        protected string assignedTo;
        protected string sendToTag;
        protected string sendMangId;

        protected string hot1display;
        protected string hot2display;
        protected string hot3display;
        protected string hot4display;
        protected string hot5display;

        int total;
        protected int onlinecount;
        protected int offlinecount;
        protected int idlecount;

        static ClientLicence getClientLic;
        protected string escalatedView;
        protected string dispatchSurv;

        protected string sourceLat;
        protected string sourceLon;
        protected string currentlocation;
        protected string grpOptionView;
        protected string incidentDisplay;
        protected string otDisplay;
        static string fpath;
        static string fontpath;
        static List<string> VideoExtensions = new List<string> { ".AVI", ".MP4", ".MKV", ".FLV" };
        protected string senderName3;
        protected string siteName;
        protected string cuserDisplay;
        protected void Page_Load(object sender, EventArgs e)
        {
            fpath = Server.MapPath("~/Uploads/");
            fontpath = Server.MapPath("~/fonts/");
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            var dtNow = CommonUtility.getDTNow();
            datetoday = dtNow.ToString("MMM dd, yyyy");
            
            senderName = User.Identity.Name;
            senderName2 = Encrypt.EncryptData(User.Identity.Name, true, dbConnection);
            var userinfo = Users.GetUserByName(senderName, dbConnection);
            sourceLat = "25.2049808502197";
            sourceLon = "55.2707939147949";
            currentlocation = "U.A.E";
            grpOptionView = "none";
            msbDisplay = "none";
            if (userinfo != null)
            {

                if (userinfo.SiteId > 0)
                {
                    siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                }

                senderName3 = userinfo.CustomerUName;
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    {
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Collaboration)
                        {
                            grpOptionView = "block";
                        }
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.MessageBoard)
                        {
                            msbDisplay = "block";
                        }

                    }
                }
                else
                {
                    msbDisplay = "block";
                    grpOptionView = "block";
                }
            }
            try
            {
                var settings = ConfigSettings.GetConfigSettings(CommonUtility.dbConnection);
                ipaddress = settings.ChatServerUrl + "/signalr";
                var firstDayOfMonth = new DateTime(dtNow.Year, dtNow.Month, 1);
                day1 = firstDayOfMonth.ToString("yyyy-MM-dd");
                day2 = firstDayOfMonth.AddDays(1).ToString("yyyy-MM-dd");
                day3 = firstDayOfMonth.AddDays(2).ToString("yyyy-MM-dd");
                day4 = firstDayOfMonth.AddDays(3).ToString("yyyy-MM-dd");
                day5 = firstDayOfMonth.AddDays(4).ToString("yyyy-MM-dd");
                day6 = firstDayOfMonth.AddDays(5).ToString("yyyy-MM-dd");
                day7 = firstDayOfMonth.AddDays(6).ToString("yyyy-MM-dd");

                var allusers = new List<Users>();
                var allgroups = new List<Group>();
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                if (getClientLic != null)
                {
                    if (!getClientLic.isSurveillance)
                        dispatchSurv = "style='display:none;'";
                }
                if (userinfo != null)
                {
                    incidentDisplay = "none";
                    otDisplay = "block";


                    if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser || userinfo.RoleId == (int)Role.Regional)
                    {
                        if (userinfo.CustomerInfoId > 0)
                        {
                            var gSite = Arrowlabs.Business.Layer.CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                            if (gSite != null)
                            {
                                sourceLat = gSite.Lati;
                                sourceLon = gSite.Long;
                                if (!string.IsNullOrEmpty(gSite.Country))
                                    currentlocation = gSite.Country.ToUpper();
                                else
                                    currentlocation = "N/A";
                            }
                        }
                    }
                    else
                    {
                        if (userinfo.SiteId > 0)
                        {
                            var gSite = Arrowlabs.Business.Layer.Site.GetSiteById(userinfo.SiteId, dbConnection);
                            if (gSite != null)
                            {
                                sourceLat = gSite.Lati;
                                sourceLon = gSite.Long;
                                currentlocation = ReverseGeocode.RetrieveFormatedAddress(sourceLat, sourceLon, getClientLic).ToUpper();
                            }
                        }
                    }
                      
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        var uModules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                        if (uModules != null)
                        {
                            if (uModules.Count > 0)
                            {
                                var isIncident = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Alarms).ToList();
                                if (isIncident.Count > 0)
                                {
                                    incidentDisplay = "block";
                                }
                                var isOther = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Database).ToList();
                                var isVeri = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Verifier).ToList();
                                if (isOther.Count == 0)
                                {
                                    otDisplay = "none";
                                }
                            }
                            else
                            {
                                Response.Redirect("~/Pages/UsersDB.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Pages/UsersDB.aspx");
                        }
                    }
                    else
                    {
                        incidentDisplay = "block";
                    }
                    getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    if (getClientLic != null)
                    {
                        if (!getClientLic.isIncident)
                            incidentDisplay = "none";
                    }
                    loggedInId = userinfo.ID.ToString();
                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        escalatedView = "style='display:none'"; 
                        userinfoDisplay = "block";
                        allusers = Users.GetAllUsers(dbConnection);
                        allgroups = Group.GetAllGroup(dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        escalatedView = "style='display:block'"; 
                        userinfoDisplay = "none";
                        allusers = Users.GetAllUsers(dbConnection);
                        allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                    }
                    else
                    {
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            cuserDisplay = "style='display:none;'";
                        }
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            userinfoDisplay = "block";
                        //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                        //if (pushDev != null)
                        //{
                        //    if (!string.IsNullOrEmpty(pushDev.Username))
                        //    {
                        //        pushDev.IsServerPortal = true;
                        //        pushDev.SiteId = userinfo.SiteId;
                        //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection, dbConnectionAudit, true);
                        //    }
                        //}
                        PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);

                        var tasktemplateList = UserTask.GetAllTaskTemplatesBySiteId(userinfo.SiteId, dbConnection);
                        if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var item in sessions)
                            {
                                tasktemplateList.AddRange(UserTask.GetAllTemplateTasksByManagerId(item, dbConnection));
                            }
                        }

                        userinfoDisplay = "none";

                        if (userinfo.RoleId == (int)Role.Admin)
                        {
                            escalatedView = "style='display:none'";
                            var allmanagers = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var usermanager in allmanagers)
                            {
                                allusers.Add(usermanager);
                                var allmanagerusers = Users.GetAllFullUsersByManagerIdForDirector(usermanager.ID, dbConnection);
                                foreach (var userM in allmanagerusers)
                                {
                                    allusers.Add(userM);
                                }
                            }
                            var unassigneds = Users.GetAllUnassignedOperators(dbConnection);
                            unassigneds = unassigneds.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                            foreach (var subsubuser in unassigneds)
                            {
                                allusers.Add(subsubuser);
                            } 

                            allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            allusers.AddRange(Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection));
                            allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();

                            allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Manager)
                        {
                            allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                            allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            allusers.AddRange(Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection));

                            allusers = allusers.Where(i => i.RoleId != (int)Role.Director).ToList();

                            allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                            //foreach (var site in sites)
                            //{
                                allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                            //}
                            allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                        }
                    }
                }

                var usersearchSelectItems = new List<SearchSelectItem>();
                var groupsearchSelectItems = new List<SearchSelectItem>();
                foreach (var searchSelectUser in allusers)
                {
                    var newSearchSelectItem = new SearchSelectItem();
                    newSearchSelectItem.ID = searchSelectUser.ID.ToString();
                    newSearchSelectItem.Name = searchSelectUser.Username;
                    usersearchSelectItems.Add(newSearchSelectItem);
                }
                foreach (var searchSelectGroup in allgroups)
                {
                    var newSearchSelectItem = new SearchSelectItem();
                    newSearchSelectItem.ID = searchSelectGroup.Id.ToString();
                    newSearchSelectItem.Name = searchSelectGroup.Name;
                    groupsearchSelectItems.Add(newSearchSelectItem);
                }

                rejectusersearchselect.DataTextField = "Name";
                rejectusersearchselect.DataValueField = "ID";
                rejectusersearchselect.DataSource = usersearchSelectItems;
                rejectusersearchselect.DataBind();

                rejectgroupsearchselect.DataTextField = "Name";
                rejectgroupsearchselect.DataValueField = "ID";
                rejectgroupsearchselect.DataSource = groupsearchSelectItems;
                rejectgroupsearchselect.DataBind();

                var allmanager = new List<Users>();
                var alldirectors = new List<Users>();
                var unassigned = new Users();
                unassigned.Username = "Unassigned";
                unassigned.ID = 0;
                allmanager.Add(unassigned);
                alldirectors.Add(unassigned);
                //allmanager.AddRange(Users.GetAllManagers(dbConnection));
                //alldirectors.AddRange(Users.GetAllAdmins(dbConnection));



                editmanagerpickerSelect.DataTextField = "Username";
                editmanagerpickerSelect.DataValueField = "ID";
                editmanagerpickerSelect.DataSource = allmanager;
                editmanagerpickerSelect.DataBind();

                editdirpickerSelect.DataTextField = "Username";
                editdirpickerSelect.DataValueField = "ID";
                editdirpickerSelect.DataSource = alldirectors;
                editdirpickerSelect.DataBind();
                 

                incidentName = Request.QueryString["IncName"];
                incidentAction = "Dispatch";
                cusEvId = "0";

                IsTask = "false";
                incidentId = "0";
                {

                    getOnlineUsers(userinfo);
                    getEventStatus2(userinfo);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "PageLoad", ex, dbConnection, userinfo.SiteId);
            }
        }
        public void getOnlineUsers(Users userinfo)
        {
            try
            {
                var users = new List<Users>();
                var devices = new List<HealthCheck>();
                onlinecount = 0;
                offlinecount = 0;
                idlecount = 0;
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        users.AddRange(Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection));
                        users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();

                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var usr in sessions)
                        {
                            users.Add(usr);
                            var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                            foreach (var subsubuser in getallUsers)
                            {
                                users.Add(subsubuser);
                            }
                        }
                        var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                        unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                        foreach (var subsubuser in unassigned)
                        {
                            users.Add(subsubuser);
                        } 
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                        //{
                            users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                        //}
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                        users = users.Where(i => i.RoleId != (int)Role.Director).ToList();
                    }
                    else if(userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        users = Users.GetAllUsers(dbConnection);
                        users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        users = Users.GetAllUsers(dbConnection);
                        devices = HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                    }
                    foreach (var item in users)
                    {
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            onlinecount++;
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            idlecount++;
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            offlinecount++;
                        }
                    }
                    foreach (var dev in devices)
                    {
                        if (dev.Status == 1)
                        {
                            onlinecount++;
                        }
                        else
                        {
                            offlinecount++;
                        }
                    }
                    lbOnlineCount.Text = onlinecount.ToString();
                    lbOfflineCount.Text = offlinecount.ToString();
                    lbIdleCount.Text = idlecount.ToString();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getOnlineUsers", ex, dbConnection, userinfo.SiteId);
            }
        }
          
        [WebMethod]
        public static string deleteReminder(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                Reminders.DeleteReminderById(id, dbConnection);
                SystemLogger.SaveSystemLog(dbConnectionAudit, "TaskDash", id.ToString(), id.ToString(), userinfo, "Delete Reminder" + id);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "deleteReminder", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static List<string> getReminders(int id,string uname, Users userinfo)
        {
           // var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
              /*  var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
*/
                //if(userinfo)
                //var getreminders = Reminders.GetAllReminders(dbConnection);
                var getreminders = Reminders.GetAllRemindersByDateAndCreator(CommonUtility.getDTNow().Date, userinfo.Username, dbConnection);
                //json = ;//'<time style="margin-left:15px">8:00 am, December 31, 2015</time><p style="margin-left:15px">Send new updates to Orry sasdasssssssss ssssssssssssss ssssssssssssssssss ssssssssssss</p><div style="margin-left:-5px" class="vertical-line green-border"></div>';
                var remindercount = 0;
                foreach (var rem in getreminders)
                {
                    if (remindercount == 6)
                        break;
                    //var convertdatetimeformat = rem.ReminderDate.ToString('')//8:00 am, December 31, 2015
                    //<time style='margin-left:15px'>" + rem.ReminderDate.ToString("MMM dd, yyyy") + "</time>
                    if (!rem.IsCompleted)
                    {
                        var colorcode = string.Empty;
                        var colorcode2 = string.Empty;
                        if (rem.ColorCode == (int)Reminders.ColorCodes.Green)
                        {
                            colorcode = "green-border";
                            colorcode2 = "green";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Blue)
                        {
                            colorcode = "blue-border";
                            colorcode2 = "blue";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Yellow)
                        {
                            colorcode = "yellow-border";
                            colorcode2 = "yellow";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Red)
                        {
                            colorcode = "red-border";
                            colorcode2 = "red";
                        }

                        var date = rem.FromDate + "-" + rem.ToDate + " " + rem.ReminderDate.Value.ToString("dddd MMMM dd, yyyy");

                        var jsonstring = "<div onclick='reminderChoice(&apos;" + rem.ID + "&apos;,&apos;" + date + "&apos;,&apos;" + rem.ReminderNote + "&apos;,&apos;" + rem.ReminderTopic + "&apos;,&apos;" + colorcode2 + "&apos;)' href='#' data-target='#" + colorcode2 + "reminderboxs' data-toggle='modal'><time style='margin-left:15px'>" + rem.FromDate + "-" + rem.ToDate + "</time><p style='margin-left:15px'>" + rem.ReminderTopic + "</p><div style='margin-left:-5px' class='vertical-line " + colorcode + "'></div></div>";
                        listy.Add(jsonstring);
                        remindercount++;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getReminders", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getGPSDataFull(int id, bool locCB, bool eventsCB, bool peopleCB, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var users = new List<Users>();
                var fullcollection = new List<UserTask>();
                var vehs = new List<ClientManager>();
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (peopleCB)
                        {
                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {
                                var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                                foreach (var usr in sessions)
                                {
                                    users.Add(usr);
                                    var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                                    foreach (var subsubuser in getallUsers)
                                    {
                                        users.Add(subsubuser);
                                    }
                                }
                                var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                                unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                                foreach (var subsubuser in unassigned)
                                {
                                    users.Add(subsubuser);
                                }
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                                users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                            }
                            else if (userinfo.RoleId == (int)Role.Regional)
                            {
                                users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                            }
                            else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                            {
                                users = Users.GetAllUsers(dbConnection);
                                users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                            }
                            else if (userinfo.RoleId == (int)Role.SuperAdmin)
                            {
                                users = Users.GetAllUsers(dbConnection);
                            }
                        }

                        if (eventsCB)
                        {
                            if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                                fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Date, dbConnection);
                            else if (userinfo.RoleId == (int)Role.Manager)
                                fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {
                                fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                                var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                                foreach (var item in sessions)
                                {
                                    fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                                }
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(CommonUtility.getDTNow().Date, userinfo.SiteId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(CommonUtility.getDTNow().Date, userinfo.CustomerInfoId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Regional)
                            { 
                                fullcollection.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(CommonUtility.getDTNow().Date, userinfo.ID, dbConnection));
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerUser)
                            {
                                fullcollection = UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection);
                                if (userinfo.ContractLinkId > 0)
                                {
                                    fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                                }
                            }
                            var groupeded = fullcollection.GroupBy(item => item.Id);
                            fullcollection = groupeded.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        }
                    }
                }
                json += "[";


                var grouped = users.GroupBy(item => item.ID);
                users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in users)
                {
                    if (item.Username != userinfo.Username)
                    {
                        var newLoginDate = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString("d MMM  hh:mm tt");
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            if (item.RoleId == (int)Role.Manager || item.RoleId == (int)Role.Admin)
                            {
                                if (item.IsServerPortal)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                            }
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                        }
                    }
                }

                foreach (var item in fullcollection)
                {
                    if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                    {
                        if (item.IsRecurring && item.RecurringParentId == 0)
                        {

                        }
                        else
                        {
                            if (item.Status == (int)TaskStatus.Pending)
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Task\",\"DName\" : \"" + item.Name + "-" + item.NewCustomerTaskId + "\"},";
                            else if (item.Status == (int)TaskStatus.InProgress || item.Status == (int)TaskStatus.OnRoute)
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"YELLOW\",\"Logs\" : \"Task\",\"DName\" : \"" + item.Name + "-" + item.NewCustomerTaskId + "\"},";
                            else if (item.Status == (int)TaskStatus.Completed)
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"GREEN\",\"Logs\" : \"Task\",\"DName\" : \"" + item.Name + "-" + item.NewCustomerTaskId + "\"},";
                        }
                    }
                }
 

                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getGPSDataFull", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string getGPSData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                json += "[";
                var fullcollection = new List<UserTask>();
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Date, dbConnection);
                        else if (userinfo.RoleId == (int)Role.Manager)
                            fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                            var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var item in sessions)
                            {
                                fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(CommonUtility.getDTNow().Date,userinfo.SiteId ,dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(CommonUtility.getDTNow().Date, userinfo.CustomerInfoId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        { 
                            fullcollection.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(CommonUtility.getDTNow().Date, userinfo.ID, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            fullcollection = UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection);
                            if (userinfo.ContractLinkId > 0)
                            {
                                fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                            }
                        }
                        var grouped = fullcollection.GroupBy(item => item.Id);
                        fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        foreach (var item in fullcollection)
                        {
                            if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                            {
                                if (item.IsRecurring && item.RecurringParentId == 0)
                                {

                                }
                                else
                                {
                                    if (item.Status == (int)TaskStatus.Pending)
                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Task\",\"DName\" : \"" + item.Name +"-"+item.NewCustomerTaskId +"\"},";
                                    else if (item.Status == (int)TaskStatus.InProgress || item.Status == (int)TaskStatus.OnRoute)
                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"YELLOW\",\"Logs\" : \"Task\",\"DName\" : \"" + item.Name + "-" + item.NewCustomerTaskId + "\"},";
                                    else if (item.Status == (int)TaskStatus.Completed)
                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"GREEN\",\"Logs\" : \"Task\",\"DName\" : \"" + item.Name + "-" + item.NewCustomerTaskId + "\"},";
                                }
                            }
                        }
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getGPSData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getGPSDataTasks(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                json += "[";
                var fullcollection = new List<UserTask>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Date, dbConnection);
                else if (userinfo.RoleId == (int)Role.Manager)
                    fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                    var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var item in sessions)
                    {
                        fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(CommonUtility.getDTNow().Date, userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(CommonUtility.getDTNow().Date, userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var site in sites)
                        fullcollection.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(CommonUtility.getDTNow().Date, userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {
                    fullcollection = UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection);
                    if (userinfo.ContractLinkId > 0)
                    {
                        fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                    }
                }
                var grouped = fullcollection.GroupBy(item => item.Id);
                fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                foreach (var item in fullcollection)
                {
                    if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                    {
                        if (item.IsRecurring && item.RecurringParentId == 0)
                        {

                        }
                        else
                        { 
                            if (item.Status == (int)TaskStatus.Pending)
                                json += "{ \"Username\" : \"" + item.Name  + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Task\"},";
                            else if (item.Status == (int)TaskStatus.InProgress || item.Status == (int)TaskStatus.OnRoute)
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"YELLOW\",\"Logs\" : \"Task\"},";
                            else if (item.Status == (int)TaskStatus.Completed)
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"GREEN\",\"Logs\" : \"Task\"},";

                        }
                    }
                }

                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getGPSDataTasks", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getGPSDataOthers(int id,string uname)
        {
            var json = string.Empty;
            json += "[";
            var cusEvents = new List<CustomEvent>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            if (userinfo.RoleId == (int)Role.Manager)
            {
                cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

            }
            else if (userinfo.RoleId == (int)Role.Admin)
            {
                var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                foreach (var manguser in managerusers)
                {
                    cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                }
            }
            else
            {
                cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
            }
            foreach (var item in cusEvents)
            {
                if (item.EventType == CustomEvent.EventTypes.DriverOffence)
                    json += "{ \"Username\" : \"" + item.Name + "-" + item.EventId + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone) + "\",\"State\" : \"YELLOW\",\"Logs\" : \"INCIDENT\"},";
            }
            json = json.Substring(0, json.Length - 1);
            json += "]";
            return json;
        }
		
		public class DashTaskList
        {
            public List<string> onlineuserspercentage { get; set; }
            public List<string> reminders { get; set; }
            public List<string> ReminderSelectorDates { get; set; }
            public List<string> TeamTasks { get; set; }
            public List<string> recentActivity { get; set; }
        }
		
        [WebMethod]
        public static DashTaskList getusershealtcheck(int id, int onlinecount, int offlinecount, int idlecount, string uname)
        {
            DashTaskList dList = new DashTaskList();
            var healthtotal = onlinecount + offlinecount + idlecount;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                dList.onlineuserspercentage = listy;
                return dList;
            }
            else
            {
                var lp15 = (int)Math.Round((float)onlinecount / (float)healthtotal * (float)100);
                var lp30 = (int)Math.Round((float)offlinecount / (float)healthtotal * (float)100);
                var lp60 = (int)Math.Round((float)idlecount / (float)healthtotal * (float)100);
                if (lp15 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp15.ToString() + "%");

                if (lp30 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp30.ToString() + "%");

                if (lp60 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp60.ToString() + "%");

                dList.onlineuserspercentage = listy;
                dList.reminders = getReminders(id, uname, userinfo);
                dList.ReminderSelectorDates = getReminderSelectorDates(id);
                dList.TeamTasks = getTableDataTeamTasks(id, uname, userinfo);
                dList.recentActivity = getRecentActivity2(id, uname, userinfo);
            }

            return dList;
        }
		
		
        [WebMethod]
        public static float getTimer(int id)
        {
            var settings = ConfigSettings.GetConfigSettings(dbConnection);
            return settings.MIMSMobileGPSInterval;
        }
        [WebMethod]
        public static List<string> getTop5Values(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var hotAlarm = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, dbConnection);
                }
                else
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }

                var userList = new List<string>();
                foreach (var hot in hotAlarm)
                {
                    if (hot.EventType == CustomEvent.EventTypes.HotEvent || hot.EventType == CustomEvent.EventTypes.MobileHotEvent || hot.EventType == CustomEvent.EventTypes.Request)
                    {

                        userList.Add(hot.UserName);

                    }
                }
                Dictionary<string, int> counts = userList.GroupBy(x => x)
                                      .ToDictionary(g => g.Key,
                                                    g => g.Count());
                var items = from pair in counts
                            orderby pair.Value descending
                            select pair;
                int count = 0;
                foreach (KeyValuePair<string, int> pair in items)
                {

                    if (count < 5)
                    {
                        listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getTop5Values", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTop5OffenceValues(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var prevDate = CommonUtility.getDTNow().Subtract(new TimeSpan(31, 0, 0, 0));
                var todayDate = CommonUtility.getDTNow();
                listy.Add(prevDate.Year.ToString() + "-" + prevDate.Month.ToString() + "-" + prevDate.Day.ToString());
                listy.Add(todayDate.Year.ToString() + "-" + todayDate.Month.ToString() + "-" + todayDate.Day.ToString());
                listy.Add(prevDate.Year.ToString() + "-" + prevDate.Month.ToString());
                var offences = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    offences = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, dbConnection);
                }
                else
                {
                    offences = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }
                var userList = new List<string>();
                foreach (var off in offences)
                {
                    if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                    {

                        userList.Add(off.UserName);

                    }
                }
                Dictionary<string, int> counts = userList.GroupBy(x => x)
                                      .ToDictionary(g => g.Key,
                                                    g => g.Count());
                var items = from pair in counts
                            orderby pair.Value descending
                            select pair;
                int count = 0;
                foreach (KeyValuePair<string, int> pair in items)
                {
                    if (count < 5)
                    {
                        listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getTop5OffenceValues", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<CustomEvent>();

                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                    }
                }
                else
                {
                    allcustomEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                }
                var imageclass = string.Empty;
                foreach (var item in allcustomEvents)
                {
                    if (item.EventType != CustomEvent.EventTypes.DriverOffence)
                    {
                        if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                            if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                item.Name = "None";
                            else
                                item.Name = mobEv.EventTypeName;
                            //imageclass = "circle-point-red";
                        }
                        else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                        {

                            var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                            if (reqEv != null)
                            {
                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                    item.UserName = CommonUtility.getPCName(reqEv);
                            }
                        }
                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                            imageclass = "circle-point-red";
                        else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                            imageclass = "circle-point-green";
                        else
                            imageclass = "circle-point-yellow";

                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getTableData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getGPSDataUsers(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var users = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                var vehs = new List<ClientManager>();
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var usr in sessions)
                            {
                                users.Add(usr);
                                var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                                foreach (var subsubuser in getallUsers)
                                {
                                    users.Add(subsubuser);
                                }
                            }

                            var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                            unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();

                            foreach (var subsubuser in unassigned)
                            {
                                users.Add(subsubuser);
                            } 
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                            //foreach (var site in sites)
                                users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                        }
                        else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            users = Users.GetAllUsers(dbConnection);
                            users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.SuperAdmin)
                        {
                            users = Users.GetAllUsers(dbConnection);
                            devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                        }
                    }
                }
                json += "[";


                var grouped = users.GroupBy(item => item.ID);
                users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in users)
                {
                    if (item.Username != userinfo.Username)
                    {
                        var newLoginDate = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString("d MMM  hh:mm tt");
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            if (item.RoleId == (int)Role.Manager || item.RoleId == (int)Role.Admin)
                            {
                                //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(item.Username, dbConnection);
                                //if (pushDev != null)
                                //{
                                // if (!string.IsNullOrEmpty(pushDev.Username))
                                // {
                                if (item.IsServerPortal)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\"},";
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\"},";
                                }
                                //  }
                                //  else
                                //  {
                                //      json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\"},";
                                //  }
                                //}
                                // else
                                // {
                                //     json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\"},";
                                // }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\"},";
                            }
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"User\"},";
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\"},";
                        }
                    }
                }
                foreach (var dev in devices)
                {
                    var camString = "";
                    var camList = ClientCamera.GetAllClientCamerasByMacAddress(dev.MacAddress, dbConnection);
                    var camCounter = 0;

                    foreach (var cam in camList)
                    {
                        camString += "CameraName" + camCounter + "=" + cam.CameraName + "&";
                        camCounter++;
                    }
                    if (camCounter == 0)
                        camString = "NO CAM";

                    if (dev.Status == 1)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"BLUE\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";
                    }
                    else if (dev.Status == 0)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"OFFCLIENT\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";

                    }
                    else
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"" + dev.Status + "\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";

                    }

                }
                if (id > 0)
                {
                    var item2 = CustomEvent.GetCustomEventById(id, dbConnection);
                    if (item2 != null)
                    {
                        json += "{ \"Username\" : \"" + item2.Name + "\",\"Id\" : \"" + item2.EventId.ToString() + "\",\"Long\" : \"" + item2.Longtitude.ToString() + "\",\"Lat\" : \"" + item2.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"User\"},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getGPSDataUsers", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static List<string> getAssignUserTableData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allusers = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        allusers.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            allusers.Add(subsubuser);
                        }
                    }

                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    //{
                        allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                    //}
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                foreach (var item in allusers)
                {
                    //if (item.DeviceType == (int)Users.DeviceTypes.Mobile || item.DeviceType == (int)Users.DeviceTypes.MobileAndClient || item.RoleId == (int)Role.Manager)
                    //{
                        var returnstring = "<tr role='row' class='odd'><td>" + item.Username + "</td><td class='sorting_1'>" + item.Status + "</td><td><a href='#' class='red-color' id=" + item.ID + item.Username + " onclick='dispatchUserchoiceTable(&apos;" + item.ID + "&apos;,&apos;" + item.Username + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                    //}
                }
                foreach (var dev in devices)
                {
                    if (dev.Status == 1)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Online</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='dispatchUserchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                    else
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Offline</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='dispatchUserchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getAssignUserTableData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableRowData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);

                if (customData != null)
                {

                    if (customData.Status == (int)TaskStatus.Completed && customData.FollowUp)
                        customData.StatusDescription = "Follow Up";
                    var getCus = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                    if (customData.Status == (int)TaskStatus.Rejected)
                        listy.Add("Accepted");
                    else
                    {
                        var isTicket = false;

                        if (getCus != null)
                        {
                            if (getCus.EventType == CustomEvent.EventTypes.DriverOffence)
                                isTicket = true;
                        }
                        if (customData.IncidentId > 0 && !isTicket)
                        {

                            listy.Add("Accepted");
                        }
                        else
                        {
                            if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                            {
                                if (customData.CreatedBy != userinfo.Username)
                                {
                                    var userGroups = GroupUser.GetGroupsByUserId(userinfo.ID, dbConnection);
                                    GroupUser isInGroup = null;
                                    if (userGroups.Count > 0)
                                    {
                                        isInGroup = userGroups.Where(x => x.GroupId == customData.AssigneeId).FirstOrDefault();
                                    }
                                    if (customData.AssigneeId == userinfo.ID)
                                        listy.Add("MyTask");
                                    else if (isInGroup != null)
                                    {
                                        listy.Add("MyTask");
                                    }
                                    else
                                        listy.Add(customData.StatusDescription);
                                }
                                else
                                {
                                    listy.Add(customData.StatusDescription);
                                }
                            }
                            else
                            {
                                var userGroups = GroupUser.GetGroupsByUserId(userinfo.ID, dbConnection);
                                GroupUser isInGroup = null;
                                if (userGroups.Count > 0)
                                {
                                    isInGroup = userGroups.Where(x => x.GroupId == customData.AssigneeId).FirstOrDefault();
                                }
                                if (customData.AssigneeId == userinfo.ID)
                                    listy.Add("MyTask");
                                else if (isInGroup != null)
                                {
                                    listy.Add("MyTask");
                                }
                                else
                                    listy.Add(customData.StatusDescription);
                            }
                        }
                    }


                    listy.Add(customData.CustomerUName);
                    listy.Add(customData.CreateDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.ACustomerUName);
                    if (customData.Status == (int)TaskStatus.RejectedSaved)
                        customData.StatusDescription = "Rejected";
                    listy.Add(customData.StatusDescription);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geolocation);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Description);
                    listy.Add(customData.Notes);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.StartDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.CheckListNotes);
                    listy.Add("circle-point " + CommonUtility.getImgStatusPriority(customData.Priority));

                    var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(customData.TemplateCheckListId.ToString(), dbConnection);
                    if (parentTasks != null)
                        listy.Add(parentTasks.Name);
                    else
                        listy.Add("None");

                    if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                    {
                        if (customData.CreatedBy == userinfo.Username)
                        {
                            listy.Add("block");
                        }
                        else
                        {
                            listy.Add("none");
                        }
                    }
                    else
                        listy.Add("block");

                    if (!string.IsNullOrEmpty(customData.RejectionNotes))
                    {
                        listy.Add(customData.RejectionNotes);
                    }
                    else
                    {
                        listy.Add("N-A");
                    }

                    listy.Add(customData.TaskTypeName);

                    if (customData.IncidentId > 0)
                    {
                        var rv = "Incident";
                        // var getCus = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                        if (getCus != null)
                        {
                            if (getCus.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus.Identifier, dbConnection);
                                if (mobEv != null)
                                {
                                    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                    {

                                    }
                                    else
                                        getCus.Name = mobEv.EventTypeName;
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.HotEvent || getCus.EventType == CustomEvent.EventTypes.Request)
                            {
                                var reqEv = HotEvent.GetHotEventById(getCus.Identifier, dbConnection);
                                if (reqEv != null)
                                {
                                    if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                        getCus.UserName = CommonUtility.getPCName(reqEv);

                                    if (getCus.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var splitName = reqEv.Name.Split('^');
                                        if (splitName.Length > 1)
                                            getCus.Name = splitName[1];
                                    }
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.DriverOffence)
                            {
                                var doffence = DriverOffence.GetDriverOffenceById(getCus.Identifier, dbConnection);
                                if (doffence != null)
                                {
                                    getCus.Name = doffence.OffenceCategory;
                                }
                                rv = "Ticket";
                            }
                            listy.Add(rv + "|" + getCus.Name + "|" + customData.IncidentId);


                        }
                        else
                            listy.Add("None");
                    }
                    else
                    {
                        if (customData.TaskLinkId > 0)
                        {
                            var gt = UserTask.GetTaskById(customData.TaskLinkId, dbConnection);
                            if (gt != null)
                            {
                                if (gt.IncidentId > 0)
                                {
                                    var getCus2 = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                                    if (getCus2 != null)
                                    {
                                        if (getCus2.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus2.Identifier, dbConnection);
                                            if (mobEv != null)
                                            {
                                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                                {

                                                }
                                                else
                                                    getCus2.Name = mobEv.EventTypeName;
                                            }
                                        }
                                        else if (getCus2.EventType == CustomEvent.EventTypes.HotEvent || getCus2.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(getCus2.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                                    getCus2.UserName = CommonUtility.getPCName(reqEv);

                                                if (getCus2.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    var splitName = reqEv.Name.Split('^');
                                                    if (splitName.Length > 1)
                                                        getCus2.Name = splitName[1];
                                                }
                                            }
                                        }
                                        else if (getCus2.EventType == CustomEvent.EventTypes.DriverOffence)
                                        {
                                            var doffence = DriverOffence.GetDriverOffenceById(getCus2.Identifier, dbConnection);
                                            if (doffence != null)
                                            {
                                                getCus2.Name = doffence.OffenceCategory;
                                            }
                                        }
                                        listy.Add(getCus2.Name + "|" + customData.IncidentId);
                                    }
                                    else
                                    {
                                        listy.Add("None");
                                    }
                                }
                                else
                                {
                                    listy.Add("None");
                                }
                            }
                            else
                            {
                                listy.Add("None");
                            }
                        }
                        else
                            listy.Add("None");
                    }

                    if (customData.TaskLinkId > 0)
                    {
                        var getT = UserTask.GetTaskById(customData.TaskLinkId, dbConnection);
                        if (getT != null)
                        {
                            listy.Add(getT.Name + "|" + getT.Id);
                        }
                        else
                        {
                            listy.Add("None");
                        }
                    }
                    else
                    {
                        listy.Add("None");
                    }
                    listy.Add(string.IsNullOrEmpty(customData.ClientName) ? "N/A" : customData.ClientName);
                    listy.Add(string.IsNullOrEmpty(customData.SysTypeName) ? "N/A" : customData.SysTypeName);
                    listy.Add(string.IsNullOrEmpty(customData.ProjectName) ? "N/A" : customData.ProjectName);
                    listy.Add(string.IsNullOrEmpty(customData.ContractName) ? "N/A" : customData.ContractName);

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTableRowData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getEventHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var eventData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);//EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(eventData.Id, dbConnection);

                var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(id), dbConnection);
                if (tasklinks.Count > 0)
                {
                    foreach (var link in tasklinks)
                    {
                        var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                        foreach (var forl in forlink)
                        {
                            forl.isSubTask = true;
                            taskeventhistory.Add(forl);
                        }
                    }
                    taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                }

                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                foreach (var task in taskeventhistory)
                {
                    var taskn = "Task";
                    if (task.isSubTask)
                    {
                        taskn = "Sub-task";
                    }
                    if (eventData.AssigneeType == (int)TaskAssigneeType.Group)
                    {
                        if (task.Action == (int)TaskAction.Update)
                        {
                            acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                                + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> updated<span class='red-color'>" + taskn
                                + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                            listy.Add(acceptedData);
                        }
                    }

                    if (task.Action == (int)TaskAction.Complete)
                    {
                        completedBy = task.CustomerUName;
                        var compLocation = string.Empty;
                        var geoLoc = ReverseGeocode.RetrieveFormatedAddress(eventData.EndLatitude.ToString(), eventData.EndLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc))
                            compLocation = " at " + geoLoc;
                        var compInfo = "completed";
                        var compstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + completedBy + "</span>" + compInfo + "<span class='red-color'>" + taskn
                            + "</span>" + compLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(compstring);
                    }
                    else if (task.Action == (int)TaskAction.InProgress)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "started";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.OnRoute)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "on route";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Pause)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "paused";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Resume)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "resumed";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Accepted)
                    {
                        acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> accepted<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(acceptedData);
                    }
                    else if (task.Action == (int)TaskAction.Rejected)
                    {
                        rejectedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> rejected<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(rejectedData);
                    }
                    else if (task.Action == (int)TaskAction.Assigned)
                    {
                        assignedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> assigned<span class='red-color'>" + taskn
                            + "</span> to " + task.ACustomerUName + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(assignedData);
                    }
                    else if (task.Action == (int)TaskAction.Pending)
                    {
                        var incidentLocation = string.Empty;
                        var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(eventData.Latitude.ToString(), eventData.Longitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc3))
                            incidentLocation = " at " + geoLoc3;
                        var actioninfo = "created";

                        var gUser = Users.GetUserByName(eventData.CreatedBy, dbConnection);

                        var dataString = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + gUser.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + taskn
                            + "</span>for " + task.ACustomerUName + " " + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(dataString);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash", "getEventHistoryData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<video id='Video" + i + "'  width='100%' height='378px'  muted controls ><source src='" + item.DocumentPath + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                //if (item.ChecklistId == 0)
                                //{
                                //    var retstring = "<audio id='Video" + i + "' width='100%' height='378px' controls ><source src='" + item.DocumentPath + "'  type='audio/mpeg' /></audio>";
                                //    listy.Add(retstring);
                                //    i++;
                                //}
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img onclick='rotateMe(this);' src='" + item.DocumentPath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getAttachmentData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataIcons(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                var c = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (i == 4)
                            {
                                var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#taskattachments-tab' onclick='hideTaskplay()'/>";
                                listy += retstring;
                                i++;
                               
                                break;
                            }
                            else
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                    c++;
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                {
                                    if (item.ChecklistId == 0)
                                    {
                                        //var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)'/>";

                                        listy += retstring;
                                        //i++;
                                    }
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                    //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                    var retstring = "<img src='" + item.DocumentPath + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                    c++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getAttachmentDataIcons", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#tasklocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Task Location</p></div></div></div>";
                var i = 1;
                var iTab = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                if (item.ChecklistId == 0)
                                {
                                    //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                    listy += retstring;
                                    //iTab++;
                                }
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {
                                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));

                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + item.DocumentPath + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + item.DocumentPath + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;

                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getAttachmentDataTab", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getLocationData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var item = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                var tracebackhistory = TraceBackHistory.GetTracBackHistoryByIncidentId(item.EventId, dbConnection);
                json += "[";
                if (item != null)
                {
                    if (!string.IsNullOrEmpty(item.Longtitude) && !string.IsNullOrEmpty(item.Longtitude))
                    {
                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    }
                    else
                    {
                        var geofencelocs = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, item.EventId);
                        foreach (var geofence in geofencelocs)
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geofence.Longitude.ToString() + "\",\"Lat\" : \"" + geofence.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"#800000\"},";
                        }
                    }
                    foreach (var tb in tracebackhistory)
                    {

                    }
                }

                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string isUser(string id)
        {

            var result = "false";
            {
                var getuser = Users.GetUserByEncryptedName(id, dbConnection);
                if (getuser != null)
                    result = "true";
            }
            return result;
        }
        [WebMethod]
        public static string getAssigneeType(string id)
        {
            if (CommonUtility.isNumeric(id))
                return "User";
            else
                return "Device";
        }
        [WebMethod]
        public static string changeIncidentState(string id, string state, string ins, string uname)
        {
            int taskId1 = 0;
            var retVal = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retValve = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retValve)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (!string.IsNullOrEmpty(id))
                    {
                        var oldIInfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        incidentinfo.UpdatedBy = userinfo.Username;

                        if (state != "Reject" && state != "Resolve")
                            incidentinfo.Instructions = ins;

                        incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue(state.ToLower());
                        incidentinfo.Handled = false;
                        //incidentinfo.SiteId = userinfo.SiteId;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        var returnV = CommonUtility.CreateIncident(incidentinfo);
                        if (returnV != "")
                        {
                            taskId1 = Convert.ToInt32(returnV);
                            CustomEvent.CustomEventHandledById(taskId1, false, userinfo.Username, dbConnection);
                            //taskId1 = CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection);
                            retVal = incidentinfo.Name;

                            if (incidentinfo.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                var getMobName = MobileHotEvent.GetMobileHotEventByGuid(incidentinfo.Identifier, dbConnection);
                                {
                                    if (string.IsNullOrEmpty(getMobName.EventTypeName))
                                        retVal = "None";
                                    else
                                        retVal = getMobName.EventTypeName;

                                }
                            }

                            if (taskId1 < 1)
                            {
                                taskId1 = Convert.ToInt32(id);

                            }

                            if (incidentinfo.IncidentStatus != (int)CustomEvent.IncidentActionStatus.Dispatch)
                            {
                                if (oldIInfo.IncidentStatus != incidentinfo.IncidentStatus)
                                {
                                    var EventHistoryEntry = new EventHistory();
                                    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                                    EventHistoryEntry.CreatedBy = userinfo.Username;
                                    EventHistoryEntry.EventId = taskId1;
                                    EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                                    EventHistoryEntry.UserName = userinfo.Username;
                                    EventHistoryEntry.Remarks = ins;
                                    EventHistoryEntry.SiteId = userinfo.SiteId;
                                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                                }
                            }
                            else
                            {

                                //Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                                var notifications = Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                if (notifications.Count > 0)
                                {
                                    var firstNotification = notifications.FirstOrDefault();
                                    if (firstNotification.AssigneeType == (int)Notification.NotificationAssigneeType.User)
                                    {
                                        var userNotification = notifications.Where(x => x.AssigneeId == userinfo.ID).FirstOrDefault();
                                        if (userNotification != null)
                                        {
                                            Arrowlabs.Business.Layer.Notification.UpdateNotificationStatus(userNotification.Id, true, dbConnection);
                                        }
                                    }
                                }

                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    task.Status = (int)TaskStatus.Cancelled;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }
                            }
                            if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                            {
                                incidentinfo.Handled = true;
                                incidentinfo.HandledTime = CommonUtility.getDTNow();
                                incidentinfo.HandledBy = userinfo.Username;
                                incidentinfo.SiteId = userinfo.SiteId;
                                taskId1 = CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection, dbConnectionAudit, true);
                                if (taskId1 < 1)
                                {
                                    taskId1 = Convert.ToInt32(id);
                                    incidentinfo.EventId = Convert.ToInt32(id);
                                }
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    var tskhistory = new TaskEventHistory();
                                    if (task.Status == (int)TaskStatus.Completed)
                                    {
                                        task.Status = (int)TaskStatus.Accepted;
                                        tskhistory.Action = (int)TaskAction.Accepted;
                                        tskhistory.Remarks = "Incident connected to task has been resolved";
                                    }
                                    else
                                    {
                                        task.Status = (int)TaskStatus.Cancelled;
                                        tskhistory.Action = (int)TaskAction.Rejected;
                                        tskhistory.Remarks = "Incident connected to task has been resolved prior to completion";
                                    }
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }
                                CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection);
                                CustomEvent.CustomEventHandledById(incidentinfo.EventId, true, userinfo.Username, dbConnection);
                            }
                            else if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Release)
                            {
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                                foreach (var task in taskinfo)
                                {

                                    var tskhistory = new TaskEventHistory();

                                    task.Status = (int)TaskStatus.Cancelled;
                                    tskhistory.Action = (int)TaskAction.Rejected;
                                    tskhistory.Remarks = "Incident connected to task has been released prior to completion";
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);

                                }
                            }
                            else if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                            {
                                var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(incidentinfo.EventId, dbConnection);
                                foreach (var noti in notifications)
                                {
                                    if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                    {
                                        var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                        if (!string.IsNullOrEmpty(taskuser.EngagedIn))
                                        {
                                            var split = taskuser.EngagedIn.Split('-');
                                            if (split[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, CommonUtility.dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                    else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                    {
                                        var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                        foreach (var taskuser in groupusers)
                                        {
                                            var splitinfo = taskuser.EngagedIn.Split('-');
                                            if (splitinfo.Length > 1)
                                            {
                                                if (splitinfo[1] == noti.Id.ToString())
                                                {
                                                    taskuser.Engaged = false;
                                                    taskuser.EngagedIn = string.Empty;
                                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                                }
                                            }
                                        }
                                    }
                                }

                                Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(incidentinfo.EventId, true, dbConnection);
                                var taskinfo = UserTask.GetAllTaskByIncidentId(incidentinfo.EventId, dbConnection);
                                foreach (var task in taskinfo)
                                {
                                    var tskhistory = new TaskEventHistory();
                                    task.Status = (int)TaskStatus.Rejected;
                                    tskhistory.Action = (int)TaskAction.Rejected;
                                    tskhistory.Remarks = "Incident connected to task has been rejected prior to completion";
                                    tskhistory.CreatedBy = userinfo.Username;
                                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                                    tskhistory.TaskId = task.Id;
                                    tskhistory.SiteId = userinfo.SiteId;
                                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                                    task.CustomerId = userinfo.CustomerInfoId;
                                    UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                                }

                            }
                        }
                    }

                }
                catch (Exception er)
                {
                    MIMSLog.MIMSLogSave("TaskDash", "changeIncidentState", er, dbConnection, userinfo.SiteId);
                }
                return retVal;
            }
        }

        [WebMethod]
        public static string sendpushMultipleNotification(string[] userIds, string msg,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                Thread thread = new Thread(() => PushNotificationClient.SendtoAppleMultiUserNotification("Alarm: " + msg, userIds, dbConnection));
                thread.Start();
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "sendpushMultipleNotification", ex, dbConnection, userinfo.SiteId);
                return "FAIL";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string getCurrentLocation(string lat, string lng,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var retVal = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                retVal = ReverseGeocode.RetrieveFormatedAddress(lat, lng, getClientLic);
                var split = retVal.Split('-');
                retVal = split[split.Length - 1];
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getCurrentLocation", ex, dbConnection, userinfo.SiteId);
            }
            return retVal;
        }
        [WebMethod]
        public static string insertNewReminder(string name, string date, string reminder, int colorcode, string todate, string fromdate,string uname)
        {
                                    var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var newReminder = new Reminders();
                    newReminder.ReminderTopic = name;
                    newReminder.ReminderNote = reminder;
                    newReminder.ToDate = todate;
                    newReminder.FromDate = fromdate;
                    newReminder.ColorCode = colorcode;
                    newReminder.ReminderDate = Convert.ToDateTime(date);
                    newReminder.CreatedBy = userinfo.Username;
                    newReminder.CreatedDate = CommonUtility.getDTNow();
                    newReminder.UpdatedBy = userinfo.Username;
                    newReminder.UpdatedDate = CommonUtility.getDTNow();
                    newReminder.SiteId = userinfo.SiteId;

                    newReminder.CustomerId = userinfo.CustomerInfoId;
                    Reminders.InsertorUpdateReminder(newReminder, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("TaskDash.aspx", "insertNewReminder", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string inserttask(string id, string assigneetype, string assigneename, string assigneeid, string templatename, string longi, string lati, int incidentId,string uname)
        {
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                //return "Logged in on different machine.Logging you out now.";
                return "0";
            }
            else
            {
                int taskId1 = 0;
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }


                    if (Convert.ToBoolean(id))
                    {
                        var temptasks = UserTask.GetTaskTemplateById(Convert.ToInt32(Convert.ToInt32(templatename)), dbConnection);
                        var newTasks = new UserTask();
                        newTasks.Name = temptasks.Name;
                        newTasks.Description = temptasks.Description;
                        newTasks.IncidentId = Convert.ToInt32(incidentId);
                        if (assigneetype == TaskAssigneeType.Location.ToString())
                        {
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);
                            newTasks.AssigneeType = (int)TaskAssigneeType.Location;
                        }
                        else if (assigneetype == TaskAssigneeType.Group.ToString())
                        {
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);
                            newTasks.AssigneeType = (int)TaskAssigneeType.Group;
                        }
                        else if (assigneetype == TaskAssigneeType.User.ToString())
                        {
                            newTasks.AssigneeType = (int)TaskAssigneeType.User;
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);

                        }
                        else if (assigneetype == TaskAssigneeType.Device.ToString())
                        {
                            newTasks.AssigneeType = (int)TaskAssigneeType.Device;
                            newTasks.AssigneeName = assigneename;
                            newTasks.AssigneeId = 0;

                        }
                        newTasks.CreateDate = CommonUtility.getDTNow();
                        newTasks.CreatedBy = userinfo.Username;
                        newTasks.ManagerName = userinfo.Username;
                        newTasks.ManagerId = userinfo.ID;

                        newTasks.IsDeleted = false;
                        newTasks.UpdatedDate = CommonUtility.getDTNow();

                        if (!string.IsNullOrEmpty(longi))
                            newTasks.Longitude = Convert.ToDouble(longi);
                        else
                            newTasks.Longitude = 0;

                        if (!string.IsNullOrEmpty(lati))
                            newTasks.Latitude = Convert.ToDouble(lati);
                        else
                            newTasks.Latitude = 0;

                        newTasks.StartDate = CommonUtility.getDTNow().Date;
                        newTasks.EndDate = CommonUtility.getDTNow().Date;
                        newTasks.Priority = temptasks.Priority;
                        newTasks.TemplateCheckListId = temptasks.TemplateCheckListId;
                        newTasks.SiteId = userinfo.SiteId;
                        newTasks.CustomerId = userinfo.CustomerInfoId;
                        taskId1 = UserTask.InsertorUpdateTask(newTasks, dbConnection, dbConnectionAudit, true);
                        newTasks.Id = taskId1;
                        var tskhistory = new TaskEventHistory();
                        tskhistory.Action = (int)TaskAction.Pending;
                        tskhistory.CreatedBy = userinfo.Username;
                        tskhistory.CreatedDate = CommonUtility.getDTNow();
                        tskhistory.TaskId = taskId1;
                        tskhistory.SiteId = userinfo.SiteId;
                        tskhistory.CustomerId = userinfo.CustomerInfoId;
                        TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);

                        var allTemplateCheckListItems = TemplateCheckListItem.GetAllTemplateCheckListItems(dbConnection);
                        if (allTemplateCheckListItems != null && allTemplateCheckListItems.Count > 0)
                        {
                            var selectedTemplateCheckListItems = allTemplateCheckListItems.Where(i => i.ParentCheckListId == newTasks.TemplateCheckListId).ToList();
                            if (selectedTemplateCheckListItems.Count > 0)
                            {
                                foreach (var templateCheckListItem in selectedTemplateCheckListItems)
                                {
                                    var taskCheckList = new TaskCheckList();
                                    taskCheckList.IsChecked = templateCheckListItem.IsChecked;
                                    taskCheckList.TaskId = taskId1;
                                    taskCheckList.UpdatedDate = CommonUtility.getDTNow();
                                    taskCheckList.CreatedDate = CommonUtility.getDTNow();
                                    taskCheckList.CreatedBy = Environment.UserName;
                                    taskCheckList.TemplateCheckListItemId = templateCheckListItem.Id;
                                    taskCheckList.SiteId = userinfo.SiteId;
                                    taskCheckList.CustomerId = userinfo.CustomerInfoId;
                                    TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckList, dbConnection, dbConnectionAudit, true);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("TaskDash.aspx", "inserttask", ex, dbConnection, userinfo.SiteId);
                }
                return taskId1.ToString();
            }
        }
        [WebMethod]
        public static string insertNewIncident(string name, string desc, string locationid, string incidenttype, string notificationid, string taskid, string receivedby, string longi, string lati, string status, string instructions, string msgtask, string incidentId,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            int taskId1 = 0;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }


                CustomEvent newincident = null;
                var dtnow = CommonUtility.getDTNow();

                if (!string.IsNullOrEmpty(incidentId))
                    if (Convert.ToInt32(incidentId) > 0)
                        newincident = CustomEvent.GetCustomEventById(Convert.ToInt32(incidentId), dbConnection);
                    else
                    {
                        newincident = new CustomEvent();
                        newincident.Identifier = Guid.NewGuid();
                        newincident.CreatedBy = userinfo.Username;
                    }

                if (newincident != null)
                {
                    newincident.UserName = userinfo.Username;

                    newincident.RecevieTime = dtnow;
                    newincident.EventType = CustomEvent.EventTypes.Incident;
                    newincident.Name = name;
                    newincident.Description = desc;
                    newincident.LocationId = Convert.ToInt32(locationid);
                    newincident.IncidentType = Convert.ToInt32(incidenttype);
                    newincident.ReceivedBy = receivedby;

                    newincident.CreatedDate = dtnow;
                    newincident.Longtitude = longi;
                    newincident.Latitude = lati;
                    newincident.Instructions = instructions;

                    if (Convert.ToBoolean(msgtask))
                        newincident.TemplateTaskId = Convert.ToInt32(taskid);
                    else
                        newincident.TemplateTaskId = 0;

                    newincident.IncidentStatus = (int)CustomEvent.IncidentActionStatus.Dispatch;
                    newincident.SiteId = userinfo.SiteId;
                    taskId1 = CustomEvent.InsertorUpdateIncident(newincident, dbConnection, dbConnectionAudit, true);



                    if (taskId1 < 1)
                    {
                        taskId1 = Convert.ToInt32(incidentId);
                    }
                    else
                    {
                        var EventHistoryEntry = new EventHistory();
                        EventHistoryEntry.CreatedDate = dtnow;
                        EventHistoryEntry.CreatedBy = userinfo.Username;
                        EventHistoryEntry.EventId = taskId1;
                        EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                        EventHistoryEntry.UserName = receivedby;
                        EventHistoryEntry.SiteId = userinfo.SiteId;
                        EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                        EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);

                    }

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "inserttask", ex, dbConnection, userinfo.SiteId);
            }
            return taskId1.ToString();
        }
        //User Profile
        [WebMethod]
        public static int changePW(int id, string password,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                return 0;
            }
            else
            { 
                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id;
                }
                else
                    return 0;
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (id > 1)
            {
                var getuser = Users.GetUserById(id, dbConnection);
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName    ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                        
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    return id;
                }
                else
                    return 0;
            }
            else if (userinfo.ID > 0)
            {
                var getuser = userinfo;
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName  ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                          
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    return 1;
                }
                else
                    return 0;
            }
            return 0;
        }
        [WebMethod]
        public static List<string> getUserProfileData(int id)
        {
            var listy = new List<string>();
            // var test = Users.GetUserById(id, dbConnection);
            var customData = Users.GetUserById(id, dbConnection);
            var supervisorId = 0;
            if (customData != null)
            {
                listy.Add(customData.Username);
                listy.Add(customData.FirstName + " " + customData.LastName);
                listy.Add(customData.Telephone);
                listy.Add(customData.Email);
                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                listy.Add(geoLoc);
                listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                if (customData.RoleId == (int)Role.Operator)
                {
                    var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                    if (usermanager != null)
                    {
                        listy.Add(usermanager.CustomerUName);
                        supervisorId = usermanager.ID;
                    }
                    else
                        listy.Add("Unassigned");
                }
                else if (customData.RoleId == (int)Role.Manager)
                {
                    var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                    if (getdir != null)
                    {
                        listy.Add(getdir.CustomerUName);
                        supervisorId = getdir.ID;
                    }
                    else
                        listy.Add("Unassigned");
                }
                else if (customData.RoleId == (int)Role.UnassignedOperator)
                {
                    listy.Add("Unassigned");
                }
                else
                {
                    listy.Add(" ");
                }
                listy.Add("Group Name");
                listy.Add(customData.Status);
                listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);//"Images/icon-user-default.png";//images / custom - images / user - 1.png;
                //if (userImg != null)
                //{
                //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                //}
                var fontstyle = string.Empty;
                if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                {
                    fontstyle = "style='color:lime;'";
                }
                //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                var monitor = string.Empty;
                //if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                if (customData.RoleId != (int)Role.SuperAdmin)
                {
                    // if (pushDev != null)
                    //  {
                    //if (!string.IsNullOrEmpty(pushDev.Username))
                    //{
                    if (customData.IsServerPortal)
                    {
                        monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        fontstyle = "";
                    }
                    else
                    {
                        monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                    }
                    // }
                    //  else
                    // {
                    //     monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                    //    fontstyle = "";
                    // }
                    //}
                }
                listy.Add(imgSrc);
                listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                listy.Add(customData.FirstName);
                listy.Add(customData.LastName);
                listy.Add(supervisorId.ToString());
                listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                listy.Add(customData.Latitude.ToString());
                listy.Add(customData.Longitude.ToString());
                var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                if (customData.RoleId != (int)Role.Regional)
                {
                    if (customData.SiteId == 0)
                        listy.Add("N/A");
                    else
                        listy.Add(userSiteDisplay);
                }
                else
                {
                    listy.Add("Multiple");
                }



                listy.Add(customData.Gender);
                listy.Add(customData.EmployeeID);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getUserRecentActivity(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }
            try
            {
                var entryCount = 0;
                var userDetails = Users.GetUserById(id, dbConnection);
                var loginhistory = LoginSession.GetLoginHistoryByMacAddress(userDetails.Username, dbConnection);
                var getrecenthistory = EventHistory.GetEventHistoryByUser(userDetails.Username, dbConnection);
                var getMyTasks = UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.Completed, dbConnection);
                getMyTasks.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.InProgress, dbConnection));
                getMyTasks.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.Pending, dbConnection));
                getMyTasks.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.Accepted, dbConnection));

                List<UserTask> SortedList = getMyTasks.OrderByDescending(o => o.CreateDate).ToList();

                var getVerifiers = Verifier.GetAllVerifierByCreatedBy(userDetails.Username, dbConnection);

                List<Verifier> vSortedList = getVerifiers.OrderByDescending(o => o.CreatedDate).ToList();

                var ehistorylist = CommonUtility.getUserHistory(getrecenthistory, SortedList, vSortedList, loginhistory, getClientLic);

                foreach (var ehistory in ehistorylist)
                {
                    if (ehistory.username.ToLower() == userDetails.CustomerUName.ToLower())
                    {
                        ehistory.username = ehistory.username.ToLower();
                        if (entryCount == 10)
                            break;
                        if (ehistory.type == "Cus")
                        {
                            var jsonstring = "<div class='col-md-2'><p>" + ehistory.date.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + ehistory.id + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + ehistory.username + "</span></br>" + ehistory.actioninfo + "<span>" + ehistory.name + "</span>" + ehistory.incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (ehistory.type == "Tsk")
                        {
                            var jsonstring = "<div class='col-md-2'><p>" + ehistory.date.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + ehistory.id + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + ehistory.username + "</span></br>" + ehistory.actioninfo + "<span>" + ehistory.name + "</span>" + ehistory.incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (ehistory.type == "Ver")
                        {
                            var jsonstring = "<div class='col-md-2'><p>" + ehistory.date.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2' data-target='#verificationDocument'  data-toggle='modal' onclick='assignVerifierId(&apos;" + ehistory.id + "&apos;) '><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + ehistory.username + "</span></br>" + ehistory.actioninfo + "<span>" + ehistory.name + "</span>" + ehistory.incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (ehistory.type == "Log")
                        {
                            var jsonstring = "<div class='col-md-2'><p>" + ehistory.date.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + ehistory.username + "</span></br>" + ehistory.actioninfo + "<span>" + ehistory.name + "</span>" + ehistory.incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                    }
                }
            }
            catch (Exception EX)
            {
                var logid = MIMSLog.MIMSLogSave("Default", "getUserRecentActivity", EX, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static List<string> getGroupDataFromUser(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var usergroups = GroupUser.GetGroupsByUserId(id, dbConnection);
                    foreach (var group in usergroups)
                    {
                        var groupInfo = Group.GetGroupById(group.GroupId, dbConnection);
                        if (groupInfo != null)
                        {
                            listy.Add(AddGroupToUserProfileList(groupInfo));
                            listy.Add(groupInfo.Id.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getGroupDataFromUser", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        private static string AddGroupToUserProfileList(Group item)
        {
            var members = Users.GetAllUserByGroupId(item.Id, dbConnection);
            var colorcode = string.Empty;
            if (item.ColorCode == (int)Reminders.ColorCodes.Green)
                colorcode = "green-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Blue)
                colorcode = "blue-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Yellow)
                colorcode = "yellow-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Red)
                colorcode = "red-column";

            var retstring = "<div class='inline-block mr-2x inherited-vertical-align'><span class='" + colorcode + " column-diemention extra-height'></span></div><div class='inline-block mr-2x inherited-vertical-align'><h4 class='no-margin-top blue-color'>" + item.Name + "</h4><h6 class='mb-4x' id='" + item.Name + "-" + item.Id + "'>MEMBERS(" + members.Count + ")</h6></div>";

            return retstring;
        }
		
        [WebMethod]
        public static List<string> getReminderSelectorDates(int id)
        {
            var listy = new List<string>();
            // Set the start time (00:00 means 12:00 AM)
            DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(7, 0, 0));
            // Set the end time (23:55 means 11:55 PM)
            DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(17, 0, 0));
            //Set 5 minutes interval
            TimeSpan Interval = new TimeSpan(1, 0, 0);
            //To set 1 hour interval
            //TimeSpan Interval = new TimeSpan(1, 0, 0);           

            while (StartTime <= EndTime)
            {
                listy.Add(StartTime.ToString("HH:mm"));
                StartTime = StartTime.Add(Interval);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getDateRangeReminder(string date)
        {
            var listy = new List<string>();
            var split = date.Split(':');
            // Set the start time (00:00 means 12:00 AM)
            DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(Convert.ToInt32(split[0]), 0, 0));
            // Set the end time (23:55 means 11:55 PM)
            DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(18, 0, 0));
            //Set 5 minutes interval
            TimeSpan Interval = new TimeSpan(1, 0, 0);
            //To set 1 hour interval
            //TimeSpan Interval = new TimeSpan(1, 0, 0);           
            //fromReminderDate.Items.Clear();
            //toReminderDate.Items.Clear();
            while (StartTime <= EndTime)
            {
                listy.Add(StartTime.ToString("HH:mm"));
                StartTime = StartTime.Add(Interval);
            }
            return listy;
        }
        // Task
        [WebMethod]
        public static List<string> taskgetAttachmentData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='" + mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/" + System.IO.Path.GetFileName(item.DocumentPath) + "' /></video>";
                                listy.Add(retstring);
                            }
                            else
                            {
                                var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img src='" + imgstring + "' class='resized-filled-image' onclick='rotateMe(this);' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "taskgetAttachmentData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataTab(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#tasklocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Incident Location</p></div></div></div>";
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div></div></div>";
                                listy += retstring;
                            }
                            else
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div></div></div>";
                                listy += retstring;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "taskgetAttachmentDataTab", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataIcons(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                listy += retstring;
                            }
                            else
                            {
                                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                listy += retstring;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "taskgetAttachmentDataIcons", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableRowDataTask(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.CustomerUName);
                    listy.Add(customData.CreateDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.ACustomerUName);
                    listy.Add(customData.StatusDescription);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geolocation);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.Description);
                    listy.Add(customData.Notes);
                    listy.Add(customData.Name + "-" + customData.NewCustomerTaskId);
                    listy.Add(customData.StartDate.Value.AddHours(userinfo.TimeZone).ToShortDateString());
                    listy.Add(customData.CheckListNotes);
                    listy.Add("circle-point " + CommonUtility.getImgStatus(customData.StatusDescription));
                    var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(customData.TemplateCheckListId.ToString(), dbConnection);
                    if (parentTasks != null)
                        listy.Add(parentTasks.Name);
                    else
                        listy.Add("None");

                    //DEMO PURPOSE
                    if (customData.RejectionNotes != null)
                    {
                        if (CommonUtility.isNumeric(customData.RejectionNotes))
                        {
                            listy.Add(customData.RejectionNotes);
                            var veriData = Verifier.GetVerifierById(Convert.ToInt32(customData.RejectionNotes), dbConnection);
                            var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            var imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + veriData.Id.ToString() + ".jpeg";
                            listy.Add(imgstring);

                            var runVerify = VerifierResults.GetVerifierResultByVerifierId(veriData.Id, dbConnection);
                            foreach (var verify in runVerify)
                            {
                                imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + veriData.Id.ToString() + "/" + verify.Percent + ".jpeg";
                                listy.Add(imgstring);
                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getTableRowDataTask", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var eventData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);//EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(eventData.Id, dbConnection);

                var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(id), dbConnection);
                if (tasklinks.Count > 0)
                {
                    foreach (var link in tasklinks)
                    {
                        var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                        foreach (var forl in forlink)
                        {
                            forl.isSubTask = true;
                            taskeventhistory.Add(forl);
                        }
                    }
                    taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                }

                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                foreach (var task in taskeventhistory)
                {
                    var taskn = "Task";
                    if (task.isSubTask)
                    {
                        taskn = "Sub-task";
                    }
                    if (eventData.AssigneeType == (int)TaskAssigneeType.Group)
                    {
                        if (task.Action == (int)TaskAction.Update)
                        {
                            acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                                + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> updated<span class='red-color'>" + taskn
                                + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                            listy.Add(acceptedData);
                        }
                    }

                    if (task.Action == (int)TaskAction.Complete)
                    {
                        completedBy = task.CustomerUName;
                        var compLocation = string.Empty;
                        var geoLoc = ReverseGeocode.RetrieveFormatedAddress(eventData.EndLatitude.ToString(), eventData.EndLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc))
                            compLocation = " at " + geoLoc;
                        var compInfo = "completed";
                        var compstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + completedBy + "</span>" + compInfo + "<span class='red-color'>" + taskn
                            + "</span>" + compLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(compstring);
                    }
                    else if (task.Action == (int)TaskAction.InProgress)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "started";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.OnRoute)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "on route";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Pause)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "paused";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Resume)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "resumed";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Accepted)
                    {
                        acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> accepted<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(acceptedData);
                    }
                    else if (task.Action == (int)TaskAction.Rejected)
                    {
                        rejectedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> rejected<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(rejectedData);
                    }
                    else if (task.Action == (int)TaskAction.Assigned)
                    {
                        assignedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> assigned<span class='red-color'>" + taskn
                            + "</span> to " + task.ACustomerUName + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(assignedData);
                    }
                    else if (task.Action == (int)TaskAction.Pending)
                    {
                        var incidentLocation = string.Empty;
                        var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(eventData.Latitude.ToString(), eventData.Longitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc3))
                            incidentLocation = " at " + geoLoc3;
                        var actioninfo = "created";

                        var gUser = Users.GetUserByName(eventData.CreatedBy, dbConnection);

                        var dataString = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + gUser.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + taskn
                            + "</span>for " + task.ACustomerUName + " " + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(dataString);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTaskHistoryData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getChecklistData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var chklisty = new List<string>();
                var isAllChecked = true;
                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);


                var groupedlist = new List<TaskCheckList>();
                var groupedItems = sessions.GroupBy(x => x.Id);
                var groupedlistItems = groupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                foreach (var child in groupedlistItems)
                {
                    var attachfiles = sessions.Where(i => i.Id == child.Id).ToList();
                    var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                    if (mp3files.Count > 0)
                    {
                        //Yes
                        groupedlist.Add(mp3files[0]);
                    }
                    else
                    {
                        groupedlist.Add(attachfiles[0]);
                    }
                }

                foreach (var item in groupedlist)
                {
                    var stringCheck = string.Empty;
                    if (item.IsChecked)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                        isAllChecked = false;
                    }
                    var imgsrc = string.Empty;
                    if (!string.IsNullOrEmpty(item.DocumentPath) && System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                    {

                        imgsrc = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                    }

                    if (item.CheckListItemType == 4 || item.CheckListItemType == 5)
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|True|" + imgsrc);
                    }
                    else if (item.CheckListItemType == 3)
                    {
                        if (!string.IsNullOrEmpty(item.TemplateCheckListItemNote))
                            chklisty.Add(item.Name + "|" + stringCheck + "|3|" + item.TemplateCheckListItemNote + "|" + imgsrc);
                        else
                            chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    else
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    if (item.ChildCheckList != null)
                    {
                        //var audiofiles = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                        //var glist = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() != ".MP3").ToList();

                        var childgroupedlist = new List<TaskCheckList>();
                        var childgroupedItems = item.ChildCheckList.GroupBy(x => x.Id);
                        var childgroupedlistItems = childgroupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                        foreach (var child in childgroupedlistItems)
                        {
                            var attachfiles = item.ChildCheckList.Where(i => i.Id == child.Id).ToList();
                            var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                            if (mp3files.Count > 0)
                            {
                                //Yes
                                childgroupedlist.Add(mp3files[0]);
                            }
                            else
                            {
                                childgroupedlist.Add(attachfiles[0]);
                            }
                        }

                        foreach (var child in childgroupedlist)
                        {
                            var imgsrc2 = string.Empty;
                            if (!string.IsNullOrEmpty(child.DocumentPath) && System.IO.Path.GetExtension(child.DocumentPath).ToUpperInvariant() == ".MP3")
                            {

                                imgsrc2 = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + child.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                            }

                            if (child.IsChecked)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                                isAllChecked = false;
                            }
                            chklisty.Add(child.Name + "|" + stringCheck + "|False|" + imgsrc2);
                        }
                    }
                }
                listy.Add(isAllChecked.ToString());
                listy.AddRange(chklisty);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getChecklistData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTaskListData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            { 
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);
                if (usertask.Count > 0)
                    listy.Add(usertask[0].Name);
                foreach (var task in usertask)
                {
                    listy.Add(task.ACustomerUName + "|" + task.Id);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getTaskListData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getTaskLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            if (item.StatusDescription == "Pending")
                            {
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                            {
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "OnRoute")
                            {
                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || item.StatusDescription == "Rejected")
                            {
                                if (item.EndLatitude > 0 && item.EndLongitude > 0)
                                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string getDispatchUserList(int id, string ttype,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        json += "<a href='#' style='color:" + CommonUtility.getHexColor(0) + "' ><i  class='fa fa-check-square-o'></i>" + item.ACustomerUName + "</a>";
                    }
                    else
                    {
                        var colorCount = 0;
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        foreach (var noti in notification)
                        {

                            var tbColor = string.Empty;
                            if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                            {

                            }
                            else
                            {
                                tbColor = CommonUtility.getHexColor(colorCount);
                                assigneeList.Add(noti.AssigneeId);
                                colorCount++;
                                json += "<a href='#' style='color:" + tbColor + ";font-size:16px;' onclick='tracebackOnUser(&apos;" + id + "&apos;,&apos;" + ttype + "&apos;,&apos;" + noti.AssigneeId + "&apos;)'><i id='" + id + noti.AssigneeId + "'  class='fa fa-check-square-o'></i>" + noti.ACustomerUName + "</a>|";

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getDispatchUserList", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string getTracebackLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    var geteventhistorys = TaskEventHistory.GetTaskEventHistoryByTaskId(Convert.ToInt32(id), dbConnection);
                    var rejecteds = geteventhistorys.Where(i => i.Action == (int)TaskAction.Rejected).ToList();

                    var gotrejected = false;

                    if (rejecteds.Count > 0)
                    {
                        gotrejected = true;
                    }

                    var inprohistory = new TaskEventHistory();
                    var onroutehistory = new TaskEventHistory();

                    //if (!gotrejected)
                    //{
                    if (item.StatusDescription == "Pending")
                    {
                        if (item.Longitude > 0 && item.Latitude > 0)
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    }
                    else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                    {
                        if (item.StartLatitude > 0 && item.StartLongitude > 0)
                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                        if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                        if (item.Longitude > 0 && item.Latitude > 0)
                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                    }
                    else if (item.StatusDescription == "OnRoute")
                    {
                        if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                        json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                    }
                    else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                    {
                        if (item.EndLongitude > 0 && item.EndLatitude > 0)
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                        if (item.StartLatitude > 0 && item.StartLongitude > 0)
                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                        if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                        if (item.Longitude > 0 && item.Latitude > 0)
                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    }
                    //}
                    //else
                    //{
                    //    if (item.Longitude > 0 && item.Latitude > 0)
                    //        json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                    //    foreach (var ev in geteventhistorys)
                    //    {
                    //        if (ev.Action == (int)TaskAction.OnRoute)
                    //        {
                    //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                    //                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                    //            if (onroutehistory.Id == 0)
                    //            {
                    //                if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                    //                {
                    //                    onroutehistory = ev;
                    //                }
                    //            } 
                    //        }
                    //        else if (ev.Action == (int)TaskAction.InProgress)
                    //        {
                    //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                    //                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                    //            if (inprohistory.Id == 0)
                    //            {
                    //                if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                    //                {
                    //                    inprohistory = ev;
                    //                }
                    //            }
                    //        }
                    //        else if (ev.Action == (int)TaskAction.Complete)
                    //        {
                    //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                    //            {
                    //                if (ev.CreatedDate.Value < item.ActualEndDate.Value)
                    //                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"X\",\"Logs\" : \"Retrieve\"},";
                    //                else
                    //                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                    //            }
                    //        }
                    //    }
                    //}
                    if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || gotrejected)
                    {
                        if (traceback.Count > 0)
                        {
                            //if (gotrejected)
                            //{

                            //    if (onroutehistory.Id == 0)
                            //    {
                            //        if (!string.IsNullOrEmpty(onroutehistory.Longtitude) && !string.IsNullOrEmpty(onroutehistory.Latitude))
                            //            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + onroutehistory.Longtitude.ToString() + "\",\"Lat\" : \"" + onroutehistory.Latitude.ToString() + "\",\"LastLog\" :  \"" + onroutehistory.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            //    }
                            //    else
                            //    {
                            //        if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                            //            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            //    }

                            //    if (inprohistory.Id == 0)
                            //    {
                            //        if (!string.IsNullOrEmpty(inprohistory.Longtitude) && !string.IsNullOrEmpty(inprohistory.Latitude))
                            //            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + inprohistory.Longtitude.ToString() + "\",\"Lat\" : \"" + inprohistory.Latitude.ToString() + "\",\"LastLog\" :  \"" + inprohistory.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            //    }
                            //    else
                            //    {
                            //        if (item.StartLatitude > 0 && item.StartLongitude > 0)
                            //            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            //    }
                            //}
                            //else
                            //{
                            //    if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                            //        json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";


                            //    if (item.StartLatitude > 0 && item.StartLongitude > 0)
                            //        json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";                           
                            //}
                            foreach (var tb in traceback)
                            {
                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                if (tb.Longitude > 0 && tb.Latitude > 0)
                                    json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + tb.Marker + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                            //if (item.EndLongitude > 0 && item.EndLatitude > 0)
                            //    json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash", "getTracebackLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getTracebackLocationByDurationData(int id, int duration, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    var geteventhistorys = TaskEventHistory.GetTaskEventHistoryByTaskId(Convert.ToInt32(id), dbConnection);
                    var rejecteds = geteventhistorys.Where(i => i.Action == (int)TaskAction.Rejected).ToList();

                    var gotrejected = false;

                    var inprohistory = new TaskEventHistory();
                    var onroutehistory = new TaskEventHistory();

                    if (rejecteds.Count > 0)
                    {
                        gotrejected = true;
                    }
                    //if (!gotrejected)
                    {
                        if (item.StatusDescription == "Pending")
                        {
                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        {
                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    //else
                    //{
                    //    if (item.Longitude > 0 && item.Latitude > 0)
                    //        json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                    //    foreach (var ev in geteventhistorys)
                    //    {
                    //        if (ev.Action == (int)TaskAction.OnRoute)
                    //        {
                    //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                    //                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                    //            if (onroutehistory.Id == 0)
                    //            {
                    //                if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                    //                {
                    //                    onroutehistory = ev;
                    //                }
                    //            } 
                    //        }
                    //        else if (ev.Action == (int)TaskAction.InProgress)
                    //        {
                    //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                    //                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                    //            if (inprohistory.Id == 0)
                    //            {
                    //                if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                    //                {
                    //                    inprohistory = ev;
                    //                }
                    //            }

                    //        }
                    //        else if (ev.Action == (int)TaskAction.Complete)
                    //        {
                    //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                    //            {
                    //                if (ev.CreatedDate.Value < item.ActualEndDate.Value)
                    //                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"X\",\"Logs\" : \"Retrieve\"},";
                    //                else
                    //                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                    //            }
                    //        }
                    //    }
                    //}

                    var curTime = CommonUtility.getDTNow();
                    var firstTime = false;
                    if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                    {
                        if (traceback.Count > 0)
                        {
                            if (item.OnRouteLatitude > 0.0 && item.OnRouteLongitude > 0.0 && item.ActualOnRouteDate != null)
                            {
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + "https://testportalcdn.azureedge.net/Images/whitegreenball.png" + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                            else
                            {
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + "https://testportalcdn.azureedge.net/Images/whiteyellowball.png" + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                            foreach (var tb in traceback)
                            {
                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                if (!firstTime)
                                {
                                    curTime = tb.CreatedDate.Value;
                                    firstTime = true;
                                    json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + tb.Marker + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";

                                }
                                else
                                {
                                    if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                    {
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + tb.Marker + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                        curTime = tb.CreatedDate.Value;
                                    }
                                }
                            }

                            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + "https://testportalcdn.azureedge.net/Images/bluesmall.png" + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash", "getTracebackLocationByDurationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string getTracebackLocationDataByUser(int id, int duration, string ttype, int[] userIds,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    json += "[";
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);

                        if (item.StatusDescription == "Pending")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                             
                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        { 
                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed")
                        {
                            json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }

                        if (item.StatusDescription == "Completed")
                        {
                            var curTime = CommonUtility.getDTNow();
                            var firstTime = false;
                            if (traceback.Count > 0)
                            {
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                    if (!firstTime)
                                    {
                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                        firstTime = true;
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";

                                    }
                                    else
                                    {
                                        if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                        {
                                            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                            curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                        }
                                    }
                                }
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    else
                    {
                        var item = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (item.EventType == CustomEvent.EventTypes.Incident)
                        {
                            var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                            if (geofence.Count > 0)
                            {
                                foreach (var geo in geofence)
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                        }
                        else
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        var colorCount = 0;
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        foreach (var noti in notification)
                        {
                            var tbColor = string.Empty;
                            if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                            {

                            }
                            else
                            {
                                tbColor = CommonUtility.getHexColor(colorCount);
                                assigneeList.Add(noti.AssigneeId);
                                colorCount++;
                            }
                            if (!userIds.Contains(noti.AssigneeId))//(noti.AssigneeId != userIds[0])
                            {
                                var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                                var curTime = CommonUtility.getDTNow();
                                var firstTime = false;
                                if (traceback.Count > 0)
                                {

                                    json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                    foreach (var tb in traceback)
                                    {
                                        var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                        if (!firstTime)
                                        {
                                            curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                            firstTime = true;
                                            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";

                                        }
                                        else
                                        {
                                            if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                            {
                                                json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                            }
                                        }
                                    }
                                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getTracebackLocationDataByUser", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        //DEMO EDI

        public int Sum(List<int> customerssalary)
        {
            int result = 0;

            for (int i = 0; i < customerssalary.Count; i++)
            {
                result += customerssalary[i];
            }

            return result;
        }
        public static int Sum2(List<int> customerssalary)
        {
            int result = 0;

            for (int i = 0; i < customerssalary.Count; i++)
            {
                result += customerssalary[i];
            }

            return result;
        }

        public void getEventStatus2(Users userinfo)
        {
            try
            {
                var pendingCount = 0;
                var inprogressCount = 0;
                var completedCount = 0;
                var acceptedCount = 0;
                var firstDayOfMonth = new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1);
                var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                demoWeek1 = firstDayOfMonth.ToString("MMM dd, yyyy");
                demoWeek2 = tenthDayOfMonth.ToString("MMM dd, yyyy");
                demoWeek3 = fifteenDayOfMonth.ToString("MMM dd, yyyy");
                demoWeek4 = twentyDayOfMonth.ToString("MMM dd, yyyy");
                demoWeek5 = lastDayOfMonth.ToString("MMM dd, yyyy");
                demoMonth = CommonUtility.getDTNow().ToString("MMMM").ToUpper();
                day1week1 = 0;
                day2week1 = 0;
                day3week1 = 0;
                day4week1 = 0;
                day5week1 = 0;
                day6week1 = 0;
                day7week1 = 0;

                day1week2 = 0;
                day2week2 = 0;
                day3week2 = 0;
                day4week2 = 0;
                day5week2 = 0;
                day6week2 = 0;
                day7week2 = 0;

                day1week3 = 0;
                day2week3 = 0;
                day3week3 = 0;
                day4week3 = 0;
                day5week3 = 0;
                day6week3 = 0;
                day7week3 = 0;

                day1week4 = 0;
                day2week4 = 0;
                day3week4 = 0;
                day4week4 = 0;
                day5week4 = 0;
                day6week4 = 0;
                day7week4 = 0;
                var templateChecklistList = new List<int>();
                var totaltaskslist = new List<UserTask>();
                if (userinfo != null)
                {

                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        totaltaskslist = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Add(new TimeSpan(23, 59, 0)), dbConnection);
                    else if (userinfo.RoleId == (int)Role.Manager)
                    {
                        totaltaskslist = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);
                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        foreach (var group in userGroups)
                        {
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
                        }
                        totaltaskslist = totaltaskslist.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(CommonUtility.getDTNow().Add(new TimeSpan(23, 59, 0)), userinfo.CustomerInfoId, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                        var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var item in sessions)
                        {
                            totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                        }
                        totaltaskslist = totaltaskslist.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(CommonUtility.getDTNow().Add(new TimeSpan(23, 59, 0)), userinfo.SiteId, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                            totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(CommonUtility.getDTNow().Add(new TimeSpan(23, 59, 0)), userinfo.ID, dbConnection));

                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        foreach (var group in userGroups)
                        {
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        totaltaskslist = UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection);
                        if (userinfo.ContractLinkId > 0)
                        {
                            totaltaskslist = totaltaskslist.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                        }
                    }

                    totaltaskslist = totaltaskslist.Where(i => i.StartDate.Value.Month == CommonUtility.getDTNow().Month && i.StartDate.Value.Year == CommonUtility.getDTNow().Year).ToList();
                     
                    var grouped = totaltaskslist.GroupBy(item => item.Id);
                    totaltaskslist = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                    var userList = new List<string>();


                    var compTasks = new List<UserTask>();
                    var intList = new List<int>();
                    foreach (var task in totaltaskslist)
                    { 
                        if (task.IsRecurring && task.RecurringParentId == 0 && !task.IsTaskTemplate)
                        {

                        }
                        else
                        {
                            if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date <= CommonUtility.getDTNow().Date)
                            {
                                if (!intList.Contains(task.Id))
                                {
                                    intList.Add(task.Id);

                                    if (task.Status == (int)TaskStatus.Pending)
                                        pendingCount++;
                                    else if (task.Status == (int)TaskStatus.Completed)
                                        completedCount++;
                                    else if (task.Status == (int)TaskStatus.Accepted)
                                        acceptedCount++;
                                    else if (task.Status == (int)TaskStatus.InProgress || task.Status == (int)TaskStatus.OnRoute)
                                        inprogressCount++;

                                    if (task.Status == (int)TaskStatus.Completed || task.Status == (int)TaskStatus.Accepted)
                                    {

                                        compTasks.Add(task);
                                        userList.Add(task.ACustomerUName);

                                        if (task.StartDate != null)
                                        {
                                            if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1))
                                            {
                                                day1week1++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 2))
                                            {
                                                day2week1++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 3))
                                            {
                                                day3week1++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 4))
                                            {
                                                day4week1++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 5))
                                            {
                                                day5week1++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 6))
                                            {
                                                day6week1++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 7))
                                            {
                                                day7week1++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 8))
                                            {
                                                day1week2++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 9))
                                            {
                                                day2week2++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 10))
                                            {
                                                day3week2++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 11))
                                            {
                                                day4week2++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 12))
                                            {
                                                day5week2++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 13))
                                            {
                                                day6week2++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 14))
                                            {
                                                day7week2++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 15))
                                            {
                                                day1week3++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 16))
                                            {
                                                day2week3++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 17))
                                            {
                                                day3week3++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 18))
                                            {
                                                day4week3++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 19))
                                            {
                                                day5week3++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 20))
                                            {
                                                day6week3++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 21))
                                            {
                                                day7week3++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 22))
                                            {
                                                day1week4++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 23))
                                            {
                                                day2week4++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 24))
                                            {
                                                day3week4++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 25))
                                            {
                                                day4week4++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 26))
                                            {
                                                day5week4++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 27))
                                            {
                                                day6week4++;
                                            }
                                            else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date >= new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 28) && task.StartDate.Value.AddHours(userinfo.TimeZone).Date <= new DateTime(CommonUtility.getDTNow().Year, (CommonUtility.getDTNow().Month + 1), 1))
                                            {
                                                day7week4++;
                                            }
                                            else
                                            {

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    week1total = (day1week1+day2week1+day3week1+day4week1+day5week1+day6week1+day7week1).ToString();
                    week2total = (day1week2 + day2week2 + day3week2 + day4week2 + day5week2 + day6week2 + day7week2).ToString();
                    week3total = (day1week3 + day2week3 + day3week3 + day4week3 + day5week3 + day6week3 + day7week3).ToString();
                    week4total = (day1week4 + day2week4 + day3week4 + day4week4 + day5week4 + day6week4 + day7week4).ToString();
                    Dictionary<string, int> counts = userList.GroupBy(x => x)
                              .ToDictionary(g => g.Key,
                                            g => g.Count());
                    var items = from pair in counts
                                orderby pair.Value descending
                                select pair;
                    int count = 0;
                    foreach (KeyValuePair<string, int> pair in items)
                    {

                        if (count < 5)
                        {

                            if (count == 0)
                            {
                                Top1InsName = pair.Key.ToString();
                                Top1InsCount = pair.Value.ToString();
                            }
                            else if (count == 1)
                            {
                                Top2InsName = pair.Key.ToString();
                                Top2InsCount = pair.Value.ToString();

                            }
                            else if (count == 2)
                            {
                                Top3InsName = pair.Key.ToString();
                                Top3InsCount = pair.Value.ToString();
                            }
                            else if (count == 3)
                            {
                                Top4InsName = pair.Key.ToString();
                                Top4InsCount = pair.Value.ToString();
                            }
                            else if (count == 4)
                            {
                                Top5InsName = pair.Key.ToString();
                                Top5InsCount = pair.Value.ToString();
                            }
                            count++;
                        }
                        else
                        {
                            break;
                        }

                    }

                    var instructor1FinishTimes = new List<int>();
                    var instructor2FinishTimes = new List<int>();
                    var instructor3FinishTimes = new List<int>();
                    var instructor4FinishTimes = new List<int>();
                    var instructor5FinishTimes = new List<int>();
                    var checklistItemsWeek1 = new List<UserTask>();
                    var checklistItemsWeek2 = new List<UserTask>();
                    var checklistItemsWeek3 = new List<UserTask>();
                    var checklistItemsWeek4 = new List<UserTask>();
                    foreach (var tsk in compTasks)
                    {
                        if (tsk.TemplateCheckListId > 0)
                        {
                            if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= firstDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < tenthDayOfMonth.Date)
                            {
                                checklistItemsWeek1.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= tenthDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < fifteenDayOfMonth.Date)
                            {
                                checklistItemsWeek2.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= fifteenDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < twentyDayOfMonth.Date)
                            {
                                checklistItemsWeek3.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= twentyDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < lastDayOfMonth.Date.AddHours(23))
                            {
                                checklistItemsWeek4.Add(tsk);
                            }
                        }
                        if (tsk.ACustomerUName.ToLower() == Top1InsName.ToLower())
                        {
                            if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor1FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                        else if (tsk.ACustomerUName.ToLower() == Top2InsName.ToLower())
                        {
                            if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor2FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                        else if (tsk.ACustomerUName.ToLower() == Top3InsName.ToLower())
                        {
                            if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor3FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                        else if (tsk.ACustomerUName.ToLower() == Top4InsName.ToLower())
                        {
                            if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor4FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                        else if (tsk.ACustomerUName.ToLower() == Top5InsName.ToLower())
                        {
                            if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor5FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                    }
                    var compChecklistTasks = compTasks.Where(i => i.TemplateCheckListId > 0).ToList();

                    Dictionary<string, int> compcounts = compChecklistTasks.GroupBy(x => x.TemplateCheckListId.ToString())
                    .ToDictionary(g => g.Key,
                    g => g.Count());
                    var compitems = from pair in compcounts
                                    orderby pair.Value descending
                                    select pair;

                    int compcount = 0;
                    var tempId1 = 0;
                    var tempId2 = 0;
                    var tempId3 = 0;
                    var tempId4 = 0;
                    var tempId5 = 0;
                    foreach (KeyValuePair<string, int> pair in compitems)
                    {

                        if (compcount < 5)
                        {

                            if (compcount == 0)
                            {
                                //SkillSet1Name = pair.Key.ToString();
                                tempId1 = Convert.ToInt32(pair.Key.ToString());
                                SkillSet1Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 1)
                            {
                                tempId2 = Convert.ToInt32(pair.Key.ToString());
                                SkillSet2Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 2)
                            {
                                tempId3 = Convert.ToInt32(pair.Key.ToString());
                                SkillSet3Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 3)
                            {
                                tempId4 = Convert.ToInt32(pair.Key.ToString());
                                SkillSet4Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 4)
                            {
                                tempId5 = Convert.ToInt32(pair.Key.ToString());
                                SkillSet5Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            compcount++;
                        }
                        else
                        {
                            break;
                        }

                    }
                    foreach (var item in checklistItemsWeek1)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            SkillSet1Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            SkillSet2Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            SkillSet3Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            SkillSet4Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            SkillSet5Week1Count++;
                        }
                    }
                    foreach (var item in checklistItemsWeek2)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            SkillSet1Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            SkillSet2Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            SkillSet3Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            SkillSet4Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            SkillSet5Week2Count++;
                        }
                    }
                    foreach (var item in checklistItemsWeek3)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            SkillSet1Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            SkillSet2Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            SkillSet3Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            SkillSet4Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            SkillSet5Week3Count++;
                        }
                    }
                    foreach (var item in checklistItemsWeek4)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            SkillSet1Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            SkillSet2Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            SkillSet3Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            SkillSet4Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            SkillSet5Week4Count++;
                        }
                    }
                    var sum = Sum(instructor1FinishTimes);
                    if (instructor1FinishTimes.Count > 0)
                    {
                        taskUser1Avg = (int)Math.Round((decimal)sum / instructor1FinishTimes.Count);
                    }

                    sum = Sum(instructor2FinishTimes);
                    if (instructor2FinishTimes.Count > 0)
                    {
                        taskUser2Avg = (int)Math.Round((decimal)sum / instructor2FinishTimes.Count);
                    }
                    sum = Sum(instructor3FinishTimes);
                    if (instructor3FinishTimes.Count > 0)
                    {
                        taskUser3Avg = (int)Math.Round((decimal)sum / instructor3FinishTimes.Count);
                    }
                    sum = Sum(instructor4FinishTimes);
                    if (instructor4FinishTimes.Count > 0)
                    {
                        taskUser4Avg = (int)Math.Round((decimal)sum / instructor4FinishTimes.Count);
                    }
                    sum = Sum(instructor5FinishTimes);
                    if (instructor5FinishTimes.Count > 0)
                    {
                        taskUser5Avg = (int)Math.Round((decimal)sum / instructor5FinishTimes.Count);
                    }
                    taskUser1 = Top1InsName;
                    taskUser2 = Top2InsName;
                    taskUser3 = Top3InsName;
                    taskUser4 = Top4InsName;
                    taskUser5 = Top5InsName;

                    AvgTotal = (taskUser1Avg + taskUser2Avg + taskUser3Avg + taskUser4Avg + taskUser5Avg).ToString();

                    total = pendingCount + inprogressCount + completedCount + acceptedCount;
                    var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                    var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                    var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                    var acceptedPercentage = (int)Math.Round((float)acceptedCount / (float)total * (float)100);
                    var handledPercent = (int)Math.Round((float)(inprogressCount + completedCount) / (float)total * (float)100);
                    var plannedcount = inprogressCount + completedCount + acceptedCount;
                    ActualDemoCount = plannedcount.ToString();
                    PlannedDemoCount = total.ToString();

                    var ActualDemoPercent = (int)Math.Round((float)plannedcount / (float)total * (float)100);
                    var PlannedDemoPercent = (int)Math.Round((float)(total - plannedcount) / (float)total * (float)100);
                    ActualDemo = ActualDemoPercent;
                    PlannedDemo = PlannedDemoPercent;

                    var top1Percent = (int)Math.Round((float)Convert.ToInt32(Top1InsCount) / (float)Convert.ToInt32(ActualDemoCount) * (float)100);
                    var top2Percent = (int)Math.Round((float)Convert.ToInt32(Top2InsCount) / (float)Convert.ToInt32(ActualDemoCount) * (float)100);
                    var top3Percent = (int)Math.Round((float)Convert.ToInt32(Top3InsCount) / (float)Convert.ToInt32(ActualDemoCount) * (float)100);
                    var top4Percent = (int)Math.Round((float)Convert.ToInt32(Top4InsCount) / (float)Convert.ToInt32(ActualDemoCount) * (float)100);
                    var top5Percent = (int)Math.Round((float)Convert.ToInt32(Top5InsCount) / (float)Convert.ToInt32(ActualDemoCount) * (float)100);

                    Top1Ins = top1Percent.ToString();
                    Top2Ins = top2Percent.ToString();
                    Top3Ins = top3Percent.ToString();
                    Top4Ins = top4Percent.ToString();
                    Top5Ins = top5Percent.ToString();

                    if (handledPercent > 0)
                    {
                        handledTeamTasks = handledPercent.ToString();
                    }
                    else
                    {
                        handledTeamTasks = "0";
                    }

                    if (pendingCount > 0)
                        pendingPercentDemo = pendingPercentage.ToString();
                    else
                        pendingPercentDemo = "0";

                    if (inprogressCount > 0)
                        inprogressPercentDemo = inprogressPercentage.ToString();
                    else
                        inprogressPercentDemo = "0";

                    if (completedCount > 0)
                        completedPercentDemo = completedPercentage.ToString();
                    else
                        completedPercentDemo = "0";

                    if (acceptedCount > 0)
                        acceptedPercent = acceptedPercentage.ToString();
                    else
                        acceptedPercent = "0";

                    acceptedPercentCount = acceptedCount.ToString();
                    completedPercentDemoCount = completedCount.ToString();
                    inprogressPercentDemoCount = inprogressCount.ToString();
                    pendingPercentDemoCount = pendingCount.ToString();


                    totalTasksCount = total.ToString();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getEventStatus2", ex, dbConnection, userinfo.SiteId);
            }
        }

//        [WebMethod]
//        public static List<string> getTableDataTeamTasks(int id, string uname,Users userinfo)
//        {
//            var listy = new List<string>();
//           // var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
//            try
//            {
////                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
////HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

////                if (isValidSession == null)
////                {
////                    listy.Add("LOGOUT");
////                    return listy;
////                }

//                var fullcollection = new List<UserTask>();
//                if (userinfo != null)
//                {
//                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
//                        fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)), dbConnection);
//                    else if (userinfo.RoleId == (int)Role.Director)
//                        fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)), userinfo.SiteId, dbConnection);
//                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
//                    {
//                        fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)), userinfo.CustomerInfoId, dbConnection);
//                    }
//                    else if (userinfo.RoleId == (int)Role.Regional)
//                    {
//                        var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
//                        foreach(var site in sites)
//                            fullcollection.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)), site.SiteId, dbConnection));
//                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
//                        foreach (var group in userGroups)
//                        {
//                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
//                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
//                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
//                        }
                    
//                    }
//                    else if (userinfo.RoleId == (int)Role.Manager)
//                    {
//                        fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);

//                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
//                        foreach (var group in userGroups)
//                        {
//                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
//                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
//                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
//                        }
//                        fullcollection = fullcollection.Where(i => i.SiteId == userinfo.SiteId).ToList();
//                    }
//                    else if (userinfo.RoleId == (int)Role.Admin)
//                    {
//                        fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
//                        var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
//                        foreach (var item in sessions)
//                        {
//                            fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
//                        }
//                        fullcollection = fullcollection.Where(i => i.SiteId == userinfo.SiteId).ToList();
//                    }
//                    var grouped = fullcollection.GroupBy(item => item.Id);
//                    fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
//                    var datetodisplay = string.Empty;
//                    var imageclass = string.Empty;
//                    var spanmargin = "style='margin-left:3px;'";


//                    foreach (var item in fullcollection)
//                    {
//                        if (!item.IsTaskTemplate)
//                        {
//                            var action = string.Empty;// "<a href='#' data-target='#deleteIncidentModal'  data-toggle='modal' onclick='rowchoiceTemplate(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

//                            if (userinfo.RoleId == (int)Role.Manager)
//                            {
//                                if (item.CreatedBy != userinfo.Username)
//                                {
//                                    action = string.Empty;
//                                    spanmargin = string.Empty;
//                                }
//                            }

//                            if (item.IsRecurring && item.RecurringParentId == 0)
//                            {

//                            }
//                            else
//                            {
//                                var xdate = item.StartDate.Value.AddHours(userinfo.TimeZone);
//                                datetodisplay = xdate.ToString();//item.StartDate.ToString();
//                                if (item.StartDate.Value.AddHours(userinfo.TimeZone).Date <= CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)))
//                                {
//                                    if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled && item.AssigneeId != userinfo.ID)
//                                    {
//                                        if (item.Status == (int)TaskStatus.RejectedSaved)
//                                            item.StatusDescription = "Rejected";

//                                        if (item.Status == (int)TaskStatus.InProgress)
//                                        {
//                                            if (item.ActualStartDate != null)
//                                            {
//                                                datetodisplay = item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString();
//                                            }
//                                        }
//                                        else if (item.Status == (int)TaskStatus.Completed)
//                                        {
//                                            if (item.ActualEndDate != null)
//                                            {
//                                                datetodisplay = item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString();
//                                            }
//                                        }
//                                        var isIncident = string.Empty;
//                                        if (item.IncidentId > 0)
//                                            isIncident = "<i class='fa fa-warning mr-1x' style='font-size:22px;margin-left:-28px;'></i>";

//                                        if (item.StatusDescription == "Completed" && item.FollowUp)
//                                            item.StatusDescription = "Follow Up";

//                                        imageclass = CommonUtility.getImgStatusPriority(item.Priority);
//                                        var returnstring = "<tr role='row' class='odd'><td>" + item.NewCustomerTaskId + "</td><td class='sorting_1'>" + item.StatusDescription + "</td><td>" + item.Name + "</td><td>" + datetodisplay + "</td><td>" + item.TaskTypeName + "</td><td>" + item.ACustomerUName + "</td><td><a href='#' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + item.Id + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
//                                        listy.Add(returnstring);
//                                    }
//                                }
//                            }
//                        }
//                    }

//                }
//            }
//            catch (Exception ex)
//            {
//                MIMSLog.MIMSLogSave("Tasks", "getTableDataTeamTasks", ex, dbConnection, userinfo.SiteId);
//            }
//            return listy;
//        }
        [WebMethod]
        public static List<string> getTableDataTeamTasks(int id, string uname, Users userinfo)
        {
            //var sbuilder = new StringBuilder();
            var listy = new List<string>();
            //  var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                //                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
                //HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                //                if (isValidSession == null)
                //                {
                //                    listy.Add("LOGOUT");
                //                    return listy;
                //                }

                var fullcollection = new List<UserTask>();
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)), dbConnection);
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)), userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)), userinfo.CustomerInfoId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                            fullcollection.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 0)), userinfo.ID, dbConnection));

                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        foreach (var group in userGroups)
                        {
                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Manager)
                    {
                        fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);

                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        foreach (var group in userGroups)
                        {
                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
                            fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
                        }
                        fullcollection = fullcollection.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                        var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var item in sessions)
                        {
                            fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                        }
                        fullcollection = fullcollection.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        fullcollection = UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection);
                        if (userinfo.ContractLinkId > 0)
                        {
                            fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                        }
                    }
                    var grouped = fullcollection.GroupBy(item => item.Id);
                    fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                    var datetodisplay = string.Empty;
                    var imageclass = string.Empty;
                    foreach (var item in fullcollection)
                    {
                        if (!item.IsTaskTemplate)
                        {
                            //var action = "<a href='#' data-target='#deleteIncidentModal'  data-toggle='modal' onclick='rowchoiceTemplate(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                            //var actionCheck = "<div  class='nice-checkbox inline-block no-vmargin'><input  onchange='deleteClickOnCheckbox(&apos;" + item.Id + "&apos;)' type='checkbox' id='task" + item.Id.ToString() + "' name='niceCheck'><label style='margin-top:15px' for='task" + item.Id.ToString() + "'></label></div>";


                            var action = "<a href='#' data-target='#deleteIncidentModal'  data-toggle='modal' onclick='rowchoiceTemplate(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                            var actionCheck = "<div  class='nice-checkbox inline-block no-vmargin'><input  onchange='deleteClickOnCheckbox(&apos;" + item.Id + "&apos;)' type='checkbox' id='task" + item.Id.ToString() + "' name='niceCheck'><label style='margin-top:15px' for='task" + item.Id.ToString() + "'></label></div>";

                            var spanmargin = "style='margin-left:3px;'";

                            if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)//userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                            {
                                if (item.CreatedBy != userinfo.Username)
                                {
                                    action = string.Empty;
                                    actionCheck = string.Empty;
                                    spanmargin = string.Empty;
                                }
                            }

                            if (item.IsRecurring && item.RecurringParentId == 0)
                            {

                            }
                            else
                            {
                                datetodisplay = item.StartDate.Value.AddHours(userinfo.TimeZone).ToString();
                                if (item.StartDate.Value.AddHours(userinfo.TimeZone).Date <= CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 59)))
                                {
                                    if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled && item.AssigneeId != userinfo.ID)
                                    {
                                        if (item.Status == (int)TaskStatus.RejectedSaved)
                                            item.StatusDescription = "Rejected";

                                        if (item.Status == (int)TaskStatus.InProgress)
                                        {
                                            if (item.ActualStartDate != null)
                                            {
                                                datetodisplay = item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString();
                                            }
                                        }
                                        else if (item.Status == (int)TaskStatus.OnRoute)
                                        {
                                            if (item.ActualOnRouteDate != null)
                                            {
                                                datetodisplay = item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString();
                                            }
                                        }
                                        else if (item.Status == (int)TaskStatus.Completed)
                                        {
                                            if (item.ActualEndDate != null)
                                            {
                                                datetodisplay = item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString();
                                            }
                                        }
                                        var isIncident = string.Empty;
                                        if (item.IncidentId > 0)
                                        {
                                            isIncident = "<i class='fa fa-warning mr-1x' style='font-size:22px; '></i>";
                                            action = string.Empty;
                                        }

                                        if (item.StatusDescription == "Completed" && item.FollowUp)
                                            item.StatusDescription = "Follow Up";

                                        imageclass = CommonUtility.getImgStatusPriority(item.Priority);
                                        var returnstring = "<tr role='row' class='odd'><td>" + item.NewCustomerTaskId + "</td><td class='sorting_1'>" + item.StatusDescription + "</td><td>" + item.Name + "</td><td>" + datetodisplay + "</td><td>" + item.TaskTypeName + "</td><td>" + item.ACustomerUName + "</td><td><a href='#' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + item.Id + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                        listy.Add(returnstring); 
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTableDataTeamTasks", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }     
 

        [WebMethod]
        public static List<string> getTableDataTeamTasksByDate(string date, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var fullcollection = new List<UserTask>();
                if (!string.IsNullOrEmpty(date))
                {
                    var dtfilter = Convert.ToDateTime(date);
                    if (userinfo != null)
                    {
                        if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(dtfilter.Date.Add(new TimeSpan(23, 59, 0)), dbConnection);
                        else if (userinfo.RoleId == (int)Role.Director)
                            fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(dtfilter.Date.Add(new TimeSpan(23, 59, 0)), userinfo.SiteId, dbConnection);
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(dtfilter.Date.Add(new TimeSpan(23, 59, 0)), userinfo.CustomerInfoId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                            //foreach (var site in sites)
                                fullcollection.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(dtfilter.Date.Add(new TimeSpan(23, 59, 0)), userinfo.ID, dbConnection));


                            var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                            foreach (var group in userGroups)
                            {
                                fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                                fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
                                fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Manager)
                        {
                            fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);
                            var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                            foreach (var group in userGroups)
                            {
                                fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                                fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
                                fullcollection.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
                            }
                            fullcollection = fullcollection.Where(i => i.SiteId == userinfo.SiteId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                            var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var item in sessions)
                            {
                                fullcollection.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                            }
                            fullcollection = fullcollection.Where(i => i.SiteId == userinfo.SiteId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            fullcollection = UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection);
                            if (userinfo.ContractLinkId > 0)
                            {
                                fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                            }
                        }

                        var grouped = fullcollection.GroupBy(item => item.Id);
                        fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        var datetodisplay = string.Empty;
                        var imageclass = string.Empty;
                        foreach (var item in fullcollection)
                        {
                            if (!item.IsTaskTemplate)
                            {
                                var action = "<a href='#' data-target='#deleteIncidentModal'  data-toggle='modal' onclick='rowchoiceTemplate(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                                if (userinfo.RoleId == (int)Role.Manager)
                                {
                                    if (item.CreatedBy != userinfo.Username)
                                        action = string.Empty;

                                    if (item.UpdatedBy == userinfo.Username)
                                        action = "<a href='#' data-target='#deleteIncidentModal'  data-toggle='modal' onclick='rowchoiceTemplate(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                                }
                                if (item.IsRecurring && item.RecurringParentId == 0)
                                {

                                }
                                else
                                {
                                    datetodisplay = item.StartDate.Value.AddHours(userinfo.TimeZone).ToString();
                                    if (item.StartDate.Value.AddHours(userinfo.TimeZone).Date == dtfilter.Date)
                                    {
                                        if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled && item.AssigneeId != userinfo.ID)
                                        {
                                            if (item.Status == (int)TaskStatus.RejectedSaved)
                                                item.StatusDescription = "Rejected";

                                            if (item.Status == (int)TaskStatus.InProgress)
                                            {
                                                if (item.ActualStartDate != null)
                                                {
                                                    datetodisplay = item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                }
                                            }
                                            else if (item.Status == (int)TaskStatus.OnRoute)
                                            {
                                                if (item.ActualOnRouteDate != null)
                                                {
                                                    datetodisplay = item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                }
                                            }
                                            else if (item.Status == (int)TaskStatus.Completed)
                                            {
                                                if (item.ActualEndDate != null)
                                                {
                                                    datetodisplay = item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                }
                                            }
                                            var isIncident = string.Empty;
                                            if (item.IncidentId > 0)
                                                isIncident = "<i class='fa fa-warning mr-1x' style='font-size:22px;margin-left:-28px;'></i>";
                                            imageclass = CommonUtility.getImgStatusPriority(item.Priority);

                                            if (item.StatusDescription == "Completed" && item.FollowUp)
                                                item.StatusDescription = "Follow Up";

                                            var returnstring = "<tr role='row' class='odd'><td>" + item.NewCustomerTaskId + "</td><td class='sorting_1'>" + item.StatusDescription + "</td><td>" + item.Name + "</td><td>" + datetodisplay + "</td><td>" + item.TaskTypeName + "</td><td>" + item.ACustomerUName + "</td><td><a href='#' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + item.Id + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";


                                            listy.Add(returnstring);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash", "getTableDataTeamTasksByDate", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getRecentActivity2(int id, string uname, Users userinfo)
        {
            var listy = new List<string>();
            // var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
             /*   var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
*/
                var getrecenthistory = new List<UserTask>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    getrecenthistory = UserTask.GetAllTask(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director)
                    getrecenthistory = UserTask.GetAllTaskBySiteId(userinfo.SiteId,dbConnection);
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    getrecenthistory = UserTask.GetAllTaskByCId(userinfo.CustomerInfoId, dbConnection);
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    getrecenthistory.AddRange(UserTask.GetAllTaskByLevel5(userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Manager)
                {
                    getrecenthistory = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);

                    getrecenthistory = getrecenthistory.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    getrecenthistory.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                    var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var item in sessions)
                    {
                        getrecenthistory.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                    }
                    getrecenthistory = getrecenthistory.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {
                    getrecenthistory = UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection);
                    if (userinfo.ContractLinkId > 0)
                    {
                        getrecenthistory = getrecenthistory.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                    }
                }
                var incidentLocation = string.Empty;
                var actioninfo = string.Empty;

                var grouped = getrecenthistory.GroupBy(item => item.Id);
                getrecenthistory = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                List<UserTask> getrecenthistory2 = getrecenthistory.OrderByDescending(o => o.UpdatedDate).ToList();

                foreach (var tsk in getrecenthistory2)
                {
                    if (!tsk.IsRecurring || (tsk.IsRecurring && tsk.RecurringParentId > 0))
                    {
                        if (tsk.UpdatedDate.Value >= CommonUtility.getDTNow().Subtract(new TimeSpan(1, 0, 0)))
                        {
                            if (tsk.Status == (int)TaskStatus.Pending)
                            {
                                if (tsk.StartDate != null)
                                {
                                    if (tsk.CreateDate.Value.Date == CommonUtility.getDTNow().Date)
                                    {
                                        incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.Latitude.ToString(), tsk.Longitude.ToString(), getClientLic);

                                        actioninfo = "got assigned task";

                                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(tsk.CreateDate.Value);

                                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes + "M</p></div><div class='col-md-2' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + tsk.Id + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + tsk.ACustomerUName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                                        listy.Add(jsonstring);
                                    }
                                }
                            }
                            else if (tsk.Status == (int)TaskStatus.InProgress)
                            {
                                if (tsk.ActualStartDate != null)
                                {

                                    incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.StartLatitude.ToString(), tsk.StartLongitude.ToString(), getClientLic);

                                    actioninfo = "started progress on";

                                    var recTimeMinusNow = CommonUtility.getDTNow().Subtract(tsk.ActualStartDate.Value);

                                    var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes + "M</p></div><div class='col-md-2' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + tsk.Id + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + tsk.ACustomerUName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";
                                    if (tsk.ActualStartDate.Value.Date == CommonUtility.getDTNow().Date)
                                    {
                                        listy.Add(jsonstring);
                                    }

                                }
                            }
                            else if (tsk.Status == (int)TaskStatus.OnRoute)
                            {
                                if (tsk.ActualOnRouteDate != null)
                                { 
                                    incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.OnRouteLatitude.ToString(), tsk.OnRouteLongitude.ToString(), getClientLic);

                                    actioninfo = "onroute";

                                    var recTimeMinusNow = CommonUtility.getDTNow().Subtract(tsk.ActualOnRouteDate.Value);

                                    var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes + "M</p></div><div class='col-md-2' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + tsk.Id + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + tsk.ACustomerUName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";
                                    if (tsk.ActualOnRouteDate.Value.Date == CommonUtility.getDTNow().Date)
                                    {
                                        listy.Add(jsonstring);
                                    }

                                }
                            } 
                            else if (tsk.Status == (int)TaskStatus.Completed || tsk.Status == (int)TaskStatus.Accepted)
                            {
                                if (tsk.ActualEndDate != null)
                                {
                                    incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.EndLatitude.ToString(), tsk.EndLongitude.ToString(), getClientLic);

                                    actioninfo = "completed";

                                    var recTimeMinusNow = CommonUtility.getDTNow().Subtract(tsk.ActualEndDate.Value);

                                    var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes + "M</p></div><div class='col-md-2' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + tsk.Id + "&apos;)'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8' ><p><span>" + tsk.ACustomerUName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";
                                    if (tsk.ActualEndDate.Value.Date == CommonUtility.getDTNow().Date)
                                    {
                                        listy.Add(jsonstring);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "getRecentActivity2", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableDataOther(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            if (userinfo != null)
            {
                var allcustomEvents = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.DriverOffence, dbConnection);
                }
                else
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                }
                foreach (var item in allcustomEvents)
                {
                    var returnstring = "<tr role='row' class='odd'><td>TICKET</td><td>" + item.UserName + "</td><td>" + item.RecevieTime.ToString() + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            return listy;
        }
        //VERIFIER PART FOR EDI DEMO
        [WebMethod]
        public static string getVerifyLocationData(int id)
        {
            var json = string.Empty;
            json += "[";
            if (id > 0)
            {
                var customData = Verifier.GetVerifierById(id, dbConnection);

                if (customData != null)
                {
                    json += "{ \"Username\" : \"" + customData.TypeName + "\",\"Id\" : \"" + customData.Id.ToString() + "\",\"Long\" : \"" + customData.Longitude.ToString() + "\",\"Lat\" : \"" + customData.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                }
            }
            json = json.Substring(0, json.Length - 1);
            json += "]";
            return json;
        }
        [WebMethod]
        public static List<string> getVerifierRequestData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var customData = Verifier.GetVerifierById(id, dbConnection);
                var caseInfo = Enrollment.GetAllEnrollmentByCaseId(customData.CaseId, dbConnection);
                var newCaseId = new List<int>();
                listy.Add(customData.CreatedBy);
                listy.Add(customData.TypeName);
                listy.Add(customData.CaseId.ToString());
                listy.Add(customData.CreatedDate.ToString());
                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                listy.Add(geoLoc);

                if (caseInfo != null)
                {
                    listy.Add(caseInfo.Reason);
                    listy.Add(caseInfo.YearOfBirth);
                    listy.Add(caseInfo.Ethnicity);
                    listy.Add(caseInfo.PID);
                    listy.Add(caseInfo.Gender);
                    listy.Add(caseInfo.ListName);
                }
                else if (customData.EnrollmentResult != null)
                {
                    listy.Add(customData.EnrollmentResult.Reason);
                    listy.Add(customData.EnrollmentResult.YearOfBirth);
                    listy.Add(customData.EnrollmentResult.Ethnicity);
                    listy.Add(customData.EnrollmentResult.PID);
                    listy.Add(customData.EnrollmentResult.Gender);
                    listy.Add(customData.EnrollmentResult.ListName);
                }
                else
                {
                    listy.Add(customData.Reason);
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                }
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + ".jpeg";
                listy.Add(imgstring);

                if (customData.Request == (int)Verifier.RequestTypes.Verify)
                {
                    var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
                    if (resultImg != null)
                    {
                        string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                        listy.Add("data:image/png;base64," + base64String1);
                    }
                    var runVerify = VerifierResults.GetVerifierResultByVerifierId(customData.Id, dbConnection);
                    foreach (var verify in runVerify)
                    {
                        newCaseId.Add(verify.CaseId);
                        imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + "/" + verify.Percent + ".jpeg";
                        listy.Add(imgstring);
                        listy.Add(((int)Math.Round(Convert.ToSingle(verify.Percent) * (float)100)).ToString());
                    }
                }
                var newcaseInfo = Verifier.GetCaseInformationByCaseId(newCaseId[0], dbConnection);
                if (!string.IsNullOrEmpty(newcaseInfo))
                {
                    listy.Add(newcaseInfo);
                }
                else
                {
                    listy.Add("N/A");
                }
                foreach (var caseid in newCaseId)
                {
                    var casename = Verifier.GetCaseInformationByCaseId(caseid, dbConnection);

                    if (!string.IsNullOrEmpty(casename))
                    {
                        listy.Add(casename);
                        listy.Add(caseid.ToString());
                    }
                    else
                    {
                        listy.Add("N/A");
                        listy.Add(caseid.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection, userinfo.SiteId);
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                //{

                //    file.WriteLine(ex.Message + "|getVerifierRequestData");
                //}
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getEnrolledData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var customData = Enrollment.GetAllEnrollmentByCaseId(id, dbConnection);
            listy.Add(customData.CreatedBy);
            listy.Add(customData.TypeName);
            listy.Add(customData.CaseId.ToString());
            listy.Add(customData.CreatedDate.ToString());
            if (!string.IsNullOrEmpty(customData.Reason))
                listy.Add(customData.Reason);
            else
                listy.Add("N/A");
            listy.Add(customData.PID);
            listy.Add(customData.YearOfBirth);
            listy.Add(customData.Gender);
            listy.Add(customData.Ethnicity);
            listy.Add(customData.ListName);
            var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
            if (resultImg != null)
            {
                string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                listy.Add("data:image/png;base64," + base64String1);
            }
            else
            {
                listy.Add("Images/icon-user-default.png");
            }
            listy.Add(customData.Name);
            return listy;
        }

        [WebMethod]
        public static string rejectReDispatch(string id, string ins,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            int taskId1 = 0;
            var retVal = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                    incidentinfo.UpdatedBy = userinfo.Username;
                    incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue("reject");
                    //incidentinfo.SiteId = userinfo.SiteId;
                    incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                    incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                    var returnV = CommonUtility.CreateIncident(incidentinfo);
                    if (returnV != "")
                    {
                        taskId1 = Convert.ToInt32(returnV);
                        retVal = incidentinfo.Name;

                        if (incidentinfo.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            var getMobName = MobileHotEvent.GetMobileHotEventByGuid(incidentinfo.Identifier, dbConnection);
                            {
                                if (string.IsNullOrEmpty(getMobName.EventTypeName))
                                    retVal = "None";
                                else
                                    retVal = getMobName.EventTypeName;

                            }
                        }

                        if (taskId1 < 1)
                        {
                            taskId1 = Convert.ToInt32(id);
                        }
                        var EventHistoryEntry = new EventHistory();
                        EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                        EventHistoryEntry.CreatedBy = userinfo.Username;
                        EventHistoryEntry.EventId = taskId1;
                        EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                        EventHistoryEntry.UserName = userinfo.Username;
                        EventHistoryEntry.Remarks = ins;
                        EventHistoryEntry.SiteId = userinfo.SiteId;
                        EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                        EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);

                        if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                        {
                            var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(incidentinfo.EventId, dbConnection);
                            foreach (var noti in notifications)
                            {
                                if (noti.AssigneeType == (int)TaskAssigneeType.User)
                                {
                                    var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                    taskuser.Engaged = false;
                                    taskuser.EngagedIn = string.Empty;
                                    Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                }
                                else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                                {
                                    var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                    foreach (var taskuser in groupusers)
                                    {
                                        var splitinfo = taskuser.EngagedIn.Split('-');
                                        if (splitinfo.Length > 1)
                                        {
                                            if (splitinfo[1] == noti.Id.ToString())
                                            {
                                                taskuser.Engaged = false;
                                                taskuser.EngagedIn = string.Empty;
                                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit, true);
                                            }
                                        }
                                    }
                                }
                            }
                            Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(incidentinfo.EventId, false, dbConnection);
                            var taskinfo = UserTask.GetAllTaskByIncidentId(incidentinfo.EventId, dbConnection);
                            foreach (var task in taskinfo)
                            {
                                task.Status = (int)TaskStatus.Pending;
                                task.RejectionNotes = ins;
                                UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                            }
                        }
                        incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                        incidentinfo.UpdatedBy = userinfo.Username;
                        incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue("dispatch");
                        incidentinfo.SiteId = userinfo.SiteId;
                        incidentinfo.CustomerId = userinfo.CustomerInfoId;
                        incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                        returnV = CommonUtility.CreateIncident(incidentinfo);
                        if (returnV != "")
                        {
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = taskId1;
                            EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                            EventHistoryEntry.UserName = userinfo.Username;
                            EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);

                            retVal = incidentinfo.Name + " has been redispatched!";
                        }
                        else
                        {
                            retVal = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash.aspx", "rejectReDispatch", ex, dbConnection, userinfo.SiteId);
            }
            return retVal;
        }

        [WebMethod]
        public static string markReminderAsRead(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var reminderInfo = Reminders.GetReminderById(id, dbConnection);
                reminderInfo.IsCompleted = true;
                Reminders.InsertorUpdateReminder(reminderInfo, dbConnection, dbConnectionAudit, true);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash", "markReminderAsRead", ex, dbConnection, userinfo.SiteId);
                return "Failed to mark reminder as read";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static List<string> getChecklistNotesData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                foreach (var item in sessions)
                {
                    if (item.CheckListItemType == 5)
                    {
                        if (item.ChildCheckList != null)
                        {
                            foreach (var child in item.ChildCheckList)
                            {
                                if (!child.IsChecked)
                                    listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash", "getChecklistNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getCanvasNotesData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var count = 1;
                foreach (var item in sessions)
                {
                    if (!string.IsNullOrEmpty(item.DocumentPath))
                    {
                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                        {
                            listy.Add("Attachment " + count + "|" + item.ImageNote);
                        }
                        count++;    //listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("TaskDash", "getCanvasNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> assignToNewUserTask(int id, string userid, string rejection, string username, string uname, string type, string ttype)
        {
            var retVal = new List<string>();
            var newAuthorize = new MyAuthorize();
            var retValve = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retValve)
            {
                retVal.Add("LOGOUT");
                return retVal;
            }
            else
            {

                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    retVal.Add("LOGOUT");
                    return retVal;
                }

                try
                {
                    var tsk = UserTask.GetTaskById(id, dbConnection);
                    if ((ttype == "assign") || (type == "user" && ttype == "reassign"))
                    {
                        if (!string.IsNullOrEmpty(rejection) || tsk.Status == (int)TaskStatus.Completed)
                        {
                            tsk.RejectionNotes = rejection;
                            var rejtskHistory = new TaskEventHistory();
                            rejtskHistory.CreatedBy = userinfo.Username;
                            rejtskHistory.CreatedDate = CommonUtility.getDTNow();
                            rejtskHistory.TaskId = id;
                            rejtskHistory.SiteId = userinfo.SiteId;
                            rejtskHistory.Remarks = rejection;
                            rejtskHistory.Action = (int)TaskAction.Rejected;
                            rejtskHistory.CustomerId = userinfo.CustomerInfoId;
                            TaskEventHistory.InsertTaskEventHistory(rejtskHistory, dbConnection, dbConnectionAudit, true);
                        }

                        tsk.Status = (int)TaskStatus.Pending;
                        if (type == TaskAssigneeType.User.ToString())
                            tsk.AssigneeType = (int)TaskAssigneeType.User;
                        else if (type == TaskAssigneeType.Group.ToString())
                            tsk.AssigneeType = (int)TaskAssigneeType.Group;
                        tsk.AssigneeId = Convert.ToInt32(userid);
                        tsk.AssigneeName = username;
                        tsk.NewAssigneeName = username;


                        tsk.UpdatedDate = CommonUtility.getDTNow();
                        if (userinfo != null)
                        {
                            tsk.UpdatedBy = userinfo.Username;
                        }

                        if (type == TaskAssigneeType.User.ToString())
                        {
                            tsk.AssigneeType = (int)TaskAssigneeType.User;
                            var aUser = Users.GetUserById(tsk.AssigneeId, dbConnection);
                            if (aUser != null)
                                tsk.AssigneeName = aUser.Username;
                            else
                                tsk.AssigneeName = username;


                        }
                        else if (type == TaskAssigneeType.Group.ToString())
                        {
                            tsk.AssigneeType = (int)TaskAssigneeType.Group;

                            var aUser = Group.GetGroupById(tsk.AssigneeId, dbConnection);
                            if (aUser != null)
                                tsk.AssigneeName = aUser.Name;
                            else
                                tsk.AssigneeName = username;

                        }

                        var tskHistory = new TaskEventHistory();
                        tskHistory.CreatedBy = userinfo.Username;
                        tskHistory.CreatedDate = CommonUtility.getDTNow();
                        tskHistory.TaskId = id;
                        tskHistory.SiteId = userinfo.SiteId;
                        tskHistory.Remarks = tsk.AssigneeName;
                        tskHistory.Action = (int)TaskAction.Assigned;
                        tskHistory.CustomerId = userinfo.CustomerInfoId;
                        TaskEventHistory.InsertTaskEventHistory(tskHistory, dbConnection, dbConnectionAudit, true);
                        tsk.CustomerId = userinfo.CustomerInfoId;
                        var tId = UserTask.InsertorUpdateTask(tsk, dbConnection, dbConnectionAudit, true);
                        TraceBackHistory.DeleteTraceBackHistoryByTaskId(tsk.Id, dbConnection);
                        retVal.Add("SUCCESS");
                        if (tsk.AssigneeType == (int)TaskAssigneeType.User)
                        {
                            retVal.Add("User");
                        }
                        else
                            retVal.Add("Group");




                        retVal.Add(tsk.AssigneeName);
                        retVal.Add(tsk.AssigneeId.ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(rejection) || tsk.Status == (int)TaskStatus.Completed)
                        {
                            tsk.RejectionNotes = rejection;
                            var rejtskHistory = new TaskEventHistory();
                            rejtskHistory.CreatedBy = userinfo.Username;
                            rejtskHistory.CreatedDate = CommonUtility.getDTNow();
                            rejtskHistory.TaskId = id;
                            rejtskHistory.SiteId = userinfo.SiteId;
                            rejtskHistory.Remarks = rejection;
                            rejtskHistory.Action = (int)TaskAction.Rejected;
                            rejtskHistory.CustomerId = userinfo.CustomerInfoId;
                            TaskEventHistory.InsertTaskEventHistory(rejtskHistory, dbConnection, dbConnectionAudit, true);
                        }

                        tsk.Status = (int)TaskStatus.Pending;

                        tsk.UpdatedDate = CommonUtility.getDTNow();
                        if (userinfo != null)
                        {
                            tsk.UpdatedBy = userinfo.Username;
                        }
                        var tskHistory = new TaskEventHistory();
                        tskHistory.CreatedBy = userinfo.Username;
                        tskHistory.CreatedDate = CommonUtility.getDTNow();
                        tskHistory.TaskId = id;
                        tskHistory.SiteId = userinfo.SiteId;
                        tskHistory.CustomerId = userinfo.CustomerInfoId;
                        tskHistory.Remarks = tsk.AssigneeName;
                        tskHistory.Action = (int)TaskAction.Assigned;
                        TaskEventHistory.InsertTaskEventHistory(tskHistory, dbConnection, dbConnectionAudit, true);
                        tsk.CustomerId = userinfo.CustomerInfoId;
                        var tId = UserTask.InsertorUpdateTask(tsk, dbConnection, dbConnectionAudit, true);
                        retVal.Add("SUCCESS");
                        if (tsk.AssigneeType == (int)TaskAssigneeType.User)
                        {
                            retVal.Add("User");
                        }
                        else
                            retVal.Add("Group");

                        if (type == TaskAssigneeType.User.ToString())
                        {
                            tsk.AssigneeType = (int)TaskAssigneeType.User;
                            var aUser = Users.GetUserById(tsk.AssigneeId, dbConnection);
                            if (aUser != null)
                                tsk.AssigneeName = aUser.Username;
                            else
                                tsk.AssigneeName = username;


                        }
                        else if (type == TaskAssigneeType.Group.ToString())
                        {
                            tsk.AssigneeType = (int)TaskAssigneeType.Group;

                            var aUser = Group.GetGroupById(tsk.AssigneeId, dbConnection);
                            if (aUser != null)
                                tsk.AssigneeName = aUser.Name;
                            else
                                tsk.AssigneeName = username;

                        }

                        retVal.Add(tsk.AssigneeName);
                        retVal.Add(tsk.AssigneeId.ToString());

                        TraceBackHistory.DeleteTraceBackHistoryByTaskId(tsk.Id, dbConnection);
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Tasks", "assignToNewUserTask", err, dbConnection, userinfo.SiteId);
                    retVal.Add(err.Message);
                }
                return retVal;
            }
        }
         

        [WebMethod]
        public static string ChangeTaskState(int id, string state, string rejection, string uname)
        {
            var retVal = "";
            var newAuthorize = new MyAuthorize();
            var retValve = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retValve)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var tsk = UserTask.GetTaskById(id, dbConnection);
                    var tskhistory = new TaskEventHistory();
                    if (state == "Accept")
                    {
                        tsk.Status = (int)TaskStatus.Accepted;
                        tskhistory.Action = (int)TaskAction.Accepted;
                        tskhistory.CreatedBy = userinfo.Username;
                        tskhistory.CreatedDate = CommonUtility.getDTNow();
                        tskhistory.TaskId = id;
                        tskhistory.SiteId = userinfo.SiteId;
                        tskhistory.CustomerId = userinfo.CustomerInfoId;
                        TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                        if (tsk.TaskLinkId == 0)
                        {
                            var gtsklinks = UserTask.GetAllTasksByTaskLinkId(tsk.Id, dbConnection);
                            if (gtsklinks.Count > 0)
                            {
                                foreach (var links in gtsklinks)
                                {
                                    links.Status = (int)TaskStatus.Accepted;
                                    links.UpdatedDate = CommonUtility.getDTNow();
                                    if (userinfo != null)
                                        links.UpdatedBy = userinfo.Username;
                                    UserTask.InsertorUpdateTask(links, dbConnection, dbConnectionAudit, true);

                                    var ltskhistory = new TaskEventHistory();
                                    ltskhistory.Action = (int)TaskAction.Accepted;
                                    ltskhistory.CreatedBy = userinfo.Username;
                                    ltskhistory.CreatedDate = CommonUtility.getDTNow();
                                    ltskhistory.TaskId = links.Id;
                                    ltskhistory.SiteId = userinfo.SiteId;
                                    ltskhistory.CustomerId = userinfo.CustomerInfoId;
                                    TaskEventHistory.InsertTaskEventHistory(ltskhistory, dbConnection, dbConnectionAudit, true);
                                }
                            }
                        }
                    }
                    else if (state == "Close")
                    {
                        tsk.Status = (int)TaskStatus.Rejected;
                        tskhistory.Action = (int)TaskAction.Rejected;
                        tskhistory.CreatedBy = userinfo.Username;
                        tskhistory.CreatedDate = CommonUtility.getDTNow();
                        tskhistory.TaskId = id;
                        tskhistory.SiteId = userinfo.SiteId;
                        tskhistory.CustomerId = userinfo.CustomerInfoId;
                        TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                    }
                    else if (state == "Reassign")
                    {
                        tsk.Status = (int)TaskStatus.Pending;
                        tskhistory.Action = (int)TaskAction.Assigned;
                        tskhistory.CreatedBy = userinfo.Username;
                        tskhistory.CreatedDate = CommonUtility.getDTNow();
                        tskhistory.TaskId = id;
                        tskhistory.SiteId = userinfo.SiteId;
                        tskhistory.CustomerId = userinfo.CustomerInfoId;
                        TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        tsk.Status = (int)TaskStatus.RejectedSaved;
                    }
                    if (!string.IsNullOrEmpty(rejection))
                    {
                        tsk.RejectionNotes = rejection;
                    }
                    tsk.UpdatedDate = CommonUtility.getDTNow();
                    if (userinfo != null)
                        tsk.UpdatedBy = userinfo.Username;


                    tsk.CustomerId = userinfo.CustomerInfoId;
                    var tId = UserTask.InsertorUpdateTask(tsk, dbConnection, dbConnectionAudit, true);
                    retVal = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Tasks", "ChangeTaskState", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
                return retVal;
            }
        }

        [WebMethod]
        public static List<string> getEventStatusRetrieve(string fromdate, string todate, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var fromDt = Convert.ToDateTime(fromdate);
                var toDt = Convert.ToDateTime(todate);
                toDt = toDt.Add(new TimeSpan(23, 59, 0));
                var intList = new List<int>();
                if (fromDt < toDt)
                {
                    var pendingCount = 0;
                    var inprogressCount = 0;
                    var completedCount = 0;
                    var acceptedCount = 0;
                    var total = 0;
                    var totaltaskslist = new List<UserTask>();

                    int zday1week1 = 0;
                    int zday2week1 = 0;
                    int zday3week1 = 0;
                    int zday4week1 = 0;
                    int zday5week1 = 0;
                    int zday6week1 = 0;
                    int zday7week1 = 0;

                    int zday1week2 = 0;
                    int zday2week2 = 0;
                    int zday3week2 = 0;
                    int zday4week2 = 0;
                    int zday5week2 = 0;
                    int zday6week2 = 0;
                    int zday7week2 = 0;

                    int zday1week3 = 0;
                    int zday2week3 = 0;
                    int zday3week3 = 0;
                    int zday4week3 = 0;
                    int zday5week3 = 0;
                    int zday6week3 = 0;
                    int zday7week3 = 0;

                    int zday1week4 = 0;
                    int zday2week4 = 0;
                    int zday3week4 = 0;
                    int zday4week4 = 0;
                    int zday5week4 = 0;
                    int zday6week4 = 0;
                    int zday7week4 = 0;

                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        totaltaskslist = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(toDt, dbConnection);
                    else if (userinfo.RoleId == (int)Role.Director)
                        totaltaskslist = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(toDt, userinfo.SiteId, dbConnection);
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(toDt, userinfo.ID, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Manager)
                    {
                        totaltaskslist = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);
                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        foreach (var group in userGroups)
                        {
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
                        }
                        totaltaskslist = totaltaskslist.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(toDt, userinfo.CustomerInfoId, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                        var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var item in sessions)
                        {
                            totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                        }
                        totaltaskslist = totaltaskslist.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection));
                        if (userinfo.ContractLinkId > 0)
                            totaltaskslist = totaltaskslist.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                    }
                     
                    var fullcollection2 = UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, userinfo.ID, (int)TaskStatus.Accepted, dbConnection);

                    fullcollection2 = fullcollection2.Where(i => i.Status == (int)TaskStatus.Accepted && i.CreatedBy == userinfo.Username && i.AssigneeId == userinfo.ID && i.AssigneeType == (int)TaskAssigneeType.User).ToList();

                    totaltaskslist.AddRange(fullcollection2);

                    //totaltaskslist = totaltaskslist.Where(i => (i.StartDate.Value.Month == toDt.Month && i.StartDate.Value.Year == toDt.Year) || (i.StartDate.Value.Month == fromDt.Month && i.StartDate.Value.Year == fromDt.Year)).ToList();

                    totaltaskslist = totaltaskslist.Where(i => i.StartDate.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.StartDate.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();
                    var grouped = totaltaskslist.GroupBy(item => item.Id);
                    totaltaskslist = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                    var compTasks = new List<UserTask>();
                    var userList = new List<string>();

                    
                    foreach (var task in totaltaskslist)
                    {
                        var completepass = false;
                        if (task.IsRecurring && task.RecurringParentId == 0)
                        {

                        }
                        else
                        {
                            if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && task.StartDate.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date)
                            {
                                if (task.AssigneeId != userinfo.ID && !task.IsTaskTemplate)
                                {
                                    if (!intList.Contains(task.Id))
                                    {
                                        intList.Add(task.Id);

                                        if (task.Status == (int)TaskStatus.Pending)
                                            pendingCount++;
                                        else if (task.Status == (int)TaskStatus.Completed)
                                            completedCount++;
                                        else if (task.Status == (int)TaskStatus.Accepted)
                                            acceptedCount++;
                                        else if (task.Status == (int)TaskStatus.InProgress || task.Status == (int)TaskStatus.OnRoute)
                                            inprogressCount++;

                                        completepass = true;
                                    }
                                }
                                else if (task.Status == (int)TaskStatus.Accepted && task.AssigneeId == userinfo.ID && task.CreatedBy == userinfo.Username && task.AssigneeType == (int)TaskAssigneeType.User)
                                {
                                    if (!intList.Contains(task.Id))
                                    {
                                        intList.Add(task.Id);
                                        acceptedCount++;

                                        completepass = true;
                                    }
                                }
                                if ((task.Status == (int)TaskStatus.Completed || task.Status == (int)TaskStatus.Accepted) && completepass)
                                {

                                    compTasks.Add(task);
                                    userList.Add(task.ACustomerUName);

                                    if (task.StartDate != null)
                                    {
                                        if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 1))
                                        {
                                            zday1week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 2))
                                        {
                                            zday2week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 3))
                                        {
                                            zday3week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 4))
                                        {
                                            zday4week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 5))
                                        {
                                            zday5week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 6))
                                        {
                                            zday6week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 7))
                                        {
                                            zday7week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 8))
                                        {
                                            zday1week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 9))
                                        {
                                            zday2week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 10))
                                        {
                                            zday3week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 11))
                                        {
                                            zday4week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 12))
                                        {
                                            zday5week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 13))
                                        {
                                            zday6week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 14))
                                        {
                                            zday7week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 15))
                                        {
                                            zday1week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 16))
                                        {
                                            zday2week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 17))
                                        {
                                            zday3week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 18))
                                        {
                                            zday4week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 19))
                                        {
                                            zday5week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 20))
                                        {
                                            zday6week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 21))
                                        {
                                            zday7week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 22))
                                        {
                                            zday1week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 23))
                                        {
                                            zday2week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 24))
                                        {
                                            zday3week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 25))
                                        {
                                            zday4week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 26))
                                        {
                                            zday5week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 27))
                                        {
                                            zday6week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date >= new DateTime(fromDt.Year, fromDt.Month, 28) && task.StartDate.Value.AddHours(userinfo.TimeZone).Date <= new DateTime(fromDt.Year, (fromDt.Month + 1), 1))
                                        {
                                            zday7week4++;
                                        }
                                        else
                                        {

                                        }
                                    }
                                }

                            }
                        }
                    } 
                    var zTop1InsName = string.Empty;
                    var zTop2InsName = string.Empty;
                    var zTop3InsName = string.Empty;
                    var zTop4InsName = string.Empty;
                    var zTop5InsName = string.Empty;

                    var zTop1InsCount = "0";
                    var zTop2InsCount = "0";
                    var zTop3InsCount = "0";
                    var zTop4InsCount = "0";
                    var zTop5InsCount = "0";

                    Dictionary<string, int> counts = userList.GroupBy(x => x)
          .ToDictionary(g => g.Key,
                        g => g.Count());
                    var items = from pair in counts
                                orderby pair.Value descending
                                select pair;
                    int count = 0;
                    foreach (KeyValuePair<string, int> pair in items)
                    {

                        if (count < 5)
                        {

                            if (count == 0)
                            {
                                zTop1InsName = pair.Key.ToString();
                                zTop1InsCount = pair.Value.ToString();
                            }
                            else if (count == 1)
                            {
                                zTop2InsName = pair.Key.ToString();
                                zTop2InsCount = pair.Value.ToString();

                            }
                            else if (count == 2)
                            {
                                zTop3InsName = pair.Key.ToString();
                                zTop3InsCount = pair.Value.ToString();
                            }
                            else if (count == 3)
                            {
                                zTop4InsName = pair.Key.ToString();
                                zTop4InsCount = pair.Value.ToString();
                            }
                            else if (count == 4)
                            {
                                zTop5InsName = pair.Key.ToString();
                                zTop5InsCount = pair.Value.ToString();
                            }
                            count++;
                        }
                        else
                        {
                            break;
                        }

                    }

                    var instructor1FinishTimes = new List<int>();
                    var instructor2FinishTimes = new List<int>();
                    var instructor3FinishTimes = new List<int>();
                    var instructor4FinishTimes = new List<int>();
                    var instructor5FinishTimes = new List<int>();
                    var checklistItemsWeek1 = new List<UserTask>();
                    var checklistItemsWeek2 = new List<UserTask>();
                    var checklistItemsWeek3 = new List<UserTask>();
                    var checklistItemsWeek4 = new List<UserTask>();

                    var firstDayOfMonth = new DateTime(fromDt.Year, fromDt.Month, 1);
                    var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                    var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                    var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                    foreach (var tsk in compTasks)
                    {
                        if (tsk.TemplateCheckListId > 0)
                        {
                            if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= firstDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < tenthDayOfMonth.Date)
                            {
                                checklistItemsWeek1.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= tenthDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < fifteenDayOfMonth.Date)
                            {
                                checklistItemsWeek2.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= fifteenDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < twentyDayOfMonth.Date)
                            {
                                checklistItemsWeek3.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= twentyDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < lastDayOfMonth.Date.AddHours(23))
                            {
                                checklistItemsWeek4.Add(tsk);
                            }
                        }
                        if (tsk.ACustomerUName.ToLower() == zTop1InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor1FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor1FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                        else if (tsk.ACustomerUName.ToLower() == zTop2InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor2FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor2FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                        else if (tsk.ACustomerUName.ToLower() == zTop3InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor3FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor3FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }

                        }
                        else if (tsk.ACustomerUName.ToLower() == zTop4InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor4FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor4FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                        else if (tsk.ACustomerUName.ToLower() == zTop5InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor5FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor5FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                    }
                    var compChecklistTasks = compTasks.Where(i => i.TemplateCheckListId > 0).ToList();

                    Dictionary<string, int> compcounts = compChecklistTasks.GroupBy(x => x.TemplateCheckListId.ToString())
                    .ToDictionary(g => g.Key,
                    g => g.Count());
                    var compitems = from pair in compcounts
                                    orderby pair.Value descending
                                    select pair;

                    int compcount = 0;
                    var tempId1 = 0;
                    var tempId2 = 0;
                    var tempId3 = 0;
                    var tempId4 = 0;
                    var tempId5 = 0;

                    var zSkillSet1Name = string.Empty;
                    var zSkillSet2Name = string.Empty;
                    var zSkillSet3Name = string.Empty;
                    var zSkillSet4Name = string.Empty;
                    var zSkillSet5Name = string.Empty;

                    foreach (KeyValuePair<string, int> pair in compitems)
                    {

                        if (compcount < 5)
                        {

                            if (compcount == 0)
                            {
                                //SkillSet1Name = pair.Key.ToString();
                                tempId1 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet1Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 1)
                            {
                                tempId2 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet2Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 2)
                            {
                                tempId3 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet3Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 3)
                            {
                                tempId4 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet4Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 4)
                            {
                                tempId5 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet5Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            compcount++;
                        }
                        else
                        {
                            break;
                        }

                    }
                    var zSkillSet1Week1Count = 0;
                    var zSkillSet2Week1Count = 0;
                    var zSkillSet3Week1Count = 0;
                    var zSkillSet4Week1Count = 0;
                    var zSkillSet5Week1Count = 0;
                     
                    foreach (var item in checklistItemsWeek1)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            zSkillSet1Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            zSkillSet2Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            zSkillSet3Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            zSkillSet4Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            zSkillSet5Week1Count++;
                        }
                    }

                    var zSkillSet1Week2Count = 0;
                    var zSkillSet2Week2Count = 0;
                    var zSkillSet3Week2Count = 0;
                    var zSkillSet4Week2Count = 0;
                    var zSkillSet5Week2Count = 0;

                    foreach (var item in checklistItemsWeek2)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            zSkillSet1Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            zSkillSet2Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            zSkillSet3Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            zSkillSet4Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            zSkillSet5Week2Count++;
                        }
                    }

                    var zSkillSet1Week3Count = 0;
                    var zSkillSet2Week3Count = 0;
                    var zSkillSet3Week3Count = 0;
                    var zSkillSet4Week3Count = 0;
                    var zSkillSet5Week3Count = 0;

                    foreach (var item in checklistItemsWeek3)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            zSkillSet1Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            zSkillSet2Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            zSkillSet3Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            zSkillSet4Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            zSkillSet5Week3Count++;
                        }
                    }

                    var zSkillSet1Week4Count = 0;
                    var zSkillSet2Week4Count = 0;
                    var zSkillSet3Week4Count = 0;
                    var zSkillSet4Week4Count = 0;
                    var zSkillSet5Week4Count = 0;

                    foreach (var item in checklistItemsWeek4)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            zSkillSet1Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            zSkillSet2Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            zSkillSet3Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            zSkillSet4Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            zSkillSet5Week4Count++;
                        }
                    }

                    var ztaskUser1Avg = 0;
                    var ztaskUser2Avg = 0;
                    var ztaskUser3Avg = 0;
                    var ztaskUser4Avg = 0;
                    var ztaskUser5Avg = 0;

                    var sum = Sum2(instructor1FinishTimes);

                    if (instructor1FinishTimes.Count > 0)
                    {
                        ztaskUser1Avg = (int)Math.Round((decimal)sum / instructor1FinishTimes.Count);
                    }

                    sum = Sum2(instructor2FinishTimes);
                    if (instructor2FinishTimes.Count > 0)
                    {
                        ztaskUser2Avg = (int)Math.Round((decimal)sum / instructor2FinishTimes.Count);
                    }
                    sum = Sum2(instructor3FinishTimes);
                    if (instructor3FinishTimes.Count > 0)
                    {
                        ztaskUser3Avg = (int)Math.Round((decimal)sum / instructor3FinishTimes.Count);
                    }
                    sum = Sum2(instructor4FinishTimes);
                    if (instructor4FinishTimes.Count > 0)
                    {
                        ztaskUser4Avg = (int)Math.Round((decimal)sum / instructor4FinishTimes.Count);
                    }
                    sum = Sum2(instructor5FinishTimes);
                    if (instructor5FinishTimes.Count > 0)
                    {
                        ztaskUser5Avg = (int)Math.Round((decimal)sum / instructor5FinishTimes.Count);
                    }
                    var ztaskUser1 = zTop1InsName;
                    var ztaskUser2 = zTop2InsName;
                    var ztaskUser3 = zTop3InsName;
                    var ztaskUser4 = zTop4InsName;
                    var ztaskUser5 = zTop5InsName;

                    var zAvgTotal = (ztaskUser1Avg + ztaskUser2Avg + ztaskUser3Avg + ztaskUser4Avg + ztaskUser5Avg).ToString();
                     
                    total = pendingCount + inprogressCount + completedCount + acceptedCount;
                    var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                    var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                    var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                    var acceptedPercentage = (int)Math.Round((float)acceptedCount / (float)total * (float)100);
                    var pendingPercent = string.Empty;
                    var inprogressPercent = string.Empty;
                    var completedPercent = string.Empty;
                    var acceptedPercent = string.Empty;

                    if (pendingCount > 0)
                        pendingPercent = pendingPercentage.ToString();
                    else
                        pendingPercent = "0";

                    if (inprogressCount > 0)
                        inprogressPercent = inprogressPercentage.ToString();
                    else
                        inprogressPercent = "0";

                    if (completedCount > 0)
                        completedPercent = completedPercentage.ToString();
                    else
                        completedPercent = "0";

                    if (acceptedCount > 0)
                        acceptedPercent = acceptedPercentage.ToString();
                    else
                        acceptedPercent = "0";
                     
                    var zActualDemoCount = total.ToString();

                    var top1Percent = (int)Math.Round((float)Convert.ToInt32(zTop1InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);
                    var top2Percent = (int)Math.Round((float)Convert.ToInt32(zTop2InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);
                    var top3Percent = (int)Math.Round((float)Convert.ToInt32(zTop3InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);
                    var top4Percent = (int)Math.Round((float)Convert.ToInt32(zTop4InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);
                    var top5Percent = (int)Math.Round((float)Convert.ToInt32(zTop5InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);

                    if (top1Percent < 0)
                    {
                        top1Percent = 0;
                    }
                    if (top2Percent < 0)
                    {
                        top2Percent = 0;
                    }
                    if (top3Percent < 0)
                    {
                        top3Percent = 0;
                    }
                    if (top4Percent < 0)
                    {
                        top4Percent = 0;
                    }
                    if (top5Percent < 0)
                    {
                        top5Percent = 0;
                    }

                    var zTop1Ins = top1Percent.ToString() + "%";
                    var zTop2Ins = top2Percent.ToString() + "%";
                    var zTop3Ins = top3Percent.ToString() + "%";
                    var zTop4Ins = top4Percent.ToString() + "%";
                    var zTop5Ins = top5Percent.ToString() + "%";
                     
                    listy.Add(acceptedCount.ToString());
                    listy.Add(acceptedPercent.ToString());
                    listy.Add(completedCount.ToString());
                    listy.Add(completedPercent.ToString());
                    listy.Add(inprogressCount.ToString());
                    listy.Add(inprogressPercent.ToString());
                    listy.Add(pendingCount.ToString());
                    listy.Add(pendingPercent.ToString());
                    listy.Add(total.ToString());

                    listy.Add(zday1week1.ToString());
                    listy.Add(zday1week2.ToString());
                    listy.Add(zday1week3.ToString());
                    listy.Add(zday1week4.ToString());

                    listy.Add(zday2week1.ToString());
                    listy.Add(zday2week2.ToString());
                    listy.Add(zday2week3.ToString());
                    listy.Add(zday2week4.ToString());

                    listy.Add(zday3week1.ToString());
                    listy.Add(zday3week2.ToString());
                    listy.Add(zday3week3.ToString());
                    listy.Add(zday3week4.ToString());

                    listy.Add(zday4week1.ToString());
                    listy.Add(zday4week2.ToString());
                    listy.Add(zday4week3.ToString());
                    listy.Add(zday4week4.ToString());

                    listy.Add(zday5week1.ToString());
                    listy.Add(zday5week2.ToString());
                    listy.Add(zday5week3.ToString());
                    listy.Add(zday5week4.ToString());

                    listy.Add(zday6week1.ToString());
                    listy.Add(zday6week2.ToString());
                    listy.Add(zday6week3.ToString());
                    listy.Add(zday6week4.ToString());

                    listy.Add(zday7week1.ToString());
                    listy.Add(zday7week2.ToString());
                    listy.Add(zday7week3.ToString());
                    listy.Add(zday7week4.ToString());
                     
                    listy.Add(firstDayOfMonth.ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(1).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(2).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(3).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(4).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(5).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(6).ToString("yyyy-MM-dd"));

                    var zweek1total = (zday1week1 + zday2week1 + zday3week1 + zday4week1 + zday5week1 + zday6week1 + zday7week1).ToString();
                    var zweek2total = (zday1week2 + zday2week2 + zday3week2 + zday4week2 + zday5week2 + zday6week2 + zday7week2).ToString();
                    var zweek3total = (zday1week3 + zday2week3 + zday3week3 + zday4week3 + zday5week3 + zday6week3 + zday7week3).ToString();
                    var zweek4total = (zday1week4 + zday2week4 + zday3week4 + zday4week4 + zday5week4 + zday6week4 + zday7week4).ToString();
                    listy.Add("WEEK 1 TOTAL " + zweek1total);
                    listy.Add("WEEK 2 TOTAL " + zweek2total);
                    listy.Add("WEEK 3 TOTAL " + zweek3total);
                    listy.Add("WEEK 4 TOTAL " + zweek4total);

                    listy.Add(ztaskUser1);
                    listy.Add(ztaskUser2);
                    listy.Add(ztaskUser3);
                    listy.Add(ztaskUser4);
                    listy.Add(ztaskUser5);

                    listy.Add(ztaskUser1Avg.ToString());
                    listy.Add(ztaskUser2Avg.ToString());
                    listy.Add(ztaskUser3Avg.ToString());
                    listy.Add(ztaskUser4Avg.ToString());
                    listy.Add(ztaskUser5Avg.ToString());

                    listy.Add(zSkillSet1Name);
                    listy.Add(zSkillSet2Name);
                    listy.Add(zSkillSet3Name);
                    listy.Add(zSkillSet4Name);
                    listy.Add(zSkillSet5Name);

                    listy.Add(zSkillSet1Week1Count.ToString());
                    listy.Add(zSkillSet1Week2Count.ToString());
                    listy.Add(zSkillSet1Week3Count.ToString());
                    listy.Add(zSkillSet1Week4Count.ToString());


                    listy.Add(zSkillSet2Week1Count.ToString());
                    listy.Add(zSkillSet2Week2Count.ToString());
                    listy.Add(zSkillSet2Week3Count.ToString());
                    listy.Add(zSkillSet2Week4Count.ToString());

                    listy.Add(zSkillSet3Week1Count.ToString());
                    listy.Add(zSkillSet3Week2Count.ToString());
                    listy.Add(zSkillSet3Week3Count.ToString());
                    listy.Add(zSkillSet3Week4Count.ToString());

                    listy.Add(zSkillSet4Week1Count.ToString()); 
                    listy.Add(zSkillSet4Week2Count.ToString());
                    listy.Add(zSkillSet4Week3Count.ToString()); 
                    listy.Add(zSkillSet4Week4Count.ToString());

                    listy.Add(zSkillSet5Week1Count.ToString());
                    listy.Add(zSkillSet5Week2Count.ToString());
                    listy.Add(zSkillSet5Week3Count.ToString());
                    listy.Add(zSkillSet5Week4Count.ToString());


                    listy.Add(zTop1Ins);
                    listy.Add(zTop2Ins);
                    listy.Add(zTop3Ins);
                    listy.Add(zTop4Ins);
                    listy.Add(zTop5Ins);

                    listy.Add(zTop1InsName);
                    listy.Add(zTop2InsName);
                    listy.Add(zTop3InsName);
                    listy.Add(zTop4InsName);
                    listy.Add(zTop5InsName);

                    listy.Add(zTop1InsCount);
                    listy.Add(zTop2InsCount);
                    listy.Add(zTop3InsCount);
                    listy.Add(zTop4InsCount);
                    listy.Add(zTop5InsCount);

                    listy.Add(fromDt.ToString("MMMM").ToUpper());
                    listy.Add("Average Total :"+zAvgTotal);
                }
                else
                {
                    listy.Add("ERROR");
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getEventStatusRetrieve", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add("ERROR");
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getEventStatusTotal(string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (userinfo != null)
                {
                    var pendingCount = 0;
                    var inprogressCount = 0;
                    var completedCount = 0;
                    var acceptedCount = 0;
                    var total = 0;
                    var totaltaskslist = new List<UserTask>();

                    var intList = new List<int>();

                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        totaltaskslist = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Add(new TimeSpan(23, 59, 0)), dbConnection);
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        totaltaskslist = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(CommonUtility.getDTNow().Add(new TimeSpan(23, 59, 0)), userinfo.SiteId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(CommonUtility.getDTNow().Add(new TimeSpan(23, 59, 0)), userinfo.ID, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Manager)
                    {
                        totaltaskslist = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);
                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        foreach (var group in userGroups)
                        {
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.Completed, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.Group, group.Id, (int)TaskStatus.RejectedSaved, dbConnection));
                        }
                        totaltaskslist = totaltaskslist.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(CommonUtility.getDTNow().Add(new TimeSpan(23, 59, 0)), userinfo.CustomerInfoId, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                        var sessions = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var item in sessions)
                        {
                            totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(item, dbConnection));
                        }
                        totaltaskslist = totaltaskslist.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        totaltaskslist.AddRange(UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection));
                        if (userinfo.ContractLinkId > 0)
                            totaltaskslist = totaltaskslist.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                    }
                     
                    var fullcollection2 = UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, userinfo.ID, (int)TaskStatus.Accepted, dbConnection);

                    fullcollection2 = fullcollection2.Where(i => i.Status == (int)TaskStatus.Accepted && i.CreatedBy == userinfo.Username && i.AssigneeId == userinfo.ID && i.AssigneeType == (int)TaskAssigneeType.User).ToList();

                    totaltaskslist.AddRange(fullcollection2);

                    var grouped = totaltaskslist.GroupBy(item => item.Id);
                    totaltaskslist = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                    totaltaskslist = totaltaskslist.Where(i => i.StartDate.Value.Month == CommonUtility.getDTNow().Month && i.StartDate.Value.Year == CommonUtility.getDTNow().Year).ToList();
                     
                    var compTasks = new List<UserTask>();
                    var userList = new List<string>();
                    int zday1week1 = 0;
                    int zday2week1 = 0;
                    int zday3week1 = 0;
                    int zday4week1 = 0;
                    int zday5week1 = 0;
                    int zday6week1 = 0;
                    int zday7week1 = 0;

                    int zday1week2 = 0;
                    int zday2week2 = 0;
                    int zday3week2 = 0;
                    int zday4week2 = 0;
                    int zday5week2 = 0;
                    int zday6week2 = 0;
                    int zday7week2 = 0;

                    int zday1week3 = 0;
                    int zday2week3 = 0;
                    int zday3week3 = 0;
                    int zday4week3 = 0;
                    int zday5week3 = 0;
                    int zday6week3 = 0;
                    int zday7week3 = 0;

                    int zday1week4 = 0;
                    int zday2week4 = 0;
                    int zday3week4 = 0;
                    int zday4week4 = 0;
                    int zday5week4 = 0;
                    int zday6week4 = 0;
                    int zday7week4 = 0;
                    var fromDt = CommonUtility.getDTNow().Date;
                    foreach (var task in totaltaskslist)
                    {
                        var completepass = false;
                        if (task.IsRecurring && task.RecurringParentId == 0)
                        {

                        }
                        else
                        {
                            if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date <= CommonUtility.getDTNow().Date.Add(new TimeSpan(23, 59, 59)))
                            {
                                if (task.AssigneeId != userinfo.ID && !task.IsTaskTemplate)
                                {
                                    if (!intList.Contains(task.Id))
                                    {
                                        intList.Add(task.Id);
                                        if (task.Status == (int)TaskStatus.Pending)
                                            pendingCount++;
                                        else if (task.Status == (int)TaskStatus.Completed)
                                            completedCount++;
                                        else if (task.Status == (int)TaskStatus.Accepted)
                                            acceptedCount++;
                                        else if (task.Status == (int)TaskStatus.InProgress || task.Status == (int)TaskStatus.OnRoute)
                                            inprogressCount++;

                                        completepass = true;
                                    }
                                }
                                else if (task.Status == (int)TaskStatus.Accepted && task.AssigneeId == userinfo.ID && task.CreatedBy == userinfo.Username && task.AssigneeType == (int)TaskAssigneeType.User)
                                {
                                    if (!intList.Contains(task.Id))
                                    {
                                        intList.Add(task.Id);
                                        acceptedCount++;

                                        completepass = true;
                                    }
                                }


                                if ((task.Status == (int)TaskStatus.Completed || task.Status == (int)TaskStatus.Accepted) && completepass)
                                {

                                    compTasks.Add(task);
                                    userList.Add(task.ACustomerUName);

                                    if (task.StartDate != null)
                                    {
                                        if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 1))
                                        {
                                            zday1week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 2))
                                        {
                                            zday2week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 3))
                                        {
                                            zday3week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 4))
                                        {
                                            zday4week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 5))
                                        {
                                            zday5week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 6))
                                        {
                                            zday6week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 7))
                                        {
                                            zday7week1++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 8))
                                        {
                                            zday1week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 9))
                                        {
                                            zday2week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 10))
                                        {
                                            zday3week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 11))
                                        {
                                            zday4week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 12))
                                        {
                                            zday5week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 13))
                                        {
                                            zday6week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 14))
                                        {
                                            zday7week2++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 15))
                                        {
                                            zday1week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 16))
                                        {
                                            zday2week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 17))
                                        {
                                            zday3week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 18))
                                        {
                                            zday4week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 19))
                                        {
                                            zday5week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 20))
                                        {
                                            zday6week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 21))
                                        {
                                            zday7week3++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 22))
                                        {
                                            zday1week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 23))
                                        {
                                            zday2week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 24))
                                        {
                                            zday3week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 25))
                                        {
                                            zday4week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 26))
                                        {
                                            zday5week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date == new DateTime(fromDt.Year, fromDt.Month, 27))
                                        {
                                            zday6week4++;
                                        }
                                        else if (task.StartDate.Value.AddHours(userinfo.TimeZone).Date >= new DateTime(fromDt.Year, fromDt.Month, 28) && task.StartDate.Value.AddHours(userinfo.TimeZone).Date <= new DateTime(fromDt.Year, (fromDt.Month + 1), 1))
                                        {
                                            zday7week4++;
                                        }
                                        else
                                        {

                                        }
                                    }
                                }
                            }
                        }
                    }
                    var zTop1InsName = string.Empty;
                    var zTop2InsName = string.Empty;
                    var zTop3InsName = string.Empty;
                    var zTop4InsName = string.Empty;
                    var zTop5InsName = string.Empty;

                    var zTop1InsCount = "0";
                    var zTop2InsCount = "0";
                    var zTop3InsCount = "0";
                    var zTop4InsCount = "0";
                    var zTop5InsCount = "0";

                    Dictionary<string, int> counts = userList.GroupBy(x => x)
          .ToDictionary(g => g.Key,
                        g => g.Count());
                    var items = from pair in counts
                                orderby pair.Value descending
                                select pair;
                    int count = 0;
                    foreach (KeyValuePair<string, int> pair in items)
                    {

                        if (count < 5)
                        {

                            if (count == 0)
                            {
                                zTop1InsName = pair.Key.ToString();
                                zTop1InsCount = pair.Value.ToString();
                            }
                            else if (count == 1)
                            {
                                zTop2InsName = pair.Key.ToString();
                                zTop2InsCount = pair.Value.ToString();

                            }
                            else if (count == 2)
                            {
                                zTop3InsName = pair.Key.ToString();
                                zTop3InsCount = pair.Value.ToString();
                            }
                            else if (count == 3)
                            {
                                zTop4InsName = pair.Key.ToString();
                                zTop4InsCount = pair.Value.ToString();
                            }
                            else if (count == 4)
                            {
                                zTop5InsName = pair.Key.ToString();
                                zTop5InsCount = pair.Value.ToString();
                            }
                            count++;
                        }
                        else
                        {
                            break;
                        }

                    }

                    var instructor1FinishTimes = new List<int>();
                    var instructor2FinishTimes = new List<int>();
                    var instructor3FinishTimes = new List<int>();
                    var instructor4FinishTimes = new List<int>();
                    var instructor5FinishTimes = new List<int>();
                    var checklistItemsWeek1 = new List<UserTask>();
                    var checklistItemsWeek2 = new List<UserTask>();
                    var checklistItemsWeek3 = new List<UserTask>();
                    var checklistItemsWeek4 = new List<UserTask>();

                    var firstDayOfMonth = new DateTime(fromDt.Year, fromDt.Month, 1);
                    var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                    var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                    var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                    foreach (var tsk in compTasks)
                    {
                        if (tsk.TemplateCheckListId > 0)
                        {
                            if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= firstDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < tenthDayOfMonth.Date)
                            {
                                checklistItemsWeek1.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= tenthDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < fifteenDayOfMonth.Date)
                            {
                                checklistItemsWeek2.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= fifteenDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < twentyDayOfMonth.Date)
                            {
                                checklistItemsWeek3.Add(tsk);
                            }
                            else if (tsk.ActualEndDate != null && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date >= twentyDayOfMonth.Date && tsk.ActualEndDate.Value.AddHours(userinfo.TimeZone).Date < lastDayOfMonth.Date.AddHours(23))
                            {
                                checklistItemsWeek4.Add(tsk);
                            }
                        }
                        if (tsk.ACustomerUName.ToLower() == zTop1InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor1FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor1FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                        }
                        else if (tsk.ACustomerUName.ToLower() == zTop2InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor2FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor2FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            } 
                        }
                        else if (tsk.ACustomerUName.ToLower() == zTop3InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor3FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor3FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            } 
 
                        }
                        else if (tsk.ACustomerUName.ToLower() == zTop4InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor4FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor4FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }  
                        }
                        else if (tsk.ACustomerUName.ToLower() == zTop5InsName.ToLower())
                        {
                            if (tsk.ActualOnRouteDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualOnRouteDate.Value);
                                instructor5FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }
                            else if (tsk.ActualStartDate != null)
                            {
                                TimeSpan span = tsk.ActualEndDate.Value.Subtract(tsk.ActualStartDate.Value);
                                instructor5FinishTimes.Add(Convert.ToInt32(span.TotalMinutes));
                            }   
                        }
                    }
                    var compChecklistTasks = compTasks.Where(i => i.TemplateCheckListId > 0).ToList();

                    Dictionary<string, int> compcounts = compChecklistTasks.GroupBy(x => x.TemplateCheckListId.ToString())
                    .ToDictionary(g => g.Key,
                    g => g.Count());
                    var compitems = from pair in compcounts
                                    orderby pair.Value descending
                                    select pair;

                    int compcount = 0;
                    var tempId1 = 0;
                    var tempId2 = 0;
                    var tempId3 = 0;
                    var tempId4 = 0;
                    var tempId5 = 0;

                    var zSkillSet1Name = string.Empty;
                    var zSkillSet2Name = string.Empty;
                    var zSkillSet3Name = string.Empty;
                    var zSkillSet4Name = string.Empty;
                    var zSkillSet5Name = string.Empty;

                    foreach (KeyValuePair<string, int> pair in compitems)
                    {

                        if (compcount < 5)
                        {

                            if (compcount == 0)
                            {
                                //SkillSet1Name = pair.Key.ToString();
                                tempId1 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet1Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 1)
                            {
                                tempId2 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet2Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 2)
                            {
                                tempId3 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet3Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 3)
                            {
                                tempId4 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet4Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            else if (compcount == 4)
                            {
                                tempId5 = Convert.ToInt32(pair.Key.ToString());
                                zSkillSet5Name = CommonUtility.getTemplateChecklistNameById(Convert.ToInt32(pair.Key.ToString()));
                            }
                            compcount++;
                        }
                        else
                        {
                            break;
                        }

                    }
                    var zSkillSet1Week1Count = 0;
                    var zSkillSet2Week1Count = 0;
                    var zSkillSet3Week1Count = 0;
                    var zSkillSet4Week1Count = 0;
                    var zSkillSet5Week1Count = 0;

                    foreach (var item in checklistItemsWeek1)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            zSkillSet1Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            zSkillSet2Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            zSkillSet3Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            zSkillSet4Week1Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            zSkillSet5Week1Count++;
                        }
                    }

                    var zSkillSet1Week2Count = 0;
                    var zSkillSet2Week2Count = 0;
                    var zSkillSet3Week2Count = 0;
                    var zSkillSet4Week2Count = 0;
                    var zSkillSet5Week2Count = 0;

                    foreach (var item in checklistItemsWeek2)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            zSkillSet1Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            zSkillSet2Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            zSkillSet3Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            zSkillSet4Week2Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            zSkillSet5Week2Count++;
                        }
                    }

                    var zSkillSet1Week3Count = 0;
                    var zSkillSet2Week3Count = 0;
                    var zSkillSet3Week3Count = 0;
                    var zSkillSet4Week3Count = 0;
                    var zSkillSet5Week3Count = 0;

                    foreach (var item in checklistItemsWeek3)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            zSkillSet1Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            zSkillSet2Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            zSkillSet3Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            zSkillSet4Week3Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            zSkillSet5Week3Count++;
                        }
                    }

                    var zSkillSet1Week4Count = 0;
                    var zSkillSet2Week4Count = 0;
                    var zSkillSet3Week4Count = 0;
                    var zSkillSet4Week4Count = 0;
                    var zSkillSet5Week4Count = 0;

                    foreach (var item in checklistItemsWeek4)
                    {
                        if (item.TemplateCheckListId == tempId1)
                        {
                            zSkillSet1Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId2)
                        {
                            zSkillSet2Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId3)
                        {
                            zSkillSet3Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId4)
                        {
                            zSkillSet4Week4Count++;
                        }
                        else if (item.TemplateCheckListId == tempId5)
                        {
                            zSkillSet5Week4Count++;
                        }
                    }

                    var ztaskUser1Avg = 0;
                    var ztaskUser2Avg = 0;
                    var ztaskUser3Avg = 0;
                    var ztaskUser4Avg = 0;
                    var ztaskUser5Avg = 0;

                    var sum = Sum2(instructor1FinishTimes);

                    if (instructor1FinishTimes.Count > 0)
                    {
                        ztaskUser1Avg = (int)Math.Round((decimal)sum / instructor1FinishTimes.Count);
                    }

                    sum = Sum2(instructor2FinishTimes);
                    if (instructor2FinishTimes.Count > 0)
                    {
                        ztaskUser2Avg = (int)Math.Round((decimal)sum / instructor2FinishTimes.Count);
                    }
                    sum = Sum2(instructor3FinishTimes);
                    if (instructor3FinishTimes.Count > 0)
                    {
                        ztaskUser3Avg = (int)Math.Round((decimal)sum / instructor3FinishTimes.Count);
                    }
                    sum = Sum2(instructor4FinishTimes);
                    if (instructor4FinishTimes.Count > 0)
                    {
                        ztaskUser4Avg = (int)Math.Round((decimal)sum / instructor4FinishTimes.Count);
                    }
                    sum = Sum2(instructor5FinishTimes);
                    if (instructor5FinishTimes.Count > 0)
                    {
                        ztaskUser5Avg = (int)Math.Round((decimal)sum / instructor5FinishTimes.Count);
                    }
                    var ztaskUser1 = zTop1InsName;
                    var ztaskUser2 = zTop2InsName;
                    var ztaskUser3 = zTop3InsName;
                    var ztaskUser4 = zTop4InsName;
                    var ztaskUser5 = zTop5InsName;

                    var zAvgTotal = (ztaskUser1Avg + ztaskUser2Avg + ztaskUser3Avg + ztaskUser4Avg + ztaskUser5Avg).ToString();

                    total = pendingCount + inprogressCount + completedCount + acceptedCount;
                    var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                    var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                    var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                    var acceptedPercentage = (int)Math.Round((float)acceptedCount / (float)total * (float)100);
                    var pendingPercent = string.Empty;
                    var inprogressPercent = string.Empty;
                    var completedPercent = string.Empty;
                    var acceptedPercent = string.Empty;

                    if (pendingCount > 0)
                        pendingPercent = pendingPercentage.ToString();
                    else
                        pendingPercent = "0";

                    if (inprogressCount > 0)
                        inprogressPercent = inprogressPercentage.ToString();
                    else
                        inprogressPercent = "0";

                    if (completedCount > 0)
                        completedPercent = completedPercentage.ToString();
                    else
                        completedPercent = "0";

                    if (acceptedCount > 0)
                        acceptedPercent = acceptedPercentage.ToString();
                    else
                        acceptedPercent = "0";
                     
                    var zActualDemoCount = total.ToString();

                    var top1Percent = (int)Math.Round((float)Convert.ToInt32(zTop1InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);
                    var top2Percent = (int)Math.Round((float)Convert.ToInt32(zTop2InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);
                    var top3Percent = (int)Math.Round((float)Convert.ToInt32(zTop3InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);
                    var top4Percent = (int)Math.Round((float)Convert.ToInt32(zTop4InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);
                    var top5Percent = (int)Math.Round((float)Convert.ToInt32(zTop5InsCount) / (float)Convert.ToInt32(zActualDemoCount) * (float)100);

                    if (top1Percent < 0)
                    {
                        top1Percent = 0;
                    }
                    if (top2Percent < 0)
                    {
                        top2Percent = 0;
                    }
                    if (top3Percent < 0)
                    {
                        top3Percent = 0;
                    }
                    if (top4Percent < 0)
                    {
                        top4Percent = 0;
                    }
                    if (top5Percent < 0)
                    {
                        top5Percent = 0;
                    }

                    var zTop1Ins = top1Percent.ToString() + "%";
                    var zTop2Ins = top2Percent.ToString() + "%";
                    var zTop3Ins = top3Percent.ToString() + "%";
                    var zTop4Ins = top4Percent.ToString() + "%";
                    var zTop5Ins = top5Percent.ToString() + "%";

                    listy.Add(acceptedCount.ToString());
                    listy.Add(acceptedPercent.ToString());
                    listy.Add(completedCount.ToString());
                    listy.Add(completedPercent.ToString());
                    listy.Add(inprogressCount.ToString());
                    listy.Add(inprogressPercent.ToString());
                    listy.Add(pendingCount.ToString());
                    listy.Add(pendingPercent.ToString());
                    listy.Add(total.ToString());

                    listy.Add(zday1week1.ToString());
                    listy.Add(zday1week2.ToString());
                    listy.Add(zday1week3.ToString());
                    listy.Add(zday1week4.ToString());

                    listy.Add(zday2week1.ToString());
                    listy.Add(zday2week2.ToString());
                    listy.Add(zday2week3.ToString());
                    listy.Add(zday2week4.ToString());

                    listy.Add(zday3week1.ToString());
                    listy.Add(zday3week2.ToString());
                    listy.Add(zday3week3.ToString());
                    listy.Add(zday3week4.ToString());

                    listy.Add(zday4week1.ToString());
                    listy.Add(zday4week2.ToString());
                    listy.Add(zday4week3.ToString());
                    listy.Add(zday4week4.ToString());

                    listy.Add(zday5week1.ToString());
                    listy.Add(zday5week2.ToString());
                    listy.Add(zday5week3.ToString());
                    listy.Add(zday5week4.ToString());

                    listy.Add(zday6week1.ToString());
                    listy.Add(zday6week2.ToString());
                    listy.Add(zday6week3.ToString());
                    listy.Add(zday6week4.ToString());

                    listy.Add(zday7week1.ToString());
                    listy.Add(zday7week2.ToString());
                    listy.Add(zday7week3.ToString());
                    listy.Add(zday7week4.ToString());

                    listy.Add(firstDayOfMonth.ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(1).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(2).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(3).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(4).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(5).ToString("yyyy-MM-dd"));
                    listy.Add(firstDayOfMonth.AddDays(6).ToString("yyyy-MM-dd"));

                    var zweek1total = (zday1week1 + zday2week1 + zday3week1 + zday4week1 + zday5week1 + zday6week1 + zday7week1).ToString();
                    var zweek2total = (zday1week2 + zday2week2 + zday3week2 + zday4week2 + zday5week2 + zday6week2 + zday7week2).ToString();
                    var zweek3total = (zday1week3 + zday2week3 + zday3week3 + zday4week3 + zday5week3 + zday6week3 + zday7week3).ToString();
                    var zweek4total = (zday1week4 + zday2week4 + zday3week4 + zday4week4 + zday5week4 + zday6week4 + zday7week4).ToString();
                    listy.Add("WEEK 1 TOTAL " + zweek1total);
                    listy.Add("WEEK 2 TOTAL " + zweek2total);
                    listy.Add("WEEK 3 TOTAL " + zweek3total);
                    listy.Add("WEEK 4 TOTAL " + zweek4total);

                    listy.Add(ztaskUser1);
                    listy.Add(ztaskUser2);
                    listy.Add(ztaskUser3);
                    listy.Add(ztaskUser4);
                    listy.Add(ztaskUser5);

                    listy.Add(ztaskUser1Avg.ToString());
                    listy.Add(ztaskUser2Avg.ToString());
                    listy.Add(ztaskUser3Avg.ToString());
                    listy.Add(ztaskUser4Avg.ToString());
                    listy.Add(ztaskUser5Avg.ToString());

                    listy.Add(zSkillSet1Name);
                    listy.Add(zSkillSet2Name);
                    listy.Add(zSkillSet3Name);
                    listy.Add(zSkillSet4Name);
                    listy.Add(zSkillSet5Name);

                    listy.Add(zSkillSet1Week1Count.ToString());
                    listy.Add(zSkillSet1Week2Count.ToString());
                    listy.Add(zSkillSet1Week3Count.ToString());
                    listy.Add(zSkillSet1Week4Count.ToString());
                     
                    listy.Add(zSkillSet2Week1Count.ToString());
                    listy.Add(zSkillSet2Week2Count.ToString());
                    listy.Add(zSkillSet2Week3Count.ToString());
                    listy.Add(zSkillSet2Week4Count.ToString());

                    listy.Add(zSkillSet3Week1Count.ToString());
                    listy.Add(zSkillSet3Week2Count.ToString());
                    listy.Add(zSkillSet3Week3Count.ToString());
                    listy.Add(zSkillSet3Week4Count.ToString());

                    listy.Add(zSkillSet4Week1Count.ToString());
                    listy.Add(zSkillSet4Week2Count.ToString());
                    listy.Add(zSkillSet4Week3Count.ToString());
                    listy.Add(zSkillSet4Week4Count.ToString());

                    listy.Add(zSkillSet5Week1Count.ToString());
                    listy.Add(zSkillSet5Week2Count.ToString());
                    listy.Add(zSkillSet5Week3Count.ToString());
                    listy.Add(zSkillSet5Week4Count.ToString());

                    listy.Add(zTop1Ins);
                    listy.Add(zTop2Ins);
                    listy.Add(zTop3Ins);
                    listy.Add(zTop4Ins);
                    listy.Add(zTop5Ins);

                    listy.Add(zTop1InsName);
                    listy.Add(zTop2InsName);
                    listy.Add(zTop3InsName);
                    listy.Add(zTop4InsName);
                    listy.Add(zTop5InsName);

                    listy.Add(zTop1InsCount);
                    listy.Add(zTop2InsCount);
                    listy.Add(zTop3InsCount);
                    listy.Add(zTop4InsCount);
                    listy.Add(zTop5InsCount);

                    listy.Add("Average Total :" + zAvgTotal);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getEventStatusTotal", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add("ERROR");
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowDataIncident(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customEventData = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);

                if (customEventData != null)
                {


                    if (customEventData.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        var mobEv = MobileHotEvent.GetMobileHotEventByGuid(customEventData.Identifier, dbConnection);
                        if (string.IsNullOrEmpty(mobEv.EventTypeName))
                            customEventData.Name = "None";
                        else
                            customEventData.Name = mobEv.EventTypeName;

                        customEventData.Description = mobEv.Comments;
                    }
                    else if (customEventData.EventType == CustomEvent.EventTypes.Request)
                    {
                        var reqEv = HotEvent.GetHotEventById(customEventData.Identifier, dbConnection);
                        var splitName = reqEv.Name.Split('^');
                        if (splitName.Length > 1)
                        {
                            customEventData.Description = "Request " + splitName[1];
                            customEventData.Name = splitName[1];
                        }
                        if (reqEv != null)
                        {
                            if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                customEventData.UserName = CommonUtility.getPCName(reqEv);
                        }
                    }
                    else if (customEventData.EventType == CustomEvent.EventTypes.HotEvent)
                    {
                        var reqEv = HotEvent.GetHotEventById(customEventData.Identifier, dbConnection);
                        if (reqEv != null)
                            customEventData.UserName = CommonUtility.getPCName(reqEv);
                    }
                    if (!string.IsNullOrEmpty(customEventData.UserName))
                    {
                        var getUserInfo = Users.GetUserByName(customEventData.UserName, dbConnection);
                        if (getUserInfo != null)
                        {
                            customEventData.EmailAddress = getUserInfo.Email;
                            customEventData.PhoneNumber = getUserInfo.Telephone;
                        }
                    }


                    listy.Add(customEventData.CustomerUName);
                    listy.Add(customEventData.RecevieTime.ToString());
                    listy.Add(customEventData.Event);
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        var dir = Accounts.GetDirectorByManagerName(userinfo.Username, dbConnection);
                        if (dir != null)
                        {
                            if (dir.Username == customEventData.HandledBy && dir.Username != customEventData.UserName)
                            {
                                if (customEventData.EventType == CustomEvent.EventTypes.Incident)
                                    listy.Add(customEventData.StatusName + "-Resolve");
                                else
                                {
                                    listy.Add(customEventData.StatusName + "-MyIncident");
                                }

                            }
                            else if (dir.Username == customEventData.UserName)
                            {
                                if (userinfo.Username == customEventData.HandledBy)
                                {
                                    listy.Add(customEventData.StatusName);
                                }
                                else
                                    listy.Add(customEventData.StatusName + "-MyIncident");
                            }
                            else
                                listy.Add(customEventData.StatusName);
                        }
                        else
                            listy.Add(customEventData.StatusName);
                    }
                    else
                        listy.Add(customEventData.StatusName);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customEventData.Latitude.ToString(), customEventData.Longtitude.ToString(), getClientLic);
                    if (!string.IsNullOrEmpty(geolocation))
                        listy.Add(geolocation);
                    else
                    {
                        var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, customEventData.EventId);
                        if (geofence.Count > 0)
                        {
                            geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        }
                        listy.Add(geolocation);
                    }
                    if (!string.IsNullOrEmpty(customEventData.ReceivedBy))
                    {
                        listy.Add(customEventData.ReceivedBy);
                    }
                    else
                    {
                        listy.Add(customEventData.UserName);
                    }
                    listy.Add(customEventData.PhoneNumber);
                    listy.Add(customEventData.EmailAddress);
                    listy.Add(customEventData.Description);
                    if (!string.IsNullOrEmpty(customEventData.Instructions))
                    {
                        var splitIns = customEventData.Instructions.Split('|');
                        if (splitIns.Length > 0)
                            customEventData.Instructions = splitIns[0];
                    }
                    listy.Add(customEventData.Instructions);
                    listy.Add(customEventData.Name+"-"+customEventData.EventId);
                    listy.Add("circle-point " + CommonUtility.getImgIncidentPriority(customEventData.Event));
                    listy.Add(customEventData.Longtitude.ToString());
                    listy.Add(customEventData.Latitude.ToString());
                    var gettasks = UserTask.GetAllTaskByIncidentId(id, dbConnection);
                    var foundtask = false;
                    if (gettasks.Count > 0)
                    {
                        foreach (var task in gettasks)
                        {
                            if (task.Status != (int)TaskStatus.Cancelled)
                            {
                                foundtask = true;
                                break;
                            }
                        }
                    }
                    if (foundtask)
                        listy.Add("TASK");

                    var foundEsca = false;
                    var evsHistory = EventHistory.GetEventHistoryByEventId(id, dbConnection);
                    if (evsHistory.Count > 0)
                    {
                        if (customEventData.StatusName=="Escalate")//customEventData.Escalate == (int)CustomEvent.EscalateTypes.Escalate)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                        else if (customEventData.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                        else if (customEventData.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                        {
                            var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject);
                            if (isEsca != null)
                            {
                                foreach (var esca in isEsca)
                                {
                                    listy.Add(esca.Remarks);
                                    foundEsca = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (!foundEsca)
                        listy.Add("N/A");

                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        if (customEventData.StatusName=="Escalate")//customEventData.Escalate == (int)CustomEvent.EscalateTypes.Escalate)
                        {
                            listy.Add("none");
                        }
                        else
                            listy.Add("block");
                    }
                    else
                    {
                        listy.Add("block");
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("TaskDash", "getTableRowDataIncident", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getEventHistoryDataIncident(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allEventHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                foreach (var rem in allEventHistory)
                {
                    var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                    if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                        if (mobEv != null)
                        {
                            if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                gethotevent.Name = "None";
                            else
                                gethotevent.Name = mobEv.EventTypeName;
                        }
                    }
                    else if (gethotevent.EventType == CustomEvent.EventTypes.Request)
                    {
                        var reqEv = HotEvent.GetHotEventById(gethotevent.Identifier, dbConnection);
                        var splitName = reqEv.Name.Split('^');
                        if (splitName.Length > 1)
                        {
                            gethotevent.Name = splitName[1];
                        }
                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerUName;
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "engaged ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";

                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);


                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getEventHistoryData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }


        [WebMethod]
        public static string getAttachmentDataIconsIncident(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection); 
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);
                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;

                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {

                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            if (i == 4)
                            {
                                var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' />";
                                listy += retstring;
                                i++;
                                break;
                            }
                            else
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {

                                    var imgstring = requiredString;
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' />";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {
                                        imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + 0 + ")' data-target='#video-" + i + "-tab'/>";
                            listy += retstring;
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring = requiredString;
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                if (i == 4)
                                {
                                    var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#attachments-tab' />";
                                    listy += retstring;
                                    i++;
                                    break;
                                }
                                else
                                {
                                    //int index = item.AttachmentPath.IndexOf("HotEvents");
                                    var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                    //requiredString = requiredString.Replace("\\", "/");

                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                    {
                                        var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='hideIncidentplay();play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                    {
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                    else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                    {

                                    }
                                    else
                                    {
                                        imgstring = requiredString;
                                        var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                        listy += retstring;
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img src='" + imgstring + "' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getAttachmentDataIcons", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getAttachmentDataTabIncident(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#location-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Incident Location</p></div></div></div>";
 
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);

                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {
                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");

                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                               // iTab++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {
                                var imgstring = String.Format(requiredString);

                                var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;
                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                  //  iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {
                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                    //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + 0 + ")' data-target='#video-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Playback</p></div></div></div>";
                            listy += retstring;
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" +  requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                   // iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstring = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border onclick='hideIncidentplay()' clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    var iTab = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab' onclick='audioincidentplay(&apos;" + requiredString + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                  //  iTab++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {
                                    imgstring = String.Format(requiredString);

                                    var attachmentName = CommonUtility.getAttachmentDisplayName(requiredString, i);

                                    var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideIncidentplay()' ><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstringExtra;
                                }
                                else
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                }
                                i++;
                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideIncidentplay()' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    listy += retstring;
                                    iTab++;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Default", "getAttachmentDataTab", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentDataIncident(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var cusEv = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                if (cusEv.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusEv.Identifier, dbConnection);
                    var attachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (attachments.Count > 0)
                    {
                        foreach (var item in attachments)
                        {

                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                var retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='" + requiredString + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                            {
                                //var retstring = "<audio id='Video" + i + "' width='100%' height='378px' controls ><source src='" + requiredString + "' type='audio/mpeg' /></audio>";
                                //listy.Add(retstring);
                                //i++;
                            }
                            else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {

                                var imgstring = requiredString;
                                var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }

                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='" + requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    //var retstring = "<audio id='Video" + i + "' width='100%' height='378px'  controls ><source type='audio/mpeg' src='" + requiredString + "' /></audio>";
                                    //listy.Add(retstring);
                                    //i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring = requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
                else if (cusEv.EventType == CustomEvent.EventTypes.HotEvent)
                {
                    var hotEv = HotEvent.ServerGetHotEventById(cusEv.Identifier, dbConnection);
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (hotEv.isActive)
                    {

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hotEv.playbackURL))
                        {
                            var retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='" + hotEv.playbackURL + "' /></video>";
                            listy.Add(retstring);
                            i++;
                        }
                    }
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='" + requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
                                    //var retstring = "<audio id='Video" + i + "' width='100%' height='378px' controls ><source type='audio/mpeg' src='" + requiredString + "' /></audio>";
                                    //listy.Add(retstring);
                                    //i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring = requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                    var i = 1;
                    if (evattachments.Count > 0)
                    {
                        foreach (var item in evattachments)
                        {
                            var imgstring = string.Empty;
                            if (!string.IsNullOrEmpty(item.AttachmentPath))
                            {
                                //int index = item.AttachmentPath.IndexOf("HotEvents");
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                                //requiredString = requiredString.Replace("\\", "/");

                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                    var retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='" +requiredString + "' /></video>";
                                    listy.Add(retstring);
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".MP3")
                                {
 
                                }
                                else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    imgstring = requiredString;
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }

                            }
                            else
                            {
                                if (item.Attachment != null)
                                {
                                    var base64 = Convert.ToBase64String(item.Attachment);
                                    imgstring = String.Format("data:image/png;base64,{0}", base64);
                                    var retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                    listy.Add(retstring);
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        



        [WebMethod]
        public static string getIncidentLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                json += "[";
                if (id > 0)
                {
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            var item = CustomEvent.GetCustomEventById(id, dbConnection);
                            if (item.EventType == CustomEvent.EventTypes.Incident)
                            {
                                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                                if (geofence.Count > 0)
                                {
                                    foreach (var geo in geofence)
                                    {
                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                    }
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                                }
                            }
                                else if (item.EventType == CustomEvent.EventTypes.DriverOffence)
                            {
                                var item2 = DriverOffence.GetDriverOffenceById(item.Identifier, dbConnection);

                                json += "{ \"Username\" : \"Ticket\",\"Id\" : \"" + item2.Identifier.ToString() + "\",\"Long\" : \"" + item2.Longitude.ToString() + "\",\"Lat\" : \"" + item2.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            } 
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("TaskDash", "getIncidentLocationData", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string uploadMobileAttachment(int id, string imgpath, string uname)
        {
            var retid = false;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (!string.IsNullOrEmpty(imgpath))
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var newimgPath = imgpath.Replace('|', '\\');
                    var bmap = new System.Drawing.Bitmap(newimgPath);
                    var newmobattach = new MobileHotEventAttachment();
                    newmobattach.Attachment = CommonUtility.ImageToByte2(bmap);
                    newmobattach.AttachmentPath = string.Empty;//savestring;
                    newmobattach.CreatedBy = userinfo.Username;
                    newmobattach.CreatedDate = CommonUtility.getDTNow();
                    newmobattach.EventId = id;
                    newmobattach.UpdatedBy = userinfo.Username;
                    newmobattach.UpdatedDate = CommonUtility.getDTNow();
                    newmobattach.IsPortal = true;
                    newmobattach.SiteId = userinfo.SiteId;
                    newmobattach.CustomerId = userinfo.CustomerInfoId;
                    retid = MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newmobattach, dbConnection);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("TaskDash", "uploadMobileAttachment", ex, dbConnection, userinfo.SiteId);
                }
            }
            return retid.ToString();
        }

        [WebMethod]
        public static string CreatePDFIncident(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var cusevent = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (cusevent != null)
                            return GeneratePDFIncident(cusevent, userinfo);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks", "CreatePDFIncident", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }
        [WebMethod]
        public static string CreateExcelTask(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var taskevent = UserTask.GetTaskById(id, dbConnection);
                        if (taskevent != null)
                            return GenerateExcelTask(taskevent, userinfo);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks", "CreateExcelTask", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }
        private static string GenerateExcelTask(UserTask task, Users userinfo)
        {
            string fileName = string.Empty;
            try
            {




                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

                var webUtility = new WebServiceUtility();

                var response = webUtility.MakeGetWebServiceRequest(mimssettings.UploadServiceUrl,
                    "GeneratePDF?id=" + task.Id + "&username=" + userinfo.Username + "&key=" + CommonUtility.googlekeyapi);

                if (!string.IsNullOrEmpty(response))
                {

                    response = webUtility.GetDeserializedJsonObject<string>(response);
                    var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + response + ".xlsx";
                    return vpath;
                }
                else
                {
                    return "Failed to generate report! Problem with service.";
                }

            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Task", "GenerateExcelTask", exp, dbConnection, userinfo.SiteId);
                return "Failed to generate report!";
            }
        }
        private static string GeneratePDFIncident(CustomEvent cusevent, Users userinfo)
        {
            string fileName = string.Empty;
            try
            {
                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                var path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };
                doc.Open();
                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                Font rweb = new Font(z_cn, 11, Font.NORMAL, arrowred);

                Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                Font eleventimesI = new Font(f_cn, 11, Font.ITALIC, Color.BLACK);

                Font mbtimes = new Font(h_cn, 18, Font.BOLD, arrowred);
                Font cbtimes = new Font(h_cn, 13, Font.BOLD, arrowred);

                float[] columnWidthsH = new float[] { 25, 60, 15 };

                var mainheadertable = new PdfPTable(3);
                mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainheadertable.WidthPercentage = 100;
                mainheadertable.SetWidths(columnWidthsH);
                var newTRIlist = new List<TimelineReportItems>();

                var headerMain = new PdfPCell();
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    headerMain.AddElement(new Phrase("G4S INCIDENT REPORT", mbtimes));
                }
                else
                {
                    headerMain.AddElement(new Phrase("MIMS INCIDENT REPORT", mbtimes));
                }
                headerMain.PaddingBottom = 10;
                headerMain.HorizontalAlignment = 1;
                headerMain.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell1 = new PdfPCell();
                pdfCell1.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell2 = new PdfPCell();
                pdfCell2.Border = Rectangle.NO_BORDER;
                mainheadertable.AddCell(pdfCell1);
                mainheadertable.AddCell(headerMain);
                mainheadertable.AddCell(pdfCell2);

                var taskdataTable = new PdfPTable(2);
                var taskdataTableA = new PdfPTable(2);
                var taskdataTableB = new PdfPTable(2);
                taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTable.WidthPercentage = 100;

                taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                taskdataTableA.WidthPercentage = 100;
                taskdataTableB.WidthPercentage = 100;

                float[] columnWidths2575 = new float[] { 25, 75 };
                float[] columnWidths3070 = new float[] { 30, 70 };
                float[] columnWidths3565 = new float[] { 35, 65 };
                float[] columnWidths4060 = new float[] { 40, 60 };
                taskdataTableA.SetWidths(columnWidths4060);

                taskdataTableB.SetWidths(columnWidths4060);

                //SIDE A
                var header1 = new PdfPCell();
                header1.AddElement(new Phrase("Incident Name:", beleventimes));
                header1.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1);

                var header1a = new PdfPCell();
                header1a.AddElement(new Phrase(cusevent.Name, eleventimes));
                header1a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1a);

                var header2 = new PdfPCell();
                header2.AddElement(new Phrase("Incident Type:", beleventimes));
                header2.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2);

                var header2a = new PdfPCell();
                header2a.AddElement(new Phrase(cusevent.Event, eleventimes));
                header2a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2a);

                var header3 = new PdfPCell();
                header3.AddElement(new Phrase("Created Date:", beleventimes));
                header3.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3);

                var header3a = new PdfPCell();
                header3a.AddElement(new Phrase(cusevent.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), eleventimes));
                header3a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3a);

                var header4 = new PdfPCell();
                header4.AddElement(new Phrase("Created by:", beleventimes));
                header4.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4);

                var header4a = new PdfPCell();
                header4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                header4a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4a);

                var header5 = new PdfPCell();
                header5.AddElement(new Phrase("Received by:", beleventimes));
                header5.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header5);

                if (string.IsNullOrEmpty(cusevent.ReceivedBy))
                {
                    cusevent.ReceivedBy = cusevent.CustomerFullName;
                }

                var header5a = new PdfPCell();
                header5a.AddElement(new Phrase(cusevent.ReceivedBy, eleventimes));
                header5a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header5a);

                if (!string.IsNullOrEmpty(cusevent.UserName))
                {
                    var getUserInfo = Users.GetUserByName(cusevent.UserName, dbConnection);
                    if (getUserInfo != null)
                    {
                        if (string.IsNullOrEmpty(cusevent.EmailAddress))
                            cusevent.EmailAddress = getUserInfo.Email;
                        if (string.IsNullOrEmpty(cusevent.PhoneNumber))
                            cusevent.PhoneNumber = getUserInfo.Telephone;
                    }
                }

                var header6 = new PdfPCell();
                header6.AddElement(new Phrase("Contact Number:", beleventimes));
                header6.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header6);

                var header6a = new PdfPCell();
                header6a.AddElement(new Phrase(cusevent.PhoneNumber, eleventimes));
                header6a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header6a);

                var header7 = new PdfPCell();
                header7.AddElement(new Phrase("Contact Email:", beleventimes));
                header7.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header7);

                var header7a = new PdfPCell();
                header7a.AddElement(new Phrase(cusevent.EmailAddress, eleventimes));
                header7a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header7a);

                var header8 = new PdfPCell();
                header8.AddElement(new Phrase("Description:", beleventimes));
                header8.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8);

                if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    cusevent.Description = cusevent.MobDescription;

                var header8a = new PdfPCell();
                header8a.AddElement(new Phrase(cusevent.Description, eleventimes));
                header8a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8a);

                var header9 = new PdfPCell();
                header9.AddElement(new Phrase("Instructions:", beleventimes));
                header9.Border = Rectangle.NO_BORDER;
                header9.PaddingBottom = 20;
                taskdataTableA.AddCell(header9);

                if (!string.IsNullOrEmpty(cusevent.Instructions))
                {
                    var splitIns = cusevent.Instructions.Split('|');
                    if (splitIns.Length > 0)
                        cusevent.Instructions = splitIns[0];
                }

                var header9a = new PdfPCell();
                header9a.AddElement(new Phrase(cusevent.Instructions, eleventimes));
                header9a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header9a);

                //SIDE B

                var headerB2 = new PdfPCell();
                headerB2.AddElement(new Phrase("Incident Status:", beleventimes));
                headerB2.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2);

                var headerB2a = new PdfPCell();
                headerB2a.AddElement(new Phrase(cusevent.StatusName, eleventimes));
                headerB2a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2a);

                var headerB1 = new PdfPCell();
                headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                headerB1.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB1);


                var siteinfo = Arrowlabs.Business.Layer.Site.GetSiteById(cusevent.SiteId, dbConnection);
                if (siteinfo != null)
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase(siteinfo.Name, eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }
                else
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase("N/A", eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }


                var headerB3 = new PdfPCell();
                headerB3.AddElement(new Phrase("Dispatched By:", beleventimes));
                headerB3.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB3);

                var headerB3a = new PdfPCell();
                headerB3a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                headerB3a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB3a);


                if (cusevent.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                {
                    var headerB4 = new PdfPCell();
                    headerB4.AddElement(new Phrase("Resolved By:", beleventimes));
                    headerB4.Border = Rectangle.NO_BORDER;
                    headerB4.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB4);

                    var headerB4a = new PdfPCell();
                    headerB4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                    headerB4a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB4a);

                    var headerB5 = new PdfPCell();
                    headerB5.AddElement(new Phrase("Resolved Notes:", beleventimes));
                    headerB5.Border = Rectangle.NO_BORDER;
                    headerB5.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB5);

                    var evsHistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                    var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve);
                    if (isEsca != null)
                    {
                        foreach (var esca in isEsca)
                        {
                            var headerB5a = new PdfPCell();
                            headerB5a.AddElement(new Phrase(esca.Remarks, eleventimes));
                            headerB5a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB5a);
                            break;
                        }
                    }
                    else
                    {
                        var headerB5a = new PdfPCell();
                        headerB5a.AddElement(new Phrase("N/A", eleventimes));
                        headerB5a.Border = Rectangle.NO_BORDER;
                        taskdataTableB.AddCell(headerB5a);
                    }
                }

                taskdataTable.AddCell(taskdataTableA);
                taskdataTable.AddCell(taskdataTableB);

                var maptextdataTable = new PdfPTable(1);
                maptextdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                maptextdataTable.WidthPercentage = 100;

                var geolocation = ReverseGeocode.RetrieveFormatedAddress(cusevent.Latitude.ToString(), cusevent.Longtitude.ToString(), getClientLic);
                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, cusevent.EventId);
                if (string.IsNullOrEmpty(geolocation))
                {
                    if (geofence.Count > 0)
                    {
                        geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        cusevent.Longtitude = geofence[0].Longitude.ToString();
                        cusevent.Latitude = geofence[0].Latitude.ToString();

                    }
                }

                if (string.IsNullOrEmpty(geolocation))
                    geolocation = "Not Specified";

                var mapheader = new PdfPCell();
                mapheader.AddElement(new Phrase("Incident Location:" + geolocation, eleventimes));
                mapheader.Border = Rectangle.NO_BORDER;
                maptextdataTable.AddCell(mapheader);

                var mapdataTable = new PdfPTable(2);
                mapdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                mapdataTable.WidthPercentage = 100;
                float[] columnWidths7030 = new float[] { 70, 30 };
                mapdataTable.SetWidths(columnWidths7030);
                //MAP

                //outer.AddCell(table);
                var latComp = string.Empty;
                var longComp = string.Empty;
                var latEng = string.Empty;
                var longEng = string.Empty;
                var arrived = string.Empty;
                var vargeopath = string.Empty; //&path=color:0xff0000ff|weight:5|25.09773,55.16316|25.0978512224338,55.1634863298386
                var tracebackpath = string.Empty; //&path=color:blue|weight:3|25.09773,55.16316|25.0978512224338,55.1634863298386


                var incidenteventhistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;

                foreach (var rem in incidenteventhistory)
                {
                    var timelineItem = new TimelineReportItems();

                    if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        cusevent.Name = cusevent.EventTypeName;
                    }
                    else if (cusevent.EventType == CustomEvent.EventTypes.Request)
                    {
                        var splitName = cusevent.RequestName.Split('^');
                        if (splitName.Length > 1)
                        {
                            cusevent.Name = splitName[1];
                        }
                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerFullName;
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "engaged ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";
                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    newTRIlist.Add(timelineItem);
                }


                if (geofence.Count > 0)
                {
                    vargeopath = "&path=color:0xff0000ff|weight:5|";
                    foreach (var geo in geofence)
                    {
                        vargeopath += geo.Latitude + "," + geo.Longitude + "|";
                    }
                    vargeopath = vargeopath.Substring(0, vargeopath.Length - 1);
                }
                var assigneeList = new List<int>();
                var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(cusevent.EventId, dbConnection);

                var colorCount = 0;
                var legendTraceback = new List<string>();
                foreach (var noti in notification)
                {
                    var tbColor = string.Empty;
                    if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                    {

                    }
                    else
                    {
                        tbColor = CommonUtility.getColorName(colorCount);
                        assigneeList.Add(noti.AssigneeId);
                        legendTraceback.Add(tbColor + " Path:  " + noti.ACustomerFullName + " route");
                        colorCount++;
                    }
                    var tracebackhistory = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);
                    var tbCount = 0;
                    var tbRemovedPointsCount = 0;

                    var tbContinueCountStart = 0;
                    if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                    {
                        tbRemovedPointsCount = tracebackhistory.Count - CommonUtility.tracebackreportmaxcount;
                        tbContinueCountStart = (CommonUtility.tracebackreportmaxcount / 2) + tbRemovedPointsCount;
                    }
                    if (tracebackhistory.Count > 0)
                    {
                        tracebackpath += "&path=color:blue|weight:5|";
                        tracebackpath += latEng + "," + longEng + "|";
                        foreach (var trace in tracebackhistory)
                        {
                            if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                            {
                                tbCount++;
                                if (tbCount < (CommonUtility.tracebackreportmaxcount / 2))
                                    tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                                else if (tbCount > tbContinueCountStart)
                                    tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                            }
                            else
                                tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                        }
                        tracebackpath += latComp + "," + longComp;
                    }
                }
                var maplat = cusevent.Latitude;
                var maplong = cusevent.Longtitude;
                var taskLocation = string.Empty;
                if (cusevent.Latitude != "0" && cusevent.Longtitude != "0")
                {
                    taskLocation = "&markers=color:red%7Clabel:T%7C" + cusevent.Latitude + "," + cusevent.Longtitude;
                }
                else
                {
                    maplat = latEng;
                    maplong = longEng;
                }
                //Map style roadMap
                String mapURL = "https://maps.googleapis.com/maps/api/staticmap?" +
"center=" + cusevent.Latitude + "," + cusevent.Longtitude + "&" +
"size=700x360" + tracebackpath + vargeopath + arrived + "&markers=color:green%7Clabel:C%7C" + latComp + "," + longComp + "&markers=color:yellow%7Clabel:E%7C" + latEng + "," + longEng + taskLocation + "&zoom=17" + "&maptype=png" + "&sensor=true";
                iTextSharp.text.Image LocationImage = iTextSharp.text.Image.GetInstance(mapURL);
                LocationImage.ScaleToFit(380, 320);
                LocationImage.Alignment = iTextSharp.text.Image.UNDERLYING;
                LocationImage.SetAbsolutePosition(0, 0);
                PdfPCell locationcell = new PdfPCell(LocationImage);
                locationcell.PaddingTop = 5;
                locationcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right 
                locationcell.Border = 0;
                mapdataTable.AddCell(locationcell);


                PdfPTable legendtable = new PdfPTable(1);
                legendtable.DefaultCell.Border = Rectangle.NO_BORDER;
                legendtable.HorizontalAlignment = 0;
                Font redfont = new Font();
                redfont.Size = 11;
                Font yellowfont = new Font();
                yellowfont.Size = 11;
                Font greenfont = new Font();
                greenfont.Size = 11;
                Font bluefont = new Font();
                bluefont.Size = 11;
                redfont.Color = Color.RED;
                legendtable.AddCell(new Phrase("Legend:", ueleventimes));
                legendtable.AddCell(new Phrase("I:  Incident Location", redfont));
                yellowfont.Color = new Color(204, 204, 0);
                legendtable.AddCell(new Phrase("E:  Engaged Location", yellowfont));
                bluefont.Color = iTextSharp.text.Color.BLUE;
                legendtable.AddCell(new Phrase("A:  Arrived Location", bluefont));
                greenfont.Color = new Color(0, 204, 0);
                legendtable.AddCell(new Phrase("C:  Completed Location", greenfont));
                var pathCell = new PdfPCell(new Phrase("Red Path:  Geofence", eleventimes));
                pathCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                legendtable.AddCell(pathCell);
                foreach (var legendtrace in legendTraceback)
                {
                    var traceCell = new PdfPCell(new Phrase(legendtrace, eleventimes));
                    traceCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    legendtable.AddCell(traceCell);
                }
                mapdataTable.AddCell(legendtable);

                iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                paragraphTable.SpacingBefore = 120f;
                Paragraph p = new Paragraph();
                p.IndentationLeft = 10;

                p.Add(mainheadertable);
                p.Add(taskdataTable);
                p.Add(maptextdataTable);
                p.Add(mapdataTable);

                PdfPTable table = new PdfPTable(1);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                //Map style roadMap

                doc.Add(paragraphTable);

                doc.Add(p);

                //Attachments

                var listycanvasnotes = new List<string>();

                var attachments = new List<MobileHotEventAttachment>();


                if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusevent.Identifier, dbConnection);
                    if (mobEv != null)
                        attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection));


                }

                attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusevent.EventId, dbConnection));

                var videoCount = 0;
                var imgCount = 0;
                var pdfCount = 0;

                PdfPTable taskimagetable = new PdfPTable(2);
                taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskimagetable.WidthPercentage = 100;

                var atheader1 = new PdfPCell();
                atheader1.AddElement(new Phrase("Attachments", cbtimes));
                atheader1.Border = Rectangle.NO_BORDER;
                atheader1.PaddingTop = 10;
                atheader1.PaddingBottom = 10;
                atheader1.Colspan = 2;
                taskimagetable.AddCell(atheader1);
                taskimagetable.KeepTogether = true;
                if (attachments.Count > 0)
                {
                    var attachcount = 0;
                    var canvasattachcount = 0;
                    var attachlist = new List<MobileHotEventAttachment>();
                    var videosList = new List<MobileHotEventAttachment>();

                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                            {
                                videoCount++;
                            }
                            else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".MP3")
                            {
                                videoCount++;
                            }
                            else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                            {
                                pdfCount++;
                            }
                            else
                            {
                                attachlist.Add(item);
                                attachcount++;
                                imgCount++;
                                listycanvasnotes.Add("Image " + attachcount);
                            }
                        }
                    }
                    taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                    var countz = 0;
                    foreach (var item in attachlist)
                    {
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                        {

                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".MP3")
                        {

                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                        {

                        }
                        else
                        {
                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.AttachmentPath);

                            if (image.Height > image.Width)
                            {
                                //Maximum height is 800 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Height;
                                image.ScalePercent(percentage * 100);
                            }
                            else
                            {
                                //Maximum width is 600 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Width;
                                image.ScalePercent(percentage * 100);
                            }

                            //image.ScaleAbsolute(120f, 155.25f);
                            image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                            if (!string.IsNullOrEmpty(listycanvasnotes[countz]))
                            {
                                Paragraph pcell = new Paragraph(listycanvasnotes[countz].ToUpper());
                                pcell.Alignment = Element.ALIGN_LEFT;
                                pcell.SpacingAfter = 10;
                                imgCell1.AddElement(pcell);
                            }
                            imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                            imgCell1.AddElement(new Chunk(image, 0, 0));
                            imgCell1.PaddingTop = 10;
                            taskimagetable.AddCell(imgCell1);
                            countz++;
                        }
                    }
                    taskimagetable.CompleteRow();
                }

                var extraattachment = new PdfPTable(3);
                extraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                extraattachment.WidthPercentage = 100;
                var phrase2 = new Phrase();
                phrase2.Add(
                    new Chunk("PDF Attached:  ", btimes)
                );
                phrase2.Add(new Chunk(pdfCount.ToString(), times));

                var zheaderA3a = new PdfPCell();
                zheaderA3a.AddElement(phrase2);
                zheaderA3a.Border = Rectangle.NO_BORDER;
                zheaderA3a.PaddingTop = 20;
                zheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(zheaderA3a);

                var phrase32 = new Phrase();
                phrase32.Add(
                    new Chunk("Media Attached:  ", btimes)
                );
                phrase32.Add(new Chunk(videoCount.ToString(), times));

                var aheaderA3a = new PdfPCell();
                aheaderA3a.AddElement(phrase32);
                aheaderA3a.Border = Rectangle.NO_BORDER;
                aheaderA3a.PaddingTop = 20;
                aheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aheaderA3a);

                var phrase323 = new Phrase();
                phrase323.Add(
                    new Chunk("Images Attached:  ", btimes)
                );
                phrase323.Add(new Chunk(imgCount.ToString(), times));

                var aaheaderA3a = new PdfPCell();
                aaheaderA3a.AddElement(phrase323);
                aaheaderA3a.Border = Rectangle.NO_BORDER;
                aaheaderA3a.PaddingTop = 20;
                aaheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aaheaderA3a);

                doc.NewPage();

                doc.Add(taskimagetable);

                doc.Add(extraattachment);


                PdfPTable timetable = new PdfPTable(2);
                timetable.WidthPercentage = 100;
                timetable.DefaultCell.Border = Rectangle.NO_BORDER;
                timetable.SetWidths(columnWidths2575);

                if (newTRIlist.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Incident Timeline", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    timetable.AddCell(headeventCell);

                    foreach (var lit in newTRIlist)
                    {
                        var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(eventCell);
                    }
                }
                doc.Add(timetable);

                //TASKS LOOP

                var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    int count = 1;

                    foreach (var task in utask)
                    {
                        var tskheadertable = new PdfPTable(2);
                        tskheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskheadertable.WidthPercentage = 100;
                        float[] columnWidths2080 = new float[] { 20, 80 };
                        tskheadertable.SetWidths(columnWidths2080);

                        var tskheader0 = new PdfPCell();
                        //tskheader0.AddElement(new Phrase("Task #:" + count, cbtimes));

                        tskheader0.AddElement(new Phrase("Attached Task", cbtimes));
                        tskheader0.Border = Rectangle.NO_BORDER;
                        tskheader0.Colspan = 2;
                        tskheadertable.AddCell(tskheader0);

                        var tskheader1 = new PdfPCell();
                        tskheader1.AddElement(new Phrase("Task Name:", beleventimes));
                        tskheader1.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1);

                        var tskheader1a = new PdfPCell();
                        tskheader1a.AddElement(new Phrase(task.Name, eleventimes));
                        tskheader1a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1a);

                        var header1z = new PdfPCell();
                        header1z.AddElement(new Phrase("Task Number:", beleventimes));
                        header1z.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1z);

                        var header1az = new PdfPCell();
                        header1az.AddElement(new Phrase(task.NewCustomerTaskId, eleventimes));
                        header1az.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1az);

                        var tskheader2 = new PdfPCell();
                        tskheader2.AddElement(new Phrase("Task Description:", beleventimes));
                        tskheader2.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2);

                        var tskheader2a = new PdfPCell();
                        tskheader2a.AddElement(new Phrase(task.Description, eleventimes));
                        tskheader2a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2a);

                        var chklistName = "None";
                        var newTCLlist = new List<TaskChecklistReportItems>();
                        var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(task.TemplateCheckListId.ToString(), dbConnection);
                        if (parentTasks != null)
                        {
                            chklistName = parentTasks.Name;

                            var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(task.Id, dbConnection);

                            foreach (var item in sessions)
                            {
                                var newTCL = new TaskChecklistReportItems();
                                newTCL.isParent = true;

                                if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, eleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                else
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, beleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                newTCL.Notes = item.TemplateCheckListItemNote;

                                var myattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(item.Id, dbConnection);
                                newTCL.Attachments = myattachments;


                                var stringCheck = string.Empty;
                                var itemNotes = string.Empty;

                                if (item.IsChecked)
                                {
                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("3", gweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;
                                }
                                else
                                {
                                    List myList = new ZapfDingbatsList(54);

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("6", rweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;

                                    //if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                    //    chkCounter++;
                                }

                                newTCLlist.Add(newTCL);

                                if (item.ChildCheckList != null)
                                {
                                    if (item.ChildCheckList.Count > 0)
                                    {
                                        foreach (var child in item.ChildCheckList)
                                        {

                                            var cnewTCL = new TaskChecklistReportItems();
                                            cnewTCL.isParent = false;

                                            var ncaheaderA = new PdfPCell();
                                            ncaheaderA.AddElement(new Phrase(child.Name, eleventimesI));
                                            ncaheaderA.Border = Rectangle.NO_BORDER;
                                            cnewTCL.itemName = ncaheaderA;

                                            cnewTCL.Notes = child.TemplateCheckListItemNote;

                                            var mycattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(child.Id, dbConnection);
                                            cnewTCL.Attachments = mycattachments;

                                            if (item.IsChecked)
                                            {
                                                //gweb rweb
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("3", gweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                            }
                                            else
                                            {
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("6", rweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                                //chkCounter++;
                                            }

                                            newTCLlist.Add(cnewTCL);
                                        }
                                    }
                                }
                            }
                        }
                        var tskheader3 = new PdfPCell();
                        tskheader3.AddElement(new Phrase("Checklist Name:", beleventimes));
                        tskheader3.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3);

                        var tskheader3a = new PdfPCell();
                        tskheader3a.AddElement(new Phrase(chklistName, eleventimes));
                        tskheader3a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3a);

                        doc.Add(tskheadertable);

                        var newchecklistTable = new PdfPTable(4);
                        newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        newchecklistTable.WidthPercentage = 100;
                        float[] columnWidthsCHK = new float[] { 30, 10, 30, 30 };
                        newchecklistTable.SetWidths(columnWidthsCHK);

                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("Checklist", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.PaddingTop = 10;
                        cheader1.PaddingBottom = 10;
                        cheader1.Colspan = 4;
                        newchecklistTable.AddCell(cheader1);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Item Name", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.PaddingLeft = 5;
                        ncaheader1.PaddingBottom = 5;
                        ncaheader1.BackgroundColor = Color.DARK_GRAY;

                        newchecklistTable.AddCell(ncaheader1);

                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Status", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Notes", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("Attachments", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader4);

                        foreach (var item in newTCLlist)
                        {
                            newchecklistTable.AddCell(item.itemName);

                            newchecklistTable.AddCell(item.Status);

                            var ncaheaderAB = new PdfPCell();
                            ncaheaderAB.AddElement(new Phrase(item.Notes, eleventimes));
                            ncaheaderAB.Border = Rectangle.NO_BORDER;
                            newchecklistTable.AddCell(ncaheaderAB);

                            if (item.Attachments.Count > 0)
                            {
                                PdfPTable imgTable = new PdfPTable(1);
                                imgTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                foreach (var attach in item.Attachments)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant()))
                                    {
                                        imgTable.AddCell(new Phrase("Video Attached", eleventimes));
                                    }
                                    else if (System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant() == ".MP3")
                                    {
                                        imgTable.AddCell(new Phrase("Audio Attached", eleventimes));
                                    }
                                    else if (System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        imgTable.AddCell(new Phrase("PDF Attached", eleventimes));
                                    }
                                    else
                                    {
                                        //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + task.Id + "/{0}", System.IO.Path.GetFileName(attach.DocumentPath));

                                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(attach.DocumentPath);

                                        if (image.Height > image.Width)
                                        {
                                            //Maximum height is 800 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Height;
                                            image.ScalePercent(percentage * 100);
                                        }
                                        else
                                        {
                                            //Maximum width is 600 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Width;
                                            image.ScalePercent(percentage * 100);
                                        }

                                        //image.ScaleAbsolute(120f, 155.25f);
                                        image.Border = Rectangle.NO_BORDER;
                                        iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1l.Border = Rectangle.NO_BORDER;
                                        imgCell1l.AddElement(new Chunk(image, 0, 0));
                                        imgTable.AddCell(imgCell1l);
                                        if (!string.IsNullOrEmpty(attach.ImageNote) && attach.ImageNote != "Task Attachment")
                                            imgTable.AddCell(new Phrase("Img Note:" + attach.ImageNote, eleventimes));
                                    }
                                }
                                newchecklistTable.AddCell(imgTable);
                            }
                            else
                            {
                                newchecklistTable.AddCell(new Phrase(" ", eleventimes));
                            }
                        }

                        doc.Add(newchecklistTable);

                        var tsklistycanvasnotes = new List<string>();
                        var tskattachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(task.Id, dbConnection);
                        var tskvideoCount = 0;
                        var tskimgCount = 0;
                        var tskpdfCount = 0;

                        PdfPTable tsktaskimagetable = new PdfPTable(2);
                        tsktaskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktaskimagetable.WidthPercentage = 100;

                        var tskatheader1 = new PdfPCell();
                        tskatheader1.AddElement(new Phrase("Task Attachments", cbtimes));
                        tskatheader1.Border = Rectangle.NO_BORDER;
                        tskatheader1.PaddingTop = 10;
                        tskatheader1.PaddingBottom = 10;
                        tskatheader1.Colspan = 2;
                        tsktaskimagetable.AddCell(tskatheader1);

                        if (tskattachments.Count > 0)
                        {
                            var attachcount = 0;
                            var canvasattachcount = 0;
                            var attachlist = new List<UserTaskAttachment>();

                            foreach (var item in tskattachments)
                            {
                                if (!string.IsNullOrEmpty(item.DocumentPath) && item.ChecklistId == 0)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        tskpdfCount++;
                                    }
                                    else
                                    {
                                        attachlist.Add(item);
                                        attachcount++;
                                        tskimgCount++;
                                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                                        {
                                            tsklistycanvasnotes.Add("Canvas Notes Image " + attachcount + ":" + item.ImageNote);
                                            canvasattachcount++;
                                        }
                                        else
                                        {
                                            tsklistycanvasnotes.Add("Image " + attachcount);
                                            canvasattachcount++;
                                        }
                                    }
                                }
                            }

                            var countz = 0;
                            foreach (var item in attachlist)
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {

                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                {

                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.DocumentPath);

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    if (!string.IsNullOrEmpty(tsklistycanvasnotes[countz]))
                                    {
                                        Paragraph pcell = new Paragraph(tsklistycanvasnotes[countz].ToUpper());
                                        pcell.Alignment = Element.ALIGN_LEFT;
                                        pcell.SpacingAfter = 10;
                                        imgCell1.AddElement(pcell);
                                    }
                                    imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                    imgCell1.AddElement(new Chunk(image, 0, 0));
                                    imgCell1.PaddingTop = 10;
                                    tsktaskimagetable.AddCell(imgCell1);
                                    countz++;
                                }
                            }
                            tsktaskimagetable.CompleteRow();
                        }

                        var tskextraattachment = new PdfPTable(3);
                        tskextraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskextraattachment.WidthPercentage = 100;
                        var tskphrase2 = new Phrase();
                        tskphrase2.Add(
                            new Chunk("PDF Attached:  ", btimes)
                        );
                        tskphrase2.Add(new Chunk(tskpdfCount.ToString(), times));

                        var tskzheaderA3a = new PdfPCell();
                        tskzheaderA3a.AddElement(tskphrase2);
                        tskzheaderA3a.Border = Rectangle.NO_BORDER;
                        tskzheaderA3a.PaddingTop = 20;
                        tskzheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskzheaderA3a);

                        var tskphrase32 = new Phrase();
                        tskphrase32.Add(
                            new Chunk("Media Attached:  ", btimes)
                        );
                        tskphrase32.Add(new Chunk(tskvideoCount.ToString(), times));

                        var tskaheaderA3a = new PdfPCell();
                        tskaheaderA3a.AddElement(tskphrase32);
                        tskaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaheaderA3a.PaddingTop = 20;
                        tskaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaheaderA3a);

                        var tskphrase323 = new Phrase();
                        tskphrase323.Add(
                            new Chunk("Images Attached:  ", btimes)
                        );
                        tskphrase323.Add(new Chunk(tskimgCount.ToString(), times));

                        var tskaaheaderA3a = new PdfPCell();
                        tskaaheaderA3a.AddElement(tskphrase323);
                        tskaaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaaheaderA3a.PaddingTop = 20;
                        tskaaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaaheaderA3a);

                        doc.Add(tsktaskimagetable);

                        doc.Add(tskextraattachment);

                        var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(task.Id, dbConnection);

                        taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();

                        var tsknewTRIlist = new List<TimelineReportItems>();

                        foreach (var taskEv in taskeventhistory)
                        {
                            var timelineItem = new TimelineReportItems();
                            var taskn = "Task";
                            if (taskEv.isSubTask)
                            {
                                taskn = "Sub-task";
                            }

                            if (taskEv.Action == (int)TaskAction.Complete)
                            {
                                completedBy = taskEv.CustomerFullName;
                                var compLocation = string.Empty;
                                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(task.EndLatitude.ToString(), task.EndLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc))
                                    compLocation = " at " + geoLoc;
                                var compInfo = "completed";
                                latComp = task.EndLatitude.ToString();
                                longComp = task.EndLongitude.ToString();
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                                var compstring = completedBy + " " + compInfo + " " + taskn + " " + compLocation;

                                timelineItem.TimelineActivity = compstring;
                                //listy.Add(compstring);
                            }
                            else if (taskEv.Action == (int)TaskAction.InProgress)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "started";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.OnRoute)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "onroute";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Pause)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "paused";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Resume)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "resumed";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Accepted)
                            {
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                acceptedData = taskEv.CustomerFullName + " accepted " + taskn;
                                //listy.Add(acceptedData);
                                timelineItem.TimelineActivity = acceptedData;
                            }
                            else if (taskEv.Action == (int)TaskAction.Rejected)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                rejectedData = taskEv.CustomerFullName + " rejected Task " + taskn;

                                timelineItem.TimelineActivity = rejectedData;
                                //listy.Add(rejectedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Assigned)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                assignedData = taskEv.CustomerFullName + " assigned " + taskn + " to " + taskEv.ACustomerFullName;

                                timelineItem.TimelineActivity = assignedData;

                                //listy.Add(assignedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Pending)
                            {
                                var incidentLocation = string.Empty;
                                var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(task.Latitude.ToString(), task.Longitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc3))
                                    incidentLocation = " at " + geoLoc3;
                                var actioninfo = "created";
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                actioninfo = task.CustomerFullName + " " + actioninfo + " " + taskn + " for " + taskEv.ACustomerFullName + " " + incidentLocation;
                                timelineItem.TimelineActivity = actioninfo;
                                //listy.Add(actioninfo);
                            }
                            tsknewTRIlist.Add(timelineItem);
                        }

                        PdfPTable tsktimetable = new PdfPTable(2);
                        tsktimetable.WidthPercentage = 100;
                        tsktimetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktimetable.SetWidths(columnWidths2575);

                        if (tsknewTRIlist.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Task Timeline", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            tsktimetable.AddCell(headeventCell);

                            foreach (var lit in tsknewTRIlist)
                            {
                                var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(eventCell);
                            }
                        }

                        doc.Add(tsktimetable);


                        PdfPTable notestable = new PdfPTable(2);
                        notestable.WidthPercentage = 100;
                        notestable.DefaultCell.Border = Rectangle.NO_BORDER;
                        notestable.SetWidths(columnWidths2575);

                        var tremarks = TaskRemarks.GetTaskRemarksByTaskId(task.Id, dbConnection);

                        if (tremarks.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Notes", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            notestable.AddCell(headeventCell);
                            foreach (var remark in tremarks)
                            {
                                var dateCell = new PdfPCell(new Phrase(remark.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString(), redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                notestable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(remark.FName + " " + remark.LName + ":" + remark.Remarks, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                notestable.AddCell(eventCell);
                            }
                        }

                        doc.Add(notestable);
                    }
                }
                //END OF TASKS



                Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                var endTable = new PdfPTable(1);
                endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                endTable.WidthPercentage = 100;

                var endheader = new PdfPCell();
                endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                endheader.Border = Rectangle.NO_BORDER;
                endTable.AddCell(endheader);
                doc.Add(endTable);

                writer.Flush();
                doc.Close();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                        return vpath;
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Incident", "GeneratePDFIncident", exp, dbConnection, userinfo.SiteId);
                return "Failed to generate report!";
            }
            return "Failed to generate report!";
        }
        [WebMethod]
        public static string CreatePDFTask(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var taskevent = UserTask.GetTaskById(id, dbConnection);
                        if (taskevent != null)
                            return GeneratePDFTask(taskevent, userinfo);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("TaskDash", "CreatePDFTask", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }
        private static string GeneratePDFTask(UserTask task, Users userinfo)
        {
            string fileName = string.Empty;
            try
            {
                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                var path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };
                doc.Open();
                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                //BaseFont a_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\arialuni.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                {
                    f_cn = BaseFont.CreateFont(fontpath + "arialuni.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                }


                BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                Font rweb = new Font(z_cn, 11, Font.NORMAL, arrowred);

                Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                Font eleventimesI = new Font(f_cn, 11, Font.ITALIC, Color.BLACK);

                Font mbtimes = new Font(h_cn, 18, Font.BOLD, arrowred);
                Font cbtimes = new Font(h_cn, 13, Font.BOLD, arrowred);


                var mainheadertable = new PdfPTable(3);
                mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainheadertable.WidthPercentage = 100;

                var newTCLlist = new List<TaskChecklistReportItems>();
                var newTRIlist = new List<TimelineReportItems>();

                var tasklinks = UserTask.GetAllTasksByTaskLinkId(task.Id, dbConnection);
                var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(task.TemplateCheckListId.ToString(), dbConnection);
                var chkStatus = "N/A";
                var chklistName = "None";
                var chkCounter = 0;
                if (parentTasks != null)
                {
                    chklistName = parentTasks.Name;

                    var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(task.Id, dbConnection);

                    foreach (var item in sessions)
                    {
                        var newTCL = new TaskChecklistReportItems();
                        newTCL.isParent = true;

                        if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                        {
                            var ncaheaderA = new PdfPCell();

                            if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                                ncaheaderA.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                            ncaheaderA.AddElement(new Phrase(item.Name, eleventimes));
                            ncaheaderA.Border = Rectangle.NO_BORDER;
                            newTCL.itemName = ncaheaderA;
                        }
                        else
                        {
                            var ncaheaderA = new PdfPCell();

                            if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                                ncaheaderA.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                            ncaheaderA.AddElement(new Phrase(item.Name, beleventimes));
                            ncaheaderA.Border = Rectangle.NO_BORDER;
                            newTCL.itemName = ncaheaderA;
                        }
                        newTCL.Notes = item.TemplateCheckListItemNote;

                        var myattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(item.Id, dbConnection);
                        newTCL.Attachments = myattachments;


                        var stringCheck = string.Empty;
                        var itemNotes = string.Empty;

                        if (item.IsChecked)
                        {
                            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                            imgCell1.AddElement(new Phrase("3", gweb));
                            imgCell1.Border = Rectangle.NO_BORDER;
                            newTCL.Status = imgCell1;
                        }
                        else
                        {
                            List myList = new ZapfDingbatsList(54);

                            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                            imgCell1.AddElement(new Phrase("6", rweb));
                            imgCell1.Border = Rectangle.NO_BORDER;
                            newTCL.Status = imgCell1;

                            if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                chkCounter++;
                        }

                        newTCLlist.Add(newTCL);

                        if (item.ChildCheckList != null)
                        {
                            if (item.ChildCheckList.Count > 0)
                            {
                                foreach (var child in item.ChildCheckList)
                                {

                                    var cnewTCL = new TaskChecklistReportItems();
                                    cnewTCL.isParent = false;

                                    var ncaheaderA = new PdfPCell();

                                    if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                                        ncaheaderA.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                                    ncaheaderA.AddElement(new Phrase(child.Name, eleventimesI));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    cnewTCL.itemName = ncaheaderA;

                                    cnewTCL.Notes = child.TemplateCheckListItemNote;

                                    var mycattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(child.Id, dbConnection);
                                    cnewTCL.Attachments = mycattachments;

                                    if (child.IsChecked)
                                    {
                                        //gweb rweb
                                        iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1.AddElement(new Phrase("3", gweb));
                                        imgCell1.Border = Rectangle.NO_BORDER;
                                        cnewTCL.Status = imgCell1;

                                    }
                                    else
                                    {
                                        iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1.AddElement(new Phrase("6", rweb));
                                        imgCell1.Border = Rectangle.NO_BORDER;
                                        cnewTCL.Status = imgCell1;

                                        chkCounter++;
                                    }

                                    newTCLlist.Add(cnewTCL);
                                }
                            }
                        }
                    }
                }
                else
                {
                    chkStatus = "N/A";
                    chklistName = "None";
                    chkCounter = 0;
                }

                var headerMain = new PdfPCell();
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    headerMain.AddElement(new Phrase("G4S SERVICE REPORT", mbtimes));
                    headerMain.PaddingBottom = 10;
                    headerMain.HorizontalAlignment = 1;
                    headerMain.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell1 = new PdfPCell();
                    pdfCell1.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell2 = new PdfPCell();
                    pdfCell2.Border = Rectangle.NO_BORDER;
                    float[] columnWidthsHeader = new float[] { 30, 60, 10 };
                    mainheadertable.SetWidths(columnWidthsHeader);
                    mainheadertable.AddCell(pdfCell1);
                    mainheadertable.AddCell(headerMain);
                    mainheadertable.AddCell(pdfCell2);



                }
                else
                {
                    headerMain.AddElement(new Phrase("MIMS TASK REPORT", mbtimes));
                    headerMain.PaddingBottom = 10;
                    headerMain.HorizontalAlignment = 1;
                    headerMain.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell1 = new PdfPCell();
                    pdfCell1.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell2 = new PdfPCell();
                    pdfCell2.Border = Rectangle.NO_BORDER;
                    mainheadertable.AddCell(pdfCell1);
                    mainheadertable.AddCell(headerMain);
                    mainheadertable.AddCell(pdfCell2);
                }

                var headertable = new PdfPTable(2);
                headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                headertable.WidthPercentage = 100;
                float[] columnWidths2080 = new float[] { 20, 80 };
                headertable.SetWidths(columnWidths2080);

                var header1 = new PdfPCell();
                header1.AddElement(new Phrase("Task Name:", beleventimes));
                header1.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header1);

                var header1a = new PdfPCell();

                if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                    header1a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                header1a.AddElement(new Phrase(task.Name, eleventimes));
                header1a.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header1a);

                var header1z = new PdfPCell();
                header1z.AddElement(new Phrase("Task Number:", beleventimes));
                header1z.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header1z);

                var header1az = new PdfPCell();
                header1az.AddElement(new Phrase(task.NewCustomerTaskId, eleventimes));
                header1az.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header1az);

                var header2 = new PdfPCell();
                header2.AddElement(new Phrase("Description:", beleventimes));
                header2.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header2);

                var header2a = new PdfPCell();

                if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                    header2a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                header2a.AddElement(new Phrase(task.Description, eleventimes));
                header2a.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header2a);

                if (task.AssigneeType == (int)TaskAssigneeType.User)
                {
                    var gUser = Users.GetUserByName(task.AssigneeName, dbConnection);
                    if (gUser != null)
                    {
                        task.ACustomerUName = gUser.FirstName + " " + gUser.LastName;
                    }
                }
                else
                {
                    task.ACustomerUName = task.AssigneeName;
                }
                var tthistory = TaskEventHistory.GetTaskEventHistoryByTaskId(task.Id, dbConnection);
                var tt = CommonUtility.GetTravelTimeByTaskByDuration(tthistory);

                if (tt > 0)
                {
                    var header33 = new PdfPCell();
                    header33.AddElement(new Phrase("Travel Time(h):", beleventimes));
                    header33.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header33);

                    var header33a = new PdfPCell();
                    header33a.AddElement(new Phrase(CommonUtility.MinutesToHoursAndMinutes(tt).ToString(), eleventimes));
                    header33a.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header33a);
                }

                var ttt = TaskTravelHistory.GetTotalTravelTimebyTaskId(task.Id, dbConnection);
                if (ttt > 0)
                {
                    var header33 = new PdfPCell();
                    header33.AddElement(new Phrase("Auto Travel Time(h):", beleventimes));
                    header33.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header33);

                    var header33a = new PdfPCell();
                    header33a.AddElement(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttt).ToString(), eleventimes));
                    header33a.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header33a);
                }
                var header3 = new PdfPCell();
                header3.AddElement(new Phrase("Completed by:", beleventimes));
                header3.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header3);

                var header3a = new PdfPCell();
                header3a.AddElement(new Phrase(task.ACustomerUName, eleventimes));
                header3a.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header3a);

                var header4 = new PdfPCell();
                header4.AddElement(new Phrase("Notes:", beleventimes));
                header4.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header4);

                var header4a = new PdfPCell();
                if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                    header4a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                header4a.AddElement(new Phrase(task.Notes, eleventimes));
                header4a.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header4a);

                var header5 = new PdfPCell();
                header5.AddElement(new Phrase("Task Status:", beleventimes));
                header5.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header5);

                var header5a = new PdfPCell();
                header5a.AddElement(new Phrase(task.StatusDescription, eleventimes));
                header5a.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header5a);

                if (!string.IsNullOrEmpty(task.RejectionNotes))
                {
                    var header4R = new PdfPCell();
                    header4R.AddElement(new Phrase("Rejection Notes:", beleventimes));
                    header4R.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header4R);

                    var header4aR = new PdfPCell();
                    if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                        header4aR.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    header4aR.AddElement(new Phrase(task.RejectionNotes, eleventimes));
                    header4aR.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header4aR);
                }

                var header6 = new PdfPCell();
                header6.AddElement(new Phrase("Checklist Status:", beleventimes));
                header6.Border = Rectangle.NO_BORDER;
                header6.PaddingBottom = 20;
                headertable.AddCell(header6);

                if (chkCounter > 0)
                {
                    chkStatus = "Unchecked Items:" + chkCounter.ToString();
                }
                else
                {
                    chkStatus = "Complete";
                }
                var header6a = new PdfPCell();
                header6a.AddElement(new Phrase(chkStatus, eleventimes));
                header6a.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header6a);

                var taskdataTable = new PdfPTable(2);
                var taskdataTableA = new PdfPTable(2);
                var taskdataTableB = new PdfPTable(2);
                taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTable.WidthPercentage = 100;

                taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                taskdataTableA.WidthPercentage = 100;
                taskdataTableB.WidthPercentage = 100;

                float[] columnWidths2575 = new float[] { 25, 75 };
                float[] columnWidths3070 = new float[] { 30, 70 };
                float[] columnWidths3565 = new float[] { 35, 65 };
                float[] columnWidths4060 = new float[] { 40, 60 };
                taskdataTableA.SetWidths(columnWidths4060);

                taskdataTableB.SetWidths(columnWidths4060);

                //SIDE A
                var headerA1 = new PdfPCell();
                headerA1.AddElement(new Phrase("Task Type:", beleventimes));
                headerA1.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(headerA1);

                var headerA1a = new PdfPCell();

                if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                    headerA1a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                headerA1a.AddElement(new Phrase(task.TaskTypeName, eleventimes));
                headerA1a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(headerA1a);

                var headerA2 = new PdfPCell();
                headerA2.AddElement(new Phrase("Due Date:", beleventimes));
                headerA2.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(headerA2);

                var headerA2a = new PdfPCell();
                headerA2a.AddElement(new Phrase(task.StartDate.Value.AddHours(userinfo.TimeZone).ToString(), eleventimes));
                headerA2a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(headerA2a);

                if (task.TaskLinkId > 0)
                {
                    var headerA3 = new PdfPCell();
                    headerA3.AddElement(new Phrase("Main Task:", beleventimes));
                    headerA3.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(headerA3);

                    var ptask = UserTask.GetTaskById(task.TaskLinkId, dbConnection);
                    if (ptask != null)
                    {
                        var headerA3a = new PdfPCell();
                        headerA3a.AddElement(new Phrase(ptask.NewCustomerTaskId, eleventimes));
                        headerA3a.Border = Rectangle.NO_BORDER;
                        taskdataTableA.AddCell(headerA3a);
                    }
                    else
                    {
                        var headerA3a = new PdfPCell();
                        headerA3a.AddElement(new Phrase("N/A", eleventimes));
                        headerA3a.Border = Rectangle.NO_BORDER;
                        taskdataTableA.AddCell(headerA3a);
                    }
                }
                else if (tasklinks.Count > 0)
                {
                    var subNameTable = new PdfPTable(1);
                    var headerA4 = new PdfPCell();
                    headerA4.AddElement(new Phrase("Sub Tasks:", beleventimes));
                    headerA4.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(headerA4);

                    foreach (var link in tasklinks)
                    {
                        var headerA4a = new PdfPCell();
                        headerA4a.AddElement(new Phrase(link.NewCustomerTaskId, eleventimes));
                        headerA4a.Border = Rectangle.NO_BORDER;
                        subNameTable.AddCell(headerA4a);
                    }
                    taskdataTableA.AddCell(subNameTable);
                }
                else
                {
                    var headerA4a = new PdfPCell();
                    headerA4a.AddElement(new Phrase("N/A", eleventimes));
                    headerA4a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(headerA4a);
                }

                //SIDE B

                var headerB1 = new PdfPCell();
                headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                headerB1.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB1);


                var siteinfo = Arrowlabs.Business.Layer.Site.GetSiteById(task.SiteId, dbConnection);
                if (siteinfo != null)
                {
                    var headerB1a = new PdfPCell();

                    if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                        headerB1a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                    headerB1a.AddElement(new Phrase(siteinfo.Name, eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }
                else
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase("N/A", eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }

                var headerB2 = new PdfPCell();
                headerB2.AddElement(new Phrase("Account Name:", beleventimes));
                headerB2.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2);
                if (!string.IsNullOrEmpty(task.ClientName))
                {
                    var headerB2a = new PdfPCell();

                    if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                        headerB2a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                    headerB2a.AddElement(new Phrase(task.ClientName, eleventimes));
                    headerB2a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB2a);
                }
                else
                {
                    var headerB2a = new PdfPCell();
                    headerB2a.AddElement(new Phrase("N/A", eleventimes));
                    headerB2a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB2a);
                }
                var headerB3 = new PdfPCell();
                headerB3.AddElement(new Phrase("Contract Name:", beleventimes));
                headerB3.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB3);

                if (!string.IsNullOrEmpty(task.ContractName))
                {
                    var headerB3a = new PdfPCell();
                    if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                        headerB3a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerB3a.AddElement(new Phrase(task.ContractName, eleventimes));
                    headerB3a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB3a);
                }
                else
                {
                    var headerB3a = new PdfPCell();
                    headerB3a.AddElement(new Phrase("N/A", eleventimes));
                    headerB3a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB3a);
                }

                var headerB4 = new PdfPCell();
                headerB4.AddElement(new Phrase("Project Name:", beleventimes));
                headerB4.Border = Rectangle.NO_BORDER;
                headerB4.PaddingBottom = 20;
                taskdataTableB.AddCell(headerB4);

                if (!string.IsNullOrEmpty(task.ProjectName))
                {
                    var headerB4a = new PdfPCell();
                    if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                        headerB4a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerB4a.AddElement(new Phrase(task.ProjectName, eleventimes));
                    headerB4a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB4a);
                }
                else
                {
                    var headerB4a = new PdfPCell();
                    headerB4a.AddElement(new Phrase("N/A", eleventimes));
                    headerB4a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB4a);
                }

                taskdataTable.AddCell(taskdataTableA);
                taskdataTable.AddCell(taskdataTableB);

                var maptextdataTable = new PdfPTable(1);
                maptextdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                maptextdataTable.WidthPercentage = 100;

                var geolocation = ReverseGeocode.RetrieveFormatedAddress(task.Latitude.ToString(), task.Longitude.ToString(), getClientLic);
                if (string.IsNullOrEmpty(geolocation))
                    geolocation = "Not Specified";

                var mapheader = new PdfPCell();
                mapheader.AddElement(new Phrase("Assigned Task Location:" + geolocation, eleventimes));
                mapheader.Border = Rectangle.NO_BORDER;
                maptextdataTable.AddCell(mapheader);

                var mapdataTable = new PdfPTable(2);
                mapdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                mapdataTable.WidthPercentage = 100;
                float[] columnWidths7030 = new float[] { 70, 30 };
                mapdataTable.SetWidths(columnWidths7030);
                //MAP

                //outer.AddCell(table);
                var latComp = string.Empty;
                var longComp = string.Empty;
                var latEng = string.Empty;
                var longEng = string.Empty;
                var arrived = string.Empty;
                var vargeopath = string.Empty; //&path=color:0xff0000ff|weight:5|25.09773,55.16316|25.0978512224338,55.1634863298386
                var tracebackpath = string.Empty; //&path=color:blue|weight:3|25.09773,55.16316|25.0978512224338,55.1634863298386


                var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(task.Id, dbConnection);
                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;

                if (tasklinks.Count > 0)
                {
                    foreach (var link in tasklinks)
                    {
                        var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                        foreach (var forl in forlink)
                        {
                            forl.isSubTask = true;
                            taskeventhistory.Add(forl);
                        }
                    }
                    taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                }

                foreach (var taskEv in taskeventhistory)
                {
                    var timelineItem = new TimelineReportItems();
                    var taskn = "Task";
                    if (taskEv.isSubTask)
                    {
                        taskn = "Sub-task";
                    }

                    if (task.AssigneeType == (int)TaskAssigneeType.Group)
                    {
                        if (taskEv.Action == (int)TaskAction.Update)
                        {
                            completedBy = taskEv.CustomerFullName;
                            var compLocation = string.Empty;
                            var geoLoc = ReverseGeocode.RetrieveFormatedAddress(taskEv.Latitude.ToString(), taskEv.Longtitude.ToString(), getClientLic);
                            if (!string.IsNullOrEmpty(geoLoc))
                                compLocation = " at " + geoLoc;
                            var compInfo = "updated";
                            latComp = task.EndLatitude.ToString();
                            longComp = task.EndLongitude.ToString();
                            timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                            var compstring = completedBy + " " + compInfo + " " + taskn + " " + compLocation;

                            timelineItem.TimelineActivity = compstring;
                        }
                    }

                    if (taskEv.Action == (int)TaskAction.Complete)
                    {
                        completedBy = taskEv.CustomerFullName;
                        var compLocation = string.Empty;
                        var geoLoc = ReverseGeocode.RetrieveFormatedAddress(task.EndLatitude.ToString(), task.EndLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc))
                            compLocation = " at " + geoLoc;
                        var compInfo = "completed";
                        latComp = task.EndLatitude.ToString();
                        longComp = task.EndLongitude.ToString();
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                        var compstring = completedBy + " " + compInfo + " " + taskn + " " + compLocation;

                        timelineItem.TimelineActivity = compstring;
                        //listy.Add(compstring);
                    }
                    else if (taskEv.Action == (int)TaskAction.InProgress)
                    {
                        inprogressby = taskEv.CustomerFullName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "started";
                        // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                        var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                        //listy.Add(returnstring);
                        timelineItem.TimelineActivity = returnstring;

                        latEng = task.StartLatitude.ToString();
                        longEng = task.StartLongitude.ToString();
                    }
                    else if (taskEv.Action == (int)TaskAction.OnRoute)
                    {
                        inprogressby = taskEv.CustomerFullName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(taskEv.Latitude.ToString(), taskEv.Longtitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "on route";
                        // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                        var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                        //listy.Add(returnstring);
                        timelineItem.TimelineActivity = returnstring;

                        latEng = task.StartLatitude.ToString();
                        longEng = task.StartLongitude.ToString();
                    }
                    else if (taskEv.Action == (int)TaskAction.Pause)
                    {
                        inprogressby = taskEv.CustomerFullName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(taskEv.Latitude.ToString(), taskEv.Longtitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "paused";
                        // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                        var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                        //listy.Add(returnstring);
                        timelineItem.TimelineActivity = returnstring;

                        latEng = task.StartLatitude.ToString();
                        longEng = task.StartLongitude.ToString();
                    }
                    else if (taskEv.Action == (int)TaskAction.Resume)
                    {
                        inprogressby = taskEv.CustomerFullName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(taskEv.Latitude.ToString(), taskEv.Longtitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "resumed";
                        // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                        var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                        //listy.Add(returnstring);
                        timelineItem.TimelineActivity = returnstring;

                        latEng = task.StartLatitude.ToString();
                        longEng = task.StartLongitude.ToString();
                    }
                    else if (taskEv.Action == (int)TaskAction.Accepted)
                    {
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                        acceptedData = taskEv.CustomerFullName + " accepted " + taskn;
                        //listy.Add(acceptedData);
                        timelineItem.TimelineActivity = acceptedData;
                    }
                    else if (taskEv.Action == (int)TaskAction.Rejected)
                    {
                        //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                        rejectedData = taskEv.CustomerFullName + " rejected Task " + taskn;

                        timelineItem.TimelineActivity = rejectedData;
                        //listy.Add(rejectedData);
                    }
                    else if (taskEv.Action == (int)TaskAction.Assigned)
                    {
                        //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                        assignedData = taskEv.CustomerFullName + " assigned " + taskn + " to " + taskEv.ACustomerFullName;

                        timelineItem.TimelineActivity = assignedData;

                        //listy.Add(assignedData);
                    }
                    else if (taskEv.Action == (int)TaskAction.Pending)
                    {
                        var incidentLocation = string.Empty;
                        var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(task.Latitude.ToString(), task.Longitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc3))
                            incidentLocation = " at " + geoLoc3;
                        var actioninfo = "created";
                        //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                        timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        actioninfo = task.CustomerFullName + " " + actioninfo + " " + taskn + " for " + taskEv.ACustomerFullName + " " + incidentLocation;
                        timelineItem.TimelineActivity = actioninfo;
                        //listy.Add(actioninfo);
                    }
                    newTRIlist.Add(timelineItem);
                }
                var tracebackhistory = TraceBackHistory.GetTracBackHistoryBytaskId(task.Id, dbConnection);
                var tbCount = 0;
                var tbRemovedPointsCount = 0;

                var tbContinueCountStart = 0;
                if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                {
                    tbRemovedPointsCount = tracebackhistory.Count - CommonUtility.tracebackreportmaxcount;
                    tbContinueCountStart = (CommonUtility.tracebackreportmaxcount / 2) + tbRemovedPointsCount;
                }
                if (tracebackhistory.Count > 0)
                {
                    tracebackpath += "&path=color:blue|weight:5|";
                    tracebackpath += latEng + "," + longEng + "|";
                    foreach (var trace in tracebackhistory)
                    {
                        if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                        {
                            tbCount++;
                            if (tbCount < (CommonUtility.tracebackreportmaxcount / 2))
                                tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                            else if (tbCount > tbContinueCountStart)
                                tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                        }
                        else
                            tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                    }
                    tracebackpath += latComp + "," + longComp;
                }
                var maplat = task.Latitude.ToString();
                var maplong = task.Longitude.ToString();
                var taskLocation = string.Empty;
                if (task.Latitude > 0 && task.Longitude > 0)
                {
                    taskLocation = "&markers=color:red%7Clabel:T%7C" + task.Latitude + "," + task.Longitude;
                }
                else
                {
                    maplat = latEng;
                    maplong = longEng;
                }
                try
                {
                    String mapURL = "https://maps.googleapis.com/maps/api/staticmap?" +
    "center=" + maplat + "," + maplong + "&" +
    "size=700x360" + tracebackpath + vargeopath + arrived + "&markers=color:green%7Clabel:C%7C" + latComp + "," + longComp + "&markers=color:yellow%7Clabel:E%7C" + latEng + "," + longEng + taskLocation + "&zoom=17" + "&maptype=png" + "&sensor=true";
                    iTextSharp.text.Image LocationImage = iTextSharp.text.Image.GetInstance(mapURL);

                    LocationImage.ScaleToFit(380, 320); //305,320
                    LocationImage.Alignment = iTextSharp.text.Image.UNDERLYING;
                    LocationImage.SetAbsolutePosition(0, 0);
                    PdfPCell locationcell = new PdfPCell(LocationImage);
                    //locationcell.PaddingBottom = 10;
                    locationcell.PaddingTop = 5;
                    locationcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    //locationcell.Colspan = 3;
                    locationcell.Border = 0;
                    mapdataTable.AddCell(locationcell);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Task", "LocationImage", ex, dbConnection, userinfo.SiteId);
                }

                PdfPTable legendtable = new PdfPTable(1);
                legendtable.DefaultCell.Border = Rectangle.NO_BORDER;
                legendtable.HorizontalAlignment = 0;
                Font redfont = new Font();
                redfont.Size = 11;
                Font yellowfont = new Font();
                yellowfont.Size = 11;
                Font greenfont = new Font();
                greenfont.Size = 11;
                Font bluefont = new Font();
                bluefont.Size = 11;
                redfont.Color = Color.RED;
                legendtable.AddCell(new Phrase("Legend:", ueleventimes));
                legendtable.AddCell(new Phrase("T:  Task Location", redfont));
                yellowfont.Color = new Color(204, 204, 0);
                legendtable.AddCell(new Phrase("E:  Engaged Location", yellowfont));
                greenfont.Color = new Color(0, 204, 0);
                legendtable.AddCell(new Phrase("C:  Completed Location", greenfont));
                mapdataTable.AddCell(legendtable);

                iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                paragraphTable.SpacingBefore = 120f;
                Paragraph p = new Paragraph();
                p.IndentationLeft = 10;

                p.Add(mainheadertable);
                p.Add(headertable);
                p.Add(taskdataTable);
                p.Add(maptextdataTable);
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {

                }
                else
                    p.Add(mapdataTable);

                PdfPTable table = new PdfPTable(1);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                //Map style roadMap

                doc.Add(paragraphTable);

                doc.Add(p);

                //NEW PAGE
                //doc.NewPage();

                var checklistHeaderTable = new PdfPTable(1);
                checklistHeaderTable.DefaultCell.Border = Rectangle.NO_BORDER;
                checklistHeaderTable.WidthPercentage = 100;

                var checklistdataTable = new PdfPTable(2);
                checklistdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                checklistdataTable.WidthPercentage = 100;

                checklistdataTable.SetWidths(columnWidths2080);

                var cheader1 = new PdfPCell();
                cheader1.AddElement(new Phrase("Checklist", cbtimes));
                cheader1.Border = Rectangle.NO_BORDER;
                cheader1.PaddingTop = 10;
                cheader1.PaddingBottom = 10;
                cheader1.Colspan = 2;
                checklistdataTable.AddCell(cheader1);

                var caheader1 = new PdfPCell();
                caheader1.AddElement(new Phrase("Checklist Name:", beleventimes));
                caheader1.Border = Rectangle.NO_BORDER;
                checklistdataTable.AddCell(caheader1);

                var caheader1a = new PdfPCell();
                if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                    caheader1a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                caheader1a.AddElement(new Phrase(chklistName, eleventimes));
                caheader1a.Border = Rectangle.NO_BORDER;
                checklistdataTable.AddCell(caheader1a);

                var caheader2 = new PdfPCell();
                caheader2.AddElement(new Phrase("Checklist Notes:", beleventimes));
                caheader2.Border = Rectangle.NO_BORDER;
                checklistdataTable.AddCell(caheader2);

                var caheader2a = new PdfPCell();
                if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                    caheader2a.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                caheader2a.AddElement(new Phrase(task.CheckListNotes, eleventimes));
                caheader2a.Border = Rectangle.NO_BORDER;
                checklistdataTable.AddCell(caheader2a);

                var caheader3 = new PdfPCell();
                caheader3.AddElement(new Phrase("Checklist Status:", beleventimes));
                caheader3.Border = Rectangle.NO_BORDER;
                caheader3.PaddingBottom = 20;
                checklistdataTable.AddCell(caheader3);

                var caheader3a = new PdfPCell();
                caheader3a.AddElement(new Phrase(chkStatus, eleventimes));
                caheader3a.Border = Rectangle.NO_BORDER;
                checklistdataTable.AddCell(caheader3a);

                doc.Add(checklistHeaderTable);
                doc.Add(checklistdataTable);

                var newchecklistTable = new PdfPTable(4);
                newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                newchecklistTable.WidthPercentage = 100;
                float[] columnWidthsCHK = new float[] { 30, 10, 30, 30 };
                newchecklistTable.SetWidths(columnWidthsCHK);

                var ncaheader1 = new PdfPCell();
                ncaheader1.AddElement(new Phrase("Item Name", weleventimes));
                ncaheader1.Border = Rectangle.NO_BORDER;
                ncaheader1.PaddingLeft = 5;
                ncaheader1.PaddingBottom = 5;
                ncaheader1.BackgroundColor = Color.DARK_GRAY;

                newchecklistTable.AddCell(ncaheader1);

                var ncaheader2 = new PdfPCell();
                ncaheader2.AddElement(new Phrase("Status", weleventimes));
                ncaheader2.Border = Rectangle.NO_BORDER;
                ncaheader2.BackgroundColor = Color.DARK_GRAY;
                newchecklistTable.AddCell(ncaheader2);

                var ncaheader3 = new PdfPCell();
                ncaheader3.AddElement(new Phrase("Notes", weleventimes));
                ncaheader3.Border = Rectangle.NO_BORDER;
                ncaheader3.BackgroundColor = Color.DARK_GRAY;
                newchecklistTable.AddCell(ncaheader3);

                var ncaheader4 = new PdfPCell();
                ncaheader4.AddElement(new Phrase("Attachments", weleventimes));
                ncaheader4.Border = Rectangle.NO_BORDER;
                ncaheader4.BackgroundColor = Color.DARK_GRAY;
                newchecklistTable.AddCell(ncaheader4);

                ///CHECKLIST 
                ///
                foreach (var item in newTCLlist)
                {

                    newchecklistTable.AddCell(item.itemName);

                    newchecklistTable.AddCell(item.Status);

                    var ncaheaderAB = new PdfPCell();
                    if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                        ncaheaderAB.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    ncaheaderAB.AddElement(new Phrase(item.Notes, eleventimes));
                    ncaheaderAB.Border = Rectangle.NO_BORDER;
                    newchecklistTable.AddCell(ncaheaderAB);

                    if (item.Attachments.Count > 0)
                    {
                        PdfPTable imgTable = new PdfPTable(1);
                        imgTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        foreach (var attach in item.Attachments)
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant()))
                            {
                                imgTable.AddCell(new Phrase("Video Attached", eleventimes));
                            }
                            else if (System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                imgTable.AddCell(new Phrase("Audio Attached", eleventimes));
                            }
                            else if (System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant() == ".PDF")
                            {
                                imgTable.AddCell(new Phrase("PDF Attached", eleventimes));
                            }
                            else
                            {
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + task.Id + "/{0}", System.IO.Path.GetFileName(attach.DocumentPath));
                                try
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(attach.DocumentPath);

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = Rectangle.NO_BORDER;
                                    iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1l.Border = Rectangle.NO_BORDER;
                                    imgCell1l.AddElement(new Chunk(image, 0, 0));
                                    imgTable.AddCell(imgCell1l);
                                }
                                catch (Exception exp)
                                {
                                    MIMSLog.MIMSLogSave("GeneratePDFTask", "Load image url task attachment", exp, dbConnection);

                                    if (attach.IsSignature)
                                    {
                                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance("https://livemimslob.blob.core.windows.net/0blob/signaturefailure.png");

                                        if (image.Height > image.Width)
                                        {
                                            //Maximum height is 800 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Height;
                                            image.ScalePercent(percentage * 100);
                                        }
                                        else
                                        {
                                            //Maximum width is 600 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Width;
                                            image.ScalePercent(percentage * 100);
                                        }

                                        //image.ScaleAbsolute(120f, 155.25f);
                                        image.Border = Rectangle.NO_BORDER;
                                        iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1l.Border = Rectangle.NO_BORDER;
                                        imgCell1l.AddElement(new Chunk(image, 0, 0));
                                        imgTable.AddCell(imgCell1l);
                                    }
                                    else
                                    {
                                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance("https://livemimslob.blob.core.windows.net/0blob/mobileimagefailure.png");

                                        if (image.Height > image.Width)
                                        {
                                            //Maximum height is 800 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Height;
                                            image.ScalePercent(percentage * 100);
                                        }
                                        else
                                        {
                                            //Maximum width is 600 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Width;
                                            image.ScalePercent(percentage * 100);
                                        }

                                        //image.ScaleAbsolute(120f, 155.25f);
                                        image.Border = Rectangle.NO_BORDER;
                                        iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1l.Border = Rectangle.NO_BORDER;
                                        imgCell1l.AddElement(new Chunk(image, 0, 0));
                                        imgTable.AddCell(imgCell1l);
                                    }
                                }
                                if (!string.IsNullOrEmpty(attach.ImageNote) && attach.ImageNote != "Task Attachment")
                                    imgTable.AddCell(new Phrase("Img Note:" + attach.ImageNote, eleventimes));
                            }
                        }
                        newchecklistTable.AddCell(imgTable);
                    }
                    else
                    {
                        newchecklistTable.AddCell(new Phrase(" ", eleventimes));
                    }
                }

                doc.Add(newchecklistTable);

                var listycanvasnotes = new List<string>();
                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(task.Id, dbConnection);
                var videoCount = 0;
                var imgCount = 0;
                var pdfCount = 0;

                PdfPTable taskimagetable = new PdfPTable(2);
                taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskimagetable.WidthPercentage = 100;

                var atheader1 = new PdfPCell();
                atheader1.AddElement(new Phrase("Attachments", cbtimes));
                atheader1.Border = Rectangle.NO_BORDER;
                atheader1.PaddingTop = 10;
                atheader1.PaddingBottom = 10;
                atheader1.Colspan = 2;
                taskimagetable.AddCell(atheader1);

                if (attachments.Count > 0)
                {
                    var attachcount = 0;
                    var canvasattachcount = 0;
                    var attachlist = new List<UserTaskAttachment>();

                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath) && item.ChecklistId == 0)
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                videoCount++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                videoCount++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {
                                pdfCount++;
                            }
                            else
                            {
                                attachlist.Add(item);
                                attachcount++;
                                imgCount++;
                                if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                                {
                                    listycanvasnotes.Add("Canvas Notes Image " + attachcount + ":" + item.ImageNote);
                                    canvasattachcount++;
                                }
                                else
                                {
                                    listycanvasnotes.Add("Image " + attachcount);
                                    canvasattachcount++;
                                }
                            }
                        }
                    }
                    taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                    var countz = 0;
                    foreach (var item in attachlist)
                    {
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                        {

                        }
                        else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                        {

                        }
                        else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                        {

                        }
                        else
                        {
                            try
                            {
                                var attach = item;
                                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(attach.DocumentPath);

                                if (image.Height > image.Width)
                                {
                                    //Maximum height is 800 pixels.
                                    float percentage = 0.0f;
                                    percentage = 155 / image.Height;
                                    image.ScalePercent(percentage * 100);
                                }
                                else
                                {
                                    //Maximum width is 600 pixels.
                                    float percentage = 0.0f;
                                    percentage = 155 / image.Width;
                                    image.ScalePercent(percentage * 100);
                                }

                                //image.ScaleAbsolute(120f, 155.25f);
                                image.Border = Rectangle.NO_BORDER;
                                iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                imgCell1l.Border = Rectangle.NO_BORDER;
                                imgCell1l.AddElement(new Chunk(image, 0, 0));
                                taskimagetable.AddCell(imgCell1l);
                                countz++;
                            }
                            catch (Exception exp)
                            {
                                MIMSLog.MIMSLogSave("GeneratePDFTask", "Load image url task attachment", exp, dbConnection);
                                var attach = item;
                                if (attach.IsSignature)
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance("https://livemimslob.blob.core.windows.net/0blob/signaturefailure.png");

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = Rectangle.NO_BORDER;
                                    iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1l.Border = Rectangle.NO_BORDER;
                                    imgCell1l.AddElement(new Chunk(image, 0, 0));
                                    taskimagetable.AddCell(imgCell1l);
                                }
                                else
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance("https://livemimslob.blob.core.windows.net/0blob/mobileimagefailure.png");

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = Rectangle.NO_BORDER;
                                    iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1l.Border = Rectangle.NO_BORDER;
                                    imgCell1l.AddElement(new Chunk(image, 0, 0));
                                    taskimagetable.AddCell(imgCell1l);
                                }
                                //iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.DocumentPath);

                                //if (image.Height > image.Width)
                                //{
                                //    //Maximum height is 800 pixels.
                                //    float percentage = 0.0f;
                                //    percentage = 155 / image.Height;
                                //    image.ScalePercent(percentage * 100);
                                //}
                                //else
                                //{
                                //    //Maximum width is 600 pixels.
                                //    float percentage = 0.0f;
                                //    percentage = 155 / image.Width;
                                //    image.ScalePercent(percentage * 100);
                                //}

                                ////image.ScaleAbsolute(120f, 155.25f);
                                //image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                                //iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                //if (!string.IsNullOrEmpty(listycanvasnotes[countz]))
                                //{
                                //    Paragraph pcell = new Paragraph(listycanvasnotes[countz].ToUpper());
                                //    pcell.Alignment = Element.ALIGN_LEFT;
                                //    imgCell1.AddElement(pcell);
                                //}
                                //imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                //imgCell1.AddElement(new Chunk(image, 0, 0));
                                //imgCell1.PaddingTop = 10;
                                //taskimagetable.AddCell(imgCell1);
                                countz++;
                            }
                        }
                    }
                    taskimagetable.CompleteRow();
                }

                var extraattachment = new PdfPTable(3);
                extraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                extraattachment.WidthPercentage = 100;
                var phrase2 = new Phrase();
                phrase2.Add(
                    new Chunk("PDF Attached:  ", btimes)
                );
                phrase2.Add(new Chunk(pdfCount.ToString(), times));

                var zheaderA3a = new PdfPCell();
                zheaderA3a.AddElement(phrase2);
                zheaderA3a.Border = Rectangle.NO_BORDER;
                zheaderA3a.PaddingTop = 20;
                zheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(zheaderA3a);

                var phrase32 = new Phrase();
                phrase32.Add(
                    new Chunk("Media Attached:  ", btimes)
                );
                phrase32.Add(new Chunk(videoCount.ToString(), times));

                var aheaderA3a = new PdfPCell();
                aheaderA3a.AddElement(phrase32);
                aheaderA3a.Border = Rectangle.NO_BORDER;
                aheaderA3a.PaddingTop = 20;
                aheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aheaderA3a);

                var phrase323 = new Phrase();
                phrase323.Add(
                    new Chunk("Images Attached:  ", btimes)
                );
                phrase323.Add(new Chunk(imgCount.ToString(), times));

                var aaheaderA3a = new PdfPCell();
                aaheaderA3a.AddElement(phrase323);
                aaheaderA3a.Border = Rectangle.NO_BORDER;
                aaheaderA3a.PaddingTop = 20;
                aaheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aaheaderA3a);

                doc.Add(taskimagetable);

                doc.Add(extraattachment);

                PdfPTable timetable = new PdfPTable(2);
                timetable.WidthPercentage = 100;
                timetable.DefaultCell.Border = Rectangle.NO_BORDER;
                timetable.SetWidths(columnWidths2575);

                if (newTRIlist.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Timeline", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    timetable.AddCell(headeventCell);

                    foreach (var lit in newTRIlist)
                    {
                        var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(eventCell);
                    }
                }
                doc.Add(timetable);

                PdfPTable notestable = new PdfPTable(2);
                notestable.WidthPercentage = 100;
                notestable.DefaultCell.Border = Rectangle.NO_BORDER;
                notestable.SetWidths(columnWidths2575);

                var tremarks = TaskRemarks.GetTaskRemarksByTaskId(task.Id, dbConnection);

                if (tremarks.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Notes", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    notestable.AddCell(headeventCell);
                    foreach (var remark in tremarks)
                    {
                        var dateCell = new PdfPCell(new Phrase(remark.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString(), redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        notestable.AddCell(dateCell);
                        string content = string.Format("{0} " + remark.Remarks, remark.FName + " " + remark.LName + ":");

                        var eventCell = new PdfPCell(new Phrase(content, eleventimes));
                        if (userinfo.CustomerInfoId == 121 || userinfo.SiteId == 130)
                            eventCell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        eventCell.Border = Rectangle.NO_BORDER;
                        notestable.AddCell(eventCell);
                    }
                }

                doc.Add(notestable);

                Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                var endTable = new PdfPTable(1);
                endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                endTable.WidthPercentage = 100;

                var endheader = new PdfPCell();
                endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                endheader.Border = Rectangle.NO_BORDER;
                endTable.AddCell(endheader);
                doc.Add(endTable);

                writer.Flush();
                doc.Close();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                        return vpath;
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Task", "GeneratePDFTask", exp, dbConnection, userinfo.SiteId);
                return "Failed to generate report!";
            }
            return "Failed to generate report!";
        }

        [WebMethod]
        public static string deleteAttachmentData(int id, int taskid, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.UserTaskAttachment.GetTaskAttachmentsByTaskId(taskid, dbConnection);
                    if (ret != null)
                    {
                        ret = ret.Where(i => i.Id == id).ToList();
                        if (ret.Count > 0)
                        {
                            ret[0].IsDeleted = true;
                            ret[0].UpdatedDate = CommonUtility.getDTNow();
                            ret[0].UpdatedBy = userinfo.Username;
                            UserTask.InsertOrUpdateTaskAttachment(ret[0], dbConnection, dbConnectionAudit, true);

                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "UserTaskAttachment", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());


                            CommonUtility.CloudDeleteFile(ret[0].DocumentPath);

                            listy = "Successfully deleted entry";
                        }
                        else
                            listy = "Failed to delete entry";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("TaskDash", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getSubTaskListData(int id, string uname)
        {

            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            try
            {
                var chklisty = new List<string>();
                var usertask = UserTask.GetAllTasksByTaskLinkId(id, dbConnection);
                foreach (var item in usertask)
                {
                    var stringCheck = string.Empty;
                    if (item.Status == (int)TaskStatus.Completed)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                    }

                    if (item.Status == (int)TaskStatus.OnRoute || item.Status == (int)TaskStatus.InProgress)
                    {
                        item.StatusDescription = "InProgress";
                    }

                    chklisty.Add(item.Name + "|" + stringCheck + "|" + item.Id.ToString() + "|" + item.StatusDescription);
                }
                listy.AddRange(chklisty);
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("UsersDB", "getSubTaskListData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTaskRemarksData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = UserTask.GetTaskById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = TaskRemarks.GetTaskRemarksByTaskId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Notes))
                    {
                        var nTR = new TaskRemarks();
                        nTR.CustomerUName = projinfo.ACustomerUName;
                        nTR.Remarks = projinfo.Notes;
                        nTR.CreatedDate = projinfo.UpdatedDate.Value;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        var count = 0;
                        foreach (var task in remarksList)
                        {
                            if (count == 3)
                            {
                                break;
                            }
                            count++;
                            var OGremarks = task.Remarks;
                            if (!string.IsNullOrEmpty(task.Remarks) && task.Remarks.Length > 50)
                            {
                                task.Remarks = task.Remarks.Remove(50);
                                task.Remarks = task.Remarks + "...";
                            }
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                        if (remarksList.Count > 3)
                        {
                            var seeall = "<h5><span  class='line-center' onmouseover='style=&apos;cursor: pointer;&apos;' onclick='showAllRemarks(&apos;" + id + "&apos;)'>SEE ALL</span></h5>";
                            listy.Add(seeall);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTaskRemarksData2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = UserTask.GetTaskById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = TaskRemarks.GetTaskRemarksByTaskId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Notes))
                    {
                        var nTR = new TaskRemarks();
                        nTR.CustomerUName = projinfo.ACustomerUName;
                        nTR.Remarks = projinfo.Notes;
                        nTR.CreatedDate = projinfo.UpdatedDate.Value;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        foreach (var task in remarksList)
                        {
                            var OGremarks = task.Remarks;

                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }


        [WebMethod]
        public static List<string> geTicketHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allEventHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var alltasks = UserTask.GetAllTaskByIncidentId(Convert.ToInt32(id), dbConnection);
                if (alltasks.Count > 0)
                {
                    foreach (var item in alltasks)
                    {
                        var alltaskhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(item.Id, dbConnection);

                        var taskeventhistory = new List<TaskEventHistory>();

                        var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(item.Id), dbConnection);
                        if (tasklinks.Count > 0)
                        {
                            foreach (var link in tasklinks)
                            {
                                var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                                foreach (var forl in forlink)
                                {
                                    forl.isSubTask = true;
                                    taskeventhistory.Add(forl);
                                }
                            }
                            taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                        }

                        alltaskhistory.AddRange(taskeventhistory);

                        if (alltaskhistory.Count > 0)
                        {
                            foreach (var task in alltaskhistory)
                            {
                                if (task.Action != (int)TaskAction.Update)
                                {
                                    var evhistory = new EventHistory();
                                    if (task.isSubTask)
                                    {
                                        evhistory.isSubTask = true;
                                    }
                                    else
                                        evhistory.isTask = true;
                                    evhistory.TaskName = task.TaskName;
                                    if (task.Action == (int)TaskAction.Pending)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                                    else if (task.Action == (int)TaskAction.InProgress)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                                    else if (task.Action == (int)TaskAction.OnRoute)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.OnRoute;
                                    else if (task.Action == (int)TaskAction.Pause)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pause;
                                    else if (task.Action == (int)TaskAction.Resume)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resume;
                                    else if (task.Action == (int)TaskAction.Complete)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                                    else if (task.Action == (int)TaskAction.Accepted)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                                    else if (task.Action == (int)TaskAction.Rejected)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Reject;

                                    evhistory.CustomerUName = task.CustomerUName;
                                    evhistory.ACustomerUName = task.CustomerUName;
                                    evhistory.CreatedDate = task.CreatedDate;
                                    evhistory.Latitude = task.Latitude;
                                    evhistory.Longtitude = task.Longtitude;
                                    allEventHistory.Add(evhistory);
                                }
                                if (item.AssigneeType == (int)TaskAssigneeType.Group)
                                {
                                    if (task.Action == (int)TaskAction.Update)
                                    {
                                        var evhistory = new EventHistory();
                                        if (task.isSubTask)
                                        {
                                            evhistory.isSubTask = true;
                                        }
                                        else
                                            evhistory.isTask = true;

                                        evhistory.TaskName = task.TaskName;
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Updated;
                                        evhistory.CustomerUName = task.CustomerUName;
                                        evhistory.ACustomerUName = task.CustomerUName;
                                        evhistory.CreatedDate = task.CreatedDate;
                                        evhistory.Latitude = task.Latitude;
                                        evhistory.Longtitude = task.Longtitude;
                                        allEventHistory.Add(evhistory);
                                    }
                                }
                            }
                        }
                    }
                }
                allEventHistory = allEventHistory.OrderByDescending(i => i.CreatedDate).ToList();

                foreach (var rem in allEventHistory)
                {
                    var remname = string.Empty;
                    if (rem.EventId > 0)
                    {
                        var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                        if (gethotevent != null)
                            gethotevent.Name = "Ticket";

                        remname = gethotevent.Name;
                    }
                    if (rem.isTask)
                    {
                        if (!string.IsNullOrEmpty(rem.TaskName))
                            remname = rem.TaskName;
                        else
                            remname = "Task";

                    }
                    else if (rem.isSubTask)
                    {
                        if (!string.IsNullOrEmpty(rem.TaskName))
                            remname = rem.TaskName;
                        else
                            remname = "Sub Task";
                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerUName;
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.OnRoute)
                    {
                        actioninfo = "onroute ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pause)
                    {
                        actioninfo = "paused ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resume)
                    {
                        actioninfo = "resumed ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "started ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";

                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Updated)
                    {
                        actioninfo = "updated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Ticketing", "geTicketHistoryData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTicketRemarksData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = CustomEventRemarks.GetCustomEventRemarksByEventId(id, dbConnection);


                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        var count = 0;
                        foreach (var task in remarksList)
                        {
                            if (count == 3)
                            {
                                break;
                            }
                            count++;
                            var OGremarks = task.Remarks;
                            if (!string.IsNullOrEmpty(task.Remarks) && task.Remarks.Length > 50)
                            {
                                task.Remarks = task.Remarks.Remove(50);
                                task.Remarks = task.Remarks + "...";
                            }
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                        if (remarksList.Count > 3)
                        {
                            var seeall = "<h5><span  class='line-center' onmouseover='style=&apos;cursor: pointer;' onclick='showAllTicketingRemarks(&apos;" + id + "&apos;)'>SEE ALL</span></h5>";
                            listy.Add(seeall);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTicketRemarksData2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = CustomEventRemarks.GetCustomEventRemarksByEventId(id, dbConnection);


                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        foreach (var task in remarksList)
                        {
                            var OGremarks = task.Remarks;
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketRemarksData2", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string addNewTicketingRemarks(int id, string notes, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var newRemarks = new CustomEventRemarks();
                    newRemarks.CreatedBy = userinfo.Username;
                    newRemarks.CreatedDate = CommonUtility.getDTNow();
                    newRemarks.EventId = id;
                    newRemarks.Remarks = notes;
                    newRemarks.UpdatedBy = userinfo.Username;
                    newRemarks.UpdatedDate = CommonUtility.getDTNow();
                    newRemarks.CustomerInfoId = userinfo.CustomerInfoId;
                    newRemarks.SiteId = userinfo.SiteId;
                    var retV = CustomEventRemarks.InsertOrUpdateCustomEventRemarks(newRemarks, dbConnection, dbConnectionAudit, true);
                    if (retV > 0)
                    {
                        json = "SUCCESS";
                    }
                    else
                    {
                        json = "Problem faced trying to save notes";
                    }
                }
                else
                {
                    json = "Problem faced trying to get ticket and save notes";
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "addNewTicketingRemarks", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }
 
        [WebMethod]
        public static List<string> getTableRowDataTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);

                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (offenceinfo != null)
                    {
                        listy.Add(offenceinfo.PlateNumber);
                        listy.Add(offenceinfo.PlateSource);
                        listy.Add(offenceinfo.PlateCode);
                        listy.Add(offenceinfo.VehicleMake);
                        listy.Add(offenceinfo.VehicleColor);
                        var ggUser = Users.GetUserByName(offenceinfo.UserName, dbConnection);
                        listy.Add(ggUser.CustomerUName);
                        var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longtitude.ToString(), getClientLic);
                        listy.Add(geolocation);
                        listy.Add(offenceinfo.CreatedDate.AddHours(userinfo.TimeZone).ToString());
                        listy.Add(offenceinfo.OffenceCategory);
                        listy.Add(offenceinfo.OffenceType);
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            if (offenceinfo.TicketStatus == 4)
                            {
                                listy.Add("5");
                            }
                            else
                                listy.Add("6");
                        }
                        else
                        {
                            if (customData.Handled)
                            {
                                listy.Add("5");
                            }
                            else
                                listy.Add(offenceinfo.TicketStatus.ToString());
                        }

                        listy.Add(offenceinfo.Comments);
                        if (!string.IsNullOrEmpty(offenceinfo.CusName))
                            listy.Add(offenceinfo.CusName);
                        else
                        {
                            listy.Add("N/A");
                        }
                        if (!string.IsNullOrEmpty(offenceinfo.ContractName))
                            listy.Add(offenceinfo.ContractName);
                        else
                        {
                            listy.Add("N/A");
                        }
                        listy.Add(offenceinfo.ContractId.ToString());
                        listy.Add(offenceinfo.CusId.ToString());
                        var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);
                        if (usertask.Count > 0)
                        {
                            usertask = usertask.Where(i => i.Status != (int)TaskStatus.Completed && i.Status != (int)TaskStatus.Accepted).ToList();
                            if (usertask.Count > 0)
                            {
                                listy.Add("0");
                            }
                            else
                            {
                                listy.Add("1");
                            }
                        }
                        else
                        {
                            listy.Add("1");
                        }

                        if (offenceinfo.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                        {
                            customData.StatusName = "Inprogress";
                        }
                        else if (offenceinfo.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                        {
                            customData.StatusName = "Completed";
                        }

                        if (customData.Handled)
                            customData.StatusName = "Resolved";

                        listy.Add(customData.StatusName);
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTableRowDataTicket", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskListDataTicket(int id, string uname)
        {

            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var chklisty = new List<string>();
                var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);

                var module = "false";

                var gUserMod = UserModules.GetUserModuleMapByUserIdAndModuleId(userinfo.ID, (int)Accounts.ModuleTypes.Tasks, dbConnection);

                if (gUserMod.Count > 0)
                    module = "true";

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    module = "true";
                }

                foreach (var item in usertask)
                {
                    var stringCheck = string.Empty;
                    if (item.Status == (int)TaskStatus.Completed || item.Status == (int)TaskStatus.Accepted)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                    }

                    if (item.Status == (int)TaskStatus.OnRoute || item.Status == (int)TaskStatus.InProgress)
                    {
                        item.StatusDescription = "InProgress";
                    }

                    chklisty.Add(item.Name + "|" + stringCheck + "|" + item.Id.ToString() + "|" + item.StatusDescription + "|" + module);

                    var linktasks = UserTask.GetAllTasksByTaskLinkId(item.Id, dbConnection);
                    if (linktasks.Count > 0)
                    {
                        foreach (var item2 in linktasks)
                        {
                            stringCheck = string.Empty;
                            if (item2.Status == (int)TaskStatus.Completed || item.Status == (int)TaskStatus.Accepted)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                            }

                            if (item2.Status == (int)TaskStatus.OnRoute || item2.Status == (int)TaskStatus.InProgress)
                            {
                                item2.StatusDescription = "InProgress";
                            }

                            chklisty.Add(item2.Name + "|" + stringCheck + "|" + item2.Id.ToString() + "|" + item2.StatusDescription + "|" + module);
                        }
                    }
                }
                listy.AddRange(chklisty);
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Ticketing", "getTaskListDataTicket", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getAttachmentDataIconsTicket(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var i = 0;
                //var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/OffenceImagePath.jpeg";
                var retstring2 = string.Empty;
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    retstring2 = "<img src='" + attachments.OffenceImagePath1 + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                    listy += retstring2;
                    i++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    var imgstring = settings.MIMSMobileAddress + "/Images/videoicon.png";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#video-0-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath1.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-2-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath2.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-3-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath3.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-4-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath4.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-5-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath5.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-6-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath6.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-7-tab'/>";
                    listy += retstring2;

                    i = 7;
                }
                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                if (evattachments.Count > 0)
                {
                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {

                            if (i == 3)
                            {
                                var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#ticketattachments-tab' />";
                                listy += retstring;
                                i++;
                                break;
                            }
                            else
                            {
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index)); 
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    var retstring = "<img src='" + requiredString + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentDataIcons", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTabTicket(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var i = 0;
                var iTab = 0;
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#ticketlocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Location</p></div></div></div>";
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Image</p></div></div></div>";
                    i++;
                    iTab++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(1)' data-target='#video-0-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Video</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-2-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 1</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-3-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 2</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-4-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 3</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-5-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 4</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-6-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 5</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-7-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 6</p></div></div></div>";
                    i = 7;
                    iTab = 7;
                }

                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);

                if (evattachments.Count > 0)
                {
                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            // int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            // requiredString = requiredString.Replace("\\", "/");

                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {
                                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                //imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/HotEvents/{0}", System.IO.Path.GetFileName(requiredString));

                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentName, i);
                                //if (!string.IsNullOrEmpty(attachmentName))
                                //    attachmentName = attachmentName.Split('-')[1];
                                var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + requiredString + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + requiredString + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;
                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentName, i);
                                //if (!string.IsNullOrEmpty(attachmentName))
                                //    attachmentName = attachmentName.Split('-')[1];
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                        else
                        {
                            if (item.Attachment != null)
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentDataTab", err, dbConnection, userinfo.SiteId);
            }

            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentDataTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var retstring = string.Empty;
                //var retstring = "<video id='Video0' width='100%' height='380px' autoplay='autoplay' type='video/ogg; codecs=theo' muted controls ><source src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' /></video>";
                //var retstring = "<video id='Video0' width='100%' height='380px' src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' type='video/ogg; codecs=theo' autoplay='autoplay' />";
                //listy.Add(retstring);
                var i = 0;
                //var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/OffenceImagePath.jpeg";
                var retstring2 = string.Empty;
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    retstring2 = "<img onclick='rotateMe(this);' src='" + attachments.OffenceImagePath1 + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);
                    i++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath1.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath2.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath3.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath4.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath5.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath6.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    i = 6;
                }
                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);

                if (evattachments.Count > 0)
                {

                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" + requiredString + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                            {

                            }
                            else
                            {
                                //imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                                retstring = "<img onclick='rotateMe(this);' src='" + requiredString + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }

                        }
                        else
                        {
                            if (item.Attachment != null)
                            {
                                var base64 = Convert.ToBase64String(item.Attachment);
                                imgstring = String.Format("data:image/png;base64,{0}", base64);
                                retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentDataTicket", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string attachFileToTicket(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new MobileHotEventAttachment();

                    var newimgPath = filepath.Replace('|', '\\');
                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            newObj.AttachmentPath = savestring;
                            newObj.AttachmentName = System.IO.Path.GetFileName(savestring);
                            //newObj.SiteId = cusEv.SiteId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            newObj.CreatedBy = userinfo.Username;
                            newObj.CreatedDate = CommonUtility.getDTNow();
                            newObj.EventId = id;
                            newObj.IsPortal = true;
                            newObj.SiteId = userinfo.SiteId;
                            newObj.CustomerId = userinfo.CustomerInfoId;
                            MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newObj, dbConnection, dbConnectionAudit, true);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Incident", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Incident", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToTask", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static string attachFileToInci(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new MobileHotEventAttachment();

                    var newimgPath = filepath.Replace('|', '\\');
                    //var mimsconfigsettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    //var savestring = mimsconfigsettings.FilePath.Remove(mimsconfigsettings.FilePath.Length - 5);
                    //savestring = savestring + "HotEvents";
                    //if (!System.IO.Directory.Exists(savestring))
                    //    System.IO.Directory.CreateDirectory(savestring);

                    //if (System.IO.Path.GetExtension(newimgPath).ToUpperInvariant() == ".PDF")
                    //{
                    //    var bytes = File.ReadAllBytes(newimgPath);
                    //    var Tsavestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + ".pdf";
                    //    if (!System.IO.File.Exists(Tsavestring))
                    //    {
                    //        savestring = Tsavestring;
                    //        File.WriteAllBytes(savestring, bytes);
                    //    }
                    //    else
                    //    {
                    //        savestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + "(1).pdf";
                    //        File.WriteAllBytes(savestring, bytes);
                    //    }
                    //}
                    //else
                    //{
                    //    var bmap = new System.Drawing.Bitmap(newimgPath);
                    //    var Tsavestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + ".jpg";
                    //    if (!System.IO.File.Exists(Tsavestring))
                    //    {
                    //        savestring = Tsavestring;
                    //        bmap.Save(savestring);
                    //    }
                    //    else
                    //    {
                    //        savestring = savestring + "\\" + System.IO.Path.GetFileNameWithoutExtension(newimgPath) + "(1).jpg";
                    //        bmap.Save(savestring);
                    //    }
                    //}


                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);
                            newObj.AttachmentName = System.IO.Path.GetFileName(savestring);
                            newObj.AttachmentPath = savestring;
                            //newObj.SiteId = cusEv.SiteId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            newObj.CreatedBy = userinfo.Username;
                            newObj.CreatedDate = CommonUtility.getDTNow();
                            newObj.EventId = id;
                            newObj.IsPortal = true;
                            newObj.SiteId = userinfo.SiteId;
                            newObj.CustomerId = userinfo.CustomerInfoId;
                            MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newObj, dbConnection, dbConnectionAudit, true);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Incident", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Incident", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToTask", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static string UpdateTicketStatus(int id, string status, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (status.ToUpper() == "START")
                        {


                            //cusEv.IncidentStatus = CommonUtility.getIncidentStatusValue("dispatch");
                            //cusEv.Handled = false;
                            ////incidentinfo.SiteId = userinfo.SiteId;
                            //cusEv.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            //cusEv.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                            //var returnV = CommonUtility.CreateIncident(cusEv);
                            //if (returnV != "")
                            //{
                            DriverOffence.UpdateStartDriverOffence(cusEv.Identifier, CommonUtility.getDTNow(), userinfo.Username, dbConnection);
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = id;
                            EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                            EventHistoryEntry.UserName = userinfo.Username;
                            //EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            json = "Successfully started work on ticket";
                            //}
                        }
                        else if (status.ToUpper() == "END")
                        {
                            //cusEv.IncidentStatus = CommonUtility.getIncidentStatusValue("complete");
                            //cusEv.Handled = false;
                            ////incidentinfo.SiteId = userinfo.SiteId;
                            //cusEv.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            //cusEv.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                            //var returnV = CommonUtility.CreateIncident(cusEv);
                            //if (returnV != "")
                            //{
                            DriverOffence.UpdateEndDriverOffence(cusEv.Identifier, CommonUtility.getDTNow(), userinfo.Username, dbConnection);
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = id;
                            EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                            EventHistoryEntry.UserName = userinfo.Username;
                            //EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            json = "Successfully ended work on ticket";
                            var gCus = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);

                            if (gCus != null && gCus.SendEmail)
                            {
                                var projinfo = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                                if (projinfo != null)
                                {
                                    try
                                    {
                                        CommonUtility.sendEmailNotification(projinfo.UserName, projinfo.CustomerFullName, "Ticket", userinfo.FirstName + " " + userinfo.LastName, projinfo.EndDate.AddHours(userinfo.TimeZone).ToString(), projinfo.OffenceCategory, cusEv.CustomerIncidentId, projinfo.StartDate.AddHours(userinfo.TimeZone).ToString(), "End", userinfo);
                                    }
                                    catch (Exception ex)
                                    {
                                        MIMSLog.MIMSLogSave("Ticketing", "UpdateTicketStatus-sendEmailNotification", ex, dbConnection, userinfo.SiteId);
                                    }
                                }
                            }
                            //}
                        }
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "UpdateTicketStatus", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static List<string> handleTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var cusData = CustomEvent.GetCustomEventById(id, dbConnection);
                    cusData.Handled = true;
                    cusData.HandledBy = userinfo.Username;
                    cusData.HandledTime = CommonUtility.getDTNow();
                    cusData.UpdatedBy = userinfo.Username;
                    cusData.IncidentStatus = CommonUtility.getIncidentStatusValue("resolve");
                    CustomEvent.InsertCustomEvent(cusData, dbConnection, dbConnectionAudit, true);
                    CustomEvent.CustomEventHandledById(id, true, userinfo.Username, dbConnection);

                    var EventHistoryEntry = new EventHistory();
                    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                    EventHistoryEntry.CreatedBy = userinfo.Username;
                    EventHistoryEntry.EventId = id;
                    EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                    EventHistoryEntry.UserName = userinfo.Username;
                    //EventHistoryEntry.Remarks = ins;
                    EventHistoryEntry.SiteId = userinfo.SiteId;
                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);

                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Handled Ticket" + id);

                    var alltasks = UserTask.GetAllTaskByIncidentId(cusData.EventId, dbConnection);
                    foreach (var tsk in alltasks)
                    {
                        if (tsk.Status == (int)TaskStatus.Pending || tsk.Status == (int)TaskStatus.OnRoute || tsk.Status == (int)TaskStatus.InProgress || tsk.Status == (int)TaskStatus.Completed)
                        {
                            var tskhistory = new TaskEventHistory();
                            tsk.Status = (int)TaskStatus.Accepted;
                            tskhistory.Action = (int)TaskAction.Accepted;
                            tskhistory.CreatedBy = userinfo.Username;
                            tskhistory.CreatedDate = CommonUtility.getDTNow();
                            tskhistory.TaskId = tsk.Id;
                            tskhistory.SiteId = userinfo.SiteId;
                            tskhistory.CustomerId = userinfo.CustomerInfoId;
                            TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);

                            if (tsk.TaskLinkId == 0)
                            {
                                var gtsklinks = UserTask.GetAllTasksByTaskLinkId(tsk.Id, dbConnection);
                                if (gtsklinks.Count > 0)
                                {
                                    foreach (var links in gtsklinks)
                                    {
                                        if (tsk.Status == (int)TaskStatus.Pending || tsk.Status == (int)TaskStatus.OnRoute || tsk.Status == (int)TaskStatus.InProgress || tsk.Status == (int)TaskStatus.Completed)
                                        {
                                            links.Status = (int)TaskStatus.Accepted;
                                            links.UpdatedDate = CommonUtility.getDTNow();
                                            if (userinfo != null)
                                                links.UpdatedBy = userinfo.Username;
                                            UserTask.InsertorUpdateTask(links, dbConnection, dbConnectionAudit, true);

                                            var ltskhistory = new TaskEventHistory();
                                            ltskhistory.Action = (int)TaskAction.Accepted;
                                            ltskhistory.CreatedBy = userinfo.Username;
                                            ltskhistory.CreatedDate = CommonUtility.getDTNow();
                                            ltskhistory.TaskId = links.Id;
                                            ltskhistory.SiteId = userinfo.SiteId;
                                            ltskhistory.CustomerId = userinfo.CustomerInfoId;
                                            TaskEventHistory.InsertTaskEventHistory(ltskhistory, dbConnection, dbConnectionAudit, true);
                                        }
                                    }
                                }
                            }
                            tsk.UpdatedDate = CommonUtility.getDTNow();
                            if (userinfo != null)
                                tsk.UpdatedBy = userinfo.Username;

                            var tId = UserTask.InsertorUpdateTask(tsk, dbConnection, dbConnectionAudit, true);

                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", tsk.Id.ToString(), tsk.Id.ToString(), userinfo, "Handled Tasks pertaining to ticket-" + id);

                        }
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "handleTicket", ex, dbConnection, userinfo.SiteId);
                    listy.Add(ex.Message);
                }
                listy.Add("SUCCESS");

            }
            return listy;
        }

        [WebMethod]
        public static List<string> getChecklistDataTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);
                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence1))
                        listy.Add(offenceinfo.Offence1);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence2))
                        listy.Add(offenceinfo.Offence2);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence3))
                        listy.Add(offenceinfo.Offence3);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence4))
                        listy.Add(offenceinfo.Offence4);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getChecklistDataTicket", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string deleteAttachmentDataTicket(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.MobileHotEventAttachment.GetMobileHotEventAttachmentById(id, dbConnection);
                    if (ret != null)
                    {
                        ret = ret.Where(i => i.Id == id).ToList();
                        if (ret.Count > 0)
                        {
                            MobileHotEventAttachment.DeleteMobileHotEventAttachmentById(id, dbConnection);

                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "MobileHotEventAttachment", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());

                            CommonUtility.CloudDeleteFile(ret[0].AttachmentPath);

                            listy = "Successfully deleted entry";
                        }
                        else
                            listy = "Failed to delete entry";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Tasks", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string CreatePDFTicket(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var cusevent = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (cusevent != null)
                            return GeneratePDFTicket(cusevent, userinfo);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticket", "CreatePDFTicket", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }

        private static string GeneratePDFTicket(CustomEvent cusevent, Users userinfo)
        {
            string fileName = string.Empty;
            try
            {
                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                var path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };

                doc.Open();
                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                Font rweb = new Font(z_cn, 11, Font.NORMAL, arrowred);

                Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                Font eleventimesI = new Font(f_cn, 11, Font.ITALIC, Color.BLACK);

                Font mbtimes = new Font(h_cn, 18, Font.BOLD, arrowred);
                Font cbtimes = new Font(h_cn, 13, Font.BOLD, arrowred);

                float[] columnWidthsH = new float[] { 30, 60, 10 };

                var mainheadertable = new PdfPTable(3);
                mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainheadertable.WidthPercentage = 100;
                mainheadertable.SetWidths(columnWidthsH);
                var newTRIlist = new List<TimelineReportItems>();

                var headerMain = new PdfPCell();
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    headerMain.AddElement(new Phrase("G4S TICKET REPORT", mbtimes));
                }
                else
                {
                    headerMain.AddElement(new Phrase("MIMS TICKET REPORT", mbtimes));
                }
                headerMain.PaddingBottom = 10;
                headerMain.HorizontalAlignment = 1;
                headerMain.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell1 = new PdfPCell();
                pdfCell1.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell2 = new PdfPCell();
                pdfCell2.Border = Rectangle.NO_BORDER;
                mainheadertable.AddCell(pdfCell1);
                mainheadertable.AddCell(headerMain);
                mainheadertable.AddCell(pdfCell2);

                var taskdataTable = new PdfPTable(2);
                var taskdataTableA = new PdfPTable(2);
                var taskdataTableB = new PdfPTable(2);
                taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTable.WidthPercentage = 100;

                taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                taskdataTableA.WidthPercentage = 100;
                taskdataTableB.WidthPercentage = 100;

                float[] columnWidths2575 = new float[] { 25, 75 };
                float[] columnWidths3070 = new float[] { 30, 70 };
                float[] columnWidths3565 = new float[] { 35, 65 };
                float[] columnWidths4060 = new float[] { 40, 60 };
                taskdataTableA.SetWidths(columnWidths4060);

                taskdataTableB.SetWidths(columnWidths4060);
                var driverOffence = DriverOffence.GetDriverOffenceById(cusevent.Identifier, dbConnection);
                if (driverOffence != null)
                {

                }
                //SIDE A

                if (driverOffence.CusId > 0)
                {
                    var header0 = new PdfPCell();
                    header0.AddElement(new Phrase("Account:", beleventimes));
                    header0.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header0);

                    var header0a = new PdfPCell();
                    header0a.AddElement(new Phrase(driverOffence.CusName, eleventimes));
                    header0a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header0a);
                }
                if (driverOffence.ContractId > 0)
                {
                    var header00 = new PdfPCell();
                    header00.AddElement(new Phrase("Contract:", beleventimes));
                    header00.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header00);

                    var header00a = new PdfPCell();
                    header00a.AddElement(new Phrase(driverOffence.ContractName, eleventimes));
                    header00a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header00a);
                }
                var header1 = new PdfPCell();
                header1.AddElement(new Phrase("Category:", beleventimes));
                header1.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1);

                var header1a = new PdfPCell();
                header1a.AddElement(new Phrase(driverOffence.OffenceCategory, eleventimes));
                header1a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1a);

                var header2 = new PdfPCell();
                header2.AddElement(new Phrase("Type:", beleventimes));
                header2.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2);

                if (string.IsNullOrEmpty(driverOffence.OffenceType))
                    driverOffence.OffenceType = "N/A";

                var header2a = new PdfPCell();
                header2a.AddElement(new Phrase(driverOffence.OffenceType, eleventimes));
                header2a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2a);

                var subNameTable = new PdfPTable(1);
                var header000 = new PdfPCell();
                header000.AddElement(new Phrase("Item:", beleventimes));
                header000.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header000);

                if (!string.IsNullOrEmpty(driverOffence.Offence1))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence1, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence2))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence2, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence3))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence3, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence4))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence4, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                taskdataTableA.AddCell(subNameTable);

                var header3 = new PdfPCell();
                header3.AddElement(new Phrase("Created Date:", beleventimes));
                header3.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3);

                var header3a = new PdfPCell();
                header3a.AddElement(new Phrase(cusevent.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), eleventimes));
                header3a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3a);

                var header4 = new PdfPCell();
                header4.AddElement(new Phrase("Created by:", beleventimes));
                header4.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4);

                var header4a = new PdfPCell();
                header4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                header4a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4a);

                var header8 = new PdfPCell();
                header8.AddElement(new Phrase("Description:", beleventimes));
                header8.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8);

                var header8a = new PdfPCell();
                header8a.AddElement(new Phrase(driverOffence.Comments, eleventimes));
                header8a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8a);

                //SIDE B

                var headerB2 = new PdfPCell();
                headerB2.AddElement(new Phrase("Status:", beleventimes));
                headerB2.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2);
                if (cusevent.Handled)
                {
                    cusevent.StatusName = "Resolved";
                }
                else if (driverOffence.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                {
                    cusevent.StatusName = "Inprogress";
                }
                else if (driverOffence.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                {
                    cusevent.StatusName = "Completed";
                }

                var headerB2a = new PdfPCell();
                headerB2a.AddElement(new Phrase(cusevent.StatusName, eleventimes));
                headerB2a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2a);

                var headerB1 = new PdfPCell();
                headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                headerB1.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB1);


                var siteinfo = Arrowlabs.Business.Layer.Site.GetSiteById(cusevent.SiteId, dbConnection);
                if (siteinfo != null)
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase(siteinfo.Name, eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }
                else
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase("N/A", eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }

                if (cusevent.Handled)
                {
                    var headerB4 = new PdfPCell();
                    headerB4.AddElement(new Phrase("Resolved By:", beleventimes));
                    headerB4.Border = Rectangle.NO_BORDER;
                    headerB4.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB4);

                    var headerB4a = new PdfPCell();
                    headerB4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                    headerB4a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB4a);

                }

                taskdataTable.AddCell(taskdataTableA);
                taskdataTable.AddCell(taskdataTableB);

                var maptextdataTable = new PdfPTable(1);
                maptextdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                maptextdataTable.WidthPercentage = 100;

                var geolocation = ReverseGeocode.RetrieveFormatedAddress(cusevent.Latitude.ToString(), cusevent.Longtitude.ToString(), getClientLic);
                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, cusevent.EventId);
                if (string.IsNullOrEmpty(geolocation))
                {
                    if (geofence.Count > 0)
                    {
                        geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        cusevent.Longtitude = geofence[0].Longitude.ToString();
                        cusevent.Latitude = geofence[0].Latitude.ToString();

                    }
                }

                if (string.IsNullOrEmpty(geolocation))
                    geolocation = "Not Specified";

                var mapheader = new PdfPCell();
                mapheader.AddElement(new Phrase("Location:" + geolocation, beleventimes));
                mapheader.Border = Rectangle.NO_BORDER;
                maptextdataTable.AddCell(mapheader);

                var latComp = string.Empty;
                var longComp = string.Empty;
                var latEng = string.Empty;
                var longEng = string.Empty;
                var arrived = string.Empty;
                var vargeopath = string.Empty; //&path=color:0xff0000ff|weight:5|25.09773,55.16316|25.0978512224338,55.1634863298386
                var tracebackpath = string.Empty; //&path=color:blue|weight:3|25.09773,55.16316|25.0978512224338,55.1634863298386


                var incidenteventhistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    foreach (var item in utask)
                    {
                        var alltaskhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(item.Id, dbConnection);
                        if (alltaskhistory.Count > 0)
                        {
                            foreach (var task in alltaskhistory)
                            {
                                if (task.Action != (int)TaskAction.Update)
                                {
                                    var evhistory = new EventHistory();

                                    evhistory.isTask = true;
                                    evhistory.TaskName = task.TaskName;
                                    if (task.Action == (int)TaskAction.Pending)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                                    else if (task.Action == (int)TaskAction.InProgress)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                                    else if (task.Action == (int)TaskAction.OnRoute)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.OnRoute;
                                    else if (task.Action == (int)TaskAction.Pause)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pause;
                                    else if (task.Action == (int)TaskAction.Resume)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resume;
                                    else if (task.Action == (int)TaskAction.Complete)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                                    else if (task.Action == (int)TaskAction.Accepted)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                                    else if (task.Action == (int)TaskAction.Rejected)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Reject;

                                    evhistory.CustomerFullName = task.CustomerFullName;
                                    evhistory.ACustomerFullName = task.CustomerFullName;
                                    evhistory.CreatedDate = task.CreatedDate;
                                    evhistory.Latitude = task.Latitude;
                                    evhistory.Longtitude = task.Longtitude;
                                    incidenteventhistory.Add(evhistory);
                                }
                                if (item.AssigneeType == (int)TaskAssigneeType.Group)
                                {
                                    if (task.Action == (int)TaskAction.Update)
                                    {
                                        var evhistory = new EventHistory();
                                        evhistory.isTask = true;
                                        evhistory.TaskName = task.TaskName;
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Updated;
                                        evhistory.CustomerFullName = task.CustomerFullName;
                                        evhistory.ACustomerFullName = task.CustomerFullName;
                                        evhistory.CreatedDate = task.CreatedDate;
                                        evhistory.Latitude = task.Latitude;
                                        evhistory.Longtitude = task.Longtitude;
                                        incidenteventhistory.Add(evhistory);
                                    }
                                }
                            }
                        }
                    }
                }
                incidenteventhistory = incidenteventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                foreach (var rem in incidenteventhistory)
                {
                    var timelineItem = new TimelineReportItems();

                    cusevent.Name = driverOffence.OffenceCategory;

                    if (rem.isTask)
                    {
                        if (!string.IsNullOrEmpty(rem.TaskName))
                            cusevent.Name = "task " + rem.TaskName;
                        else
                            cusevent.Name = "Task";

                    }


                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerFullName;
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "started ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.OnRoute)
                    {
                        actioninfo = "onroute ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pause)
                    {
                        actioninfo = "paused ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resume)
                    {
                        actioninfo = "resumed ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";
                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Updated)
                    {
                        actioninfo = "updated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    newTRIlist.Add(timelineItem);
                }




                iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                paragraphTable.SpacingBefore = 120f;
                Paragraph p = new Paragraph();
                p.IndentationLeft = 10;

                p.Add(mainheadertable);
                p.Add(taskdataTable);
                p.Add(maptextdataTable);
                //p.Add(mapdataTable);

                PdfPTable table = new PdfPTable(1);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                //Map style roadMap

                doc.Add(paragraphTable);

                doc.Add(p);

                //Attachments

                var listycanvasnotes = new List<string>();

                var attachments = new List<MobileHotEventAttachment>();


                //if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                //{
                //    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusevent.Identifier, dbConnection);
                //    if (mobEv != null)
                //        attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection));


                //}

                attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusevent.EventId, dbConnection));

                var videoCount = 0;
                var imgCount = 0;
                var pdfCount = 0;

                PdfPTable taskimagetable = new PdfPTable(2);
                taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskimagetable.WidthPercentage = 100;

                var atheader1 = new PdfPCell();
                atheader1.AddElement(new Phrase("Attachments", cbtimes));
                atheader1.Border = Rectangle.NO_BORDER;
                atheader1.PaddingTop = 10;
                atheader1.PaddingBottom = 10;
                atheader1.Colspan = 2;
                taskimagetable.AddCell(atheader1);
                taskimagetable.KeepTogether = true;
                if (attachments.Count > 0)
                {
                    var attachcount = 0;
                    var canvasattachcount = 0;
                    var attachlist = new List<MobileHotEventAttachment>();
                    var videosList = new List<MobileHotEventAttachment>();

                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                            {
                                videoCount++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                            {
                                pdfCount++;
                            }
                            else
                            {
                                attachlist.Add(item);
                                attachcount++;
                                imgCount++;
                                listycanvasnotes.Add("Image " + attachcount);
                            }
                        }
                    }
                    taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                    var countz = 0;
                    foreach (var item in attachlist)
                    {
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                        {

                        }
                        else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                        {

                        }
                        else
                        {
                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.AttachmentPath);

                            if (image.Height > image.Width)
                            {
                                //Maximum height is 800 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Height;
                                image.ScalePercent(percentage * 100);
                            }
                            else
                            {
                                //Maximum width is 600 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Width;
                                image.ScalePercent(percentage * 100);
                            }

                            //image.ScaleAbsolute(120f, 155.25f);
                            image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                            if (!string.IsNullOrEmpty(listycanvasnotes[countz]))
                            {
                                Paragraph pcell = new Paragraph(listycanvasnotes[countz].ToUpper());
                                pcell.Alignment = Element.ALIGN_LEFT;
                                pcell.SpacingAfter = 10;
                                imgCell1.AddElement(pcell);
                            }
                            imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                            imgCell1.AddElement(new Chunk(image, 0, 0));
                            imgCell1.PaddingTop = 10;
                            taskimagetable.AddCell(imgCell1);
                            countz++;
                        }
                    }
                    taskimagetable.CompleteRow();
                }

                var extraattachment = new PdfPTable(3);
                extraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                extraattachment.WidthPercentage = 100;
                var phrase2 = new Phrase();
                phrase2.Add(
                    new Chunk("Files Attached:  ", btimes)
                );
                phrase2.Add(new Chunk(pdfCount.ToString(), times));

                var zheaderA3a = new PdfPCell();
                zheaderA3a.AddElement(phrase2);
                zheaderA3a.Border = Rectangle.NO_BORDER;
                zheaderA3a.PaddingTop = 20;
                zheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(zheaderA3a);

                var phrase32 = new Phrase();
                phrase32.Add(
                    new Chunk("Videos Attached:  ", btimes)
                );
                phrase32.Add(new Chunk(videoCount.ToString(), times));

                var aheaderA3a = new PdfPCell();
                aheaderA3a.AddElement(phrase32);
                aheaderA3a.Border = Rectangle.NO_BORDER;
                aheaderA3a.PaddingTop = 20;
                aheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aheaderA3a);

                var phrase323 = new Phrase();
                phrase323.Add(
                    new Chunk("Images Attached:  ", btimes)
                );
                phrase323.Add(new Chunk(imgCount.ToString(), times));

                var aaheaderA3a = new PdfPCell();
                aaheaderA3a.AddElement(phrase323);
                aaheaderA3a.Border = Rectangle.NO_BORDER;
                aaheaderA3a.PaddingTop = 20;
                aaheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aaheaderA3a);

                //doc.NewPage();

                doc.Add(taskimagetable);

                doc.Add(extraattachment);


                PdfPTable timetable = new PdfPTable(2);
                timetable.WidthPercentage = 100;
                timetable.DefaultCell.Border = Rectangle.NO_BORDER;
                timetable.SetWidths(columnWidths2575);

                if (newTRIlist.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Timeline", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    timetable.AddCell(headeventCell);

                    foreach (var lit in newTRIlist)
                    {
                        var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(eventCell);
                    }
                }
                doc.Add(timetable);

                PdfPTable notestable = new PdfPTable(2);
                notestable.WidthPercentage = 100;
                notestable.DefaultCell.Border = Rectangle.NO_BORDER;
                notestable.SetWidths(columnWidths2575);

                var tremarks = CustomEventRemarks.GetCustomEventRemarksByEventId(cusevent.EventId, dbConnection);

                if (tremarks.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Notes", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    notestable.AddCell(headeventCell);
                    foreach (var remark in tremarks)
                    {
                        var dateCell = new PdfPCell(new Phrase(remark.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString(), redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        notestable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(remark.FName + " " + remark.LName + ":" + remark.Remarks, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        notestable.AddCell(eventCell);
                    }
                }

                doc.Add(notestable);

                //TASKS LOOP

                //var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    int count = 1;

                    foreach (var task in utask)
                    {
                        var tskheadertable = new PdfPTable(2);
                        tskheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskheadertable.WidthPercentage = 100;
                        float[] columnWidths2080 = new float[] { 20, 80 };
                        tskheadertable.SetWidths(columnWidths2080);

                        var tskheader0 = new PdfPCell();
                        //tskheader0.AddElement(new Phrase("Task #:" + count, cbtimes));

                        tskheader0.AddElement(new Phrase("Attached Task", cbtimes));
                        tskheader0.Border = Rectangle.NO_BORDER;
                        tskheader0.Colspan = 2;
                        tskheadertable.AddCell(tskheader0);

                        var tskheader1 = new PdfPCell();
                        tskheader1.AddElement(new Phrase("Task Name:", beleventimes));
                        tskheader1.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1);

                        var tskheader1a = new PdfPCell();
                        tskheader1a.AddElement(new Phrase(task.Name, eleventimes));
                        tskheader1a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1a);

                        var header1z = new PdfPCell();
                        header1z.AddElement(new Phrase("Task Number:", beleventimes));
                        header1z.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1z);

                        var header1az = new PdfPCell();
                        header1az.AddElement(new Phrase(task.NewCustomerTaskId, eleventimes));
                        header1az.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1az);

                        var tskheader2 = new PdfPCell();
                        tskheader2.AddElement(new Phrase("Task Description:", beleventimes));
                        tskheader2.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2);

                        var tskheader2a = new PdfPCell();
                        tskheader2a.AddElement(new Phrase(task.Description, eleventimes));
                        tskheader2a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2a);

                        var chklistName = "None";
                        var newTCLlist = new List<TaskChecklistReportItems>();
                        var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(task.TemplateCheckListId.ToString(), dbConnection);
                        if (parentTasks != null)
                        {
                            chklistName = parentTasks.Name;

                            var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(task.Id, dbConnection);

                            foreach (var item in sessions)
                            {
                                var newTCL = new TaskChecklistReportItems();
                                newTCL.isParent = true;

                                if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, eleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                else
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, beleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                newTCL.Notes = item.TemplateCheckListItemNote;

                                var myattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(item.Id, dbConnection);
                                newTCL.Attachments = myattachments;


                                var stringCheck = string.Empty;
                                var itemNotes = string.Empty;

                                if (item.IsChecked)
                                {
                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("3", gweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;
                                }
                                else
                                {
                                    List myList = new ZapfDingbatsList(54);

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("6", rweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;

                                    //if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                    //    chkCounter++;
                                }

                                newTCLlist.Add(newTCL);

                                if (item.ChildCheckList != null)
                                {
                                    if (item.ChildCheckList.Count > 0)
                                    {
                                        foreach (var child in item.ChildCheckList)
                                        {

                                            var cnewTCL = new TaskChecklistReportItems();
                                            cnewTCL.isParent = false;

                                            var ncaheaderA = new PdfPCell();
                                            ncaheaderA.AddElement(new Phrase(child.Name, eleventimesI));
                                            ncaheaderA.Border = Rectangle.NO_BORDER;
                                            cnewTCL.itemName = ncaheaderA;

                                            cnewTCL.Notes = child.TemplateCheckListItemNote;

                                            var mycattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(child.Id, dbConnection);
                                            cnewTCL.Attachments = mycattachments;

                                            if (item.IsChecked)
                                            {
                                                //gweb rweb
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("3", gweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                            }
                                            else
                                            {
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("6", rweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                                //chkCounter++;
                                            }

                                            newTCLlist.Add(cnewTCL);
                                        }
                                    }
                                }
                            }
                        }
                        var tskheader3 = new PdfPCell();
                        tskheader3.AddElement(new Phrase("Checklist Name:", beleventimes));
                        tskheader3.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3);

                        var tskheader3a = new PdfPCell();
                        tskheader3a.AddElement(new Phrase(chklistName, eleventimes));
                        tskheader3a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3a);

                        doc.Add(tskheadertable);

                        var newchecklistTable = new PdfPTable(4);
                        newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        newchecklistTable.WidthPercentage = 100;
                        float[] columnWidthsCHK = new float[] { 30, 10, 30, 30 };
                        newchecklistTable.SetWidths(columnWidthsCHK);

                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("Checklist", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.PaddingTop = 10;
                        cheader1.PaddingBottom = 10;
                        cheader1.Colspan = 4;
                        newchecklistTable.AddCell(cheader1);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Item Name", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.PaddingLeft = 5;
                        ncaheader1.PaddingBottom = 5;
                        ncaheader1.BackgroundColor = Color.DARK_GRAY;

                        newchecklistTable.AddCell(ncaheader1);

                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Status", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Notes", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("Attachments", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader4);

                        foreach (var item in newTCLlist)
                        {
                            newchecklistTable.AddCell(item.itemName);

                            newchecklistTable.AddCell(item.Status);

                            var ncaheaderAB = new PdfPCell();
                            ncaheaderAB.AddElement(new Phrase(item.Notes, eleventimes));
                            ncaheaderAB.Border = Rectangle.NO_BORDER;
                            newchecklistTable.AddCell(ncaheaderAB);

                            if (item.Attachments.Count > 0)
                            {
                                PdfPTable imgTable = new PdfPTable(1);
                                imgTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                foreach (var attach in item.Attachments)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant()))
                                    {
                                        imgTable.AddCell(new Phrase("Video Attached", eleventimes));
                                    }
                                    else
                                    {
                                        //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + task.Id + "/{0}", System.IO.Path.GetFileName(attach.DocumentPath));

                                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(attach.DocumentPath);

                                        if (image.Height > image.Width)
                                        {
                                            //Maximum height is 800 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Height;
                                            image.ScalePercent(percentage * 100);
                                        }
                                        else
                                        {
                                            //Maximum width is 600 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Width;
                                            image.ScalePercent(percentage * 100);
                                        }

                                        //image.ScaleAbsolute(120f, 155.25f);
                                        image.Border = Rectangle.NO_BORDER;
                                        iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1l.Border = Rectangle.NO_BORDER;
                                        imgCell1l.AddElement(new Chunk(image, 0, 0));
                                        imgTable.AddCell(imgCell1l);
                                        if (!string.IsNullOrEmpty(attach.ImageNote) && attach.ImageNote != "Task Attachment")
                                            imgTable.AddCell(new Phrase("Img Note:" + attach.ImageNote, eleventimes));
                                    }
                                }
                                newchecklistTable.AddCell(imgTable);
                            }
                            else
                            {
                                newchecklistTable.AddCell(new Phrase(" ", eleventimes));
                            }
                        }

                        doc.Add(newchecklistTable);

                        var tsklistycanvasnotes = new List<string>();
                        var tskattachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(task.Id, dbConnection);
                        var tskvideoCount = 0;
                        var tskimgCount = 0;
                        var tskpdfCount = 0;

                        PdfPTable tsktaskimagetable = new PdfPTable(2);
                        tsktaskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktaskimagetable.WidthPercentage = 100;

                        var tskatheader1 = new PdfPCell();
                        tskatheader1.AddElement(new Phrase("Task Attachments", cbtimes));
                        tskatheader1.Border = Rectangle.NO_BORDER;
                        tskatheader1.PaddingTop = 10;
                        tskatheader1.PaddingBottom = 10;
                        tskatheader1.Colspan = 2;
                        tsktaskimagetable.AddCell(tskatheader1);

                        if (tskattachments.Count > 0)
                        {
                            var attachcount = 0;
                            var canvasattachcount = 0;
                            var attachlist = new List<UserTaskAttachment>();

                            foreach (var item in tskattachments)
                            {
                                if (!string.IsNullOrEmpty(item.DocumentPath) && item.ChecklistId == 0)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        tskpdfCount++;
                                    }
                                    else
                                    {
                                        attachlist.Add(item);
                                        attachcount++;
                                        tskimgCount++;
                                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                                        {
                                            tsklistycanvasnotes.Add("Canvas Notes Image " + attachcount + ":" + item.ImageNote);
                                            canvasattachcount++;
                                        }
                                        else
                                        {
                                            tsklistycanvasnotes.Add("Image " + attachcount);
                                            canvasattachcount++;
                                        }
                                    }
                                }
                            }

                            var countz = 0;
                            foreach (var item in attachlist)
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {

                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.DocumentPath);

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    if (!string.IsNullOrEmpty(tsklistycanvasnotes[countz]))
                                    {
                                        Paragraph pcell = new Paragraph(tsklistycanvasnotes[countz].ToUpper());
                                        pcell.Alignment = Element.ALIGN_LEFT;
                                        pcell.SpacingAfter = 10;
                                        imgCell1.AddElement(pcell);
                                    }
                                    imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                    imgCell1.AddElement(new Chunk(image, 0, 0));
                                    imgCell1.PaddingTop = 10;
                                    tsktaskimagetable.AddCell(imgCell1);
                                    countz++;
                                }
                            }
                            tsktaskimagetable.CompleteRow();
                        }

                        var tskextraattachment = new PdfPTable(3);
                        tskextraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskextraattachment.WidthPercentage = 100;
                        var tskphrase2 = new Phrase();
                        tskphrase2.Add(
                            new Chunk("PDF Attached:  ", btimes)
                        );
                        tskphrase2.Add(new Chunk(tskpdfCount.ToString(), times));

                        var tskzheaderA3a = new PdfPCell();
                        tskzheaderA3a.AddElement(tskphrase2);
                        tskzheaderA3a.Border = Rectangle.NO_BORDER;
                        tskzheaderA3a.PaddingTop = 20;
                        tskzheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskzheaderA3a);

                        var tskphrase32 = new Phrase();
                        tskphrase32.Add(
                            new Chunk("Videos Attached:  ", btimes)
                        );
                        tskphrase32.Add(new Chunk(tskvideoCount.ToString(), times));

                        var tskaheaderA3a = new PdfPCell();
                        tskaheaderA3a.AddElement(tskphrase32);
                        tskaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaheaderA3a.PaddingTop = 20;
                        tskaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaheaderA3a);

                        var tskphrase323 = new Phrase();
                        tskphrase323.Add(
                            new Chunk("Images Attached:  ", btimes)
                        );
                        tskphrase323.Add(new Chunk(tskimgCount.ToString(), times));

                        var tskaaheaderA3a = new PdfPCell();
                        tskaaheaderA3a.AddElement(tskphrase323);
                        tskaaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaaheaderA3a.PaddingTop = 20;
                        tskaaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaaheaderA3a);

                        doc.Add(tsktaskimagetable);

                        doc.Add(tskextraattachment);

                        var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(task.Id, dbConnection);

                        taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();

                        var tsknewTRIlist = new List<TimelineReportItems>();

                        foreach (var taskEv in taskeventhistory)
                        {
                            var timelineItem = new TimelineReportItems();
                            var taskn = "Task";
                            if (taskEv.isSubTask)
                            {
                                taskn = "Sub-task";
                            }

                            if (taskEv.Action == (int)TaskAction.Complete)
                            {
                                completedBy = taskEv.CustomerFullName;
                                var compLocation = string.Empty;
                                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(task.EndLatitude.ToString(), task.EndLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc))
                                    compLocation = " at " + geoLoc;
                                var compInfo = "completed";
                                latComp = task.EndLatitude.ToString();
                                longComp = task.EndLongitude.ToString();
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                                var compstring = completedBy + " " + compInfo + " " + taskn + " " + compLocation;

                                timelineItem.TimelineActivity = compstring;
                                //listy.Add(compstring);
                            }
                            else if (taskEv.Action == (int)TaskAction.OnRoute)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "onroute ";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Pause)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "paused ";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Resume)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "resumed ";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.InProgress)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "started";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Accepted)
                            {
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                acceptedData = taskEv.CustomerFullName + " accepted " + taskn;
                                //listy.Add(acceptedData);
                                timelineItem.TimelineActivity = acceptedData;
                            }
                            else if (taskEv.Action == (int)TaskAction.Rejected)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                rejectedData = taskEv.CustomerFullName + " rejected Task " + taskn;

                                timelineItem.TimelineActivity = rejectedData;
                                //listy.Add(rejectedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Assigned)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                assignedData = taskEv.CustomerFullName + " assigned " + taskn + " to " + taskEv.ACustomerFullName;

                                timelineItem.TimelineActivity = assignedData;

                                //listy.Add(assignedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Pending)
                            {
                                var incidentLocation = string.Empty;
                                var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(task.Latitude.ToString(), task.Longitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc3))
                                    incidentLocation = " at " + geoLoc3;
                                var actioninfo = "created";
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                actioninfo = task.CustomerFullName + " " + actioninfo + " " + taskn + " for " + taskEv.ACustomerFullName + " " + incidentLocation;
                                timelineItem.TimelineActivity = actioninfo;
                                //listy.Add(actioninfo);
                            }
                            tsknewTRIlist.Add(timelineItem);
                        }

                        PdfPTable tsktimetable = new PdfPTable(2);
                        tsktimetable.WidthPercentage = 100;
                        tsktimetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktimetable.SetWidths(columnWidths2575);

                        if (tsknewTRIlist.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Task Timeline", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            tsktimetable.AddCell(headeventCell);

                            foreach (var lit in tsknewTRIlist)
                            {
                                var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(eventCell);
                            }
                        }

                        PdfPTable tasknotestable = new PdfPTable(2);
                        tasknotestable.WidthPercentage = 100;
                        tasknotestable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tasknotestable.SetWidths(columnWidths2575);

                        var taskremarks = TaskRemarks.GetTaskRemarksByTaskId(task.Id, dbConnection);

                        if (taskremarks.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Notes", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            tasknotestable.AddCell(headeventCell);
                            foreach (var remark in taskremarks)
                            {
                                var dateCell = new PdfPCell(new Phrase(remark.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString(), redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                tasknotestable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(remark.FName + " " + remark.LName + ":" + remark.Remarks, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                tasknotestable.AddCell(eventCell);
                            }
                        }
                        doc.Add(tsktimetable);
                        doc.Add(tasknotestable);
                    }
                }
                //END OF TASKS



                Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                var endTable = new PdfPTable(1);
                endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                endTable.WidthPercentage = 100;

                var endheader = new PdfPCell();
                endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                endheader.Border = Rectangle.NO_BORDER;
                endTable.AddCell(endheader);
                doc.Add(endTable);

                writer.Flush();
                doc.Close();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                        return vpath;
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Ticketing", "GeneratePDFTicket", exp, dbConnection, userinfo.SiteId);
                return "Failed to generate report!";
            }
            return "Failed to generate report!";
        }
    }

    public class TaskChecklistReportItems
    {
        public iTextSharp.text.pdf.PdfPCell itemName { get; set; }
        public iTextSharp.text.pdf.PdfPCell Status { get; set; }
        public string Notes { get; set; }
        public List<UserTaskAttachment> Attachments { get; set; }

        public bool isParent { get; set; }
    }

    public class TimelineReportItems
    {
        public string DatetimeN { get; set; }
        public string TimelineActivity { get; set; }
    }
}
