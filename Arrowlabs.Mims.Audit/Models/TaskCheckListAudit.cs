﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class TaskCheckListAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int TemplateCheckListItemId { get; set; }
        public string TemplateCheckListItemNote { get; set; }
        public bool IsChecked { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        // Mapping fields
        public string Name { get; set; }
        public int Quantity { get; set; }
        public int EntryQuantity { get; set; }
        public int ExitQuantity { get; set; }
        public int CheckListItemType { get; set; }
        public int ParentCheckListId { get; set; }
        public int ChildCheckListId { get; set; }
        public int SiteId { get;set;}

                public int CustomerId { get;set;}

                       
        public List<TaskCheckListAudit> ChildCheckList { get; set; }

        private static TaskCheckListAudit GetTaskCheckListAuditItemFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var taskCheckListItem = new TaskCheckListAudit();
            taskCheckListItem.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            
            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                taskCheckListItem.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("TaskId") && reader["TaskId"] != DBNull.Value)
                taskCheckListItem.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
            if (columns.Contains("TemplateCheckListItemId") && reader["TemplateCheckListItemId"] != DBNull.Value)
                taskCheckListItem.TemplateCheckListItemId = Convert.ToInt32(reader["TemplateCheckListItemId"].ToString());
            if (columns.Contains("ParentCheckListId") && reader["ParentCheckListId"] != DBNull.Value)
                taskCheckListItem.ParentCheckListId = Convert.ToInt32(reader["ParentCheckListId"].ToString());
            if (columns.Contains("ChildCheckListId") && reader["ChildCheckListId"] != DBNull.Value)
                taskCheckListItem.ChildCheckListId = (String.IsNullOrEmpty(reader["ChildCheckListId"].ToString())) ? 0 : Convert.ToInt32(reader["ChildCheckListId"].ToString());
            if (columns.Contains("TemplateCheckListItemNote") && reader["TemplateCheckListItemNote"] != DBNull.Value)
                taskCheckListItem.TemplateCheckListItemNote = reader["TemplateCheckListItemNote"].ToString();
            if (columns.Contains("IsChecked") && reader["IsChecked"] != DBNull.Value)
                taskCheckListItem.IsChecked = Convert.ToBoolean(reader["IsChecked"].ToString().ToLower());
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                taskCheckListItem.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                taskCheckListItem.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                taskCheckListItem.Name = reader["Name"].ToString();
            if (columns.Contains("Quantity") && reader["Quantity"] != DBNull.Value)
                taskCheckListItem.Quantity = Convert.ToInt32(reader["Quantity"].ToString());
            if (columns.Contains("EntryQuantity") && reader["EntryQuantity"] != DBNull.Value)
                taskCheckListItem.EntryQuantity = Convert.ToInt32(reader["EntryQuantity"].ToString());
            if (columns.Contains("ExitQuantity") && reader["ExitQuantity"] != DBNull.Value)
                taskCheckListItem.ExitQuantity = Convert.ToInt32(reader["ExitQuantity"].ToString());
            if (columns.Contains("CheckListItemType") && reader["CheckListItemType"] != DBNull.Value)
                taskCheckListItem.CheckListItemType = Convert.ToInt32(reader["CheckListItemType"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                taskCheckListItem.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return taskCheckListItem;
        }
        public static List<TaskCheckListAudit> GetAllTaskCheckListAudit(string dbConnection)
        {
            var taskCheckListItems = new List<TaskCheckListAudit>();
            var parentCheckListItems = new List<TaskCheckListAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTaskCheckListAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var taskCheckListItem = GetTaskCheckListAuditItemFromSqlReader(reader);
                    taskCheckListItems.Add(taskCheckListItem);
                }

                if (taskCheckListItems.Count > 0)
                {
                    foreach (var taskCheckListItem in taskCheckListItems)
                    {
                        if (taskCheckListItem.ChildCheckListId == 0)
                        {
                            taskCheckListItem.ChildCheckList = new List<TaskCheckListAudit>();

                            foreach (var taskCheckLstItem in taskCheckListItems)
                            {
                                if (taskCheckLstItem.ChildCheckListId == taskCheckListItem.TemplateCheckListItemId)
                                {
                                    taskCheckListItem.ChildCheckList.Add(taskCheckLstItem);
                                }
                            }
                            parentCheckListItems.Add(taskCheckListItem);
                        }
                    }
                }


                connection.Close();
                reader.Close();
            }
            return parentCheckListItems;
        }
        public static bool InsertTaskCheckListItem(TaskCheckListAudit taskCheckListItem, string dbConnection)
        {
            if (taskCheckListItem != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskCheckListAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(taskCheckListItem.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = taskCheckListItem.Id;

                    cmd.Parameters.Add("@TaskId", SqlDbType.Int).Value = taskCheckListItem.TaskId;

                    cmd.Parameters.Add("@TemplateCheckListItemId", SqlDbType.Int).Value = taskCheckListItem.TemplateCheckListItemId;

                    cmd.Parameters.Add("@TemplateCheckListItemNote", SqlDbType.NVarChar).Value = taskCheckListItem.TemplateCheckListItemNote;

                    cmd.Parameters.Add("@IsChecked", SqlDbType.Bit).Value = taskCheckListItem.IsChecked;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = taskCheckListItem.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = taskCheckListItem.CreatedBy;

                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = taskCheckListItem.SiteId;

                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = taskCheckListItem.CustomerId;
                     
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
