﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{

    [Serializable]
  [DataContract]
 public class HotEvent
    {
        public int Id { get; set; }

      [DataMember]
        public Guid Identifier { get; set; }
      public string UpdatedBy { get; set; }
     [DataMember]
      public string Name { get; set; }
     [DataMember]
      public string Detail1 { get; set; }
     [DataMember]
      public string Detail2 { get; set; }
     [DataMember]
      public string Detail3 { get; set; }
     [DataMember]
      public string Detail4{ get; set; }
     [DataMember]
     public byte[] ImagePath { get; set; }
     [DataMember]
     public int EventType { get; set; }
     [DataMember]
     public string Comments { get; set; }
     [DataMember]
      public int CamType { get; set; }
     [DataMember]
      public string CreatedBy { get; set; }
     [DataMember]
     public DateTime? CreatedDate { get; set; }
     [DataMember]
     public bool Status { get; set; }
        [DataMember]
     public string AccountName { get; set; }
        [DataMember]
        public string playbackURL { get; set; }
        [DataMember]
        public bool isActive { get; set; }

        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string SessionId { get; set; }
        [DataMember]
        public string Password { get; set; }
     

  //  public static List<string> HotEventType = new List<string>() { "None", "Traffic", "Accident", "Damage", "Theft", "Vandalism" };

    

     public static List<HotEvent> GetAllHotEvents(string dbConnection)
     {
         var hotEvents = new List<HotEvent>();
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var sqlCommand = new SqlCommand();
             sqlCommand.Connection = connection;
             sqlCommand.CommandText = "GetAllHotEventsByDate";
             sqlCommand.CommandType = CommandType.StoredProcedure;

             var reader = sqlCommand.ExecuteReader();
             var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
             while (reader.Read())
             {
                 var hotEvent = new HotEvent();

                 hotEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                 hotEvent.Name = reader["Name"].ToString();
                 hotEvent.CreatedBy = reader["CreatedBy"].ToString();
                 hotEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                 hotEvent.CamType = Convert.ToInt32(reader["CamType"].ToString());
                 hotEvent.Detail1 = reader["Detail1"].ToString();
                 hotEvent.Detail2 = reader["Detail2"].ToString();
                 hotEvent.Detail3 = reader["Detail3"].ToString();
                 hotEvent.Detail4 = reader["Detail4"].ToString();
                 hotEvent.Status = Convert.ToBoolean(reader["Status"].ToString());
                 hotEvent.isActive = Convert.ToBoolean(reader["Active"].ToString());
                 hotEvent.playbackURL= reader["PlaybackUrl"].ToString();
                 if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                     hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

                 hotEvents.Add(hotEvent);
             }
             connection.Close();
             reader.Close();
         }
         return hotEvents;
     }

     //public static bool IsDomainAdmin(this WindowsIdentity identity)
     //{
     //    Domain d = Domain.GetDomain(new
     //        DirectoryContext(DirectoryContextType.Domain, getDomainName()));
     //    using (DirectoryEntry de = d.GetDirectoryEntry())
     //    {
     //        byte[] domainSIdArray = (byte[])de.Properties["objectSid"].Value;
     //        SecurityIdentifier domainSId = new SecurityIdentifier(domainSIdArray, 0);
     //        SecurityIdentifier domainAdminsSId = new SecurityIdentifier(
     //        WellKnownSidType.AccountDomainAdminsSid, domainSId);
     //        WindowsPrincipal wp = new WindowsPrincipal(identity);
     //        return wp.IsInRole(domainAdminsSId);
     //    }
     //}

     //private static string getDomainName()
     //{
     //    return IPGlobalProperties.GetIPGlobalProperties().DomainName;
     //}
      //if (WindowsIdentity.GetCurrent().IsDomainAdmin())
      //  {
      //      //Some actions…
      //  }

     public static DateTime DateFormater(string dateTime)
     {
         try
         {
             CultureInfo provider = CultureInfo.InvariantCulture;
             var format = "MM/dd/yyyy hh:mm:sstt";

             DateTime dt2 = DateTime.Parse(dateTime, provider, System.Globalization.DateTimeStyles.AssumeLocal);
             return DateTime.ParseExact(dateTime, format, provider);

         }
         catch (Exception ex)
         {
             return DateTime.Now;
         }
     }

     public static List<HotEvent> GetAllHotEventByStatus(string dbConnection)
     {
         var hotEvents = new List<HotEvent>();
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var sqlCommand = new SqlCommand();
             sqlCommand.Connection = connection;
             sqlCommand.CommandText = "GetAllHotEventsByStatus";
             sqlCommand.CommandType = CommandType.StoredProcedure;

             var reader = sqlCommand.ExecuteReader();
             var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
             while (reader.Read())
             {
                 var hotEvent = new HotEvent();

                 hotEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                 hotEvent.Name = reader["Name"].ToString();
                 hotEvent.CreatedBy = reader["CreatedBy"].ToString();
                 hotEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                 hotEvent.CamType = Convert.ToInt32(reader["CamType"].ToString());
                 hotEvent.Detail1 = reader["Detail1"].ToString();
                 hotEvent.Detail2 = reader["Detail2"].ToString();
                 hotEvent.Detail3 = reader["Detail3"].ToString();
                 hotEvent.Detail4 = reader["Detail4"].ToString();
                 if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                     hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                 try
                 {
                     hotEvent.Status = Convert.ToBoolean(reader["Status"].ToString());
                     hotEvent.isActive = Convert.ToBoolean(reader["Active"].ToString());
                     hotEvent.playbackURL = reader["PlaybackUrl"].ToString();
                 }
                 catch { }
                 hotEvents.Add(hotEvent);
             }
             connection.Close();
             reader.Close();
         }
         return hotEvents;
     }

     public static bool InsertHotEvent(HotEvent hotEvent, string dbConnection, 
			string auditDbConnection = "", bool isAuditEnabled = true)
     {
         if (hotEvent != null)
         {
             var id = 0;
             var action = Arrowlabs.Mims.Audit.Enums.Action.Create; 

             using (var connection = new SqlConnection(dbConnection))
             {
                 connection.Open();
                 var cmd = new SqlCommand("InsertHotEvent", connection);

                 cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                 cmd.Parameters["@Identifier"].Value = hotEvent.Identifier;

                 cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                 cmd.Parameters["@Name"].Value = hotEvent.Name;

                 cmd.Parameters.Add(new SqlParameter("@Detail1", SqlDbType.Xml));
                 cmd.Parameters["@Detail1"].Value = hotEvent.Detail1;

                 cmd.Parameters.Add(new SqlParameter("@Detail2", SqlDbType.Xml));
                 cmd.Parameters["@Detail2"].Value = hotEvent.Detail2;

                 cmd.Parameters.Add(new SqlParameter("@Detail3", SqlDbType.Xml));
                 cmd.Parameters["@Detail3"].Value = hotEvent.Detail3;

                 cmd.Parameters.Add(new SqlParameter("@Detail4", SqlDbType.Xml));
                 cmd.Parameters["@Detail4"].Value = hotEvent.Detail4;

                 cmd.Parameters.Add(new SqlParameter("@CamType", SqlDbType.Int));
                 cmd.Parameters["@CamType"].Value = hotEvent.CamType;

                 cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                 cmd.Parameters["@CreatedBy"].Value = hotEvent.CreatedBy;

                 cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                 cmd.Parameters["@CreatedDate"].Value = hotEvent.CreatedDate;

                 cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Bit));
                 cmd.Parameters["@Status"].Value = hotEvent.Status;

                 cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                 cmd.Parameters["@SiteId"].Value = hotEvent.SiteId; 
                 
                 SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                 returnParameter.Direction = ParameterDirection.ReturnValue;


                 cmd.CommandType = CommandType.StoredProcedure;

                 cmd.CommandText = "InsertHotEvent";

                 cmd.ExecuteNonQuery();

                 if (id == 0)
                     id = (int)returnParameter.Value;

                 try
                 {
                     if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                     {
                         hotEvent.Id = id;
                         var audit = new HotEventAudit();
                         audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, hotEvent.UpdatedBy);
                         Reflection.CopyProperties(hotEvent, audit);
                         HotEventAudit.InsertHotEventAudit(audit, auditDbConnection);
                     }
                 }
                 catch (Exception ex)
                 {
                     MIMSLog.MIMSLogSave("Business Layer", "InsertHotEvent", ex, dbConnection);
                 }
             }
         }
         return true;
     }

     public static bool UpdateHotEvent(Guid identifier, bool Active, string playbackUrl, string dbConnection)
     {          
         
             using (var connection = new SqlConnection(dbConnection))
             {
                 connection.Open();
                 var cmd = new SqlCommand("UpdateHotEvent", connection);

                 cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                 cmd.Parameters["@Identifier"].Value = identifier;

                 cmd.Parameters.Add(new SqlParameter("@PlaybackUrl", SqlDbType.NVarChar));
                 cmd.Parameters["@PlaybackUrl"].Value = playbackUrl;

                 cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit));
                 cmd.Parameters["@Active"].Value = Active;

                 cmd.CommandType = CommandType.StoredProcedure;

                 cmd.CommandText = "UpdateHotEvent";

                 cmd.ExecuteNonQuery();
             }
         return true;
     }


     public static List<HotEvent> GetUserNamesHotEvents(string dbConnection)
     {
         var hotEvents = new List<HotEvent>();
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var sqlCommand = new SqlCommand();
             sqlCommand.Connection = connection;
             sqlCommand.CommandText = "GetUserNamesHotEvents";
             sqlCommand.CommandType = CommandType.StoredProcedure;

             var reader = sqlCommand.ExecuteReader();
             while (reader.Read())
             {
                 var hotEvent = new HotEvent();
                 hotEvent.CreatedBy = reader["CreatedBy"].ToString();
                 hotEvents.Add(hotEvent);
             }
             connection.Close();
             reader.Close();
         }
         return hotEvents;
     }

     public static List<HotEvent> GetUserNamesHotEventsByDate(DateTime startDate, DateTime fromDate, string dbConnection)
     {
         var hotEvents = new List<HotEvent>();

         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var sqlCommand = new SqlCommand("GetUserNamesHotEventsByDate", connection);
             sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
             sqlCommand.Parameters["@toDateTime"].Value = startDate;

             sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
             sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

             sqlCommand.CommandType = CommandType.StoredProcedure;
             sqlCommand.CommandText = "GetUserNamesHotEventsByDate";

             var reader = sqlCommand.ExecuteReader();
             while (reader.Read())
             {
                 var hotEvent = new HotEvent();
                 hotEvent.CreatedBy = reader["CreatedBy"].ToString();
                 hotEvents.Add(hotEvent);
             }
             connection.Close();
             reader.Close();
         }
         return hotEvents;
     }

     public static HotEvent GetHotEventById(Guid identifier, string dbConnection)
     {
         HotEvent hotEvent = null;
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var sqlCommand = new SqlCommand();
             sqlCommand.Connection = connection;
             sqlCommand.CommandText = "GetHotEventById";
             sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.UniqueIdentifier));
             sqlCommand.Parameters["@Id"].Value = identifier;
             sqlCommand.CommandType = CommandType.StoredProcedure;

             var reader = sqlCommand.ExecuteReader();
             var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
             while (reader.Read())
             {
                 hotEvent = new HotEvent();

                 hotEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                 hotEvent.Name = reader["Name"].ToString();
                 hotEvent.CreatedBy = reader["CreatedBy"].ToString();
                 hotEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                 hotEvent.CamType = Convert.ToInt32(reader["CamType"].ToString());
                 hotEvent.Detail1 = reader["Detail1"].ToString();
                 hotEvent.Detail2 = reader["Detail2"].ToString();
                 hotEvent.Detail3 = reader["Detail3"].ToString();
                 hotEvent.Detail4 = reader["Detail4"].ToString();
                 if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                     hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                 try
                 {
                     hotEvent.Status = Convert.ToBoolean(reader["Status"].ToString());
                     hotEvent.isActive = Convert.ToBoolean(reader["Active"].ToString());
                     hotEvent.playbackURL = reader["PlaybackUrl"].ToString();
                 }
                 catch { }
                
             }
             connection.Close();
             reader.Close();
         }

         return hotEvent;
     }

     public static HotEvent ServerGetHotEventById(Guid identifier, string dbConnection)
     {
         HotEvent hotEvent = null;
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var sqlCommand = new SqlCommand();
             sqlCommand.Connection = connection;
             sqlCommand.CommandText = "GetHotEventById";
             sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.UniqueIdentifier));
             sqlCommand.Parameters["@Id"].Value = identifier;
             sqlCommand.CommandType = CommandType.StoredProcedure;

             var reader = sqlCommand.ExecuteReader();
             var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
             while (reader.Read())
             {
                 hotEvent = new HotEvent();

                 hotEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                 hotEvent.Name = reader["Name"].ToString();
                 hotEvent.CreatedBy = reader["CreatedBy"].ToString();
                 hotEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                 hotEvent.CamType = Convert.ToInt32(reader["CamType"].ToString());
                 hotEvent.Detail1 = reader["Detail1"].ToString();
                 hotEvent.Detail2 = reader["Detail2"].ToString();
                 hotEvent.Detail3 = reader["Detail3"].ToString();
                 hotEvent.Detail4 = reader["Detail4"].ToString();
                 hotEvent.Status = Convert.ToBoolean(reader["Status"].ToString());
                 hotEvent.isActive = Convert.ToBoolean(reader["Active"].ToString());
                 hotEvent.playbackURL = reader["PlaybackUrl"].ToString();
                 if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                     hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());


             }
             connection.Close();
             reader.Close();
         }

         return hotEvent;
     }

     public static List<HotEvent> GetAllHotEventByDate(DateTime startDate, DateTime fromDate, string dbConnection)
     {
         var hotEvents = new List<HotEvent>();

         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var sqlCommand = new SqlCommand("GetAllHotEventsReportByDate", connection);
             sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
             sqlCommand.Parameters["@toDateTime"].Value = startDate;

             sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
             sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

             sqlCommand.CommandType = CommandType.StoredProcedure;
             sqlCommand.CommandText = "GetAllHotEventsReportByDate";

             var reader = sqlCommand.ExecuteReader();
             var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList(); 
             while (reader.Read())
             {
                 var hotEvent = new HotEvent();

                 hotEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                 hotEvent.Name = reader["Name"].ToString();
                 hotEvent.CreatedBy = reader["CreatedBy"].ToString();
                 hotEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                 hotEvent.CamType = Convert.ToInt32(reader["CamType"].ToString());
                 hotEvent.Detail1 = reader["Detail1"].ToString();
                 hotEvent.Detail2 = reader["Detail2"].ToString();
                 hotEvent.Detail3 = reader["Detail3"].ToString();
                 hotEvent.Detail4 = reader["Detail4"].ToString();
                 hotEvent.Status = Convert.ToBoolean(reader["Status"].ToString());
                 hotEvent.isActive = Convert.ToBoolean(reader["Active"].ToString());
                 hotEvent.playbackURL = reader["PlaybackUrl"].ToString();
                 if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                     hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

                 hotEvents.Add(hotEvent);
             }
             connection.Close();
             reader.Close();
         }
         return hotEvents;
     }

     public static IList<TamplateHotEvent> GetAllTamplateHotEvent(DateTime frmDateTime, DateTime toDateTime, string dbConnection)
     {
         try
         {
             var hotEventList = new List<TamplateHotEvent>();

             using (var connection = new SqlConnection(dbConnection))
             {
                 connection.Open();

                 var sqlCommand = new SqlCommand("GetAllTamplateHotEvents", connection);
                 sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.NVarChar));
                 sqlCommand.Parameters["@frmDateTime"].Value = frmDateTime;
                 sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.NVarChar));
                 sqlCommand.Parameters["@toDateTime"].Value = toDateTime;
                 sqlCommand.CommandType = CommandType.StoredProcedure;
                 sqlCommand.CommandText = "GetAllTamplateHotEvents";

                 var reader = sqlCommand.ExecuteReader();
                 var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                 while (reader.Read())
                 {
                     var hotEvent = new TamplateHotEvent();
                     XmlDocument doc = new XmlDocument();
                     if (reader["Name"].ToString().Contains("_A") || reader["Name"].ToString().Contains("_I"))
                     {
                         doc.LoadXml(reader["Detail1"].ToString());

                         XmlNodeList nodes = doc.SelectNodes("/HotEvents/HotEvent");
                         int i = nodes.Count;
                         foreach (XmlNode n in nodes)
                         {
                             hotEvent.StartDate = DateFormater(n["StartDate"].InnerText);
                             hotEvent.EndDate = DateFormater(n["EndDate"].InnerText);
                             if (!String.IsNullOrEmpty(n["Longtitude"].InnerText))
                                 hotEvent.Longtitude = Convert.ToDouble(n["Longtitude"].InnerText);
                             if (!String.IsNullOrEmpty(n["Latitude"].InnerText))
                                 hotEvent.Longtitude = Convert.ToDouble(n["Latitude"].InnerText);
                             hotEvent.CameraName = n["CameraName"].InnerText;
                             if (n["PcName"] != null)
                                 hotEvent.PcName = n["PcName"].InnerText;
                             else
                                 hotEvent.PcName = "N/A";
                         }
                         hotEvent.EventType = 0;
                     }
                     else
                     {
                         var detail = String.Empty;
                         if (!String.IsNullOrWhiteSpace(reader["Detail1"].ToString()))
                             detail = reader["Detail1"].ToString();
                         else if (!String.IsNullOrWhiteSpace(reader["Detail2"].ToString()))
                             detail = reader["Detail2"].ToString();
                         else if (!String.IsNullOrWhiteSpace(reader["Detail3"].ToString()))
                             detail = reader["Detail3"].ToString();
                         else if (!String.IsNullOrWhiteSpace(reader["Detail4"].ToString()))
                             detail = reader["Detail4"].ToString();

                         hotEvent = ReceiveMileHotEvent(detail);
                         hotEvent.EventType = 1;

                     }
                     hotEvent.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                     hotEvent.UserName = reader["CreatedBy"].ToString();
                     hotEvent.ReceiveDate = DateFormater(reader["ReceiveDate"].ToString());
                     hotEvent.CreatedDate = DateFormater(reader["CreatedDate"].ToString());
               
                     if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                         hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                     hotEventList.Add(hotEvent);
                 }
                 connection.Close();
                 reader.Close();
                 return hotEventList;
             }
         }
         catch (Exception ex) { }

         return null;
     }

     private static TamplateHotEvent ReceiveMileHotEvent(string detail)
     {
         var hotEvent = new TamplateHotEvent();

         XmlDocument doc = new XmlDocument();
         doc.LoadXml(detail);

         XmlNodeList nodes = doc.SelectNodes("/MilestoneHotClass");
         int i = nodes.Count;
         foreach (XmlNode n in nodes)
         {
             hotEvent.StartDate = DateFormater(n["StartDate"].InnerText);
             hotEvent.EndDate = DateFormater(n["EndDate"].InnerText);
             
             hotEvent.CameraName = n["CameraName"].InnerText;

             hotEvent.PcName = n["Username"].InnerText;
             if (n["Longtitude"] != null)
                 hotEvent.Longtitude = Convert.ToDouble(n["Longtitude"].InnerText);
             else
                 hotEvent.Longtitude = 0;
             if (n["Latitude"] != null)
                 hotEvent.Latitude = Convert.ToDouble(n["Latitude"].InnerText);
             else
                 hotEvent.Latitude = 0;

         }
         return hotEvent;
     }

     public static bool DeleteHotEventByDate(DateTime delDate, string dbConnection)
     {
         using (var connection = new SqlConnection(dbConnection))
         {
             connection.Open();
             var cmd = new SqlCommand("DeleteHotEventByDate", connection);

             cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
             cmd.Parameters["@DeleteDate"].Value = delDate;

             cmd.CommandType = CommandType.StoredProcedure;

             cmd.CommandText = "DeleteHotEventByDate";

             cmd.ExecuteNonQuery();
         }
         return true;
     }

    }
}
