﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Arrowlabs.Business.Layer
{
    [DataContract]
    public class MobileDriverOffence
    {
        [DataMember]
        public byte[] OffenceImage { get; set; }
        [DataMember]
        public string Comments { get; set; }
        public string UpdatedBy { get; set; }
    }
}
