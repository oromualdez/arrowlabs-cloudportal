﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class GroupRole
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int RoleId { get; set; }
        public string GroupName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int SiteId { get; set; }

        private static GroupRole GetGroupRoleFromSqlReader(SqlDataReader resultSet)
        {
            var groupRole = new GroupRole();
            var columns = Enumerable.Range(0, resultSet.FieldCount).Select(resultSet.GetName).ToList();
            if (resultSet["Id"] != DBNull.Value)
                groupRole.Id = Convert.ToInt32(resultSet["Id"].ToString());
            if (resultSet["CreatedDate"] != DBNull.Value)
                groupRole.CreateDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());
            if (resultSet["Updateddate"] != DBNull.Value)
                groupRole.UpdatedDate = Convert.ToDateTime(resultSet["Updateddate"].ToString());
            if (resultSet["CreatedBy"] != DBNull.Value)
                groupRole.CreatedBy = resultSet["CreatedBy"].ToString();
            if (resultSet["GroupName"] != DBNull.Value)
                groupRole.GroupName = resultSet["GroupName"].ToString();
            if (resultSet["GroupId"] != DBNull.Value)
                groupRole.GroupId = Convert.ToInt32(resultSet["GroupId"].ToString());
            if (resultSet["RoleId"] != DBNull.Value)
                groupRole.RoleId = Convert.ToInt32(resultSet["RoleId"].ToString());

            if (columns.Contains("SiteId") && resultSet["SiteId"] != DBNull.Value)
                groupRole.SiteId = Convert.ToInt32(resultSet["SiteId"].ToString());
            
            return groupRole;
        }

        public static bool InsertorUpdateGroupRole(GroupRole groupRole, string dbConnection)
        {
            if (groupRole != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateGroupRole", connection);

                    cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                    cmd.Parameters["@GroupId"].Value = groupRole.GroupId;

                    cmd.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                    cmd.Parameters["@RoleId"].Value = groupRole.RoleId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = groupRole.CreateDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = groupRole.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = groupRole.CreatedBy;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static bool DeleteGroupRoleByGroupId(int groupId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteGroupRoleByGroupId", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static List<GroupRole> GetGroupRolesByGroupId(int groupId, string dbConnection)
        {
            List<GroupRole> allGroupRoles = new List<GroupRole>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetGroupRoleByGroupId", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupRole = GetGroupRoleFromSqlReader(resultSet);
                    allGroupRoles.Add(groupRole);
                }
                connection.Close();
                resultSet.Close();
                return allGroupRoles;
            }
        }

        public static List<GroupRole> GetGroupRolesByRoleId(int roleId, string dbConnection)
        {
            List<GroupRole> allGroupRoles = new List<GroupRole>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetGroupRoleByRoleId", connection);

                cmd.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                cmd.Parameters["@RoleId"].Value = roleId;

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupRole = GetGroupRoleFromSqlReader(resultSet);
                    allGroupRoles.Add(groupRole);
                }
                connection.Close();
                resultSet.Close();
                return allGroupRoles;
            }
        }

        public static bool DeleteGroupRoleByGroupIdAndUserId(int groupId, int roleId, string dbConnection)
        {
            List<GroupUser> allGroupUsers = new List<GroupUser>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteGroupRoleByGroupIdAndRoleId", connection);
                                          
                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = groupId;

                cmd.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                cmd.Parameters["@RoleId"].Value = roleId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
