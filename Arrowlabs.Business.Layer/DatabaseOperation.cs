﻿using System;

namespace Arrowlabs.Business.Layer
{
    public enum DatabaseOperation : int
    {
        Unknown = -1,
        Inserted = 0,
        Updated = 1,
        Deleted = 2,
    }
}
