﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for ItemFoundImgUpload
    /// </summary>
    public class ItemFoundImgUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (context.Request.Files.Count > 0)
                {
                    HttpFileCollection files = context.Request.Files;
                    string fname = string.Empty;
                    var name = string.Empty;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];
                        fname = context.Server.MapPath("~/Uploads/" + file.FileName);
                        name = file.FileName;
                        file.SaveAs(fname);
                    }
                    System.Drawing.Image imageVar = System.Drawing.Image.FromFile(fname);
                    var retV = "FAIL";

                    if (File.Exists(fname))
                    {
                        Stream fs = File.OpenRead(fname);
                        var savestring = CommonUtility.CloudUploadFile(0,name, fs);
                        if (savestring != "FAIL")
                        {
                            retV = savestring;
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        //if (File.Exists(fname))
                        //    File.Delete(fname);
                    }
                    //var mimcon = MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection);
                    //var consplit = mimcon.FilePath.Split('\\');
                    //var newstring = string.Empty;
                    //var count = 0;
                    //foreach (var split in consplit)
                    //{
                    //    if (count < 4)
                    //    {
                    //        newstring += split + "\\";
                    //        count++;
                    //    }
                    //    else
                    //    {
                    //        break;
                    //    }
                    //}
                    //HttpPostedFile file = files[0];

                    //if (System.IO.Path.GetExtension(file.FileName).ToUpperInvariant() == ".PDF")
                    //    newstring = newstring + "Uploads\\ItemFound\\" + System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf";
                    //else
                    //    newstring = newstring + "Uploads\\ItemFound\\" + System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpeg";

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(retV);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("ItemFoundImgUpload", "ProcessRequest", ex, CommonUtility.dbConnection, 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}