﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class DutyAudit
    {
        public int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DutyDate { get; set; }
        public int DutyStatus { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public int BaseAuditId { get; set; }
        public BaseAudit BaseAudit { get; set; }
        public float Latitude { get; set; }
        public float Longtitude { get; set; }

        public static int InsertDutyAudit(DutyAudit dutyAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(dutyAudit.BaseAudit, dbConnection);

            int id = 0;
            if (dutyAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertDutyAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = dutyAudit.Id;
                   
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = dutyAudit.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@DutyStatus", SqlDbType.Int));
                    cmd.Parameters["@DutyStatus"].Value = dutyAudit.DutyStatus;

                    cmd.Parameters.Add(new SqlParameter("@DutyDate", SqlDbType.NVarChar));
                    cmd.Parameters["@DutyDate"].Value = dutyAudit.DutyDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = dutyAudit.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = dutyAudit.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = dutyAudit.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = dutyAudit.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.Float));
                    cmd.Parameters["@Longtitude"].Value = dutyAudit.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = dutyAudit.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = dutyAudit.SiteId;

                    cmd.CommandText = "InsertDutyAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                }
            }
            return id;
        }
    }
}