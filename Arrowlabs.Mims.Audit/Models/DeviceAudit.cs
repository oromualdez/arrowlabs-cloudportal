﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class DeviceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string ClientID { get; set; }
        public string MACAddress { get; set; }
        public string Version { get; set; }
        public string Register { get; set; }
        public string PCName { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ExpiryDate { get; set; }
        public string LastLoginDate { get; set; }
        public string Curfew { get; set; }
        public string State { get; set; }
        public string[] Devicestates { get; set; }
        public static string[] States { get; set; }
        public string GroupName { get; set; }
        public string IP { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string EngagedIn { get; set; }
        public bool Engaged { get; set; }
        public int GeofenceStatus { get; set; }
        public int SiteId { get; set; }

        public static void InsertClientDevice(DeviceAudit dev, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(dev.BaseAudit, dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("InsertOrUpdateClientDevice", connection);

                sqlCommand.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(dev.ClientID, true);

                sqlCommand.Parameters.Add(new SqlParameter("@MACAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MACAddress"].Value = Encrypt.EncryptData(dev.MACAddress, true);

                sqlCommand.Parameters.Add(new SqlParameter("@Version", SqlDbType.NVarChar));
                sqlCommand.Parameters["@version"].Value = Encrypt.EncryptData(dev.Version, true);


                sqlCommand.Parameters.Add(new SqlParameter("@Register", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Register"].Value = Encrypt.EncryptData(dev.Register, true);


                sqlCommand.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@PCName"].Value = Encrypt.EncryptData(dev.PCName, true);

                sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(dev.CreatedDate, true);

                sqlCommand.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ExpiryDate"].Value = Encrypt.EncryptData(dev.ExpiryDate, true);

                sqlCommand.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastLoginDate"].Value = Encrypt.EncryptData(dev.LastLoginDate, true);

                sqlCommand.Parameters.Add(new SqlParameter("@Curfew", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Curfew"].Value = Encrypt.EncryptData(dev.Curfew, true);

                sqlCommand.Parameters.Add(new SqlParameter("@ip", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ip"].Value = dev.IP;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = dev.SiteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "InsertOrUpdateClientDevice";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
        }
    }
}
