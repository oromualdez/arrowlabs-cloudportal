﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class CustomerAndSystemType
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }
        public int CustSystemTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }

        public int CustomerInfoId { get; set; }

        public static int InsertOrUpdateCustomerSystemType(CustomerAndSystemType customerSystemType, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = customerSystemType.Id;

            var action = (customerSystemType.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (customerSystemType != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateCustomerAndSystemType", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customerSystemType.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = customerSystemType.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = customerSystemType.CustomerId;
                    
                    cmd.Parameters.Add(new SqlParameter("@CustSystemTypeId", SqlDbType.Int));
                    cmd.Parameters["@CustSystemTypeId"].Value = customerSystemType.CustSystemTypeId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customerSystemType.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = customerSystemType.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customerSystemType.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customerSystemType.UpdatedBy;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateCustomerAndSystemType";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            customerSystemType.Id = id;
                            var audit = new CustomerAndSystemTypeAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customerSystemType.UpdatedBy);
                            Reflection.CopyProperties(customerSystemType, audit);
                            CustomerAndSystemTypeAudit.InsertCustomerAndSystemTypeAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertCustomerAndSystemTypeAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }


        public static bool DeleteCustomerAndSystemTypeById(int id, int customerId,string dbConnection)
        {
            try
            {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();

                        var cmd = new SqlCommand("DeleteCustomerAndSystemTypeById", connection);

                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = id;
                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = customerId;
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.CommandText = "DeleteCustomerAndSystemTypeById";

                        cmd.ExecuteNonQuery();
                    }
                    return true;
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("CustomerAndSystem", "DeleteCustomerAndSystemTypeById", ex, dbConnection);
            }
            return false;
        }

    }
}
