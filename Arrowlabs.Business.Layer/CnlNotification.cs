﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class CnlNotification
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Notification { get; set; }
        [DataMember]
        public string Owner { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }

        private static CnlNotification GetCnlNotificationFromSqlReader(SqlDataReader reader)
        {
            var notification = new CnlNotification();

            if (reader["ID"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["ID"].ToString());
            if (reader["Notification"] != DBNull.Value)
                notification.Notification = reader["Notification"].ToString();
            if (reader["Owner"] != DBNull.Value)
                notification.Owner = reader["Owner"].ToString();
            if (reader["DateCreated"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["DateCreated"].ToString());
            
            return notification;
        }

        /// <summary>
        /// It returns all notifications
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<CnlNotification> GetAllCnlNotifications(string dbConnection)
        {
            var notifications = new List<CnlNotification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotifications", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetCnlNotificationFromSqlReader(reader);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

    }
}
