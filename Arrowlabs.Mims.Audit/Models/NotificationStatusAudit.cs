﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class NotificationStatusAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int AssigneeId { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int SiteId { get; set; }
        public static int InsertorUpdateNotificationStatus(NotificationStatusAudit notification, string dbConnection)
        {
            var id = 0;
            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateNotificationStatus", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(notification.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.Int));
                    cmd.Parameters["@NotificationId"].Value = notification.NotificationId;

                    cmd.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                    cmd.Parameters["@AssigneeId"].Value = notification.AssigneeId;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Bit));
                    cmd.Parameters["@Status"].Value = notification.Status;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;
                    //cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    //cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
