﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class RosterView
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string TotalDayLieu { get; set; }
        public string TotalDayOff { get; set; }
        public string CurrentMonth { get; set; }
        public string TotalDutyHours { get; set; }
        public string TotalOTHours { get; set; }
        public bool ActiveRoster { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public string Notes { get; set; }

        public string EmployeeName { get; set; }
           

        public int DutyOn { get; set; }

        public int TaskOn { get; set; }

        public int Amendment { get; set; }

                public int IncidentComplete { get; set; }

                public int StatusComplete { get; set; }
         

        public int TotalAssignment { get; set; }

        public int TotalDays { get; set; }

        public int UtilizationAverage { get; set; }

        public string FirstName { get; set; }
       
        public string LastName { get; set; }


        private static RosterView GetRosterViewFromSqlReader(SqlDataReader reader, string dbconnection)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var rosterView = new RosterView();
            if (reader["Id"] != DBNull.Value)
                rosterView.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["EmployeeId"] != DBNull.Value)
                rosterView.EmployeeId = Convert.ToInt32(reader["EmployeeId"].ToString());
            if (reader["TotalDayLieu"] != DBNull.Value)
                rosterView.TotalDayLieu = Decrypt.DecryptData(reader["TotalDayLieu"].ToString(), true, dbconnection);
            if (reader["TotalDayOff"] != DBNull.Value)
                rosterView.TotalDayOff = Decrypt.DecryptData(reader["TotalDayOff"].ToString(), true, dbconnection);
            if (reader["CurrentMonth"] != DBNull.Value)
                rosterView.CurrentMonth = reader["CurrentMonth"].ToString();
            if (reader["SiteId"] != DBNull.Value)
                rosterView.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (reader["CustomerId"] != DBNull.Value)
                rosterView.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
             
            if (reader["CreatedDate"] != DBNull.Value)
                rosterView.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CurrentDayOff"] != DBNull.Value)
                rosterView.TotalDutyHours = Decrypt.DecryptData(reader["CurrentDayOff"].ToString(), true, dbconnection);
            if (reader["CurrentDayLieu"] != DBNull.Value)
                rosterView.TotalOTHours = Decrypt.DecryptData(reader["CurrentDayLieu"].ToString(), true, dbconnection);
            if (reader["ActiveRoster"] != DBNull.Value)
                rosterView.ActiveRoster = Convert.ToBoolean(reader["ActiveRoster"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                rosterView.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                rosterView.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["UpdatedBy"] != DBNull.Value)
                rosterView.UpdatedBy = reader["UpdatedBy"].ToString();


            if (columns.Contains("Notes"))
            {
                if (reader["Notes"] != DBNull.Value)
                    rosterView.Notes = reader["Notes"].ToString();
            }
            if (columns.Contains("Amendment"))
            {
                if (reader["Amendment"] != DBNull.Value)
                    rosterView.Amendment = Convert.ToInt32(reader["Amendment"].ToString());
            }
            return rosterView;
        }
        public static RosterView GetRosterViewByCurrentMonth(int employeeId, string currentMonth, string dbConnection)
        {
            RosterView rosterView = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetRosterViewByCurrentMonth", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.Int));
                    sqlCommand.Parameters["@EmployeeId"].Value = employeeId;
                    sqlCommand.Parameters.Add(new SqlParameter("@CurrentMonth", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@CurrentMonth"].Value = currentMonth;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        rosterView = GetRosterViewFromSqlReader(reader, dbConnection);

                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("RosterView", "GetRosterViewByCurrentMonth", exp, dbConnection);

            }
            return rosterView;
        }

        public static List<RosterView> GetWorkHoursAverage(int employeeId, string from,string to, string dbConnection)
        {
            List<RosterView> rosterViews = new List<RosterView>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetWorkHoursAverage", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@cid", SqlDbType.Int));
                    sqlCommand.Parameters["@cid"].Value = employeeId;
                    sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                    sqlCommand.Parameters["@frmDateTime"].Value = from;

                    sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                    sqlCommand.Parameters["@toDateTime"].Value = to;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var rosterView = new RosterView();

                        if (reader["DutyOn"] != DBNull.Value)
                            rosterView.DutyOn = Convert.ToInt32(reader["DutyOn"].ToString());

                        if (reader["Id"] != DBNull.Value)
                            rosterView.EmployeeId = Convert.ToInt32(reader["Id"].ToString());
                          
                        rosterViews.Add(rosterView);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("RosterView", "GetWorkHoursAverage", exp, dbConnection);

            }
            return rosterViews;
        }

        public static List<RosterView> GetActiveHoursAverage(int employeeId, string from, string to, string dbConnection)
        {
            List<RosterView> rosterViews = new List<RosterView>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetActiveHoursAverage", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@cid", SqlDbType.Int));
                    sqlCommand.Parameters["@cid"].Value = employeeId;
                    sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                    sqlCommand.Parameters["@frmDateTime"].Value = from;

                    sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                    sqlCommand.Parameters["@toDateTime"].Value = to;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var rosterView = new RosterView();

                        if (reader["TaskOn"] != DBNull.Value)
                            rosterView.TaskOn = Convert.ToInt32(reader["TaskOn"].ToString());

                        if (reader["Id"] != DBNull.Value)
                            rosterView.EmployeeId = Convert.ToInt32(reader["Id"].ToString());
                         

                        rosterViews.Add(rosterView);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("RosterView", "GetActiveHoursAverage", exp, dbConnection);

            }
            return rosterViews;
        }

        public static List<RosterView> GetAssignmentAverage(int employeeId,int at ,string from, string to, string dbConnection)
        {
            List<RosterView> rosterViews = new List<RosterView>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAssignmentAverage", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@cid", SqlDbType.Int));
                    sqlCommand.Parameters["@cid"].Value = employeeId;

                    sqlCommand.Parameters.Add(new SqlParameter("@Atype", SqlDbType.Int));
                    sqlCommand.Parameters["@Atype"].Value = at;

                    sqlCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@startDate"].Value = from;

                    sqlCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@endDate"].Value = to;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var rosterView = new RosterView();

                        if (reader["StatusComplete"] != DBNull.Value)
                            rosterView.TaskOn = Convert.ToInt32(reader["StatusComplete"].ToString());

                        if (reader["IncidentComplete"] != DBNull.Value)
                            rosterView.DutyOn = Convert.ToInt32(reader["IncidentComplete"].ToString());

                        rosterView.TotalAssignment = rosterView.TaskOn + rosterView.DutyOn;

                        if (reader["Id"] != DBNull.Value)
                            rosterView.EmployeeId = Convert.ToInt32(reader["Id"].ToString()); 

                        rosterViews.Add(rosterView);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("RosterView", "GetAssignmentAverage", exp, dbConnection);

            }
            return rosterViews;
        }

        public static List<RosterView> GetStaffAverage(int employeeId, int at, string from, string to, string dbConnection)
        {
            List<RosterView> rosterViews = new List<RosterView>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetStaffAverage", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@cid", SqlDbType.Int));
                    sqlCommand.Parameters["@cid"].Value = employeeId;

                    sqlCommand.Parameters.Add(new SqlParameter("@Atype", SqlDbType.Int));
                    sqlCommand.Parameters["@Atype"].Value = at;

                    sqlCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@startDate"].Value = from;

                    sqlCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@endDate"].Value = to;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var rosterView = new RosterView();

                        if (reader["TaskOn"] != DBNull.Value)
                            rosterView.TaskOn = Convert.ToInt32(reader["TaskOn"].ToString());

                        if (reader["DutyOn"] != DBNull.Value)
                            rosterView.DutyOn = Convert.ToInt32(reader["DutyOn"].ToString());

                        if (reader["FirstName"] != DBNull.Value)
                            rosterView.FirstName = reader["FirstName"].ToString();

                        if (reader["EmployeeName"] != DBNull.Value)
                            rosterView.EmployeeName = reader["EmployeeName"].ToString();

                        if (reader["LastName"] != DBNull.Value)
                            rosterView.LastName = reader["LastName"].ToString();
                          
                        if (reader["StatusComplete"] != DBNull.Value)
                            rosterView.StatusComplete = Convert.ToInt32(reader["StatusComplete"].ToString());

                        if (reader["IncidentComplete"] != DBNull.Value)
                            rosterView.IncidentComplete = Convert.ToInt32(reader["IncidentComplete"].ToString());

                        if (reader["TotalDays"] != DBNull.Value)
                            rosterView.TotalDays = Convert.ToInt32(reader["TotalDays"].ToString());

                        if (reader["Amendment"] != DBNull.Value)
                            rosterView.Amendment = Convert.ToInt32(reader["Amendment"].ToString());

                        if (rosterView.DutyOn > 0 && rosterView.TaskOn > 0)
                            rosterView.UtilizationAverage = (int)Math.Round((float)rosterView.TaskOn / (float)rosterView.DutyOn * (float)100);

                        rosterView.TotalAssignment = rosterView.StatusComplete + rosterView.IncidentComplete;

                        if (reader["Id"] != DBNull.Value)
                            rosterView.EmployeeId = Convert.ToInt32(reader["Id"].ToString());

                        rosterViews.Add(rosterView);
                    }
                    connection.Close();
                    reader.Close();
                } 
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("RosterView", "GetStaffAverage", exp, dbConnection);

            }
            return rosterViews;
        }
        public static List<RosterView> GetStaffAverageTemp(int employeeId, int at, string from, string to, string dbConnection)
        {
            List<RosterView> rosterViews = new List<RosterView>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetStaffAverageTemp", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@cid", SqlDbType.Int));
                    sqlCommand.Parameters["@cid"].Value = employeeId;

                    sqlCommand.Parameters.Add(new SqlParameter("@Atype", SqlDbType.Int));
                    sqlCommand.Parameters["@Atype"].Value = at;

                    sqlCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@startDate"].Value = from;

                    sqlCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@endDate"].Value = to;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var rosterView = new RosterView();

                        if (reader["TaskOn"] != DBNull.Value)
                            rosterView.TaskOn = Convert.ToInt32(reader["TaskOn"].ToString());

                        if (reader["DutyOn"] != DBNull.Value)
                            rosterView.DutyOn = Convert.ToInt32(reader["DutyOn"].ToString());

                        if (reader["FirstName"] != DBNull.Value)
                            rosterView.FirstName = reader["FirstName"].ToString();

                        if (reader["EmployeeName"] != DBNull.Value)
                            rosterView.EmployeeName = reader["EmployeeName"].ToString();

                        if (reader["LastName"] != DBNull.Value)
                            rosterView.LastName = reader["LastName"].ToString();

                        if (reader["StatusComplete"] != DBNull.Value)
                            rosterView.StatusComplete = Convert.ToInt32(reader["StatusComplete"].ToString());

                        if (reader["IncidentComplete"] != DBNull.Value)
                            rosterView.IncidentComplete = Convert.ToInt32(reader["IncidentComplete"].ToString());

                        if (reader["TotalDays"] != DBNull.Value)
                            rosterView.TotalDays = Convert.ToInt32(reader["TotalDays"].ToString());

                        //if (reader["Amendment"] != DBNull.Value)
                        //    rosterView.Amendment = Convert.ToInt32(reader["Amendment"].ToString());

                        if (rosterView.DutyOn > 0 && rosterView.TaskOn > 0)
                            rosterView.UtilizationAverage = (int)Math.Round((float)rosterView.TaskOn / (float)rosterView.DutyOn * (float)100);

                        rosterView.TotalAssignment = rosterView.StatusComplete + rosterView.IncidentComplete;

                        if (reader["Id"] != DBNull.Value)
                            rosterView.EmployeeId = Convert.ToInt32(reader["Id"].ToString());

                        rosterViews.Add(rosterView);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("RosterView", "GetStaffAverageTemp", exp, dbConnection);

            }
            return rosterViews;
        }

        public static List<RosterView> GetUtilizationAverage(int employeeId, string from, string to, string dbConnection)
        {
            List<RosterView> rosterViews = new List<RosterView>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetUtilizationAverage", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@cid", SqlDbType.Int));
                    sqlCommand.Parameters["@cid"].Value = employeeId;
                     
                    sqlCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@startDate"].Value = from;

                    sqlCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@endDate"].Value = to;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var rosterView = new RosterView();

                        if (reader["TaskOn"] != DBNull.Value)
                            rosterView.TaskOn = Convert.ToInt32(reader["TaskOn"].ToString());

                        if (reader["DutyOn"] != DBNull.Value)
                            rosterView.DutyOn = Convert.ToInt32(reader["DutyOn"].ToString());


                        if (rosterView.DutyOn > 0 && rosterView.TaskOn > 0)
                            rosterView.UtilizationAverage = (int)Math.Round((float)rosterView.TaskOn / (float)rosterView.DutyOn * (float)100);

                        if (reader["Id"] != DBNull.Value)
                            rosterView.EmployeeId = Convert.ToInt32(reader["Id"].ToString());

                        rosterViews.Add(rosterView);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("RosterView", "GetUtilizationAverage", exp, dbConnection);

            }
            return rosterViews;
        }

        public static int InsertOrUpdateRosterView(RosterView rosterView, string dbConnection,
      string auditDbConnection = "", bool isAuditEnabled = true)
    {
            int id = 0;
            var action = (rosterView.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;
            if (rosterView != null)
            {
                id = rosterView.Id;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateRosterView", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = rosterView.Id;
                    cmd.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.Int));
                    cmd.Parameters["@EmployeeId"].Value = rosterView.EmployeeId;
                    cmd.Parameters.Add(new SqlParameter("@TotalDayLieu", SqlDbType.NVarChar));
                    cmd.Parameters["@TotalDayLieu"].Value = Encrypt.EncryptData(rosterView.TotalDayLieu, true, dbConnection);
                    cmd.Parameters.Add(new SqlParameter("@TotalDayOff", SqlDbType.NVarChar));
                    cmd.Parameters["@TotalDayOff"].Value = Encrypt.EncryptData(rosterView.TotalDayOff, true, dbConnection);
                    cmd.Parameters.Add(new SqlParameter("@CurrentMonth", SqlDbType.NVarChar));
                    cmd.Parameters["@CurrentMonth"].Value = rosterView.CurrentMonth;
                    cmd.Parameters.Add(new SqlParameter("@CurrentDayOff", SqlDbType.NVarChar));
                    cmd.Parameters["@CurrentDayOff"].Value = Encrypt.EncryptData(rosterView.TotalDutyHours, true, dbConnection);
                    cmd.Parameters.Add(new SqlParameter("@CurrentDayLieu", SqlDbType.NVarChar));
                    cmd.Parameters["@CurrentDayLieu"].Value = Encrypt.EncryptData(rosterView.TotalOTHours, true, dbConnection);
                    cmd.Parameters.Add(new SqlParameter("@ActiveRoster", SqlDbType.Bit));
                    cmd.Parameters["@ActiveRoster"].Value = rosterView.ActiveRoster;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = rosterView.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = rosterView.CustomerId;
                     
                    cmd.Parameters.Add(new SqlParameter("@Amendment", SqlDbType.Int));
                    cmd.Parameters["@Amendment"].Value = rosterView.Amendment; 
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = rosterView.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = rosterView.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = rosterView.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Notes", SqlDbType.NVarChar));
                    cmd.Parameters["@Notes"].Value = rosterView.Notes;
                     
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = rosterView.UpdatedBy;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            if (id > 0)
                                rosterView.Id = id;

                            var audit = new RosterViewAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, rosterView.UpdatedBy);
                            Reflection.CopyProperties(rosterView, audit);
                            RosterViewAudit.InsertOrUpdateRosterViewAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateRosterView", ex, dbConnection);
                    }
                }
            }
            return id;
        }
    }
}

