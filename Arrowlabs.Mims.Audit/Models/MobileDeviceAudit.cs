﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public enum MobileDeviceAuditType
    {
        None = 0,
        Mobile = 1,
        Tablet = 2
    }
    public class MobileDeviceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string Uuid { get; set; }
        public int DeviceType { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public int SiteId { get;set;}

        private static MobileDeviceAudit GetMobileDeviceAuditFromSqlReader(SqlDataReader reader, string dbConnection)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var mobileDevice = new MobileDeviceAudit();
            mobileDevice.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                mobileDevice.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Uuid") && reader["Uuid"] != DBNull.Value)
                mobileDevice.Uuid = Decrypt.DecryptData(reader["Uuid"].ToString(), true);
            if (columns.Contains("DeviceType") && reader["DeviceType"] != DBNull.Value)
                mobileDevice.DeviceType = Convert.ToInt32(Decrypt.DecryptData(reader["DeviceType"].ToString(), true));
            if (columns.Contains("IsActive") && reader["IsActive"] != DBNull.Value)
                mobileDevice.IsActive = Convert.ToBoolean(Decrypt.DecryptData(reader["IsActive"].ToString(), true));
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                mobileDevice.CreatedDate = Convert.ToDateTime(Decrypt.DecryptData(reader["CreatedDate"].ToString(), true));
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                mobileDevice.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return mobileDevice;
        }

        public static bool InsertMobileDeviceAudit(MobileDeviceAudit mobileDevice, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(mobileDevice.BaseAudit, dbConnection);

            if (mobileDevice != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMobileDeviceAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = mobileDevice.Id;
                    cmd.Parameters.Add("@Uuid", SqlDbType.NVarChar).Value = Encrypt.EncryptData(mobileDevice.Uuid, true);
                    cmd.Parameters.Add("@DeviceType", SqlDbType.NVarChar).Value = Encrypt.EncryptData(mobileDevice.DeviceType.ToString(), true);
                    cmd.Parameters.Add("@IsActive", SqlDbType.NVarChar).Value = Encrypt.EncryptData(mobileDevice.IsActive.ToString(), true);
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.NVarChar).Value = Encrypt.EncryptData(mobileDevice.CreatedDate.ToString(), true);
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = mobileDevice.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static MobileDeviceAudit GetAllMobileDeviceAudit(string dbConnection)
        {
            var mobileDevice = new MobileDeviceAudit();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllMobileDeviceAudit", connection);
         
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    mobileDevice = GetMobileDeviceAuditFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return mobileDevice;
        }

    }
}
