﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{
    public class TransactionLog
    {
        public int ID { get; set; }
        public Guid DriverIdentifier { get; set; }
        public string ReceivedBy { get; set; }
        public string ForwardedBy { get; set; }
        public string SentBy { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public DateTime? SentDate { get; set; }
        public DateTime? ForwardedDate { get; set; }
        public int Status { get; set; }
        public string ReceivingType { get; set; }

        public static List<TransactionLog> GetAllTransactionLog(string dbConnection)
        {
            var TransactionList = new List<TransactionLog>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllTransactionLog", connection);
                sqlCommand.CommandText = "GetAllTransactionLog";
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var transactionClass = new TransactionLog();
                    transactionClass.ID = Convert.ToInt32(reader["ID"].ToString());
                    transactionClass.DriverIdentifier = Guid.Parse(reader["DriverIdentifier"].ToString());

                    if (reader["ReceivedBy"] != DBNull.Value)
                        transactionClass.ReceivedBy = reader["ReceivedBy"].ToString();
                    if (reader["ForwardedBy"] != DBNull.Value)
                        transactionClass.ForwardedBy = reader["ForwardedBy"].ToString();
                    if (reader["SentBy"] != DBNull.Value)
                        transactionClass.SentBy = reader["SentBy"].ToString();
                    if (reader["ReceivedDate"] != DBNull.Value)
                        transactionClass.ReceivedDate = Convert.ToDateTime(reader["ReceivedDate"].ToString());
                    if (reader["SentDate"] != DBNull.Value)
                        transactionClass.SentDate = Convert.ToDateTime(reader["SentDate"].ToString());
                    if (reader["ForwardedDate"] != DBNull.Value)
                        transactionClass.ForwardedDate = Convert.ToDateTime(reader["ForwardedDate"].ToString());
                    if (reader["Status"] != DBNull.Value)
                        transactionClass.Status = Convert.ToInt32(reader["Status"].ToString());
                    if (reader["ReceivingType"] != DBNull.Value)
                        transactionClass.ReceivingType = reader["ReceivingType"].ToString();

                    TransactionList.Add(transactionClass);
                }
                connection.Close();
                reader.Close();
            }
            return TransactionList;
        }
        public static bool InsertOrUpdateTransactionLog(TransactionLog transaction, string dbConnection)
        {
            if (transaction != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateTransactionLog", connection);

                    cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int));
                    cmd.Parameters["@ID"].Value = transaction.ID;

                    cmd.Parameters.Add(new SqlParameter("@DriverIdentifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@DriverIdentifier"].Value = transaction.DriverIdentifier;

                    cmd.Parameters.Add(new SqlParameter("@ReceivedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@ReceivedBy"].Value = transaction.ReceivedBy;

                    cmd.Parameters.Add(new SqlParameter("@ForwardedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@ForwardedBy"].Value = transaction.ForwardedBy;

                    cmd.Parameters.Add(new SqlParameter("@SentBy", SqlDbType.NVarChar));
                    cmd.Parameters["@SentBy"].Value = transaction.SentBy;

                    cmd.Parameters.Add(new SqlParameter("@ReceivedDate", SqlDbType.DateTime));
                    cmd.Parameters["@ReceivedDate"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SqlParameter("@SentDate", SqlDbType.DateTime));
                    cmd.Parameters["@SentDate"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SqlParameter("@ForwardedDate", SqlDbType.DateTime));
                    cmd.Parameters["@ForwardedDate"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = transaction.Status;

                    cmd.Parameters.Add(new SqlParameter("@ReceivingType", SqlDbType.NVarChar));
                    cmd.Parameters["@ReceivingType"].Value = transaction.ReceivingType;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateTransactionLog";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
