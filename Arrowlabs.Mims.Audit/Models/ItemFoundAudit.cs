﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
   public class ItemFoundAudit
    {
        public BaseAudit BaseAudit { get; set; } 
        public int Id { get; set; }
        public DateTime DateFound { get; set; }
        public string LocationFound { get; set; }
        public string RoomNumber { get; set; }
        public string Type { get; set; }
        public string Brand { get; set; }
        public string Colour { get; set; }
        public string Status { get; set; }
        public string FinderName { get; set; }
        public string FinderDepartment { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiveDate { get; set; }
        public string StorageLocation { get; set; }
        public string Reference { get; set; }
        public string ImagePath { get; set; }
        public int SiteId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public string ShelfLife { get; set; }
        public string SubType { get; set; }
        public string SubStorageLocation { get; set; }

        public string Remarks { get; set; }
       
        public int TransferFromSiteId { get; set; }
        public int CustomerId { get; set; }
        private static ItemFoundAudit GetItemFoundFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var itemFound = new ItemFoundAudit();
            if (reader["Id"] != DBNull.Value)
                itemFound.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["DateFound"] != DBNull.Value)
                itemFound.DateFound = Convert.ToDateTime(reader["DateFound"].ToString());
            if (reader["LocationFound"] != DBNull.Value)
                itemFound.LocationFound = reader["LocationFound"].ToString();
            if (reader["RoomNumber"] != DBNull.Value)
                itemFound.RoomNumber = reader["RoomNumber"].ToString();
            if (reader["Type"] != DBNull.Value)
                itemFound.Type = reader["Type"].ToString();
            if (reader["Brand"] != DBNull.Value)
                itemFound.Brand = reader["Brand"].ToString();
            if (reader["Colour"] != DBNull.Value)
                itemFound.Colour = reader["Colour"].ToString();
            if (reader["Status"] != DBNull.Value)
                itemFound.Status = reader["Status"].ToString();
            if (reader["FinderName"] != DBNull.Value)
                itemFound.FinderName = reader["FinderName"].ToString();
            if (reader["FinderDepartment"] != DBNull.Value)
                itemFound.FinderDepartment = reader["FinderDepartment"].ToString();
            if (reader["ReceiverName"] != DBNull.Value)
                itemFound.ReceiverName = reader["ReceiverName"].ToString();
            if (reader["ReceiveDate"] != DBNull.Value)
                itemFound.ReceiveDate = reader["ReceiveDate"].ToString();
            if (reader["StorageLocation"] != DBNull.Value)
                itemFound.StorageLocation = reader["StorageLocation"].ToString();
            if (reader["Reference"] != DBNull.Value)
                itemFound.Reference = reader["Reference"].ToString();
            if (reader["ImagePath"] != DBNull.Value)
                itemFound.ImagePath = reader["ImagePath"].ToString();

            if (reader["SiteId"] != DBNull.Value)
                itemFound.SiteId = Convert.ToInt32(reader["SiteId"]);
            if (reader["CreatedDate"] != DBNull.Value)
                itemFound.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                itemFound.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                itemFound.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["UpdatedBy"] != DBNull.Value)
                itemFound.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["SubStorageLocation"] != DBNull.Value)
                itemFound.SubStorageLocation = reader["SubStorageLocation"].ToString();

            if (reader["SubType"] != DBNull.Value)
                itemFound.SubType = reader["SubType"].ToString();

            if (reader["ShelfLife"] != DBNull.Value)
                itemFound.ShelfLife = reader["ShelfLife"].ToString();
             
            

            if (reader["TransferFromSiteId"] != DBNull.Value)
                itemFound.TransferFromSiteId = Convert.ToInt32(reader["TransferFromSiteId"].ToString());


            return itemFound;
        }

        public static bool InsertorUpdateItemFound(ItemFoundAudit itemFound, string auditDbConnection )
        {
            if (itemFound != null)
            {
                var baseAuditId = BaseAudit.InsertBaseAudit(itemFound.BaseAudit, auditDbConnection);

                using (var connection = new SqlConnection(auditDbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateItemFound", connection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = itemFound.Id;
                    cmd.Parameters.Add(new SqlParameter("@DateFound", SqlDbType.DateTime));
                    cmd.Parameters["@DateFound"].Value = itemFound.DateFound;
                    cmd.Parameters.Add(new SqlParameter("@LocationFound", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationFound"].Value = itemFound.LocationFound;
                    cmd.Parameters.Add(new SqlParameter("@RoomNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@RoomNumber"].Value = itemFound.RoomNumber;
                    cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.NVarChar));
                    cmd.Parameters["@Type"].Value = itemFound.Type;
                    cmd.Parameters.Add(new SqlParameter("@Colour", SqlDbType.NVarChar));
                    cmd.Parameters["@Colour"].Value = itemFound.Colour;
                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.NVarChar));
                    cmd.Parameters["@Status"].Value = itemFound.Status;
                    cmd.Parameters.Add(new SqlParameter("@FinderName", SqlDbType.NVarChar));
                    cmd.Parameters["@FinderName"].Value = itemFound.FinderName;
                    cmd.Parameters.Add(new SqlParameter("@FinderDepartment", SqlDbType.NVarChar));
                    cmd.Parameters["@FinderDepartment"].Value = itemFound.FinderDepartment;
                    cmd.Parameters.Add(new SqlParameter("@ReceiverName", SqlDbType.NVarChar));
                    cmd.Parameters["@ReceiverName"].Value = itemFound.ReceiverName;
                    cmd.Parameters.Add(new SqlParameter("@ReceiveDate", SqlDbType.DateTime));
                    cmd.Parameters["@ReceiveDate"].Value = itemFound.ReceiveDate;
                    cmd.Parameters.Add(new SqlParameter("@StorageLocation", SqlDbType.NVarChar));
                    cmd.Parameters["@StorageLocation"].Value = itemFound.StorageLocation;
                    cmd.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    cmd.Parameters["@Reference"].Value = itemFound.Reference;
                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = itemFound.ImagePath;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = itemFound.SiteId;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = itemFound.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = itemFound.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = itemFound.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = itemFound.UpdatedBy;
                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = itemFound.Remarks;
                    
                    cmd.Parameters.Add(new SqlParameter("@SubStorageLocation", SqlDbType.NVarChar));
                    cmd.Parameters["@SubStorageLocation"].Value = itemFound.SubStorageLocation;

                    cmd.Parameters.Add(new SqlParameter("@SubType", SqlDbType.NVarChar));
                    cmd.Parameters["@SubType"].Value = itemFound.SubType;

                    cmd.Parameters.Add(new SqlParameter("@ShelfLife", SqlDbType.NVarChar));
                    cmd.Parameters["@ShelfLife"].Value = itemFound.ShelfLife;

                    cmd.Parameters.Add(new SqlParameter("@TransferFromSiteId", SqlDbType.Int));
                    cmd.Parameters["@TransferFromSiteId"].Value = itemFound.TransferFromSiteId;


                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = itemFound.CustomerId;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                }
            }
            return true;
        }

    }
}
