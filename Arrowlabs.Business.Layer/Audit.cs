﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class TaskAudit
    {
        public int TaskId { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string TaskName { get; set; }
        public string AssigneeName { get; set; }
        public static bool InsertTasksDeletedAudit(TaskAudit audit, string dbConnection)
        {
            if (audit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTasksDeletedAudit", connection);

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = audit.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@TaskName", SqlDbType.NVarChar));
                    cmd.Parameters["@TaskName"].Value = audit.TaskName;

                    cmd.Parameters.Add(new SqlParameter("@AssigneeName", SqlDbType.NVarChar));
                    cmd.Parameters["@AssigneeName"].Value = audit.AssigneeName;

                    cmd.Parameters.Add(new SqlParameter("@DeletedDate", SqlDbType.DateTime));
                    cmd.Parameters["@DeletedDate"].Value = audit.DeletedDate;

                    cmd.Parameters.Add(new SqlParameter("@DeletedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@DeletedBy"].Value = audit.DeletedBy;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static List<TaskAudit> GetAllAuditedTasks(string dbConnection)
        {
            var assetList = new List<TaskAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAuditedTasks", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = new TaskAudit();

                    asset.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
                    asset.TaskName = reader["TaskName"].ToString();
                    asset.AssigneeName = reader["AssigneeName"].ToString();
                    asset.DeletedDate = Convert.ToDateTime(reader["DeletedDate"].ToString());
                    asset.DeletedBy = reader["DeletedBy"].ToString();


                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

    }
    [Serializable]
    public class UserAudit
    {
        public int UserID { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }

        public static bool InsertUsersDeletedAudit(UserAudit audit, string dbConnection)
        {
            if (audit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertUsersDeletedAudit", connection);

                    cmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
                    cmd.Parameters["@UserID"].Value = audit.UserID;

                    cmd.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                    cmd.Parameters["@LastName"].Value = audit.LastName;

                    cmd.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                    cmd.Parameters["@FirstName"].Value = audit.FirstName;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = audit.UserName;

                    cmd.Parameters.Add(new SqlParameter("@DeletedDate", SqlDbType.DateTime));
                    cmd.Parameters["@DeletedDate"].Value = audit.DeletedDate;

                    cmd.Parameters.Add(new SqlParameter("@DeletedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@DeletedBy"].Value = audit.DeletedBy;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static List<UserAudit> GetAllAuditedUsers(string dbConnection)
        {
            var assetList = new List<UserAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAuditedUsers", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = new UserAudit();

                    asset.UserID = Convert.ToInt32(reader["UserID"].ToString());
                    asset.UserName = reader["UserName"].ToString();
                    asset.FirstName = reader["FirstName"].ToString();
                    asset.LastName = reader["LastName"].ToString();
                    asset.DeletedDate = Convert.ToDateTime(reader["DeletedDate"].ToString());
                    asset.DeletedBy = reader["DeletedBy"].ToString();


                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }
    }
    [Serializable]
    public class DeviceAudit
    {
        public string MacAddress { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string PCName { get; set; }

        public static bool InsertDeviceDeletedAudit(DeviceAudit audit, string dbConnection)
        {
            if (audit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertDeviceDeletedAudit", connection);


                    cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MacAddress"].Value = audit.MacAddress;

                    cmd.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                    cmd.Parameters["@PCName"].Value = audit.PCName;

                    cmd.Parameters.Add(new SqlParameter("@DeletedDate", SqlDbType.DateTime));
                    cmd.Parameters["@DeletedDate"].Value = audit.DeletedDate;

                    cmd.Parameters.Add(new SqlParameter("@DeletedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@DeletedBy"].Value = audit.DeletedBy;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static List<DeviceAudit> GetAllAuditedDevices(string dbConnection)
        {
            var assetList = new List<DeviceAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAuditedDevices", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = new DeviceAudit();

                    asset.MacAddress = reader["MacAddress"].ToString();
                    asset.PCName = reader["PCName"].ToString();
                    asset.DeletedDate = Convert.ToDateTime(reader["DeletedDate"].ToString());
                    asset.DeletedBy = reader["DeletedBy"].ToString();


                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }
    }
}
