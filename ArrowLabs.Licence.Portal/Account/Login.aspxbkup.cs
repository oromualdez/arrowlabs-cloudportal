﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Configuration;
using ArrowLabs.Licence.Portal.Helpers;
using System.Web.Security;
using System.Web.Mvc;

namespace ArrowLabs.Licence.Portal.Account
{
    
    public partial class Login : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            TextBox txtUserName = (TextBox)LoginUser.FindControl("UserName");
            
            try
            {
                if (!IsPostBack)
                {
                    if (Request.Cookies["loginCookie"] != null)
                    {
                        HttpCookie loginCookie = Request.Cookies["loginCookie"];
                        LoginUser.UserName = Decrypt.DecryptData(loginCookie.Values["username"].ToString(),true);
                        LoginUser.RememberMeSet = true;
                    }
                }
                var langs = LanguageClass.GetAllLanguage(CommonUtility.dbConnection);
                var langlist = new List<LanguageClass>();
                var newLang = new LanguageClass();
                newLang.LanguageName = "Please Select Language";
                langlist.Add(newLang);
                langlist.AddRange(langs);
                DropDownList UsersList = LoginUser.FindControl("languageSelect") as DropDownList;
                UsersList.DataTextField = "LanguageName";
                UsersList.DataValueField = "LanguageName";
                UsersList.DataSource = langlist;
                UsersList.DataBind();
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Login", "Page_Load", er, CommonUtility.dbConnection);
            }

            this.SetFocus(txtUserName);    
        }
        string dbConnection { get; set; }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        [Authorize]
        protected void LoginUser_Authenticate(object sender, AuthenticateEventArgs e)
        {
            
            dbConnection = CommonUtility.dbConnection;
            //Membership.ValidateUser(LoginUser.UserName, LoginUser.Password) for Active directory.
         
            try
            {
                if (LoginUser.RememberMeSet)
                {
                    HttpCookie loginCookie = new HttpCookie("loginCookie");
                    Response.Cookies.Remove("loginCookie");
                    Response.Cookies.Add(loginCookie);
                    loginCookie.Values.Add("username",Encrypt.EncryptData(LoginUser.UserName.ToString(),true));
                    DateTime dtExpiry = DateTime.Now.AddDays(15);
                    Response.Cookies["loginCookie"].Expires = dtExpiry;
                }
                else
                {
                    HttpCookie loginCookie = new HttpCookie("loginCookie");
                    Response.Cookies.Remove("loginCookie");
                    Response.Cookies.Add(loginCookie);
                    loginCookie.Values.Add("username", Encrypt.EncryptData(LoginUser.UserName.ToString(), true));
                    DateTime dtExpiry = DateTime.Now.AddSeconds(1); //you can add years and months too here
                    Response.Cookies["loginCookie"].Expires = dtExpiry;
                }
                var loggedInUser = Users.Login(LoginUser.UserName, LoginUser.Password, dbConnection);
                var localip = GetIPAddress();

                if (loggedInUser != null && (loggedInUser.RoleId == (int)Role.Manager || loggedInUser.RoleId == (int)Role.Admin || loggedInUser.RoleId == (int)Role.SuperAdmin))
                {
                    var isValid = true;
                    if (loggedInUser.RoleId == (int)Role.SuperAdmin)
                    {
                        var serverip = ConfigSettings.GetConfigSettings(dbConnection).ServerIP;
                        if(serverip != "127.0.0.1")
                        {
                            var splits = serverip.Split('.');
                           
                            var localsplits = localip.Split('.');
                            if (splits.Length > 3 && localip.Length > 3)
                            {
                                if (splits[0] != localsplits[0])
                                {
                                    isValid = false;
                                    LoginUser.FailureText = "Cannot access site from outside the domain.";
                                    var loginHistory = new LoginSession();
                                    loginHistory.IPAddress = localip;
                                    loginHistory.LoginDate = DateTime.Now;
                                    loginHistory.LogoutDate = DateTime.Now;
                                    loginHistory.MacAddress = LoginUser.UserName;
                                    loginHistory.SiteId = loggedInUser.SiteId;
                                    loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                    loginHistory.PCName = "Tried to access site from another domain";
                                    LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection);
                                }
                                else if (splits[1] != localsplits[1])
                                {
                                    isValid = false;
                                    LoginUser.FailureText = "Cannot access site from outside the domain.";
                                    var loginHistory = new LoginSession();
                                    loginHistory.IPAddress = localip;
                                    loginHistory.LoginDate = DateTime.Now;
                                    loginHistory.LogoutDate = DateTime.Now;
                                    loginHistory.MacAddress = LoginUser.UserName;
                                    loginHistory.SiteId = loggedInUser.SiteId;
                                    loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                    loginHistory.PCName = "Tried to access site from another domain";
                                    LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection);
                                }
                                else if (splits[2] != localsplits[2])
                                {
                                    isValid = false;
                                    LoginUser.FailureText = "Cannot access site from outside the domain.";
                                    var loginHistory = new LoginSession();
                                    loginHistory.IPAddress = localip;
                                    loginHistory.LoginDate = DateTime.Now;
                                    loginHistory.LogoutDate = DateTime.Now;
                                    loginHistory.MacAddress = LoginUser.UserName;
                                    loginHistory.SiteId = loggedInUser.SiteId;
                                    loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                    loginHistory.PCName = "Tried to access site from another domain";
                                    LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection);
                                }
                            }
                            else
                            {
                                isValid = false;
                                LoginUser.FailureText = "Cannot access site from outside the domain.";
                                var loginHistory = new LoginSession();
                                loginHistory.IPAddress = localip;
                                loginHistory.LoginDate = DateTime.Now;
                                loginHistory.LogoutDate = DateTime.Now;
                                loginHistory.MacAddress = LoginUser.UserName;
                                loginHistory.SiteId = loggedInUser.SiteId;
                                loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                loginHistory.PCName = "Tried to access site from another domain";
                                LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection);
                            }
                        }
                    }
                    if (isValid) {
                        if (loggedInUser.Register != "2")
                        {
                            LoginSession.LogoutUserByUsername(LoginUser.UserName, dbConnection);
                            var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(LoginUser.UserName, dbConnection);
                            if (pushDev != null)
                            {
                                if (!string.IsNullOrEmpty(pushDev.Username))
                                {
                                    pushDev.IsServerPortal = true;
                                    PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection);
                                }
                            }
                            Session["SessionId"] = System.Web.HttpContext.Current.Session.SessionID;
                            var userLoginDetail = new LoginSession();
                            userLoginDetail.Username = loggedInUser.Username;
                            userLoginDetail.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            userLoginDetail.LoggedIn = true;
                            userLoginDetail.DeviceType = loggedInUser.DeviceType;
                            userLoginDetail.SiteId = loggedInUser.SiteId;
                            //LoginSession.InsertLogin(userLoginDetail, AppConstant.ConnectionString);
                            LoginSession.InsertLogin(userLoginDetail, dbConnection);

                            var loginHistory = new LoginSession();
                            loginHistory.IPAddress = localip;
                            loginHistory.LoginDate = DateTime.Now;
                            loginHistory.LogoutDate = DateTime.Now;
                            loginHistory.MacAddress = LoginUser.UserName;
                            loginHistory.SiteId = loggedInUser.SiteId;
                            loginHistory.LoggedIn = true;
                            loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            loginHistory.PCName = "Successful login";
                            LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection);
                            e.Authenticated = true;
                            var uModules = UserModules.GetAllModulesByUsername(LoginUser.UserName, dbConnection);
                            if (uModules.Count > 0)
                            {
                                foreach (var mods in uModules)
                                {
                                    if (mods.ModuleId == (int)Accounts.ModuleTypes.Dash)
                                    {
                                        LoginUser.DestinationPageUrl = "~/Default.aspx";
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Alarms)
                                    {
                                        LoginUser.DestinationPageUrl = "~/Pages/Incident.aspx";
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Database)
                                    {
                                        LoginUser.DestinationPageUrl = "~/Pages/Ticketing.aspx";
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks)
                                    {
                                        LoginUser.DestinationPageUrl = "~/Pages/Tasks.aspx";
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Reports)
                                    {
                                        LoginUser.DestinationPageUrl = "~/ReportPages/Reports.aspx";
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Notification)
                                    {
                                        LoginUser.DestinationPageUrl = "~/Pages/Messages.aspx";
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Verifier)
                                    {
                                        LoginUser.DestinationPageUrl = "~/Pages/VerifierPage.aspx";
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.Devices)
                                    {
                                        LoginUser.DestinationPageUrl = "~/Pages/Devices.aspx";
                                        break;
                                    }
                                    else if (mods.ModuleId == (int)Accounts.ModuleTypes.System)
                                    {
                                        LoginUser.DestinationPageUrl = "~/Pages/Users.aspx";
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                LoginUser.DestinationPageUrl = "~/Default.aspx";
                            }
                        }
                        else
                        {
                            LoginUser.FailureText = "User has been deactivated, Kindly contact administrator.";
                            var loginHistory = new LoginSession();
                            loginHistory.IPAddress = localip;
                            loginHistory.LoginDate = DateTime.Now;
                            loginHistory.LogoutDate = DateTime.Now;
                            loginHistory.MacAddress = LoginUser.UserName;
                            loginHistory.SiteId = loggedInUser.SiteId;
                            loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            loginHistory.PCName = "Deactivated user tried to login";
                            LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection);
                        }
                    }
                }
                else
                {
                    var gUser = Users.GetUserByName(LoginUser.UserName, dbConnection);


                    if (gUser.Register != "2")
                    {
                        var loginHistory = new LoginSession();
                        loginHistory.IPAddress = localip;
                        loginHistory.LoginDate = DateTime.Now;
                        loginHistory.LogoutDate = DateTime.Now;
                        loginHistory.MacAddress = LoginUser.UserName;
                        loginHistory.SiteId = loggedInUser.SiteId;
                        loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        loginHistory.PCName = "Failed login attempt";
                        LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection);
                    }
                    else
                    {
                        LoginUser.FailureText = "User has been deactivated, Kindly contact administrator.";
                        var loginHistory = new LoginSession();
                        loginHistory.IPAddress = localip;
                        loginHistory.LoginDate = DateTime.Now;
                        loginHistory.LogoutDate = DateTime.Now;
                        loginHistory.MacAddress = LoginUser.UserName;
                        loginHistory.SiteId = loggedInUser.SiteId;
                        loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        loginHistory.PCName = "Deactivated user tried to login";
                        LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection);
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Login", "LoginUser_Authenticate", er, CommonUtility.dbConnection);
            }
        }
    }
}
