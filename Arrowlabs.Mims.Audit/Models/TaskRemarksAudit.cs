﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
  public  class TaskRemarksAudit
  {
      public int Id { get; set; }
      public string Remarks { get; set; }
      public DateTime? CreatedDate { get; set; }
      public DateTime? UpdatedDate { get; set; }
      public string CreatedBy { get; set; }
      public string UpdatedBy { get; set; }

      public BaseAudit BaseAudit { get; set; }
      public int CustomerId { get; set; }
      public int TaskId { get; set; }
      public int SiteId { get; set; }
      public int CustomerInfoId { get; set; }
      public static int InsertTaskRemarksAudit(TaskRemarksAudit taskRemarks, string auditDbConnection)
      {
          var baseAuditId = BaseAudit.InsertBaseAudit(taskRemarks.BaseAudit, auditDbConnection);

          int id = 0;

          if (taskRemarks != null)
          {
              using (var connection = new SqlConnection(auditDbConnection))
              {
                  connection.Open();
                  var cmd = new SqlCommand("InsertTaskRemarksAudit", connection);
                  cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                  cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                  cmd.Parameters["@Id"].Value = taskRemarks.Id;

                  cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                  cmd.Parameters["@Remarks"].Value = taskRemarks.Remarks;

                  cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                  cmd.Parameters["@CreatedDate"].Value = taskRemarks.CreatedDate;

                  cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                  cmd.Parameters["@UpdatedDate"].Value = taskRemarks.UpdatedDate;

                  cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                  cmd.Parameters["@CreatedBy"].Value = taskRemarks.CreatedBy;

                  cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                  cmd.Parameters["@UpdatedBy"].Value = taskRemarks.UpdatedBy;

                  cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                  cmd.Parameters["@CustomerId"].Value = taskRemarks.CustomerId;

                  cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.NVarChar));
                  cmd.Parameters["@TaskId"].Value = taskRemarks.TaskId;

                  cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                  cmd.Parameters["@SiteId"].Value = taskRemarks.SiteId;

                  cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                  cmd.Parameters["@CustomerInfoId"].Value = taskRemarks.CustomerInfoId;
                  

                  SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                  returnParameter.Direction = ParameterDirection.ReturnValue;

                  cmd.CommandText = "InsertTaskRemarksAudit";
                  cmd.CommandType = CommandType.StoredProcedure;

                  cmd.ExecuteNonQuery();

              }

          }
          return id;
      }
    }
}
