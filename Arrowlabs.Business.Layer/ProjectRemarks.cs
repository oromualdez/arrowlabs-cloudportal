﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ProjectRemarks

    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int CustomerId { get; set; }
        public string Remarks { get; set; }
        public int ProjectId { get; set; }
        public int SiteId { get; set; }

        public string FName { get; set; }
        public string LName { get; set; }

        public string CustomerUName { get; set; }

        public int CustomerInfoId { get; set; }
        public static int InsertOrUpdateProjectRemarks(ProjectRemarks project, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = project.Id;

            var action = (project.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (project != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateProjectRemarks", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = project.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = project.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = project.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = project.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = project.UpdatedDate;
                   
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = project.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = project.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = project.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@ProjectId", SqlDbType.Int));
                    cmd.Parameters["@ProjectId"].Value = project.ProjectId;


                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = project.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateProjectRemarks";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            project.Id = id;
                            var audit = new ProjectRemarksAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, project.UpdatedBy);
                            Reflection.CopyProperties(project, audit);
                            ProjectRemarksAudit.InsertProjectRemarksAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertProjectRemarksAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static ProjectRemarks GetProjectRemarksFromSqlReader(SqlDataReader reader)
        {
            var projectRemarks = new ProjectRemarks();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                projectRemarks.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["Remarks"] != DBNull.Value)
                projectRemarks.Remarks = reader["Remarks"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                projectRemarks.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["UpdatedDate"] != DBNull.Value)
                projectRemarks.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                projectRemarks.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedBy"] != DBNull.Value)
                projectRemarks.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["CustomerId"] != DBNull.Value)
                projectRemarks.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (reader["ProjectId"] != DBNull.Value)
                projectRemarks.ProjectId = Convert.ToInt32(reader["ProjectId"].ToString());

            
            if (reader["SiteId"] != DBNull.Value)
                projectRemarks.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains( "FName") && reader["FName"] != DBNull.Value)
                projectRemarks.FName = reader["FName"].ToString();

            if (columns.Contains( "LName") && reader["LName"] != DBNull.Value)
                projectRemarks.LName = reader["LName"].ToString();

            if (columns.Contains( "CustomerInfoId") & reader["CustomerInfoId"] != DBNull.Value)
                projectRemarks.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"].ToString());

            if (columns.Contains("CustomerUName"))
                projectRemarks.CustomerUName = reader["CustomerUName"].ToString();
            
            return projectRemarks;
        }
        public static List<ProjectRemarks> GetProjectRemarksByProjectId(int projectId, string dbConnection)
        {
            var projects = new List<ProjectRemarks>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetProjectRemarksByProjectId";
                sqlCommand.Parameters.Add(new SqlParameter("@ProjectId", SqlDbType.Int));
                sqlCommand.Parameters["@ProjectId"].Value = projectId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var project = GetProjectRemarksFromSqlReader(reader);
                    projects.Add(project);
                }
                connection.Close();
                reader.Close();
            }
            return projects;
        }

        public static ProjectRemarks GetProjectRemarksById(int id, string dbConnection)
        {
            ProjectRemarks project = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetProjectRemarksById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    project = GetProjectRemarksFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return project;
        }
      
      

        public static void DeleteProjectRemarksById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteProjectRemarksById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteProjectRemarksById";

                cmd.ExecuteNonQuery();
            }
        }
    }
}
