﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class WarehouseAssetCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerUName { get; set; }

        /// <summary>
        /// It will return all asset categories
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<WarehouseAssetCategory> GetAllWarehouseAssetCategory(string dbConnection)
        {
            var assetCategoryList = new List<WarehouseAssetCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllWarehouseAssetCategory", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var category = new WarehouseAssetCategory();

                    if (reader["Id"] != DBNull.Value)
                        category.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (reader["Name"] != DBNull.Value)
                        category.Name = reader["Name"].ToString();
                    if (reader["CreatedDate"] != DBNull.Value)
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    if (reader["UpdatedDate"] != DBNull.Value)
                        category.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    if (reader["CreatedBy"] != DBNull.Value)
                        category.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString().ToLower());

                    if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                        category.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                        category.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        category.CustomerUName = reader["CustomerUName"].ToString();


                    assetCategoryList.Add(category);
                }
                connection.Close();
                reader.Close();
            }
            return assetCategoryList;
        }
        public static List<WarehouseAssetCategory> GetAllWarehouseAssetCategoryBySiteId(int id, string dbConnection)
        {
            var assetCategoryList = new List<WarehouseAssetCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllWarehouseAssetCategoryBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var category = new WarehouseAssetCategory();

                    if (reader["Id"] != DBNull.Value)
                        category.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (reader["Name"] != DBNull.Value)
                        category.Name = reader["Name"].ToString();
                    if (reader["CreatedDate"] != DBNull.Value)
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    if (reader["UpdatedDate"] != DBNull.Value)
                        category.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    if (reader["CreatedBy"] != DBNull.Value)
                        category.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString().ToLower());

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        category.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        category.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        category.CustomerUName = reader["CustomerUName"].ToString();


                    assetCategoryList.Add(category);
                }
                connection.Close();
                reader.Close();
            }
            return assetCategoryList;
        }
        public static List<WarehouseAssetCategory> GetAllWarehouseAssetCategoryByLevel5(int id, string dbConnection)
        {
            var assetCategoryList = new List<WarehouseAssetCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllWarehouseAssetCategoryByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var category = new WarehouseAssetCategory();

                    if (reader["Id"] != DBNull.Value)
                        category.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (reader["Name"] != DBNull.Value)
                        category.Name = reader["Name"].ToString();
                    if (reader["CreatedDate"] != DBNull.Value)
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    if (reader["UpdatedDate"] != DBNull.Value)
                        category.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    if (reader["CreatedBy"] != DBNull.Value)
                        category.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString().ToLower());

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        category.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        category.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        category.CustomerUName = reader["CustomerUName"].ToString();


                    assetCategoryList.Add(category);
                }
                connection.Close();
                reader.Close();
            }
            return assetCategoryList;
        }
        public static List<WarehouseAssetCategory> GetAllWarehouseAssetCategoryByCustomerId(int id, string dbConnection)
        {
            var assetCategoryList = new List<WarehouseAssetCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllWarehouseAssetCategoryByCustomerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var category = new WarehouseAssetCategory();

                    if (reader["Id"] != DBNull.Value)
                        category.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (reader["Name"] != DBNull.Value)
                        category.Name = reader["Name"].ToString();
                    if (reader["CreatedDate"] != DBNull.Value)
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    if (reader["UpdatedDate"] != DBNull.Value)
                        category.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    if (reader["CreatedBy"] != DBNull.Value)
                        category.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString().ToLower());

                    if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                        category.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                        category.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        category.CustomerUName = reader["CustomerUName"].ToString();


                    assetCategoryList.Add(category);
                }
                connection.Close();
                reader.Close();
            }
            return assetCategoryList;
        }

        /// <summary>
        /// it will insert or update asset category
        /// </summary>
        /// <param name="assetCategory"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static int InsertorUpdateWarehouseAssetCategory(WarehouseAssetCategory assetCategory, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (assetCategory.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (assetCategory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateWarehouseAssetCategory", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = assetCategory.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = assetCategory.Name;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = assetCategory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = assetCategory.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.Int));
                    cmd.Parameters["@CreatedBy"].Value = assetCategory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = assetCategory.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = assetCategory.CustomerId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new WarehouseAssetCategoryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, assetCategory.UpdatedBy);
                            Reflection.CopyProperties(assetCategory, audit);
                            WarehouseAssetCategoryAudit.InsertWarehouseAssetCategoryAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertWarehouseAssetCategoryAuditAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static bool DeleteWarehouseAssetCategorybyId(int id, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true, string auditBy = "")
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteWarehouseAssetCategorybyId", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

                try
                {
                    if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection) && !string.IsNullOrEmpty(auditBy))
                    {
                        var bAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.BeforeDelete, auditBy, "DeleteAssetCategorybyId");
                        BaseAudit.InsertBaseAudit(bAudit, auditDbConnection);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Business Layer", "DeleteWarehouseAssetCategorybyId", ex, dbConnection);
                }


            }
            return true;
        }

        public static WarehouseAssetCategory GetWarehouseAssetCategoryByName(string name, string dbConnection)
        {
            WarehouseAssetCategory category = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetWarehouseAssetCategoryByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    category = new WarehouseAssetCategory();

                    if (reader["Id"] != DBNull.Value)
                        category.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (reader["Name"] != DBNull.Value)
                        category.Name = reader["Name"].ToString();
                    if (reader["CreatedDate"] != DBNull.Value)
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    if (reader["UpdatedDate"] != DBNull.Value)
                        category.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    if (reader["CreatedBy"] != DBNull.Value)
                        category.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString().ToLower());

                    if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        category.CustomerUName = reader["CustomerUName"].ToString();

                    if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                        category.SiteId = Convert.ToInt32(reader["SiteId"]);

                }
                connection.Close();
                reader.Close();
            }
            return category;
        }

        public static WarehouseAssetCategory GetWarehouseAssetCategoryById(int name, string dbConnection)
        {
            WarehouseAssetCategory category = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetWarehouseAssetCategoryById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    category = new WarehouseAssetCategory();

                    if (reader["Id"] != DBNull.Value)
                        category.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (reader["Name"] != DBNull.Value)
                        category.Name = reader["Name"].ToString();
                    if (reader["CreatedDate"] != DBNull.Value)
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    if (reader["UpdatedDate"] != DBNull.Value)
                        category.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    if (reader["CreatedBy"] != DBNull.Value)
                        category.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString().ToLower());

                    if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        category.CustomerUName = reader["CustomerUName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        category.SiteId = Convert.ToInt32(reader["SiteId"]);

                }
                connection.Close();
                reader.Close();
            }
            return category;
        }

        public static WarehouseAssetCategory GetWarehouseAssetCategoryByNameAndCId(string name, int id, string dbConnection)
        {
            WarehouseAssetCategory category = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetWarehouseAssetCategoryByNameAndCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    category = new WarehouseAssetCategory();

                    if (reader["Id"] != DBNull.Value)
                        category.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (reader["Name"] != DBNull.Value)
                        category.Name = reader["Name"].ToString();
                    if (reader["CreatedDate"] != DBNull.Value)
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    if (reader["UpdatedDate"] != DBNull.Value)
                        category.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    if (reader["CreatedBy"] != DBNull.Value)
                        category.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString().ToLower());

                    if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        category.CustomerUName = reader["CustomerUName"].ToString();

                    if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                        category.SiteId = Convert.ToInt32(reader["SiteId"]);

                }
                connection.Close();
                reader.Close();
            }
            return category;
        }

        public static WarehouseAssetCategory GetWarehouseAssetCategoryByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            WarehouseAssetCategory category = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetWarehouseAssetCategoryByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    category = new WarehouseAssetCategory();

                    if (reader["Id"] != DBNull.Value)
                        category.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (reader["Name"] != DBNull.Value)
                        category.Name = reader["Name"].ToString();
                    if (reader["CreatedDate"] != DBNull.Value)
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    if (reader["UpdatedDate"] != DBNull.Value)
                        category.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    if (reader["CreatedBy"] != DBNull.Value)
                        category.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString().ToLower());

                    if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        category.CustomerUName = reader["CustomerUName"].ToString();

                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        category.SiteId = Convert.ToInt32(reader["SiteId"]);

                }
                connection.Close();
                reader.Close();
            }
            return category;
        }
    }
}
