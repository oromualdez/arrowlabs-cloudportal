﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;


namespace Arrowlabs.Business.Layer
{
    public class OffenceCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public int AccountId { get; set; }
        public int CustomerId { get; set; }
        public int SiteId { get; set; }
        public static int InsertorUpdateOffenceCategory(OffenceCategory offenceType, string dbConnection,
    string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var id = 0;
            if (offenceType != null)
            {
                id = offenceType.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateOffenceCategory", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = offenceType.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = offenceType.Name;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = offenceType.Description;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = offenceType.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = offenceType.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = offenceType.CreatedBy.ToLower();

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = offenceType.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = offenceType.SiteId;
                     
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateOffenceCategory";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            offenceType.Id = id;
                            var audit = new OffenceCategoryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, offenceType.UpdatedBy);
                            Reflection.CopyProperties(offenceType, audit);
                            OffenceCategoryAudit.InsertorUpdateOffenceCategoryAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateOffenceCategoryAudit", ex, dbConnection);
                    }

                }
            }
            return id;
        }

        public static List<OffenceCategory> GetAllOffenceCategory(string dbConnection)
        {
            var offenceTypes = new List<OffenceCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceCategory", connection);


                sqlCommand.CommandText = "GetAllOffenceCategory";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offenceType = new OffenceCategory();

                    offenceType.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (columns.Contains( "CustomerId"))
                    offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    offenceType.Name = reader["Name"].ToString();
                    offenceType.Description = reader["Description"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("SiteId"))
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    //if (columns.Contains( "AccountId"))
                    //    offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                     
                    offenceTypes.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offenceTypes;
        }

        public static List<OffenceCategory> GetAllOffenceCategoryByCId(int id,string dbConnection)
        {
            var offenceTypes = new List<OffenceCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceCategoryByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllOffenceCategoryByCId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offenceType = new OffenceCategory();

                    offenceType.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (columns.Contains("CustomerId"))
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    offenceType.Name = reader["Name"].ToString();
                    offenceType.Description = reader["Description"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("SiteId"))
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    //if (columns.Contains( "AccountId"))
                    //    offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);

                    offenceTypes.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offenceTypes;
        }

        public static List<OffenceCategory> GetAllOffenceCategoryBySiteId(int id, string dbConnection)
        {
            var offenceTypes = new List<OffenceCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceCategoryBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllOffenceCategoryBySiteId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offenceType = new OffenceCategory();

                    offenceType.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (columns.Contains("CustomerId"))
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

                    if (columns.Contains("SiteId"))
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

                    offenceType.Name = reader["Name"].ToString();
                    offenceType.Description = reader["Description"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    //if (columns.Contains( "AccountId"))
                    //    offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);

                    offenceTypes.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offenceTypes;
        }

        public static List<OffenceCategory> GetAllOffenceCategoryByLevel5(int id, string dbConnection)
        {
            var offenceTypes = new List<OffenceCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOffenceCategoryByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllOffenceCategoryByLevel5";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var offenceType = new OffenceCategory();

                    offenceType.Id = Convert.ToInt32(reader["Id"].ToString());
                    if (columns.Contains("CustomerId"))
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

                    if (columns.Contains("SiteId"))
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

                    offenceType.Name = reader["Name"].ToString();
                    offenceType.Description = reader["Description"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    //if (columns.Contains( "AccountId"))
                    //    offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);

                    offenceTypes.Add(offenceType);
                }
                connection.Close();
                reader.Close();
            }
            return offenceTypes;
        }

        public static OffenceCategory GetOffenceCategoryByName(string name, string dbConnection)
        {
            OffenceCategory offenceType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceCategoryByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offenceType = new OffenceCategory();
                    offenceType.Id = Convert.ToInt32(reader["Id"].ToString());
                    offenceType.Name = reader["Name"].ToString();
                    offenceType.Description = reader["Description"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "CustomerId"))
                    offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    if (columns.Contains("SiteId"))
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    //if (columns.Contains( "AccountId"))
                    //    offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                     

                }
                connection.Close();
                reader.Close();
            }
            return offenceType;
        }

        public static OffenceCategory GetOffenceCategoryById(int id, string dbConnection)
        {
            OffenceCategory offenceType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceCategoryById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offenceType = new OffenceCategory();
                    offenceType.Id = Convert.ToInt32(reader["Id"].ToString());
                    offenceType.Name = reader["Name"].ToString();
                    offenceType.Description = reader["Description"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("CustomerId"))
                        offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    if (columns.Contains("SiteId"))
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    //if (columns.Contains( "AccountId"))
                    //    offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);


                }
                connection.Close();
                reader.Close();
            }
            return offenceType;
        }

        public static OffenceCategory GetOffenceCategoryByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            OffenceCategory offenceType = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetOffenceCategoryByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    offenceType = new OffenceCategory();
                    offenceType.Id = Convert.ToInt32(reader["Id"].ToString());
                    offenceType.Name = reader["Name"].ToString();
                    offenceType.Description = reader["Description"].ToString();
                    offenceType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    offenceType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    offenceType.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "CustomerId"))
                    offenceType.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    if (columns.Contains("SiteId"))
                        offenceType.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    //if (columns.Contains( "AccountId"))
                    //    offenceType.AccountId = Convert.ToInt32(reader["AccountId"]);
                     

                }
                connection.Close();
                reader.Close();
            }
            return offenceType;
        }

        

        public static bool DeleteOffenceCategoryById(int colorCode, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteOffenceCategoryById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = colorCode;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteOffenceCategoryById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
