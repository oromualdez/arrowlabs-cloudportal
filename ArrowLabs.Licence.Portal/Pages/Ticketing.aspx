﻿<%@ Page EnableEventValidation="false"  Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ticketing.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.Ticketing" %>
<%@ Register TagPrefix="NewTask" TagName="MyNewTask" Src="~/Controls/Modals/NewTask.ascx" %>
<%@ Register TagPrefix="NewTicket" TagName="MyNewTicket" Src="~/Controls/Modals/NewTicket.ascx" %>
<%@ Register TagPrefix="TicketingCard" TagName="MyTicketingCard" Src="~/Controls/Modals/TicketingCard.ascx" %>
<%@ Register TagPrefix="AddOnTaskCard" TagName="MyAddOnTaskCard" Src="~/Controls/Modals/AddOnTaskCard.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
         <style>
                                           .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 172px;
        margin-top:6px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 185px;
        height:23px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
                                           .pac-cardticket {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-containerticket {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controlsticket {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controlsticket label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-inputticket {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 172px;
        margin-top:6px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 185px;
        height:23px;
      }

      #pac-inputticket:focus {
        border-color: #4d90fe;
      }

             #newDocument {
    overflow-y:scroll;
} 
                          #newTicketDocument {
    overflow-y:scroll;
} 
                                       #ticketingViewCard {
    overflow-y:scroll;
} 
                                       #taskDocument {
    overflow-y:scroll;
} 
                                       
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });

        var width = 1;
        function move() {
            var elem = document.getElementById("myBar");
            width = 1;
            var id = setInterval(frame, 200);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        function moveTick() {
            var elem = document.getElementById("myBarTick");
            width = 1;
            var id = setInterval(frame, 200);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        var loggedinUsername = '<%=senderName2%>';
        var mapIncidentLocation;
        var myMarkersIncidentLocation = new Array();
        var infoWindowIncidentLocation;
        var sourceLat = 25.2049808502197;
        var sourceLon = 55.2707939147949;
        var mapUrlLocation;
        var locationAllowed = false;
        var divArray = new Array();

        var rotate_factor = 0;
        var rotated = false; 

        //User-profile
        function changePassword() {
            try {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;
                var isErr = false;
                if (!isErr) { 
                    if (!letterGood) {
                        showAlert('Password does not contain letter');
                        isErr = true;
                    }
                    if (!isErr) {
                        if (!capitalGood) {
                            showAlert('Password does not contain capital letter');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!numGood) {
                            showAlert('Password does not contain number');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!lengthGood) {
                            showAlert('Password length not enough');
                            isErr = true;
                        }
                    }
                }
                if (!isErr) {
                    if (newPw == confPw && newPw != "" && confPw != "") {
                        jQuery.ajax({
                            type: "POST",
                            url: "Ticketing.aspx/changePW",
                            data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d != "LOGOUT") {
                                    jQuery('#changePasswordModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                    jQuery('#successfulDispatch').modal('show');
                                    document.getElementById("newPwInput").value = "";
                                    document.getElementById("confirmPwInput").value = "";
                                    document.getElementById("oldPwInput").value = confPw;
                                }
                                else {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        showAlert("Kindly match new password with confirm password.")
                    }
                }
        }
        catch (ex)
        { showAlert('Error 60: Problem loading page element.-' + ex) }
    }
    function editUnlock() {
        document.getElementById("profilePhoneNumberDIV").style.display = "none";
        document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
        document.getElementById("editProfileA").style.display = "none";
        document.getElementById("saveProfileA").style.display = "block";
        document.getElementById("profileEmailAddDIV").style.display = "none";
        document.getElementById("profileEmailAddEditDIV").style.display = "block";
        document.getElementById("userFullnameSpanDIV").style.display = "none";
        document.getElementById("userFullnameSpanEditDIV").style.display = "block";
        if (document.getElementById('profileRoleName').innerHTML == "User") {
            document.getElementById("superviserInfoDIV").style.display = "none";
            document.getElementById("managerInfoDIV").style.display = "block";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
            document.getElementById("superviserInfoDIV").style.display = "none";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "block";
        }
    }
    function editJustLock() {
        document.getElementById("profilePhoneNumberDIV").style.display = "block";
        document.getElementById("userFullnameSpanEditDIV").style.display = "none";
        document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
        document.getElementById("editProfileA").style.display = "block";
        document.getElementById("saveProfileA").style.display = "none";
        document.getElementById("profileEmailAddDIV").style.display = "block";
        document.getElementById("profileEmailAddEditDIV").style.display = "none";
        document.getElementById("userFullnameSpanDIV").style.display = "block";
        document.getElementById("superviserInfoDIV").style.display = "block";
        document.getElementById("managerInfoDIV").style.display = "none";
        document.getElementById("dirInfoDIV").style.display = "none";
    }
    function editLock() {
        document.getElementById("profilePhoneNumberDIV").style.display = "block";
        document.getElementById("userFullnameSpanEditDIV").style.display = "none";
        document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
        document.getElementById("editProfileA").style.display = "block";
        document.getElementById("saveProfileA").style.display = "none";
        document.getElementById("profileEmailAddDIV").style.display = "block";
        document.getElementById("profileEmailAddEditDIV").style.display = "none";
        document.getElementById("userFullnameSpanDIV").style.display = "block";
        document.getElementById("superviserInfoDIV").style.display = "block";
        document.getElementById("managerInfoDIV").style.display = "none";
        document.getElementById("dirInfoDIV").style.display = "none";
        var role = document.getElementById('UserRoleSelector').value;
        var roleid = 0;
        var supervisor = 0;
        var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
        if (retVal > 0) {
            assignUserProfileData();
        }
        else {
            showAlert("Error 61: Problem occured while trying to update user profile.")
        }

    }
    function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
        var output = 0;
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/addUserProfile",
            data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                output = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
        function saveTZ() {
            var scountr = $("#countrySelect option:selected").text();
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/saveTZ",
                data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                        jQuery('#successfulDispatch').modal('show');
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    function getserverInfo() {
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getServerData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById('mobileRemaining').value = data.d[9];
                    document.getElementById('mobileTotal').value = data.d[10];
                    document.getElementById('mobileUsed').value = data.d[11];

                    document.getElementById("countrySelect").value = data.d[28];
                    jQuery('#countrySelect').selectpicker('val', data.d[28]);

                    document.getElementById('surveillanceCheck').checked = false;
                    document.getElementById('notificationCheck').checked = false;
                    document.getElementById('locationCheck').checked = false;
                    document.getElementById('ticketingCheck').checked = false;
                    document.getElementById('taskCheck').checked = false;
                    document.getElementById('incidentCheck').checked = false;
                    document.getElementById('warehouseCheck').checked = false;
                    document.getElementById('chatCheck').checked = false;
                    document.getElementById('collaborationCheck').checked = false;
                    document.getElementById('lfCheck').checked = false;
                    document.getElementById('dutyrosterCheck').checked = false;
                    document.getElementById('postorderCheck').checked = false;
                    document.getElementById('verificationCheck').checked = false;
                    document.getElementById('requestCheck').checked = false;
                    document.getElementById('dispatchCheck').checked = false;
                    document.getElementById('activityCheck').checked = false;

                    if (data.d[12] == "true")
                        document.getElementById('surveillanceCheck').checked = true;
                    if (data.d[13] == "true")
                        document.getElementById('notificationCheck').checked = true;
                    if (data.d[14] == "true")
                        document.getElementById('locationCheck').checked = true;
                    if (data.d[15] == "true")
                        document.getElementById('ticketingCheck').checked = true;
                    if (data.d[16] == "true")
                        document.getElementById('taskCheck').checked = true;
                    if (data.d[17] == "true")
                        document.getElementById('incidentCheck').checked = true;
                    if (data.d[18] == "true")
                        document.getElementById('warehouseCheck').checked = true;
                    if (data.d[19] == "true")
                        document.getElementById('chatCheck').checked = true;
                    if (data.d[20] == "true")
                        document.getElementById('collaborationCheck').checked = true;
                    if (data.d[21] == "true")
                        document.getElementById('lfCheck').checked = true;
                    if (data.d[22] == "true")
                        document.getElementById('dutyrosterCheck').checked = true;
                    if (data.d[23] == "true")
                        document.getElementById('postorderCheck').checked = true;
                    if (data.d[24] == "true")
                        document.getElementById('verificationCheck').checked = true;
                    if (data.d[25] == "true")
                        document.getElementById('requestCheck').checked = true;
                    if (data.d[26] == "true")
                        document.getElementById('dispatchCheck').checked = true;
                    if (data.d[27] == "true")
                        document.getElementById('activityCheck').checked = true;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function assignUserProfileData() {
        try {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/getUserProfileData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        try {
                            document.getElementById('containerDiv2').style.display = "none";
                            document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                            document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                            document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                            document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                            document.getElementById('profileLastLocation').innerHTML = data.d[4];
                            document.getElementById('profileRoleName').innerHTML = data.d[5];
                            document.getElementById('profileManagerName').innerHTML = data.d[6];
                            document.getElementById('userStatusSpan').innerHTML = data.d[8];

                            if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                document.getElementById('containerDiv2').style.display = "block";
                                document.getElementById('defaultGenderDiv').style.display = "none";
                                getserverInfo();
                            }

                            var el = document.getElementById('userStatusIconSpan');
                            if (el) {
                                el.className = data.d[9];
                            }
                            document.getElementById('userprofileImgSrc').src = data.d[10];
                            document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                            document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                            document.getElementById('userFirstnameSpan').value = data.d[13];
                            document.getElementById('userLastnameSpan').value = data.d[14];
                            document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                            document.getElementById('profileEmailAddEdit').value = data.d[3];

                            document.getElementById('oldPwInput').value = data.d[16];

                            document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                            document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                            document.getElementById('profileGender').innerHTML = data.d[20];

                            if (document.getElementById('profileRoleName').innerHTML != "Level 7") {
                                document.getElementById('deviceTypesDiv').innerHTML = "<i class='fa fa-mobile fa-2x mr-2x'></i><i style='color:lime;' class='fa fa-laptop fa-2x mr-2x'></i>";//data.d[11];

                            }
                        }
                        catch (err) { alert(err) }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (err)
        { alert('Error 60: Problem loading page element.-' + err) }
    }
    function forceLogout() {
        document.getElementById('<%= closingbtn.ClientID %>').click();
        }
        var viewpopupUP = false;
        function showTicketCardModal(id) {
            viewpopupUP = true;
            rowchoice(id);
        }
        function closeModal() {
            if (viewpopupUP) {
                window.close();
            }
            else {
                location.reload();
            }
            showLoader();
        }
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }
        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
        var firstOpenNewTask = false;
        var firstOpenNewTicket = false;
        jQuery(document).ready(function () {
            try { infoWindowLocation = new google.maps.InfoWindow(); infoWindowTaskLocation = new google.maps.InfoWindow(); infoWindowIncidentLocation = new google.maps.InfoWindow(); } catch (err) { }
            jQuery('#ticketingViewCard').on('shown.bs.modal', function () {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getIncidentLocationData",
                    data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var obj = jQuery.parseJSON(data.d)
                            getTicketLocationMarkers(obj);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            });
            jQuery('#newDocument').on('shown.bs.modal', function () {
                if (firstOpenNewTask == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Ticketing.aspx/getLocationData",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                if (data.d != "]") {
                                    var obj = jQuery.parseJSON(data.d)
                                    getLocationMarkers(obj);
                                }
                                else {
                                    getLocationNoMarkers(obj);
                                }
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                    firstOpenNewTask = true;
                }
            });

            jQuery('#newTicketDocument').on('shown.bs.modal', function () {
                document.getElementById("ticketItemsList").innerHTML = "";
                clearTicketInfo();
                if (firstOpenNewTicket == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Ticketing.aspx/getLocationData",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                if (data.d != "]") {
                                    var obj = jQuery.parseJSON(data.d)
                                    getLocationMarkersTicket(obj);
                                }
                                else {
                                    getLocationNoMarkersTicket(obj);
                                }
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                    firstOpenNewTicket = true;

                    var myDropzoneNew = new Dropzone("#dz-test", {
                        init: function () {
                            //You can do this 
                            jQuery('#clearTicketInfoID').on("click", function (e) {
                                myDropzoneNew.removeFile(ff);
                                document.getElementById("imagePostAttachment").text = "";
                            });
                        }
                    });
                    myDropzoneNew.on("addedfile", function (file) {
                        /* Maybe display some more file information on your page */
                        file.previewElement.addEventListener("click", function () {
                            myDropzoneNew.removeFile(file);
                            document.getElementById("imagePostAttachment").text = '';
                        });
                        if (typeof document.getElementById("imagePostAttachment").text === 'undefined' || document.getElementById("imagePostAttachment").text == '') {
                           // if (file.type != "image/jpeg" && file.type != "image/png" && file.type != "application/pdf") {
                           //     showAlert("Kindly provided a JPEG , PNG Image or PDF for upload");
                            //    this.removeFile(file);
                            //}
                            //else {
                                ff = file;
                                var data = new FormData();
                                data.append(file.name, file);
                                jQuery.ajax({
                                    url: "../Handlers/MobileIncidentUpload.ashx",
                                    type: "POST",
                                    data: data,
                                    contentType: false,
                                    processData: false,
                                    success: function (result) {
                                        document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|");
                                    },
                                    error: function (err) {
                                        showError("Session timeout. Kindly login again.");
                                    }
                                })
                            //}
                        }
                        else {
                            showAlert("You can only add one image at a time kindly delete previous image");
                            this.removeFile(file);
                        }
                    });
                }
            });

            jQuery('#taskDocument').on('shown.bs.modal', function () {
                if (firstpresstask == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "Ticketing.aspx/getTaskLocationData",
                        data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                var obj = $.parseJSON(data.d)
                                getTaskLocationMarkers(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                    firstpresstask = true;
                }
                else {
                    jQuery.ajax({
                        type: "POST",
                        url: "Ticketing.aspx/getTaskLocationData",
                        data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                var obj = $.parseJSON(data.d)
                                updateTaskMarker(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            });
            try {
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    if ($(e.target).attr('href') == "#home-tab" || $(e.target).attr('href') == "#subtab1-tab" || $(e.target).attr('href') == "#subtab2-tab")
                        localStorage.setItem('activeTabTick', $(e.target).attr('href'));
                    //alert($(e.target).attr('href')+'----------NOW Selected');
                });
                var activeTab = localStorage.getItem('activeTabTick');
                if (activeTab) {
                    //alert(activeTab +'--------Selected');
                    //$('#myTab a[href="' + activeTab + '"]').tab('show'); 
                    jQuery('a[data-toggle="tab"][href="' + activeTab + '"]').tab('show');
                }
                localStorage.removeItem("activeTabActivity");
                localStorage.removeItem("activeTabDev");
                localStorage.removeItem("activeTabInci");
                localStorage.removeItem("activeTabMessage");
                localStorage.removeItem("activeTabTask");
                localStorage.removeItem("activeTabUB");
                localStorage.removeItem("activeTabVer");
                localStorage.removeItem("activeTabLost");

                $('input[type=password]').keyup(function () {
                    // keyup event code here
                    var pswd = $(this).val();
                    if (pswd.length < 8) {
                        $('#length').removeClass('valid').addClass('invalid');
                        lengthGood = false;
                    } else {
                        $('#length').removeClass('invalid').addClass('valid');
                        lengthGood = true;
                    }
                    //validate letter
                    if (pswd.match(/[A-z]/)) {
                        $('#letter').removeClass('invalid').addClass('valid');
                        letterGood = true;

                    } else {
                        $('#letter').removeClass('valid').addClass('invalid');
                        letterGood = false;
                    }

                    //validate capital letter
                    if (pswd.match(/[A-Z]/)) {
                        $('#capital').removeClass('invalid').addClass('valid');
                        capitalGood = true;
                    } else {
                        $('#capital').removeClass('valid').addClass('invalid');

                        capitalGood = false;
                    }

                    //validate number
                    if (pswd.match(/\d/)) {
                        $('#number').removeClass('invalid').addClass('valid');
                        numGood = true;

                    } else {
                        $('#number').removeClass('valid').addClass('invalid');
                        numGood = false;
                    }
                });
                $('input[type=password]').focus(function () {
                    // focus code here
                    $('#pswd_info').show();
                });
                $('input[type=password]').blur(function () {
                    // blur code here
                    $('#pswd_info').hide();
                });

            } catch (err)
            { alert(err); }

            addrowtoTable1();
            addrowtoTable2();
            addrowtoTable3();
            assignUserTableData();
            addrowtoTableCat();
            addrowtoTableResolved();
            //addrowtoTable4();
            //addrowtoTable5();
            //addrowtoTablePlateCode(); handleticket()
            //addrowtoTablePlateSource();
            if (viewpopupUP) {
                jQuery('#ticketingViewCard').modal('show');
            } 
            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getProjectListByCustomerId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#projectlst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                         );
                    }
                }
            });

            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getCustomerByProjectId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#customerslst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                         );
                    }
                }
            });

            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getCustomerByProjectId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#ticketcustomerslst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                         );
                    }
                }
            });

            var myDropzonePost = new Dropzone("#dz-postticket");
            myDropzonePost.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */
                width = 1;
                moveTick();
                document.getElementById("pdfloadingAcceptTick").style.display = "block";
                document.getElementById("gnNoteTick").innerHTML = "ATTACHING FILE";
                //if (file.type != "image/jpeg" && file.type != "image/png" && file.type != "application/pdf") {
                //    showAlert("Kindly provided a JPEG , PNG Image or PDF for upload");
                //    this.removeFile(file);
                //    document.getElementById("pdfloadingAccept").style.display = "none";
                //}
                //else {
                    var data = new FormData();
                    data.append(file.name, file);
                    jQuery.ajax({
                        url: "../Handlers/MobileIncidentUpload.ashx",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|")
                            jQuery.ajax({
                                type: "POST",
                                url: "Ticketing.aspx/attachFileToTicket",
                                data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','filepath':'" + document.getElementById("imagePostAttachment").text + "','uname':'" + loggedinUsername + "'}",
                                async: false,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    if (data.d == "LOGOUT") {
                                        showError("Session has expired. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    }
                                    else {
                                        showAlert(data.d);
                                        rowchoice(document.getElementById('rowidChoiceTicket').value);
                                        document.getElementById("pdfloadingAcceptTick").style.display = "none";
                                    }
                                },
                                error: function () {
                                    showError("Session timeout. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            });
                        },
                        error: function (err) {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    })
                //}
            });

        });
        function clearTicketInfo() {
            jQuery("#MainContent_NewTicketControl_ticketCategorySelect").val(0);

            jQuery("#MainContent_NewTicketControl_ticketlocationSelect").val(0);

            jQuery("#ticketcustomerslst").val(0);

            jQuery("#ticketcontractslst").val(0); 

            jQuery("#ticketTypeSelect").val(0);
            jQuery("#ticketItemSelect").val(0);
            document.getElementById('tbNewTicketComment').value = "";
            jQuery("#ticketlocationSelect").val(0); 
            document.getElementById('tbNewTicketLatitude').value = "";
            document.getElementById('tbNewTicketLongitude').value = "";
            document.getElementById("imagePostAttachment").text = "";
        }

        function updateStatus(status) {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/UpdateTicketStatus",
                data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','status':'" + status + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showAlert(data.d);
<%--                        if (status == "START") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";

                            if ('<%=tskOptionView%>' == "block")
                                document.getElementById("liTicketTask").style.display = "block";

                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "block";
                        }
                        else if (status == "END") {
                            handledClickTicket();
                            document.getElementById("liTicketBack").style.display = "none";
                            document.getElementById("liTicketTask").style.display = "none";
                            document.getElementById("liTicketStart").style.display = "none";
                            document.getElementById("liTicketEnd").style.display = "none";
                        }--%>
                        rowchoice(document.getElementById('rowidChoiceTicket').value);
                        addrowtoTable1();
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        var ff;
        var myTraceBackMarkers = new Array();
        var updatepoligon;
        var tracebackpoligon;
        function tracebackOnFilter(duration,t) {

            if (duration == "1") {
                var el = document.getElementById('taskfilter1');
                if (el) {
                    if (el.className == 'fa fa-check-square-o') {
                        el.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el.className = 'fa fa-check-square-o';
                }
                var ell2 = document.getElementById('taskfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('taskfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('taskfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "5") {
                var el2 = document.getElementById('taskfilter2');
                if (el2) {
                    if (el2.className == 'fa fa-check-square-o') {
                        el2.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el2.className = 'fa fa-check-square-o';
                }
                var ell = document.getElementById('taskfilter1');
                if (ell) {
                    ell.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('taskfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('taskfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "30") {
                var el3 = document.getElementById('taskfilter3');
                if (el3) {
                    if (el3.className == 'fa fa-check-square-o') {
                        el3.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el3.className = 'fa fa-check-square-o';
                }
                var ell = document.getElementById('taskfilter1');
                if (ell) {
                    ell.className = 'fa fa-square-o';
                }
                var ell2 = document.getElementById('taskfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell4 = document.getElementById('taskfilter4');
                if (ell4) {
                    ell4.className = 'fa fa-square-o';
                }
            }
            else if (duration == "60") {
                var el4 = document.getElementById('taskfilter4');
                if (el4) {
                    if (el4.className == 'fa fa-check-square-o') {
                        el4.className = 'fa fa-square-o';
                        duration = "0";
                    }
                    else
                        el4.className = 'fa fa-check-square-o';
                }
                var ell = document.getElementById('taskfilter1');
                if (ell) {
                    ell.className = 'fa fa-square-o';
                }
                var ell2 = document.getElementById('taskfilter2');
                if (ell2) {
                    ell2.className = 'fa fa-square-o';
                }
                var ell3 = document.getElementById('taskfilter3');
                if (ell3) {
                    ell3.className = 'fa fa-square-o';
                }
            }

            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getTracebackLocationByDurationData",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','duration':'" + duration + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d)
                        updateTaskMarker(obj);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function tracebackOn(t) {
            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getTracebackLocationData",
                data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        var obj = jQuery.parseJSON(data.d)
                        if (obj.length > 1) {
                            updateTaskMarker(obj);
                            document.getElementById("rowtasktracebackUser").style.display = "block";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function updateTaskMarker(obj) {
            try {
                var first = true;
                var poligonCoords = [];

                for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                    if (myTraceBackMarkers[i] != null) {
                        myTraceBackMarkers[i].setMap(null);
                    }
                }

                if (typeof tracebackpoligon === 'undefined') {
                    // your code here.
                }
                else {
                    tracebackpoligon.setMap(null);
                }

                if (typeof myMarkersTasksLocation["Pending"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Pending"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["Completed"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Completed"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["Rejected"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Rejected"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["Accepted"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Accepted"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["InProgress"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["InProgress"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["Cancelled"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Cancelled"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["OnRoute"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["OnRoute"].setMap(null);
                }
                var tbcounter = 0;
                for (var i = 0; i < obj.length; i++) {
                    var contentString = '<div id="content">' + obj[i].Username + '<br/></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    if (obj[i].Username == "Pending") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "InProgress") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "OnRoute") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "Completed") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "Accepted") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].State == "RED") {
                        if (first) {
                            first = false;
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);

                        }
                        else {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                        }
                    }
                    else {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                }
                if (poligonCoords.length > 0) {
                    tracebackpoligon = new google.maps.Polyline({
                        path: poligonCoords,
                        geodesic: true,
                        strokeColor: '#1b93c0',
                        strokeOpacity: 1.0,
                        strokeWeight: 7,
                        icons: [{
                            icon: iconsetngs,
                            offset: '100%'
                        }]
                    });
                    tracebackpoligon.setMap(mapTaskLocation);
                    animateCircle(tracebackpoligon);
                }
            }
            catch (err) {
                //showAlert(err);
            }
        }
        function getTaskLocationMarkers(obj) {
            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;

            var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 15,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapTaskLocation = new google.maps.Map(document.getElementById("taskmap_canvasIncidentLocation"), mapOptions);

            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div id="content">' + obj[i].Username +
                '<br/></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });

                if (obj[i].Username == "Pending") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                }
                else if (obj[i].Username == "InProgress") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                }
                else if (obj[i].Username == "Completed") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                }
                else if (obj[i].Username == "Accepted") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                }
                else if (obj[i].Username == "OnRoute") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
                }
                else {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                }
                myMarkersTasksLocation[obj[i].Username] = marker;
                createInfoWindowTaskLocation(marker, contentString);
            }
        }
        function createInfoWindowTaskLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindowTaskLocation.setContent(popupContent);
                infoWindowTaskLocation.open(mapTaskLocation, this);
            });
        }
        function initializetables() {

        }
        var isMyself = false;
        function assignMySelf() {
            if (!isMyself) {
                document.getElementById("assigneeDIV").style.display = "none";
                isMyself = true;
            }
            else {
                document.getElementById("assigneeDIV").style.display = "block";
                isMyself = false;
            }
        }
        var isLink = false;
        function sendTask() {
            try {

                var name = document.getElementById("tbNewTaskName").value;

                var linkId = '0';
                if (isLink) {
                    linkId = document.getElementById("rowChoiceTasks").value;
                }
                var ticketId = '0';
                ticketId = document.getElementById('rowidChoiceTicket').value;


                if (!isEmptyOrSpaces(name)) {
                    if (!isSpecialChar(name)) {
                        var isErr = false;
                        var desc = document.getElementById("tbNewDescription").value;
                        if (!isEmptyOrSpaces(desc)) {
                            if (isSpecialChar(desc)) {
                                isErr = true;
                            }
                        }
                        var longi = document.getElementById("tbNewLongitude").value;
                        var lati = document.getElementById("tbNewLatitude").value;


                        if (!isEmptyOrSpaces(longi)) {
                            if (isInvalidDouble(longi)) {
                                isErr = true;
                            }
                        }

                        if (!isEmptyOrSpaces(longi)) {
                            if (isInvalidDouble(lati)) {
                                isErr = true;
                            }
                        }
                        var tasktype = document.getElementById("MainContent_NewTaskControl_taskTypeSelect").value;
                        var assignType = document.getElementById("taskSelectAssigneeType").value;
                        var assignId = "0";
                        var assigneeName = "";
                        var isMultiple = false;
                        if (assignType == "User") {
                            assignId = document.getElementById("MainContent_NewTaskControl_usersearchSelect").value;
                            assigneeName = getSelectedText('MainContent_NewTaskControl_usersearchSelect');
                        }
                        else if (assignType == "Group") {
                            assignId = document.getElementById("MainContent_NewTaskControl_groupsearchSelect").value;
                            assigneeName = getSelectedText('MainContent_NewTaskControl_groupsearchSelect');
                        }
                        else if (assignType == "Multiple") {
                            isMultiple = true;
                        }
                        if (!isMultiple && assignId == "0" && document.getElementById('myselfCheck').checked == false) {
                            isErr = true;
                            showAlert("Kindly provide assignee for the task");
                        }
                        var startdate = document.getElementById("newtaskCalendar").value;
                        var startT = document.getElementById("newtaskTime").value + ":" + document.getElementById("newtaskTimeMinute").value;
                        var s1 = startdate + " " + startT;
                        var d1 = new Date(s1);

                        var q = new Date();
                        var m = q.getMonth();
                        var d = q.getDate();
                        var y = q.getFullYear();

                        var date = new Date(y, m, d);
                        var dd = dates.compare(date, d1);

                        if (dd == 1) {
                            isErr = true;
                            showAlert("Date should be set to today or future date.")
                        }

                        var priority = document.getElementById("prioritySelect").value;
                        var checklistid = document.getElementById("MainContent_NewTaskControl_checklistSelect").value;
                        var lbrecurring = document.getElementById("newrecurringSelect").value;
                        var issig = document.getElementById("requiresignatureCheck").checked;
                        if (!checklistid) {
                            isErr = true;
                            showAlert("Kindly provide checklist for the task");
                        }
                        if (!isErr) {
                            var recurring = false;
                            if (lbrecurring == "Recurring") {
                                recurring = false;
                            }
                            else {
                                recurring = true;
                                showAlert("Starting Task Creation for recurring this might take some time. Kindly click ok and wait for success message.")
                            }
                            if (!isMultiple) {
                                $.ajax({
                                    type: "POST",
                                    url: "Ticketing.aspx/InsertTaskOnly",
                                    data: "{'name':'" + name
                                        + "','desc':'" + desc
                                        + "','assignType':'" + assignType
                                        + "','assignId':'" + assignId
                                        + "','assigneeName':'" + assigneeName
                                        + "','longi':'" + longi
                                        + "','lati':'" + lati
                                        + "','startdate':'" + startdate
                                        + "','priority':'" + priority
                                        + "','checklistid':'" + checklistid
                                        + "','recurring':'" + recurring
                                        + "','lbrecurring':'" + lbrecurring
                                        + "','uname':'" + loggedinUsername
                                        + "','tasktype':'" + tasktype
                                        + "','linkId':'" + linkId
                                        + "','projectid':'" + document.getElementById('projectlst').value
                                        + "','contractId':'" + document.getElementById('contractslst').value
                                        + "','customerId':'" + document.getElementById('customerslst').value
                                        + "','startT':'" + startT
                                        + "','issig':'" + issig
                                        + "','ticketId':'" + ticketId 
                                        + "','myself':'" + document.getElementById('myselfCheck').checked
                                        + "'}",

                                    async: false,
                                    dataType: "json",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        if (data.d[0] == "SUCCESS") {
                                            hideLoader();


                                            var ss1 = startdate + " 00:00";
                                            var dd1 = new Date(ss1);

                                            var q1 = new Date();
                                            var m2 = q1.getMonth();
                                            var d2 = q1.getDate();
                                            var y2 = q1.getFullYear();

                                            var date12 = new Date(y2, m2, d2);
                                            var dd12 = dates.compare(date12, dd1);

                                            if (dd12 == 0) {
                                                if (assignType == "User") {
                                                    chat.server.sendtoUser(btoa(data.d[1]), "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi, assignId);
                                                }
                                                else if (assignType == "Group") {
                                                    chat.server.sendtoGroup(btoa(data.d[1]), assignId, "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi);
                                                }
                                            }

                                            jQuery('#newDocument').modal('hide');
                                            showAlert("Successfully sent task");
                                            rowchoice(document.getElementById('rowidChoiceTicket').value);
                                            jQuery('#ticketingViewCard').modal('show');
                                            newTaskClick();
                                            //document.getElementById('successincidentScenario').innerHTML = "Task has been sent.";
                                            //jQuery('#successfulDispatch').modal('show');
                                        }
                                        else if (data.d[0] == "LOGOUT") {
                                            showError("Session has expired. Kindly login again.");
                                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                        }
                                        else {
                                            hideLoader();
                                            showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                                        }
                                    },
                                    error: function () {
                                        showError("Session timeout. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    }
                                });
                            }
                            else {
                                var list = document.getElementById("sendToListBox");
                                var noError = false;
                                if (list.length > 0) {
                                    for (i = 0; i < list.length; i++) {
                                        var split = list.options[i].text.split('+');
                                        var aType = "Multiple";
                                        var aName = "empty";
                                        if (split.length > 0) {
                                            aType = split[1];
                                            aName = split[0];
                                        }
                                        $.ajax({
                                            type: "POST",
                                            url: "Ticketing.aspx/InsertTaskOnly",
                                            data: "{'name':'" + name
                                        + "','desc':'" + desc
                                        + "','assignType':'" + aType
                                        + "','assignId':'" + list.options[i].value
                                        + "','assigneeName':'" + aName
                                        + "','longi':'" + longi
                                        + "','lati':'" + lati
                                        + "','startdate':'" + startdate
                                        + "','priority':'" + priority
                                        + "','checklistid':'" + checklistid
                                        + "','recurring':'" + recurring
                                        + "','lbrecurring':'" + lbrecurring
                                        + "','uname':'" + loggedinUsername
                                        + "','tasktype':'" + tasktype
                                        + "','linkId':'" + linkId
                                        + "','projectid':'" + document.getElementById('projectlst').value
                                        + "','contractId':'" + document.getElementById('contractslst').value
                                        + "','customerId':'" + document.getElementById('customerslst').value
                                        + "','startT':'" + startT
                                         + "','issig':'" + issig
                                         + "','ticketId':'" + ticketId
                                         + "','myself':'" + document.getElementById('myselfCheck').checked
                                         + "'}",
                                            async: false,
                                            dataType: "json",
                                            contentType: "application/json; charset=utf-8",
                                            success: function (data) {
                                                if (data.d[0] == "LOGOUT") {
                                                    showError("Session has expired. Kindly login again.");
                                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                                } else if (data.d[0] == "SUCCESS") {

                                                    var ss1 = startdate + " 00:00";
                                                    var dd1 = new Date(ss1);

                                                    var q1 = new Date();
                                                    var m2 = q1.getMonth();
                                                    var d2 = q1.getDate();
                                                    var y2 = q1.getFullYear();

                                                    var date12 = new Date(y2, m2, d2);
                                                    var dd12 = dates.compare(date12, dd1);

                                                    if (dd12 == 0) {
                                                        if (aType == "User") {
                                                            chat.server.sendtoUser(btoa(data.d[1]), "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi, list.options[i].value);
                                                        }
                                                        else if (assignType == "Group") {
                                                            chat.server.sendtoGroup(btoa(data.d[1]), list.options[i].value, "TASK┴" + name + "┴" + desc + "┴" + lati + "┴" + longi);
                                                        }
                                                    }
                                                }
                                                else {
                                                    noError = true;
                                                    hideLoader();
                                                    showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                                                }
                                            },
                                            error: function () {
                                                showError("Session timeout. Kindly login again.");
                                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                            }
                                        });
                                    }
                                    if (!noError) {
                                        hideLoader();
                                        jQuery('#newDocument').modal('hide');
                                        showAlert("Successfully sent task");
                                        rowchoice(document.getElementById('rowidChoiceTicket').value);
                                        jQuery('#ticketingViewCard').modal('show');
                                        newTaskClick();
                                    }
                                }
                                else {
                                    showAlert("There are no selected users to send task to.");
                                }
                            }
                        }
                    }
                }
                else {
                    hideLoader();
                    showAlert("Kindly provide task name");
                }
            }
            catch (err) {
                hideLoader();
                showAlert('Error 49: Error occured trying to get task information');
            }
        }
        function cleardispatchList() {
            try {
                var elSel = document.getElementById('sendToListBox');
                var i;
                for (i = elSel.length - 1; i >= 0; i--) {

                    var split = elSel.options[i].text.split('+');
                    var element2 = document.getElementById(elSel.options[i].value + "-" + split[1]);
                    if (element2) {
                        element2.style.color = "#f44e4b";
                        element2.className = "red-color";
                        element2.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                    }
                    var element = document.getElementById("li-" + elSel.options[i].text);
                    if (element) {
                        element.parentNode.removeChild(element);
                    }
                    elSel.remove(i);
                }
            }
            catch (err) {
                alert('Error 42: Failed to clear dispatch list-' + err);
            }
        }
        function newTaskClick() {
            document.getElementById('newtaskHeader').innerHTML = "CREATE NEW TASK";
            document.getElementById('tbNewLongitude').value = "";
            document.getElementById('tbNewLatitude').value = "";
            document.getElementById('tbNewTaskName').value = "";
            document.getElementById('tbNewDescription').value = "";
            cleardispatchList();
            document.getElementById("saveAsTemplateLi").style.display = "block";
            document.getElementById('projectlst').disabled = "";
            document.getElementById('customerslst').disabled = "";
            document.getElementById("requiresignatureCheck").checked = false;
        }
        function sendTicket() {
            try {
                var name = "empty"; //document.getElementById("tbNewTicketName").value;
                //if (!isEmptyOrSpaces(name)) {
                    //if (!isSpecialChar(name)) {
                var isErr = false;
                if (document.getElementById("MainContent_NewTicketControl_ticketCategorySelect").options[document.getElementById("MainContent_NewTicketControl_ticketCategorySelect").selectedIndex].text != "Select Category") {
                    var desc = document.getElementById("tbNewTicketComment").value;
                    if (!isEmptyOrSpaces(desc)) {
                        if (isSpecialChar(desc)) {
                            isErr = true;
                        }
                    }
                    var longi = document.getElementById("tbNewTicketLongitude").value;
                    var lati = document.getElementById("tbNewTicketLatitude").value;


                    if (!isEmptyOrSpaces(longi)) {
                        if (isInvalidDouble(longi)) {
                            isErr = true;
                        }
                    }

                    if (!isEmptyOrSpaces(longi)) {
                        if (isInvalidDouble(lati)) {
                            isErr = true;
                        }
                    }
                    var ticketItems = [];
                    $('#ticketItemsList li').each(function (i) { 
                        ticketItems.push($(this).attr('id'));
                    });
                    if (!isErr) {
 
                        $.ajax({
                            type: "POST",
                            url: "Ticketing.aspx/InsertTicketOnly",
                            data: JSON.stringify({
                                name: name
                                , desc: desc
                                , ticketCat: document.getElementById("MainContent_NewTicketControl_ticketCategorySelect").options[document.getElementById("MainContent_NewTicketControl_ticketCategorySelect").selectedIndex].text
                                , ticketType: document.getElementById("ticketTypeSelect").options[document.getElementById("ticketTypeSelect").selectedIndex].text
                                , ticketItems: ticketItems
                                , longi: longi
                                , lati: lati
                                , uname: loggedinUsername
                                , imgp: document.getElementById("imagePostAttachment").text
                                , cusid: document.getElementById("ticketcustomerslst").value
                                , contractid: document.getElementById("ticketcontractslst").value
                            }), 
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "SUCCESS") {
                                    hideLoader();
                                    jQuery('#newTicketDocument').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Ticket has been created.";
                                    jQuery('#successfulDispatch').modal('show');
                                }
                                else if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                                else {
                                    hideLoader();
                                    showAlert('Error 48: Error occured while creating ticket-' + data.d);
                                }
                            },
                            error: function (jqXHR, textStatus, exception, errorThrown) {
                                AjaxErrorHandler(jqXHR, textStatus, exception, errorThrown);
                                //showError("Session timeout. Kindly login again.");
                               // setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            },timeout:5000
                        });
                    }
                }
                else {
                    hideLoader();
                    showAlert("Kindly provide ticket category");
                }
                    //}
                //}
                //else {
                //    hideLoader();
                //    showAlert("Kindly provide ticket name");
                //}
            }
            catch (err) {
                hideLoader();
                showAlert('Error 49: Error occured trying to get ticket information');
            }
        }
        function AjaxErrorHandler(jqXHR, textStatus, exception, errorThrown) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            showError(msg + "-" + errorThrown);
        }
        function alldeletemarker() {
            try {
                for (var i = 0; i < markers2.length; i++) {
                    markers2[i].setMap(null);
                    markers2[i] = null;
                }
                markers2 = [];
            }
            catch (err) {
                alert('Error 54: Javascript error occured while trying to delete markers-' + err);
            }
        }
        var markers2 = [];
        var uniqueId = 1;
        var mapLocation;
        var mapTicketLocation;
        var mapTaskLocation;
        var myMarkersLocation = new Array();
        var myMarkersTasksLocation = new Array();
        var infoWindowLocation;
        var infoWindowTaskLocation;
        var firstpresstask = false;
        function useLocation(lat, long) {
            document.getElementById("tbNewLatitude").value = lat;
            document.getElementById("tbNewLongitude").value = long;
             
            document.getElementById("tbNewTicketLatitude").value = lat;
            document.getElementById("tbNewTicketLongitude").value = long;

            document.getElementById("pac-input").value = "";
            document.getElementById("pac-inputticket").value = "";
        }

        function getLocationMarkers(obj) {

            try {
                locationAllowed = true;
                //setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                mapLocation = new google.maps.Map(document.getElementById("map_canvasLocation"), mapOptions);

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                mapLocation.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                mapLocation.addListener('bounds_changed', function () {
                    searchBox.setBounds(mapLocation.getBounds());
                });

                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }


                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function (place) {

                        alldeletemarker();

                        if (!place.geometry) {
                            console.log("Returned place contains no geometry");
                            return;
                        }
                        var icon = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        var marker2 = new google.maps.Marker({
                            position: place.geometry.location,
                            title: place.name,
                            map: mapLocation
                        });
                        marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                        //Attach click event handler to the marker.
                        google.maps.event.addListener(marker2, "click", function (e) {
                            var infoWindow = new google.maps.InfoWindow({
                                content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + place.geometry.location.lat() + '&apos;,&apos;' + place.geometry.location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                            });
                            infoWindow.open(mapLocation, marker2);
                        });

                        // Create a marker for each place.
                        //markers.push(new google.maps.Marker({
                        //    map: mapLocation,
                        //    icon: icon,
                        //    title: place.name,
                        //    position: place.geometry.location
                        //}));

                        markers2.push(marker2);

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }

                    });
                    mapLocation.fitBounds(bounds);
                });


                google.maps.event.addListener(mapLocation, 'click', function (event) {
                    alldeletemarker();
                    var location = event.latLng;
                    //Create a marker and placed it on the map.
                    var marker2 = new google.maps.Marker({
                        position: location,
                        map: mapLocation
                    });
                    marker2.id = uniqueId;
                    marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                    uniqueId++;
                    markers2.push(marker2);
                    //Attach click event handler to the marker.
                    google.maps.event.addListener(marker2, "click", function (e) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                        });
                        infoWindow.open(mapLocation, marker2);
                    });
                });
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="locationChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    var marker = new google.maps.Marker({ position: myLatlng, map: mapLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png')
                    myMarkersLocation[obj[i].Username] = marker;
                    createInfoWindowLocation(marker, contentString);
                }
            }
            catch (err) {
                alert(err)
            }
        }
        function getLocationNoMarkers() {

            try {
                locationAllowed = true;
                //setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                mapLocation = new google.maps.Map(document.getElementById("map_canvasLocation"), mapOptions);

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                mapLocation.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                mapLocation.addListener('bounds_changed', function () {
                    searchBox.setBounds(mapLocation.getBounds());
                });

                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }

                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function (place) {

                        alldeletemarker();

                        if (!place.geometry) {
                            console.log("Returned place contains no geometry");
                            return;
                        }
                        var icon = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        var marker2 = new google.maps.Marker({
                            position: place.geometry.location,
                            title: place.name,
                            map: mapLocation
                        });
                        marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                        //Attach click event handler to the marker.
                        google.maps.event.addListener(marker2, "click", function (e) {
                            var infoWindow = new google.maps.InfoWindow({
                                content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + place.geometry.location.lat() + '&apos;,&apos;' + place.geometry.location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                            });
                            infoWindow.open(mapLocation, marker2);
                        });

                        markers2.push(marker2);

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }

                    });
                    mapLocation.fitBounds(bounds);
                });


                google.maps.event.addListener(mapLocation, 'click', function (event) {
                    alldeletemarker();
                    var location = event.latLng;
                    //Create a marker and placed it on the map.
                    var marker2 = new google.maps.Marker({
                        position: location,
                        map: mapLocation
                    });
                    marker2.id = uniqueId;
                    marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                    uniqueId++;
                    markers2.push(marker2);
                    //Attach click event handler to the marker.
                    google.maps.event.addListener(marker2, "click", function (e) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                        });
                        infoWindow.open(mapLocation, marker2);
                    });
                });
            }
            catch (err) {
            }
        }
        function getLocationMarkersTicket(obj) {

            try {
                locationAllowed = true;
                //setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                mapTicketLocation = new google.maps.Map(document.getElementById("map_newticketcanvasLocation"), mapOptions);

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-inputticket');
                var searchBox = new google.maps.places.SearchBox(input);
                mapTicketLocation.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                mapTicketLocation.addListener('bounds_changed', function () {
                    searchBox.setBounds(mapTicketLocation.getBounds());
                });

                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }


                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function (place) {

                        alldeletemarker();

                        if (!place.geometry) {
                            console.log("Returned place contains no geometry");
                            return;
                        }
                        var icon = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        var marker2 = new google.maps.Marker({
                            position: place.geometry.location,
                            title: place.name,
                            map: mapTicketLocation
                        });
                        marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                        //Attach click event handler to the marker.
                        google.maps.event.addListener(marker2, "click", function (e) {
                            var infoWindow = new google.maps.InfoWindow({
                                content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + place.geometry.location.lat() + '&apos;,&apos;' + place.geometry.location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                            });
                            infoWindow.open(mapTicketLocation, marker2);
                        });
                         
                        markers2.push(marker2);

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }

                    });
                    mapTicketLocation.fitBounds(bounds);
                });
                 
                google.maps.event.addListener(mapTicketLocation, 'click', function (event) {
                    alldeletemarker();
                    var location = event.latLng;
                    //Create a marker and placed it on the map.
                    var marker2 = new google.maps.Marker({
                        position: location,
                        map: mapTicketLocation
                    });
                    marker2.id = uniqueId;
                    marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                    uniqueId++;
                    markers2.push(marker2);
                    //Attach click event handler to the marker.
                    google.maps.event.addListener(marker2, "click", function (e) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                        });
                        infoWindow.open(mapTicketLocation, marker2);
                    });
                });
                for (var i = 0; i < obj.length; i++) {

                    var contentString = '<div class="help-block text-center pt-2x"><i class="fa fa-map-marker pr-1x"></i><p class="inline-block red-color" style="margin-top:-2px;color: #b2163b;font-size: 14px;font-family:Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;">' + obj[i].Username + '</p></div><div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color" onclick="ticketlocationChoice(&apos;' + obj[i].Id + '&apos;,&apos;' + obj[i].Username + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    var marker = new google.maps.Marker({ position: myLatlng, map: mapTicketLocation, title: obj[i].Username });
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png')
                    myMarkersLocation[obj[i].Username] = marker;
                    createInfoWindowLocation(marker, contentString);
                }
            }
            catch (err) {
                alert(err)
            }
        }
        
        function getLocationNoMarkersTicket() {

            try {
                locationAllowed = true;
                //setTimeout(function () {
                google.maps.visualRefresh = true;
                var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

                // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
                var mapOptions = {
                    zoom: 8,
                    center: Liverpool,
                    mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
                };

                // This makes the div with id "map_canvas" a google map
                mapTicketLocation = new google.maps.Map(document.getElementById("map_newticketcanvasLocation"), mapOptions);

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-inputticket');
                var searchBox = new google.maps.places.SearchBox(input);
                mapTicketLocation.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                mapTicketLocation.addListener('bounds_changed', function () {
                    searchBox.setBounds(mapTicketLocation.getBounds());
                });

                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }


                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function (place) {

                        alldeletemarker();

                        if (!place.geometry) {
                            console.log("Returned place contains no geometry");
                            return;
                        }
                        var icon = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        var marker2 = new google.maps.Marker({
                            position: place.geometry.location,
                            title: place.name,
                            map: mapTicketLocation
                        });
                        marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                        //Attach click event handler to the marker.
                        google.maps.event.addListener(marker2, "click", function (e) {
                            var infoWindow = new google.maps.InfoWindow({
                                content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + place.geometry.location.lat() + '&apos;,&apos;' + place.geometry.location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'
                            });
                            infoWindow.open(mapTicketLocation, marker2);
                        });

                        markers2.push(marker2);

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }

                    });
                    mapTicketLocation.fitBounds(bounds);
                });

                google.maps.event.addListener(mapTicketLocation, 'click', function (event) {
                    alldeletemarker();
                    var location = event.latLng;
                    //Create a marker and placed it on the map.
                    var marker2 = new google.maps.Marker({
                        position: location,
                        map: mapTicketLocation
                    });
                    marker2.id = uniqueId;
                    marker2.setIcon('https://testportalcdn.azureedge.net/Images/mainlocation.png');
                    uniqueId++;
                    markers2.push(marker2);
                    //Attach click event handler to the marker.
                    google.maps.event.addListener(marker2, "click", function (e) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: '<div  class="help-block text-center"><a href="#" style="color: #b2163b;font-size: 14px;font-family: Montserrat-Regular,Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;" class="red-borders light-button red-color"  onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)"><i class="fa fa-map-marker red-color"></i>USE LOCATION</a></div>'//'<input type="button" onclick="useLocation(&apos;' + location.lat() + '&apos;,&apos;' + location.lng() + '&apos;)" value="Use Location"></input>'
                        });
                        infoWindow.open(mapTicketLocation, marker2);
                    });
                });
            }
            catch (err) {
            }
        }
 
        function createInfoWindowLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindowLocation.setContent(popupContent);
                infoWindowLocation.open(mapLocation, this);
            });
        }
        //NEWTASK DISPATCH
        function usersliOnclickRemove(name, id, gtype) { 
            dispatchUserchoiceTable(id, name, gtype);
        }
        function removenameFromDispatchList(name, gtype) {
            try {
                var element = document.getElementById("li-" + name.split(" ")[0] + "+" + gtype);
                element.parentNode.removeChild(element);
            }
            catch (err) {
                alert(err + 'rfd')
            }
        }
        function addnametoUserDispatchList(name, id, gtype) {
            var ul = document.getElementById("UsersToDispatchList");
            var li = document.createElement("li");
            li.setAttribute("id", "li-" + name.split(" ")[0] + "+" + gtype);
            li.innerHTML = '<a href="#"  class="capitalize-text" >' + name + '<i class="fa fa-close" onclick="usersliOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;,&apos;' + gtype + '&apos;)"></i></a>';
            ul.appendChild(li);
        }
        function dispatchUserchoiceTable(id, name, gtype) {
            try {
                var element = document.getElementById(id + "-" + gtype);
                var result = element.innerHTML.indexOf("ADDED");
                if (result < 0) {

                    var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                    if (exists == false) {
                        var myOption;
                        myOption = document.createElement("Option");
                        myOption.text = name.split(" ")[0] + "+" + gtype; //Textbox's value
                        myOption.value = id; //Textbox's value
                        sendToListBox.add(myOption);
                        addnametoUserDispatchList(name, id, gtype);

                        element.style.color = "#3ebb64";
                        element.className = "green-color";
                        element.innerHTML = '<i class="fa fa-check green-color"></i>ADDED';
                    }
                }
                else {
                    var elSel = document.getElementById('sendToListBox');
                    var i;
                    for (i = elSel.length - 1; i >= 0; i--) {
                        if (elSel.options[i].value == id) {
                            var split = elSel.options[i].text.split('+');
                            removenameFromDispatchList(split[0], split[1]);
                            elSel.remove(i);
                        }
                    }

                    element.style.color = "#b2163b";
                    element.className = "red-color";
                    element.innerHTML = '<i class="fa fa-plus red-color"></i>ADD';
                }
            }
            catch (ex)
            { alert(ex) }
        }
        function locationChoice(id, name) {
            var element = document.getElementById('MainContent_NewTaskControl_locationSelect');
            element.value = id;
            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getLocationById",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('tbNewLongitude').value = data.d[0];
                        document.getElementById('tbNewLatitude').value = data.d[1];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function ticketlocationChoice(id, name) {
            var element = document.getElementById('MainContent_NewTicketControl_ticketlocationSelect');
            element.value = id;
            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getLocationById",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('tbNewTicketLongitude').value = data.d[0];
                        document.getElementById('tbNewTicketLatitude').value = data.d[1];
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function assignUserTableData() {
            $("#assignUsersTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/getAssignUserTableData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#assignUsersTable tbody").append(data.d[i]);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function taskselectAssigneeTypeChange() {
            var assignType = document.getElementById("taskSelectAssigneeType").value;
            if (document.getElementById("taskSelectAssigneeType").selectedIndex == 0){//assignType == "User") 
                document.getElementById("usersearchSelectDIV").style.display = "block";
                document.getElementById("groupsearchSelectDIV").style.display = "none";
                document.getElementById("devicesearchSelectDIV").style.display = "none";
                document.getElementById("map_canvasLocationDIV").style.display = "block";
            }
            else if (document.getElementById("taskSelectAssigneeType").selectedIndex == 1){
                document.getElementById("usersearchSelectDIV").style.display = "none";
                document.getElementById("groupsearchSelectDIV").style.display = "block";
                document.getElementById("devicesearchSelectDIV").style.display = "none";
                document.getElementById("map_canvasLocationDIV").style.display = "block";
            }
            else if (document.getElementById("taskSelectAssigneeType").selectedIndex == 2){
                document.getElementById("usersearchSelectDIV").style.display = "none";
                document.getElementById("groupsearchSelectDIV").style.display = "none";
                document.getElementById("devicesearchSelectDIV").style.display = "block";
                document.getElementById("map_canvasLocationDIV").style.display = "none";
            }
        }
        function ticketlocationOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
            if (e.options[e.selectedIndex].value > 0) {
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getLocationById",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            document.getElementById('tbNewTicketLongitude').value = data.d[0];
                            document.getElementById('tbNewTicketLatitude').value = data.d[1];
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                document.getElementById('tbNewTicketLongitude').value = "";
                document.getElementById('tbNewTicketLatitude').value = "";
            }
        }
        function ticketCategorySelectOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
            $('#ticketTypeSelect option').remove();
            var projectoption = $("#ticketTypeSelect");
            projectoption.append($('<option></option>').val(0).html('Select Type'));
            if (e.options[e.selectedIndex].value > 0) {
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getTicketTypeByCategory",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var options = $("#ticketTypeSelect");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].OffenceTypeId).html(data.d[i].OffenceTypeDesc)
                                 );
                            } 
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            } 
        }
        function ticketTypeSelectOnChange(e) {
            $('#ticketItemSelect option').remove();
            var projectoption = $("#ticketItemSelect");
            projectoption.append($('<option></option>').val(0).html('Select Item'));
            if (e.options[e.selectedIndex].value > 0) {
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getTicketItemByType",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var options = $("#ticketItemSelect");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].ID).html(data.d[i].OffenceDesc)
                                 );
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            } 
        }
        function locationOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
            if (e.options[e.selectedIndex].value > 0) {
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getLocationById",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            document.getElementById('tbNewLongitude').value = data.d[0];
                            document.getElementById('tbNewLatitude').value = data.d[1];
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                document.getElementById('tbNewLongitude').value = "";
                document.getElementById('tbNewLatitude').value = "";
            }
        }


        function ticketcustomersOnChange(e) { 
            $('#ticketcontractslst option').remove();

            var contractoption = $("#ticketcontractslst");
            contractoption.append($('<option></option>').val(0).html('Select Contract'));
             
            if (e.options[e.selectedIndex].value > 0) {
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getContractListByCustomerId",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#ticketcontractslst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                             );
                        }
                    }
                }); 

            }
            else { 
                $('#ticketcustomerslst option').remove();
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getCustomerByProjectId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#ticketcustomerslst");
                        for (var i = 0; i < data.d.length; i++) {
                            cid = data.d[i].Id;
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                             );
                        }
                    }
                }); 
            } 
        }

        function customersOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display

            $('#contractslst option').remove();

            var contractoption = $("#contractslst");
            contractoption.append($('<option></option>').val(0).html('Select Contract'));


            if (e.options[e.selectedIndex].value > 0) { 
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getContractListByCustomerId",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#contractslst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                             );
                        }
                    }
                });
                if (document.getElementById('projectlst').value > 0) {

                }
                else {
                    $('#projectlst option').remove();
                    var projectoption = $("#projectlst");
                    projectoption.append($('<option></option>').val(0).html('Select Project'));
                    $.ajax({
                        type: "POST",
                        url: "Ticketing.aspx/getProjectListByCustomerId",
                        data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#projectlst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                                 );
                            }
                        }
                    });
                }

            }
            else {

                 
                $('#customerslst option').remove();
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getCustomerByProjectId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#customerslst");
                        for (var i = 0; i < data.d.length; i++) {
                            cid = data.d[i].Id;
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                             );
                        }
                    }
                });

                $('#projectlst option').remove();
                var projectoption = $("#projectlst");
                projectoption.append($('<option></option>').val(0).html('Select Project'));
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getProjectListByCustomerId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#projectlst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                             );
                        }
                    }
                });
            }

        }


        function projectlstOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
            if (document.getElementById('customerslst').value > 0) {

            }
            else {
                $('#customerslst option').remove();
                if (e.options[e.selectedIndex].value == 0) {
                    $('#contractslst option').remove();
                    $('#projectlst option').remove();
                    var contractoption = $("#contractslst");
                    contractoption.append($('<option></option>').val(0).html('Select Contract'));
                    var projectoption = $("#projectlst");
                    projectoption.append($('<option></option>').val(0).html('Select Project'));
                    $.ajax({
                        type: "POST",
                        url: "Ticketing.aspx/getContractListByCustomerId",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#contractslst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                                 );
                            }
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "Ticketing.aspx/getProjectListByCustomerId",
                        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#projectlst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                                 );
                            }
                        }
                    });
                }
                var cid = 0;
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getCustomerByProjectId",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#customerslst");
                        for (var i = 0; i < data.d.length; i++) {
                            cid = data.d[i].Id;
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                             );
                        }
                    }
                });
                if (cid > 0) {
                    $('#contractslst option').remove();
                    var contractoption = $("#contractslst");
                    contractoption.append($('<option></option>').val(0).html('Select Contract'));
                    $.ajax({
                        type: "POST",
                        url: "Ticketing.aspx/getContractListByCustomerId",
                        data: "{'id':'" + cid + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var options = $("#contractslst");
                            for (var i = 0; i < data.d.length; i++) {
                                options.append(
                                     $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                                 );
                            }
                        }
                    });
                }
            }
        }

        function projectOnChange(e) {
            //alert(e.id + ' ' + e.options[e.selectedIndex].value); // display
            if (e.options[e.selectedIndex].value > 0) {
                document.getElementById('tbProjectId').value = e.options[e.selectedIndex].value;
            }

        }

        function addnewtask() {
            jQuery('#ticketingViewCard').modal('hide');
            jQuery("#customerslst").val(document.getElementById("ticketcustomerid").value);

            $('#ticketcontractslst option').remove();
            var contractoption = $("#contractslst");
            contractoption.append($('<option></option>').val(0).html('Select Contract'));


            if (document.getElementById("ticketcustomerid").value > 0) {
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getContractListByCustomerId",
                    data: "{'id':'" + document.getElementById("ticketcustomerid").value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#contractslst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].ContractRef)
                             );
                        }
                        jQuery("#contractslst").val(document.getElementById("ticketcontractid").value);
                    }
                });
            }
            jQuery("#contractslst").val(document.getElementById("ticketcontractid").value);
              
        }
        function unhandledClickTicket() {
            document.getElementById("ticketinitialOptionsDiv").style.display = "block";
            document.getElementById("tickethandleOptionsDiv").style.display = "none";
        }
        function handledClickTicket() {
            document.getElementById("ticketinitialOptionsDiv").style.display = "none";
            document.getElementById("tickethandleOptionsDiv").style.display = "block";
        }
        function getTasklistItemsTicket(id) {
            document.getElementById("tickettaskItemsList").innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/getTaskListDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("tickettaskItemsList");
                            var li = document.createElement("li");
                            var colorRet = 'green';
                            if (res[3] == "Pending") {
                                colorRet = 'red';
                            }
                            else if (res[3] == "InProgress") {
                                colorRet = 'yellow';
                            }
                            var action = "";

                            if (res[4] == "true")
                                action = 'href="#taskDocument"  data-toggle="modal" data-dismiss="modal" onclick="showTaskDocument(&apos;' + res[2] + '&apos;);"';

                            li.innerHTML = '<i class="fa fa-circle  ' + colorRet + '-color"></i><a style="margin-left:5px;"  class="capitalize-text" '+action+' >' + res[0] + '</a>';

                            ul.appendChild(li);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addOffenceToList() {
            if (document.getElementById("ticketItemSelect").value > 0) {

                var currentcount = 0;
                var founditem = false;
                $('#ticketItemsList li').each(function (i) {
                    //$(this).attr('id'); // This is your rel value 
                    if ('ticket-' + document.getElementById("ticketItemSelect").value == $(this).attr('id')) {
                        founditem = true;
                    }
                    currentcount++;
                });
                if (!founditem) {
                    if (currentcount < 4) {
                        var ul = document.getElementById("ticketItemsList");
                        var li = document.createElement("li");

                        li.id = 'ticket-' + document.getElementById("ticketItemSelect").value;
                        var rel = document.getElementById("ticketItemSelect").options[document.getElementById("ticketItemSelect").selectedIndex].text;

                        li.innerHTML = '<a style="margin-left:5px;cursor:default" href="#"   class="capitalize-text" >' + rel + '</a>&nbsp;&nbsp;<i style="cursor: pointer;" class="fa fa-close fa-1x" onclick="removeLi(&apos;ticket-' + document.getElementById("ticketItemSelect").value + '&apos;)"></i>';

                        ul.appendChild(li);
                    }
                    else {
                        showAlert("Reached max number of items (4)");
                    }
                }
                else {
                    showAlert("Item already added.");
                }
            }
        }
        function removeLi(id) {
            $('#'+id).remove(); 
        }
        function deleteAttachmentChoiceTicket(id) {
            jQuery('#deleteAttachTicketModal').modal('show');
            document.getElementById('rowidChoiceAttachment').value = id;
            jQuery('#ticketingViewCard').modal('hide');
        }

        function deleteAttachmentTicket() {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/deleteAttachmentDataTicket",
                data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d != "LOGOUT") {
                        showAlert(data.d);
                        rowchoice(document.getElementById('rowidChoiceTicket').value);
                        jQuery('#ticketingViewCard').modal('show');
                    }
                    else {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function startTicket() {
            var id = document.getElementById('rowidChoiceTicket').value;
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/startTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "SUCCESS") {


                    }
                    else if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d[0]);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function handleTicket() {
            var id = document.getElementById('rowidChoiceTicket').value;
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/handleTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "SUCCESS") {
                        jQuery('#ticketingViewCard').modal('hide');
                        document.getElementById('successMessage').innerHTML = "Ticket has successfully been resolved";
                        jQuery('#successfulModal').modal('show');
                    }
                    else if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d[0]);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
    }
    function activaTab(tab) {
        var el = document.getElementById('video-0-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('ticketlocation-tab');
        if (el3) {
            el3.className = 'tab-pane fade';
        }
        var el4 = document.getElementById('image-1-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
        var el5 = document.getElementById('image-2-tab');
        if (el5) {
            el5.className = 'tab-pane fade';
        }
        var el6 = document.getElementById('image-3-tab');
        if (el6) {
            el6.className = 'tab-pane fade';
        }
        var el7 = document.getElementById('image-4-tab');
        if (el7) {
            el7.className = 'tab-pane fade';
        }
        var el8 = document.getElementById('image-5-tab');
        if (el8) {
            el8.className = 'tab-pane fade';
        }
        var el9 = document.getElementById('image-6-tab');
        if (el9) {
            el9.className = 'tab-pane fade';
        }
        var el10 = document.getElementById('image-0-tab');
        if (el10) {
            el10.className = 'tab-pane fade';
        }
        var ell = document.getElementById(tab);
        if (ell) {
            ell.className = 'tab-pane fade active in';
        }
    }
    var attachmentIndex = 0;
    function onclickAttachmentForward() {
        attachmentIndex++;
        if (attachmentIndex < 8) {
            activaTab('image-' + attachmentIndex + '-tab');
        }
        else if (attachmentIndex == 8) {
            activaTab('video-0-tab');
        }
        else if (attachmentIndex == 9) {
            attachmentIndex = 0;
            activaTab('ticketlocation-tab');
        }
    }
    function onclickAttachmentBack() {
        attachmentIndex--;
        if (attachmentIndex == 0) {
            attachmentIndex = 9;
        }
        else if (attachmentIndex < 0) {
            attachmentIndex = 8;
        }
        if (attachmentIndex < 8) {
            activaTab('image-' + attachmentIndex + '-tab');
        }
        else if (attachmentIndex == 8) {
            activaTab('video-0-tab');
        }
        else if (attachmentIndex == 9) {
            attachmentIndex = 0;
            activaTab('ticketlocation-tab');
        }
    }
    function editOffenceTypeSave() {
        var isPass = true;
        if (isEmptyOrSpaces(document.getElementById('tbEditOffenceName').value)) {
            isPass = false;
            showAlert("Kindly provide Offence");
        }
        else {
            if (isSpecialChar(document.getElementById('tbEditOffenceName').value)) {
                isPass = false;
                showAlert("Kindly remove special character");
            }
        }
        if (!isEmptyOrSpaces(document.getElementById('tbEditOffenceArName').value)) {
            if (isSpecialChar(document.getElementById('tbEditOffenceArName').value)) {
                isPass = false;
                showAlert("Kindly remove special character description");
            }
        }
        if (isPass) {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/editOffenceType",
                data: "{'id':'" + document.getElementById('tbOffenceIdChoice').value + "','name':'" + document.getElementById('tbEditOffenceName').value + "','nameAR':'" + document.getElementById('tbEditOffenceArName').value + "','type':'" + document.getElementById('MainContent_typeEditSelect').value + "','tvalue':'" + document.getElementById('tbEditOffenceValue').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") { 
                        jQuery('#editOffenceModal').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = "Item successfully edited!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
        function editOffenceCatTypeSave2() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbEditCategoryName2').value)) {
                isPass = false;
                showAlert("Kindly provide category");
            }
            else {
                if (isSpecialChar(document.getElementById('tbEditCategoryName2').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character category");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbEditArCategoryName2').value)) {
                if (isSpecialChar(document.getElementById('tbEditArCategoryName2').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character category description");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/editOffenceCat2",
                    data: "{'id':'" + document.getElementById('tbOffenceCatChoice').value + "','name':'" + document.getElementById('tbEditCategoryName2').value + "','nameAR':'" + document.getElementById('tbEditArCategoryName2').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#EditOffenceCategoryModal2').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Category successfully edited!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function editOffenceCatTypeSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbEditCategoryName').value)) {
                isPass = false;
                showAlert("Kindly provide type");
            }
            else {
                if (isSpecialChar(document.getElementById('tbEditCategoryName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character type");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbEditArCategoryName').value)) {
                if (isSpecialChar(document.getElementById('tbEditArCategoryName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character type description");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/editOffenceCat",
                    data: "{'id':'" + document.getElementById('tbOffenceCatChoice').value + "','name':'" + document.getElementById('tbEditCategoryName').value + "','nameAR':'" + document.getElementById('tbEditArCategoryName').value + "','cval':'" + document.getElementById('MainContent_editoffenceCategorySelect').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#EditOffenceCategoryModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Type successfully edited!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function editVehMakeSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbEditVehicleMakeName').value)) {
                isPass = false;
                showAlert("Kindly provide vehicle make");
            }
            else {
                if (isSpecialChar(document.getElementById('tbEditVehicleMakeName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character vehicle make");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbEditArVehicleMakeName').value)) {
                if (isSpecialChar(document.getElementById('tbEditArVehicleMakeName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character vehicle make description");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/editVehMake",
                    data: "{'id':'" + document.getElementById('tbVehicleMakeChoice').value + "','name':'" + document.getElementById('tbEditVehicleMakeName').value + "','nameAR':'" + document.getElementById('tbEditArVehicleMakeName').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#editVehicleMakeModal').modal('hide');

                            document.getElementById('successincidentScenario').innerHTML = "Vehicle Make successfully edited!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }

                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function editVehColorSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbEditVehicleColorName').value)) {
                isPass = false;
                showAlert("Kindly provide Vehicle Color");
            }
            else {
                if (isSpecialChar(document.getElementById('tbEditVehicleColorName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character Vehicle Color");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbEditArVehicleColorName').value)) {
                if (isSpecialChar(document.getElementById('tbEditArVehicleColorName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character Vehicle Color description");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/editVehColor",
                    data: "{'id':'" + document.getElementById('tbVehicleColorChoice').value + "','name':'" + document.getElementById('tbEditVehicleColorName').value + "','nameAR':'" + document.getElementById('tbEditArVehicleColorName').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#editVehicleColorModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Vehicle Color successfully edited!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function delOffenceTypeSave() {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/delOffenceType",
                data: "{'id':'" + document.getElementById('tbOffenceIdChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Item successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function delOffenceCatTypeSave() {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/delOffenceCat",
                data: "{'id':'" + document.getElementById('tbOffenceCatChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Type successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function delOffenceCatTypeSave2() {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/delOffenceCat2",
                data: "{'id':'" + document.getElementById('tbOffenceCatChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Category successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }


        function delPlateCode() {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/delPlateCode",
                data: "{'id':'" + document.getElementById('tbPlateCodeChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Plate Code successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function delPlateSouce() {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/delPlateSource",
                data: "{'id':'" + document.getElementById('tbPlateSourceChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Plate Source successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }

                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function delVehColorSave() {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/delVehColor",
                data: "{'id':'" + document.getElementById('tbVehicleColorChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Vehicle Color successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function delVehMakeSave() {
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/delVehMake",
                data: "{'id':'" + document.getElementById('tbVehicleMakeChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Vehicle Color successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }

                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function addOffenceTypeSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbName').value)) {
                isPass = false;
                showAlert("Kindly provide item");
            }
            else {
                if (isSpecialChar(document.getElementById('tbName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character item");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbArName').value)) {

            }
            if (!isEmptyOrSpaces(document.getElementById('tbValue').value)) {
                if (!isNumeric(document.getElementById('tbValue').value)) {
                    isPass = false;
                    showAlert("Kindly provide numeric value for the value field");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/addOffenceType",
                    data: "{'id':'0','name':'" + document.getElementById('tbName').value + "','nameAR':'" + document.getElementById('tbArName').value + "','type':'" + document.getElementById('MainContent_typeSelect').value + "','tvalue':'" + document.getElementById('tbValue').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newOffenceModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Item successfully added!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function addOffenceCatTypeSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbCategoryName').value)) {
                isPass = false;
                showAlert("Kindly provide type");
            }
            else {
                if (isSpecialChar(document.getElementById('tbCategoryName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character type");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbArCategoryName').value)) {

            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/addOffenceCat",
                    data: "{'id':'0','name':'" + document.getElementById('tbCategoryName').value + "','nameAR':'" + document.getElementById('tbArCategoryName').value + "','cval':'" + document.getElementById('MainContent_offenceCategorySelect').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newOffenceCategoryModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Type successfully added!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function addOffenceCatTypeSave2() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbCategoryName2').value)) {
                isPass = false;
                showAlert("Kindly provide category");
            }
            else {
                if (isSpecialChar(document.getElementById('tbCategoryName2').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character category");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbArCategoryName2').value)) {

            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/addOffenceCat2",
                    data: "{'id':'0','name':'" + document.getElementById('tbCategoryName2').value + "','nameAR':'" + document.getElementById('tbArCategoryName2').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newOffenceCategoryModal2').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "category successfully added!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }

                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }

        function addPlateCodeSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbPlateCodeName').value)) {
                isPass = false;
                showAlert("Kindly provide plate code");
            }
            else {
                if (isSpecialChar(document.getElementById('tbPlateCodeName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character plate code");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbArPlateCodeName').value)) {

            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/addPlateCode",
                    data: "{'id':'0','name':'" + document.getElementById('tbPlateCodeName').value + "','nameAR':'" + document.getElementById('tbArPlateCodeName').value + "','type':'" + document.getElementById('MainContent_typeSelectPlateSource').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newPlateCode').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Plate code successfully added!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }

                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function editPlateCodeSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbEditPlateCodeName').value)) {
                isPass = false;
                showAlert("Kindly provide Plate code");
            }
            else {
                if (isSpecialChar(document.getElementById('tbEditPlateCodeName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character Plate code");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbEditArPlateCodeName').value)) {
                if (isSpecialChar(document.getElementById('tbEditArPlateCodeName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character Plate code description");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/editPlateCode",
                    data: "{'id':'" + document.getElementById('tbPlateCodeChoice').value + "','name':'" + document.getElementById('tbEditPlateCodeName').value + "','nameAR':'" + document.getElementById('tbEditArPlateCodeName').value + "','type':'" + document.getElementById('MainContent_typeEditSelectPlateSource').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successincidentScenario').innerHTML = "Plate code successfully edited!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }

                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function addPlateSourceSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbPlateSourceName').value)) {
                isPass = false;
                showAlert("Kindly provide plate source");
            }
            else {
                if (isSpecialChar(document.getElementById('tbPlateSourceName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character plate source");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbArPlateSourceName').value)) {

            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/addPlateSource",
                    data: "{'id':'0','name':'" + document.getElementById('tbPlateSourceName').value + "','nameAR':'" + document.getElementById('tbArPlateSourceName').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newPlateSourceModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Plate source successfully added!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }

                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function editPlateSourceSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbEditPlateSourceName').value)) {
                isPass = false;
                showAlert("Kindly provide Plate source");
            }
            else {
                if (isSpecialChar(document.getElementById('tbEditPlateSourceName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character Plate source");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbEditArPlateSourceName').value)) {
                if (isSpecialChar(document.getElementById('tbEditArPlateSourceName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character Plate source description");
                }
            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/editPlateSource",
                    data: "{'id':'" + document.getElementById('tbPlateSourceChoice').value + "','name':'" + document.getElementById('tbEditPlateSourceName').value + "','nameAR':'" + document.getElementById('tbEditArPlateSourceName').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            document.getElementById('successincidentScenario').innerHTML = "Plate source successfully edited!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }

                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function addVehMakeSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbVehicleMakeName').value)) {
                isPass = false;
                showAlert("Kindly provide vehicle make");
            }
            else {
                if (isSpecialChar(document.getElementById('tbVehicleMakeName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character vehicle make");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbArVehicleMakeName').value)) {

            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/addVehMake",
                    data: "{'id':'0','name':'" + document.getElementById('tbVehicleMakeName').value + "','nameAR':'" + document.getElementById('tbArVehicleMakeName').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newVehicleMakeModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Vehicle Make successfully added!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }

                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }

        function addVehColorSave() {
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('tbVehicleColorName').value)) {
                isPass = false;
                showAlert("Kindly provide vehicle color");
            }
            else {
                if (isSpecialChar(document.getElementById('tbVehicleColorName').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character vehicle color");
                }
            }
            if (!isEmptyOrSpaces(document.getElementById('tbArVehicleColorName').value)) {

            }
            if (isPass) {
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/addVehColor",
                    data: "{'id':'0','name':'" + document.getElementById('tbVehicleColorName').value + "','nameAR':'" + document.getElementById('tbArVehicleColorName').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {

                        if (data.d == "SUCCESS") {
                            jQuery('#newVehicleColorModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Vehicle Color successfully added!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
    function oldDivContainers() {
        try {
            for (var i = 0; i < divArray.length; i++) {
                if (typeof divArray[i] === "undefined") {
                }
                else {
                    var el = document.getElementById(divArray[i]);
                    el.parentNode.removeChild(el);
                }
            }
            divArray = new Array();
        }
        catch (ex) {
            // alert(ex);
        }
    }
    function ticketinfotabDefault() {
        var el3 = document.getElementById('ticketinfo-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('ticketattachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
        var el42 = document.getElementById('ticketactivity-tab');
        if (el42) {
            el42.className = 'tab-pane fade';
        }
        var el43 = document.getElementById('ticketnotes-tab');
        if (el43) {
            el43.className = 'tab-pane fade';
        }
        var el44 = document.getElementById('tickettasks-tab');
        if (el44) {
            el44.className = 'tab-pane fade';
        }
        var el2 = document.getElementById('liTicketInfo');
        if (el2) {
            el2.className = 'active';
        }
        var el6 = document.getElementById('liTicketAtta');
        if (el6) {
            el6.className = ' ';
        }
        var el61 = document.getElementById('liTicketAct');
        if (el61) {
            el61.className = ' ';
        }
        var el62 = document.getElementById('liTicketNotes');
        if (el62) {
            el62.className = ' ';
        }
        var el63 = document.getElementById('liTicketTasks');
        if (el63) {
            el63.className = ' ';
        }
    }
    function showTaskDocument(id) {
        document.getElementById('rowChoiceTasks').value = id;
        document.getElementById("taskinitialOptionsDiv").style.display = "block";
        document.getElementById("taskhandleOptionsDiv").style.display = "none";
        document.getElementById("taskrejectOptionsDiv").style.display = "none";
        var el = document.getElementById('tasklocation-tab');
        if (el) {
            el.className = 'tab-pane fade active in';
        }
        var el2 = document.getElementById('taskrejection-tab');
        if (el2) {
            el2.className = 'tab-pane fade';
        }

        oldDivContainers();

        var retVal = assignrowDataTask(id);
        if (retVal == "Completed")
            TaskIsCompleted();

        taskHistoryData(id);
        taskinsertAttachmentIcons(id);
        taskinsertAttachmentTabData(id);
        taskinsertAttachmentData(id);
        taskgetChecklistItems(id);
        getChecklistItemsNotes(id);
        getCanvasNotes(id);
        taskinfotabDefault();
        gettaskRemarks(id);
        hideAllRemarks();
    }
    function taskinfotabDefault() {
        var el = document.getElementById('taskactivity-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('taskinfo-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('taskattachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
        var el2 = document.getElementById('taskliInfo');
        if (el2) {
            el2.className = 'active';
        }
        var el5 = document.getElementById('taskliActi');
        if (el5) {
            el5.className = ' ';
        }
        var el6 = document.getElementById('taskliAtta');
        if (el6) {
            el6.className = ' ';
        }
        var el7 = document.getElementById('taskliNotes');
        if (el7) {
            el7.className = ' ';
        }
        var el8 = document.getElementById('tasknotes-tab');
        if (el8) {
            el8.className = 'tab-pane fade';
        }
    }
    function getCanvasNotes(id) {
        document.getElementById("canvasItemsListNotes").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Ticketing.aspx/getCanvasNotesData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    if (data.d.length > 0) {
                        document.getElementById("pCanvasNotes").style.display = "block";
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("canvasItemsListNotes");
                            var li = document.createElement("li");
                            li.innerHTML = '<i style="margin-left:-15px;" ></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                            ul.appendChild(li);
                            if (res.length > 1) {
                                if (res[1] != '') {
                                    var li2 = document.createElement("li");
                                    li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                    ul.appendChild(li2);
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById("pCanvasNotes").style.display = "none";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
    function getChecklistItemsNotes(id) {
        document.getElementById("checklistItemsListNotes").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Ticketing.aspx/getChecklistNotesData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    if (data.d.length > 0) {
                        document.getElementById("pchecklistItemsListNotes").style.display = "block";
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("checklistItemsListNotes");
                            var li = document.createElement("li");
                            li.innerHTML = '<i style="margin-left:-15px;" class="fa fa-square-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                            ul.appendChild(li);
                            if (res.length > 1) {
                                if (res[1] != '') {
                                    var li2 = document.createElement("li");
                                    li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                    ul.appendChild(li2);
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById("pchecklistItemsListNotes").style.display = "none";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
    function taskgetChecklistItems(id) {
        document.getElementById("checklistItemsList").innerHTML = "";
        $.ajax({
            type: "POST",
            url: "Ticketing.aspx/taskgetChecklistData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    if (data.d[0] == "False") {
                        var el = document.getElementById('checklistnamespanFA');
                        if (el) {
                            el.className = "fa fa-square-o";
                        }
                    }
                    else {
                        var el = document.getElementById('checklistnamespanFA');
                        if (el) {
                            el.className = "fa fa-check-square-o";
                        }
                    }
                    for (var i = 1; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("checklistItemsList");
                        var li = document.createElement("li");
                        var marginLeft = '';
                        if (res[2] == 'True') {
                            marginLeft = 'style = "margin-left:-15px;"';
                        }

                        if (res[2] == '3') {

                            if (res[1] == "Checked")
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];
                            else
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];

                            ul.appendChild(li);

                            var li2 = document.createElement("li");
                            li2.innerHTML = '<a style="margin-left:5px;" href="#"  class="capitalize-text" >Notes: ' + res[3] + '</a>';
                            ul.appendChild(li2);
                        }
                        else {
                            if (res[1] == "Checked")
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];
                            else
                                li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];

                            ul.appendChild(li);
                        }
                        //li.appendChild(document.createTextNode(data.d[i]));

                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
        function audiotaskplay(sr) {
            document.getElementById('taskAudioDIV').style.display = "block";
            document.getElementById('taskAudioSrc').src = sr;
            document.getElementById('taskAudio').load(); //call this to just preload the audio without playing

        }
        function hideTaskplay() {
            document.getElementById('taskAudioDIV').style.display = "none";
        }
        function taskinsertAttachmentData(id) {
            document.getElementById('taskrotationDIV1').style.display = "none";
            document.getElementById('taskrotationDIV2').style.display = "none";
        $.ajax({
            type: "POST",
            url: "Ticketing.aspx/taskgetAttachmentData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    imgcount = 0;
                    document.getElementById('taskAudioDIV').style.display = "none";
                    for (var i = 0; i < data.d.length; i++) {
                        if (data.d[i].indexOf("video") >= 0) {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.innerHTML = data.d[i];
                            div.id = 'video-' + (i + 1) + '-tab';
                            document.getElementById('taskdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'video-' + (i + 1) + '-tab';
                            imgcount++;
                        } 
                        else {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.align = 'center';
                            div.style.height = '380px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i + 1) + '-tab';
                            document.getElementById('taskdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'image-' + (i + 1) + '-tab';
                            imgcount++;
                        }
                    }

                    if (imgcount > 0) {
                        document.getElementById('taskrotationDIV1').style.display = "block";
                        document.getElementById('taskrotationDIV2').style.display = "block";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }

    function taskinsertAttachmentTabData(id) {
        jQuery('#taskattachments-info-tab div').html('');

        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/taskgetAttachmentDataTab",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskattachments-info-tab").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function taskinsertAttachmentIcons(id) {
        jQuery('#taskdivAttachment div').html('');
        $.ajax({
            type: "POST",
            url: "Ticketing.aspx/taskgetAttachmentDataIcons",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else
                    document.getElementById("taskdivAttachment").innerHTML = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function taskHistoryData(id) {
        jQuery('#taskdivIncidentHistoryActivity div').html('');
        $.ajax({
            type: "POST",
            url: "Ticketing.aspx/getTaskHistoryData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('taskdivIncidentHistoryActivity').appendChild(div);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function TaskIsCompleted() {
        document.getElementById("taskinitialOptionsDiv").style.display = "block";
        document.getElementById("taskhandleOptionsDiv").style.display = "none";
        document.getElementById("taskrejectOptionsDiv").style.display = "none";
    }
    function assignrowDataTask(id) {
        var output = "";
        $.ajax({
            type: "POST",
            url: "Ticketing.aspx/getTableRowDataTask",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskusernameSpan").innerHTML = data.d[0];
                    document.getElementById("tasktimeSpan").innerHTML = data.d[1];
                    document.getElementById("tasktypeSpan").innerHTML = data.d[2];
                    document.getElementById("taskstatusSpan").innerHTML = data.d[3];
                    document.getElementById("tasklocSpan").innerHTML = data.d[4];
                    document.getElementById("taskdescriptionSpan").innerHTML = data.d[8];
                    document.getElementById("taskinstructionSpan").innerHTML = data.d[9];
                    document.getElementById("taskincidentNameHeader").innerHTML = data.d[10];
                    document.getElementById("assignedTimeSpan").innerHTML = data.d[11];
                    document.getElementById("checklistNotesSpan").innerHTML = data.d[12];
                    document.getElementById("checklistnameSpan").innerHTML = data.d[14];

                    var el = document.getElementById('headerImageClass');
                    if (el) {
                        el.className = data.d[13];
                    }
                    output = data.d[3];

                    document.getElementById('ttypeSpan').innerHTML = data.d[15];

                    document.getElementById("incidentItemsList").innerHTML = "";
                    var res = data.d[16].split("|");
                    if (res.length > 0) {
                        document.getElementById("incidentItemsList").innerHTML = 'Ticket: <a style="color:#b2163b;" href="#ticketingViewCard"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="rowchoice(&apos;' + res[1] + '&apos;)">' + res[0] + '</a>';
                    }

                    document.getElementById("CustomerNameSpan").innerHTML = ""; 
                    document.getElementById("ProjectNameSpan").innerHTML = "";
                    document.getElementById("ContractNameSpan").innerHTML = "";

                    document.getElementById('dvCustomerNameSpan').style.display = "none"; 
                    document.getElementById('dvProjectNameSpan').style.display = "none";
                    document.getElementById('dvContractNameSpan').style.display = "none";

                    //NEWCUSTOMER
 
                    if (data.d[17] != "N/A") {
                        document.getElementById("CustomerNameSpan").innerHTML = data.d[17];
                            document.getElementById('dvCustomerNameSpan').style.display = "block";
                        } 
                    if (data.d[18] != "N/A") {
                            document.getElementById("ProjectNameSpan").innerHTML = data.d[18];
                            document.getElementById('dvProjectNameSpan').style.display = "block";
                        }
                    if (data.d[19] != "N/A") {
                            document.getElementById("ContractNameSpan").innerHTML = data.d[19];
                            document.getElementById('dvContractNameSpan').style.display = "block";
                        }
 
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
    function rowchoiceOffenceType(id, name, nameAR, typeId, tvalue) {
        document.getElementById('tbOffenceIdChoice').value = id;
        document.getElementById('tbEditOffenceName').value = name;
        document.getElementById('tbEditOffenceArName').value = nameAR;
        document.getElementById('tbEditOffenceValue').value = tvalue;
        document.getElementById('MainContent_typeEditSelect').value = typeId;
    }
    function rowchoiceOffenceCategory(id, name, nameAR, type) {
        document.getElementById('tbOffenceCatChoice').value = id;
        document.getElementById('tbEditCategoryName').value = name;
        document.getElementById('tbEditArCategoryName').value = nameAR;
        document.getElementById('MainContent_editoffenceCategorySelect').value = type;

    }
    function rowchoiceOffenceCategory2(id, name, nameAR) {
        document.getElementById('tbOffenceCatChoice').value = id;
        document.getElementById('tbEditCategoryName2').value = name;
        document.getElementById('tbEditArCategoryName2').value = nameAR;
    }
    function rowchoiceVehicleMake(id, name, nameAR) {
        document.getElementById('tbVehicleMakeChoice').value = id;
        document.getElementById('tbEditVehicleMakeName').value = name;
        document.getElementById('tbEditArVehicleMakeName').value = nameAR;
    }
    function rowchoiceVehicleColor(id, name, nameAR) {
        document.getElementById('tbVehicleColorChoice').value = id;
        document.getElementById('tbEditVehicleColorName').value = name;
        document.getElementById('tbEditArVehicleColorName').value = nameAR;
    }
    function rowchoicePlateCode(id, name, nameAR, typeId) {
        document.getElementById('tbPlateCodeChoice').value = id;
        document.getElementById('tbEditPlateCodeName').value = name;
        document.getElementById('tbEditArPlateCodeName').value = nameAR;
        document.getElementById('MainContent_typeEditSelectPlateSource').value = typeId;
    }
    function rowchoicePlateSource(id, name, nameAR) {
        document.getElementById('tbPlateSourceChoice').value = id;
        document.getElementById('tbEditPlateSourceName').value = name;
        document.getElementById('tbEditArPlateSourceName').value = nameAR;
    }
    function rowchoice(name) {

        startRot();
        document.getElementById('rowidChoiceTicket').value = name;
        document.getElementById('addticketNotesTA').style.display = 'none';
        document.getElementById('backTicketCardLi').style.display = 'block';
        document.getElementById('saveAsTemplateLi').style.display = 'none';

        document.getElementById('backTicketCardClose').style.display = 'block';
        document.getElementById('backTaskClose').style.display = 'none';
          
        assignrowDataTicket(name);
        getticketRemarks(name);
        getChecklistItemsTicket(name);
        ticketHistoryData(name);
        oldDivContainers();
        ticketinsertAttachmentIcons(name);
        ticketinsertAttachmentTabData(name);
        ticketinsertAttachmentData(name);
        ticketinfotabDefault();
        ticketdispatchAssignMapTab();
        getTasklistItemsTicket(name);
        hideAllTicketRemarks();
    }
    function generateTicketPDF() {
        width = 1;
        moveTick();
        document.getElementById("pdfloadingAcceptTick").style.display = "block";
        document.getElementById("gnNoteTick").innerHTML = "GENERATING REPORT";

        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/CreatePDFTicket",
            data: "{'id':'" + document.getElementById('rowidChoiceTicket').value + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else if (data.d != "Failed to generate report!") {
                    //jQuery('#ticketingViewCard').modal('hide');
                    //document.getElementById('successincidentScenario').innerHTML = "Report successfully created!";
                    //jQuery('#successfulDispatch').modal('show');
                    showAlert("Report successfully created!");
                    document.getElementById("pdfloadingAcceptTick").style.display = "none";
                    window.open(data.d);  
                }
                else {
                    showError(data.d);
                    document.getElementById("pdfloadingAcceptTick").style.display = "none";
                }
            }
        });
    }
    function ticketdispatchAssignMapTab() {
        var el = document.getElementById('video-0-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('ticketlocation-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('image-1-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
        var el5 = document.getElementById('image-2-tab');
        if (el5) {
            el5.className = 'tab-pane fade';
        }
        var el6 = document.getElementById('image-3-tab');
        if (el6) {
            el6.className = 'tab-pane fade';
        }
        var el7 = document.getElementById('image-4-tab');
        if (el7) {
            el7.className = 'tab-pane fade';
        }
        var el8 = document.getElementById('image-5-tab');
        if (el8) {
            el8.className = 'tab-pane fade';
        }
        var el9 = document.getElementById('image-6-tab');
        if (el9) {
            el9.className = 'tab-pane fade';
        }
        var el10 = document.getElementById('image-0-tab');
        if (el10) {
            el10.className = 'tab-pane fade';
        }

    }
    function ticketinsertAttachmentIcons(id) {
        jQuery('#ticketdivAttachment div').html('');
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getAttachmentDataIconsTicket",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("ticketdivAttachment").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
        function ticketinsertAttachmentTabData(id) {
        jQuery('#ticketattachments-info-tab div').html('');

        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getAttachmentDataTabTicket",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("ticketattachments-info-tab").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
        function ticketinsertAttachmentData(id) {
            document.getElementById('ticketrotationDIV1').style.display = "none";
            document.getElementById('ticketrotationDIV2').style.display = "none";
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/getAttachmentDataTicket",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        imgcount = 0;
                        //var div = document.createElement('div');
                        //div.className = 'tab-pane fade';
                        //div.innerHTML = data.d[0];
                        //div.id = 'video-0-tab';
                        //document.getElementById('ticketdivAttachmentHolder').appendChild(div);
                        //divArray[0] = 'video-0-tab';
                        for (var i = 0; i < data.d.length; i++) {

                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.align = 'center';
                            div.style.height = '380px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i) + '-tab';
                            document.getElementById('ticketdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'image-' + (i) + '-tab';
                            imgcount++;
                        }

                        if (imgcount > 0) {
                            document.getElementById('ticketrotationDIV1').style.display = "block";
                            document.getElementById('ticketrotationDIV2').style.display = "block";
                        }

                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        var imgcount = 0;
        function ticketnextImg() {
            var found = false;
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i + 1 < imgcount) {
                        var el3 = document.getElementById(divArray[i + 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("ticketlocation-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (divArray.length > 0) {
                    var ell = document.getElementById(divArray[0]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("ticketlocation-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }
        function ticketbackImg() {
            var found = false;
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i == 0) {
                        var elz = document.getElementById("ticketlocation-tab");
                        if (elz) {
                            elz.className = 'tab-pane fade active in';
                        }
                    }
                    else if (i - imgcount < imgcount) {
                        var el3 = document.getElementById(divArray[i - 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("ticketlocation-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (divArray.length > 0) {
                    var ell = document.getElementById(divArray[imgcount - 1]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("ticketlocation-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }

        function tasknextImg() {
            var found = false;
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i + 1 < imgcount) {
                        var el3 = document.getElementById(divArray[i + 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("tasklocation-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (divArray.length > 0) {
                    var ell = document.getElementById(divArray[0]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("tasklocation-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }
        function taskbackImg() {
            var found = false;
            for (var i = 0; i < divArray.length; i++) {
                var el = document.getElementById(divArray[i]);
                if (el.classList.contains("active")) {
                    el.className = 'tab-pane fade ';
                    if (i == 0) {
                        var elz = document.getElementById("tasklocation-tab");
                        if (elz) {
                            elz.className = 'tab-pane fade active in';
                        }
                    }
                    else if (i - imgcount < imgcount) {
                        var el3 = document.getElementById(divArray[i - 1]);
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    else {
                        var el3 = document.getElementById("tasklocation-tab");
                        if (el3) {
                            el3.className = 'tab-pane fade active in';
                        }
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (divArray.length > 0) {
                    var ell = document.getElementById(divArray[imgcount - 1]);
                    if (ell) {
                        ell.className = 'tab-pane fade active in';
                    }
                    var ell1 = document.getElementById("tasklocation-tab");
                    if (ell1) {
                        ell1.className = 'tab-pane fade';
                    }
                }
            }
        }

    function play(i) {
        try {
            var player = document.getElementById('Video0');
            player.play();
        } catch (error) {
            //alert('play-' + err);
        }
    }
    function getChecklistItemsTicket(id) {
        document.getElementById("offencesItemsList").innerHTML = "";
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getChecklistData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        var ul = document.getElementById("offencesItemsList");
                        var li = document.createElement("li");
                        li.appendChild(document.createTextNode(data.d[i]));
                        ul.appendChild(li);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });

    }
        function assignrowDataTicket(id) {
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getTableRowDataTicket",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                   // document.getElementById("platenumberSpan").innerHTML = data.d[0];
                   // document.getElementById("platesourceSpan").innerHTML = data.d[1];
                  //  document.getElementById("plateCodeSpan").innerHTML = data.d[2];
                  //  document.getElementById("vehicleMakeSpan").innerHTML = data.d[3];
                  //  document.getElementById("vehicleColorSpan").innerHTML = data.d[4];
                    document.getElementById("ticketusernameSpan").innerHTML = data.d[5];
                    document.getElementById("ticketlocSpan").innerHTML = data.d[6];
                    document.getElementById("tickettimeSpan").innerHTML = data.d[7];
                    document.getElementById("offcatSpan").innerHTML = data.d[8];
                    document.getElementById("offtypeSpan").innerHTML = data.d[9];
                    document.getElementById("liTicketReport").style.display = "none";
                    document.getElementById("liTicketResolve").style.display = "none";
                    document.getElementById("liTicketOpen").style.display = "none";
                    if (data.d[10] == "0") {
                        unhandledClickTicket();
                        document.getElementById("liTicketBack").style.display = "none";
                        document.getElementById("liTicketTask").style.display = "none";
                        document.getElementById("liTicketEnd").style.display = "none";
                        document.getElementById("liTicketOpen").style.display = "none";
                        document.getElementById("liTicketStart").style.display = "block";

                    } else if (data.d[10] == "3") {
                        handledClickTicket(); 
                        document.getElementById("liTicketBack").style.display = "none";

                        if ('<%=tskOptionView%>' == "block")
                            document.getElementById("liTicketTask").style.display = "block";

                        document.getElementById("liTicketStart").style.display = "none";
                        document.getElementById("liTicketEnd").style.display = "block";
                        document.getElementById("liTicketOpen").style.display = "none";

                    } else if (data.d[10] == "4") {
                        handledClickTicket();
                        document.getElementById("liTicketBack").style.display = "none";
                        document.getElementById("liTicketTask").style.display = "none";
                        document.getElementById("liTicketStart").style.display = "none";
                        document.getElementById("liTicketEnd").style.display = "none";
                        document.getElementById("liTicketReport").style.display = "block";
                        document.getElementById("liTicketOpen").style.display = "block";
                        if (data.d[16] == "0") {
                            document.getElementById("liTicketResolve").style.display = "none";
                        }
                        else {
                            document.getElementById("liTicketResolve").style.display = "block";
                        }
                        //liTicketResolve
                    }
                    else if (data.d[10] == "5") {
                        handledClickTicket();
                        document.getElementById("liTicketBack").style.display = "none";
                        document.getElementById("liTicketTask").style.display = "none";
                        document.getElementById("liTicketStart").style.display = "none";
                        document.getElementById("liTicketEnd").style.display = "none";
                        document.getElementById("liTicketOpen").style.display = "none";
                        document.getElementById("liTicketResolve").style.display = "none";
                        document.getElementById("liTicketReport").style.display = "block";
                        
                        //
                    }
                    else if (data.d[10] == "6") {
                        handledClickTicket();
                        document.getElementById("liTicketBack").style.display = "none";
                        document.getElementById("liTicketTask").style.display = "none";
                        document.getElementById("liTicketStart").style.display = "none";
                        document.getElementById("liTicketEnd").style.display = "none";
                        document.getElementById("liTicketOpen").style.display = "none";
                        document.getElementById("liTicketResolve").style.display = "none";
                        document.getElementById("liTicketReport").style.display = "none"; 
                    }
                    document.getElementById("ticketCommentSpan").innerHTML = data.d[11];
                    document.getElementById('dvticketAccountSpan').style.display = "none";
                    document.getElementById('dvticketContract').style.display = "none";
                    if (data.d[12] != "N/A") {
                        document.getElementById("ticketAccountSpan").innerHTML = data.d[12];
                        document.getElementById('dvticketAccountSpan').style.display = "block";
                    }
                    if (data.d[13] != "N/A") {
                        document.getElementById("ticketContract").innerHTML = data.d[13];
                        document.getElementById('dvticketContract').style.display = "block";
                    }
                    document.getElementById("ticketcontractid").value = data.d[14];
                    document.getElementById("ticketcustomerid").value = data.d[15];
                    document.getElementById("ticketstatusSpan").innerHTML = data.d[17];
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
        function saveTicketingRemarks() {
            var projn = document.getElementById("addticketNotesTA").value;
            var id = document.getElementById('rowidChoiceTicket').value;
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('addticketNotesTA').value)) {
                isPass = false;
                showAlert("Kindly provide notes to be added");
            }
            else {
                if (isSpecialChar(document.getElementById('addticketNotesTA').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character from notes");
                }
            }
            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/addNewTicketingRemarks",
                    data: "{'id':'" + id + "','notes':'" + projn + "','uname':'" + loggedinUsername  + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            showAlert("Successfully added notes");
                            document.getElementById("addticketNotesTA").value = "";
                            getticketRemarks(id);
                            document.getElementById('addticketNotesTA').style.display = 'none';
                        }
                        else if (data.d == "LOGOUT") {
                            document.getElementById('<%= logoutbtn.ClientID %>').click();
                        }
                        else {
                            showAlert('Failed to save notes. ' + data.d);
                        }
                    }
                });
            }
        }
        function addNotesToTicket() {
            if (document.getElementById('addticketNotesTA').style.display != 'block') {
                document.getElementById('addticketNotesTA').style.display = 'block';
            }
            else {
                saveTicketingRemarks();
            }
        }
        function showAllTicketingRemarks(id) {
            document.getElementById("ticketrotationDIV1").style.display = "none";
            document.getElementById("ticketrotationDIV2").style.display = "none";
            var el2 = document.getElementById('ticketlocation-tab');
                if (el2) {
                    el2.className = 'tab-pane fade';
                }
                var el = document.getElementById('ticketremarks-tab');
                if (el) {
                    el.className = 'tab-pane fade active in';
                }

                for (var i = 0; i < divArray.length; i++) {
                    var el2 = document.getElementById(divArray[i]);
                    el2.className = 'tab-pane fade';
                }

                jQuery('#ticketRemarksList2 div').html('');
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getTicketRemarksData2",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('ticketRemarksList2').appendChild(div);
                        }
                    }
                });
 
        }
        function hideAllTicketRemarks() {

            document.getElementById("ticketrotationDIV1").style.display = "block";
            document.getElementById("ticketrotationDIV2").style.display = "block";

            var el2 = document.getElementById('ticketlocation-tab');
            if (el2) {
                el2.className = 'tab-pane fade active in';
            }
            var el = document.getElementById('ticketremarks-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
        }

        function gettaskRemarks(id) {
            jQuery('#taskRemarksList div').html('');
            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getTaskRemarksData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('taskRemarksList').appendChild(div);
                    }
                }
            });
        }
        function hideAllRemarks() {
            var el2 = document.getElementById('tasklocation-tab');
            if (el2) {
                el2.className = 'tab-pane fade active in';
            }
            var el = document.getElementById('tremarks-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
            document.getElementById("taskrotationDIV1").style.display = "block";
            document.getElementById("taskrotationDIV2").style.display = "block";
        }
        function showAllRemarks(id) {
            document.getElementById("taskrotationDIV1").style.display = "none";
            document.getElementById("taskrotationDIV2").style.display = "none";
            if (document.getElementById('taskrejection-tab').className == "tab-pane fade active in") {

            }
            else {
                var el2 = document.getElementById('tasklocation-tab');
                if (el2) {
                    el2.className = 'tab-pane fade';
                }
                var el = document.getElementById('tremarks-tab');
                if (el) {
                    el.className = 'tab-pane fade active in';
                }

                for (var i = 0; i < divArray.length; i++) {
                    var el2 = document.getElementById(divArray[i]);
                    el2.className = 'tab-pane fade';
                }

                jQuery('#taskRemarksList2 div').html('');
                jQuery.ajax({
                    type: "POST",
                    url: "Ticketing.aspx/getTaskRemarksData2",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('taskRemarksList2').appendChild(div);
                        }
                    }
                });
            }
        }
        function getticketRemarks(id) {
            jQuery('#ticketRemarksList div').html('');
            $.ajax({
                type: "POST",
                url: "Ticketing.aspx/getTicketRemarksData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('ticketRemarksList').appendChild(div);
                    }
                }
            });
        }
        function ticketHistoryData(id) {
            jQuery('#divTicketActivity div').html('');
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/geTicketHistoryData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('divTicketActivity').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addrowtoTableResolved() {
            jQuery("#resolvedticketingTable tbody").empty();
            jQuery("#resolvedticketingTable").dataTable().fnClearTable();
            jQuery("#resolvedticketingTable").dataTable().fnDraw();
            jQuery("#resolvedticketingTable").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "Ticketing.aspx/getTableDataTicketResolved",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#resolvedticketingTable tbody").append(data.d[i]);
                        }
                        jQuery("#resolvedticketingTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 10,
                            "order": [[4, "desc"]]
                        });
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    function addrowtoTable1() {
        jQuery("#ticketingTable tbody").empty(); 
        jQuery("#ticketingTable").dataTable().fnClearTable();
        jQuery("#ticketingTable").dataTable().fnDraw();
        jQuery("#ticketingTable").dataTable().fnDestroy();
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getTicketingData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#ticketingTable tbody").append(data.d[i]);
                    }
                    jQuery("#ticketingTable").DataTable({
                        "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                        'iDisplayLength': 10,
                        "order": [[4, "desc"]]
                    });
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTable2() {
        jQuery("#offenceTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getOffenceData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#offenceTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTable3() {
        jQuery("#offenceTypeTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getOffenceTypeData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#offenceTypeTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableCat() {
        jQuery("#offenceCategoryTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getOffenceCategoryData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#offenceCategoryTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }

    function addrowtoTable4() {
        jQuery("#vehiclemakeTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getVehicleMakeData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#vehiclemakeTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTable5() {
        jQuery("#vehiclecolorTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getVehicleColorData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#vehiclecolorTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTablePlateCode() {
        jQuery("#platecodeTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getPlateCodeData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#platecodeTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTablePlateSource() {

        jQuery("#platesourceTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "Ticketing.aspx/getPlateSourceData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#platesourceTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function getTicketLocationMarkers(obj) {


        locationAllowed = true;
        //setTimeout(function () {
        google.maps.visualRefresh = true;
        var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 15,
            center: Liverpool,
            mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
        };

        // This makes the div with id "map_canvas" a google map
        mapIncidentLocation = new google.maps.Map(document.getElementById("map_canvasTicketLocation"), mapOptions);

        for (var i = 0; i < obj.length; i++) {

            var contentString = '<div id="content">' + obj[i].Username +
            '<br/></div>';

            var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

            var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
            marker.setIcon('../Images/marker.png')
            myMarkersIncidentLocation[obj[i].Username] = marker;
            createInfoWindowIncidentLocation(marker, contentString);
        }
    }
    function createInfoWindowIncidentLocation(marker, popupContent) {
        google.maps.event.addListener(marker, 'click', function () {
            infoWindowIncidentLocation.setContent(popupContent);
            infoWindowIncidentLocation.open(mapIncidentLocation, this);
        });
    }
    </script>
            <section class="content-wrapper" role="main">
            <div class="content">
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">TICKETING</span></h3>
                                </div>
                                <div class="col-md-7">
                                    <div class="panel-control">
                                        <ul id="demo3-tabs" class="nav nav-tabs nav-main">
                                            <li class="active"><a data-toggle="tab" onclick="jQuery('.show-component').show();" href="#home-tab">Tickets</a>
                                            </li>
                                            <li style="display:<%=userinfoDisplay%>"><a data-toggle="tab" onclick="jQuery('.show-component').show();" href="#subtab1-tab">Settings</a>
                                            </li>
                                            <li style="display:none;"><a data-toggle="tab" onclick="jQuery('.show-component').show();" href="#subtab2-tab">Vehicles</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div role="group" class="pull-right">
                                        <%=siteName%>
                                        <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                    <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
							<div class="tab-pane fade active in" id="home-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li><a href="#" data-target="#newTicketDocument" data-toggle="modal" class="capitalize-text" id="clearTicketInfoID">+ NEW TICKET</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#" data-target="#newOffenceCategoryModal2" data-toggle="modal" class="capitalize-text">+ NEW CATEGORY</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#" data-target="#newOffenceCategoryModal" data-toggle="modal" class="capitalize-text">+ NEW TYPE</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>;"><a href="#" id="firstModal" data-target="#newOffenceModal" data-toggle="modal" class="capitalize-text">+ NEW ITEM</a>
                                                    </li>
                                                 
                                                    <li style="display:none;"><a href="#" data-target="#newVehicleMakeModal" data-toggle="modal" class="capitalize-text">+ NEW MAKE</a>
                                                    </li>
                                                    <li style="display:none;"><a href="#" data-target="#newVehicleColorModal" data-toggle="modal" class="capitalize-text">+ NEW COLOR</a>
                                                    </li>
                                                    <li style="display:none;"><a href="#" data-target="#newPlateCode" data-toggle="modal" class="capitalize-text">+ NEW PLATE CODE</a>
                                                    </li>
                                                    <li style="display:none;"><a href="#" data-target="#newPlateSourceModal" data-toggle="modal" class="capitalize-text">+ NEW PLATE SOURCE</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row horizontal-chart">

                                            <div class="col-md-2 text-center panel-seperator panel-heading">
                                                <h3 class="panel-title capitalize-text">STATUS</h3>
                                            </div>

                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">PENDING</p>
                                                </div>
                                                <div class="inline-block">
                                                   <div class="easyPieChart" data-percent="<%=pendingPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f44e4b">
                                                        <span class="percentage text-dark fa fa-1x">
														<span class="data-percent"><asp:Label ID="lbPendingpercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                   </div>
                                                </div>
                                                <div class="inline-block ">
                                                    <h3><asp:Label ID="lbPending" runat="server"></asp:Label></h3>

                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">IN PROGRESS</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart"  data-percent="<%=inprogressPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#f2c400">
                                                        <span class="percentage text-dark fa fa-1x">
														  <span class="data-percent"><asp:Label ID="lbInprogresspercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbInprogress" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text">COMPLETED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" data-percent="<%=completedPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#3ebb64">
                                                        <span class="percentage text-dark fa fa-1x">
																		<span class="data-percent"><asp:Label ID="lbCompletedpercent" runat="server"></asp:Label></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbCompleted" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 panel-seperator">
                                                <div class="help-block">
                                                    <p class="capitalize-text" >RESOLVED</p>
                                                </div>
                                                <div class="inline-block">
                                                    <div class="easyPieChart" data-percent="<%=resolvedPercent%>" data-size="45" data-line-width="3" data-line-cap="square" data-scale-color="false" data-track-color="#F5F7FA" data-bar-color="#1b93c0">
                                                        <span class="percentage text-dark fa fa-1x">
																	<span class="data-percent"><%=resolvedPercent%></span>%
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="inline-block">
                                                    <h3><asp:Label ID="lbresolvedPercent" runat="server"></asp:Label></h3>
                                                </div>
                                            </div>
                                            <div class="col-md-2 chart-total">
                                                <p><span class="capitalize-text"><asp:Label ID="lbTotalAlarms" runat="server"></asp:Label></span>TOTAL TICKETS</p>
                                            </div>
                                        </div>
                                        <div class="row show-component component-number-1">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">TICKETS</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="ticketingTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->

                                        <div class="row show-component component-resolved">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">RESOLVED TICKETS</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="resolvedticketingTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ID">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">USER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
							</div>
							<div class="tab-pane fade" id="subtab1-tab">
                               <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                     <li><a href="#component-offencecategory2" data-toggle="tab">Category</a>
                                                    </li>
                                                    <li><a href="#component-offencecategory" data-toggle="tab">Type</a>
                                                    </li>
                                                    <li><a href="#component-offencetype" data-toggle="tab">Item</a>
                                                    </li>
                                                   
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#" data-target="#newOffenceCategoryModal2" data-toggle="modal" class="capitalize-text">+ NEW CATEGORY</a>
                                                    </li>

                                                    <li style="display:<%=userinfoDisplay%>"><a href="#" data-target="#newOffenceCategoryModal" data-toggle="modal" class="capitalize-text">+ NEW TYPE</a>
                                                    </li>
                                                    
                                                    <li style="display:<%=userinfoDisplay%>" ><a href="#" data-target="#newOffenceModal" data-toggle="modal" class="capitalize-text">+ NEW ITEM</a>
                                                    </li>

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-offencecategory2">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">CATEGORY</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="offenceCategoryTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">DESC<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                       
                                        <!-- /.table -->
                                        <div class="row show-component component-offencecategory">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">TYPE</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="offenceTypeTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">DESC<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">CATEGORY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-offencetype">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ITEM</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="offenceTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="VALUE">VALUE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="subtab2-tab">
                               <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab" >All</a>
                                                    </li>
                                                    <li><a href="#component-vehiclemake" data-toggle="tab">Vehicle Make</a>
                                                    </li>
                                                    <li><a href="#component-vehiclecolor" data-toggle="tab">Vehicle Color</a>
                                                    </li>
                                                    <li><a href="#component-platecode" data-toggle="tab">Plate Code</a>
                                                    </li>
                                                    <li><a href="#component-platesource" data-toggle="tab">Plate Source</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li style="display:<%=userinfoDisplay%>" ><a href="#" data-target="#newVehicleMakeModal" data-toggle="modal" class="capitalize-text">+ NEW MAKE</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#" data-target="#newVehicleColorModal" data-toggle="modal" class="capitalize-text">+ NEW COLOR</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#" data-target="#newPlateCode" data-toggle="modal" class="capitalize-text">+ NEW PLATE CODE</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>"><a href="#" data-target="#newPlateSourceModal" data-toggle="modal" class="capitalize-text">+ NEW PLATE SOURCE</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-vehiclemake">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">VEHICLE MAKE</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="vehiclemakeTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">DESC<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-vehiclecolor">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">VEHICLE COLOR</h3>
                                                           <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="vehiclecolorTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">DESC<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-platecode">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">PLATE CODE</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="platecodeTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">DESC<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TYPE">PLATE SOURCE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-platesource">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">PLATE SOURCE</h3>
                                                           <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="platesourceTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">DESC<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
                                                            <div class="tab-content">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div> 
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                  			<label class="select select-o" >
                                                                              <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>        
                                                            <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                                      <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Contract</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                   <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                         <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                        
                                            </div>
                                                         <div class="row" style="display:none;">
                                            <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                                </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </div>
                <!-- /panel-body --> 
            </div>
            <!-- /.panel -->

                <NewTask:MyNewTask id="NewTaskControl" runat="server" />
                <TicketingCard:MyTicketingCard id="TicketingCardControl" runat="server" />
                <AddOnTaskCard:MyAddOnTaskCard id="AddOnTaskCardControl" runat="server" />
                <NewTicket:MyNewTicket id="NewTicketControl" runat="server" />
                
			<div aria-hidden="true" aria-labelledby="newOffenceModal" role="dialog" tabindex="-1" id="newOffenceModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW ITEM</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbArName" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
										<input placeholder="Value" id="tbValue" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="typeSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>				
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"  onclick="addOffenceTypeSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>		

            <div aria-hidden="true" aria-labelledby="editOffenceModal" role="dialog" tabindex="-1" id="editOffenceModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT ITEM</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditOffenceName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbEditOffenceArName" class="form-control">
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
										<input placeholder="Value" id="tbEditOffenceValue" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="typeEditSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>				
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"  onclick="editOffenceTypeSave()" class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
			 
			<div aria-hidden="true" aria-labelledby="newOffenceCategoryModal" role="dialog" tabindex="-1" id="newOffenceCategoryModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW TYPE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbCategoryName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbArCategoryName" class="form-control">
									</div>
								</div>	
                                <div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="offenceCategorySelect" runat="server">
											</select>
										 </label>
									</div>
								</div>		
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"   onclick="addOffenceCatTypeSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                			<div aria-hidden="true" aria-labelledby="newOffenceCategoryModal2" role="dialog" tabindex="-1" id="newOffenceCategoryModal2" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW CATEGORY</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbCategoryName2" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbArCategoryName2" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"   onclick="addOffenceCatTypeSave2()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="EditOffenceCategoryModal" role="dialog" tabindex="-1" id="EditOffenceCategoryModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT TYPE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditCategoryName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbEditArCategoryName" class="form-control">
									</div>
								</div>	
                                <div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="editoffenceCategorySelect" runat="server">
											</select>
										 </label>
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"  onclick="editOffenceCatTypeSave()" class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                
            <div aria-hidden="true" aria-labelledby="EditOffenceCategoryModal2" role="dialog" tabindex="-1" id="EditOffenceCategoryModal2" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT CATEGORY</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditCategoryName2" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbEditArCategoryName2" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"  onclick="editOffenceCatTypeSave2()" class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
		 	<div aria-hidden="true" aria-labelledby="newVehicleMakeModal" role="dialog" tabindex="-1" id="newVehicleMakeModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW VEHICLE MAKE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbVehicleMakeName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbArVehicleMakeName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#" onclick="addVehMakeSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

            <div aria-hidden="true" aria-labelledby="editVehicleMakeModal" role="dialog" tabindex="-1" id="editVehicleMakeModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT VEHICLE MAKE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditVehicleMakeName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbEditArVehicleMakeName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#" onclick="editVehMakeSave()"  class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

            <div aria-hidden="true" aria-labelledby="newVehicleColorModal" role="dialog" tabindex="-1" id="newVehicleColorModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW VEHICLE COLOR</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbVehicleColorName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbArVehicleColorName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"  onclick="addVehColorSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

            <div aria-hidden="true" aria-labelledby="editVehicleColorModal" role="dialog" tabindex="-1" id="editVehicleColorModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT VEHICLE COLOR</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditVehicleColorName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbEditArVehicleColorName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#" onclick="editVehColorSave()"  class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

                
            <div aria-hidden="true" aria-labelledby="newPlateCode" role="dialog" tabindex="-1" id="newPlateCode" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE PLATE CODE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbPlateCodeName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbArPlateCodeName" class="form-control">
									</div>
								</div>	
                                <div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="typeSelectPlateSource" runat="server">
											</select>
										 </label>
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li ><a href="#"  onclick="addPlateCodeSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

            <div aria-hidden="true" aria-labelledby="editPlateCodeModal" role="dialog" tabindex="-1" id="editPlateCodeModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT PLATE CODE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditPlateCodeName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbEditArPlateCodeName" class="form-control">
									</div>
								</div>	
                                <div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="typeEditSelectPlateSource" runat="server">
											</select>
										 </label>
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li ><a href="#" onclick="editPlateCodeSave()" data-dismiss="modal" class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

            <div aria-hidden="true" aria-labelledby="newPlateSourceModal" role="dialog" tabindex="-1" id="newPlateSourceModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE PLATE SOURCE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbPlateSourceName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbArPlateSourceName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li ><a href="#"  onclick="addPlateSourceSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	

            <div aria-hidden="true" aria-labelledby="editPlateSourceModal" role="dialog" tabindex="-1" id="editPlateSourceModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT PLATE SOURCE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditPlateSourceName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Description" id="tbEditArPlateSourceName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li ><a href="#" onclick="editPlateSourceSave()" data-dismiss="modal" class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>


            <div aria-hidden="true" aria-labelledby="deleteOffenceTypeModal" role="dialog" tabindex="-1" id="deleteOffenceTypeModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delOffenceTypeSave()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>			
            <div aria-hidden="true" aria-labelledby="deleteOffenceCatModal" role="dialog" tabindex="-1" id="deleteOffenceCatModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delOffenceCatTypeSave()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="deleteOffenceCatModal2" role="dialog" tabindex="-1" id="deleteOffenceCatModal2" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delOffenceCatTypeSave2()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="deleteVehColorModal" role="dialog" tabindex="-1" id="deleteVehColorModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delVehColorSave()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	   
            <div aria-hidden="true" aria-labelledby="deleteVehMakeModal" role="dialog" tabindex="-1" id="deleteVehMakeModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delVehMakeSave()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	  
            <div aria-hidden="true" aria-labelledby="deletePlateCode" role="dialog" tabindex="-1" id="deletePlateCode" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delPlateCode()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->  
			 </div>	   
            <div aria-hidden="true" aria-labelledby="deletePlateSouce" role="dialog" tabindex="-1" id="deletePlateSouce" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delPlateSouce()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                                <div aria-hidden="true" aria-labelledby="deleteAttachTicketModal" role="dialog" tabindex="-1" id="deleteAttachTicketModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#ticketingViewCard').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#ticketingViewCard').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachmentTicket()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="successfulModal" role="dialog" tabindex="-1" id="successfulModal" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="../Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successMessage"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="closeModal();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="../Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li ><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                                         <form style="display:none" enctype="multipart/form-data" id="dz-postticket" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </form>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 

                

                 			  <input style="display:none;" id="imagePostAttachment" type="text"/>
              <input id="tbOffenceIdChoice" style="display:none;">
                            <select style="display:none;" multiple="multiple" id="sendToListBox" ></select>	
                <input id="rowidChoiceTicket" style="display:none;" value=""> 
                <input id="tbOffenceCatChoice" style="display:none;">
                <input id="tbVehicleMakeChoice" style="display:none;">
                <input id="tbVehicleColorChoice" style="display:none;">
                <input id="tbPlateCodeChoice" style="display:none;"> 
                <input id="tbPlateSourceChoice" style="display:none;">
                <input style="display:none;" id="imagePath" type="text"/>
                 <input id="rowidChoice" style="display:none;">
                <input id="ticketcontractid" style="display:none;"> 
                <input id="ticketcustomerid" style="display:none;">
                <input id="rowChoiceTasks" style="display:none;">
                 <input id="rowidChoiceAttachment" style="display:none;">
                
              <asp:HiddenField runat="server" ID="tbUserID" />
              <asp:HiddenField runat="server" ID="tbUserName" />
             </section>
</asp:Content>
