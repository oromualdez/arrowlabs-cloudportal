﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
   public class CustomerSystemTypeAudit
    {
        public int Id { get; set; }
        public BaseAudit BaseAudit { get; set; }
        public int BaseAuditId { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }
        public string Brand { get; set; }
        public int SiteId { get; set; }
        public int CustomerInfoId { get; set; }
        public static int InsertCustomerSystemTypeAudit(CustomerSystemTypeAudit customerAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(customerAudit.BaseAudit, dbConnection);

            int id = 0;
            if (customerAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomerSystemTypeAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customerAudit.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = customerAudit.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = customerAudit.Description;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customerAudit.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = customerAudit.UpdatedDate;


                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customerAudit.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customerAudit.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Brand", SqlDbType.NVarChar));
                    cmd.Parameters["@Brand"].Value = customerAudit.Brand;

                    cmd.Parameters.Add(new SqlParameter("@Version", SqlDbType.NVarChar));
                    cmd.Parameters["@Version"].Value = customerAudit.Version;

                    cmd.Parameters.Add(new SqlParameter("@Model", SqlDbType.NVarChar));
                    cmd.Parameters["@Model"].Value = customerAudit.Model;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customerAudit.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertCustomerSystemTypeAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
