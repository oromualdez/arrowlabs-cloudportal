﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class UserImage
    {
        public int UserId { get; set; }
        public string ImagePath { get; set; }
        [DataMember]
        public byte[] ImageFile { get; set; }
        public string CreatedBy { get; set; }
        public string AccountName { get; set; }
        public DateTime? CreatedDate { get; set; }
        
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public static bool InsertUserImage(int UserId, string ImagePath, byte[] ImageFile, int siteId, int customerid, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("InsertUserImage", connection);

                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = UserId;

                cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                cmd.Parameters["@ImagePath"].Value = ImagePath;

                cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                cmd.Parameters["@SiteId"].Value = siteId;

                cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                cmd.Parameters["@CustomerId"].Value = customerid;


                if (ImageFile != null)
                {
                    cmd.Parameters.Add(new SqlParameter("@ImageFile", SqlDbType.VarBinary));
                    cmd.Parameters["@ImageFile"].Value = ImageFile;
                }

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "InsertUserImage";

                cmd.ExecuteNonQuery();
            } 
            return true;
        }
        public static UserImage GetUserImageByUserId(int userid, string dbConnection)
        {
            UserImage userimg = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetUserImageByUserId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = userid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetUserImageByUserId";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    userimg = new UserImage();
                    if (reader["UserId"] != DBNull.Value)
                        userimg.UserId = Convert.ToInt32(reader["UserId"].ToString());
                    if (reader["ImagePath"] != DBNull.Value)
                        userimg.ImagePath = reader["ImagePath"].ToString();
                    if (reader["AccountName"] != DBNull.Value)
                        userimg.AccountName = reader["AccountName"].ToString();
                    if (reader["CreatedBy"] != DBNull.Value)
                        userimg.CreatedBy = reader["CreatedBy"].ToString();
                    if (reader["ImageFile"] != DBNull.Value)
                        userimg.ImageFile = (byte[])reader["ImageFile"];
                    if (reader["CreatedDate"] != DBNull.Value)
                        userimg.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

                }
                connection.Close();
                reader.Close();
            }
            return userimg;
        }
        
    }
}
