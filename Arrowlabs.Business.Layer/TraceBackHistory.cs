﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class TraceBackHistory
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int TaskId { get; set; }
        public int IncidentId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int UserId { get; set; }
        public int Status { get; set; } //TaskJobActivity Enum 
        public int SiteId { get; set; }
        public int CustomerId { get; set; }

        public string Marker { get; set; }

        private static TraceBackHistory GetDutyTraceBackHistorySqlReader(SqlDataReader reader)
        {
            var traceBackHistory = new TraceBackHistory();

            if (reader["Id"] != DBNull.Value)
                traceBackHistory.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Latitude"] != DBNull.Value)
                traceBackHistory.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
            if (reader["Longitude"] != DBNull.Value)
                traceBackHistory.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
            if (reader["TaskId"] != DBNull.Value)
                traceBackHistory.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
            if (reader["IncidentId"] != DBNull.Value)
                traceBackHistory.IncidentId = Convert.ToInt32(reader["IncidentId"].ToString());
            if (reader["UserId"] != DBNull.Value)
                traceBackHistory.UserId = Convert.ToInt32(reader["UserId"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                traceBackHistory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["Status"] != DBNull.Value)
                traceBackHistory.Status = Convert.ToInt32(reader["Status"].ToString());

            traceBackHistory.Marker = "https://testportalcdn.azureedge.net/Images/bluesmall.png";

            if (traceBackHistory.Status == (int)TaskJobAction.OnRoute)
            {
                traceBackHistory.Marker = "https://testportalcdn.azureedge.net/Images/whitegreenball.png";
            }
            else if (traceBackHistory.Status == (int)TaskJobAction.Pause)
            {
                traceBackHistory.Marker = "https://testportalcdn.azureedge.net/Images/whiteredball.png";
            }
            else if (traceBackHistory.Status == (int)TaskJobAction.Resume)
            {
                traceBackHistory.Marker = "https://testportalcdn.azureedge.net/Images/whiteblueball.png";
            }
            else if (traceBackHistory.Status == (int)TaskJobAction.InProgress)
            {
                traceBackHistory.Marker = "https://testportalcdn.azureedge.net/Images/whiteyellowball.png";
            } 
            return traceBackHistory;
        }
        public static bool InsertOrUpdateTraceBackHistory(TraceBackHistory traceBackHistory, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {

            var id = 0;

            var action = (traceBackHistory.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;


            if (traceBackHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateTraceBackHistory", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = traceBackHistory.Id;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = traceBackHistory.UserId;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = traceBackHistory.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = traceBackHistory.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = traceBackHistory.Longitude;
                    
                    cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                    cmd.Parameters["@IncidentId"].Value = traceBackHistory.IncidentId;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = traceBackHistory.Status;
                      
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = traceBackHistory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = traceBackHistory.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = traceBackHistory.SiteId;


                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            traceBackHistory.Id = id;
                            var audit = new TraceBackHistoryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, traceBackHistory.UserId.ToString());
                            Reflection.CopyProperties(traceBackHistory, audit);
                            TraceBackHistoryAudit.InsertOrUpdateTraceBackHistory(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer Audit", "InsertOrUpdateTraceBackHistory", ex, dbConnection);
                    }
                }
            }
            return true;
        }
        public static List<TraceBackHistory> GetTracBackHistoryByUserName(int UserId, string dbConnection)
        {
            var traceBackHistories = new List<TraceBackHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTracBackHistoryByUserId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = UserId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var tracbackHistory = GetDutyTraceBackHistorySqlReader(reader);
                    traceBackHistories.Add(tracbackHistory);
                }
                connection.Close();
                reader.Close();
            }
            return traceBackHistories;
        }

        public static List<TraceBackHistory> GetTracBackHistoryBytaskId(int taskId, string dbConnection)
        {
            var traceBackHistories = new List<TraceBackHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTracBackHistoryByTaskId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                sqlCommand.Parameters["@TaskId"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var tracbackHistory = GetDutyTraceBackHistorySqlReader(reader);
                    traceBackHistories.Add(tracbackHistory);
                }
                connection.Close();
                reader.Close();
            }
            return traceBackHistories;
        }

        public static List<TraceBackHistory> GetTracBackHistoryByIncidentId(int incidentId, string dbConnection)
        {
            var traceBackHistories = new List<TraceBackHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTracBackHistoryByIncidentId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                sqlCommand.Parameters["@IncidentId"].Value = incidentId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var tracbackHistory = GetDutyTraceBackHistorySqlReader(reader);
                    traceBackHistories.Add(tracbackHistory);
                }
                connection.Close();
                reader.Close();
            }
            return traceBackHistories;
        }

        public static bool DeleteTraceBackHistoryByTaskId(int locationId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteTraceBackHistoryByTaskId", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = locationId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tracebackhistory", "DeleteTraceBackHistoryByTaskId", ex, dbConnection);
                return false;
            }
            return true;
        }
        public static bool DeleteTraceBackHistoryByIncidentId(int locationId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteTraceBackHistoryByIncidentId", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = locationId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tracebackhistory", "DeleteTraceBackHistoryByIncidentId", ex, dbConnection);
                return false;
            }
            return true;
        }
    }
}
