﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ItemDisposeAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public DateTime DisposedDate { get; set; }
        public string DisposedTo { get; set; }
        public string RecipientName { get; set; }
        public string RecipientCompany { get; set; }
        public string RecipientLocation { get; set; }
        public string RecipientContactNo { get; set; }
        public string RecipientIdNo { get; set; }
        public string Destroyed { get; set; }
        public string DisposedAfter { get; set; }
        public string DisposedBy { get; set; }
        public string Witness { get; set; }
        public string ImagePath { get; set; }
        public int ItemFoundId { get; set; }
        public string ItemReference { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        private static ItemDisposeAudit GetItemFoundFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var itemDispose = new ItemDisposeAudit();
            if (reader["Id"] != DBNull.Value)
                itemDispose.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["DisposedDate"] != DBNull.Value)
                itemDispose.DisposedDate = Convert.ToDateTime(reader["DisposedDate"].ToString());
            if (reader["DisposedTo"] != DBNull.Value)
                itemDispose.DisposedTo = reader["DisposedTo"].ToString();
            if (reader["RecipientName"] != DBNull.Value)
                itemDispose.RecipientName = reader["RecipientName"].ToString();
            if (reader["RecipientCompany"] != DBNull.Value)
                itemDispose.RecipientCompany = reader["RecipientCompany"].ToString();
            if (reader["RecipientLocation"] != DBNull.Value)
                itemDispose.RecipientLocation = reader["RecipientLocation"].ToString();
            if (reader["RecipientContactNo"] != DBNull.Value)
                itemDispose.RecipientContactNo = reader["RecipientContactNo"].ToString();
            if (reader["RecipientIdNo"] != DBNull.Value)
                itemDispose.RecipientIdNo = reader["RecipientIdNo"].ToString();
            if (reader["Destroyed"] != DBNull.Value)
                itemDispose.Destroyed = reader["Destroyed"].ToString();
            if (reader["DisposedAfter"] != DBNull.Value)
                itemDispose.DisposedAfter = reader["DisposedAfter"].ToString();
            if (reader["DisposedBy"] != DBNull.Value)
                itemDispose.DisposedBy = reader["DisposedBy"].ToString();
            if (reader["Witness"] != DBNull.Value)
                itemDispose.Witness = reader["Witness"].ToString();
            if (reader["ImagePath"] != DBNull.Value)
                itemDispose.ImagePath = reader["ImagePath"].ToString();
            if (reader["ItemFoundId"] != DBNull.Value)
                itemDispose.ItemFoundId = Convert.ToInt32(reader["ItemFoundId"].ToString());
            if (reader["SiteId"] != DBNull.Value)
                itemDispose.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["ItemReference"] != DBNull.Value)
                itemDispose.ItemReference = reader["ItemReference"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                itemDispose.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                itemDispose.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                itemDispose.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["UpdatedBy"] != DBNull.Value)
                itemDispose.UpdatedBy = reader["UpdatedBy"].ToString();

            return itemDispose;
        }

        public static void InsertorUpdateItemOwnerAudit(ItemDisposeAudit itemDispose, string auditDbConnection)
        {
            if (itemDispose != null)
            {
                var baseAuditId = BaseAudit.InsertBaseAudit(itemDispose.BaseAudit, auditDbConnection);

                using (var connection = new SqlConnection(auditDbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateItemDispose", connection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = itemDispose.Id;
                    cmd.Parameters.Add(new SqlParameter("@DisposedDate", SqlDbType.DateTime));
                    cmd.Parameters["@DisposedDate"].Value = itemDispose.DisposedDate;
                    cmd.Parameters.Add(new SqlParameter("@DisposedTo", SqlDbType.NVarChar));
                    cmd.Parameters["@DisposedTo"].Value = itemDispose.DisposedTo;
                    cmd.Parameters.Add(new SqlParameter("@RecipientName", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientName"].Value = itemDispose.RecipientName;
                    cmd.Parameters.Add(new SqlParameter("@RecipientCompany", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientCompany"].Value = itemDispose.RecipientCompany;
                    cmd.Parameters.Add(new SqlParameter("@RecipientLocation", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientLocation"].Value = itemDispose.RecipientLocation;
                    cmd.Parameters.Add(new SqlParameter("@RecipientContactNo", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientContactNo"].Value = itemDispose.RecipientContactNo;
                    cmd.Parameters.Add(new SqlParameter("@RecipientIdNo", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientIdNo"].Value = itemDispose.RecipientIdNo;
                    cmd.Parameters.Add(new SqlParameter("@Destroyed", SqlDbType.NVarChar));
                    cmd.Parameters["@Destroyed"].Value = itemDispose.Destroyed;
                    cmd.Parameters.Add(new SqlParameter("@DisposedAfter", SqlDbType.NVarChar));
                    cmd.Parameters["@DisposedAfter"].Value = itemDispose.DisposedAfter;
                    cmd.Parameters.Add(new SqlParameter("@DisposedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@DisposedBy"].Value = itemDispose.DisposedBy;
                    cmd.Parameters.Add(new SqlParameter("@Witness", SqlDbType.NVarChar));
                    cmd.Parameters["@Witness"].Value = itemDispose.Witness;
                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = itemDispose.ImagePath;
                    cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    cmd.Parameters["@ItemFoundId"].Value = itemDispose.ItemFoundId;
                    cmd.Parameters.Add(new SqlParameter("@ItemReference", SqlDbType.NVarChar));
                    cmd.Parameters["@ItemReference"].Value = itemDispose.ItemReference;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = itemDispose.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = itemDispose.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = itemDispose.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = itemDispose.UpdatedBy;
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = itemDispose.CustomerId;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = itemDispose.SiteId;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                }
            }

        }
    }
}
