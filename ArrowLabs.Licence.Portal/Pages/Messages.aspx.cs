﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Web.Services;
using System.Web.Configuration;
using System.Globalization;
using System.Text;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using System.Threading;
using ArrowLabs.Licence.Portal.Helpers;

namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class Messages : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static ClientLicence getClientLic;

        protected string senderName;
        protected string senderName2;
        protected string year;
        protected string month;
        protected string ipaddress;
        protected string isMessagesDisplay;
        protected string messagesDisplay;
        protected string remindersDisplay;
        protected string actmessagesDisplay;
        protected string actremindersDisplay;
        protected string userinfoDisplay;
        protected string senderName3;
        protected string siteName;
        [WebMethod]
        public static string pushNotificationSend(string id, string assigneename, string assigneetype, string name, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                if (assigneetype == TaskAssigneeType.Group.ToString())
                {
                    var thread = new Thread(() => CommonUtility.sendPushNotificationGroup(Convert.ToInt32(id), "Notification: " + name, dbConnection, (int)PushNotificationDevice.PushNotificationType.ChatMessage));
                    thread.Start();
                }
                else if (assigneetype == TaskAssigneeType.User.ToString())
                {
                    var pushDevInfo = PushNotificationDevice.GetPushNotificationDeviceByUsername(assigneename, dbConnection);
                    if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Apple)
                    {
                        Thread thread = new Thread(() => PushNotificationClient.SendtoAppleUserNotification("Notification: " + name, assigneename, dbConnection));

                        thread.Start();
                    }
                    else if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Android)
                    {
                        Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(assigneename, CommonUtility.androidpushApplicationID, CommonUtility.androidpushSenderId, pushDevInfo.DeviceToken, "Alarm", name, dbConnection, (int)PushNotificationDevice.PushNotificationType.ChatMessage));

                        thread.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages.aspx", "pushNotificationSend", ex, dbConnection, userinfo.SiteId);
                return "FAIL";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string sendpushMultipleNotification(string[] userIds, string msg,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                Thread thread = new Thread(() => CommonUtility.sendPushNotificationMessage(userIds, msg, dbConnection));
                thread.Start();

                //Thread thread = new Thread(() => PushNotificationClient.SendtoAppleMultiUserNotification("Notification: " + msg, userIds, dbConnection));
                //thread.Start();
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "sendpushMultipleNotification", ex, dbConnection, userinfo.SiteId);
                return "FAIL";
            }
            return "SUCCESS";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }

            userinfoDisplay = "block";

            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            var manager = User.Identity.Name;
            senderName = manager;
            senderName2 = Encrypt.EncryptData(manager, true, dbConnection);
            var userinfo = Users.GetUserByName(manager, dbConnection);
            try
            {

                year = CommonUtility.getDTNow().Year.ToString();
                month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(CommonUtility.getDTNow().Month);
                var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
                ipaddress = configtSettings.ChatServerUrl + "/signalr";
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                messagesDisplay = "class='active'";
                actmessagesDisplay = "active in";
                actremindersDisplay = string.Empty;
                isMessagesDisplay = string.Empty;
                remindersDisplay = string.Empty;
                if (getClientLic != null)
                {
                    if(!getClientLic.isNotification)
                    {
                        messagesDisplay = string.Empty;
                        remindersDisplay = "class='active'";
                        isMessagesDisplay = "style='display:none'";
                        actremindersDisplay = "active in";
                        actmessagesDisplay = string.Empty;
                    }
                }
                senderName3 = userinfo.CustomerUName;

                if (userinfo.SiteId > 0)
                {
                    siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                }

                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                    //if (pushDev != null)
                    //{
                    //    if (!string.IsNullOrEmpty(pushDev.Username))
                    //    {
                    //        pushDev.IsServerPortal = true;
                    //        pushDev.SiteId = userinfo.SiteId;
                    //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection,dbConnectionAudit,true);
                    //    }
                    //}
                    PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);
                }
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    userinfoDisplay = "none";
                    
                    if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        userinfoDisplay = "block";

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "Page_Load", ex, dbConnection, userinfo.SiteId);
            }
        }

        [WebMethod]
        public static List<string> getReminderSelectorDates(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                // Set the start time (00:00 means 12:00 AM)
                DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(7, 0, 0));
                // Set the end time (23:55 means 11:55 PM)
                DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(17, 0, 0));
                //Set 5 minutes interval
                TimeSpan Interval = new TimeSpan(1, 0, 0);
                //To set 1 hour interval
                //TimeSpan Interval = new TimeSpan(1, 0, 0);           

                while (StartTime <= EndTime)
                {
                    listy.Add(StartTime.ToString("HH:mm"));
                    StartTime = StartTime.Add(Interval);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getReminderSelectorDates", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getDateRangeReminder(string date,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var split = date.Split(':');
                // Set the start time (00:00 means 12:00 AM)
                DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(Convert.ToInt32(split[0]), 0, 0));
                // Set the end time (23:55 means 11:55 PM)
                DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(18, 0, 0));
                //Set 5 minutes interval
                TimeSpan Interval = new TimeSpan(1, 0, 0);
                //To set 1 hour interval
                //TimeSpan Interval = new TimeSpan(1, 0, 0);           
                //fromReminderDate.Items.Clear();
                //toReminderDate.Items.Clear();
                while (StartTime <= EndTime)
                {
                    listy.Add(StartTime.ToString("HH:mm"));
                    StartTime = StartTime.Add(Interval);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getDateRangeReminder", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getCalendarDays(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var prevmonth = 0;
                var prevyear = 0;
                var dtyear = CommonUtility.getDTNow().Year;
                prevyear = CommonUtility.getDTNow().Year;
                if (id == 99)
                    id = CommonUtility.getDTNow().Month;
                else if (id > 12)
                {
                    id = 1;
                    prevyear = CommonUtility.getDTNow().AddYears(1).Year;
                    dtyear = CommonUtility.getDTNow().AddYears(1).Year;
                }
                else if (id == 0)
                    id = 12;

                prevmonth = id - 1;
                if (prevmonth == 0)
                {
                    prevmonth = 12;
                    prevyear = prevyear - 1;
                }

                var firstDayOfMonth = new DateTime(dtyear, id, 1);
                var previousMonth = DateTime.DaysInMonth(prevyear, prevmonth);
                var days = DateTime.DaysInMonth(dtyear, id);
                var startday = firstDayOfMonth.ToString("dddd");
                var adddays = 0;
                if (startday == "Monday")
                {
                    adddays = 1;
                }
                else if (startday == "Tuesday")
                {
                    adddays = 2;
                }
                else if (startday == "Wednesday")
                {
                    adddays = 3;
                }
                else if (startday == "Thursday")
                {
                    adddays = 4;
                }
                else if (startday == "Friday")
                {
                    adddays = 5;
                }
                else if (startday == "Saturday")
                {
                    adddays = 6;
                }

                var retString = string.Empty;
                var firstprev = false;
                for (var i = 1; i < days + 1 + adddays; i++)
                {
                    var IsEvent = "";
                    var today = CommonUtility.getDTNow();
                    var day = "";
                    if ((i - adddays) > 0)
                    {
                        day = (i - adddays).ToString();
                        if ((i - adddays) <= days)
                        {
                            var DayOfMonth = new DateTime(dtyear, id, (i - adddays));
                            var reminder = Reminders.GetAllRemindersByDateAndCreator(DayOfMonth, userinfo.Username, dbConnection);
                            if (reminder.Count > 0)
                                IsEvent = "event";
                        }
                        if (today.Date == new DateTime(dtyear, id, (i - adddays)))
                            retString += "<td class='day today " + IsEvent + "'><div class='day-contents' onclick='getReminderTableData(&apos;" + new DateTime(dtyear, id, (i - adddays)) + "&apos;,&apos;&apos;)'>" + day + "</div></td>";
                        else
                            retString += "<td class='day " + IsEvent + "'><div class='day-contents' onclick='getReminderTableData(&apos;" + new DateTime(dtyear, id, (i - adddays)) + "&apos;,&apos;&apos;)'>" + day + "</div></td>";
                    }
                    else
                    {

                        if (!firstprev)
                        {
                            previousMonth = previousMonth - adddays + 1;
                            firstprev = true;
                        }
                        else { previousMonth = previousMonth + 1; }
                        var DayOfMonth = new DateTime(prevyear, prevmonth, previousMonth);
                        var reminder = Reminders.GetAllRemindersByDateAndCreator(DayOfMonth, userinfo.Username, dbConnection);
                        if (reminder.Count > 0)
                            IsEvent = "event";
                        retString += "<td class='day past adjacent-month last-month " + IsEvent + "'><div class='day-contents' onclick='getReminderTableData(&apos;" + DayOfMonth + "&apos;,&apos;&apos;)'>" + previousMonth + "</div></td>";


                    }
                    if (i == 7 || i == 14 || i == 21 || i == 28 || (i == (days + adddays) || i == 35))
                    {
                        if (i == (days + adddays))
                        {
                            var endDayOfMonth = new DateTime(dtyear, id, days);
                            var endday = endDayOfMonth.ToString("dddd");
                            if (endday != "Saturday")
                                retString += "<td class='day '><div class='day-contents'></div></td>";

                        }
                        listy.Add(retString);
                        retString = string.Empty;
                    }
                    if (i == 35)
                        break;
                }
                listy.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(id) + " " + dtyear.ToString());
                listy.Add(id.ToString());
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getCalendarDays", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        public static string getCalendarTD()
        {
            return string.Empty;
        }
        [WebMethod]
        public static List<string> getReminders(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                //if(userinfo)
                //var getreminders = Reminders.GetAllReminders(dbConnection);
                var getreminders = Reminders.GetAllRemindersByDateAndCreator(CommonUtility.getDTNow().Date, userinfo.Username, dbConnection);
                //json = ;//'<time style="margin-left:15px">8:00 am, December 31, 2015</time><p style="margin-left:15px">Send new updates to Orry sasdasssssssss ssssssssssssss ssssssssssssssssss ssssssssssss</p><div style="margin-left:-5px" class="vertical-line green-border"></div>';
                var remindercount = 0;
                foreach (var rem in getreminders)
                {
                    if (remindercount == 6)
                        break;
                    //var convertdatetimeformat = rem.ReminderDate.ToString('')//8:00 am, December 31, 2015
                    //<time style='margin-left:15px'>" + rem.ReminderDate.ToString("MMM dd, yyyy") + "</time>
                    if (!rem.IsCompleted)
                    {
                        var colorcode = string.Empty;
                        var colorcode2 = string.Empty;
                        if (rem.ColorCode == (int)Reminders.ColorCodes.Green)
                        {
                            colorcode = "green-border";
                            colorcode2 = "green";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Blue)
                        {
                            colorcode = "blue-border";
                            colorcode2 = "blue";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Yellow)
                        {
                            colorcode = "yellow-border";
                            colorcode2 = "yellow";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Red)
                        {
                            colorcode = "red-border";
                            colorcode2 = "red";
                        }
                        var date = rem.FromDate + "-" + rem.ToDate + " " + rem.ReminderDate.Value.ToString("dddd MMMM dd, yyyy");

                        var jsonstring = "<div onclick='reminderChoice(&apos;" + rem.ID + "&apos;,&apos;" + date + "&apos;,&apos;" + rem.ReminderNote + "&apos;,&apos;" + rem.ReminderTopic + "&apos;,&apos;" + colorcode2 + "&apos;)' href='#' data-target='#" + colorcode2 + "reminderboxs' data-toggle='modal'><time style='margin-left:15px'>" + rem.FromDate + "-" + rem.ToDate + "</time><p style='margin-left:15px'>" + rem.ReminderTopic + "</p><div style='margin-left:-5px' class='vertical-line " + colorcode + "'></div></div>";
                        listy.Add(jsonstring);
                        remindercount++;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getReminders", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }


        public static bool isPrevReminder(string dtto , string dtfrom)
        {
            if (dtto == dtfrom)
                return true;
            else
            {
                //var splitto = dtto.Split(':');
                //var splitfrom = dtfrom.Split(':');
                //var to = Convert.ToInt32(splitto[0]);
                //var from = Convert.ToInt32(splitfrom[0]);
                //if (to > from)
                //    return true;
            }
            return false;
        }

        [WebMethod]
        public static List<string> getReminderTableData(string id, string state,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var dt = Convert.ToDateTime(id);

                if (!string.IsNullOrEmpty(state))
                {
                    if (state == "add")
                        dt = dt.Add(new TimeSpan(1, 0, 0, 0));
                    else
                        dt = dt.Subtract(new TimeSpan(1, 0, 0, 0));
                }

                var getreminders = Reminders.GetAllRemindersByDateAndCreator(dt.Date, userinfo.Username, dbConnection);
                var dtNow = dt.ToString("dddd MMMM dd, yyyy");

                var dt7AM = new List<Reminders>();
                var dt8AM = new List<Reminders>();
                var dt9AM = new List<Reminders>();
                var dt10AM = new List<Reminders>();
                var dt11AM = new List<Reminders>();
                var dt12NN = new List<Reminders>();
                var dt1PM = new List<Reminders>();
                var dt2PM = new List<Reminders>();
                var dt3PM = new List<Reminders>();
                var dt4PM = new List<Reminders>();
                var dt5PM = new List<Reminders>();
                var dt6PM = new List<Reminders>();

                var is7AM = false;
                var is8AM = false;
                var is9AM = false;
                var is10AM = false;
                var is11AM = false;
                var is12NN = false;
                var is1PM = false;
                var is2PM = false;
                var is3PM = false;
                var is4PM = false;
                var is5PM = false;
                var is6PM = false;

                var preRem = new Reminders();
                preRem.ColorCode = 10;
                preRem.isBlank = true;
                var colmdList = new List<string>();
                var colmd = "col-md-4";
                colmdList.Add(colmd);
                colmdList.Add(colmd);
                colmdList.Add(colmd);
                if (getreminders.Count > 3)
                {
                    if (getreminders.Count == 4)
                    {
                        colmdList.Clear();
                        colmd = "col-md-3";
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                    }
                    else if (getreminders.Count == 5)
                    {
                        colmdList.Clear();
                        colmdList.Add("col-md-3");
                        colmdList.Add("col-md-3");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                    }
                    else if (getreminders.Count == 6)
                    {
                        colmdList.Clear();
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                    }
                    else if (getreminders.Count == 7)
                    {
                        colmdList.Clear();
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                    }
                    else if (getreminders.Count == 8)
                    {
                        colmdList.Clear();
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                    }
                    else if (getreminders.Count == 9)
                    {
                        colmdList.Clear();
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                    }
                    else if (getreminders.Count == 10)
                    {
                        colmdList.Clear();
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                    }
                    else if (getreminders.Count == 11)
                    {
                        colmdList.Clear();
                        colmdList.Add("col-md-2");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                        colmdList.Add("col-md-1");
                    }
                    else
                    {
                        colmdList.Clear();
                        colmd = "col-md-1";
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                        colmdList.Add(colmd);
                    }
                }

                getreminders = getreminders.OrderBy(n => n.FromDate).ToList();

                listy.Add(dtNow);
                foreach (var rem in getreminders)
                {
                    is7AM = false;
                    is8AM = false;
                    is9AM = false;
                    is10AM = false;
                    is11AM = false;
                    is12NN = false;
                    is1PM = false;
                    is2PM = false;
                    is3PM = false;
                    is4PM = false;
                    is5PM = false;
                    is6PM = false;

                    //FROM DATE START

                    if (rem.FromDate == "07:00")
                    {
                        dt7AM.Add(rem);
                        is7AM = true;
                    }
                    else if (rem.FromDate == "08:00")
                    {
                        dt8AM.Add(rem);
                        is8AM = true;
                    }
                    else if (rem.FromDate == "09:00")
                    {
                        dt9AM.Add(rem);
                        is9AM = true;
                    }
                    else if (rem.FromDate == "10:00")
                    {
                        dt10AM.Add(rem);
                        is10AM = true;
                    }
                    else if (rem.FromDate == "11:00")
                    {
                        dt11AM.Add(rem);
                        is11AM = true;
                    }
                    else if (rem.FromDate == "12:00")
                    {
                        dt12NN.Add(rem);
                        is12NN = true;
                    }
                    else if (rem.FromDate == "13:00")
                    {
                        dt1PM.Add(rem);
                        is1PM = true;
                    }
                    else if (rem.FromDate == "14:00")
                    {
                        dt2PM.Add(rem);
                        is2PM = true;
                    }
                    else if (rem.FromDate == "15:00")
                    {
                        dt3PM.Add(rem);
                        is3PM = true;
                    }
                    else if (rem.FromDate == "16:00")
                    {
                        dt4PM.Add(rem);
                        is4PM = true;
                    }
                    else if (rem.FromDate == "17:00")
                    {
                        dt5PM.Add(rem);
                        is5PM = true;
                    }
                    else if (rem.FromDate == "18:00")
                    {
                        dt6PM.Add(rem);
                        is6PM = true;
                    }

                    var torem = new Reminders();
                    torem.isBlank = true;
                    torem.FromDate = rem.FromDate;
                    torem.ReminderNote = rem.ReminderNote;
                    torem.ReminderTopic = rem.ReminderTopic;
                    torem.ReminderDate = rem.ReminderDate;
                    torem.IsCompleted = rem.IsCompleted;
                    torem.ToDate = rem.ToDate;
                    torem.ColorCode = rem.ColorCode;

                    //TO DATE START
                    if (rem.ToDate == "08:00")
                    {
                        dt8AM.Add(torem);

                        dt9AM.Add(preRem);
                        dt10AM.Add(preRem);
                        dt11AM.Add(preRem);
                        dt12NN.Add(preRem);
                        dt1PM.Add(preRem);
                        dt2PM.Add(preRem);
                        dt3PM.Add(preRem);
                        dt4PM.Add(preRem);
                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "09:00")
                    {

                        if (is7AM)
                            dt8AM.Add(torem);

                        dt9AM.Add(torem);

                        dt10AM.Add(preRem);
                        dt11AM.Add(preRem);
                        dt12NN.Add(preRem);
                        dt1PM.Add(preRem);
                        dt2PM.Add(preRem);
                        dt3PM.Add(preRem);
                        dt4PM.Add(preRem);
                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "10:00")
                    {

                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                        }

                        dt10AM.Add(torem);

                        dt11AM.Add(preRem);
                        dt12NN.Add(preRem);
                        dt1PM.Add(preRem);
                        dt2PM.Add(preRem);
                        dt3PM.Add(preRem);
                        dt4PM.Add(preRem);
                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "11:00")
                    {
                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                        }
                        else if (is9AM)
                            dt10AM.Add(torem);

                        dt11AM.Add(torem);

                        dt12NN.Add(preRem);
                        dt1PM.Add(preRem);
                        dt2PM.Add(preRem);
                        dt3PM.Add(preRem);
                        dt4PM.Add(preRem);
                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "12:00")
                    {
                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                        }
                        else if (is9AM)
                        {
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                        }
                        else if (is10AM)
                            dt11AM.Add(torem);

                        dt12NN.Add(torem);

                        dt1PM.Add(preRem);
                        dt2PM.Add(preRem);
                        dt3PM.Add(preRem);
                        dt4PM.Add(preRem);
                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "13:00")
                    {
                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                        }
                        else if (is9AM)
                        {
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                        }
                        else if (is10AM)
                        {
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                        }
                        else if (is11AM)
                            dt12NN.Add(torem);

                        dt1PM.Add(torem);

                        dt2PM.Add(preRem);
                        dt3PM.Add(preRem);
                        dt4PM.Add(preRem);
                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "14:00")
                    {
                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                        }
                        else if (is9AM)
                        {
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                        }
                        else if (is10AM)
                        {
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                        }
                        else if (is11AM)
                        {
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                        }
                        else if (is12NN)
                            dt1PM.Add(torem);

                        dt2PM.Add(torem);

                        dt3PM.Add(preRem);
                        dt4PM.Add(preRem);
                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "15:00")
                    {
                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                        }
                        else if (is9AM)
                        {
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                        }
                        else if (is10AM)
                        {
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                        }
                        else if (is11AM)
                        {
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                        }
                        else if (is12NN)
                        {
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                        }
                        else if (is1PM)
                            dt2PM.Add(torem);

                        dt3PM.Add(torem);

                        dt4PM.Add(preRem);
                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "16:00")
                    {
                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                        }
                        else if (is9AM)
                        {
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                        }
                        else if (is10AM)
                        {
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                        }
                        else if (is11AM)
                        {
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                        }
                        else if (is12NN)
                        {
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                        }
                        else if (is1PM)
                        {
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                        }
                        else if (is2PM)
                            dt3PM.Add(torem);

                        dt4PM.Add(torem);

                        dt5PM.Add(preRem);
                        dt6PM.Add(preRem);

                    }
                    else if (rem.ToDate == "17:00")
                    {
                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                        }
                        else if (is9AM)
                        {
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                        }
                        else if (is10AM)
                        {
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                        }
                        else if (is11AM)
                        {
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                        }
                        else if (is12NN)
                        {
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                        }
                        else if (is1PM)
                        {
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                        }
                        else if (is2PM)
                        {
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                        }
                        else if (is3PM)
                            dt4PM.Add(torem);

                        dt5PM.Add(torem);

                        dt6PM.Add(preRem);
                    }
                    else if (rem.ToDate == "18:00")
                    {
                        if (is7AM)
                        {
                            dt8AM.Add(torem);
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is8AM)
                        {
                            dt9AM.Add(torem);
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is9AM)
                        {
                            dt10AM.Add(torem);
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is10AM)
                        {
                            dt11AM.Add(torem);
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is11AM)
                        {
                            dt12NN.Add(torem);
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is12NN)
                        {
                            dt1PM.Add(torem);
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is1PM)
                        {
                            dt2PM.Add(torem);
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is2PM)
                        {
                            dt3PM.Add(torem);
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is3PM)
                        {
                            dt4PM.Add(torem);
                            dt5PM.Add(torem);
                        }
                        else if (is4PM)
                            dt5PM.Add(torem);

                        dt6PM.Add(torem);
                    }
                }
                listy.Add(remindeString(dt7AM, colmdList, uname));
                listy.Add(remindeString(dt8AM, colmdList, uname));
                listy.Add(remindeString(dt9AM, colmdList, uname));
                listy.Add(remindeString(dt10AM, colmdList, uname));
                listy.Add(remindeString(dt11AM, colmdList, uname));
                listy.Add(remindeString(dt12NN, colmdList, uname));
                listy.Add(remindeString(dt1PM, colmdList, uname));
                listy.Add(remindeString(dt2PM, colmdList, uname));
                listy.Add(remindeString(dt3PM, colmdList, uname));
                listy.Add(remindeString(dt4PM, colmdList, uname));
                listy.Add(remindeString(dt5PM, colmdList, uname));
                listy.Add(remindeString(dt6PM, colmdList, uname));
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getReminderTableData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        public static string remindeString(List<Reminders> items,List<string> colmd,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var retVal = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var count = 0;
                foreach (var item in items)
                {
                    if (item != null)
                    {
                        var colorcode = string.Empty;
                        var ischecked = string.Empty;
                        if (item.ColorCode == (int)Reminders.ColorCodes.Green)
                            colorcode = "green";
                        else if (item.ColorCode == (int)Reminders.ColorCodes.Blue)
                            colorcode = "blue";
                        else if (item.ColorCode == (int)Reminders.ColorCodes.Yellow)
                            colorcode = "yellow";
                        else if (item.ColorCode == (int)Reminders.ColorCodes.Red)
                            colorcode = "red";
                        else
                            colorcode = "white";


                        if (item.isBlank)
                        {
                            var lineinfo = "<div style='height:110px;margin-top:-24;' class='vertical-line " + colorcode + "-border'></div>";

                            if (colorcode == "white")
                                lineinfo = "";

                            retVal += "<div class='" + colmd[count] + "'><a><div style='height:110px;margin-top:-25;border-top:0 none;border-top-left-radius:0;border-top-right-radius:0;' class='no-margin " + colorcode + "-reminder reminder-block'><h4></h4><div class='reminder-checkbox'><i></i></div>" + lineinfo + "</div></a></div>";
                        }
                        else
                        {
                            if (item.IsCompleted)
                                ischecked = "checked";
                            var date = item.FromDate + "-" + item.ToDate + " " + item.ReminderDate.Value.ToString("dddd MMMM dd, yyyy");

                            retVal += "<div class='" + colmd[count] + "'><a onclick='reminderChoice(&apos;" + item.ID + "&apos;,&apos;" + date + "&apos;,&apos;" + item.ReminderNote + "&apos;,&apos;" + item.ReminderTopic + "&apos;,&apos;" + colorcode + "&apos;)' href='#' data-target='#" + colorcode + "reminderboxs' data-toggle='modal'><div style='height:80px;' class='no-margin " + colorcode + "-reminder reminder-block'><h4>" + item.ReminderTopic + "</h4><div class='reminder-checkbox'><i class='fa fa-2x fa-check-square-o " + ischecked + "'></i></div><div class='vertical-line " + colorcode + "-border'></div></div></a></div>";
                        }
                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "remindeString", ex, dbConnection, userinfo.SiteId);
            }
            return retVal;
        }
        public static string remindeBlockString(List<Reminders> items,Users userinfo)
        {
            var retVal = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                foreach (var item in items)
                {
                    if (item != null)
                    {
                        var colorcode = string.Empty;
                        if (item.ColorCode == (int)Reminders.ColorCodes.Green)
                            colorcode = "green";
                        else if (item.ColorCode == (int)Reminders.ColorCodes.Blue)
                            colorcode = "blue";
                        else if (item.ColorCode == (int)Reminders.ColorCodes.Yellow)
                            colorcode = "yellow";
                        else if (item.ColorCode == (int)Reminders.ColorCodes.Red)
                            colorcode = "red";
                        else
                            colorcode = "white";

                        var lineinfo = "<div style='height:110px;margin-top:-24;' class='vertical-line " + colorcode + "-border'></div>";

                        if (colorcode == "white")
                            lineinfo = "";

                        retVal += "<div class='col-md-4'><a><div style='height:110px;margin-top:-25;border-top:0 none;border-top-left-radius:0;border-top-right-radius:0;' class='no-margin " + colorcode + "-reminder reminder-block'><h4></h4><div class='reminder-checkbox'><i></i></div>" + lineinfo + "</div></a></div>";
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "remindeBlockString", ex, dbConnection, userinfo.SiteId);
            }
            return retVal;
        }
        [WebMethod]
        public static string insertNewReminder(string name, string date, string reminder, int colorcode, string todate, string fromdate,string uname)
        {
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var newReminder = new Reminders();
                    newReminder.ReminderTopic = name;
                    newReminder.ReminderNote = reminder;
                    newReminder.ToDate = todate;
                    newReminder.FromDate = fromdate;
                    newReminder.ColorCode = colorcode;
                    newReminder.ReminderDate = Convert.ToDateTime(date);
                    newReminder.CreatedBy = userinfo.Username;
                    newReminder.CreatedDate = CommonUtility.getDTNow();
                    newReminder.UpdatedBy = userinfo.Username;
                    newReminder.UpdatedDate = CommonUtility.getDTNow();
                    newReminder.SiteId = userinfo.SiteId;

                    newReminder.CustomerId = userinfo.CustomerInfoId;
                    Reminders.InsertorUpdateReminder(newReminder, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Messages", "insertNewReminder", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static List<string> getMessages(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                List<Arrowlabs.Business.Layer.Notification> sessions = new List<Arrowlabs.Business.Layer.Notification>();

                if (userinfo != null)
                {
                    var subordinates = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection).Select(x => x.Username).ToList();

                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        sessions = Arrowlabs.Business.Layer.Notification.GetAllNotifications(dbConnection);
                    else if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                        sessions = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByCreator(userinfo.Username, dbConnection);
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        sessions = Arrowlabs.Business.Layer.Notification.GetAllNotificationsBySiteId(userinfo.SiteId, dbConnection);
                        sessions = sessions.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        sessions = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByCId(userinfo.CustomerInfoId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                       // {
                        var notis = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByLevel5(userinfo.ID, dbConnection);
                          //  notis = notis.Where(i => i.SiteId == site.SiteId).ToList();
                            sessions.AddRange(notis);
                        //}
                    }

                    var grouped = sessions.GroupBy(item => item.Id);
                    sessions = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                    sessions = sessions.Where(i => i.IncidentId == 0 && !i.Description.Contains('┴') && i.NotificationType != Arrowlabs.Business.Layer.Notification.NotificationTypes.Chat ).ToList();
                    sessions = sessions.OrderByDescending(i => i.Id).ToList();

                    foreach (var item in sessions)
                    {
                        //if (item.NotificationType != Arrowlabs.Business.Layer.Notification.NotificationTypes.Chat)
                        {

                            //if (item.IncidentId == 0)
                            if (!string.IsNullOrEmpty(item.VoiceUrl))
                            {
                                //if (!item.Description.Contains('┴'))
                                listy.Add("<tr role='row'><td style='display:none'>" + item.Id + "</td><td><p style='margin-left:10px;'><b>" + item.ACustomerUName + "</b><span class='date-container pull-right'><i class='fa fa-calendar-o red-color'></i><span class='gray-color pr-2x'>   " + item.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</span></span></p><audio controls><source src='" + item.VoiceUrl + "' type='audio/mpeg'/></audio><span class='operations-container pull-right pr-2x'><a href='#' class='resend-container'><i class='fa fa-eye red-color'></i><span class='gray-color mr-2x' data-target='#viewmsgModal' data-toggle='modal'  onclick='messageviewChoice(&apos;" + item.Id + "&apos;)'>View</span></a><a href='#' class='resend-container'><i class='fa fa-location-arrow red-color'></i><span class='gray-color mr-2x' data-target='#resendModal' data-toggle='modal'  onclick='messageChoice(&apos;" + item.Id + "&apos;)'>Resend</span></a><a href='#' class='delete-container'><i class='fa fa-trash gray-color'></i><span class='gray-color' data-target='#deleteModal' data-toggle='modal' onclick='messageChoice(&apos;" + item.Id + "&apos;)'>Delete</span></a></span></td></tr>");
                                
                            }
                            else
                            {
                                listy.Add("<tr role='row'><td style='display:none'>" + item.Id + "</td><td><p style='margin-left:10px;'><b>" + item.ACustomerUName + "</b><span class='date-container pull-right'><i class='fa fa-calendar-o red-color'></i><span class='gray-color pr-2x'>   " + item.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</span></span></p><p style='margin-left:10px;'>" + item.Description + "</p><span class='operations-container pull-right pr-2x'><a href='#' class='resend-container'><i class='fa fa-eye red-color'></i><span class='gray-color mr-2x' data-target='#viewmsgModal' data-toggle='modal'  onclick='messageviewChoice(&apos;" + item.Id + "&apos;)'>View</span></a><a href='#' class='resend-container'><i class='fa fa-location-arrow red-color'></i><span class='gray-color mr-2x' data-target='#resendModal' data-toggle='modal'  onclick='messageChoice(&apos;" + item.Id + "&apos;)'>Resend</span></a><a href='#' class='delete-container'><i class='fa fa-trash gray-color'></i><span class='gray-color' data-target='#deleteModal' data-toggle='modal' onclick='messageChoice(&apos;" + item.Id + "&apos;)'>Delete</span></a></span></td></tr>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getMessages", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAssigneeType(string uname,string id)
        {
            var userInfo = Users.GetUserByName(uname, dbConnection);
            if (userInfo != null)
            {
                return "User";
            }
            else
            {
                if (CommonUtility.isNumeric(id))
                    return "Group";
                else
                    return "Device";
            }
        }
        [WebMethod]
        public static List<string> getUserTableData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allusers = new List<Users>();
                var allgroups = new List<Group>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        allusers.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            allusers.Add(subsubuser);
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    } 
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {

                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var site in sites)
                   // {
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                    //}
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if(userinfo.RoleId == (int)Role.ChiefOfficer)
                {

                    allusers = Users.GetAllUsers(dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();

                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {

                    allusers = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    allgroups = Group.GetAllGroup(dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }
                allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                var grouped = allusers.GroupBy(item => item.ID);
                allusers = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in allusers)
                {
                    if (item.Username != userinfo.Username)
                    {
                        var returnstring = "<tr role='row' class='odd'><td>" + item.CustomerUName + "</td><td class='sorting_1'>" + item.Status + "</td><td><a href='#' class='red-color' id=" + item.ID + item.Username + " onclick='userchoiceTable(&apos;" + item.ID + "&apos;,&apos;" + item.Username + "&apos;,&apos;" + item.CustomerUName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                    }
                    //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                }
                foreach (var dev in devices)
                {
                    if (dev.Status == 1)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Online</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='userchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                    else
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Offline</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='userchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                        listy.Add(returnstring);
                    }
                }
                foreach(var grp in allgroups)
                {
                    var returnstring = "<tr role='row' class='odd'><td>" + grp.Name + "</td><td class='sorting_1'>Group</td><td><a href='#' class='red-color' id=" + grp.Id + grp.Name.Replace(" ", "") + " onclick='userchoiceTable(&apos;" + grp.Id + "&apos;,&apos;" + grp.Name.Replace(" ", "") + "&apos;,&apos;" + grp.Name.Replace(" ", "") + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getUserTableData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getUserMsgView(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var gHistory = MessageHistory.GetMessageHistoryByNotificationId(id, dbConnection);
                if (gHistory.Count > 0)
                {
                    listy.Add("SUCCESS");
                    foreach (var history in gHistory)
                    {
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + history.CreatedDate.Value.AddHours(userinfo.TimeZone) + "</p><p><span class='red-color'>" + history.CustomerUName + "</span> has viewed</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);//history.CustomerUName + "-" + history.CreatedDate.Value.AddHours(userinfo.TimeZone));
                    }
                }
                else
                {
                    listy.Add("No activity found");
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getUserMsgView", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string markReminderAsRead(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var reminderInfo = Reminders.GetReminderById(id, dbConnection);
                reminderInfo.IsCompleted = true;
                Reminders.InsertorUpdateReminder(reminderInfo, dbConnection,dbConnectionAudit,true);
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "markReminderAsRead", ex, dbConnection, userinfo.SiteId);
                return "Failed to mark reminder as read";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string deleteReminder(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                Reminders.DeleteReminderById(id, dbConnection);
                SystemLogger.SaveSystemLog(dbConnectionAudit, "Messages", id.ToString(), id.ToString(), userinfo, "Delete Reminder" + id);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "deleteReminder", ex, dbConnection, userinfo.SiteId);
                return "Failed to delete reminder";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string deleteMessage(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }
                var noti = Arrowlabs.Business.Layer.Notification.GetNotificationsById(id, dbConnection);
                noti.IsDeleted = true;
                noti.UpdatedBy = userinfo.Username;
                noti.UpdatedDate = CommonUtility.getDTNow();
                Arrowlabs.Business.Layer.Notification.InsertorUpdateNotification(noti, dbConnection,dbConnectionAudit,true);
                SystemLogger.SaveSystemLog(dbConnectionAudit, "Messages", id.ToString(), id.ToString(), userinfo, "Delete Notification" + id);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "deleteMessage", ex, dbConnection, userinfo.SiteId);
                return "Failed to delete message";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static List<string> resendMessage(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                var noti = Arrowlabs.Business.Layer.Notification.GetNotificationsById(id, dbConnection);
                //noti.Id = 0;
                //noti.CreatedBy = userinfo.Username;
                //noti.CreatedDate = CommonUtility.getDTNow();
                //noti.UpdatedBy = userinfo.Username;
                //noti.UpdatedDate = CommonUtility.getDTNow();
                //Arrowlabs.Business.Layer.Notification.InsertorUpdateNotification(noti, dbConnection);
                listy.Add(noti.Description);
                listy.Add(noti.AssigneeName);
                listy.Add(noti.AssigneeId.ToString());
                listy.Add(noti.AssigneeType.ToString());
                if (!string.IsNullOrEmpty(noti.VoiceUrl))
                {
                    listy.Add("True");
                    listy.Add(noti.VoiceUrl);
                }
                else
                {
                    listy.Add("False");
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "resendMessage", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        //User Profile
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static string changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                if (userinfo.ID > 0)
                {
                    var getuser = userinfo;
                    getuser.FirstName = firstname;
                    getuser.LastName = lastname;
                    getuser.Email = emailaddress;

                    if (getuser.RoleId != role)
                    {
                        getuser.RoleId = role;
                        if (role == (int)Role.Manager)
                        {
                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                        else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                        }
                        else
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                    }
                    if (getuser.RoleId == (int)Role.Manager)
                    {

                        var dirUser = Users.GetUserById(supervisor, dbConnection);
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        if (dirUser != null)
                        {
                            if (!string.IsNullOrEmpty(dirUser.Username))
                            {
                                List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                            
                                            }};
                                DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                            }
                        }
                    }
                    else if (getuser.RoleId == (int)Role.Operator)
                    {
                        if (supervisor > 0)
                        {
                            var manUser = Users.GetUserById(supervisor, dbConnection);
                            List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                            UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                    else if (getuser.RoleId == (int)Role.UnassignedOperator)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                    {
                        return userinfo.ID;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "addUserProfile", ex, dbConnection, userinfo.SiteId);
            }
            return userinfo.ID;
        }

        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);

                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                   // var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    if (customData.RoleId != (int)Role.SuperAdmin)
                    {
                        //if (pushDev != null)
                       // {
                       //     if (!string.IsNullOrEmpty(pushDev.Username))
                        //    {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                        //    }
                        //    else
                        //    {
                        //        monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        ///        fontstyle = "";
                         //   }
                        //}
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }
    }
}