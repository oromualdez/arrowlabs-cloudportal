﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class DriverOffence
    {
        [DataMember]
        public Guid Identifier { get; set; }
        [DataMember]
        public string OffenceDate { get; set; }
        [DataMember]
        public string OffenceTime { get; set; }
        [DataMember]
        public string PlateNumber { get; set; }
        [DataMember]
        public string PlateSource { get; set; }
        [DataMember]
        public string PlateCode { get; set; }
        [DataMember]
        public string VehicleMake { get; set; }
        [DataMember]
        public string VehicleColor { get; set; }
        [DataMember]
        public string VehicleType { get; set; }
        [DataMember]
        public string LocationType { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public string OffenceType { get; set; }
        public string OffenceCategory { get; set; }
        [DataMember]
        public string Offence1 { get; set; }
        [DataMember]
        public string Offence2 { get; set; }
        [DataMember]
        public string Offence3 { get; set; }
        [DataMember]
        public string Offence4 { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public byte[] VideoPath { get; set; }
        [DataMember]
        public byte[] ImagePath1 { get; set; }
        [DataMember]
        public byte[] ImagePath2 { get; set; }
        [DataMember]
        public byte[] ImagePath3 { get; set; }
        [DataMember]
        public byte[] ImagePath4 { get; set; }
        [DataMember]
        public byte[] ImagePath5 { get; set; }
        [DataMember]
        public byte[] ImagePath6 { get; set; }
        [DataMember]
        public byte[] OffenceImagePath { get; set; }
        [DataMember]
        public string VideoPath1 { get; set; }
        [DataMember]
        public string Image1Path { get; set; }
        [DataMember]
        public string Image2Path { get; set; }
        [DataMember]
        public string Image3Path { get; set; }
        [DataMember]
        public string Image4Path { get; set; }
        [DataMember]
        public string Image5Path { get; set; }
        [DataMember]
        public string Image6Path { get; set; }
        [DataMember]
        public string OffenceImagePath1 { get; set; }
        [DataMember]
        public double Longitude { get; set; }
        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public string StartBy { get; set; }
        public string StartByName { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public string EndBy { get; set; }
        public string EndByName { get; set; }
        public string CustomerUName { get; set; }

        public string CustomerFullName { get; set; }

        [DataMember]
        public int Status { get; set; }
        public int TicketStatus { get; set; }
        [DataMember]
        public string Comments { get; set; }

        public string UpdatedBy { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public string SessionId { get; set; }
        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public int SiteId { get; set; }

        [DataMember]
        public int ContractId { get; set; }
        public string ContractName { get; set; }
        [DataMember]
        public int CusId { get; set; }
        public string CusName { get; set; }


        [DataMember]
        public int CustomerId { get; set; }
        public enum StatusType : int
        {
            Unknown = -1,
            NotProcessed = 0,
            Processed = 1,
            Deleted = 2,
            Sent = 3
        }

        /// <summary>
        /// It will return all driver offences with provided date
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="fromDate"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<DriverOffence> GetAllDriverOffenceByDate(DateTime startDate, DateTime fromDate, string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllDriverOffenceByDate";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetUserNamesDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetUserNamesDriverOffence", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetUserNamesDriverOffence";

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);

                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetUserNamesDriverOffenceByDate(DateTime startDate, DateTime fromDate, string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetUserNamesDriverOffenceByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = startDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllFullDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFullDriverOffence", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllFullDriverOffence";

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    driverOffence.Comments = reader["Comments"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllReportFullDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllReportFullDriverOffence", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllDriverOffenceClientSide(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.NotProcessed;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.NotProcessed;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllDriverOffence1(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.NotProcessed;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllReportDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllReportDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.NotProcessed;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllReportProcessedDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllReportDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.Processed;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllReportDeletedDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllReportDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.Deleted;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    driverOffence.Comments = reader["Comments"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllToDeleteDriverOffenceClientSide(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.Deleted;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAllToDeleteDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.Deleted;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    driverOffence.Comments = reader["Comments"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAlltoSendDriverOffenceClientSide(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.Processed;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    if (!string.IsNullOrEmpty(reader["Longitude"].ToString()))
                    {
                        driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["Latitude"].ToString()))
                    {
                        driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    }
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.Offence1 = reader["Offence1"].ToString();
                    driverOffence.Offence2 = reader["Offence2"].ToString();
                    driverOffence.Offence3 = reader["Offence3"].ToString();
                    driverOffence.Offence4 = reader["Offence4"].ToString();

                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static List<DriverOffence> GetAlltoSendDriverOffence(string dbConnection)
        {
            var driverOffences = new List<DriverOffence>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDriverOffenceByStatus‏", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                sqlCommand.Parameters["@Status"].Value = (int)StatusType.Processed;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new DriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.Offence1 = reader["Offence1"].ToString();
                    driverOffence.Offence2 = reader["Offence2"].ToString();
                    driverOffence.Offence3 = reader["Offence3"].ToString();

                    driverOffence.Offence4 = reader["Offence4"].ToString();

                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    if (reader["Comments"] != null)
                        driverOffence.Comments = reader["Comments"].ToString();
                    else
                        driverOffence.Comments = "";

                    driverOffences.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return driverOffences;
        }

        public static DriverOffence GetSmallDriverOffenceById(Guid identifier, string dbConnection)
        {
            var driverOffence = new DriverOffence();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetSmallDriverOffenceById";
                sqlCommand.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@Identifier"].Value = identifier;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                // connection.Open();
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {


                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.UserName = reader["UserName"].ToString();
                }
                connection.Close();
                reader.Close();
            }
            return driverOffence;
        }

        public static bool InsertOrUpdateDriverOffenceClientSide(DriverOffence driverOffence, string dbConnection)
        {
            if (driverOffence != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateDriverOffence", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = driverOffence.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@OffenceDate", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceDate"].Value = driverOffence.OffenceDate;

                    cmd.Parameters.Add(new SqlParameter("@OffenceTime", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceTime"].Value = driverOffence.OffenceTime;

                    cmd.Parameters.Add(new SqlParameter("@PlateNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateNumber"].Value = driverOffence.PlateNumber;

                    cmd.Parameters.Add(new SqlParameter("@PlateSource", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateSource"].Value = driverOffence.PlateSource;

                    cmd.Parameters.Add(new SqlParameter("@PlateCode", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateCode"].Value = driverOffence.PlateCode;

                    cmd.Parameters.Add(new SqlParameter("@VehicleMake", SqlDbType.NVarChar));
                    cmd.Parameters["@VehicleMake"].Value = driverOffence.VehicleMake;

                    cmd.Parameters.Add(new SqlParameter("@VehicleColor", SqlDbType.NVarChar));
                    cmd.Parameters["@VehicleColor"].Value = driverOffence.VehicleColor;

                    cmd.Parameters.Add(new SqlParameter("@VehicleType", SqlDbType.VarChar));
                    cmd.Parameters["@VehicleType"].Value = driverOffence.VehicleType;

                    cmd.Parameters.Add(new SqlParameter("@LocationType", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationType"].Value = driverOffence.LocationType;

                    cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.NVarChar));
                    cmd.Parameters["@Location"].Value = driverOffence.Location;

                    cmd.Parameters.Add(new SqlParameter("@OffenceType", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceType"].Value = driverOffence.OffenceType;

                    cmd.Parameters.Add(new SqlParameter("@Offence1", SqlDbType.NVarChar));
                    cmd.Parameters["@Offence1"].Value = driverOffence.Offence1;

                    cmd.Parameters.Add(new SqlParameter("@Offence2", SqlDbType.NVarChar));
                    cmd.Parameters["@Offence2"].Value = driverOffence.Offence2;

                    cmd.Parameters.Add(new SqlParameter("@Offence3", SqlDbType.NVarChar));
                    cmd.Parameters["@Offence3"].Value = driverOffence.Offence3;

                    cmd.Parameters.Add(new SqlParameter("@Offence4", SqlDbType.NVarChar));
                    cmd.Parameters["@Offence4"].Value = driverOffence.Offence4;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = driverOffence.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = driverOffence.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = driverOffence.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Image1Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image1Path"].Value = driverOffence.Image1Path;

                    cmd.Parameters.Add(new SqlParameter("@Image2Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image2Path"].Value = driverOffence.Image2Path;

                    cmd.Parameters.Add(new SqlParameter("@Image3Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image3Path"].Value = driverOffence.Image3Path;

                    cmd.Parameters.Add(new SqlParameter("@Image4Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image4Path"].Value = driverOffence.Image4Path;

                    cmd.Parameters.Add(new SqlParameter("@Image5Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image5Path"].Value = driverOffence.Image5Path;

                    cmd.Parameters.Add(new SqlParameter("@Image6Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image6Path"].Value = driverOffence.Image6Path;

                    cmd.Parameters.Add(new SqlParameter("@OffenceImagePath1", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceImagePath1"].Value = driverOffence.OffenceImagePath1;

                    cmd.Parameters.Add(new SqlParameter("@VideoPath1", SqlDbType.NVarChar));
                    cmd.Parameters["@VideoPath1"].Value = driverOffence.VideoPath1;


                    if (driverOffence.ImagePath1 != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath1", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath1"].Value = driverOffence.ImagePath1;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath1", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath1"].Value = ImageBytes(driverOffence.Image1Path);
                    }
                    if (driverOffence.ImagePath2 != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath2", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath2"].Value = driverOffence.ImagePath2;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath2", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath2"].Value = driverOffence.ImagePath2;
                    }
                    if (driverOffence.ImagePath3 != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath3", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath3"].Value = driverOffence.ImagePath3;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath3", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath3"].Value = ImageBytes(driverOffence.Image3Path);
                    }
                    if (driverOffence.ImagePath4 != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath4", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath4"].Value = driverOffence.ImagePath4;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath4", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath4"].Value = ImageBytes(driverOffence.Image4Path);
                    }
                    if (driverOffence.ImagePath5 != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath5", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath5"].Value = driverOffence.ImagePath5;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath5", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath5"].Value = ImageBytes(driverOffence.Image5Path);
                    }
                    if (driverOffence.ImagePath6 != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath6", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath6"].Value = driverOffence.ImagePath6;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImagePath6", SqlDbType.VarBinary));
                        cmd.Parameters["@ImagePath6"].Value = ImageBytes(driverOffence.Image6Path);
                    }
                    if (driverOffence.OffenceImagePath != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@OffenceImagePath", SqlDbType.VarBinary));
                        cmd.Parameters["@OffenceImagePath"].Value = driverOffence.OffenceImagePath;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add(new SqlParameter("@OffenceImagePath", SqlDbType.VarBinary));
                        cmd.Parameters["@OffenceImagePath"].Value = ImageBytes(driverOffence.OffenceImagePath1);
                    }
                    if (driverOffence.VideoPath != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@VideoPath", SqlDbType.VarBinary));
                        cmd.Parameters["@VideoPath"].Value = driverOffence.VideoPath;
                    }
                    else if (driverOffence.Status == (int)StatusType.Processed)
                    {
                        cmd.Parameters.Add(new SqlParameter("@VideoPath", SqlDbType.VarBinary));
                        cmd.Parameters["@VideoPath"].Value = ImageBytes(driverOffence.VideoPath1);
                    }

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = driverOffence.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = driverOffence.Status;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateDriverOffence";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        /// <summary>
        /// It will insert or update driver offence
        /// </summary>
        /// <param name="driverOffence"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static bool InsertOrUpdateDriverOffence(DriverOffence driverOffence, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {

            if (driverOffence != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateDriverOffence", connection);

                    cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Identifier"].Value = driverOffence.Identifier;

                    cmd.Parameters.Add(new SqlParameter("@OffenceDate", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceDate"].Value = driverOffence.OffenceDate;

                    cmd.Parameters.Add(new SqlParameter("@OffenceTime", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceTime"].Value = driverOffence.OffenceTime;

                    cmd.Parameters.Add(new SqlParameter("@PlateNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateNumber"].Value = driverOffence.PlateNumber;

                    cmd.Parameters.Add(new SqlParameter("@PlateSource", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateSource"].Value = driverOffence.PlateSource;

                    cmd.Parameters.Add(new SqlParameter("@PlateCode", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateCode"].Value = driverOffence.PlateCode;

                    cmd.Parameters.Add(new SqlParameter("@VehicleMake", SqlDbType.NVarChar));
                    cmd.Parameters["@VehicleMake"].Value = driverOffence.VehicleMake;

                    cmd.Parameters.Add(new SqlParameter("@VehicleColor", SqlDbType.NVarChar));
                    cmd.Parameters["@VehicleColor"].Value = driverOffence.VehicleColor;

                    cmd.Parameters.Add(new SqlParameter("@VehicleType", SqlDbType.VarChar));
                    cmd.Parameters["@VehicleType"].Value = driverOffence.VehicleType;

                    cmd.Parameters.Add(new SqlParameter("@LocationType", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationType"].Value = driverOffence.LocationType;

                    cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.NVarChar));
                    cmd.Parameters["@Location"].Value = driverOffence.Location;

                    cmd.Parameters.Add(new SqlParameter("@OffenceType", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceType"].Value = driverOffence.OffenceType;

                    cmd.Parameters.Add(new SqlParameter("@Offence1", SqlDbType.NVarChar));
                    cmd.Parameters["@Offence1"].Value = driverOffence.Offence1;

                    cmd.Parameters.Add(new SqlParameter("@Offence2", SqlDbType.NVarChar));
                    cmd.Parameters["@Offence2"].Value = driverOffence.Offence2;

                    cmd.Parameters.Add(new SqlParameter("@Offence3", SqlDbType.NVarChar));
                    cmd.Parameters["@Offence3"].Value = driverOffence.Offence3;

                    cmd.Parameters.Add(new SqlParameter("@Offence4", SqlDbType.NVarChar));
                    cmd.Parameters["@Offence4"].Value = driverOffence.Offence4;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = driverOffence.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = driverOffence.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = driverOffence.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Image1Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image1Path"].Value = driverOffence.Image1Path;

                    cmd.Parameters.Add(new SqlParameter("@Image2Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image2Path"].Value = driverOffence.Image2Path;

                    cmd.Parameters.Add(new SqlParameter("@Image3Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image3Path"].Value = driverOffence.Image3Path;

                    cmd.Parameters.Add(new SqlParameter("@Image4Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image4Path"].Value = driverOffence.Image4Path;

                    cmd.Parameters.Add(new SqlParameter("@Image5Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image5Path"].Value = driverOffence.Image5Path;

                    cmd.Parameters.Add(new SqlParameter("@Image6Path", SqlDbType.NVarChar));
                    cmd.Parameters["@Image6Path"].Value = driverOffence.Image6Path;

                    cmd.Parameters.Add(new SqlParameter("@OffenceImagePath1", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceImagePath1"].Value = driverOffence.OffenceImagePath1;

                    cmd.Parameters.Add(new SqlParameter("@VideoPath1", SqlDbType.NVarChar));
                    cmd.Parameters["@VideoPath1"].Value = driverOffence.VideoPath1;

                    cmd.Parameters.Add(new SqlParameter("@OffenceCategory", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceCategory"].Value = driverOffence.OffenceCategory;


                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = driverOffence.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@ContractId", SqlDbType.Int));
                    cmd.Parameters["@ContractId"].Value = driverOffence.ContractId;

                    cmd.Parameters.Add(new SqlParameter("@CusId", SqlDbType.Int));
                    cmd.Parameters["@CusId"].Value = driverOffence.CusId;


                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = driverOffence.CustomerId;


                    if (!string.IsNullOrEmpty(driverOffence.Comments))
                    {
                        cmd.Parameters.Add(new SqlParameter("@Comments", SqlDbType.NVarChar));
                        cmd.Parameters["@Comments"].Value = driverOffence.Comments;
                    }
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = driverOffence.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = driverOffence.Status;


                    cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                    cmd.Parameters["@StartDate"].Value = driverOffence.StartDate;

                    cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                    cmd.Parameters["@EndDate"].Value = driverOffence.EndDate;

                    cmd.Parameters.Add(new SqlParameter("@StartBy", SqlDbType.NVarChar));
                    cmd.Parameters["@StartBy"].Value = driverOffence.StartBy;

                    cmd.Parameters.Add(new SqlParameter("@EndBy", SqlDbType.NVarChar));
                    cmd.Parameters["@EndBy"].Value = driverOffence.EndBy;

                    cmd.Parameters.Add(new SqlParameter("@TicketStatus", SqlDbType.Int));
                    cmd.Parameters["@TicketStatus"].Value = driverOffence.TicketStatus;

                    cmd.CommandText = "InsertOrUpdateDriverOffence";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new DriverOffenceAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.Create, driverOffence.UpdatedBy);
                            Reflection.CopyProperties(driverOffence, audit);
                            DriverOffenceAudit.InsertDriverOffenceAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateDriverOffence", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static byte[] ImageBytes(string imagePath)
        {
            if (string.IsNullOrEmpty(imagePath))
                return null;
            using (FileStream FS = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
            {
                byte[] img = new byte[FS.Length];
                FS.Read(img, 0, Convert.ToInt32(FS.Length));
                return img;
            }
        }
        public static DriverOffence GetDriverOffenceByIdClientSide(Guid identifier, string dbConnection)
        {
            DriverOffence driverOffence = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetDriverOffenceById";
                sqlCommand.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@Identifier"].Value = identifier;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    driverOffence = new DriverOffence();
                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.Offence1 = reader["Offence1"].ToString();
                    driverOffence.Offence2 = reader["Offence2"].ToString();
                    driverOffence.Offence3 = reader["Offence3"].ToString();

                    driverOffence.Offence4 = reader["Offence4"].ToString();

                    if (reader["ImagePath1"] != DBNull.Value)
                        driverOffence.ImagePath1 = (byte[])reader["ImagePath1"];

                    if (reader["ImagePath2"] != DBNull.Value)
                        driverOffence.ImagePath2 = (byte[])reader["ImagePath2"];

                    if (reader["ImagePath3"] != DBNull.Value)
                        driverOffence.ImagePath3 = (byte[])reader["ImagePath3"];

                    if (reader["ImagePath4"] != DBNull.Value)
                        driverOffence.ImagePath4 = (byte[])reader["ImagePath4"];

                    if (reader["ImagePath5"] != DBNull.Value)
                        driverOffence.ImagePath5 = (byte[])reader["ImagePath5"];

                    if (reader["ImagePath6"] != DBNull.Value)
                        driverOffence.ImagePath6 = (byte[])reader["ImagePath6"];

                    if (reader["OffenceImagePath"] != DBNull.Value)
                        driverOffence.OffenceImagePath = (byte[])reader["OffenceImagePath"];

                    if (reader["VideoPath"] != DBNull.Value)
                        driverOffence.VideoPath = (byte[])reader["VideoPath"];
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                }
                connection.Close();
                reader.Close();
            }

            return driverOffence;
        }

        public static DriverOffence GetDriverOffenceById(Guid identifier, string dbConnection)
        {
            DriverOffence driverOffence = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetDriverOffenceById";
                sqlCommand.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@Identifier"].Value = identifier;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandTimeout = 600;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    driverOffence = new DriverOffence();
                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.VehicleType = reader["VehicleType"].ToString();
                    driverOffence.LocationType = reader["LocationType"].ToString();
                    driverOffence.Location = reader["Location"].ToString();
                    driverOffence.OffenceType = reader["OffenceType"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    driverOffence.Image1Path = reader["Image1Path"].ToString();
                    driverOffence.Image2Path = reader["Image2Path"].ToString();
                    driverOffence.Image3Path = reader["Image3Path"].ToString();
                    driverOffence.Image4Path = reader["Image4Path"].ToString();
                    driverOffence.Image5Path = reader["Image5Path"].ToString();
                    driverOffence.Image6Path = reader["Image6Path"].ToString();
                    driverOffence.OffenceImagePath1 = reader["OffenceImagePath1"].ToString();
                    driverOffence.VideoPath1 = reader["VideoPath1"].ToString();
                    driverOffence.Offence1 = reader["Offence1"].ToString();
                    driverOffence.Offence2 = reader["Offence2"].ToString();
                    driverOffence.Offence3 = reader["Offence3"].ToString();

                    driverOffence.OffenceCategory = reader["OffenceCategory"].ToString();
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

                    if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                        driverOffence.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

                    if (columns.Contains("CusId") && reader["CusId"] != DBNull.Value)
                        driverOffence.CusId = Convert.ToInt32(reader["CusId"].ToString());

                    if (columns.Contains("ContractId") && reader["ContractId"] != DBNull.Value)
                        driverOffence.ContractId = Convert.ToInt32(reader["ContractId"].ToString());

                    driverOffence.Offence4 = reader["Offence4"].ToString();

                    if (reader["ImagePath1"] != DBNull.Value)
                        driverOffence.ImagePath1 = (byte[])reader["ImagePath1"];

                    if (reader["ImagePath2"] != DBNull.Value)
                        driverOffence.ImagePath2 = (byte[])reader["ImagePath2"];

                    if (reader["ImagePath3"] != DBNull.Value)
                        driverOffence.ImagePath3 = (byte[])reader["ImagePath3"];

                    if (reader["ImagePath4"] != DBNull.Value)
                        driverOffence.ImagePath4 = (byte[])reader["ImagePath4"];

                    if (reader["ImagePath5"] != DBNull.Value)
                        driverOffence.ImagePath5 = (byte[])reader["ImagePath5"];

                    if (reader["ImagePath6"] != DBNull.Value)
                        driverOffence.ImagePath6 = (byte[])reader["ImagePath6"];

                    if (reader["OffenceImagePath"] != DBNull.Value)
                        driverOffence.OffenceImagePath = (byte[])reader["OffenceImagePath"];

                    if (reader["VideoPath"] != DBNull.Value)
                        driverOffence.VideoPath = (byte[])reader["VideoPath"];

                    driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    driverOffence.UserName = reader["UserName"].ToString();
                    driverOffence.Comments = reader["Comments"].ToString();

                    if (columns.Contains("StartDate") && reader["StartDate"] != DBNull.Value)
                        driverOffence.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());

                    if (columns.Contains("EndDate") && reader["EndDate"] != DBNull.Value)
                        driverOffence.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());

                    if (columns.Contains("StartBy") && reader["StartBy"] != DBNull.Value)
                        driverOffence.StartBy = reader["StartBy"].ToString();

                    if (columns.Contains("ContractName") && reader["ContractName"] != DBNull.Value)
                        driverOffence.ContractName = reader["ContractName"].ToString();

                    if (columns.Contains("CusName") && reader["CusName"] != DBNull.Value)
                        driverOffence.CusName = reader["CusName"].ToString();

                    if (columns.Contains("EndBy") && reader["EndBy"] != DBNull.Value)
                        driverOffence.EndBy = reader["EndBy"].ToString();

                    if (columns.Contains("StartByName") && reader["StartByName"] != DBNull.Value)
                        driverOffence.StartByName = reader["StartByName"].ToString();

                    if (columns.Contains("EndByName") && reader["EndByName"] != DBNull.Value)
                        driverOffence.EndByName = reader["EndByName"].ToString();

                    if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        driverOffence.CustomerUName = reader["CustomerUName"].ToString();

                    if (columns.Contains("CustomerFullName") && reader["CustomerFullName"] != DBNull.Value)
                        driverOffence.CustomerFullName = reader["CustomerFullName"].ToString();


                    if (columns.Contains("TicketStatus") && reader["TicketStatus"] != DBNull.Value)
                        driverOffence.TicketStatus = Convert.ToInt32(reader["TicketStatus"].ToString());

                }
                connection.Close();
                reader.Close();
            }

            return driverOffence;
        }

        public static string GetUserName(Guid identifier, string dbConnection)
        {
            string user = string.Empty;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetDriverOffenceById";
                sqlCommand.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@Identifier"].Value = identifier;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    //driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    //driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    user = reader["UserName"].ToString();

                }
                connection.Close();
                reader.Close();
            }

            return user;
        }

        public static bool UpdateCustomerInfoForDriverOffence(Guid identifier, int cid, int conid, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateCustomerInfoForDriverOffence", connection);

                cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@Identifier"].Value = identifier;

                cmd.Parameters.Add(new SqlParameter("@CusId", SqlDbType.Int));
                cmd.Parameters["@CusId"].Value = cid;

                cmd.Parameters.Add(new SqlParameter("@ContractId", SqlDbType.Int));
                cmd.Parameters["@ContractId"].Value = conid;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "UpdateCustomerInfoForDriverOffence";

                cmd.ExecuteNonQuery();
            }

            return true;
        }

        public static bool UpdateDriverOffenceStatus(Guid identifier, int status, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateDriverOffenceStatus", connection);

                cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@Identifier"].Value = identifier;

                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@Status"].Value = status;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "UpdateDriverOffenceStatus";

                cmd.ExecuteNonQuery();
            }

            return true;
        }

        public static bool UpdateStartDriverOffence(Guid identifier, DateTime date, string user, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateStartDriverOffence", connection);

                cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@Identifier"].Value = identifier;

                cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                cmd.Parameters["@StartDate"].Value = date;

                cmd.Parameters.Add(new SqlParameter("@StartBy", SqlDbType.NVarChar));
                cmd.Parameters["@StartBy"].Value = user;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "UpdateStartDriverOffence";

                cmd.ExecuteNonQuery();
            }

            return true;
        }

        public static bool UpdateEndDriverOffence(Guid identifier, DateTime date, string user, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateEndDriverOffence", connection);

                cmd.Parameters.Add(new SqlParameter("@Identifier", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@Identifier"].Value = identifier;

                cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                cmd.Parameters["@EndDate"].Value = date;

                cmd.Parameters.Add(new SqlParameter("@EndBy", SqlDbType.NVarChar));
                cmd.Parameters["@EndBy"].Value = user;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "UpdateEndDriverOffence";

                cmd.ExecuteNonQuery();
            }

            return true;
        }


        public static IList<TamplateDriverOffence> GetAllTamplateDriverOffences(DateTime frmDateTime, DateTime toDateTime, string dbConnection)
        {
            var offenceList = new List<TamplateDriverOffence>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTamplateDriverOffences", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = frmDateTime;
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = toDateTime;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllTamplateDriverOffences";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var driverOffence = new TamplateDriverOffence();

                    driverOffence.Identifier = Guid.Parse((reader["Identifier"].ToString()));
                    driverOffence.OffenceDate = reader["OffenceDate"].ToString();
                    driverOffence.OffenceTime = reader["OffenceTime"].ToString();
                    driverOffence.PlateNumber = reader["PlateNumber"].ToString();
                    driverOffence.PlateSource = reader["PlateSource"].ToString();
                    driverOffence.PlateCode = reader["PlateCode"].ToString();
                    driverOffence.VehicleMake = reader["VehicleMake"].ToString();
                    driverOffence.VehicleColor = reader["VehicleColor"].ToString();
                    driverOffence.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
                    driverOffence.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                        driverOffence.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

                    if (reader["ImagePath1"] != DBNull.Value)
                        driverOffence.ImagePath1 = (reader["ImagePath1"] == null) ? null : (byte[])reader["ImagePath1"];

                    if (reader["ImagePath2"] != DBNull.Value)
                        driverOffence.ImagePath2 = (reader["ImagePath2"] == null) ? null : (byte[])reader["ImagePath2"];

                    if (reader["ImagePath3"] != DBNull.Value)
                        driverOffence.ImagePath3 = (reader["ImagePath3"] == null) ? null : (byte[])reader["ImagePath3"];

                    if (reader["ImagePath4"] != DBNull.Value)
                        driverOffence.ImagePath4 = (reader["ImagePath4"] == null) ? null : (byte[])reader["ImagePath4"];

                    if (reader["ImagePath5"] != DBNull.Value)
                        driverOffence.ImagePath5 = (reader["ImagePath5"] == null) ? null : (byte[])reader["ImagePath5"];

                    if (reader["ImagePath6"] != DBNull.Value)
                        driverOffence.ImagePath6 = (reader["ImagePath6"] == null) ? null : (byte[])reader["ImagePath6"];

                    if (reader["OffenceImagePath"] != DBNull.Value)
                        driverOffence.OffenceImagePath = (reader["OffenceImagePath"] == null) ? null : (byte[])reader["OffenceImagePath"];

                    if (reader["VideoPath"] != DBNull.Value)
                        driverOffence.VideoPath = (reader["VideoPath"] == null) ? null : (byte[])reader["VideoPath"];

                    driverOffence.Offence1 = reader["Offence1"].ToString();
                    driverOffence.Offence2 = reader["Offence2"].ToString();
                    driverOffence.Offence3 = reader["Offence3"].ToString();
                    driverOffence.Offence4 = reader["Offence4"].ToString();
                    try
                    {
                        driverOffence.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                        driverOffence.ReceiveDate = Convert.ToDateTime(reader["ReceiveDate"].ToString());
                    }
                    catch (Exception ex)
                    {

                    }
                    driverOffence.UserName = reader["UserName"].ToString();
                    offenceList.Add(driverOffence);
                }
                connection.Close();
                reader.Close();
            }
            return offenceList;
        }

        public static bool DeleteDriverOffenceByDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteDriverOffenceByDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDriverOffenceByDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

    }
}
