﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
 public class HouseKeeping
    { 
     public int Id {get;set;}
      public int Asset{get;set;}
        public int AssetCategory{get;set;}
        public int ChatUser{get;set;}
        public int ClientDevice{get;set;}
        public int ConfigSettings{get;set;}
        public int CustomEvent{get;set;}
        public int DeviceGroup{get;set;}
        public int DeviceWhiteList{get;set;}
        public int DriverOffence{get;set;}
        public int DutyRoaster{get;set;}
        public int Enrollment{get;set;}
        public int Group{get;set;}
        public int GroupDeviceTask{get;set;}
        public int GroupRole{get;set;}
        public int GroupUser{get;set;}
        public int HealthCheck{get;set;}
        public int HotEvent{get;set;}
        public int Language{get;set;}
        public int LicenceClientDevice{get;set;}
        public int Location{get;set;}
        public int LocationType{get;set;}
        public int Login {get;set;}
        public int MailSetting{get;set;}
        public int MainSession{get;set;}
        public int Manager{get;set;}
        public int MIMSConfigSetting{get;set;}
        public int MIMSLog{get;set;}
        public int MobileDevice{get;set;}
        public int MobileHotEvent{get;set;}
        public int MobileHotEventAttachment{get;set;}
        public int MobileHotEventCategory{get;set;}
        public int Notification{get;set;}
        public int NotificationAttachment{get;set;}
        public int NotificationStatus{get;set;}
        public int Offence{get;set;}
        public int Offence_Audit{get;set;}
        public int OffenceType{get;set;}
        public int OffenceType_Audit{get;set;}
        public int PlateCode{get;set;}
        public int PlateSource{get;set;}
        public int PostOrder{get;set;}
        public int Reminder{get;set;}
        public int ServerDevice{get;set;}
        public int Settings{get;set;}
        public int SiteLocation{get;set;}
        public int Task{get;set;}
        public int TaskAttachment{get;set;}
        public int TaskCanvasCategory{get;set;}
        public int TaskCheckList{get;set;}
        public int TemplateCheckList{get;set;}
        public int TemplateCheckListItem{get;set;}
        public int TransactionLog{get;set;}
        public int User{get;set;}
        public int UserManager{get;set;}
        public int UserRole{get;set;}
        public int VehicleColor{get;set;}
        public int VehicleColor_Audit{get;set;}
        public int VehicleMake{get;set;}
        public int VehicleMake_Audit{get;set;}
        public int VehicleType{get;set;}
        public int Verifier{get;set;}
        public int VerifierService{get;set;}
        public int WebPort { get; set; }
        public int Interval { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }

     public bool IsAsset {get;set;}
     public bool IsUser {get;set;}
     public bool IsAlarm {get;set;}
     public bool IsTask {get;set;}
     public bool IsVerifier {get;set;}
     public bool IsReminders {get;set;}
     public bool IsGroups {get;set;}
     public bool IsLocation {get;set;}
     public bool IsCheckList {get;set;}
     public bool IsAttachmentFiles {get;set;}
     public bool IsMIMSLogs {get;set;}
     public bool IsNotification {get;set;}
     public bool IsTicketing {get;set;}
     public bool IsTransactionLogs {get;set;}
     public bool IsDutyRostersLogs {get;set;}
     public bool IsPostOrders {get;set;}

     public bool IsWarehouse { get; set; }
     public int Warehouse { get; set; }

        public static HouseKeeping GetHouseKeeping(string dbConnection)
        {
            HouseKeeping houseKeeping = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetHouseKeeping", connection);

                sqlCommand.CommandText = "GetHouseKeeping";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    houseKeeping = GetHouseKeepingFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return houseKeeping;
        }

        private static HouseKeeping GetHouseKeepingFromSqlReader(SqlDataReader reader)
        {
            var houseKeeping = new HouseKeeping();
            if (reader["Id"] != DBNull.Value)
                houseKeeping.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Asset"] != DBNull.Value)
                houseKeeping.Asset = Convert.ToInt16(reader["Asset"].ToString());
            if (reader["AssetCategory"] != DBNull.Value)
                houseKeeping.AssetCategory = Convert.ToInt16(reader["AssetCategory"].ToString());
            if (reader["ChatUser"] != DBNull.Value)
                houseKeeping.ChatUser = Convert.ToInt16(reader["ChatUser"].ToString());
            if (reader["ClientDevice"] != DBNull.Value)
                houseKeeping.ClientDevice = Convert.ToInt16(reader["ClientDevice"].ToString());
            if (reader["ConfigSettings"] != DBNull.Value)
                houseKeeping.ConfigSettings = Convert.ToInt16(reader["ConfigSettings"].ToString());
            if (reader["CustomEvent"] != DBNull.Value)
                houseKeeping.CustomEvent = Convert.ToInt16(reader["CustomEvent"].ToString());
            if (reader["DeviceGroup"] != DBNull.Value)
                houseKeeping.DeviceGroup = Convert.ToInt16(reader["DeviceGroup"].ToString());
            if (reader["DeviceWhiteList"] != DBNull.Value)
                houseKeeping.DeviceWhiteList = Convert.ToInt16(reader["DeviceWhiteList"].ToString());
            if (reader["DriverOffence"] != DBNull.Value)
                houseKeeping.DriverOffence = Convert.ToInt16(reader["DriverOffence"].ToString());
            if (reader["DutyRoaster"] != DBNull.Value)
                houseKeeping.DutyRoaster = Convert.ToInt16(reader["DutyRoaster"].ToString());
            if (reader["Enrollment"] != DBNull.Value)
                houseKeeping.Enrollment = Convert.ToInt16(reader["Enrollment"].ToString());
            if (reader["Group"] != DBNull.Value)
                houseKeeping.Group = Convert.ToInt16(reader["Group"].ToString());
            if (reader["GroupDeviceTask"] != DBNull.Value)
                houseKeeping.GroupDeviceTask = Convert.ToInt16(reader["GroupDeviceTask"].ToString());
            if (reader["GroupRole"] != DBNull.Value)
                houseKeeping.GroupRole = Convert.ToInt16(reader["GroupRole"].ToString());
            if (reader["GroupUser"] != DBNull.Value)
                houseKeeping.GroupUser = Convert.ToInt16(reader["GroupUser"].ToString());
            if (reader["HealthCheck"] != DBNull.Value)
                houseKeeping.HealthCheck = Convert.ToInt16(reader["HealthCheck"].ToString());
            if (reader["HotEvent"] != DBNull.Value)
                houseKeeping.HotEvent = Convert.ToInt16(reader["HotEvent"].ToString());
            if (reader["Language"] != DBNull.Value)
                houseKeeping.Language = Convert.ToInt16(reader["Language"].ToString());
            if (reader["LicenceClientDevice"] != DBNull.Value)
                houseKeeping.LicenceClientDevice = Convert.ToInt16(reader["LicenceClientDevice"].ToString());
            if (reader["Location"] != DBNull.Value)
                houseKeeping.Location = Convert.ToInt16(reader["Location"].ToString());
            if (reader["LocationType"] != DBNull.Value)
                houseKeeping.LocationType = Convert.ToInt16(reader["LocationType"].ToString());
            if (reader["Login"] != DBNull.Value)
                houseKeeping.Login = Convert.ToInt16(reader["Login"].ToString());
            if (reader["MailSetting"] != DBNull.Value)
                houseKeeping.MailSetting = Convert.ToInt16(reader["MailSetting"].ToString());
            if (reader["MainSession"] != DBNull.Value)
                houseKeeping.MainSession = Convert.ToInt16(reader["MainSession"].ToString());
            if (reader["Manager"] != DBNull.Value)
                houseKeeping.Manager = Convert.ToInt16(reader["Manager"].ToString());
            if (reader["MIMSConfigSetting"] != DBNull.Value)
                houseKeeping.MIMSConfigSetting = Convert.ToInt16(reader["MIMSConfigSetting"].ToString());
            if (reader["MIMSLog"] != DBNull.Value)
                houseKeeping.MIMSLog = Convert.ToInt16(reader["MIMSLog"].ToString());
            if (reader["MobileDevice"] != DBNull.Value)
                houseKeeping.MobileDevice = Convert.ToInt16(reader["MobileDevice"].ToString());
            if (reader["MobileHotEvent"] != DBNull.Value)
                houseKeeping.MobileHotEvent = Convert.ToInt16(reader["MobileHotEvent"].ToString());
            if (reader["MobileHotEventAttachment"] != DBNull.Value)
                houseKeeping.MobileHotEventAttachment = Convert.ToInt16(reader["MobileHotEventAttachment"].ToString());
            if (reader["MobileHotEventCategory"] != DBNull.Value)
                houseKeeping.MobileHotEventCategory = Convert.ToInt16(reader["MobileHotEventCategory"].ToString());
            if (reader["Notification"] != DBNull.Value)
                houseKeeping.Notification = Convert.ToInt16(reader["Notification"].ToString());
            if (reader["NotificationAttachment"] != DBNull.Value)
                houseKeeping.NotificationAttachment = Convert.ToInt16(reader["NotificationAttachment"].ToString());
            if (reader["NotificationStatus"] != DBNull.Value)
                houseKeeping.NotificationStatus = Convert.ToInt16(reader["NotificationStatus"].ToString());
            if (reader["Offence"] != DBNull.Value)
                houseKeeping.Offence = Convert.ToInt16(reader["Offence"].ToString());
            if (reader["Offence_Audit"] != DBNull.Value)
                houseKeeping.Offence_Audit = Convert.ToInt16(reader["Offence_Audit"].ToString());
            if (reader["OffenceType"] != DBNull.Value)
                houseKeeping.OffenceType = Convert.ToInt16(reader["OffenceType"].ToString());
            if (reader["OffenceType_Audit"] != DBNull.Value)
                houseKeeping.OffenceType_Audit = Convert.ToInt16(reader["OffenceType_Audit"].ToString());
            if (reader["PlateCode"] != DBNull.Value)
                houseKeeping.PlateCode = Convert.ToInt16(reader["PlateCode"].ToString());
            if (reader["PlateSource"] != DBNull.Value)
                houseKeeping.PlateSource = Convert.ToInt16(reader["PlateSource"].ToString());
            if (reader["PostOrder"] != DBNull.Value)
                houseKeeping.PostOrder = Convert.ToInt16(reader["PostOrder"].ToString());
            if (reader["Reminder"] != DBNull.Value)
                houseKeeping.Reminder = Convert.ToInt16(reader["Reminder"].ToString());
            if (reader["ServerDevice"] != DBNull.Value)
                houseKeeping.ServerDevice = Convert.ToInt16(reader["ServerDevice"].ToString());
            if (reader["Settings"] != DBNull.Value)
                houseKeeping.Settings = Convert.ToInt16(reader["Settings"].ToString());
            if (reader["SiteLocation"] != DBNull.Value)
                houseKeeping.SiteLocation = Convert.ToInt16(reader["SiteLocation"].ToString());
            if (reader["Task"] != DBNull.Value)
                houseKeeping.Task = Convert.ToInt16(reader["Task"].ToString());
            if (reader["TaskAttachment"] != DBNull.Value)
                houseKeeping.TaskAttachment = Convert.ToInt16(reader["TaskAttachment"].ToString());
            if (reader["TaskCanvasCategory"] != DBNull.Value)
                houseKeeping.TaskCanvasCategory = Convert.ToInt16(reader["TaskCanvasCategory"].ToString());
            if (reader["TaskCheckList"] != DBNull.Value)
                houseKeeping.TaskCheckList = Convert.ToInt16(reader["TaskCheckList"].ToString());
            if (reader["TemplateCheckList"] != DBNull.Value)
                houseKeeping.TemplateCheckList = Convert.ToInt16(reader["TemplateCheckList"].ToString());
            if (reader["TemplateCheckListItem"] != DBNull.Value)
                houseKeeping.TemplateCheckListItem = Convert.ToInt16(reader["TemplateCheckListItem"].ToString());
            if (reader["TransactionLog"] != DBNull.Value)
                houseKeeping.TransactionLog = Convert.ToInt16(reader["TransactionLog"].ToString());
            if (reader["User"] != DBNull.Value)
                houseKeeping.User = Convert.ToInt16(reader["User"].ToString());
            if (reader["UserManager"] != DBNull.Value)
                houseKeeping.UserManager = Convert.ToInt16(reader["UserManager"].ToString());
            if (reader["UserRole"] != DBNull.Value)
                houseKeeping.UserRole = Convert.ToInt16(reader["UserRole"].ToString());
            if (reader["VehicleColor"] != DBNull.Value)
                houseKeeping.VehicleColor = Convert.ToInt16(reader["VehicleColor"].ToString());
            if (reader["TransactionLog"] != DBNull.Value)
                houseKeeping.TransactionLog = Convert.ToInt16(reader["TransactionLog"].ToString());
            if (reader["VehicleColor_Audit"] != DBNull.Value)
                houseKeeping.VehicleColor_Audit = Convert.ToInt16(reader["VehicleColor_Audit"].ToString());
            if (reader["VehicleMake"] != DBNull.Value)
                houseKeeping.VehicleMake = Convert.ToInt16(reader["VehicleMake"].ToString());
            if (reader["VehicleMake_Audit"] != DBNull.Value)
                houseKeeping.VehicleMake_Audit = Convert.ToInt16(reader["VehicleMake_Audit"].ToString());
            if (reader["VehicleType"] != DBNull.Value)
                houseKeeping.VehicleType = Convert.ToInt16(reader["VehicleType"].ToString());
            if (reader["Verifier"] != DBNull.Value)
                houseKeeping.Verifier = Convert.ToInt16(reader["Verifier"].ToString());
            if (reader["VerifierService"] != DBNull.Value)
                houseKeeping.VerifierService = Convert.ToInt16(reader["VerifierService"].ToString());
            if (reader["WebPort"] != DBNull.Value)
                houseKeeping.WebPort = Convert.ToInt16(reader["WebPort"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                houseKeeping.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                houseKeeping.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                houseKeeping.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedBy"] != DBNull.Value)
                houseKeeping.CreatedBy = reader["UpdatedBy"].ToString();
            if (reader["Interval"] != DBNull.Value)
                houseKeeping.Interval = Convert.ToInt16(reader["Interval"].ToString());


            if (reader["Warehouse"] != DBNull.Value)
                houseKeeping.Warehouse = Convert.ToInt16(reader["Warehouse"].ToString());

            if (reader["IsWarehouse"] != DBNull.Value)
                houseKeeping.IsWarehouse = Convert.ToBoolean(reader["IsWarehouse"].ToString());


            if (reader["IsAsset"] != DBNull.Value)
                houseKeeping.IsAsset = Convert.ToBoolean(reader["IsAsset"].ToString());
            if (reader["IsUser"] != DBNull.Value)
                houseKeeping.IsUser = Convert.ToBoolean(reader["IsUser"].ToString());
            if (reader["IsAlarm"] != DBNull.Value)
                houseKeeping.IsAlarm = Convert.ToBoolean(reader["IsAlarm"].ToString());
            if (reader["IsTask"] != DBNull.Value)
                houseKeeping.IsTask = Convert.ToBoolean(reader["IsTask"].ToString());
            if (reader["IsVerifier"] != DBNull.Value)
                houseKeeping.IsVerifier = Convert.ToBoolean(reader["IsVerifier"].ToString());

            if (reader["IsReminders"] != DBNull.Value)
                houseKeeping.IsReminders = Convert.ToBoolean(reader["IsReminders"].ToString());
            if (reader["IsGroups"] != DBNull.Value)
                houseKeeping.IsGroups = Convert.ToBoolean(reader["IsGroups"].ToString());
            if (reader["IsLocation"] != DBNull.Value)
                houseKeeping.IsLocation = Convert.ToBoolean(reader["IsLocation"].ToString());
            if (reader["IsCheckList"] != DBNull.Value)
                houseKeeping.IsCheckList = Convert.ToBoolean(reader["IsCheckList"].ToString());
            if (reader["IsAttachmentFiles"] != DBNull.Value)
                houseKeeping.IsAttachmentFiles = Convert.ToBoolean(reader["IsAttachmentFiles"].ToString());

            if (reader["IsMIMSLogs"] != DBNull.Value)
                houseKeeping.IsMIMSLogs = Convert.ToBoolean(reader["IsMIMSLogs"].ToString());
            if (reader["IsNotification"] != DBNull.Value)
                houseKeeping.IsNotification = Convert.ToBoolean(reader["IsNotification"].ToString());
            if (reader["IsTicketing"] != DBNull.Value)
                houseKeeping.IsTicketing = Convert.ToBoolean(reader["IsTicketing"].ToString());
            if (reader["IsTransactionLogs"] != DBNull.Value)
                houseKeeping.IsTransactionLogs = Convert.ToBoolean(reader["IsTransactionLogs"].ToString());
            if (reader["IsDutyRostersLogs"] != DBNull.Value)
                houseKeeping.IsDutyRostersLogs = Convert.ToBoolean(reader["IsDutyRostersLogs"].ToString());
            if (reader["IsPostOrders"] != DBNull.Value)
                houseKeeping.IsPostOrders = Convert.ToBoolean(reader["IsPostOrders"].ToString());

            return houseKeeping;
        }

        public static bool InsertOrUpdateHouseKeeping(HouseKeeping houseKeeping, string dbConnection)
        {
            if (houseKeeping != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateHouseKeeping", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = houseKeeping.Id;
                    
                    cmd.Parameters.Add(new SqlParameter("@Asset", SqlDbType.Int));
                    cmd.Parameters["@Asset"].Value = houseKeeping.Asset;
                    cmd.Parameters.Add(new SqlParameter("@AssetCategory", SqlDbType.Int));
                    cmd.Parameters["@AssetCategory"].Value = houseKeeping.AssetCategory;
                    cmd.Parameters.Add(new SqlParameter("@ChatUser", SqlDbType.Int));
                    cmd.Parameters["@ChatUser"].Value = houseKeeping.ChatUser;
                    cmd.Parameters.Add(new SqlParameter("@ClientDevice", SqlDbType.Int));
                    cmd.Parameters["@ClientDevice"].Value = houseKeeping.ClientDevice;
                    cmd.Parameters.Add(new SqlParameter("@ConfigSettings", SqlDbType.Int));
                    cmd.Parameters["@ConfigSettings"].Value = houseKeeping.ConfigSettings;
                    cmd.Parameters.Add(new SqlParameter("@CustomEvent", SqlDbType.Int));
                    cmd.Parameters["@CustomEvent"].Value = houseKeeping.CustomEvent;
                    cmd.Parameters.Add(new SqlParameter("@DeviceGroup", SqlDbType.Int));
                    cmd.Parameters["@DeviceGroup"].Value = houseKeeping.DeviceGroup;
                    cmd.Parameters.Add(new SqlParameter("@DeviceWhiteList", SqlDbType.Int));
                    cmd.Parameters["@DeviceWhiteList"].Value = houseKeeping.DeviceWhiteList;
                    cmd.Parameters.Add(new SqlParameter("@DriverOffence", SqlDbType.Int));
                    cmd.Parameters["@DriverOffence"].Value = houseKeeping.DriverOffence;
                    cmd.Parameters.Add(new SqlParameter("@DutyRoaster", SqlDbType.Int));
                    cmd.Parameters["@DutyRoaster"].Value = houseKeeping.DutyRoaster;
                    cmd.Parameters.Add(new SqlParameter("@Enrollment", SqlDbType.Int));
                    cmd.Parameters["@Enrollment"].Value = houseKeeping.Enrollment;
                    cmd.Parameters.Add(new SqlParameter("@Group", SqlDbType.Int));
                    cmd.Parameters["@Group"].Value = houseKeeping.Group;
                    cmd.Parameters.Add(new SqlParameter("@GroupDeviceTask", SqlDbType.Int));
                    cmd.Parameters["@GroupDeviceTask"].Value = houseKeeping.GroupDeviceTask;
                    cmd.Parameters.Add(new SqlParameter("@GroupRole", SqlDbType.Int));
                    cmd.Parameters["@GroupRole"].Value = houseKeeping.GroupRole;
                    cmd.Parameters.Add(new SqlParameter("@GroupUser", SqlDbType.Int));
                    cmd.Parameters["@GroupUser"].Value = houseKeeping.GroupUser;
                    cmd.Parameters.Add(new SqlParameter("@HealthCheck", SqlDbType.Int));
                    cmd.Parameters["@HealthCheck"].Value = houseKeeping.HealthCheck;
                    cmd.Parameters.Add(new SqlParameter("@HotEvent", SqlDbType.Int));
                    cmd.Parameters["@HotEvent"].Value = houseKeeping.HotEvent;
                    cmd.Parameters.Add(new SqlParameter("@Language", SqlDbType.Int));
                    cmd.Parameters["@Language"].Value = houseKeeping.Language;
                    cmd.Parameters.Add(new SqlParameter("@LicenceClientDevice", SqlDbType.Int));
                    cmd.Parameters["@LicenceClientDevice"].Value = houseKeeping.LicenceClientDevice;
                    cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.Int));
                    cmd.Parameters["@Location"].Value = houseKeeping.Location;
                    cmd.Parameters.Add(new SqlParameter("@LocationType", SqlDbType.Int));
                    cmd.Parameters["@LocationType"].Value = houseKeeping.LocationType;
                    cmd.Parameters.Add(new SqlParameter("@Login", SqlDbType.Int));
                    cmd.Parameters["@Login"].Value = houseKeeping.Login;
                    cmd.Parameters.Add(new SqlParameter("@MailSetting", SqlDbType.Int));
                    cmd.Parameters["@MailSetting"].Value = houseKeeping.MailSetting;
                    cmd.Parameters.Add(new SqlParameter("@MainSession", SqlDbType.Int));
                    cmd.Parameters["@MainSession"].Value = houseKeeping.MainSession;
                    cmd.Parameters.Add(new SqlParameter("@Manager", SqlDbType.Int));
                    cmd.Parameters["@Manager"].Value = houseKeeping.Manager;
                    cmd.Parameters.Add(new SqlParameter("@MIMSConfigSetting", SqlDbType.Int));
                    cmd.Parameters["@MIMSConfigSetting"].Value = houseKeeping.MIMSConfigSetting;
                    cmd.Parameters.Add(new SqlParameter("@MIMSLog", SqlDbType.Int));
                    cmd.Parameters["@MIMSLog"].Value = houseKeeping.MIMSLog;
                    cmd.Parameters.Add(new SqlParameter("@MobileDevice", SqlDbType.Int));
                    cmd.Parameters["@MobileDevice"].Value = houseKeeping.MobileDevice;
                    cmd.Parameters.Add(new SqlParameter("@MobileHotEvent", SqlDbType.Int));
                    cmd.Parameters["@MobileHotEvent"].Value = houseKeeping.MobileHotEvent;
                    cmd.Parameters.Add(new SqlParameter("@MobileHotEventAttachment", SqlDbType.Int));
                    cmd.Parameters["@MobileHotEventAttachment"].Value = houseKeeping.MobileHotEventAttachment;
                    cmd.Parameters.Add(new SqlParameter("@MobileHotEventCategory", SqlDbType.Int));
                    cmd.Parameters["@MobileHotEventCategory"].Value = houseKeeping.MobileHotEventCategory;
                    cmd.Parameters.Add(new SqlParameter("@Notification", SqlDbType.Int));
                    cmd.Parameters["@Notification"].Value = houseKeeping.Notification;
                    cmd.Parameters.Add(new SqlParameter("@NotificationAttachment", SqlDbType.Int));
                    cmd.Parameters["@NotificationAttachment"].Value = houseKeeping.NotificationAttachment;
                    cmd.Parameters.Add(new SqlParameter("@NotificationStatus", SqlDbType.Int));
                    cmd.Parameters["@NotificationStatus"].Value = houseKeeping.NotificationStatus;
                    cmd.Parameters.Add(new SqlParameter("@Offence", SqlDbType.Int));
                    cmd.Parameters["@Offence"].Value = houseKeeping.Offence;
                    cmd.Parameters.Add(new SqlParameter("@Offence_Audit", SqlDbType.Int));
                    cmd.Parameters["@Offence_Audit"].Value = houseKeeping.Offence_Audit;
                    cmd.Parameters.Add(new SqlParameter("@OffenceType", SqlDbType.Int));
                    cmd.Parameters["@OffenceType"].Value = houseKeeping.OffenceType;
                    cmd.Parameters.Add(new SqlParameter("@OffenceType_Audit", SqlDbType.Int));
                    cmd.Parameters["@OffenceType_Audit"].Value = houseKeeping.OffenceType_Audit;
                    cmd.Parameters.Add(new SqlParameter("@PlateCode", SqlDbType.Int));
                    cmd.Parameters["@PlateCode"].Value = houseKeeping.PlateCode;
                    cmd.Parameters.Add(new SqlParameter("@PlateSource", SqlDbType.Int));
                    cmd.Parameters["@PlateSource"].Value = houseKeeping.PlateSource;
                    cmd.Parameters.Add(new SqlParameter("@PostOrder", SqlDbType.Int));
                    cmd.Parameters["@PostOrder"].Value = houseKeeping.PostOrder;
                    cmd.Parameters.Add(new SqlParameter("@Reminder", SqlDbType.Int));
                    cmd.Parameters["@Reminder"].Value = houseKeeping.Reminder;
                    cmd.Parameters.Add(new SqlParameter("@ServerDevice", SqlDbType.Int));
                    cmd.Parameters["@ServerDevice"].Value = houseKeeping.ServerDevice;
                    cmd.Parameters.Add(new SqlParameter("@Settings", SqlDbType.Int));
                    cmd.Parameters["@Settings"].Value = houseKeeping.Settings;
                    cmd.Parameters.Add(new SqlParameter("@SiteLocation", SqlDbType.Int));
                    cmd.Parameters["@SiteLocation"].Value = houseKeeping.SiteLocation;
                    cmd.Parameters.Add(new SqlParameter("@Task", SqlDbType.Int));
                    cmd.Parameters["@Task"].Value = houseKeeping.Task;
                    cmd.Parameters.Add(new SqlParameter("@TaskAttachment", SqlDbType.Int));
                    cmd.Parameters["@TaskAttachment"].Value = houseKeeping.TaskAttachment;
                    cmd.Parameters.Add(new SqlParameter("@TaskCanvasCategory", SqlDbType.Int));
                    cmd.Parameters["@TaskCanvasCategory"].Value = houseKeeping.TaskCanvasCategory;
                    cmd.Parameters.Add(new SqlParameter("@TaskCheckList", SqlDbType.Int));
                    cmd.Parameters["@TaskCheckList"].Value = houseKeeping.TaskCheckList;
                    cmd.Parameters.Add(new SqlParameter("@TemplateCheckList", SqlDbType.Int));
                    cmd.Parameters["@TemplateCheckList"].Value = houseKeeping.TemplateCheckList;
                    cmd.Parameters.Add(new SqlParameter("@TemplateCheckListItem", SqlDbType.Int));
                    cmd.Parameters["@TemplateCheckListItem"].Value = houseKeeping.TemplateCheckListItem;
                    cmd.Parameters.Add(new SqlParameter("@TransactionLog", SqlDbType.Int));
                    cmd.Parameters["@TransactionLog"].Value = houseKeeping.TransactionLog;
                    cmd.Parameters.Add(new SqlParameter("@User", SqlDbType.Int));
                    cmd.Parameters["@User"].Value = houseKeeping.User;
                    cmd.Parameters.Add(new SqlParameter("@UserManager", SqlDbType.Int));
                    cmd.Parameters["@UserManager"].Value = houseKeeping.UserManager;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.Int));
                    cmd.Parameters["@UserRole"].Value = houseKeeping.UserRole;
                    cmd.Parameters.Add(new SqlParameter("@VehicleColor", SqlDbType.Int));
                    cmd.Parameters["@VehicleColor"].Value = houseKeeping.VehicleColor;
                    cmd.Parameters.Add(new SqlParameter("@VehicleColor_Audit", SqlDbType.Int));
                    cmd.Parameters["@VehicleColor_Audit"].Value = houseKeeping.VehicleColor_Audit;
                    cmd.Parameters.Add(new SqlParameter("@VehicleMake", SqlDbType.Int));
                    cmd.Parameters["@VehicleMake"].Value = houseKeeping.VehicleMake;
                    cmd.Parameters.Add(new SqlParameter("@VehicleMake_Audit", SqlDbType.Int));
                    cmd.Parameters["@VehicleMake_Audit"].Value = houseKeeping.VehicleMake_Audit;
                    cmd.Parameters.Add(new SqlParameter("@VehicleType", SqlDbType.Int));
                    cmd.Parameters["@VehicleType"].Value = houseKeeping.VehicleType;
                    cmd.Parameters.Add(new SqlParameter("@Verifier", SqlDbType.Int));
                    cmd.Parameters["@Verifier"].Value = houseKeeping.Verifier;
                    cmd.Parameters.Add(new SqlParameter("@VerifierService", SqlDbType.Int));
                    cmd.Parameters["@VerifierService"].Value = houseKeeping.VerifierService;
                    cmd.Parameters.Add(new SqlParameter("@WebPort", SqlDbType.Int));
                    cmd.Parameters["@WebPort"].Value = houseKeeping.WebPort;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = houseKeeping.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = houseKeeping.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = houseKeeping.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = houseKeeping.UpdatedBy;
                    cmd.Parameters.Add(new SqlParameter("@Interval", SqlDbType.Int));
                    cmd.Parameters["@Interval"].Value = houseKeeping.Interval;


                    cmd.Parameters.Add(new SqlParameter("@Warehouse", SqlDbType.Int));
                    cmd.Parameters["@Warehouse"].Value = houseKeeping.Warehouse;

                    cmd.Parameters.Add(new SqlParameter("@IsWarehouse", SqlDbType.Bit));
                    cmd.Parameters["@IsWarehouse"].Value = houseKeeping.IsWarehouse;


                    cmd.Parameters.Add(new SqlParameter("@IsAsset", SqlDbType.Bit));
                    cmd.Parameters["@IsAsset"].Value = houseKeeping.IsAsset;

                    cmd.Parameters.Add(new SqlParameter("@IsUser", SqlDbType.Bit));
                    cmd.Parameters["@IsUser"].Value = houseKeeping.IsUser;

                    cmd.Parameters.Add(new SqlParameter("@IsAlarm", SqlDbType.Bit));
                    cmd.Parameters["@IsAlarm"].Value = houseKeeping.IsAlarm;

                    cmd.Parameters.Add(new SqlParameter("@IsTask", SqlDbType.Bit));
                    cmd.Parameters["@IsTask"].Value = houseKeeping.IsTask;

                    cmd.Parameters.Add(new SqlParameter("@IsVerifier", SqlDbType.Bit));
                    cmd.Parameters["@IsVerifier"].Value = houseKeeping.IsVerifier;

                    cmd.Parameters.Add(new SqlParameter("@IsReminders", SqlDbType.Bit));
                    cmd.Parameters["@IsReminders"].Value = houseKeeping.IsReminders;

                    cmd.Parameters.Add(new SqlParameter("@IsGroups", SqlDbType.Bit));
                    cmd.Parameters["@IsGroups"].Value = houseKeeping.IsGroups;

                    cmd.Parameters.Add(new SqlParameter("@IsLocation", SqlDbType.Bit));
                    cmd.Parameters["@IsLocation"].Value = houseKeeping.IsLocation;

                    cmd.Parameters.Add(new SqlParameter("@IsCheckList", SqlDbType.Bit));
                    cmd.Parameters["@IsCheckList"].Value = houseKeeping.IsCheckList;

                    cmd.Parameters.Add(new SqlParameter("@IsAttachmentFiles", SqlDbType.Bit));
                    cmd.Parameters["@IsAttachmentFiles"].Value = houseKeeping.IsAttachmentFiles;

                    cmd.Parameters.Add(new SqlParameter("@IsMIMSLogs", SqlDbType.Bit));
                    cmd.Parameters["@IsMIMSLogs"].Value = houseKeeping.IsMIMSLogs;

                    cmd.Parameters.Add(new SqlParameter("@IsNotification", SqlDbType.Bit));
                    cmd.Parameters["@IsNotification"].Value = houseKeeping.IsNotification;

                    cmd.Parameters.Add(new SqlParameter("@IsTicketing", SqlDbType.Bit));
                    cmd.Parameters["@IsTicketing"].Value = houseKeeping.IsTicketing;

                    cmd.Parameters.Add(new SqlParameter("@IsTransactionLogs", SqlDbType.Bit));
                    cmd.Parameters["@IsTransactionLogs"].Value = houseKeeping.IsTransactionLogs;

                    cmd.Parameters.Add(new SqlParameter("@IsDutyRostersLogs", SqlDbType.Bit));
                    cmd.Parameters["@IsDutyRostersLogs"].Value = houseKeeping.IsDutyRostersLogs;

                    cmd.Parameters.Add(new SqlParameter("@IsPostOrders", SqlDbType.Bit));
                    cmd.Parameters["@IsPostOrders"].Value = houseKeeping.IsPostOrders;
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "InsertOrUpdateHouseKeeping";
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static bool DeleteAssetByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteAssetByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteAssetByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteAssetCategoryByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteAssetCategoryByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteAssetCategoryByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteChatUserByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteChatUserByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteChatUserByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteClientDeviceByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteClientDeviceByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteClientDeviceByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteConfigSettingsByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteConfigSettingsByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteConfigSettingsByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteCustomEventByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteCustomEventByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteCustomEventByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteDeviceGroupByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteDeviceGroupByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDeviceGroupByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteDeviceWhiteListByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteDeviceWhiteListByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDeviceWhiteListByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteDriverOffenceByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteDriverOffenceByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDriverOffenceByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteEnrollmentByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteEnrollmentByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteEnrollmentByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteGroupByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteGroupByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteGroupByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteGroupDeviceTaskByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteGroupDeviceTaskByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteGroupDeviceTaskByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteGroupRoleTaskByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteGroupRoleTaskByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteGroupRoleTaskByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteGroupUserTaskByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteGroupUserTaskByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteGroupUserTaskByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteLicenceClientDeviceByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteLicenceClientDeviceByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteLicenceClientDeviceByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteLocationByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteLocationByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteLocationByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteLocationTypeByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteLocationTypeByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteLocationTypeByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteMailSettingByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMailSettingByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMailSettingByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteManagerByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteManagerByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteManagerByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteMIMSConfigSettingByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMIMSConfigSettingByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMIMSConfigSettingByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteMobileDeviceByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMobileDeviceByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMobileDeviceByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteMobileHotEventAttachmentByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMobileHotEventAttachmentByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMobileHotEventAttachmentByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteMobileHotEventByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMobileHotEventByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMobileHotEventByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteMobileHotEventCategoryByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMobileHotEventCategoryByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMobileHotEventCategoryByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteNotificationAttachmentByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteNotificationAttachmentByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteNotificationAttachmentByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteNotificationByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteNotificationByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteNotificationByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteNotificationStatusByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteNotificationStatusByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteNotificationStatusByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteOffenceByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteOffenceByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteOffenceByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeletePlateCodeByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeletePlateCodeByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeletePlateCodeByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeletePlateSourceByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeletePlateSourceByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeletePlateSourceByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeletePostOrderByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeletePostOrderByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeletePostOrderByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteReminderByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteReminderByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteReminderByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteServerDeviceByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteServerDeviceByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteServerDeviceByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteSiteLocationByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteSiteLocationByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteSiteLocationByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteTaskAttachmentByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTaskAttachmentByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTaskAttachmentByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteTaskByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTaskByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTaskByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteTaskCanvasCategoryByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTaskCanvasCategoryByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTaskCanvasCategoryByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteTaskCheckListByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTaskCheckListByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTaskCheckListByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteTemplateCheckListByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTemplateCheckListByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTemplateCheckListByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteTemplateCheckListItemByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTemplateCheckListItemByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTemplateCheckListItemByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteTransactionLogByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTransactionLogByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTransactionLogByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteUserByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteUserByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteUserByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteVehicleColorByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteVehicleColorByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteVehicleColorByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteVehicleMakeByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteVehicleMakeByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteVehicleMakeByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteVehicleTypeByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteVehicleTypeByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteVehicleTypeByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteVerifierByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteVerifierByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteVerifierByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteVerifierServiceByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteVerifierServiceByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteVerifierServiceByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteWebPortByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteWebPortByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteWebPortByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteItemDisposeByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteItemDisposeByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteItemDisposeByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteItemFoundByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteItemFoundByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteItemFoundByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteItemInquiryByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteItemFoundByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteItemFoundByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteItemLostByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteItemLostByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteItemLostByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteItemLostAttachmentByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteItemLostAttachmentByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteItemLostAttachmentByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteItemOwnerByHouseKeepingDate(DateTime delDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteItemOwnerByHouseKeepingDate", connection);

                cmd.Parameters.Add(new SqlParameter("@DeleteDate", SqlDbType.DateTime));
                cmd.Parameters["@DeleteDate"].Value = delDate;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteItemOwnerByHouseKeepingDate";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
