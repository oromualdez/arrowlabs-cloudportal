﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class LanguageClass
    {
        public int Id { get; set; }
        public string LanguageName { get; set; }
        public string Main { get; set; }
        public string Picture { get; set; }
        public string Session { get; set; }
        public string Settings { get; set; }
        public string Delete { get; set; }
        public string UpdatedBy { get; set; }
        public string Save { get; set; }
        public string Connect { get; set; }
        public string Yes { get; set; }
        public string No { get; set; }
        public string Disconnect { get; set; }
        public string Resolution { get; set; }
        public string Camera { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Integrated { get; set; }
        public string XtralisSync { get; set; }
        public string PlateData { get; set; }
        public string VehicleData { get; set; }
        public string Fine { get; set; }
        public string Process { get; set; }
        public string PlateNo { get; set; }

        public string Source { get; set; }
        public string Maker { get; set; }
        public string Color { get; set; }
        public string Subtract { get; set; }
        public string Add { get; set; }
        public string Driving { get; set; }
        public string Parking { get; set; }
        public string Regulation { get; set; }
        public string Usage { get; set; }
        public string Other { get; set; }
        public string Overpass { get; set; }
        public string Port { get; set; }
        public string EnterIP { get; set; }
        public string ErrorMessage1 { get; set; }
        public string ErrorMessage2 { get; set; }
        public string ErrorMessage3 { get; set; }
        public string ErrorMessage4 { get; set; }
        public string ErrorMessage5 { get; set; }
        public string ErrorMessage6 { get; set; }
        public string ErrorMessage7 { get; set; }
        public string ErrorMessage8 { get; set; }
        public string ErrorMessage9 { get; set; }
        public string ErrorMessage10 { get; set; }
        public string ErrorMessage11 { get; set; }
        public string ErrorMessage12 { get; set; }
        public string ErrorMessage13 { get; set; }
        public string ErrorMessage14 { get; set; }
        public string ErrorMessage15 { get; set; }
        public string ErrorMessage16 { get; set; }
        public string ErrorMessage17 { get; set; }
        public string ErrorMessage18 { get; set; }
        public string ErrorMessage19 { get; set; }
        public string ErrorMessage20 { get; set; }

        public string HotAvailable { get; set; }
        public string GPSAvailable { get; set; }
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string E { get; set; }
        public string F { get; set; }
        public string G { get; set; }
        public string H { get; set; }
        public string I { get; set; }
        public string J { get; set; }
        public string K { get; set; }
        public string L { get; set; }
        public string M { get; set; }
        public string N { get; set; }
        public string O { get; set; }
        public string P { get; set; }
        public string Q { get; set; }
        public string R { get; set; }
        public string S { get; set; }
        public string T { get; set; }
        public string U { get; set; }
        public string V { get; set; }
        public string W { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Z { get; set; }
        public string Period { get; set; }
        public string ALL { get; set; }
        public string Dash { get; set; }
        public string PlaybackRequest { get; set; }
        public string MainA { get; set; }
        public string MainB { get; set; }

        public string FineSource1 { get; set; }
        public string FineSource2 { get; set; }
        public string FineSource3 { get; set; }
        public string FineSource4 { get; set; }
        public string FineSource5 { get; set; }
        public string FineSource6 { get; set; }
        public string FineSource7 { get; set; }

        public string FineCode1 { get; set; }
        public string FineCode2 { get; set; }
        public string FineCode3 { get; set; }

        public string RED { get; set; }
        public string ORANGE { get; set; }
        public string YELLOW { get; set; }
        public string WHITE { get; set; }
        public string BLACK { get; set; }
        public string BLUE { get; set; }
        public string CASTLE { get; set; }
        public string MOTORCYCLE { get; set; }

        public string LoginPageTB1 { get; set; }
        public string LoginPageTB2 { get; set; }
        public string LoginPageButton { get; set; }
        public string LoginPageErrorMessage { get; set; }

        public string Sessions { get; set; }
        public string Canvas { get; set; }
        public string Notify { get; set; }
        public string Control { get; set; }
        public string Video { get; set; }

        public string Draw { get; set; }
        public string Select { get; set; }
        public string Crop { get; set; }
        public string Toggle { get; set; }
        public string Clear { get; set; }

        public string More { get; set; }
        public string Less { get; set; }
        public string Verifier { get; set; }
        public string HotEvent { get; set; }
        public string GPS { get; set; }

        public string Reset { get; set; }
        public string Library { get; set; }
        public string Capture { get; set; }
        public string Snap { get; set; }
        public string Enroll { get; set; }

        public string Verify { get; set; }
        public string Reason { get; set; }
        public string View { get; set; }
        public string Details { get; set; }
        public string Alarm { get; set; }

        public string NewPageTB1 { get; set; }
        public string NewPageTB2 { get; set; }
        public string NewPageTB3 { get; set; }
        public string NewPageTB4 { get; set; }
        public string NewPageTB5 { get; set; }
        public string NewPageErrorMessage1 { get; set; }
        public string NewPageErrorMessage2 { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string Request { get; set; }

        public string PoliceDog { get; set; }

        public string Ambulance { get; set; }

        public string SWAT { get; set; }

        public string BSquad { get; set; }

        public string RequestSuccess { get; set; }

        public string RequestFailed { get; set; }

        public string LoginFail { get; set; }

        public string NoAlarmLocation { get; set; }

        public string Assistance { get; set; }

        public string Notification { get; set; }

        public string ViewDetails { get; set; }

        public string Instructions { get; set; }

        public string ViewLocation { get; set; }

        //New Columns for MIMS Portal
        public string NewIncident  { get; set; }
        public string CreateNewIncident { get; set; }
        public string Receivedby { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public string SelectLocation { get; set; }
        public string Lat { get; set; }
        public string IncidentName { get; set; }
        public string IncidentDescription { get; set; }
        public string Next { get; set; }

        public string ClearSavedLocation { get; set; }
        public string Cameras { get; set; }
        public string Task { get; set; }
        //public string Instructions { get; set; }
        public string Back { get; set; }

        public string Park { get; set; }
        public string Dispatch { get; set; }
        public string DispatchFromUsers { get; set; }
        public string Longi { get; set; }
        public static LanguageClass GetLanguage(string dbConnection)
        {
            var langClass = new LanguageClass();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetLanguage", connection);

                sqlCommand.CommandText = "GetLanguage";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    langClass = GetLanguageFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return langClass;
        }

        public static List<LanguageClass> GetAllLanguage(string dbConnection)
        {
            var langClassList = new List<LanguageClass>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllLanguage", connection);

                sqlCommand.CommandText = "GetAllLanguage";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {

                    var langClass = GetLanguageFromSqlReader(reader);
                    langClassList.Add(langClass);
                }
                connection.Close();
                reader.Close();
            }
            return langClassList;
        }

        private static LanguageClass GetLanguageFromSqlReader(SqlDataReader reader)
        {
            var langClass = new LanguageClass();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["LanguageName"] != DBNull.Value)
                langClass.LanguageName = reader["LanguageName"].ToString();
            if (reader["Main"] != DBNull.Value)
                langClass.Main = reader["Main"].ToString();
            if (reader["Picture"] != DBNull.Value)
                langClass.Picture = reader["Picture"].ToString();
            if (reader["Session"] != DBNull.Value)
                langClass.Session = reader["Session"].ToString();
            if (reader["Settings"] != DBNull.Value)
                langClass.Settings = reader["Settings"].ToString();
            if (reader["DeleteWord"] != DBNull.Value)
                langClass.Delete = reader["DeleteWord"].ToString();
            if (reader["SaveWord"] != DBNull.Value)
                langClass.Save = reader["SaveWord"].ToString();
            if (reader["Connect"] != DBNull.Value)
                langClass.Connect = reader["Connect"].ToString();
            if (reader["Yes"] != DBNull.Value)
                langClass.Yes = reader["Yes"].ToString();
            if (reader["No"] != DBNull.Value)
                langClass.No = reader["No"].ToString();
            if (reader["Disconnect"] != DBNull.Value)
                langClass.Disconnect = reader["Disconnect"].ToString();
            if (reader["Resolution"] != DBNull.Value)
                langClass.Resolution = reader["Resolution"].ToString();
            if (reader["Camera"] != DBNull.Value)
                langClass.Camera = reader["Camera"].ToString();
            if (reader["UserName"] != DBNull.Value)
                langClass.UserName = reader["UserName"].ToString();
            if (reader["Password"] != DBNull.Value)
                langClass.Password = reader["Password"].ToString();
            if (reader["Integrated"] != DBNull.Value)
                langClass.Integrated = reader["Integrated"].ToString();
            if (reader["Sync"] != DBNull.Value)
                langClass.XtralisSync = reader["Sync"].ToString();
            if (reader["PlateData"] != DBNull.Value)
                langClass.PlateData = reader["PlateData"].ToString();
            if (reader["VehicleData"] != DBNull.Value)
                langClass.VehicleData = reader["VehicleData"].ToString();
            if (reader["Fine"] != DBNull.Value)
                langClass.Fine = reader["Fine"].ToString();
            if (reader["Process"] != DBNull.Value)
                langClass.Process = reader["Process"].ToString();
            if (reader["PlateNo"] != DBNull.Value)
                langClass.PlateNo = reader["PlateNo"].ToString();
            if (reader["Source"] != DBNull.Value)
                langClass.Source = reader["Source"].ToString();
            if (reader["Maker"] != DBNull.Value)
                langClass.Maker = reader["Maker"].ToString();
            if (reader["Color"] != DBNull.Value)
                langClass.Color = reader["Color"].ToString();
            if (reader["Subtract"] != DBNull.Value)
                langClass.Subtract = reader["Subtract"].ToString();
            if (reader["AddWord"] != DBNull.Value)
                langClass.Add = reader["AddWord"].ToString();
            if (reader["Driving"] != DBNull.Value)
                langClass.Driving = reader["Driving"].ToString();
            if (reader["Parking"] != DBNull.Value)
                langClass.Parking = reader["Parking"].ToString();
            if (reader["Regulation"] != DBNull.Value)
                langClass.Regulation = reader["Regulation"].ToString();
            if (reader["Usage"] != DBNull.Value)
                langClass.Usage = reader["Usage"].ToString();
            if (reader["Other"] != DBNull.Value)
                langClass.Other = reader["Other"].ToString();
            if (reader["Overpass"] != DBNull.Value)
                langClass.Overpass = reader["Overpass"].ToString();
            if (reader["Port"] != DBNull.Value)
                langClass.Port = reader["Port"].ToString();
            if (reader["EnterIP"] != DBNull.Value)
                langClass.EnterIP = reader["EnterIP"].ToString();
            if (reader["ErrorMessage1"] != DBNull.Value)
                langClass.ErrorMessage1 = reader["ErrorMessage1"].ToString();
            if (reader["ErrorMessage2"] != DBNull.Value)
                langClass.ErrorMessage2 = reader["ErrorMessage2"].ToString();
            if (reader["ErrorMessage3"] != DBNull.Value)
                langClass.ErrorMessage3 = reader["ErrorMessage3"].ToString();
            if (reader["ErrorMessage4"] != DBNull.Value)
                langClass.ErrorMessage4 = reader["ErrorMessage4"].ToString();
            if (reader["ErrorMessage5"] != DBNull.Value)
                langClass.ErrorMessage5 = reader["ErrorMessage5"].ToString();
            if (reader["ErrorMessage6"] != DBNull.Value)
                langClass.ErrorMessage6 = reader["ErrorMessage6"].ToString();
            if (reader["ErrorMessage7"] != DBNull.Value)
                langClass.ErrorMessage7 = reader["ErrorMessage7"].ToString();
            if (reader["ErrorMessage8"] != DBNull.Value)
                langClass.ErrorMessage8 = reader["ErrorMessage8"].ToString();
            if (reader["ErrorMessage9"] != DBNull.Value)
                langClass.ErrorMessage9 = reader["ErrorMessage9"].ToString();
            if (reader["ErrorMessage10"] != DBNull.Value)
                langClass.ErrorMessage10 = reader["ErrorMessage10"].ToString();
            if (reader["ErrorMessage11"] != DBNull.Value)
                langClass.ErrorMessage11 = reader["ErrorMessage11"].ToString();
            if (reader["ErrorMessage12"] != DBNull.Value)
                langClass.ErrorMessage12 = reader["ErrorMessage12"].ToString();
            if (reader["ErrorMessage13"] != DBNull.Value)
                langClass.ErrorMessage13 = reader["ErrorMessage13"].ToString();
            if (reader["ErrorMessage14"] != DBNull.Value)
                langClass.ErrorMessage14 = reader["ErrorMessage14"].ToString();
            if (reader["ErrorMessage15"] != DBNull.Value)
                langClass.ErrorMessage15 = reader["ErrorMessage15"].ToString();
            if (reader["ErrorMessage16"] != DBNull.Value)
                langClass.ErrorMessage16 = reader["ErrorMessage16"].ToString();
            if (reader["ErrorMessage17"] != DBNull.Value)
                langClass.ErrorMessage17 = reader["ErrorMessage17"].ToString();
            if (reader["ErrorMessage18"] != DBNull.Value)
                langClass.ErrorMessage18 = reader["ErrorMessage18"].ToString();
            if (reader["ErrorMessage19"] != DBNull.Value)
                langClass.ErrorMessage19 = reader["ErrorMessage19"].ToString();
            if (reader["ErrorMessage20"] != DBNull.Value)
                langClass.ErrorMessage20 = reader["ErrorMessage20"].ToString();
            if (reader["HotAvailable"] != DBNull.Value)
                langClass.HotAvailable = reader["HotAvailable"].ToString();
            if (reader["GPSAvailable"] != DBNull.Value)
                langClass.GPSAvailable = reader["GPSAvailable"].ToString();
            if (reader["A"] != DBNull.Value)
                langClass.A = reader["A"].ToString();
            if (reader["B"] != DBNull.Value)
                langClass.B = reader["B"].ToString();
            if (reader["C"] != DBNull.Value)
                langClass.C = reader["C"].ToString();
            if (reader["D"] != DBNull.Value)
                langClass.D = reader["D"].ToString();
            if (reader["E"] != DBNull.Value)
                langClass.E = reader["E"].ToString();
            if (reader["F"] != DBNull.Value)
                langClass.F = reader["F"].ToString();
            if (reader["G"] != DBNull.Value)
                langClass.G = reader["G"].ToString();
            if (reader["H"] != DBNull.Value)
                langClass.H = reader["H"].ToString();
            if (reader["I"] != DBNull.Value)
                langClass.I = reader["I"].ToString();
            if (reader["J"] != DBNull.Value)
                langClass.J = reader["J"].ToString();
            if (reader["K"] != DBNull.Value)
                langClass.K = reader["K"].ToString();
            if (reader["L"] != DBNull.Value)
                langClass.L = reader["L"].ToString();
            if (reader["M"] != DBNull.Value)
                langClass.M = reader["M"].ToString();
            if (reader["N"] != DBNull.Value)
                langClass.N = reader["N"].ToString();
            if (reader["O"] != DBNull.Value)
                langClass.O = reader["O"].ToString();
            if (reader["P"] != DBNull.Value)
                langClass.P = reader["P"].ToString();
            if (reader["Q"] != DBNull.Value)
                langClass.Q = reader["Q"].ToString();
            if (reader["R"] != DBNull.Value)
                langClass.R = reader["R"].ToString();
            if (reader["S"] != DBNull.Value)
                langClass.S = reader["S"].ToString();
            if (reader["T"] != DBNull.Value)
                langClass.T = reader["T"].ToString();
            if (reader["U"] != DBNull.Value)
                langClass.U = reader["U"].ToString();
            if (reader["V"] != DBNull.Value)
                langClass.V = reader["V"].ToString();
            if (reader["W"] != DBNull.Value)
                langClass.W = reader["W"].ToString();
            if (reader["X"] != DBNull.Value)
                langClass.X = reader["X"].ToString();
            if (reader["Y"] != DBNull.Value)
                langClass.Y = reader["Y"].ToString();
            if (reader["Z"] != DBNull.Value)
                langClass.Z = reader["Z"].ToString();
            if (reader["Period"] != DBNull.Value)
                langClass.Period = reader["Period"].ToString();
            if (reader["ALL"] != DBNull.Value)
                langClass.ALL = reader["ALL"].ToString();
            if (reader["Dash"] != DBNull.Value)
                langClass.Dash = reader["Dash"].ToString();
            if (reader["PlaybackRequest"] != DBNull.Value)
                langClass.PlaybackRequest = reader["PlaybackRequest"].ToString();
            if (reader["MainA"] != DBNull.Value)
                langClass.MainA = reader["MainA"].ToString();
            if (reader["MainB"] != DBNull.Value)
                langClass.MainB = reader["MainB"].ToString();
            if (reader["FineSource1"] != DBNull.Value)
                langClass.FineSource1 = reader["FineSource1"].ToString();
            if (reader["FineSource2"] != DBNull.Value)
                langClass.FineSource2 = reader["FineSource2"].ToString();
            if (reader["FineSource3"] != DBNull.Value)
                langClass.FineSource3 = reader["FineSource3"].ToString();
            if (reader["FineSource4"] != DBNull.Value)
                langClass.FineSource4 = reader["FineSource4"].ToString();
            if (reader["FineSource5"] != DBNull.Value)
                langClass.FineSource5 = reader["FineSource5"].ToString();
            if (reader["FineSource6"] != DBNull.Value)
                langClass.FineSource6 = reader["FineSource6"].ToString();
            if (reader["FineSource7"] != DBNull.Value)
                langClass.FineSource7 = reader["FineSource7"].ToString();
            if (reader["FineCode1"] != DBNull.Value)
                langClass.FineCode1 = reader["FineCode1"].ToString();
            if (reader["FineCode2"] != DBNull.Value)
                langClass.FineCode2 = reader["FineCode2"].ToString();
            if (reader["FineCode3"] != DBNull.Value)
                langClass.FineCode3 = reader["FineCode3"].ToString();
            if (reader["RED"] != DBNull.Value)
                langClass.RED = reader["RED"].ToString();
            if (reader["ORANGE"] != DBNull.Value)
                langClass.ORANGE = reader["ORANGE"].ToString();
            if (reader["YELLOW"] != DBNull.Value)
                langClass.YELLOW = reader["YELLOW"].ToString();
            if (reader["WHITE"] != DBNull.Value)
                langClass.WHITE = reader["WHITE"].ToString();
            if (reader["BLACK"] != DBNull.Value)
                langClass.BLACK = reader["BLACK"].ToString();
            if (reader["BLUE"] != DBNull.Value)
                langClass.BLUE = reader["BLUE"].ToString();
            if (reader["CASTLE"] != DBNull.Value)
                langClass.CASTLE = reader["CASTLE"].ToString();
            if (reader["MOTORCYCLE"] != DBNull.Value)
                langClass.MOTORCYCLE = reader["MOTORCYCLE"].ToString();
            if (reader["LoginPageTB1"] != DBNull.Value)
                langClass.LoginPageTB1 = reader["LoginPageTB1"].ToString();
            if (reader["LoginPageTB2"] != DBNull.Value)
                langClass.LoginPageTB2 = reader["LoginPageTB2"].ToString();
            if (reader["LoginPageButton"] != DBNull.Value)
                langClass.LoginPageButton = reader["LoginPageButton"].ToString();
            if (reader["LoginPageErrorMessage"] != DBNull.Value)
                langClass.LoginPageErrorMessage = reader["LoginPageErrorMessage"].ToString();
            if (reader["NewPageTB1"] != DBNull.Value)
                langClass.NewPageTB1 = reader["NewPageTB1"].ToString();
            if (reader["NewPageTB2"] != DBNull.Value)
                langClass.NewPageTB2 = reader["NewPageTB2"].ToString();
            if (reader["NewPageTB3"] != DBNull.Value)
                langClass.NewPageTB3 = reader["NewPageTB3"].ToString();
            if (reader["NewPageTB4"] != DBNull.Value)
                langClass.NewPageTB4 = reader["NewPageTB4"].ToString();
            if (reader["NewPageTB5"] != DBNull.Value)
                langClass.NewPageTB5 = reader["NewPageTB5"].ToString();
            
            if (reader["NewPageErrorMessage1"] != DBNull.Value)
                langClass.NewPageErrorMessage1 = reader["NewPageErrorMessage1"].ToString();
            if (reader["NewPageErrorMessage2"] != DBNull.Value)
                langClass.NewPageErrorMessage2 = reader["NewPageErrorMessage2"].ToString();
            if (reader["UpdatedBy"] != DBNull.Value)
                langClass.UpdatedBy = reader["UpdatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                langClass.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (columns.Contains( "Sessions"))
            {
                if (reader["Sessions"] != DBNull.Value)
                    langClass.Sessions = reader["Sessions"].ToString();
                if (reader["Canvas"] != DBNull.Value)
                    langClass.Canvas = reader["Canvas"].ToString();
                if (reader["Notify"] != DBNull.Value)
                    langClass.Notify = reader["Notify"].ToString();
                if (reader["Video"] != DBNull.Value)
                    langClass.Video = reader["Video"].ToString();
                if (reader["Control"] != DBNull.Value)
                    langClass.Control = reader["Control"].ToString();

                if (reader["Draw"] != DBNull.Value)
                    langClass.Draw = reader["Draw"].ToString();
                if (reader["Select"] != DBNull.Value)
                    langClass.Select = reader["Select"].ToString();
                if (reader["Crop"] != DBNull.Value)
                    langClass.Crop = reader["Crop"].ToString();
                if (reader["Toggle"] != DBNull.Value)
                    langClass.Toggle = reader["Toggle"].ToString();
                if (reader["Clear"] != DBNull.Value)
                    langClass.Clear = reader["Clear"].ToString();

                if (reader["More"] != DBNull.Value)
                    langClass.More = reader["More"].ToString();
                if (reader["Less"] != DBNull.Value)
                    langClass.Less = reader["Less"].ToString();
                if (reader["Verifier"] != DBNull.Value)
                    langClass.Verifier = reader["Verifier"].ToString();
                if (reader["HotEvent"] != DBNull.Value)
                    langClass.HotEvent = reader["HotEvent"].ToString();
                if (reader["GPS"] != DBNull.Value)
                    langClass.GPS = reader["GPS"].ToString();

                if (reader["Reset"] != DBNull.Value)
                    langClass.Reset = reader["Reset"].ToString();
                if (reader["Library"] != DBNull.Value)
                    langClass.Library = reader["Library"].ToString();
                if (reader["Capture"] != DBNull.Value)
                    langClass.Capture = reader["Capture"].ToString();
                if (reader["Snap"] != DBNull.Value)
                    langClass.Snap = reader["Snap"].ToString();
                if (reader["Enroll"] != DBNull.Value)
                    langClass.Enroll = reader["Enroll"].ToString();

                if (reader["Verify"] != DBNull.Value)
                    langClass.Verify = reader["Verify"].ToString();
                if (reader["Reason"] != DBNull.Value)
                    langClass.Reason = reader["Reason"].ToString();
                if (reader["View"] != DBNull.Value)
                    langClass.View = reader["View"].ToString();
                if (reader["Details"] != DBNull.Value)
                    langClass.Details = reader["Details"].ToString();
                if (reader["Alarm"] != DBNull.Value)
                    langClass.Alarm = reader["Alarm"].ToString();
            }
            if (columns.Contains( "Request"))
            {
                if (reader["Request"] != DBNull.Value)
                    langClass.Request = reader["Request"].ToString();
            }
            if (columns.Contains( "PoliceDog"))
            {
                if (reader["PoliceDog"] != DBNull.Value)
                    langClass.PoliceDog = reader["PoliceDog"].ToString();
            }
            if (columns.Contains( "Ambulance"))
            {
                if (reader["Ambulance"] != DBNull.Value)
                    langClass.Ambulance = reader["Ambulance"].ToString();
            }
            if (columns.Contains( "SWAT"))
            {
                if (reader["SWAT"] != DBNull.Value)
                    langClass.SWAT = reader["SWAT"].ToString();
            }
            if (columns.Contains( "BSquad"))
            {
                if (reader["BSquad"] != DBNull.Value)
                    langClass.BSquad = reader["BSquad"].ToString();
            }
            if (columns.Contains( "RequestSuccess"))
            {
                if (reader["RequestSuccess"] != DBNull.Value)
                    langClass.RequestSuccess = reader["RequestSuccess"].ToString();
            }
            if (columns.Contains( "RequestFailed"))
            {
                if (reader["RequestFailed"] != DBNull.Value)
                    langClass.RequestFailed = reader["RequestFailed"].ToString();
            }
            if (columns.Contains( "LoginFail"))
            {
                if (reader["LoginFail"] != DBNull.Value)
                    langClass.LoginFail = reader["LoginFail"].ToString();
            }
            if (columns.Contains( "NoAlarmLocation"))
            {
                if (reader["NoAlarmLocation"] != DBNull.Value)
                    langClass.NoAlarmLocation = reader["NoAlarmLocation"].ToString();
            }
            if (columns.Contains( "Assistance"))
            {
                if (reader["Assistance"] != DBNull.Value)
                    langClass.Assistance = reader["Assistance"].ToString();
            }
            if (columns.Contains( "ViewDetails"))
            {
                if (reader["ViewDetails"] != DBNull.Value)
                    langClass.ViewDetails = reader["ViewDetails"].ToString();
            }
            if (columns.Contains( "Instructions"))
            {
                if (reader["Instructions"] != DBNull.Value)
                    langClass.Instructions = reader["Instructions"].ToString();
            }
            if (columns.Contains( "ViewLocation"))
            {
                if (reader["ViewLocation"] != DBNull.Value)
                    langClass.ViewLocation = reader["ViewLocation"].ToString();
            }

            if (columns.Contains( "NewIncident"))
            {
                if (reader["NewIncident"] != DBNull.Value)
                    langClass.NewIncident = reader["NewIncident"].ToString();
            }

            if (columns.Contains( "CreateNewIncident"))
            {
                if (reader["CreateNewIncident"] != DBNull.Value)
                    langClass.CreateNewIncident = reader["CreateNewIncident"].ToString();
            }
            if (columns.Contains( "Receivedby"))
            {
                if (reader["Receivedby"] != DBNull.Value)
                    langClass.Receivedby = reader["Receivedby"].ToString();
            }
            if (columns.Contains( "PhoneNumber"))
            {
                if (reader["PhoneNumber"] != DBNull.Value)
                    langClass.PhoneNumber = reader["PhoneNumber"].ToString();
            }
            if (columns.Contains( "Email"))
            {
                if (reader["Email"] != DBNull.Value)
                    langClass.Email = reader["Email"].ToString();
            }
            if (columns.Contains( "SelectLocation"))
            {
                if (reader["SelectLocation"] != DBNull.Value)
                    langClass.SelectLocation = reader["SelectLocation"].ToString();
            }
            if (columns.Contains( "Lat"))
            {
                if (reader["Lat"] != DBNull.Value)
                    langClass.Lat = reader["Lat"].ToString();
            }

            if (columns.Contains( "IncidentName"))
            {
                if (reader["IncidentName"] != DBNull.Value)
                    langClass.IncidentName = reader["IncidentName"].ToString();
            }
            if (columns.Contains( "IncidentDescription"))
            {
                if (reader["IncidentDescription"] != DBNull.Value)
                    langClass.IncidentDescription = reader["IncidentDescription"].ToString();
            }

            if (columns.Contains( "Next"))
            {
                if (reader["Next"] != DBNull.Value)
                    langClass.Next = reader["Next"].ToString();
            }
            if (columns.Contains( "ClearSavedLocation"))
            {
                if (reader["ClearSavedLocation"] != DBNull.Value)
                    langClass.ClearSavedLocation = reader["ClearSavedLocation"].ToString();
            }
            if (columns.Contains( "Cameras"))
            {
                if (reader["Cameras"] != DBNull.Value)
                    langClass.Cameras = reader["Cameras"].ToString();
            }
            if (columns.Contains( "Task"))
            {
                if (reader["Task"] != DBNull.Value)
                    langClass.Task = reader["Task"].ToString();
            }
            if (columns.Contains( "Back"))
            {
                if (reader["Back"] != DBNull.Value)
                    langClass.Back = reader["Back"].ToString();
            }
            if (columns.Contains( "Park"))
            {
                if (reader["Park"] != DBNull.Value)
                    langClass.Park = reader["Park"].ToString();
            }
            if (columns.Contains( "Dispatch"))
            {
                if (reader["Dispatch"] != DBNull.Value)
                    langClass.Dispatch = reader["Dispatch"].ToString();
            }
            if (columns.Contains( "DispatchFromUsers"))
            {
                if (reader["DispatchFromUsers"] != DBNull.Value)
                    langClass.DispatchFromUsers = reader["DispatchFromUsers"].ToString();
            }
            if (columns.Contains( "Longi"))
            {
                if (reader["Longi"] != DBNull.Value)
                    langClass.Longi = reader["Longi"].ToString();
            }
            return langClass;
        }

        public static LanguageClass GetLanguageByDate(DateTime updatedDate, string dbConnection)
        {
            var langClass = new LanguageClass();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetLanguageByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetLanguageByDate";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    langClass = GetLanguageFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return langClass;
        }

        public static LanguageClass GetLanguageByName(string name, string dbConnection)
        {
            LanguageClass langClass = new LanguageClass();
           
            try
            { 
                using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetLanguageByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetLanguageByName";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    langClass = GetLanguageFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }}
                catch{}
            return langClass;
        }

        public static bool InsertOrUpdateSettings(LanguageClass langClass, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)

        {
            if (langClass != null)
            {
                int id = langClass.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateLanguage", connection);

                    cmd.Parameters.Add(new SqlParameter("@LanguageName", SqlDbType.NVarChar));
                    cmd.Parameters["@LanguageName"].Value = langClass.LanguageName;

                    cmd.Parameters.Add(new SqlParameter("@Main", SqlDbType.NVarChar));
                    cmd.Parameters["@Main"].Value = langClass.Main;

                    cmd.Parameters.Add(new SqlParameter("@Picture", SqlDbType.NVarChar));
                    cmd.Parameters["@Picture"].Value = langClass.Picture;

                    cmd.Parameters.Add(new SqlParameter("@Session", SqlDbType.NVarChar));
                    cmd.Parameters["@Session"].Value = langClass.Session;

                    cmd.Parameters.Add(new SqlParameter("@Settings", SqlDbType.NVarChar));
                    cmd.Parameters["@Settings"].Value = langClass.Settings;

                    cmd.Parameters.Add(new SqlParameter("@DeleteWord", SqlDbType.NVarChar));
                    cmd.Parameters["@DeleteWord"].Value = langClass.Delete;

                    cmd.Parameters.Add(new SqlParameter("@SaveWord", SqlDbType.NVarChar));
                    cmd.Parameters["@SaveWord"].Value = langClass.Save;

                    cmd.Parameters.Add(new SqlParameter("@Connect", SqlDbType.NVarChar));
                    cmd.Parameters["@Connect"].Value = langClass.Connect;

                    cmd.Parameters.Add(new SqlParameter("@Yes", SqlDbType.NVarChar));
                    cmd.Parameters["@Yes"].Value = langClass.Yes;

                    cmd.Parameters.Add(new SqlParameter("@No", SqlDbType.NVarChar));
                    cmd.Parameters["@No"].Value = langClass.No;

                    cmd.Parameters.Add(new SqlParameter("@Disconnect", SqlDbType.NVarChar));
                    cmd.Parameters["@Disconnect"].Value = langClass.Disconnect;

                    cmd.Parameters.Add(new SqlParameter("@Resolution", SqlDbType.NVarChar));
                    cmd.Parameters["@Resolution"].Value = langClass.Resolution;

                    cmd.Parameters.Add(new SqlParameter("@Camera", SqlDbType.NVarChar));
                    cmd.Parameters["@Camera"].Value = langClass.Camera;
                    //
                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = langClass.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                    cmd.Parameters["@Password"].Value = langClass.Password;

                    cmd.Parameters.Add(new SqlParameter("@Integrated", SqlDbType.NVarChar));
                    cmd.Parameters["@Integrated"].Value = langClass.Integrated;

                    cmd.Parameters.Add(new SqlParameter("@Sync", SqlDbType.NVarChar));
                    cmd.Parameters["@Sync"].Value = langClass.XtralisSync;

                    cmd.Parameters.Add(new SqlParameter("@PlateData", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateData"].Value = langClass.PlateData;

                    cmd.Parameters.Add(new SqlParameter("@VehicleData", SqlDbType.NVarChar));
                    cmd.Parameters["@VehicleData"].Value = langClass.VehicleData;

                    cmd.Parameters.Add(new SqlParameter("@Fine", SqlDbType.NVarChar));
                    cmd.Parameters["@Fine"].Value = langClass.Fine;

                    cmd.Parameters.Add(new SqlParameter("@Process", SqlDbType.NVarChar));
                    cmd.Parameters["@Process"].Value = langClass.Process;

                    cmd.Parameters.Add(new SqlParameter("@PlateNo", SqlDbType.NVarChar));
                    cmd.Parameters["@PlateNo"].Value = langClass.PlateNo;

                    cmd.Parameters.Add(new SqlParameter("@Source", SqlDbType.NVarChar));
                    cmd.Parameters["@Source"].Value = langClass.Source;

                    cmd.Parameters.Add(new SqlParameter("@Maker", SqlDbType.NVarChar));
                    cmd.Parameters["@Maker"].Value = langClass.Maker;

                    cmd.Parameters.Add(new SqlParameter("@Color", SqlDbType.NVarChar));
                    cmd.Parameters["@Color"].Value = langClass.Color;

                    cmd.Parameters.Add(new SqlParameter("@Subtract", SqlDbType.NVarChar));
                    cmd.Parameters["@Subtract"].Value = langClass.Subtract;

                    cmd.Parameters.Add(new SqlParameter("@AddWord", SqlDbType.NVarChar));
                    cmd.Parameters["@AddWord"].Value = langClass.Add;

                    cmd.Parameters.Add(new SqlParameter("@Driving", SqlDbType.NVarChar));
                    cmd.Parameters["@Driving"].Value = langClass.Driving;

                    cmd.Parameters.Add(new SqlParameter("@Parking", SqlDbType.NVarChar));
                    cmd.Parameters["@Parking"].Value = langClass.Parking;

                    cmd.Parameters.Add(new SqlParameter("@Regulation", SqlDbType.NVarChar));
                    cmd.Parameters["@Regulation"].Value = langClass.Regulation;

                    cmd.Parameters.Add(new SqlParameter("@Usage", SqlDbType.NVarChar));
                    cmd.Parameters["@Usage"].Value = langClass.Usage;

                    cmd.Parameters.Add(new SqlParameter("@Other", SqlDbType.NVarChar));
                    cmd.Parameters["@Other"].Value = langClass.Other;

                    cmd.Parameters.Add(new SqlParameter("@Overpass", SqlDbType.NVarChar));
                    cmd.Parameters["@Overpass"].Value = langClass.Overpass;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.NVarChar));
                    cmd.Parameters["@Port"].Value = langClass.Port;

                    cmd.Parameters.Add(new SqlParameter("@EnterIP", SqlDbType.NVarChar));
                    cmd.Parameters["@EnterIP"].Value = langClass.EnterIP;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage1", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage1"].Value = langClass.ErrorMessage1;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage2", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage2"].Value = langClass.ErrorMessage2;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage3", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage3"].Value = langClass.ErrorMessage3;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage4", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage4"].Value = langClass.ErrorMessage4;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage5", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage5"].Value = langClass.ErrorMessage5;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage6", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage6"].Value = langClass.ErrorMessage6;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage7", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage7"].Value = langClass.ErrorMessage7;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage8", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage8"].Value = langClass.ErrorMessage8;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage9", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage9"].Value = langClass.ErrorMessage9;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage10", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage10"].Value = langClass.ErrorMessage10;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage11", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage11"].Value = langClass.ErrorMessage11;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage12", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage12"].Value = langClass.ErrorMessage12;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage13", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage13"].Value = langClass.ErrorMessage13;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage14", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage14"].Value = langClass.ErrorMessage14;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage15", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage15"].Value = langClass.ErrorMessage15;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage16", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage16"].Value = langClass.ErrorMessage16;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage17", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage17"].Value = langClass.ErrorMessage17;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage18", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage18"].Value = langClass.ErrorMessage18;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage19", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage19"].Value = langClass.ErrorMessage19;

                    cmd.Parameters.Add(new SqlParameter("@ErrorMessage20", SqlDbType.NVarChar));
                    cmd.Parameters["@ErrorMessage20"].Value = langClass.ErrorMessage20;
                    
                    cmd.Parameters.Add(new SqlParameter("@HotAvailable", SqlDbType.NVarChar));
                    cmd.Parameters["@HotAvailable"].Value = langClass.HotAvailable;
                    
                    cmd.Parameters.Add(new SqlParameter("@GPSAvailable", SqlDbType.NVarChar));
                    cmd.Parameters["@GPSAvailable"].Value = langClass.GPSAvailable;

                    cmd.Parameters.Add(new SqlParameter("@A", SqlDbType.NVarChar));
                    cmd.Parameters["@A"].Value = langClass.A;

                    cmd.Parameters.Add(new SqlParameter("@B", SqlDbType.NVarChar));
                    cmd.Parameters["@B"].Value = langClass.B;

                    cmd.Parameters.Add(new SqlParameter("@C", SqlDbType.NVarChar));
                    cmd.Parameters["@C"].Value = langClass.C;

                    cmd.Parameters.Add(new SqlParameter("@D", SqlDbType.NVarChar));
                    cmd.Parameters["@D"].Value = langClass.D;
                    
                    cmd.Parameters.Add(new SqlParameter("@E", SqlDbType.NVarChar));
                    cmd.Parameters["@E"].Value = langClass.E;  
                  
                    cmd.Parameters.Add(new SqlParameter("@F", SqlDbType.NVarChar));
                    cmd.Parameters["@F"].Value = langClass.F;
                                        
                    cmd.Parameters.Add(new SqlParameter("@G", SqlDbType.NVarChar));
                    cmd.Parameters["@G"].Value = langClass.G;

                    cmd.Parameters.Add(new SqlParameter("@H", SqlDbType.NVarChar));
                    cmd.Parameters["@H"].Value = langClass.H;
                    
                    cmd.Parameters.Add(new SqlParameter("@I", SqlDbType.NVarChar));
                    cmd.Parameters["@I"].Value = langClass.I;
                    
                    cmd.Parameters.Add(new SqlParameter("@J", SqlDbType.NVarChar));
                    cmd.Parameters["@J"].Value = langClass.J;

                    cmd.Parameters.Add(new SqlParameter("@K", SqlDbType.NVarChar));
                    cmd.Parameters["@K"].Value = langClass.K;

                    cmd.Parameters.Add(new SqlParameter("@L", SqlDbType.NVarChar));
                    cmd.Parameters["@L"].Value = langClass.L;

                    cmd.Parameters.Add(new SqlParameter("@M", SqlDbType.NVarChar));
                    cmd.Parameters["@M"].Value = langClass.M;

                    cmd.Parameters.Add(new SqlParameter("@N", SqlDbType.NVarChar));
                    cmd.Parameters["@N"].Value = langClass.N;

                    cmd.Parameters.Add(new SqlParameter("@O", SqlDbType.NVarChar));
                    cmd.Parameters["@O"].Value = langClass.O;

                    cmd.Parameters.Add(new SqlParameter("@P", SqlDbType.NVarChar));
                    cmd.Parameters["@P"].Value = langClass.P;
                    
                    cmd.Parameters.Add(new SqlParameter("@Q", SqlDbType.NVarChar));
                    cmd.Parameters["@Q"].Value = langClass.Q;
                    
                    cmd.Parameters.Add(new SqlParameter("@R", SqlDbType.NVarChar));
                    cmd.Parameters["@R"].Value = langClass.R;
                    
                    cmd.Parameters.Add(new SqlParameter("@S", SqlDbType.NVarChar));
                    cmd.Parameters["@S"].Value = langClass.S;
                    
                    cmd.Parameters.Add(new SqlParameter("@T", SqlDbType.NVarChar));
                    cmd.Parameters["@T"].Value = langClass.T;
                    
                    cmd.Parameters.Add(new SqlParameter("@U", SqlDbType.NVarChar));
                    cmd.Parameters["@U"].Value = langClass.U;

                    cmd.Parameters.Add(new SqlParameter("@V", SqlDbType.NVarChar));
                    cmd.Parameters["@V"].Value = langClass.V;
                    
                    cmd.Parameters.Add(new SqlParameter("@W", SqlDbType.NVarChar));
                    cmd.Parameters["@W"].Value = langClass.W;
                    
                    cmd.Parameters.Add(new SqlParameter("@X", SqlDbType.NVarChar));
                    cmd.Parameters["@X"].Value = langClass.X;
                    
                    cmd.Parameters.Add(new SqlParameter("@Y", SqlDbType.NVarChar));
                    cmd.Parameters["@Y"].Value = langClass.Y;
                    
                    cmd.Parameters.Add(new SqlParameter("@Z", SqlDbType.NVarChar));
                    cmd.Parameters["@Z"].Value = langClass.Z;

                    cmd.Parameters.Add(new SqlParameter("@Period", SqlDbType.NVarChar));
                    cmd.Parameters["@Period"].Value = langClass.Period;

                    cmd.Parameters.Add(new SqlParameter("@ALL", SqlDbType.NVarChar));
                    cmd.Parameters["@ALL"].Value = langClass.ALL;
                    
                    cmd.Parameters.Add(new SqlParameter("@Dash", SqlDbType.NVarChar));
                    cmd.Parameters["@Dash"].Value = langClass.Dash;
                    
                    cmd.Parameters.Add(new SqlParameter("@PlaybackRequest", SqlDbType.NVarChar));
                    cmd.Parameters["@PlaybackRequest"].Value = langClass.PlaybackRequest;
                    
                    cmd.Parameters.Add(new SqlParameter("@MainA", SqlDbType.NVarChar));
                    cmd.Parameters["@MainA"].Value = langClass.MainA;
                    
                    cmd.Parameters.Add(new SqlParameter("@MainB", SqlDbType.NVarChar));
                    cmd.Parameters["@MainB"].Value = langClass.MainB;
                    //
                    cmd.Parameters.Add(new SqlParameter("@FineSource1", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource1"].Value = langClass.FineSource1;

                    cmd.Parameters.Add(new SqlParameter("@FineSource2", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource2"].Value = langClass.FineSource2;

                    cmd.Parameters.Add(new SqlParameter("@FineSource3", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource3"].Value = langClass.FineSource3;    
             
                    cmd.Parameters.Add(new SqlParameter("@FineSource4", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource4"].Value = langClass.FineSource4;

                    cmd.Parameters.Add(new SqlParameter("@FineSource5", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource5"].Value = langClass.FineSource5;
                    
                    cmd.Parameters.Add(new SqlParameter("@FineSource6", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource6"].Value = langClass.FineSource6;

                    cmd.Parameters.Add(new SqlParameter("@FineSource7", SqlDbType.NVarChar));
                    cmd.Parameters["@FineSource7"].Value = langClass.FineSource7;    
             
                    cmd.Parameters.Add(new SqlParameter("@FineCode1", SqlDbType.NVarChar));
                    cmd.Parameters["@FineCode1"].Value = langClass.FineCode1;

                    cmd.Parameters.Add(new SqlParameter("@FineCode2", SqlDbType.NVarChar));
                    cmd.Parameters["@FineCode2"].Value = langClass.FineCode2;
                    
                    cmd.Parameters.Add(new SqlParameter("@FineCode3", SqlDbType.NVarChar));
                    cmd.Parameters["@FineCode3"].Value = langClass.FineCode3;
            
                    cmd.Parameters.Add(new SqlParameter("@RED", SqlDbType.NVarChar));
                    cmd.Parameters["@RED"].Value = langClass.RED;

                    cmd.Parameters.Add(new SqlParameter("@ORANGE", SqlDbType.NVarChar));
                    cmd.Parameters["@ORANGE"].Value = langClass.ORANGE;
                                
                    cmd.Parameters.Add(new SqlParameter("@YELLOW", SqlDbType.NVarChar));
                    cmd.Parameters["@YELLOW"].Value = langClass.YELLOW;
                                
                    cmd.Parameters.Add(new SqlParameter("@WHITE", SqlDbType.NVarChar));
                    cmd.Parameters["@WHITE"].Value = langClass.WHITE;
                                
                    cmd.Parameters.Add(new SqlParameter("@BLACK", SqlDbType.NVarChar));
                    cmd.Parameters["@BLACK"].Value = langClass.BLACK;
                                
                    cmd.Parameters.Add(new SqlParameter("@BLUE", SqlDbType.NVarChar));
                    cmd.Parameters["@BLUE"].Value = langClass.BLUE;
                                
                    cmd.Parameters.Add(new SqlParameter("@CASTLE", SqlDbType.NVarChar));
                    cmd.Parameters["@CASTLE"].Value = langClass.CASTLE;
                                
                    cmd.Parameters.Add(new SqlParameter("@MOTORCYCLE", SqlDbType.NVarChar));
                    cmd.Parameters["@MOTORCYCLE"].Value = langClass.MOTORCYCLE;
                                                  
                    cmd.Parameters.Add(new SqlParameter("@LoginPageTB1", SqlDbType.NVarChar));
                    cmd.Parameters["@LoginPageTB1"].Value = langClass.LoginPageTB1;
                                
                    cmd.Parameters.Add(new SqlParameter("@LoginPageTB2", SqlDbType.NVarChar));
                    cmd.Parameters["@LoginPageTB2"].Value = langClass.LoginPageTB2;
                                
                    cmd.Parameters.Add(new SqlParameter("@LoginPageButton", SqlDbType.NVarChar));
                    cmd.Parameters["@LoginPageButton"].Value = langClass.LoginPageButton;
                                                                        
                    cmd.Parameters.Add(new SqlParameter("@LoginPageErrorMessage", SqlDbType.NVarChar));
                    cmd.Parameters["@LoginPageErrorMessage"].Value = langClass.LoginPageErrorMessage;
                                
                    cmd.Parameters.Add(new SqlParameter("@NewPageTB1", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB1"].Value = langClass.NewPageTB1;
                                                  
                    cmd.Parameters.Add(new SqlParameter("@NewPageTB2", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB2"].Value = langClass.NewPageTB2;
                                
                    cmd.Parameters.Add(new SqlParameter("@NewPageTB3", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB3"].Value = langClass.NewPageTB3;
                                
                    cmd.Parameters.Add(new SqlParameter("@NewPageTB4", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB4"].Value = langClass.NewPageTB4;
                                                    
                    cmd.Parameters.Add(new SqlParameter("@NewPageTB5", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageTB5"].Value = langClass.NewPageTB5;


                    cmd.Parameters.Add(new SqlParameter("@Sessions", SqlDbType.NVarChar));
                    cmd.Parameters["@Sessions"].Value = langClass.Sessions;

                    cmd.Parameters.Add(new SqlParameter("@Canvas", SqlDbType.NVarChar));
                    cmd.Parameters["@Canvas"].Value = langClass.Canvas;

                    cmd.Parameters.Add(new SqlParameter("@Notify", SqlDbType.NVarChar));
                    cmd.Parameters["@Notify"].Value = langClass.Notify;

                    cmd.Parameters.Add(new SqlParameter("@Video", SqlDbType.NVarChar));
                    cmd.Parameters["@Video"].Value = langClass.Video;

                    cmd.Parameters.Add(new SqlParameter("@Control", SqlDbType.NVarChar));
                    cmd.Parameters["@Control"].Value = langClass.Control;

                    cmd.Parameters.Add(new SqlParameter("@Draw", SqlDbType.NVarChar));
                    cmd.Parameters["@Draw"].Value = langClass.Draw;

                    cmd.Parameters.Add(new SqlParameter("@Select", SqlDbType.NVarChar));
                    cmd.Parameters["@Select"].Value = langClass.Select;

                    cmd.Parameters.Add(new SqlParameter("@Crop", SqlDbType.NVarChar));
                    cmd.Parameters["@Crop"].Value = langClass.Crop;

                    cmd.Parameters.Add(new SqlParameter("@Toggle", SqlDbType.NVarChar));
                    cmd.Parameters["@Toggle"].Value = langClass.Toggle;

                    cmd.Parameters.Add(new SqlParameter("@Clear", SqlDbType.NVarChar));
                    cmd.Parameters["@Clear"].Value = langClass.Clear;

                    cmd.Parameters.Add(new SqlParameter("@More", SqlDbType.NVarChar));
                    cmd.Parameters["@More"].Value = langClass.More;

                    cmd.Parameters.Add(new SqlParameter("@Less", SqlDbType.NVarChar));
                    cmd.Parameters["@Less"].Value = langClass.Less;

                    cmd.Parameters.Add(new SqlParameter("@Verifier", SqlDbType.NVarChar));
                    cmd.Parameters["@Verifier"].Value = langClass.Verifier;

                    cmd.Parameters.Add(new SqlParameter("@HotEvent", SqlDbType.NVarChar));
                    cmd.Parameters["@HotEvent"].Value = langClass.HotEvent;

                    cmd.Parameters.Add(new SqlParameter("@GPS", SqlDbType.NVarChar));
                    cmd.Parameters["@GPS"].Value = langClass.GPS;

                    cmd.Parameters.Add(new SqlParameter("@Reset", SqlDbType.NVarChar));
                    cmd.Parameters["@Reset"].Value = langClass.Reset;

                    cmd.Parameters.Add(new SqlParameter("@Library", SqlDbType.NVarChar));
                    cmd.Parameters["@Library"].Value = langClass.Library;

                    cmd.Parameters.Add(new SqlParameter("@Capture", SqlDbType.NVarChar));
                    cmd.Parameters["@Capture"].Value = langClass.Capture;

                    cmd.Parameters.Add(new SqlParameter("@Snap", SqlDbType.NVarChar));
                    cmd.Parameters["@Snap"].Value = langClass.Snap;

                    cmd.Parameters.Add(new SqlParameter("@Enroll", SqlDbType.NVarChar));
                    cmd.Parameters["@Enroll"].Value = langClass.Enroll;

                    cmd.Parameters.Add(new SqlParameter("@Verify", SqlDbType.NVarChar));
                    cmd.Parameters["@Verify"].Value = langClass.Verify;

                    cmd.Parameters.Add(new SqlParameter("@Reason", SqlDbType.NVarChar));
                    cmd.Parameters["@Reason"].Value = langClass.Reason;

                    cmd.Parameters.Add(new SqlParameter("@View", SqlDbType.NVarChar));
                    cmd.Parameters["@View"].Value = langClass.View;

                    cmd.Parameters.Add(new SqlParameter("@Details", SqlDbType.NVarChar));
                    cmd.Parameters["@Details"].Value = langClass.Details;

                    cmd.Parameters.Add(new SqlParameter("@Alarm", SqlDbType.NVarChar));
                    cmd.Parameters["@Alarm"].Value = langClass.Alarm;


                                                  
                    cmd.Parameters.Add(new SqlParameter("@NewPageErrorMessage1", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageErrorMessage1"].Value = langClass.NewPageErrorMessage1;
                                
                    cmd.Parameters.Add(new SqlParameter("@NewPageErrorMessage2", SqlDbType.NVarChar));
                    cmd.Parameters["@NewPageErrorMessage2"].Value = langClass.NewPageErrorMessage2;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = langClass.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = langClass.UpdatedDate;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateLanguage";

                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            langClass.Id = id;
                            var audit = new LanguageAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, langClass.UpdatedBy);
                            Reflection.CopyProperties(langClass, audit);
                            LanguageAudit.InsertLanguageAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateLanguage", ex, auditDbConnection);
                    }


                }
            }
            return true;//
        }

    }
}
