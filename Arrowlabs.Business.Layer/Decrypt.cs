﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Arrowlabs.Business.Layer
{
    /// <summary>
    /// Decrypt Class
    /// </summary>
    public class Decrypt
    {
        static TripleDESCryptoServiceProvider tripleDes = null;

        private static TripleDESCryptoServiceProvider TripleDesCrypto (string dbconnection)
        {
            
            {
                tripleDes  = new TripleDESCryptoServiceProvider();
                //tripleDes.Key = TruncateHash("ArrowlabsFZCLLCSecuritySolutions2013", tripleDes.KeySize / 8);
                tripleDes.Key = TruncateHash(Miscellaneous.FinalResult(dbconnection), tripleDes.KeySize / 8);
                tripleDes.IV = TruncateHash("", tripleDes.BlockSize / 8);
                return tripleDes;
            }
        }
        private static  byte[] TruncateHash(string key, int length)
        {
            SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
            // Hash the key.
            byte[] keyBytes = System.Text.Encoding.Unicode.GetBytes(key);
            byte[] hash = sha1.ComputeHash(keyBytes);

            // Truncate or pad the hash.
            Array.Resize(ref hash, length);
            return hash;
        }
        public static string DecryptData(string encryptedtext, bool useHashing,string dbconnection)
        {

            if (String.IsNullOrEmpty(encryptedtext))
                return string.Empty;

            // Convert the encrypted text string to a byte array.
            byte[] encryptedBytes = Convert.FromBase64String(encryptedtext);

            // Create the stream.
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            // Create the decoder to write to the stream.
            var tripleDesCrypto = TripleDesCrypto(dbconnection);
            CryptoStream decStream = new CryptoStream(ms, tripleDesCrypto.CreateDecryptor(), System.Security.Cryptography.CryptoStreamMode.Write);

            // Use the crypto stream to write the byte array to the stream.
            decStream.Write(encryptedBytes, 0, encryptedBytes.Length);
            decStream.FlushFinalBlock();

            // Convert the plaintext stream to a string.
            return System.Text.Encoding.Unicode.GetString(ms.ToArray());
        }       
        /// <summary>
        /// DeCrypt a string using dual encryption method. Return a DeCrypted clear string
        /// </summary>
        /// <param name="cipherString">encrypted string</param>
        /// <param name="useHashing">Did you use hashing to encrypt this data? pass true is yes</param>
        /// <returns></returns>
        //public static string DecryptData(string cipherString, bool useHashing)
        //{
        //    byte[] keyArray;
        //    byte[] toEncryptArray = Convert.FromBase64String(cipherString);

        //    string key = "ArrowlabsFZCLLCSecuritySolutions2013";

        //    if (useHashing)
        //    {
        //        MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
        //        keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
        //        hashmd5.Clear();
        //    }
        //    else
        //        keyArray = UTF8Encoding.UTF8.GetBytes(key);

        //    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //    tdes.Key = keyArray;
        //    tdes.Mode = CipherMode.ECB;
        //    tdes.Padding = PaddingMode.PKCS7;

        //    ICryptoTransform cTransform = tdes.CreateDecryptor();
        //    byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

        //    tdes.Clear();
        //    return UTF8Encoding.UTF8.GetString(resultArray);
        //}
    }
}
