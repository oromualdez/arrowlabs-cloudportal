﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class GroupItemFoundAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int ItemFoundId { get; set; }
        public string ItemGroupId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public static void InsertGroupItemFoundAudit(GroupItemFoundAudit dev, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(dev.BaseAudit, dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("InsertorUpdateGroupItemFoundAudit", connection);

                sqlCommand.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = dev.Id;

                sqlCommand.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                sqlCommand.Parameters["@ItemFoundId"].Value = dev.ItemFoundId;

                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerId"].Value = dev.CustomerId;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = dev.SiteId;

                sqlCommand.Parameters.Add(new SqlParameter("@ItemGroupId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ItemGroupId"].Value = dev.ItemGroupId;
                 
                sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@CreatedDate"].Value = dev.CreatedDate;
                 
                sqlCommand.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CreatedBy"].Value = dev.CreatedBy;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "InsertorUpdateGroupItemFoundAudit";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
        }
    }
}
