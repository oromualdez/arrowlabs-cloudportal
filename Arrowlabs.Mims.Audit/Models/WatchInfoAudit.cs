﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class WatchInfoAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }

                public int SiteId { get; set; }

                public int CustomerId { get; set; }

                     
        public string PinCode { get; set; }
        public string SecurityCode { get; set; }
        public string DeviceTocken { get; set; }
        public string UserName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Updatedby { get; set; }
        public DateTime? ExpiryDate { get; set; }

        public static bool InsertorUpdateWatchInfo(WatchInfoAudit watchInfo, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(watchInfo.BaseAudit, dbConnection);

            if (watchInfo != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertWatchInfoAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = watchInfo.Id;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = watchInfo.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = watchInfo.CustomerId;


                    cmd.Parameters.Add(new SqlParameter("@PinCode", SqlDbType.NVarChar));
                    cmd.Parameters["@PinCode"].Value = watchInfo.PinCode;

                    cmd.Parameters.Add(new SqlParameter("@SecurityCode", SqlDbType.NVarChar));
                    cmd.Parameters["@SecurityCode"].Value = watchInfo.SecurityCode;

                    cmd.Parameters.Add(new SqlParameter("@DeviceTocken", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceTocken"].Value = watchInfo.DeviceTocken;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = watchInfo.UserName;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = watchInfo.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = watchInfo.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.DateTime));
                    cmd.Parameters["@ExpiryDate"].Value = watchInfo.ExpiryDate;

                    cmd.Parameters.Add(new SqlParameter("@Updatedby", SqlDbType.NVarChar));
                    cmd.Parameters["@Updatedby"].Value = watchInfo.Updatedby;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = watchInfo.CreatedBy;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                }
                return true;
            }
            return false;
        }
    }
}

