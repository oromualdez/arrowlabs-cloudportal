﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Web.Services;
using System.Web.Configuration;
using System.Globalization;
using System.Text;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using System.Threading;
using ArrowLabs.Licence.Portal.Helpers;

namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class Lost : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static ClientLicence getClientLic;
        static List<string> VideoExtensions = new List<string> { ".AVI", ".MP4", ".MKV", ".FLV" };

        protected string senderName;
        protected string pendingPercent;
        protected string inprogressPercent;
        protected string completedPercent;
        protected string pendingLabel;
        protected string inprogressLabel;
        protected string completedLabel;
        protected string totalLabel;
        protected string ipaddress;
        protected string userinfoDisplay;
        protected string senderName2;
        protected string sourceLat;
        protected string sourceLon;
        protected string currentlocation;
        protected string landfview;
        protected string assetview;
        protected string keyview;
        protected string assetactive; 
        protected string keyactive;
        protected string senderName3;
        protected string siteName;

        protected string newitemPlaceholder = "New Item Received";
        protected string groupPlaceholder = "Group";
        protected string transferedfromsitePlaceholder = "Transfered From Site";
        protected string datetimefoundPlaceholder = "Date Time Found";
        protected string findernamePlaceholder = "Finder Name";
        protected string finderdepPlaceholder = "Finder Department";
        protected string itembrandPlaceholder = "Item Brand";
        protected string shelflifePlaceholder = "Shelf Life";
        protected string itemtypePlaceholder = "Item Type";
        protected string itemsubtypePlaceholder = "Item Sub Type";
        protected string locationfoundPlaceholder = "Location Found";
        protected string colorPlaceholder = "Color";
        protected string receivernamePlaceholder = "Receiver Name";
        protected string flightnumberPlaceholder = "Flight Number";
        protected string itemstoragePlaceholder = "Item Storage";
        protected string substoragePlaceholder = "Sub Storage";
        protected string itemimagePlaceholder = "Item Image";
        protected string itemreferencePlaceholder = "Item Reference";
        protected string clearPlaceholder = "Clear";
        protected string savePlaceholder = "Save";
        protected string generatebarcodePlaceholder = "Generate Barcode";
        protected string searchbarcodePlaceholder = "Search Barcode";
        protected string remarksPlaceholder = "Remarks";
        protected string rtlstyle = "";
        protected string rtlonly = "";
        protected string pleaseselectPlaceholder = "Please Select";

        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }


            userinfoDisplay = "block";
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            var manager = User.Identity.Name;
            senderName2 = Encrypt.EncryptData(User.Identity.Name, true, dbConnection);
            senderName = manager;
            var userinfo = Users.GetUserByName(manager, dbConnection);
            landfview = "style='display:none'";
            assetview = "style='display:none'";
            keyview = "style='display:none'";
            try
            {
                var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
                ipaddress = configtSettings.ChatServerUrl + "/signalr";

                if (userinfo.SiteId > 0)
                {
                    siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                }

                if (userinfo.SiteId == 130)
                {
                    newitemPlaceholder = "استلام جديد";
                    groupPlaceholder = "مجموعة";
                    transferedfromsitePlaceholder = "نقل من الموقع";
                    datetimefoundPlaceholder = "تاريخ و وقت العثور";
                    findernamePlaceholder = "اسم من وجد الشيء";
                    finderdepPlaceholder = "القسم الذي يعمل فيه";
                    itembrandPlaceholder = "العلامة التجارية للمفقود";
                    shelflifePlaceholder = "مدة التخزين";
                    itemtypePlaceholder = "نوع المفقود";
                    itemsubtypePlaceholder = "نوع المفقود / فرعي";
                    locationfoundPlaceholder = "موقع العثور";
                    colorPlaceholder = "اللون";
                    receivernamePlaceholder = "اسم المستلم";
                    flightnumberPlaceholder = "رقم الرحلة الجوية";
                    itemstoragePlaceholder = "موقع التخزين";
                    substoragePlaceholder = "تخزين / فرعي";
                    itemimagePlaceholder = "الصورة";
                    itemreferencePlaceholder = "رقم التعريف";
                    clearPlaceholder = "إلغاء";
                    savePlaceholder = "حفظ";
                    generatebarcodePlaceholder = "إصدار الباركود";
                    searchbarcodePlaceholder = "البحث عن الباركود";
                    remarksPlaceholder = "ملاحظات";
                    rtlstyle = "style='direction:rtl'";
                    rtlonly = "direction:rtl";
                    pleaseselectPlaceholder = "اختر من القائمة";
                }

                senderName3 = userinfo.CustomerUName;
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                
                if(userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    //landfview = "style='display:block'";
                    assetview = "style='display:block'";
                    keyview = "style='display:block'";
                }
                if(landfview != "style='display:block'" && assetview == "style='display:block'")
                {
                    assetactive = "active in";
                }
                else if(landfview != "style='display:block'" && assetview != "style='display:block'"  && keyview == "style='display:block'")
                {
                    keyactive = "active in";

                }
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    {
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.LostandFound)
                        {
                            landfview = "style='display:block'";
                        }
                        //if (mods.ModuleId == (int)Accounts.ModuleTypes.Warehouse)
                        //{
                        //    assetview = "style='display:block'";
                        //}
                        //if (mods.ModuleId == (int)Accounts.ModuleTypes.ServerKey)
                        //{
                        //    keyview = "style='display:block'";
                        //}
                    }
                    if (landfview != "style='display:block'" && assetview == "style='display:block'")
                    {
                        assetactive = "active in";
                    }
                    else if (landfview != "style='display:block'" && assetview != "style='display:block'" && keyview == "style='display:block'")
                    {
                        keyactive = "active in";

                    }
                    userinfoDisplay = "none";

                    if(userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        userinfoDisplay = "block";
                    if (userinfo.RoleId == (int)Role.Director)
                        userinfoDisplay = "block";
                    //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                    //if (pushDev != null)
                    //{
                    //    if (!string.IsNullOrEmpty(pushDev.Username))
                    //    {
                    //        pushDev.IsServerPortal = true;
                    //        pushDev.SiteId = userinfo.SiteId;
                    //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection, dbConnectionAudit, true);
                    //    }
                    //}
                    PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);
                }
                 
                var offenceTypes = new List<AssetCategory>();
                var fDeps = new List<FinderDepartment>();
                var colorsList = new List<VehicleColor>();  
                  
                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    offenceTypes = AssetCategory.GetAllAssetCategoryByCustomerId(userinfo.CustomerInfoId, dbConnection);
                    fDeps = FinderDepartment.GetAllFinderDepartmentByCId(userinfo.CustomerInfoId, dbConnection);
                    colorsList = VehicleColor.GetAllVehicleColorByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    offenceTypes = AssetCategory.GetAllAssetCategoryByLevel5(userinfo.ID, dbConnection);
                    fDeps = FinderDepartment.GetAllFinderDepartmentByLevel5(userinfo.ID, dbConnection);
                    colorsList = VehicleColor.GetAllVehicleColorByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    offenceTypes = AssetCategory.GetAllAssetCategory(dbConnection);
                    fDeps = FinderDepartment.GetAllFinderDepartment(dbConnection);
                    colorsList = VehicleColor.GetAllVehicleColor(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    offenceTypes = AssetCategory.GetAllAssetCategoryBySiteId(userinfo.SiteId, dbConnection);
                    fDeps = FinderDepartment.GetAllFinderDepartmentBySiteId(userinfo.SiteId, dbConnection);
                    colorsList = VehicleColor.GetAllVehicleColorBySiteId(userinfo.SiteId, dbConnection);
                } 

                offenceTypes = offenceTypes.OrderBy(i => i.Name).ToList();

                typeEditSelect.DataTextField = "Name";
                typeEditSelect.DataValueField = "Id";
                typeEditSelect.DataSource = offenceTypes;
                typeEditSelect.DataBind();


                categoryAssetSelect.DataTextField = "Name";
                categoryAssetSelect.DataValueField = "Id";
                categoryAssetSelect.DataSource = offenceTypes;
                categoryAssetSelect.DataBind();

                typeSelect.DataTextField = "Name";
                typeSelect.DataValueField = "Id";
                typeSelect.DataSource = offenceTypes;
                typeSelect.DataBind();
                 
                var selAsset = new AssetCategory();
                selAsset.Name = pleaseselectPlaceholder;

                var selAssetList = new List<AssetCategory>();
                selAssetList.Add(selAsset);
                selAssetList.AddRange(offenceTypes);

                itemTypeSelect.DataTextField = "Name";
                itemTypeSelect.DataValueField = "Name";
                itemTypeSelect.DataSource = selAssetList;
                itemTypeSelect.DataBind();

                searchItemTypeSelect.DataTextField = "Name";
                searchItemTypeSelect.DataValueField = "Name";
                searchItemTypeSelect.DataSource = selAssetList;
                searchItemTypeSelect.DataBind();


                enquiryTypeSelect.DataTextField = "Name";
                enquiryTypeSelect.DataValueField = "Name";
                enquiryTypeSelect.DataSource = selAssetList;
                enquiryTypeSelect.DataBind();

                var fdepAsset = new FinderDepartment();
                fdepAsset.Name = pleaseselectPlaceholder;

                var fdepAssetList = new List<FinderDepartment>();
                fdepAssetList.Add(fdepAsset);
                fdepAssetList.AddRange(fDeps);

                finderDepartmentSelect.DataTextField = "Name";
                finderDepartmentSelect.DataValueField = "Name";
                finderDepartmentSelect.DataSource = fdepAssetList;
                finderDepartmentSelect.DataBind();



 




                var selColor = new VehicleColor();
                selColor.ColorDesc = pleaseselectPlaceholder;
                var searchColorList = new List<VehicleColor>();
                searchColorList.Add(selColor);
                searchColorList.AddRange(colorsList);


                itemColourSelect.DataTextField = "COLORDESC";
                itemColourSelect.DataValueField = "COLORDESC";
                itemColourSelect.DataSource = searchColorList;
                itemColourSelect.DataBind();

                searchColorSelect.DataTextField = "COLORDESC";
                searchColorSelect.DataValueField = "COLORDESC";
                searchColorSelect.DataSource = searchColorList;
                searchColorSelect.DataBind();

                enquiryColourSelect.DataTextField = "COLORDESC";
                enquiryColourSelect.DataValueField = "COLORDESC";
                enquiryColourSelect.DataSource = searchColorList;
                enquiryColourSelect.DataBind();


                var sitesample = new Arrowlabs.Business.Layer.Site();
                sitesample.Name = pleaseselectPlaceholder;
                sitesample.Id = 0;
                var allsites = new List<Arrowlabs.Business.Layer.Site>();
                allsites.Add(sitesample);

                    //allsites.AddRange();
                    var sessions = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);

                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                        sessions = sessions.Where(i => i.CustomerInfoId == userinfo.CustomerInfoId).ToList();

                    allsites.AddRange(sessions);

                var locsList = Location.GetAllLocation(dbConnection);
                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin)
                {
                    allsites = allsites.Where(i => i.Id != userinfo.SiteId).ToList();
                    locsList = Location.GetAllLocationBySiteId(userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    locsList.Clear();
                   // var usites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var usite in usites)
                  //  {
                    locsList.AddRange(Location.GetAllLocationByLevel5(userinfo.ID, dbConnection));
                  //  }
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    locsList = Location.GetAllMainLocationByCustomerId(userinfo.CustomerInfoId, dbConnection);
                }


                var grouped = locsList.GroupBy(item => item.ID);
                locsList = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();
                locsList = locsList.OrderBy(i => i.LocationDesc).ToList();

                var selLoc = new Location();
                selLoc.LocationDesc = pleaseselectPlaceholder;

                var searchLocs = new List<Location>();
                searchLocs.Add(selLoc);
                searchLocs.AddRange(locsList);


                locationFoundSelect.DataTextField = "LOCATIONDESC";
                locationFoundSelect.DataValueField = "LOCATIONDESC";
                locationFoundSelect.DataSource = searchLocs;
                locationFoundSelect.DataBind();

                locationStoreSelect.DataTextField = "LOCATIONDESC";
                locationStoreSelect.DataValueField = "LOCATIONDESC";
                locationStoreSelect.DataSource = searchLocs;
                locationStoreSelect.DataBind();
                 
                searchLocationSelect.DataTextField = "LOCATIONDESC";
                searchLocationSelect.DataValueField = "LOCATIONDESC";
                searchLocationSelect.DataSource = searchLocs;
                searchLocationSelect.DataBind();

                searchLocationStoreSelect.DataTextField = "LOCATIONDESC";
                searchLocationStoreSelect.DataValueField = "LOCATIONDESC";
                searchLocationStoreSelect.DataSource = searchLocs;
                searchLocationStoreSelect.DataBind();

                enquiryLocationSelect.DataTextField = "LOCATIONDESC";
                enquiryLocationSelect.DataValueField = "LOCATIONDESC";
                enquiryLocationSelect.DataSource = searchLocs;
                enquiryLocationSelect.DataBind();
                
                categoryAssetMainLocation.DataTextField = "LOCATIONDESC";
                categoryAssetMainLocation.DataValueField = "LOCATIONDESC";
                categoryAssetMainLocation.DataSource = locsList;
                categoryAssetMainLocation.DataBind();





                transferSiteSelect.DataTextField = "Name";
                transferSiteSelect.DataValueField = "Id";
                transferSiteSelect.DataSource = allsites;
                transferSiteSelect.DataBind();

                transferSiteSelectFrom.DataTextField = "Name";
                transferSiteSelectFrom.DataValueField = "Id";
                transferSiteSelectFrom.DataSource = allsites;
                transferSiteSelectFrom.DataBind();
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "Page_Load", err, dbConnection, userinfo.SiteId);
            }

        }

        [WebMethod]
        public static string editAssetSub(int id, string name, string type, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }



                    var getOffence = Asset.GetAssetById(id, dbConnection);
                    if (getOffence != null)
                    {
                        if (getOffence.Name != name || getOffence.AssetCategoryId != Convert.ToInt32(type))
                        {
                            var oldValue = getOffence.Name;
                            var newValue = name;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", newValue, oldValue, userinfo, "Asset Sub Category change name-" + id);
                            oldValue = getOffence.AssetCategoryId.ToString();
                            newValue = type;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", newValue, oldValue, userinfo, "Asset Sub Category change type-" + id);
                            getOffence.Name = name;
                            getOffence.AssetCategoryId = Convert.ToInt32(type);
                            getOffence.UpdatedBy = userinfo.Username;
                            getOffence.UpdatedDate = CommonUtility.getDTNow();

                            if (userinfo.CustomerInfoId > 0)
                                getOffence.CustomerId = userinfo.CustomerInfoId;

                            Asset.InsertOrUpdateAsset(getOffence, dbConnection);
                        } 
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "editAssetSub", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }

                return listy;
            }
        }
        [WebMethod]
        public static string editAssetCat(int id, string name, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var off = AssetCategory.GetAssetCategoryById(id, dbConnection);
                    if (off != null)
                    {

                        if (off.Name.ToLower() != name.ToLower())
                        {
                            var getData2 = AssetCategory.GetAssetCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = AssetCategory.GetAssetCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                var oldValue = off.Name;
                                var newValue = name;
                                off.Name = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", newValue, oldValue, userinfo, "Asset Category change name-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        } 
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();

                        if (userinfo.CustomerInfoId > 0)
                            off.CustomerId = userinfo.CustomerInfoId;

                        AssetCategory.InsertorUpdateAssetCategory(off, dbConnection);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "editAssetCat", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string addAssetCat(int id, string name, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }
                     
                    var getData = AssetCategory.GetAssetCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = AssetCategory.GetAssetCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }   

                    if (getData == null)
                    {
                        var alist = new List<Asset>();
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = AssetCategory.GetAllAssetCategoryByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.Name.ToLower() == name.ToLower())
                                    {
                                        var gassets = Asset.GetAllAssetByAssetCategoryId(getcat.Id, dbConnection);
                                        alist.AddRange(gassets);
                                        AssetCategory.DeleteAssetCategorybyId(getcat.Id, dbConnection);
                                    }
                                }
                            }
                        }

                        var off = new AssetCategory();
                        off.Name = name;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CreatedBy = userinfo.ID;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.SiteId = userinfo.SiteId;

                        if (userinfo.CustomerInfoId > 0)
                            off.CustomerId = userinfo.CustomerInfoId;

                        var rid = AssetCategory.InsertorUpdateAssetCategory(off, dbConnection);
                        foreach (var item in alist)
                        {
                            item.AssetCategoryId = rid;
                            item.UpdatedBy = userinfo.Username;
                            item.UpdatedDate = CommonUtility.getDTNow();
                            Asset.InsertOrUpdateAsset(item, dbConnection);
                        }
                    }
                    else
                    {
                        return "Name already exists";
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "addAssetCat", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string addAssetSubCategory(int id, string name, string type, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                  //  var getAll = Asset.GetAllAsset(dbConnection);
                //    getAll = getAll.Where(i => i.Name == name && i.AssetCategoryId == Convert.ToInt32(type)).ToList();



                   // if (getAll.Count > 0)
                   // {
                   //     listy = "The asset name matches another entry under this category";
                  //  }
                   // else
                   // {
                        var off = new Asset();
                        off.Name = name;
                        off.AssetCategoryId = Convert.ToInt32(type);
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CreatedBy = userinfo.ID;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.LocationId = 1;
                        off.SiteId = userinfo.SiteId;

                        if (userinfo.CustomerInfoId > 0)
                            off.CustomerId = userinfo.CustomerInfoId;

                        Asset.InsertOrUpdateAsset(off, dbConnection);
                        listy = "SUCCESS";
                    //}
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "addAssetSubCategory", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static List<string> getAssetCatData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                 
                var allcustomEvents = new List<AssetCategory>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = AssetCategory.GetAllAssetCategoryByCustomerId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents = AssetCategory.GetAllAssetCategoryByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = AssetCategory.GetAllAssetCategory(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    allcustomEvents = AssetCategory.GetAllAssetCategoryBySiteId(userinfo.SiteId, dbConnection);
                }


                foreach (var item in allcustomEvents)
                {
                    var cuserinfo = Users.GetUserById(item.CreatedBy, dbConnection);

                    var action = "<a href='#' data-target='#EditOffenceCategoryModal'  data-toggle='modal' onclick='rowchoiceOffenceCategory(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;) '><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteOffenceCatModal'  data-toggle='modal' onclick='rowchoiceOffenceCategory(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != cuserinfo.Username)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.Name + "</td><td>" + item.CreatedDate.Value.AddHours(userinfo.TimeZone) + "</td><td>" + cuserinfo.CustomerUName + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getAssetCatData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAssetSubCatData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
 
                var allcustomEvents = new List<Asset>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = Asset.GetAllAssetByCustomerId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents = Asset.GetAllAssetByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = Asset.GetAllAsset(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    allcustomEvents = Asset.GetAllAssetBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in allcustomEvents)
                {
                    var cuserinfo = Users.GetUserById(item.CreatedBy, dbConnection);

                    var action = "<a href='#' data-target='#editOffenceModal'  data-toggle='modal' onclick='rowchoiceOffenceType(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;,&apos;" + item.AssetCategoryId + "&apos;) '><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteOffenceTypeModal'  data-toggle='modal' onclick='rowchoiceOffenceType(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;,&apos;" + item.AssetCategoryId + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != cuserinfo.Username)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.Name + "</td><td>" + item.AssetCategoryName + "</td><td>" + item.CreatedDate.Value.AddHours(userinfo.TimeZone) + "</td><td>" + cuserinfo.CustomerUName + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getAssetSubCatData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = ItemLost.GetItemLostById(id, dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.Name);
                    listy.Add(customData.Comments);
                    var assetCat = Asset.GetAssetById(customData.AssetCatId, dbConnection);
                    listy.Add(assetCat.AssetCategoryName + "-" + assetCat.Name);
                    if (customData.Lost == 2)
                    {
                        listy.Add("Transfered to Police");
                    }
                    else
                        listy.Add(customData.LocationDesc);

                    listy.Add("");
                    listy.Add(customData.CustomerUName);
                    var createdUser = Users.GetUserByName(customData.CreatedBy, dbConnection);
                    var sites = Arrowlabs.Business.Layer.Site.GetSiteById(createdUser.SiteId, dbConnection);
                    if(sites != null)
                    {
                        if (!string.IsNullOrEmpty(sites.Name))
                            listy.Add(sites.Name);
                        else
                            listy.Add("N/A");
                    }
                    else
                    {
                        listy.Add("N/A");
                    }

                    listy.Add(customData.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                    if (customData.Lost == 1)
                    {
                        var uUser = Users.GetUserByName(customData.UpdatedBy, dbConnection);
                        if (uUser != null)
                            listy.Add(uUser.CustomerUName);
                        else
                            listy.Add(customData.UpdatedBy);

                        listy.Add(customData.UpdatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                        var oUser = Users.GetUserByName(customData.OwnerName, dbConnection);
                        if (oUser != null)
                            listy.Add(oUser.CustomerUName);
                        else
                            listy.Add(customData.OwnerName);
                        
                    }
                    else
                    {
                        var uUser = Users.GetUserByName(customData.UpdatedBy, dbConnection);
                        if (uUser != null)
                            listy.Add(uUser.CustomerUName);
                        else
                            listy.Add(customData.UpdatedBy);

                        listy.Add(customData.UpdatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                        var oUser = Users.GetUserByName(customData.OwnerName, dbConnection);
                        if (oUser != null)
                            listy.Add(oUser.CustomerUName);
                        else
                            listy.Add(customData.OwnerName);
                    }
                    if (customData.Lost == 1)
                    {
                        listy.Add("block");
                        listy.Add("none");
                    }
                    else{
                        listy.Add("none");
                        listy.Add("block");
                    }
                    if (customData.isKey)
                        listy.Add("KEY");
                    else
                        listy.Add("NO");

                    if (customData.Lost == 1)
                    {
                        listy.Add("OUT");
                    }
                    else
                    {
                        listy.Add("IN");
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTableRowData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        
        [WebMethod]
        public static List<string> getChecklistData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);
                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence1))
                        listy.Add(offenceinfo.Offence1);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence2))
                        listy.Add(offenceinfo.Offence2);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence3))
                        listy.Add(offenceinfo.Offence3);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence4))
                        listy.Add(offenceinfo.Offence4);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getChecklistData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getSubLocations(string id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                if (userinfo.SiteId == 130)
                {
                    listy.Add("اختر من القائمة");
                }
                else
                {
                    listy.Add("Please Select");
                }
                if (!string.IsNullOrEmpty(id))
                {
                    var customData = Location.GetAllLocationByLocationDescAndCId(id,userinfo.CustomerInfoId ,dbConnection);
                    if (customData.Count > 0)
                    {
                        var grouped = customData.GroupBy(item => item.ID);
                        customData = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                        var getSubs = Location.GetAllSubLocationByMainId(customData[0].ID, dbConnection);
                        if (getSubs.Count > 0)
                        {
                            getSubs = getSubs.OrderBy(i => i.LocationDesc).ToList();
                            foreach(var Subs in getSubs)
                            {
                                listy.Add(Subs.LocationDesc);
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getSubLocations", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getSubCategories(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                //if (!string.IsNullOrEmpty(id))
                {
                    var customData = Asset.GetAllAsset(dbConnection);
                    if (customData.Count > 0)
                    {
                        customData = customData.Where(i => i.AssetCategoryId == id).ToList();
                        var grouped = customData.GroupBy(item => item.Id);
                        customData = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        customData = customData.OrderBy(i => i.Name).ToList();
                        foreach (var Subs in customData)
                        {
                            listy.Add(Subs.Name + "_" + Subs.Id);
                        }

                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getSubCategories", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getSubItem(string id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                //if (!string.IsNullOrEmpty(id))
                {

                    if (userinfo.SiteId == 130)
                    {
                        listy.Add("اختر من القائمة" + "_0"); 
                    }
                    else
                    {
                        listy.Add("Please Select_0");
                    }
                    var customData = Asset.GetAllAsset(dbConnection);
                    if (customData.Count > 0)
                    {

                        if (userinfo.RoleId != (int)Role.SuperAdmin)
                        {
                            customData = customData.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                        } 

                        customData = customData.Where(i => i.AssetCategoryName == id).ToList();
                        var grouped = customData.GroupBy(item => item.Id);
                        customData = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        customData = customData.OrderBy(i => i.Name).ToList();
                        foreach (var Subs in customData)
                        {
                            listy.Add(Subs.Name + "_" + Subs.Id);
                        }

                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getSubItem", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
         
        [WebMethod]
        public static string attachFileToItemCheckOut(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var cusEv = ItemLost.GetItemLostById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new ItemLostAttachment();
                    newObj.CreatedBy = userinfo.Username;
                    newObj.CreatedDate = CommonUtility.getDTNow();
                    var newimgPath = filepath.Replace('|', '\\');

                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);

                            newObj.AttachmentPath = savestring;
                            newObj.ItemLostId = cusEv.Id;
                            newObj.SiteId = cusEv.SiteId;
                            newObj.CustomerId = cusEv.CustomerId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            ItemLostAttachment.InsertOrUpdateItemLostAttachment(newObj, dbConnection);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToItemCheckOut", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static string attachFileToItem(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var cusEv = ItemFound.GetItemFoundById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new LostAndFoundAttachments();
                    newObj.CreatedBy = userinfo.Username;
                    newObj.CreatedDate = CommonUtility.getDTNow();
                    var newimgPath = filepath.Replace('|', '\\');

                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);

                            newObj.ImagePath = savestring;
                            newObj.ItemFoundId = cusEv.Id;
                            newObj.SiteId = cusEv.SiteId;
                            newObj.CustomerId = cusEv.CustomerId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj, dbConnection);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToItem", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }
        
        [WebMethod]
        public static string getIncidentLocationData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var cusEv = ItemLost.GetItemLostById(id, dbConnection);
                if (cusEv != null)
                {
                    json += "[";
                    json += "{ \"Username\" : \"" + cusEv.Name + "\",\"Id\" : \"" + cusEv.Id + "\",\"Long\" : \"" + cusEv.Longitude.ToString() + "\",\"Lat\" : \"" + cusEv.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getIncidentLocationData", err, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getAttachmentDataIcons(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var attachments = ItemLostAttachment.GetItemLostAttachmentByItemLostId(id, dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        //int index = item.AttachmentPath.IndexOf("Lost");
                        //if (index > 0)
                        //{

                        //}
                        //else
                        //{
                        //    index = item.AttachmentPath.IndexOf("ItemFound");
                        //}
                        var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");

                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                        {
                            var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                            listy += retstring;
                            i++;
                        }
                        else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                        {

                        }
                        else
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img src='" + requiredString + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                            listy += retstring;
                            i++;
                        }
                        
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentDataIcons", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTab(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Location</p></div></div></div>";

                var attachments = ItemLostAttachment.GetItemLostAttachmentByItemLostId(id, dbConnection);
                var i = 1;
                var iTab = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        //int index = item.AttachmentPath.IndexOf("Lost");
                        //if (index > 0)
                        //{

                        //}
                        //else
                        //{
                        //    index = item.AttachmentPath.IndexOf("ItemFound");
                        //}
                        var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");

                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                        {
                            var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div></div></div>";
                            listy += retstring;
                            iTab++;
                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;

                            var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentPath, i);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + requiredString + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + requiredString + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;
                           
                        }
                        else
                        {
                            var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentPath, i);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-file-image-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;
                            iTab++;
                        }
                        i++;
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentDataTab", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = ItemLostAttachment.GetItemLostAttachmentByItemLostId(id, dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        //int index = item.AttachmentPath.IndexOf("Lost");
                        //if (index > 0)
                        //{

                        //}
                        //else
                        //{
                        //    index = item.AttachmentPath.IndexOf("ItemFound");
                        //}
                        var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");
                        //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                        {
                            //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                            var retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='"+ requiredString + "' /></video>";
                            listy.Add(retstring);
                            i++;
                        }
                        else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                        {

                        }
                        else
                        {

                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img onclick='rotateMe(this);' src='" + requiredString + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstring);
                            i++;
                        }
                        
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> handleTicket(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var cusData = CustomEvent.GetCustomEventById(id, dbConnection);
                cusData.Handled = true;
                cusData.HandledBy = userinfo.Username;
                cusData.HandledTime = CommonUtility.getDTNow();
                cusData.UpdatedBy = userinfo.Username;
                cusData.IncidentStatus = CommonUtility.getIncidentStatusValue("resolve");
                CustomEvent.InsertCustomEvent(cusData, dbConnection, dbConnectionAudit, true);
                CustomEvent.CustomEventHandledById(cusData.EventId, true, userinfo.Username, dbConnection);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "handleTicket", ex, dbConnection, userinfo.SiteId);
                listy.Add("FAIL");
            }
            listy.Add("SUCCESS");
            return listy;
        }
        //User Profile
        [WebMethod]
        public static string changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return 0; 
                }

                if (userinfo.ID > 0)
                {
                    var getuser = userinfo;
                    getuser.FirstName = firstname;
                    getuser.LastName = lastname;
                    getuser.Email = emailaddress;

                    if (getuser.RoleId != role)
                    {
                        getuser.RoleId = role;
                        if (role == (int)Role.Manager)
                        {
                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                        else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                        }
                        else
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                    }
                    if (getuser.RoleId == (int)Role.Manager)
                    {

                        var dirUser = Users.GetUserById(supervisor, dbConnection);
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        if (dirUser != null)
                        {
                            if (!string.IsNullOrEmpty(dirUser.Username))
                            {
                                List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                            
                                            }};
                                DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                            }
                        }
                    }
                    else if (getuser.RoleId == (int)Role.Operator)
                    {
                        if (supervisor > 0)
                        {
                            var manUser = Users.GetUserById(supervisor, dbConnection);
                            List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                            UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                    else if (getuser.RoleId == (int)Role.UnassignedOperator)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                    {
                        return userinfo.ID;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "addUserProfile", err, dbConnection, userinfo.SiteId);
            }
            return userinfo.ID;
        }

        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add("Online");//customData.Status);
                    listy.Add("circle-point circle-point-green") ;//+ CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                    fontstyle = "style='color:lime;'";
                  //  var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    //if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                    if (customData.RoleId != (int)Role.SuperAdmin)
                    {
                     //   if (pushDev != null)
                   //     {
                      //      if (!string.IsNullOrEmpty(pushDev.Username))
                      //      {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                          //  }
                         //   else
                         //   {
                         //      monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                           //     fontstyle = "";
                           // }
                        //}
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static List<string> getReminderSelectorDates(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                // Set the start time (00:00 means 12:00 AM)
                DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(1, 0, 0));
                // Set the end time (23:55 means 11:55 PM)
                DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(24, 0, 0));
                //Set 5 minutes interval
                TimeSpan Interval = new TimeSpan(1, 0, 0);
                //To set 1 hour interval
                //TimeSpan Interval = new TimeSpan(1, 0, 0);           

                while (StartTime <= EndTime)
                {
                    listy.Add(StartTime.ToString("HH:mm"));
                    StartTime = StartTime.Add(Interval);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getReminderSelectorDates", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTicketingData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemLost>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    allcustomEvents = ItemLost.GetAllItemLost(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    allcustomEvents = ItemLost.GetAllItemLostBySite(userinfo.SiteId, dbConnection);
                    //allcustomEvents = allcustomEvents.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByCustomerId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var site in sites)
                   // {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByLevel5(userinfo.ID, dbConnection));
                    //}
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostBySite(userinfo.SiteId, dbConnection));
                }

                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                allcustomEvents = allcustomEvents.Where(i => i.isKey == false).ToList();

                foreach (var item in allcustomEvents)
                {

                    var loststate = "Checked In";
                    var location = item.LocationDesc;
                    if (item.Lost == 1)
                        loststate = "Checked Out";
                    else if (item.Lost == 2)
                    {
                        loststate = "Transfered";
                        location = "Transfered to Police";
                    }

                    var action = string.Empty;
                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        action = "<a href='#' data-target='#deletelostItem'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td >" + item.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td>" + location + "</td><td>" + loststate + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);

                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTicketingData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getKeysData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemLost>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    allcustomEvents = ItemLost.GetAllItemLost(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    allcustomEvents = ItemLost.GetAllItemLostBySite(userinfo.SiteId, dbConnection);
                    //allcustomEvents = allcustomEvents.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByCustomerId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                  //  foreach (var site in sites)
                   // {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByLevel5(userinfo.ID, dbConnection));
                  //  }
                }

                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                allcustomEvents = allcustomEvents.Where(i => i.isKey == true).ToList();

                foreach (var item in allcustomEvents)
                {

                    var loststate = "Checked In";
                    var location = item.LocationDesc;
                    if (item.Lost == 1)
                        loststate = "Checked Out";
                    else if (item.Lost == 2)
                    {
                        loststate = "Transfered";
                        location = "Transfered to Police";
                    }

                    var action = string.Empty;
                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        action = "<a href='#' data-target='#deletelostItem'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td >" + item.CreatedDate.ToString() + "</td><td>" + item.CustomerUName + "</td><td>" + location + "</td><td>" + loststate + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);

                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getKeysData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getFoundItemData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemFound>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = ItemFound.GetAllItemFound(dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager)
                    allcustomEvents = ItemFound.GetItemFoundBySiteId(userinfo.SiteId, dbConnection);
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                   // foreach (var site in sites)
                   // {
                    allcustomEvents = ItemFound.GetItemFoundByLevel5(userinfo.ID, dbConnection);
                    //}
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = ItemFound.GetItemFoundByCId(userinfo.CustomerInfoId, dbConnection);
                }
                var grouped = allcustomEvents.GroupBy(item => item.Id);

                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();


                
                foreach (var item in allcustomEvents)
                {

                    var action = string.Empty;

                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        action = "<a href='#' data-target='#deletefoundItem'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Reference + "</td><td >" + item.CreatedDate.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.Brand + "</td><td>" + item.LocationFound + "</td><td>" + item.StorageLocation + "</td><td>" + item.Status + "</td><td><a href='#' data-target='#foundItemViewCard'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getFoundItemData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getEnquiryData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemInquiry>();
                if(userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = ItemInquiry.GetAllItemInquiry(dbConnection);
                    
                }
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager)
                    allcustomEvents = ItemInquiry.GetItemInquiryBySiteId(userinfo.SiteId, dbConnection);
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    //{
                    allcustomEvents = ItemInquiry.GetItemInquiryByLevel5(userinfo.ID, dbConnection);
                    //}
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = ItemInquiry.GetItemInquiryByCId(userinfo.CustomerInfoId, dbConnection);
                }
                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                
                //var usersList = CommonUtility.getUsersOfManagerDirector(userinfo);
                foreach (var item in allcustomEvents)
                {
                    var action = string.Empty;

                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        action = "<a href='#' data-target='#deleteinquiryItem'  data-toggle='modal' onclick='rowchoiceEnquiry(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.ReferenceNo + "</td><td>" + item.Name + "</td><td >" + item.CreatedDate.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#itemEnquiryModal'  data-toggle='modal' onclick='rowchoiceEnquiry(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                        listy.Add(returnstring);
                    //}
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getEnquiryData", err, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }
        
        [WebMethod]
        public static string getAttachmentDataTabFound(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                //listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Incident Location</p></div></div></div>";

                var attachments = ItemFound.GetItemFoundById(id, dbConnection);
                var getExtraAttachments = new List<LostAndFoundAttachments>();
                //if (attachments.Status != "Found")
                //{
                    getExtraAttachments = LostAndFoundAttachments.GetAttachmentsByItemFoundId(attachments.Id, dbConnection);
                //}
                var i = 1;
                if (attachments.Status == "Found")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Item Image</p></div></div></div>";
                        listy += retstring;
                        i++;
                    }
                }
                else if (attachments.Status == "Return")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Item Image</p></div></div></div>";
                        listy += retstring;
                        i++;
                    }
                    var returnData = ItemOwner.GetItemOwnerByReference(attachments.Reference, dbConnection);
                    if (returnData != null)
                    {
                        if (!string.IsNullOrEmpty(returnData.IdCopy))
                        {
                            var retstring2 = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Return Image</p></div></div></div>";
                            listy += retstring2;
                            i++;
                        }
                        if (!string.IsNullOrEmpty(returnData.Signature))
                        {
                            var retstring2 = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Signature</p></div></div></div>";
                            listy += retstring2;
                            i++;
                        }
                    }
                }
                else if (attachments.Status == "Dispose" || attachments.Status == "Donate")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Item Image</p></div></div></div>";
                        listy += retstring;
                        i++;
                    }
                    var returnData = ItemDispose.GetItemDisposeByReference(attachments.Reference, dbConnection);
                    if (returnData != null)
                    {
                        if (!string.IsNullOrEmpty(returnData.ImagePath))
                        {
                            var retstring2 = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Dispose Image</p></div></div></div>";
                            listy += retstring2;
                            i++;
                        }
                    }
                }
                var extraI=1;
                foreach (var attach in getExtraAttachments)
                {
                    //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    if (!string.IsNullOrEmpty(attach.ImagePath))
                    {
                        if (System.IO.Path.GetExtension(attach.ImagePath).ToUpperInvariant() == ".PDF")
                        {
                            //int index = attach.ImagePath.IndexOf("ItemFound");
                            //var requiredString = attach.ImagePath.Substring(index, (attach.ImagePath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;

                            var attachmentName = CommonUtility.getAttachmentDisplayName(attach.ImagePath, extraI);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + attach.ImagePath + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + attach.ImagePath + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoice(&apos;" + attach.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;
                            extraI++;
                        }
                        else
                        {

                            var attachmentName = CommonUtility.getAttachmentDisplayName(attach.ImagePath, extraI);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-file-image-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoice(&apos;" + attach.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;
                            i++;
                            extraI++;
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentDataTabFound", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentDataFound(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = ItemFound.GetItemFoundById(id, dbConnection);
                var getExtraAttachments = new List<LostAndFoundAttachments>();

                    getExtraAttachments = LostAndFoundAttachments.GetAttachmentsByItemFoundId(attachments.Id, dbConnection);
                
                var i = 1;
                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                if (attachments.Status == "Found")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        //int index = attachments.ImagePath.IndexOf("ItemFound");
                       // var requiredString = attachments.ImagePath.Substring(index, (attachments.ImagePath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");
                        //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                        var retstring = "<img onclick='rotateMe(this);' src='" + attachments.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                        listy.Add(retstring);
                    }
                }
                else if (attachments.Status == "Return")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        //int index = attachments.ImagePath.IndexOf("ItemFound");
                        //var requiredString = attachments.ImagePath.Substring(index, (attachments.ImagePath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");
                        //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                        var retstring = "<img onclick='rotateMe(this);' src='" + attachments.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                        listy.Add(retstring);
                    }
                    var returnData = ItemOwner.GetItemOwnerByReference(attachments.Reference, dbConnection);
                    if (returnData != null)
                    {
                        if (!string.IsNullOrEmpty(returnData.IdCopy))
                        {
                            //int index = returnData.IdCopy.IndexOf("ItemFound");
                            //var requiredString = returnData.IdCopy.Substring(index, (returnData.IdCopy.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img onclick='rotateMe(this);' src='" + returnData.IdCopy + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstring);
                        }
                        if (!string.IsNullOrEmpty(returnData.Signature))
                        {
                            //int index = returnData.Signature.IndexOf("ItemFound");
                            //var requiredString = returnData.Signature.Substring(index, (returnData.Signature.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img onclick='rotateMe(this);' src='" + returnData.Signature + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstring);
                        }
                    }
                }
                else if (attachments.Status == "Dispose" || attachments.Status == "Donate")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        //int index = attachments.ImagePath.IndexOf("ItemFound");
                        //var requiredString = attachments.ImagePath.Substring(index, (attachments.ImagePath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");
                        //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                        var retstring = "<img onclick='rotateMe(this);' src='" + attachments.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                        listy.Add(retstring);
                    }
                    var returnData = ItemDispose.GetItemDisposeByReference(attachments.Reference, dbConnection);
                    if (returnData != null)
                    {
                        if (!string.IsNullOrEmpty(returnData.ImagePath))
                        {
                            //int index = returnData.ImagePath.IndexOf("ItemFound");
                            //var requiredString = returnData.ImagePath.Substring(index, (returnData.ImagePath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img onclick='rotateMe(this);' src='" + returnData.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstring);
                        }
                    }
                }
                foreach (var attach in getExtraAttachments)
                {
                    if (!string.IsNullOrEmpty(attach.ImagePath))
                    {
                        if (System.IO.Path.GetExtension(attach.ImagePath).ToUpperInvariant() != ".PDF")
                        {
                            //int index = attach.ImagePath.IndexOf("ItemFound");
                            //var requiredString = attach.ImagePath.Substring(index, (attach.ImagePath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstringExtra = "<img onclick='rotateMe(this);' src='" + attach.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstringExtra);
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> saveFoundTimeData(string founddate, string receivedate, string foundT, string receiveT, string itemBrnd, string itemType, string recName, string finderName, string roomNumber
            , string itemColor, string itemStorage, string itemReference, string imgPath, string finderDepartment, string itemStatus
            , string locFound, string siteTransfer, string itemSubStorage, string itemSubType, string shelfLife
            , string uname,string fremarks)
        {
            var listy = new List<string>();
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var itemData = ItemFound.GetItemFoundByReference(itemReference, dbConnection);
                if (itemData == null)
                {

                    var customData = new ItemFound();
                    customData.Brand = itemBrnd;
                    if (!itemColor.ToLower().Contains("please select") && !itemColor.Contains("اختر من القائمة"))
                        customData.Colour = itemColor;
                    customData.CreatedBy = userinfo.Username;
                    customData.CreatedDate = CommonUtility.getDTNow();
                    var dtFound = DateTime.Parse(founddate + "," + foundT);
                    customData.DateFound = dtFound;

                    if (!finderDepartment.ToLower().Contains("please select") && !finderDepartment.Contains("اختر من القائمة"))
                        customData.FinderDepartment = finderDepartment;

                    customData.FinderName = finderName;
                    var newimgPath = imgPath.Replace('|', '\\');
                    customData.ImagePath = newimgPath;

                    if (!locFound.ToLower().Contains("please select") && !locFound.Contains("اختر من القائمة"))
                        customData.LocationFound = locFound;
                   
                    //var dtReceive = DateTime.Parse(receivedate + "," + receiveT);
                    customData.ReceiveDate = CommonUtility.getDTNow().ToString();//dtReceive.ToString();
                    customData.ReceiverName = recName;
                    customData.Reference = itemReference;
                    customData.RoomNumber = roomNumber;
                    customData.SiteId = userinfo.SiteId;
                    customData.Status = "Found";

                    if (!itemStorage.ToLower().Contains("please select") && !itemStorage.Contains("اختر من القائمة"))
                        customData.StorageLocation = itemStorage;

                    customData.Type = itemType;

                    if (!shelfLife.ToLower().Contains("please select") && !shelfLife.Contains("اختر من القائمة"))
                        customData.ShelfLife = shelfLife;

                    if (!itemSubStorage.ToLower().Contains("please select") && !itemSubStorage.Contains("اختر من القائمة"))
                        customData.SubStorageLocation = itemSubStorage;

                    if (!itemSubType.ToLower().Contains("please select") && !itemSubType.Contains("اختر من القائمة"))
                        customData.SubType = itemSubType;

                    customData.Remarks = fremarks;
                    customData.UpdatedBy = userinfo.Username;
                    customData.UpdatedDate = CommonUtility.getDTNow();
                    customData.TransferFromSiteId = Convert.ToInt32(siteTransfer);
                    customData.CustomerId = userinfo.CustomerInfoId;
                    var retId = ItemFound.InsertorUpdateItemFound(customData, dbConnection, dbConnectionAudit, true);
                    if (retId > 0)
                    {
                        retVal = "SUCCESS";
                        listy.Clear();
                        listy.Add(retVal);
                        listy.Add(retId.ToString());
                        listy.Add(itemBrnd);
                    }
                    else
                    {
                        retVal = "Error occured while trying to add item to system.";
                        listy.Clear();
                        listy.Add(retVal);
                    }
                }
                else
                {
                    retVal = "Item reference already in the system,generate new barcode.";
                    listy.Clear();
                    listy.Add(retVal);
                }

            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("saveFoundTimeData", "Lost.aspx", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add(retVal);
                retVal = ex.Message;
            }
            
            return listy;
        }

        [WebMethod]
        public static string saveReturnTimeData(string ownerNationality, string ownerNumber, string ownerName, string ownerAddress
            , string imgPath, string ownerTypeSelect, string ownerContact, string shipTracking, string ownerEmail
            , string ownerRoomNumber, string uname, string itemreference, string itemid, int eId, string siteTransfer)
        {
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var customData = new ItemOwner();
                customData.Address = ownerAddress;
                customData.Email = ownerEmail;
                customData.HandedBy = userinfo.Username;
                customData.MobileNo = ownerContact;
                customData.IdNumber = ownerNumber;
                customData.Name = ownerName;
                customData.Nationality = ownerNationality;
                customData.ReturnDate = CommonUtility.getDTNow();
                customData.RoomNumber = ownerRoomNumber;
                customData.ShippingNumber = shipTracking;
                customData.SiteId = userinfo.SiteId;
                customData.Type = ownerTypeSelect;

                if(!string.IsNullOrEmpty(siteTransfer))
                {
                    var site = Convert.ToInt32(siteTransfer);
                    if (site > 0)
                        customData.TransferSiteId = site;
                }

                customData.ItemFoundId = Convert.ToInt32(itemid);
                customData.ItemReference = itemreference;
                var newimgPath = imgPath.Replace('|', '\\');
                customData.IdCopy = newimgPath;

                customData.ImagePath = newimgPath;
                customData.UpdatedBy = userinfo.Username;
                customData.UpdatedDate = CommonUtility.getDTNow();
                customData.CreatedBy = userinfo.Username;
                customData.CreatedDate = CommonUtility.getDTNow();
                customData.CustomerId = userinfo.CustomerInfoId;



                var retId = ItemOwner.InsertorUpdateItemOwner(customData, dbConnection, dbConnectionAudit, true);
                if (retId > 0)
                {
                    var getFoundItem = ItemFound.GetItemFoundById(Convert.ToInt32(itemid), dbConnection);
                    getFoundItem.Status = "Return";
                    getFoundItem.UpdatedBy = userinfo.Username;
                    getFoundItem.UpdatedDate = CommonUtility.getDTNow();
                    ItemFound.InsertorUpdateItemFound(getFoundItem, dbConnection, dbConnectionAudit, true);

                    if (eId > 0)
                    {
                        var itemEnq = ItemInquiry.GetItemInquiryById(eId, dbConnection);
                        itemEnq.UpdatedBy = userinfo.Username;
                        itemEnq.UpdatedDate = CommonUtility.getDTNow();
                        itemEnq.ItemReference = itemreference;
                        itemEnq.ItemFoundId = Convert.ToInt32(itemid);
                        ItemInquiry.InsertorUpdateItemInquiry(itemEnq, dbConnection, dbConnectionAudit, true);
                    }

                    retVal = "SUCCESS";
                }
                else
                    retVal = "Error occured while trying to add item to system.";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("saveReturnTimeData", "Lost.aspx", ex, dbConnection, userinfo.SiteId);
                retVal = ex.Message;
            }
            return retVal;
        }

        [WebMethod]
        public static string saveDisposeItemData(string disposeSelect, string disposeName, string disposeCompany, string disposeLocation
            , string disposeContact, string disposeIDNumber, string sop, string disposeAfter, string disposeWitness
            , string imgPath, string uname, string itemreference, string itemid, int eId)
        {
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var customData = new ItemDispose();
                
                customData.DisposedAfter = disposeAfter;
                customData.DisposedBy = userinfo.Username;
                customData.DisposedDate = CommonUtility.getDTNow();
                customData.DisposedTo = disposeSelect;

                if (disposeSelect == "Destroyed")
                {
                    customData.Destroyed = sop;
                }
                if (imgPath != "undefined")
                {
                    var newimgPath = imgPath.Replace('|', '\\');
                    customData.ImagePath = newimgPath;
                }
                customData.ItemFoundId = Convert.ToInt32(itemid);
                customData.ItemReference = itemreference;
                customData.RecipientCompany = disposeCompany;
                customData.RecipientContactNo = disposeContact;
                customData.RecipientIdNo = disposeIDNumber;
                customData.RecipientLocation = disposeLocation;
                customData.RecipientName = disposeName;
                customData.Witness = disposeWitness;
                customData.SiteId = userinfo.SiteId;
                customData.UpdatedBy = userinfo.Username;
                customData.UpdatedDate = CommonUtility.getDTNow();
                customData.CreatedBy = userinfo.Username;
                customData.CreatedDate = CommonUtility.getDTNow();
                customData.CustomerId = userinfo.CustomerInfoId;


                var retId = ItemDispose.InsertorUpdateItemDispose(customData, dbConnection, dbConnectionAudit, true);
                if (retId > 0)
                {
                    var getFoundItem = ItemFound.GetItemFoundById(Convert.ToInt32(itemid), dbConnection);

                    if (disposeSelect == "Charity")
                    {
                        getFoundItem.Status = "Donate";
                    }
                    else
                        getFoundItem.Status = "Dispose";

                    getFoundItem.UpdatedBy = userinfo.Username;
                    getFoundItem.UpdatedDate = CommonUtility.getDTNow();
                    ItemFound.InsertorUpdateItemFound(getFoundItem, dbConnection, dbConnectionAudit, true);
                    if (eId > 0)
                    {
                        var itemEnq = ItemInquiry.GetItemInquiryById(eId, dbConnection);
                        itemEnq.UpdatedBy = userinfo.Username;
                        itemEnq.UpdatedDate = CommonUtility.getDTNow();
                        itemEnq.ItemReference = itemreference;
                        itemEnq.ItemFoundId = Convert.ToInt32(itemid);
                        ItemInquiry.InsertorUpdateItemInquiry(itemEnq, dbConnection, dbConnectionAudit, true);
                    }
                    retVal = "SUCCESS";
                }
                else
                    retVal = "Error occured while trying to add item to system.";


            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("saveReturnTimeData", "Lost.aspx", ex, dbConnection, userinfo.SiteId);
                retVal = ex.Message;
            }
            return retVal;
        }


        [WebMethod]
        public static string saveMultipleItemsGroup(string[] userIds, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var guid = Guid.NewGuid().ToString();
                    foreach (var ids in userIds)
                    {
                        var grpItem = new GroupItemFound();
                        grpItem.ItemFoundId = Convert.ToInt32(ids);
                        grpItem.ItemGroupId = guid;
                        grpItem.CreatedBy = userinfo.Username;
                        grpItem.CreatedDate = CommonUtility.getDTNow();
                        GroupItemFound.InsertorUpdateGroupItemFound(grpItem, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("LOST.aspx", "saveMultipleItemsGroup", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
            }
            else
            {
                return "Problem occured while trying to submit form";
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static List<string> getGroupItemsData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var itemGroups = ItemFound.GetAllItemFoundGroupByUserId(id, dbConnection);
                if (itemGroups.Count > 0)
                {
                    foreach(var item in itemGroups){
                        if (item.Id != id)
                            listy.Add(item.Id + "|" + item.Brand);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Lost", "getGroupItemsData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> saveItemEnquiryData(string itemBrnd, string locFound, string itemName, string itemColor
            , string itemType, string itemDesc, string itemAddress, string itemContact, string itemRoom, string subtype
            , string uname)
        {
            var listy = new List<string>();
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = new ItemInquiry();

                customData.Address = itemAddress;
                customData.Closed = false;
                customData.ContactNo = itemContact;
                customData.DateFound = CommonUtility.getDTNow();
                customData.Delivered = false;
                customData.DeliveredDate = CommonUtility.getDTNow();
                customData.Found = false;
                customData.ItemBrand = itemBrnd;
                customData.ItemColour = itemColor;
                customData.ItemDescription = itemDesc;
                customData.LocationLost = locFound;
                customData.Name = itemName;
                customData.RoomNumber = itemRoom;
                customData.Type = itemType;
                customData.SecurityTracking = userinfo.Username;
                customData.SiteId = userinfo.SiteId;
                customData.UpdatedBy = userinfo.Username;
                customData.UpdatedDate = CommonUtility.getDTNow();
                customData.CreatedBy = userinfo.Username;
                customData.CreatedDate = CommonUtility.getDTNow();
                if (!string.IsNullOrEmpty(subtype))
                {
                    if (!subtype.ToLower().Contains("please select") || !subtype.ToLower().Contains("اختر من القائمة"))
                    {
                        customData.SubType = subtype;
                    }
                }
                customData.CustomerId = userinfo.CustomerInfoId;
                var lastid = ItemInquiry.GetLastItemInquiryIdBySiteId(new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1), CommonUtility.getDTNow().AddHours(24), userinfo.SiteId, dbConnection);
                var getmonth = CommonUtility.getDTNow().Month;
                int getyear = CommonUtility.getDTNow().Year % 20;
                var retmonth = getmonth.ToString();
                if (getmonth < 10)
                    retmonth = "0" + getmonth.ToString();

                var retsiteid = userinfo.SiteId.ToString();
                if (userinfo.SiteId < 10)
                    retsiteid = "0" + userinfo.SiteId;
                var retstring = string.Empty;
                while (true)
                {
                    lastid = lastid + 1;
                    var retCount = (lastid).ToString();
                    var itemCount = (lastid);
                    if (itemCount.ToString().Length == 3)
                        retCount = "0" + itemCount.ToString();
                    else if (itemCount.ToString().Length == 2)
                        retCount = "00" + itemCount.ToString();
                    else if (itemCount.ToString().Length == 1)
                        retCount = "000" + itemCount.ToString();

                    retstring = "E" + retsiteid + retCount + retmonth + getyear.ToString();

                    var fItem = ItemInquiry.GetItemInquiryByReferenceNo(retstring, dbConnection);
                    if (fItem == null)
                    {
                        customData.ReferenceNo = retstring;
                        break;
                    }
                }
                var retId = ItemInquiry.InsertorUpdateItemInquiry(customData, dbConnection, dbConnectionAudit, true);
                if (retId > 0)
                {
                    retVal = retId.ToString();
                    listy.Add(retVal);
                    listy.Add(retstring);
                }
                else
                    listy.Clear();
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("saveReturnTimeData", "Lost.aspx", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowDataFound(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = ItemFound.GetItemFoundById(id, dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.Status.ToUpper() + " ITEM");
                    listy.Add(customData.FinderName);
                    listy.Add(customData.LocationFound);
                    listy.Add(customData.DateFound.ToString());
                    listy.Add(customData.Type);
                    listy.Add(customData.Brand);
                    listy.Add(customData.StorageLocation);
                    listy.Add(customData.Reference);
                    listy.Add(customData.FinderDepartment);
                    listy.Add(customData.ReceiverName);
                    listy.Add(Convert.ToDateTime(customData.ReceiveDate).AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.RoomNumber);
                    listy.Add(customData.Colour);

                    listy.Add(customData.SubType);
                    listy.Add(customData.SubStorageLocation);
                    listy.Add(customData.ShelfLife);
                    listy.Add(customData.Remarks);

                    if (customData.Status.ToUpper() == "RETURN")
                    {
                        var returnData = ItemOwner.GetItemOwnerByReference(customData.Reference, dbConnection);
                        if (returnData != null)
                        {
                            listy.Add(returnData.ReturnDate.AddHours(userinfo.TimeZone).ToString());
                            listy.Add(returnData.Name);
                            listy.Add(returnData.Type);
                            listy.Add(returnData.RoomNumber);
                            listy.Add(returnData.Nationality);
                            listy.Add(returnData.Address);
                            listy.Add(returnData.MobileNo);
                            listy.Add(returnData.Email);
                            listy.Add(returnData.IdNumber);
                            listy.Add(returnData.ShippingNumber);
                            listy.Add(returnData.CustomerUName);//HandedBy
                            if (returnData.TransferSiteId > 0)
                            {
                                listy.Add("block");
                                listy.Add(returnData.TransferSiteName);
                            }else
                            {
                                listy.Add("none");
                                listy.Add("N/A");
                            }

                        }
                    }
                    else if (customData.Status.ToUpper() == "DISPOSE" || customData.Status.ToUpper() == "DONATE")
                    {
                        var returnData = ItemDispose.GetItemDisposeByReference(customData.Reference, dbConnection);
                        if (returnData != null)
                        {
                            listy.Add(returnData.DisposedDate.AddHours(userinfo.TimeZone).ToString());
                            listy.Add(returnData.DisposedTo);
                            listy.Add(returnData.RecipientName);
                            listy.Add(returnData.RecipientCompany);
                            listy.Add(returnData.RecipientLocation);
                            listy.Add(returnData.RecipientContactNo);
                            listy.Add(returnData.RecipientIdNo);
                            listy.Add(returnData.Destroyed);
                            listy.Add(returnData.DisposedAfter);
                            listy.Add(returnData.CustomerUName); //DisposedBy
                            listy.Add(returnData.Witness);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTableRowDataFound", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowDataEnquiry(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = ItemInquiry.GetItemInquiryById(id, dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.ItemBrand);
                    listy.Add(customData.LocationLost);
                    listy.Add(customData.ItemColour);
                    listy.Add(customData.Type);
                    listy.Add(customData.ItemDescription);
                    listy.Add(customData.Name);
                    listy.Add(customData.Address);
                    listy.Add(customData.ContactNo);
                    listy.Add(customData.RoomNumber);
                    var foundCheck = false;
                    if(customData.Found)
                    {
                        foundCheck = true;
                    }

                    if (!string.IsNullOrEmpty(customData.ItemReference))
                    {
                        listy.Add(customData.ItemReference);
                        var itemRef = ItemFound.GetItemFoundByReference(customData.ItemReference, dbConnection);
                        listy.Add(itemRef.Status);

                        if(foundCheck)
                        {
                            listy.Add("<p>Search done and item has been processsed <a class='red-color' href='#' data-target='#foundItemViewCard'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + itemRef.Id + "&apos;) '> (" + itemRef.Reference + ")</a></p>");
                        }
                    }
                    else
                    {
                        listy.Add("N/A");
                        listy.Add("Not Found");
                        if (foundCheck)
                        {
                            listy.Add("<p>Search done but could not find item</p>");
                        }
                        else
                            listy.Add("<p>Search has not yet been done</p>");
                    }
                    listy.Add(customData.ReferenceNo);
                    listy.Add(customData.SubType);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTableRowDataEnquiry", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getVehicleColorData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var alldata = new List<VehicleColor>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = VehicleColor.GetAllVehicleColorByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = VehicleColor.GetAllVehicleColorByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = VehicleColor.GetAllVehicleColor(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    alldata = VehicleColor.GetAllVehicleColorBySiteId(userinfo.SiteId, dbConnection);
                }


                foreach (var item in alldata)
                {
                    var action = "<a href='#' data-target='#editVehicleColorModal'  data-toggle='modal' onclick='rowchoiceVehicleColor(&apos;" + item.ColorCode + "&apos;,&apos;" + item.ColorDesc + "&apos;,&apos;" + item.ColorDesc_AR + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteVehColorModal'  data-toggle='modal' onclick='rowchoiceVehicleColor(&apos;" + item.ColorCode + "&apos;,&apos;" + item.ColorDesc + "&apos;,&apos;" + item.ColorDesc_AR + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.ColorDesc + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getVehicleColorData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
         
        [WebMethod]
        public static string delVehColor(int id, string uname)
        {
            var listy = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {

                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    Arrowlabs.Business.Layer.VehicleColor.DeleteVehicleColorByColorCode(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", id.ToString(), id.ToString(), userinfo, "Delete Color" + id);
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "delVehColor", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
            }
            return listy;
        }

        [WebMethod]
        public static string delAssetCatSub(int id, string uname)
        {
            var listy = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var ret = Arrowlabs.Business.Layer.Asset.DeleteAssetbyId(id, dbConnection);
                    
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", id.ToString(), id.ToString(), userinfo, "Delete AssetSub" + id);
                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "delAssetCatSub", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteItemFound(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var ret = Arrowlabs.Business.Layer.ItemFound.DeleteItemFoundById(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "ItemFound-" + id, id.ToString(), "ItemFound", userinfo, "Lost and Found was deleted on_" + CommonUtility.getDTNow());


                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteItemFound", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteItemLost(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var itemLostData = ItemLost.GetItemLostById(id, dbConnection);
                    var msg = "Asset";

                    if (itemLostData.isKey)
                        msg = "Key";

                    var ret = Arrowlabs.Business.Layer.ItemLost.DeleteItemLostById(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, msg + "-" + id, id.ToString(), msg, userinfo, msg + " was deleted on_" + CommonUtility.getDTNow());
                        try
                        {
                            var atchments = Arrowlabs.Business.Layer.ItemLostAttachment.GetItemLostAttachmentByItemLostId(id,dbConnection);
                            foreach (var att in atchments)
                            {
                                CommonUtility.CloudDeleteFile(att.AttachmentPath);
                            }
                        }
                        catch { }
                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteItemLost", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteItemEnquiry(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var ret = Arrowlabs.Business.Layer.ItemInquiry.DeleteItemInquiryById(id, dbConnection);
                    

                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "ItemInquiry-" + id, id.ToString(), "ItemInquiry", userinfo, "Enquiry was deleted on_" + CommonUtility.getDTNow());


                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteItemEnquiry", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteAttachmentDataAsset(int id, string uname)
        {
            var listy = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var getRet = ItemLostAttachment.GetAllItemLostAttachments(dbConnection);
                    getRet = getRet.Where(i => i.Id == id).ToList();
                    if (getRet != null && getRet.Count > 0)
                    {
                        var ret = Arrowlabs.Business.Layer.ItemLostAttachment.DeleteItemLostAttachmentById(id, dbConnection);
                        if (ret)
                        {
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "ItemLostAttachment", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());

                            CommonUtility.CloudDeleteFile(getRet[0].AttachmentPath);

                            listy = "Successfully deleted entry";
                        }
                        else
                            listy = "Failed to delete entry";
                    }
                    else
                        listy = "Attachment already deleted";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteAttachmentData(int id, string uname)
        {
            var listy = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var ret = Arrowlabs.Business.Layer.LostAndFoundAttachments.DeleteLostAndFoundAttachmentsById(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "LostAndFoundAttachments", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());


                        listy = "Successfully deleted entry";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string delAssetCat(int id, string uname)
        {
            var listy = string.Empty;
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var retCats = Asset.GetAllAsset(dbConnection);
                    foreach (var rets in retCats)
                    {
                        if (rets.AssetCategoryId == id)
                            Arrowlabs.Business.Layer.Asset.DeleteAssetbyId(rets.Id, dbConnection);
                    }
                    var ret = Arrowlabs.Business.Layer.AssetCategory.DeleteAssetCategorybyId(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", id.ToString(), id.ToString(), userinfo, "Delete AssetCat" + id);
                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "delAssetCat", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string saveAssetItemData(string name, string comment, string asset, string location, string imgpath, string uname, bool isKey)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var newItemLost = new ItemLost();
                    newItemLost.Name = name;
                    newItemLost.Comments = comment;
                    newItemLost.CreatedBy = userinfo.Username;
                    newItemLost.CreatedDate = CommonUtility.getDTNow();
                    newItemLost.SiteId = userinfo.SiteId;
                    newItemLost.UpdatedBy = userinfo.Username;
                    newItemLost.UpdatedDate = CommonUtility.getDTNow();
                    newItemLost.isKey = isKey;
                    var loc = Location.GetAllLocationByLocationDescAndCId(location,userinfo.CustomerInfoId ,dbConnection);
                    loc = loc.Where(i => i.ParentLocationId > 0).ToList();
                    if (loc.Count > 0)
                        newItemLost.LocationId = loc[0].ID;

                    if (!string.IsNullOrEmpty(asset))
                        newItemLost.AssetCatId = Convert.ToInt32(asset);

                    newItemLost.CustomerId = userinfo.CustomerInfoId;
                    var retid = ItemLost.InsertOrUpdateItemLost(newItemLost, dbConnection, dbConnectionAudit, true);
                    if (imgpath.Contains('|'))
                    {
                        var itemLostA = new ItemLostAttachment();

                        var newimgPath = imgpath.Replace('|', '\\');

                        if (File.Exists(newimgPath))
                        {
                            Stream fs = File.OpenRead(newimgPath);
                            var getFileName = Path.GetFileName(newimgPath);
                            //str.CopyTo(data);
                            //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                            //byte[] buf = new byte[data.Length];
                            //data.Read(buf, 0, buf.Length);
                            getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                            var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                            if (savestring != "FAIL")
                            {
                                //bmap.Save(savestring);

                                itemLostA.AttachmentPath = savestring;

                                itemLostA.ItemLostId = retid;
                                itemLostA.CreatedBy = userinfo.Username;
                                itemLostA.CreatedDate = CommonUtility.getDTNow();
                                itemLostA.SiteId = userinfo.SiteId;
                                itemLostA.CustomerId = userinfo.CustomerInfoId;
                                
                                itemLostA.UpdatedBy = userinfo.Username;
                                itemLostA.UpdatedDate = CommonUtility.getDTNow();

                                ItemLostAttachment.InsertOrUpdateItemLostAttachment(itemLostA, dbConnection, dbConnectionAudit, true);

                            }
                            else
                            {
                                MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                                listy = "Failed to upload file to cloud.";
                            }
                            if (fs != null)
                            {
                                fs.Close();
                            }
                            if (File.Exists(newimgPath))
                                File.Delete(newimgPath);
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                            listy = "File trying to upload doesn't exist.";
                        }
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "saveAssetItemData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        
        
        [WebMethod]
        public static string processAssetItem(int id, string process, string chkname, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var lostItem = ItemLost.GetItemLostById(id, dbConnection);
                    if (lostItem != null)
                    {
                        var oldName = lostItem.OwnerName;
                        if (string.IsNullOrEmpty(oldName))
                            oldName = lostItem.UpdatedBy;

                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Asset-" + id, chkname, oldName, userinfo, "Checked " + process + "_" + CommonUtility.getDTNow());

                        if (process == "in")
                        {
                            lostItem.Lost = 0;
                            lostItem.CheckedinBy = chkname;
                        }
                        else if (process == "out"){
                            lostItem.Lost = 1;
                        }
                        lostItem.OwnerName = chkname;
                        lostItem.UpdatedDate = CommonUtility.getDTNow();
                        lostItem.UpdatedBy = userinfo.Username;
                        lostItem.CustomerId = userinfo.CustomerInfoId;
                        ItemLost.InsertOrUpdateItemLost(lostItem, dbConnection, dbConnectionAudit, true);

                        listy = "SUCCESS";
                    }
                    else
                    {
                        listy = "Not able to find data entry of item";
                    }
                    
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "processAssetItem", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string editVehColor(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = VehicleColor.GetVehicleColorById(id, dbConnection);
                    if (off != null)
                    {
                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (off.ColorDesc != name)
                        {
                            var getData2 = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                oldValue = off.ColorDesc;
                                newValue = name;
                                off.ColorDesc = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "VEHICLE COLOR ColorDesc-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        if (off.ColorDesc_AR != nameAR)
                        {
                            oldValue = off.ColorDesc_AR;
                            newValue = nameAR;
                            off.ColorDesc_AR = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "VEHICLE COLOR ColorDesc_AR-" + id);
                        }
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        VehicleColor.InsertorUpdateVehicleColor(off, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "editVehColor", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string addVehColor(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }

                    if (getData == null)
                    {

                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = VehicleColor.GetAllVehicleColorByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.ColorDesc.ToLower() == name.ToLower())
                                        VehicleColor.DeleteVehicleColorByColorCode(getcat.ColorCode, dbConnection);
                                }
                            }
                        }

                        var off = new VehicleColor();
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.ColorDesc = name;
                        off.ColorDesc_AR = nameAR;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CustomerId = userinfo.CustomerInfoId;
                        off.SiteId = userinfo.SiteId;
                        VehicleColor.InsertorUpdateVehicleColor(off, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "addVehColor", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static List<string> searchForItemsTable(string searchItemTypeSelect, string dateFromCalendar, string FromTime, string dateToCalendar, string ToTime, string tbSearchBrand, string searchLocationSelect, string searchColorSelect, string searchItemSubSelect, string searchLocationStoreSelect, string searchSubStoreSelect, string searchShelfLifeSelect,string description ,string uname, int eId)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemFound>();
                var dtFrom = CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0));
                var dtTo = CommonUtility.getDTNow();

                if (!string.IsNullOrEmpty(dateFromCalendar) && !string.IsNullOrEmpty(FromTime) && !string.IsNullOrEmpty(dateToCalendar) && !string.IsNullOrEmpty(ToTime))
                {
                    dtFrom = DateTime.Parse(dateFromCalendar + "," + FromTime);
                    dtTo = DateTime.Parse(dateToCalendar).Add(new TimeSpan(23,59,0));
                }
                if (!string.IsNullOrEmpty(searchLocationSelect))
                {
                    if (searchLocationSelect.ToLower().Contains("please select"))
                        searchLocationSelect = "";
                }

                if (!string.IsNullOrEmpty(searchColorSelect))
                {
                    if (searchColorSelect.ToLower().Contains("please select"))
                        searchColorSelect = "";
                }

                if (!string.IsNullOrEmpty(searchItemSubSelect))
                {
                    if (searchItemSubSelect.ToLower().Contains("please select"))
                        searchItemSubSelect = "";
                }
                //string searchLocationStoreSelect, string searchSubStoreSelect, string searchShelfLifeSelect, 
                if (!string.IsNullOrEmpty(searchItemTypeSelect))
                {
                    if (searchItemTypeSelect.ToLower().Contains("please select"))
                        searchItemTypeSelect = "";
                }
                if (!string.IsNullOrEmpty(searchLocationStoreSelect))
                {
                    if (searchLocationStoreSelect.ToLower().Contains("please select"))
                        searchLocationStoreSelect = "";
                }
                if (!string.IsNullOrEmpty(searchSubStoreSelect))
                {
                    if (searchSubStoreSelect.ToLower().Contains("please select"))
                        searchSubStoreSelect = "";
                }
                if (!string.IsNullOrEmpty(searchShelfLifeSelect))
                {
                    if (searchShelfLifeSelect.ToLower().Contains("please select"))
                        searchShelfLifeSelect = "";
                }
                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allcustomEvents = ItemFound.SearchItemFoundSuperAdmin(searchItemTypeSelect, dtFrom.ToString(), dtTo.ToString(), tbSearchBrand, searchLocationSelect, searchColorSelect, searchItemSubSelect, searchLocationStoreSelect, searchSubStoreSelect, searchShelfLifeSelect, description, dbConnection);
                    
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = ItemFound.SearchItemFoundSuperAdmin(searchItemTypeSelect, dtFrom.ToString(), dtTo.ToString(), tbSearchBrand, searchLocationSelect, searchColorSelect, searchItemSubSelect, searchLocationStoreSelect, searchSubStoreSelect, searchShelfLifeSelect, description, dbConnection);
                    allcustomEvents = allcustomEvents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                }
                else
                {
                    allcustomEvents = ItemFound.SearchItemFoundSuperAdmin(searchItemTypeSelect, dtFrom.ToString(), dtTo.ToString(), tbSearchBrand, searchLocationSelect, searchColorSelect, searchItemSubSelect, searchLocationStoreSelect, searchSubStoreSelect, searchShelfLifeSelect, description, dbConnection);
                    allcustomEvents = allcustomEvents.Where(i => (i.SiteId == userinfo.SiteId) || (i.CustomerId == userinfo.CustomerInfoId && i.SiteId == 0)).ToList();
                 //   allcustomEvents = ItemFound.SearchItemFound(searchItemTypeSelect, dtFrom.ToString(), dtTo.ToString(), tbSearchBrand, searchLocationSelect, searchColorSelect, searchItemSubSelect, searchLocationStoreSelect, searchSubStoreSelect, searchShelfLifeSelect, description, userinfo.SiteId, dbConnection);
                }
                //var usersList = CommonUtility.getUsersOfManagerDirector(userinfo);
                foreach (var item in allcustomEvents)
                { 
                        var returnstring = "<tr role='row' class='odd'><td >" + item.CreatedDate.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.Reference + "</td><td>" + item.Status + "</td><td><a href='#' data-target='#foundItemViewCard'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                        listy.Add(returnstring);
 
                }
                if (eId > 0)
                {
                    var itemEnq = ItemInquiry.GetItemInquiryById(eId, dbConnection);
                    itemEnq.UpdatedBy = userinfo.Username;
                    itemEnq.UpdatedDate = CommonUtility.getDTNow();
                    //if (allcustomEvents.Count > 0)
                        itemEnq.Found = true;
                   // else
                      //  itemEnq.Found = false;

                    ItemInquiry.InsertorUpdateItemInquiry(itemEnq, dbConnection, dbConnectionAudit, true);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "searchForItemsTable", err, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }

        [WebMethod]
        public static string getGenerateBarcode(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                listy = "FAIL";
                try
                {
                    //if(userinfo.RoleId == (int)Role.Manager ||userinfo.RoleId == (int)Role.Admin ||userinfo.RoleId == (int)Role.Admin )
                    //{
                    var lastid = ItemFound.GetLastItemFoundIdBySiteId(new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1), CommonUtility.getDTNow().AddHours(24), userinfo.SiteId, dbConnection);

                    var getmonth = CommonUtility.getDTNow().Month;
                    int getyear = CommonUtility.getDTNow().Year % 20;
                    var retmonth = getmonth.ToString();
                    if (getmonth < 10)
                        retmonth = "0" + getmonth.ToString();

                    var prefix = "S";

                    var retsiteid = userinfo.SiteId.ToString();
                    if (userinfo.SiteId > 0)
                    {
                        if (userinfo.SiteId.ToString().Length == 1)
                            retsiteid = "00" + userinfo.SiteId;
                        else if (userinfo.SiteId.ToString().Length == 2)
                            retsiteid = "0" + userinfo.SiteId;
                    }
                    else
                    {
                        prefix = "C";
                        retsiteid = userinfo.CustomerInfoId.ToString();
                        if (userinfo.CustomerInfoId.ToString().Length == 1)
                            retsiteid = "00" + userinfo.CustomerInfoId;
                        else if (userinfo.CustomerInfoId.ToString().Length == 2)
                            retsiteid = "0" + userinfo.CustomerInfoId;
                    }

                    while (true)
                    {
                        lastid = lastid + 1;
                        var retCount = (lastid).ToString();

                        var itemCount = (lastid);

                        if (itemCount.ToString().Length == 4)
                            retCount = "0" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 3)
                            retCount = "00" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 2)
                            retCount = "000" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 1)
                            retCount = "0000" + itemCount.ToString();

                        var retstring = prefix+retsiteid + retCount + retmonth + getyear.ToString();
                        var fItem = ItemFound.GetItemFoundByReference(retstring, dbConnection);
                        if (fItem == null)
                        {
                            listy = retstring;
                            break;
                        }
                    }
                    //}
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "getGenerateBarcode", err, dbConnection, userinfo.SiteId);
                    listy = "FAIL";
                }
                return listy;
            }
        }

        [WebMethod]
        public static List<string> searchBarcode(string id, string uname)
        {
            var listy = new List<string>();
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
                return listy;
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    { 
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var customData = ItemFound.GetItemFoundByReference(id, dbConnection);
                    if (customData != null) 
                    {
                        if (customData.CustomerId == userinfo.CustomerInfoId)
                        {
                            listy.Add(customData.FinderName);
                            listy.Add(customData.ReceiverName);
                            listy.Add(customData.Brand);
                            listy.Add(customData.RoomNumber);
                            listy.Add(customData.FinderDepartment);
                            listy.Add(customData.Type);
                            listy.Add(customData.LocationFound);
                            listy.Add(customData.Colour);
                            listy.Add(customData.StorageLocation);
                            listy.Add(customData.DateFound.ToString());
                            listy.Add(customData.ReceiveDate);
                            listy.Add(customData.ImagePath);

                            listy.Add(customData.SubType);
                            listy.Add(customData.SubStorageLocation);
                            listy.Add(customData.ShelfLife);
                        }
                        else
                        {
                            listy.Add("ERROR");
                            return listy;
                        }
                    }
                    else
                    {
                        listy.Add("ERROR");
                        return listy;
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "searchBarcode", err, dbConnection, userinfo.SiteId);
                    listy.Add("FAIL");
                    return listy;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string tranferImages(string id,int newid ,string uname)
        {
            var listy = new List<string>();
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
                return listy[0];
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var customData = ItemFound.GetItemFoundByReference(id, dbConnection);

                    if (customData != null)
                    {
                        var getExtraAttachments = new List<LostAndFoundAttachments>();

                            getExtraAttachments = LostAndFoundAttachments.GetAttachmentsByItemFoundId(customData.Id, dbConnection);
                        
                        var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        if (customData.Status == "Return")
                        {
                            var returnData = ItemOwner.GetItemOwnerByReference(customData.Reference, dbConnection);
                            if (returnData != null)
                            {
                                var newObj = new LostAndFoundAttachments();
                                newObj.CreatedBy = userinfo.Username;
                                newObj.CreatedDate = CommonUtility.getDTNow();
                                newObj.ImagePath = returnData.IdCopy;
                                newObj.ItemFoundId = newid;
                                newObj.SiteId = userinfo.SiteId;
                                newObj.CustomerId = userinfo.CustomerInfoId;
                                newObj.UpdatedBy = userinfo.Username;
                                newObj.UpdatedDate = CommonUtility.getDTNow();
                                LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj, dbConnection);

                                var newObj2 = new LostAndFoundAttachments();
                                newObj2.CreatedBy = userinfo.Username;
                                newObj2.CreatedDate = CommonUtility.getDTNow();
                                newObj2.ImagePath = returnData.Signature;
                                newObj2.ItemFoundId = newid;
                                newObj2.SiteId = userinfo.SiteId;
                                newObj2.CustomerId = userinfo.CustomerInfoId;
                                newObj2.UpdatedBy = userinfo.Username;
                                newObj2.UpdatedDate = CommonUtility.getDTNow();
                                LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj2, dbConnection);
                            }                            
                        }
                        else if (customData.Status == "Dispose" || customData.Status == "Donate")
                        {
                            var returnData = ItemDispose.GetItemDisposeByReference(customData.Reference, dbConnection);
                            if (returnData != null)
                            {
                                var newObj = new LostAndFoundAttachments();
                                newObj.CreatedBy = userinfo.Username;
                                newObj.CreatedDate = CommonUtility.getDTNow();
                                newObj.ImagePath = returnData.ImagePath;
                                newObj.ItemFoundId = newid;
                                newObj.SiteId = userinfo.SiteId;
                                newObj.CustomerId = userinfo.CustomerInfoId;
                                newObj.UpdatedBy = userinfo.Username;
                                newObj.UpdatedDate = CommonUtility.getDTNow();
                                LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj, dbConnection);
                            }
                        }
                        foreach (var attach in getExtraAttachments)
                        {
                            if (!string.IsNullOrEmpty(attach.ImagePath))
                            {
                                if (System.IO.Path.GetExtension(attach.ImagePath).ToUpperInvariant() != ".PDF")
                                {
                                    var newObj = new LostAndFoundAttachments();
                                    newObj.CreatedBy = userinfo.Username;
                                    newObj.CreatedDate = CommonUtility.getDTNow();
                                    newObj.ImagePath = attach.ImagePath;
                                    newObj.ItemFoundId = newid;
                                    newObj.CustomerId = userinfo.CustomerInfoId;
                                    newObj.SiteId = userinfo.SiteId;
                                    newObj.UpdatedBy = userinfo.Username;
                                    newObj.UpdatedDate = CommonUtility.getDTNow();
                                    LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj, dbConnection);
                                }
                            }
                        }
                    }
                    else
                    {
                        listy.Add("ERROR");
                        return listy[0];
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "tranferImages", err, dbConnection, userinfo.SiteId);
                    listy.Add("FAIL");
                    return listy[0];
                }
                return listy[0];
            }
        }    

        //Finder department

        [WebMethod]
        public static List<string> getFinderDepData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }
                 
                var allcustomEvents = new List<FinderDepartment>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = FinderDepartment.GetAllFinderDepartmentByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents = FinderDepartment.GetAllFinderDepartmentByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = FinderDepartment.GetAllFinderDepartment(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    allcustomEvents = FinderDepartment.GetAllFinderDepartmentBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in allcustomEvents)
                {
                    var action = "<a href='#' data-target='#editFinderDepModal'  data-toggle='modal' onclick='rowchoiceFinderDep(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteFinderModal'  data-toggle='modal' onclick='rowchoiceFinderDep(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }
                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getFinderDepData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string editFinderDepModal(int id, string name, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }


                    var off = FinderDepartment.GetFinderDepartmentById(id, dbConnection);
                    if (off != null)
                    {
                        if (off.Name != name)
                        {
                            var getData2 = FinderDepartment.GetFinderDepartmentByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = FinderDepartment.GetFinderDepartmentByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                var oldValue = off.Name;
                                var newValue = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", newValue, oldValue, userinfo, "Finder Department Settings change for id-" + id);
                                off.Name = name;
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        off.UpdatedDate = CommonUtility.getDTNow();
                        FinderDepartment.InsertOrUpdateFinderDepartment(off, dbConnection, dbConnectionAudit, true); 
                    }
                    else
                    {
                        return "Name already exists";
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "editFinderDepModal", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string addFinderDep(int id, string name,  string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }
                     
                    var getData = FinderDepartment.GetFinderDepartmentByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = FinderDepartment.GetFinderDepartmentByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }
                    if (getData == null)
                    {
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = FinderDepartment.GetAllFinderDepartmentByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.Name.ToLower() == name.ToLower())
                                        FinderDepartment.DeleteFinderDepartmentById(getcat.Id, dbConnection);
                                }
                            }
                        }

                        var off = new FinderDepartment();
                        off.Name = name;
                        off.SiteId = userinfo.SiteId;
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CustomerId  = userinfo.CustomerInfoId;
                        FinderDepartment.InsertOrUpdateFinderDepartment(off, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "addFinderDep", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string delFinderDep(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var ret = Arrowlabs.Business.Layer.FinderDepartment.DeleteFinderDepartmentById(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", id.ToString(), id.ToString(), userinfo, "Delete FinderDepartment" + id);
                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "delFinderDep", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }

        [WebMethod]
        public static string addNewItemRemarks(int id, string notes, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            { 
                var projinfo = ItemFound.GetItemFoundById(id, dbConnection);
                if (projinfo != null)
                {
                    var newRemarks = new ItemFoundRemarks();
                    newRemarks.CreatedBy = userinfo.Username;
                    newRemarks.CreatedDate = CommonUtility.getDTNow();
                    newRemarks.ItemFoundId = id;
                    newRemarks.Remarks = notes;
                    newRemarks.UpdatedBy = userinfo.Username;
                    newRemarks.UpdatedDate = CommonUtility.getDTNow();
                    newRemarks.Reference = projinfo.Reference;
                    newRemarks.CustomerId = userinfo.CustomerInfoId;
                    newRemarks.SiteId = userinfo.SiteId;
                    var retV = 0;
                    if (!string.IsNullOrEmpty(notes))
                    {
                        retV = ItemFoundRemarks.InsertOrUpdateItemFoundRemarks(newRemarks, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        retV = 1;
                    }
                    if (retV > 0)
                    {
                        json = "SUCCESS";
                    }
                    else
                    {
                        json = "Problem faced trying to save notes";
                    }
                }
                else
                {
                    json = "Problem faced trying to get lost item and save remarks";
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "addNewItemRemarks", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static List<string> getItemRemarksData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = ItemFound.GetItemFoundById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = ItemFoundRemarks.GetItemFoundRemarksByItemFoundId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Remarks))
                    {
                        var nTR = new ItemFoundRemarks();
                        nTR.CustomerUName = projinfo.CustomerUName;
                        nTR.Remarks = projinfo.Remarks;
                        nTR.CreatedDate = projinfo.UpdatedDate;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        var count = 0;
                        foreach (var task in remarksList)
                        {
                            if (count == 3)
                            {
                                break;
                            }
                            count++;
                            var OGremarks = task.Remarks;
                            if (!string.IsNullOrEmpty(task.Remarks) && task.Remarks.Length > 50)
                            {
                                task.Remarks = task.Remarks.Remove(50);
                                task.Remarks = task.Remarks + "...";
                            }
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                        if (remarksList.Count > 3)
                        {
                            var seeall = "<h5><span  class='line-center' onmouseover='style=&apos;cursor: pointer;' onclick='showAllRemarks(&apos;" + id + "&apos;)'>SEE ALL</span></h5>";
                            listy.Add(seeall);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Lost", "getItemRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getItemRemarksData2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = ItemFound.GetItemFoundById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = ItemFoundRemarks.GetItemFoundRemarksByItemFoundId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Remarks))
                    {
                        var nTR = new ItemFoundRemarks();
                        nTR.CustomerUName = projinfo.CustomerUName;
                        nTR.Remarks = projinfo.Remarks;
                        nTR.CreatedDate = projinfo.UpdatedDate;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        foreach (var task in remarksList)
                        {
                            var OGremarks = task.Remarks;
 
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Lost", "getItemRemarksData2", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
    }
}