﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ConfigSettingsAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string DeviceMAC { get; set; }
        public int Port { get; set; }
        public int Interval { get; set; }
        public string ServerIP { get; set; }
        public string ConnectionString { get; set; }
        public string ServerConnection { get; set; }
        public string GPSURL { get; set; }
        public string SettingsPassword { get; set; }
        public string SettingsMasterPassword { get; set; }
        public string SnapFilePath { get; set; }
        public string PCName { get; set; }
        public string ClientID { get; set; }
        public string AppVersion { get; set; }
        public int RegisterState { get; set; }
        public int CurfewState { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? CurrentDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CurfewDays { get; set; }
        public int ThirdPartyPort { get; set; }
        public int VideoLength { get; set; }
        public int NoImages { get; set; }

        private static ConfigSettingsAudit GetConfigSettingsAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var configSettingsAudit = new ConfigSettingsAudit();
            configSettingsAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("DeviceMAC") && !string.IsNullOrEmpty(reader["DeviceMAC"].ToString()))
                configSettingsAudit.DeviceMAC = reader["DeviceMAC"].ToString();

            if (columns.Contains("Port") && !string.IsNullOrEmpty(reader["Port"].ToString()))
                configSettingsAudit.Port = Convert.ToInt16(reader["Port"].ToString());

            if (columns.Contains("Interval") && !string.IsNullOrEmpty(reader["Interval"].ToString()))
                configSettingsAudit.Interval = Convert.ToInt16(reader["Interval"].ToString());

            if (columns.Contains("ServerIP") && !string.IsNullOrEmpty(reader["ServerIP"].ToString()))
                configSettingsAudit.ServerIP = reader["ServerIP"].ToString();

            if (columns.Contains("ConnectionString") && !string.IsNullOrEmpty(reader["ConnectionString"].ToString()))
                configSettingsAudit.ConnectionString = reader["ConnectionString"].ToString();

            if (columns.Contains("ServerConnection") && !string.IsNullOrEmpty(reader["ServerConnection"].ToString()))
                configSettingsAudit.ServerConnection = reader["ServerConnection"].ToString();

            if (columns.Contains("GPSURL") && !string.IsNullOrEmpty(reader["GPSURL"].ToString()))
                configSettingsAudit.GPSURL = reader["GPSURL"].ToString();

            if (columns.Contains("SettingsPassword") && !string.IsNullOrEmpty(reader["SettingsPassword"].ToString()))
                configSettingsAudit.SettingsPassword = reader["SettingsPassword"].ToString();

            if (columns.Contains("SettingsMasterPassword") && !string.IsNullOrEmpty(reader["SettingsMasterPassword"].ToString()))
                configSettingsAudit.SettingsMasterPassword = reader["SettingsMasterPassword"].ToString();

            if (columns.Contains("SnapFilePath") && !string.IsNullOrEmpty(reader["SnapFilePath"].ToString()))
                configSettingsAudit.SnapFilePath = reader["SnapFilePath"].ToString();

            if (columns.Contains("PCName") && !string.IsNullOrEmpty(reader["PCName"].ToString()))
                configSettingsAudit.PCName = reader["PCName"].ToString();

            if (columns.Contains("ClientID") && !string.IsNullOrEmpty(reader["ClientID"].ToString()))
                configSettingsAudit.ClientID = reader["ClientID"].ToString();

            if (columns.Contains("AppVersion") && !string.IsNullOrEmpty(reader["AppVersion"].ToString()))
                configSettingsAudit.AppVersion = reader["AppVersion"].ToString();

            if (columns.Contains("RegisterState") && !string.IsNullOrEmpty(reader["RegisterState"].ToString()))
                configSettingsAudit.RegisterState = Convert.ToInt16(reader["RegisterState"].ToString());

            if (columns.Contains("CurfewState") && !string.IsNullOrEmpty(reader["CurfewState"].ToString()))
                configSettingsAudit.CurfewState = Convert.ToInt16(reader["CurfewState"].ToString());

            if (columns.Contains("CurfewDays") && !string.IsNullOrEmpty(reader["CurfewDays"].ToString()))
                configSettingsAudit.CurfewDays = reader["CurfewDays"].ToString();

            if (columns.Contains("ExpiryDate") && !string.IsNullOrEmpty(reader["ExpiryDate"].ToString()))
                configSettingsAudit.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"].ToString());

            if (columns.Contains("CreatedDate") && !string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                configSettingsAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("CurrentDate") && !string.IsNullOrEmpty(reader["CurrentDate"].ToString()))
                configSettingsAudit.CurrentDate = Convert.ToDateTime(reader["CurrentDate"].ToString());

            if (columns.Contains("ThirdPartyPort") && !string.IsNullOrEmpty(reader["ThirdPartyPort"].ToString()))
                configSettingsAudit.ThirdPartyPort = Convert.ToInt16(reader["ThirdPartyPort"].ToString());

            if (columns.Contains("VideoLength") && !string.IsNullOrEmpty(reader["VideoLength"].ToString()))
                configSettingsAudit.VideoLength = Convert.ToInt16(reader["VideoLength"].ToString());

            if (columns.Contains("NoImages") && !string.IsNullOrEmpty(reader["NoImages"].ToString()))
                configSettingsAudit.NoImages = Convert.ToInt16(reader["NoImages"].ToString());

            return configSettingsAudit;
        }
        public static ConfigSettingsAudit GetAllConfigSettingsAudit(string dbConnection)
        {
            ConfigSettingsAudit configSettingsAudit = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllConfigSettingsAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    configSettingsAudit = GetConfigSettingsAuditFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return configSettingsAudit;
        }
        public static bool InsertConfigSettingsAudit(ConfigSettingsAudit configSettingsAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(configSettingsAudit.BaseAudit, dbConnection);

            if (configSettingsAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertConfigSettingsAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@DeviceMAC", SqlDbType.NVarChar).Value = configSettingsAudit.DeviceMAC;
                    cmd.Parameters.Add("@Port", SqlDbType.Int).Value = configSettingsAudit.Port;
                    cmd.Parameters.Add("@Interval", SqlDbType.Int).Value = configSettingsAudit.Interval;
                    cmd.Parameters.Add("@ServerIP", SqlDbType.NVarChar).Value = configSettingsAudit.ServerIP;
                    cmd.Parameters.Add("@ConnectionString", SqlDbType.NVarChar).Value = configSettingsAudit.ConnectionString;
                    cmd.Parameters.Add("@ServerConnection", SqlDbType.NVarChar).Value = configSettingsAudit.ServerConnection;
                    cmd.Parameters.Add("@GPSURL", SqlDbType.NVarChar).Value = configSettingsAudit.GPSURL;
                    cmd.Parameters.Add("@SettingsPassword", SqlDbType.NVarChar).Value = configSettingsAudit.SettingsPassword;
                    cmd.Parameters.Add("@SettingsMasterPassword", SqlDbType.NVarChar).Value = configSettingsAudit.SettingsMasterPassword;
                    cmd.Parameters.Add("@SnapFilePath", SqlDbType.NVarChar).Value = configSettingsAudit.SnapFilePath;
                    cmd.Parameters.Add("@PCName", SqlDbType.NVarChar).Value = configSettingsAudit.PCName;
                    cmd.Parameters.Add("@ClientID", SqlDbType.NVarChar).Value = configSettingsAudit.ClientID;
                    cmd.Parameters.Add("@AppVersion", SqlDbType.NVarChar).Value = configSettingsAudit.AppVersion;
                    cmd.Parameters.Add("@RegisterState", SqlDbType.Int).Value = configSettingsAudit.RegisterState;
                    cmd.Parameters.Add("@CurfewState", SqlDbType.Int).Value = configSettingsAudit.CurfewState;
                    cmd.Parameters.Add("@CurfewDays", SqlDbType.Int).Value = configSettingsAudit.CurfewDays;
                    cmd.Parameters.Add("@ExpiryDate", SqlDbType.DateTime).Value = configSettingsAudit.ExpiryDate;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = configSettingsAudit.CreatedDate;
                    cmd.Parameters.Add("@CurrentDate", SqlDbType.DateTime).Value = configSettingsAudit.CurrentDate;
                    cmd.Parameters.Add("@ThirdPartyPort", SqlDbType.Int).Value = configSettingsAudit.ThirdPartyPort;
                    cmd.Parameters.Add("@VideoLength", SqlDbType.Int).Value = configSettingsAudit.VideoLength;
                    cmd.Parameters.Add("@NoImages", SqlDbType.Int).Value = configSettingsAudit.NoImages;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
