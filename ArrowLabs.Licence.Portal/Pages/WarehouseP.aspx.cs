﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Web.Services;
using System.Web.Configuration;
using System.Globalization;
using System.Text;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using System.Threading;
using ArrowLabs.Licence.Portal.Helpers;
namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class WarehouseP : System.Web.UI.Page
    {
        protected string customersDisplay;
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static ClientLicence getClientLic;
        static List<string> VideoExtensions = new List<string> { ".AVI", ".MP4", ".MKV", ".FLV" };

        protected string namePlaceholder;
        protected string descPlaceholder;
        protected string longiPlaceholder;
        protected string latiPlaceholder;
        protected string assigntomyselfPlaceholder;
        protected string sendPlaceholder;
        protected string saveastemplatePlaceholder;

        protected string severePlaceholder;
        protected string highPlaceholder;
        protected string mediumPlaceholder;
        protected string lowPlaceholder;

        protected string userPlaceholder;
        protected string groupPlaceholder;
        protected string multiplePlaceholder;

        protected string taskTypePlaceholder;
        protected string selectChecklistPlaceholder;
        protected string selectAssigneePlaceholder;
        protected string selectLocationPlaceholder;

        protected string requiresignaturePlaceholder;

        protected string selectAccountPlaceholder;
        protected string selectContractPlaceholder;
        protected string selectProjectPlaceholder;


        protected string senderName;
        protected string pendingPercent;
        protected string inprogressPercent;
        protected string completedPercent;
        protected string pendingLabel;
        protected string inprogressLabel;
        protected string completedLabel;
        protected string totalLabel;
        protected string ipaddress;
        protected string userinfoDisplay;
        protected string senderName2;
        protected string sourceLat;
        protected string sourceLon;
        protected string currentlocation;
        protected string landfview;
        protected string assetview;
        protected string keyview;
        protected string assetactive;
        protected string keyactive;
        protected string senderName3;
        protected string siteName;

        protected string grpOptionView;
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }


            namePlaceholder = "Name";
            descPlaceholder = "Description";
            longiPlaceholder = "Longitude";
            latiPlaceholder = "Latitude";
            assigntomyselfPlaceholder = "Assign to myself";
            sendPlaceholder = "SEND";
            saveastemplatePlaceholder = "SAVE AS TEMPLATE";
            severePlaceholder = "Severe";
            highPlaceholder = "High";
            mediumPlaceholder = "Medium";
            lowPlaceholder = "Low";

            userPlaceholder = "User";
            groupPlaceholder = "Group";
            multiplePlaceholder = "Multiple";

            taskTypePlaceholder = "Task Type";
            selectChecklistPlaceholder = "Select Checklist";
            selectAssigneePlaceholder = "Select Assignee";
            selectLocationPlaceholder = "Select Location";

            requiresignaturePlaceholder = "REQUIRES SIGNATURE";

            selectAccountPlaceholder = "Select Account";
            selectContractPlaceholder = "Select Contract";

            selectProjectPlaceholder = "Select Project";

            grpOptionView = "none";

            customersDisplay = "none";
            userinfoDisplay = "block";
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            var manager = User.Identity.Name;
            senderName2 = Encrypt.EncryptData(User.Identity.Name, true, dbConnection);
            senderName = manager;
            var userinfo = Users.GetUserByName(manager, dbConnection);
            landfview = "style='display:none'";
            assetview = "style='display:none'";
            keyview = "style='display:none'";
            try
            {
                var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
                ipaddress = configtSettings.ChatServerUrl + "/signalr";

                if (userinfo.SiteId > 0)
                {
                    siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                }

                senderName3 = userinfo.CustomerUName;
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    assetview = "style='display:block'";
                }
                if (landfview != "style='display:block'" && assetview == "style='display:block'")
                {
                    assetactive = "active in";
                }
                else if (landfview != "style='display:block'" && assetview != "style='display:block'" && keyview == "style='display:block'")
                {
                    keyactive = "active in";

                }
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    {
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Warehouse)
                        {
                            assetview = "style='display:block'";
                        }
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.ServerKey)
                        {
                            keyview = "style='display:block'";
                        }
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Customer)
                        { 
                            customersDisplay = "block";
                        }
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Collaboration)
                        {
                            grpOptionView = "block";
                        }
                    }
                    if (landfview != "style='display:block'" && assetview == "style='display:block'")
                    {
                        assetactive = "active in";
                    }
                    else if (landfview != "style='display:block'" && assetview != "style='display:block'" && keyview == "style='display:block'")
                    {
                        keyactive = "active in";

                    }
                    userinfoDisplay = "none";

                    if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        userinfoDisplay = "block";
                    if (userinfo.RoleId == (int)Role.Director)
                        userinfoDisplay = "block";
                    //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                    //if (pushDev != null)
                    //{
                    //    if (!string.IsNullOrEmpty(pushDev.Username))
                    //    {
                    //        pushDev.IsServerPortal = true;
                    //        pushDev.SiteId = userinfo.SiteId;
                    //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection, dbConnectionAudit, true);
                    //    }
                    //}
                    PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);
                }
                else
                {
                    grpOptionView = "block";
                    customersDisplay = "block";
                }
                var offenceTypes = new List<WarehouseAssetCategory>();
                var fDeps = new List<FinderDepartment>();
                var colorsList = new List<VehicleColor>();

                var ooffenceTypes = new List<WarehouseAssetCategory>();
                var otype = new WarehouseAssetCategory();
                otype.Name = "Select Category";
                otype.Id = 0;
                ooffenceTypes.Add(otype);

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    offenceTypes = WarehouseAssetCategory.GetAllWarehouseAssetCategoryByCustomerId(userinfo.CustomerInfoId, dbConnection);
                    //fDeps = FinderDepartment.GetAllFinderDepartmentByCId(userinfo.CustomerInfoId, dbConnection);
                    //colorsList = VehicleColor.GetAllVehicleColorByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    offenceTypes = WarehouseAssetCategory.GetAllWarehouseAssetCategoryByLevel5(userinfo.ID, dbConnection);
                    //fDeps = FinderDepartment.GetAllFinderDepartmentByLevel5(userinfo.ID, dbConnection);
                    //colorsList = VehicleColor.GetAllVehicleColorByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    offenceTypes = WarehouseAssetCategory.GetAllWarehouseAssetCategory(dbConnection);
                    //fDeps = FinderDepartment.GetAllFinderDepartment(dbConnection);
                    //colorsList = VehicleColor.GetAllVehicleColor(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    offenceTypes = WarehouseAssetCategory.GetAllWarehouseAssetCategoryBySiteId(userinfo.SiteId, dbConnection);
                    // fDeps = FinderDepartment.GetAllFinderDepartmentBySiteId(userinfo.SiteId, dbConnection);
                    // colorsList = VehicleColor.GetAllVehicleColorBySiteId(userinfo.SiteId, dbConnection);
                }

                offenceTypes = offenceTypes.OrderBy(i => i.Name).ToList();

                ooffenceTypes.AddRange(offenceTypes);

                typeEditSelect.DataTextField = "Name";
                typeEditSelect.DataValueField = "Id";
                typeEditSelect.DataSource = ooffenceTypes;
                typeEditSelect.DataBind();

                categoryAssetSelect.DataTextField = "Name";
                categoryAssetSelect.DataValueField = "Id";
                categoryAssetSelect.DataSource = ooffenceTypes;
                categoryAssetSelect.DataBind();

                sparecategoryAssetSelect.DataTextField = "Name";
                sparecategoryAssetSelect.DataValueField = "Id";
                sparecategoryAssetSelect.DataSource = ooffenceTypes;
                sparecategoryAssetSelect.DataBind();

                newspareassetCategorySelect.DataTextField = "Name";
                newspareassetCategorySelect.DataValueField = "Id";
                newspareassetCategorySelect.DataSource = ooffenceTypes;
                newspareassetCategorySelect.DataBind();
                

                typeSelect.DataTextField = "Name";
                typeSelect.DataValueField = "Id";
                typeSelect.DataSource = ooffenceTypes;
                typeSelect.DataBind();

                itemTypeSelect.DataTextField = "Name";
                itemTypeSelect.DataValueField = "Name";
                itemTypeSelect.DataSource = ooffenceTypes;
                itemTypeSelect.DataBind();

                var selAsset = new WarehouseAssetCategory();
                selAsset.Name = "Select Type";
                var selAssetList = new List<WarehouseAssetCategory>();
                selAssetList.Add(selAsset);
                selAssetList.AddRange(offenceTypes);

                searchItemTypeSelect.DataTextField = "Name";
                searchItemTypeSelect.DataValueField = "Name";
                searchItemTypeSelect.DataSource = selAssetList;
                searchItemTypeSelect.DataBind();

                enquiryTypeSelect.DataTextField = "Name";
                enquiryTypeSelect.DataValueField = "Name";
                enquiryTypeSelect.DataSource = selAssetList;
                enquiryTypeSelect.DataBind();

                finderDepartmentSelect.DataTextField = "Name";
                finderDepartmentSelect.DataValueField = "Name";
                finderDepartmentSelect.DataSource = fDeps;
                finderDepartmentSelect.DataBind();

                colorsList = colorsList.OrderBy(i => i.ColorDesc).ToList();

                itemColourSelect.DataTextField = "COLORDESC";
                itemColourSelect.DataValueField = "COLORDESC";
                itemColourSelect.DataSource = colorsList;
                itemColourSelect.DataBind();

                var selColor = new VehicleColor();
                selColor.ColorDesc = "Please Select Color";
                var searchColorList = new List<VehicleColor>();
                searchColorList.Add(selColor);
                searchColorList.AddRange(colorsList);

                searchColorSelect.DataTextField = "COLORDESC";
                searchColorSelect.DataValueField = "COLORDESC";
                searchColorSelect.DataSource = searchColorList;
                searchColorSelect.DataBind();

                enquiryColourSelect.DataTextField = "COLORDESC";
                enquiryColourSelect.DataValueField = "COLORDESC";
                enquiryColourSelect.DataSource = searchColorList;
                enquiryColourSelect.DataBind();


                var sitesample = new Arrowlabs.Business.Layer.Site();
                sitesample.Name = "Please select site";
                sitesample.Id = 0;
                var allsites = new List<Arrowlabs.Business.Layer.Site>();
                allsites.Add(sitesample);

                //allsites.AddRange();
                var sessions = Arrowlabs.Business.Layer.Site.GetAllSite(dbConnection);

                if (userinfo.RoleId != (int)Role.SuperAdmin)
                    sessions = sessions.Where(i => i.CustomerInfoId == userinfo.CustomerInfoId).ToList();

                allsites.AddRange(sessions);

                var locsList = Location.GetAllLocation(dbConnection);
                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin)
                {
                    allsites = allsites.Where(i => i.Id != userinfo.SiteId).ToList();
                    locsList = Location.GetAllLocationBySiteId(userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    locsList.Clear();
                    // var usites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    // foreach (var usite in usites)
                    //  {
                    locsList.AddRange(Location.GetAllLocationByLevel5(userinfo.ID, dbConnection));
                    //  }
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    locsList = Location.GetAllMainLocationByCustomerId(userinfo.CustomerInfoId, dbConnection);
                }


                var grouped = locsList.GroupBy(item => item.ID);
                locsList = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();
                locsList = locsList.OrderBy(i => i.LocationDesc).ToList();

                var llocsList = new List<Location>();

                var loc = new Location();
                loc.LocationDesc = "Select Location";
                llocsList.Add(loc);
                llocsList.AddRange(locsList);

                locationFoundSelect.DataTextField = "LOCATIONDESC";
                locationFoundSelect.DataValueField = "LOCATIONDESC";
                locationFoundSelect.DataSource = llocsList;
                locationFoundSelect.DataBind();

                locationStoreSelect.DataTextField = "LOCATIONDESC";
                locationStoreSelect.DataValueField = "LOCATIONDESC";
                locationStoreSelect.DataSource = llocsList;
                locationStoreSelect.DataBind();


                var selLoc = new Location();
                selLoc.LocationDesc = "Select Location";

                var searchLocs = new List<Location>();
                searchLocs.Add(selLoc);
                searchLocs.AddRange(locsList);

                searchLocationSelect.DataTextField = "LOCATIONDESC";
                searchLocationSelect.DataValueField = "LOCATIONDESC";
                searchLocationSelect.DataSource = searchLocs;
                searchLocationSelect.DataBind();

                searchLocationStoreSelect.DataTextField = "LOCATIONDESC";
                searchLocationStoreSelect.DataValueField = "LOCATIONDESC";
                searchLocationStoreSelect.DataSource = searchLocs;
                searchLocationStoreSelect.DataBind();

                enquiryLocationSelect.DataTextField = "LOCATIONDESC";
                enquiryLocationSelect.DataValueField = "LOCATIONDESC";
                enquiryLocationSelect.DataSource = searchLocs;
                enquiryLocationSelect.DataBind();

                categoryAssetMainLocation.DataTextField = "LOCATIONDESC";
                categoryAssetMainLocation.DataValueField = "LOCATIONDESC";
                categoryAssetMainLocation.DataSource = llocsList;
                categoryAssetMainLocation.DataBind();
                 
                transferSiteSelect.DataTextField = "Name";
                transferSiteSelect.DataValueField = "Id";
                transferSiteSelect.DataSource = allsites;
                transferSiteSelect.DataBind();

                transferSiteSelectFrom.DataTextField = "Name";
                transferSiteSelectFrom.DataValueField = "Id";
                transferSiteSelectFrom.DataSource = allsites;
                transferSiteSelectFrom.DataBind();

                var usersearchSelectItems = new List<SearchSelectItem>();
                var groupsearchSelectItems = new List<SearchSelectItem>();

                var allusers = new List<Users>();

                var allgroups = new List<Group>();

                var tasktemplateList = new List<TemplateCheckList>();
                var finaltasktemplateList = new List<TemplateCheckList>();
                var TemplateCheck = new TemplateCheckList();
                TemplateCheck.Name = selectChecklistPlaceholder;
                TemplateCheck.Id = -1;
                finaltasktemplateList.Add(TemplateCheck);


                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                { 
                    allusers = Users.GetAllUsers(dbConnection);
                    allgroups = Group.GetAllGroup(dbConnection);

                    if (userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                        allgroups = allgroups.Where(i => i.CreatedBy == userinfo.Username).ToList();
                    }
                }
                else if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                { 
                    tasktemplateList.AddRange(TemplateCheckList.GetAllTemplateCheckListBySiteId(userinfo.SiteId, dbConnection));
                    if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var allmanagers = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                        allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                        foreach (var usermanager in allmanagers)
                        {
                            allusers.Add(usermanager);
                            var allmanagerusers = Users.GetAllFullUsersByManagerIdForDirector(usermanager.ID, dbConnection);
                            allusers.AddRange(allmanagerusers);
                        }
                        var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                        unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                        foreach (var subsubuser in unassigned)
                        {
                            allusers.Add(subsubuser);
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Manager)
                    {
                        allusers.AddRange(Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection));
                        allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        allusers.AddRange(Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection));
                        allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                { 
                    tasktemplateList.AddRange(TemplateCheckList.GetAllTemplateCheckListByLevel5(userinfo.ID, dbConnection));

                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                    allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser)
                { 
                    tasktemplateList.AddRange(TemplateCheckList.GetAllTemplateCheckListByCId(userinfo.CustomerInfoId, dbConnection));

                    allusers.AddRange(Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection));
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                }
                allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                var grouped3 = allusers.GroupBy(item => item.ID);
                allusers = grouped3.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();


                var newSearchSelectItem2 = new SearchSelectItem();
                newSearchSelectItem2.ID = "0";
                newSearchSelectItem2.Name = selectAssigneePlaceholder;
                usersearchSelectItems.Add(newSearchSelectItem2);
                groupsearchSelectItems.Add(newSearchSelectItem2);

                if (userinfo.RoleId == (int)Role.Director)
                {
                    foreach (var searchSelectUser in allusers)
                    {
                        if (searchSelectUser.RoleId != (int)Role.Director)
                        {
                            var newSearchSelectItem = new SearchSelectItem();
                            newSearchSelectItem.ID = searchSelectUser.ID.ToString();
                            newSearchSelectItem.Name = searchSelectUser.CustomerUName;
                            usersearchSelectItems.Add(newSearchSelectItem);
                        }
                    }
                }
                else
                {
                    foreach (var searchSelectUser in allusers)
                    {
                        if (searchSelectUser.Username != userinfo.Username)
                        {
                            var newSearchSelectItem = new SearchSelectItem();
                            newSearchSelectItem.ID = searchSelectUser.ID.ToString();
                            newSearchSelectItem.Name = searchSelectUser.CustomerUName;
                            usersearchSelectItems.Add(newSearchSelectItem);
                        }
                    }
                }
                foreach (var searchSelectGroup in allgroups)
                {
                    var newSearchSelectItem = new SearchSelectItem();
                    newSearchSelectItem.ID = searchSelectGroup.Id.ToString();
                    newSearchSelectItem.Name = searchSelectGroup.Name;
                    groupsearchSelectItems.Add(newSearchSelectItem);
                }

                groupsearchSelect.DataTextField = "Name";
                groupsearchSelect.DataValueField = "ID";
                groupsearchSelect.DataSource = groupsearchSelectItems;
                groupsearchSelect.DataBind();

                var grouped2 = tasktemplateList.GroupBy(item => item.Id);
                tasktemplateList = grouped2.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                tasktemplateList = tasktemplateList.OrderBy(item => item.Name).ToList();

                finaltasktemplateList.AddRange(tasktemplateList);

                checklistSelect.DataTextField = "Name";
                checklistSelect.DataValueField = "Id";
                checklistSelect.DataSource = finaltasktemplateList;
                checklistSelect.DataBind();

                usersearchSelect.DataTextField = "Name";
                usersearchSelect.DataValueField = "ID";
                usersearchSelect.DataSource = usersearchSelectItems;
                usersearchSelect.DataBind();
                var tskType = new TaskCategory();
                tskType.CategoryId = 0;
                tskType.Description = taskTypePlaceholder;
                var tasktypes = new List<TaskCategory>();
                tasktypes.Add(tskType);

                var gtTypes = TaskCategory.GetAllTaskCategories(dbConnection);
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        gtTypes = TaskCategory.GetAllTaskCategoriesByCId(userinfo.CustomerInfoId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        gtTypes = TaskCategory.GetAllTaskCategoriesByLevel5(userinfo.ID, dbConnection);
                    }
                    else
                    {
                        gtTypes = TaskCategory.GetAllTaskCategoriesBySiteId(userinfo.SiteId, dbConnection);
                    }
                }
                else
                {
                    gtTypes = TaskCategory.GetAllTaskCategories(dbConnection);
                }
                tasktypes.AddRange(gtTypes);
                taskTypeSelect.DataTextField = "Description";
                taskTypeSelect.DataValueField = "CategoryId";
                taskTypeSelect.DataSource = tasktypes;
                taskTypeSelect.DataBind();


                var ilosts = new List<ItemLost>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    ilosts = ItemLost.GetAllItemLost(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    ilosts = ItemLost.GetAllItemLostBySite(userinfo.SiteId, dbConnection);
                    //allcustomEvents = allcustomEvents.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    ilosts.AddRange(ItemLost.GetAllItemLostByCustomerId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    ilosts.AddRange(ItemLost.GetAllItemLostByLevel5(userinfo.ID, dbConnection));
                }

                var groupedilosts = ilosts.GroupBy(item => item.Id);
                ilosts = groupedilosts.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                ilosts = ilosts.Where(i => i.isKey == false).ToList();
                ilosts = ilosts.Where(i => !i.isSparePart).ToList();
                 
                assetplanchartSelect.DataTextField = "Name";
                assetplanchartSelect.DataValueField = "Id";
                assetplanchartSelect.DataSource = ilosts;
                assetplanchartSelect.DataBind();

                assetsparechartSelect.DataTextField = "Name";
                assetsparechartSelect.DataValueField = "Id";
                assetsparechartSelect.DataSource = ilosts;
                assetsparechartSelect.DataBind();

                var allcustomEvents = new List<AssetPlan>();
                var callcustomEvents = new List<AssetPlan>();
                var ac = new AssetPlan();
                ac.TaskName = "Please Select";
                ac.Id = 0;
                ac.isTemplate = true;
                allcustomEvents.Add(ac);

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    callcustomEvents = AssetPlan.GetAllAssetPlan(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    callcustomEvents = AssetPlan.GetAllAssetPlanBySiteId(userinfo.SiteId, dbConnection); 
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    callcustomEvents.AddRange(AssetPlan.GetAllAssetPlanByCId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    callcustomEvents.AddRange(AssetPlan.GetAllAssetPlanByLevel5(userinfo.ID, dbConnection));
                }

                var groupedallcustomEvents = callcustomEvents.GroupBy(item => item.Id);
                callcustomEvents = groupedallcustomEvents.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                callcustomEvents = callcustomEvents.Where(i => i.isTemplate).ToList();
                allcustomEvents.AddRange(callcustomEvents);

                plantempSelect.DataTextField = "TaskName";
                plantempSelect.DataValueField = "Id";
                plantempSelect.DataSource = allcustomEvents;
                plantempSelect.DataBind();

            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "Page_Load", err, dbConnection, userinfo.SiteId);
            }

        }

        [WebMethod]
        public static string editAssetSub(int id, string name, string type, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }



                    var getOffence = WarehouseAsset.GetWarehouseAssetById(id, dbConnection);
                    if (getOffence != null)
                    {
                        if (getOffence.Name != name || getOffence.AssetCategoryId != Convert.ToInt32(type))
                        {
                            var oldValue = getOffence.Name;
                            var newValue = name;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", newValue, oldValue, userinfo, "Asset Sub Category change name-" + id);
                            oldValue = getOffence.AssetCategoryId.ToString();
                            newValue = type;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", newValue, oldValue, userinfo, "Asset Sub Category change type-" + id);
                            getOffence.Name = name;
                            getOffence.AssetCategoryId = Convert.ToInt32(type);
                            getOffence.UpdatedBy = userinfo.Username;
                            getOffence.UpdatedDate = CommonUtility.getDTNow();

                            if (userinfo.CustomerInfoId > 0)
                                getOffence.CustomerId = userinfo.CustomerInfoId;

                            WarehouseAsset.InsertOrUpdateWarehouseAsset(getOffence, dbConnection);
                        }
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "editAssetSub", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }

                return listy;
            }
        }
        [WebMethod]
        public static string editAssetCat(int id, string name, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = WarehouseAssetCategory.GetWarehouseAssetCategoryById(id, dbConnection);
                    if (off != null)
                    {

                        if (off.Name.ToLower() != name.ToLower())
                        {
                            var getData2 = WarehouseAssetCategory.GetWarehouseAssetCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = WarehouseAssetCategory.GetWarehouseAssetCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                var oldValue = off.Name;
                                var newValue = name;
                                off.Name = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", newValue, oldValue, userinfo, "Asset Category change name-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();

                        if (userinfo.CustomerInfoId > 0)
                            off.CustomerId = userinfo.CustomerInfoId;

                        WarehouseAssetCategory.InsertorUpdateWarehouseAssetCategory(off, dbConnection);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "editAssetCat", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string addAssetCat(int id, string name, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = WarehouseAssetCategory.GetWarehouseAssetCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = WarehouseAssetCategory.GetWarehouseAssetCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }

                    if (getData == null)
                    {
                        var alist = new List<WarehouseAsset>();
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = WarehouseAssetCategory.GetAllWarehouseAssetCategoryByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.Name.ToLower() == name.ToLower())
                                    {
                                        var gassets = WarehouseAsset.GetAllWarehouseAssetByAssetCategoryId(getcat.Id, dbConnection);
                                        alist.AddRange(gassets);
                                        WarehouseAssetCategory.DeleteWarehouseAssetCategorybyId(getcat.Id, dbConnection);
                                    }
                                }
                            }
                        }

                        var off = new WarehouseAssetCategory();
                        off.Name = name;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CreatedBy = userinfo.ID;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.SiteId = userinfo.SiteId;

                        if (userinfo.CustomerInfoId > 0)
                            off.CustomerId = userinfo.CustomerInfoId;

                        var rid = WarehouseAssetCategory.InsertorUpdateWarehouseAssetCategory(off, dbConnection);
                        foreach (var item in alist)
                        {
                            item.AssetCategoryId = rid;
                            item.UpdatedBy = userinfo.Username;
                            item.UpdatedDate = CommonUtility.getDTNow();
                            WarehouseAsset.InsertOrUpdateWarehouseAsset(item, dbConnection);
                        }
                    }
                    else
                    {
                        return "Name already exists";
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "addAssetCat", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string addAssetSubCategory(int id, string name, string type, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    //  var getAll = WarehouseAsset.GetAllAsset(dbConnection);
                    //    getAll = getAll.Where(i => i.Name == name && i.AssetCategoryId == Convert.ToInt32(type)).ToList();



                    // if (getAll.Count > 0)
                    // {
                    //     listy = "The asset name matches another entry under this category";
                    //  }
                    // else
                    // {
                    var off = new WarehouseAsset();
                    off.Name = name;
                    off.AssetCategoryId = Convert.ToInt32(type);
                    off.UpdatedBy = userinfo.Username;
                    off.UpdatedDate = CommonUtility.getDTNow();
                    off.CreatedBy = userinfo.ID;
                    off.CreatedDate = CommonUtility.getDTNow();
                    off.LocationId = 1;
                    off.SiteId = userinfo.SiteId;

                    if (userinfo.CustomerInfoId > 0)
                        off.CustomerId = userinfo.CustomerInfoId;

                    WarehouseAsset.InsertOrUpdateWarehouseAsset(off, dbConnection);
                    listy = "SUCCESS";
                    //}
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "addAssetSubCategory", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static List<string> getAssetCatData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<WarehouseAssetCategory>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = WarehouseAssetCategory.GetAllWarehouseAssetCategoryByCustomerId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents = WarehouseAssetCategory.GetAllWarehouseAssetCategoryByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = WarehouseAssetCategory.GetAllWarehouseAssetCategory(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    allcustomEvents = WarehouseAssetCategory.GetAllWarehouseAssetCategoryBySiteId(userinfo.SiteId, dbConnection);
                }


                foreach (var item in allcustomEvents)
                {
                    var cuserinfo = Users.GetUserById(item.CreatedBy, dbConnection);

                    var action = "<a href='#' data-target='#EditOffenceCategoryModal'  data-toggle='modal' onclick='rowchoiceOffenceCategory(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;) '><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteOffenceCatModal'  data-toggle='modal' onclick='rowchoiceOffenceCategory(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != cuserinfo.Username)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.Name + "</td><td>" + item.CreatedDate.Value.AddHours(userinfo.TimeZone) + "</td><td>" + cuserinfo.CustomerUName + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getAssetCatData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAssetSubCatData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<WarehouseAsset>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = WarehouseAsset.GetAllWarehouseAssetByCustomerId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents = WarehouseAsset.GetAllWarehouseAssetByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = WarehouseAsset.GetAllWarehouseAsset(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    allcustomEvents = WarehouseAsset.GetAllWarehouseAssetBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in allcustomEvents)
                {
                    var cuserinfo = Users.GetUserById(item.CreatedBy, dbConnection);

                    var action = "<a href='#' data-target='#editOffenceModal'  data-toggle='modal' onclick='rowchoiceOffenceType(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;,&apos;" + item.AssetCategoryId + "&apos;) '><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteOffenceTypeModal'  data-toggle='modal' onclick='rowchoiceOffenceType(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;,&apos;" + item.AssetCategoryId + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != cuserinfo.Username)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.Name + "</td><td>" + item.AssetCategoryName + "</td><td>" + item.CreatedDate.Value.AddHours(userinfo.TimeZone) + "</td><td>" + cuserinfo.CustomerUName + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getAssetSubCatData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = ItemLost.GetItemLostById(id, dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.Name); //0

                    if (!string.IsNullOrEmpty(customData.Comments))
                        listy.Add(customData.Comments); //1
                    else
                        listy.Add("N/A"); //1
                     
                    var assetCat = WarehouseAsset.GetWarehouseAssetById(customData.AssetCatId, dbConnection);
                    if (assetCat != null)
                        listy.Add(assetCat.AssetCategoryName); //2
                    else
                        listy.Add("N/A"); //2

                    if (string.IsNullOrEmpty(customData.LocationDesc))
                        customData.LocationDesc = "N/A";

                    listy.Add(customData.LocationDesc); //3 
                    listy.Add(customData.CustomerUName); //4
                    var createdUser = Users.GetUserByName(customData.CreatedBy, dbConnection);
                    var sites = Arrowlabs.Business.Layer.Site.GetSiteById(createdUser.SiteId, dbConnection);
                    if (sites != null)
                    {
                        if (!string.IsNullOrEmpty(sites.Name))
                            listy.Add(sites.Name); //5
                        else
                            listy.Add("N/A"); //5
                    }
                    else
                    {
                        listy.Add("N/A"); //5
                    }

                    listy.Add(customData.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());//6
                    if (customData.MainLocationId > 0)
                    {
                        var ploc = Location.GetLocationById(customData.MainLocationId, dbConnection);
                        if (ploc != null)
                        {
                            listy.Add(ploc.LocationDesc); //7
                        }
                        else
                        {
                            listy.Add("N/A"); //7
                        }
                    }
                    else
                    {
                        listy.Add("N/A"); //7
                    }

                    if (assetCat != null)
                        listy.Add(assetCat.Name); //8
                    else
                        listy.Add("N/A"); //8

                    if (!string.IsNullOrEmpty(customData.Barcode))
                    {
                        listy.Add(customData.Barcode); //9
                    }
                    else
                    {
                        listy.Add("N/A"); //9
                    } 
                    if (!string.IsNullOrEmpty(customData.SerialNo))
                    {
                        listy.Add(customData.SerialNo); //10
                    }
                    else
                    {
                        listy.Add("N/A"); //10
                    }

                    var gparts = ItemLost.GetAllItemLostBySparePartId(customData.Id, dbConnection);
                    if(gparts.Count >0)
                        listy.Add("block"); //11
                    else
                        listy.Add("none"); //11

                    var gplans = AssetPlan.GetAllAssetPlanByAssetId(customData.Id, dbConnection);
                    if (gplans.Count > 0)
                        listy.Add("block"); //12
                    else
                        listy.Add("none"); //12

                    if (!string.IsNullOrEmpty(customData.AssetMake))
                    {
                        listy.Add(customData.AssetMake); //13
                    }
                    else
                    {
                        listy.Add("N/A"); //13
                    }

                    if (!string.IsNullOrEmpty(customData.AssetModel))
                    {
                        listy.Add(customData.AssetModel); //14
                    }
                    else
                    {
                        listy.Add("N/A"); //14
                    }

                    if (!string.IsNullOrEmpty(customData.Remarks) && customData.isSparePart)
                    {
                        listy.Add(customData.Remarks); //15
                    }
                    else
                    {
                        listy.Add("N/A"); //15
                    }

                    if (!string.IsNullOrEmpty(customData.AccountName))
                    {
                        listy.Add(customData.AccountName); //16
                    }
                    else
                    {
                        listy.Add("N/A"); //16
                    }

                    if (!string.IsNullOrEmpty(customData.ProjectName))
                    {
                        listy.Add(customData.ProjectName); //17
                    }
                    else
                    {
                        listy.Add("N/A"); //17
                    }

                    if (!string.IsNullOrEmpty(customData.SparePartParentName) && customData.isSparePart)
                    {
                        listy.Add(customData.SparePartParentName); //18
                    }
                    else
                    {
                        listy.Add("N/A"); //18
                    }
                    listy.Add(customData.SparePartParentId.ToString());//19
                    listy.Add(customData.MainAssetCatId.ToString()); //20
                    listy.Add(customData.AssetCatId.ToString()); //21
                    listy.Add(customData.ProjectId.ToString()); //22
                    listy.Add(customData.AccountId.ToString()); //23
                    if (!string.IsNullOrEmpty(customData.Contractor))
                    {
                        listy.Add(customData.Contractor); //24
                    }
                    else
                    {
                        listy.Add("N/A"); //24
                    }

                    listy.Add(customData.MainLocationId.ToString()); //25
                    listy.Add(customData.LocationId.ToString()); //26
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTableRowData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowDataPlan(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = AssetPlan.GetAssetPlanById(id, dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.PlanName); //0
                    listy.Add(customData.TaskName); //1 

                    if (!string.IsNullOrEmpty(customData.TaskDescription))
                        listy.Add(customData.TaskDescription); //2
                    else
                        listy.Add("N/A"); //2

                    if (customData.TaskType > 0)
                        listy.Add(customData.TaskTypeName); //3
                    else
                        listy.Add("N/A");//3 

                    if (customData.isRecurring)
                        listy.Add("Recurring"); //4
                    else
                        listy.Add("Scheduled"); //4

                    listy.Add(customData.StartDate.Value.ToShortDateString()); //5
                    listy.Add(customData.EndDate.Value.ToShortDateString()); //6
                    listy.Add(customData.PlanTime); //7

                    if (customData.ChecklistId > 0)
                        listy.Add(customData.ChecklistName); //8 
                    else
                        listy.Add("N/A");//8 

                    if (customData.Priority == (int)TaskPiority.Severe) //9
                        listy.Add(TaskPiority.Severe.ToString());
                    else if (customData.Priority == (int)TaskPiority.High)
                        listy.Add(TaskPiority.High.ToString());
                    else if (customData.Priority == (int)TaskPiority.Medium)
                        listy.Add(TaskPiority.Medium.ToString());
                    else if (customData.Priority == (int)TaskPiority.Low)
                        listy.Add(TaskPiority.Low.ToString());

                    if (customData.AssigneeType == (int)TaskAssigneeType.User)
                        listy.Add(TaskAssigneeType.User.ToString()); //10
                    else if (customData.AssigneeType == (int)TaskAssigneeType.Group)
                        listy.Add(TaskAssigneeType.Group.ToString()); //10

                    listy.Add(customData.CustomerAName); //11

                    if (customData.isSignatures) //12
                        listy.Add("Requires Signature");
                    else
                        listy.Add("Does not Signature");

                    listy.Add(customData.CustomerUName); //13

                    if (!string.IsNullOrEmpty(customData.AssetName))
                        listy.Add(customData.AssetName); //14
                    else
                        listy.Add("N/A"); //14

                    //var createdUser = Users.GetUserByName(customData.CreatedBy, dbConnection);
                    //var sites = Arrowlabs.Business.Layer.Site.GetSiteById(createdUser.SiteId, dbConnection);
                    //if (sites != null)
                    //{
                    //    if (!string.IsNullOrEmpty(sites.Name))
                    //        listy.Add(sites.Name); //14
                    //    else
                    //        listy.Add("N/A"); //14
                    //}
                    //else
                    //{
                    //    listy.Add("N/A"); //14
                    //} 

                    listy.Add(customData.CreateDate.Value.AddHours(userinfo.TimeZone).ToString()); //15
                     
                    listy.Add(customData.AssigneeId.ToString());//16
                    listy.Add(customData.ChecklistId.ToString());//17
                    if (customData.AssigneeType == 0 && userinfo.ID == customData.AssigneeId)
                        listy.Add("TRUE"); //18
                    else
                        listy.Add("FALSE"); //18

                    listy.Add(customData.TaskType.ToString());//19
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTableRowData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }


        [WebMethod]
        public static List<string> getassetplanScheduleDates(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = AssetPlan.GetAssetPlanById(id, dbConnection);

                if (customData != null)
                {
                    var aps = AssetPlanSchedule.GetAllAssetPlanScheduleByPlanId(customData.Id, dbConnection);
                    foreach(var s in aps)
                    {
                        listy.Add(s.PlanDateTime.Value.ToShortDateString());
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTableRowData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }


        [WebMethod]
        public static List<string> getChecklistData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);
                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence1))
                        listy.Add(offenceinfo.Offence1);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence2))
                        listy.Add(offenceinfo.Offence2);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence3))
                        listy.Add(offenceinfo.Offence3);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence4))
                        listy.Add(offenceinfo.Offence4);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getChecklistData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getSubLocations(string id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                listy.Add("Select Sub Location");
                if (!string.IsNullOrEmpty(id))
                {
                    var customData = Location.GetAllLocationByLocationDescAndCId(id, userinfo.CustomerInfoId, dbConnection);
                    if (customData.Count > 0)
                    {
                        var grouped = customData.GroupBy(item => item.ID);
                        customData = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                        var getSubs = Location.GetAllSubLocationByMainId(customData[0].ID, dbConnection);
                        if (getSubs.Count > 0)
                        {
                            getSubs = getSubs.OrderBy(i => i.LocationDesc).ToList();
                            
                            foreach (var Subs in getSubs)
                            {
                                listy.Add(Subs.LocationDesc);
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getSubLocations", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getSubCategories(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                //if (!string.IsNullOrEmpty(id))
                {
                    var customData = WarehouseAsset.GetAllWarehouseAsset(dbConnection);
                    if (customData.Count > 0)
                    {
                        customData = customData.Where(i => i.AssetCategoryId == id).ToList();
                        var grouped = customData.GroupBy(item => item.Id);
                        customData = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        customData = customData.OrderBy(i => i.Name).ToList();
                        listy.Add("Select Sub Category_0");
                        foreach (var Subs in customData)
                        {
                            listy.Add(Subs.Name + "_" + Subs.Id);
                        }

                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getSubCategories", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getSubItem(string id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                //if (!string.IsNullOrEmpty(id))
                {
                    var customData = WarehouseAsset.GetAllWarehouseAsset(dbConnection);
                    if (customData.Count > 0)
                    {

                        if (userinfo.RoleId != (int)Role.SuperAdmin)
                        {
                            customData = customData.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                        }

                        customData = customData.Where(i => i.AssetCategoryName == id).ToList();
                        var grouped = customData.GroupBy(item => item.Id);
                        customData = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        customData = customData.OrderBy(i => i.Name).ToList();
                        foreach (var Subs in customData)
                        {
                            listy.Add(Subs.Name + "_" + Subs.Id);
                        }

                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getSubItem", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string attachFileToItemCheckOut(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var cusEv = ItemLost.GetItemLostById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new ItemLostAttachment();
                    newObj.CreatedBy = userinfo.Username;
                    newObj.CreatedDate = CommonUtility.getDTNow();
                    var newimgPath = filepath.Replace('|', '\\');

                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);

                            newObj.AttachmentPath = savestring;
                            newObj.ItemLostId = cusEv.Id;
                            newObj.SiteId = cusEv.SiteId;
                            newObj.CustomerId = cusEv.CustomerId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            ItemLostAttachment.InsertOrUpdateItemLostAttachment(newObj, dbConnection);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToItemCheckOut", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }
        
        [WebMethod]
        public static string attachFileToAsset(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var cusEv = ItemLost.GetItemLostById(id, dbConnection);
                if (cusEv != null)
                {
                    var itemLostA = new ItemLostAttachment(); 
                    var newimgPath = filepath.Replace('|', '\\');

                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);

                            itemLostA.AttachmentPath = savestring;
                            itemLostA.ItemLostId = cusEv.Id;
                            itemLostA.CreatedBy = userinfo.Username;
                            itemLostA.CreatedDate = CommonUtility.getDTNow();
                            itemLostA.SiteId = userinfo.SiteId;
                            itemLostA.CustomerId = userinfo.CustomerInfoId;

                            itemLostA.UpdatedBy = userinfo.Username;
                            itemLostA.UpdatedDate = CommonUtility.getDTNow();
                            ItemLostAttachment.InsertOrUpdateItemLostAttachment(itemLostA, dbConnection, dbConnectionAudit, true);

                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToAsset", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }
        [WebMethod]
        public static string attachFileToItem(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var cusEv = ItemFound.GetItemFoundById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new LostAndFoundAttachments();
                    newObj.CreatedBy = userinfo.Username;
                    newObj.CreatedDate = CommonUtility.getDTNow();
                    var newimgPath = filepath.Replace('|', '\\');

                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        //str.CopyTo(data);
                        //data.Seek(0, SeekOrigin.Begin); // <-- missing line
                        //byte[] buf = new byte[data.Length];
                        //data.Read(buf, 0, buf.Length);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            //bmap.Save(savestring);

                            newObj.ImagePath = savestring;
                            newObj.ItemFoundId = cusEv.Id;
                            newObj.SiteId = cusEv.SiteId;
                            newObj.CustomerId = cusEv.CustomerId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj, dbConnection);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToItem", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static string getIncidentLocationData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var cusEv = ItemLost.GetItemLostById(id, dbConnection);
                if (cusEv != null)
                {
                    json += "[";
                    json += "{ \"Username\" : \"" + cusEv.Name + "\",\"Id\" : \"" + cusEv.Id + "\",\"Long\" : \"" + cusEv.Longitude.ToString() + "\",\"Lat\" : \"" + cusEv.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getIncidentLocationData", err, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getAttachmentDataIcons(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = ItemLostAttachment.GetItemLostAttachmentByItemLostId(id, dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        //int index = item.AttachmentPath.IndexOf("Lost");
                        //if (index > 0)
                        //{

                        //}
                        //else
                        //{
                        //    index = item.AttachmentPath.IndexOf("ItemFound");
                        //}
                        var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");

                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                        {
                            var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                            listy += retstring;
                            i++;
                        }
                        else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                        {

                        }
                        else
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img src='" + requiredString + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                            listy += retstring;
                            i++;
                        }

                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentDataIcons", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentAssetDataTab(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                } 
                var attachments = ItemLostAttachment.GetItemLostAttachmentByItemLostId(id, dbConnection);
                var i = 1;
                var iTab = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
 
                        var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
 

                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                        {
                            var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div></div></div>";
                            listy += retstring;
                            iTab++;
                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                        { 
                            var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentPath, i);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + requiredString + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + requiredString + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;margin-left:-20px;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset2(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;

                        }
                        else
                        {
                            var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentPath, i);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><img src='"+ item.AttachmentPath + "' style='width:50px;height:50px;'></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;margin-left:-20px;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset2(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;
                            iTab++;
                        }
                        i++;
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentAssetDataTab", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTab(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Location</p></div></div></div>";

                var attachments = ItemLostAttachment.GetItemLostAttachmentByItemLostId(id, dbConnection);
                var i = 1;
                var iTab = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        //int index = item.AttachmentPath.IndexOf("Lost");
                        //if (index > 0)
                        //{

                        //}
                        //else
                        //{
                        //    index = item.AttachmentPath.IndexOf("ItemFound");
                        //}
                        var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");

                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                        {
                            var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div></div></div>";
                            listy += retstring;
                            iTab++;
                        }
                        else if (System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant() == ".PDF")
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;

                            var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentPath, i);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + requiredString + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + requiredString + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;

                        }
                        else
                        {
                            var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentPath, i);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-file-image-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;
                            iTab++;
                        }
                        i++;
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentDataTab", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = ItemLostAttachment.GetItemLostAttachmentByItemLostId(id, dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        //int index = item.AttachmentPath.IndexOf("Lost");
                        //if (index > 0)
                        //{

                        //}
                        //else
                        //{
                        //    index = item.AttachmentPath.IndexOf("ItemFound");
                        //}
                        var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");
                        //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                        {
                            //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                            var retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='" + requiredString + "' /></video>";
                            listy.Add(retstring);
                            i++;
                        }
                        else if (System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                        {

                        }
                        else
                        {

                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img onclick='rotateMe(this);' src='" + requiredString + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstring);
                            i++;
                        }

                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> handleTicket(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var cusData = CustomEvent.GetCustomEventById(id, dbConnection);
                cusData.Handled = true;
                cusData.HandledBy = userinfo.Username;
                cusData.HandledTime = CommonUtility.getDTNow();
                cusData.UpdatedBy = userinfo.Username;
                cusData.IncidentStatus = CommonUtility.getIncidentStatusValue("resolve");
                CustomEvent.InsertCustomEvent(cusData, dbConnection, dbConnectionAudit, true);
                CustomEvent.CustomEventHandledById(cusData.EventId, true, userinfo.Username, dbConnection);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "handleTicket", ex, dbConnection, userinfo.SiteId);
                listy.Add("FAIL");
            }
            listy.Add("SUCCESS");
            return listy;
        }
        //User Profile
        [WebMethod]
        public static string changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return 0;
                }

                if (userinfo.ID > 0)
                {
                    var getuser = userinfo;
                    getuser.FirstName = firstname;
                    getuser.LastName = lastname;
                    getuser.Email = emailaddress;

                    if (getuser.RoleId != role)
                    {
                        getuser.RoleId = role;
                        if (role == (int)Role.Manager)
                        {
                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                        else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                        }
                        else
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                    }
                    if (getuser.RoleId == (int)Role.Manager)
                    {

                        var dirUser = Users.GetUserById(supervisor, dbConnection);
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        if (dirUser != null)
                        {
                            if (!string.IsNullOrEmpty(dirUser.Username))
                            {
                                List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                            
                                            }};
                                DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                            }
                        }
                    }
                    else if (getuser.RoleId == (int)Role.Operator)
                    {
                        if (supervisor > 0)
                        {
                            var manUser = Users.GetUserById(supervisor, dbConnection);
                            List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                            UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                    else if (getuser.RoleId == (int)Role.UnassignedOperator)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                    {
                        return userinfo.ID;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "addUserProfile", err, dbConnection, userinfo.SiteId);
            }
            return userinfo.ID;
        }

        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add("Online");//customData.Status);
                    listy.Add("circle-point circle-point-green");//+ CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                    fontstyle = "style='color:lime;'";
                    //  var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    //if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                    if (customData.RoleId != (int)Role.SuperAdmin)
                    {
                        //   if (pushDev != null)
                        //     {
                        //      if (!string.IsNullOrEmpty(pushDev.Username))
                        //      {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                        //  }
                        //   else
                        //   {
                        //      monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        //     fontstyle = "";
                        // }
                        //}
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static List<string> getReminderSelectorDates(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                // Set the start time (00:00 means 12:00 AM)
                DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(1, 0, 0));
                // Set the end time (23:55 means 11:55 PM)
                DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(24, 0, 0));
                //Set 5 minutes interval
                TimeSpan Interval = new TimeSpan(1, 0, 0);
                //To set 1 hour interval
                //TimeSpan Interval = new TimeSpan(1, 0, 0);           

                while (StartTime <= EndTime)
                {
                    listy.Add(StartTime.ToString("HH:mm"));
                    StartTime = StartTime.Add(Interval);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Messages", "getReminderSelectorDates", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getPlanData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<AssetPlan>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    allcustomEvents = AssetPlan.GetAllAssetPlan(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    allcustomEvents = AssetPlan.GetAllAssetPlanBySiteId(userinfo.SiteId, dbConnection);
                    //allcustomEvents = allcustomEvents.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(AssetPlan.GetAllAssetPlanByCId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents.AddRange(AssetPlan.GetAllAssetPlanByLevel5(userinfo.ID, dbConnection));
                }

                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                allcustomEvents = allcustomEvents.Where(i => !i.isTemplate).ToList();
                foreach (var item in allcustomEvents)
                { 
                    var action = string.Empty;
                    //if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    //{
                        action = "<a href='#' data-target='#newAssetModal'  data-toggle='modal' onclick='newPlanClick();loadtempplan(&apos;" + item.Id + "&apos;);'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteAssetItem'  data-toggle='modal' onclick='rowchoicePlan(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    //}

                    var returnstring = "<tr role='row' class='odd'><td>" + item.TaskName + "</td><td >" + item.CreateDate.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td>" + item.StartDate.Value.ToShortDateString() + "</td><td>" + item.EndDate.Value.ToShortDateString() + "</td><td><a href='#' data-target='#assetplanViewCard'  data-toggle='modal' onclick='hideBackPlan();rowchoicePlan(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);

                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getPlanData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTempPlanData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<AssetPlan>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    allcustomEvents = AssetPlan.GetAllAssetPlan(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    allcustomEvents = AssetPlan.GetAllAssetPlanBySiteId(userinfo.SiteId, dbConnection);
                    //allcustomEvents = allcustomEvents.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(AssetPlan.GetAllAssetPlanByCId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents.AddRange(AssetPlan.GetAllAssetPlanByLevel5(userinfo.ID, dbConnection));
                }

                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                allcustomEvents = allcustomEvents.Where(i => i.isTemplate).ToList();
                foreach (var item in allcustomEvents)
                {
                    var action = string.Empty;
                    //if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    //{
                        action = "<a href='#' data-target='#deleteAssetItem'  data-toggle='modal' onclick='rowchoicePlan(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    //}

                    var returnstring = "<tr role='row' class='odd'><td>" + item.TaskName + "</td><td >" + item.CreateDate.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td>" + item.StartDate.Value.ToShortDateString() + "</td><td>" + item.EndDate.Value.ToShortDateString() + "</td><td><a href='#' data-target='#newAssetModal'  data-toggle='modal' onclick='newPlanClick();loadtempplan(&apos;" + item.Id + "&apos;);'><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);

                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getPlanData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAssetPlansList(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var itemGroups = AssetPlan.GetAllAssetPlanByAssetId(id, dbConnection);
                if (itemGroups.Count > 0)
                {
                    foreach (var item in itemGroups)
                    {
                        if (item.Id != id)
                            listy.Add(item.Id + "|" + item.TaskName);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Lost", "getAssetPlansList", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getSparePartsList(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var itemGroups = ItemLost.GetAllItemLostBySparePartId(id, dbConnection);
                if (itemGroups.Count > 0)
                {
                    foreach (var item in itemGroups)
                    {
                        if (item.Id != id)
                            listy.Add(item.Id + "|" + item.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Lost", "getSparePartsList", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAssetScheduleItems(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var itemGroups = AssetPlanSchedule.GetAllAssetPlanScheduleByPlanId(id, dbConnection);
                if (itemGroups.Count > 0)
                {
                    foreach (var item in itemGroups)
                    {
                        var color = "red";

                        if (item.isSent)
                            color = "green";

                        listy.Add(item.TaskId + "|" + item.PlanDateTime.Value.ToString() + "|" + color);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Lost", "getAssetScheduleItems", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        
        [WebMethod]
        public static List<string> getSparePartData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemLost>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    allcustomEvents = ItemLost.GetAllItemLost(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    allcustomEvents = ItemLost.GetAllItemLostBySite(userinfo.SiteId, dbConnection);
                    //allcustomEvents = allcustomEvents.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByCustomerId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByLevel5(userinfo.ID, dbConnection));
                }

                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                allcustomEvents = allcustomEvents.Where(i => i.isKey == false).ToList();
                allcustomEvents = allcustomEvents.Where(i => i.isSparePart).ToList();
                foreach (var item in allcustomEvents)
                {


                    var action = string.Empty;
                    //if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    //{
                        action = "<a href='#' data-target='#newSpareModal'  data-toggle='modal' onclick='clearnewSpare();rowchoiceSpareEdit(&apos;" + item.Id + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deletelostItem'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    //}
                    //OPTIMIZE TO BE ADDED TO STORE PROCEDURE
                    var parent = string.Empty;

                    if (string.IsNullOrEmpty(item.Barcode))
                        item.Barcode = "N/A";

                    if (string.IsNullOrEmpty(item.SparePartParentName))
                        item.SparePartParentName = "N/A";

                    parent = item.SparePartParentName;
                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td >" + item.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td>" + item.Barcode + "</td><td>" + parent + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='hideAssetBack();rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);

                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getSparePartData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTicketingData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemLost>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    allcustomEvents = ItemLost.GetAllItemLost(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    allcustomEvents = ItemLost.GetAllItemLostBySite(userinfo.SiteId, dbConnection);
                    //allcustomEvents = allcustomEvents.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByCustomerId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                { 
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByLevel5(userinfo.ID, dbConnection)); 
                }

                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                allcustomEvents = allcustomEvents.Where(i => i.isKey == false).ToList();
                allcustomEvents = allcustomEvents.Where(i => !i.isSparePart).ToList();
                foreach (var item in allcustomEvents)
                {

                    var loststate = "Checked In";
                    var location = item.LocationDesc;
                    if (item.Lost == 1)
                        loststate = "Checked Out";
                    else if (item.Lost == 2)
                    {
                        loststate = "Transfered";
                        location = "Transfered to Police";
                    }

                    var action = string.Empty;
                    //if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    //{
                    action = "<a href='#' data-target='#newAssetModal'  data-toggle='modal' onclick='rowchoiceEdit(&apos;" + item.Id + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deletelostItem'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    //}
                    if (string.IsNullOrEmpty(location))
                        location = "N/A";

                    if (string.IsNullOrEmpty(item.Barcode))
                        item.Barcode = "N/A";

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td >" + item.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td>" + location + "</td><td>" + item.Barcode + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='showAssetBack();rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);

                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTicketingData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getKeysData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemLost>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    allcustomEvents = ItemLost.GetAllItemLost(dbConnection);
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    allcustomEvents = ItemLost.GetAllItemLostBySite(userinfo.SiteId, dbConnection);
                    //allcustomEvents = allcustomEvents.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByCustomerId(userinfo.CustomerInfoId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //  foreach (var site in sites)
                    // {
                    allcustomEvents.AddRange(ItemLost.GetAllItemLostByLevel5(userinfo.ID, dbConnection));
                    //  }
                }

                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                allcustomEvents = allcustomEvents.Where(i => i.isKey == true).ToList();

                foreach (var item in allcustomEvents)
                {

                    var loststate = "Checked In";
                    var location = item.LocationDesc;
                    if (item.Lost == 1)
                        loststate = "Checked Out";
                    else if (item.Lost == 2)
                    {
                        loststate = "Transfered";
                        location = "Transfered to Police";
                    }

                    var action = string.Empty;
                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        action = "<a href='#' data-target='#deletelostItem'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td >" + item.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td>" + location + "</td><td>" + loststate + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);

                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getKeysData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getFoundItemData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemFound>();

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = ItemFound.GetAllItemFound(dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager)
                    allcustomEvents = ItemFound.GetItemFoundBySiteId(userinfo.SiteId, dbConnection);
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    // foreach (var site in sites)
                    // {
                    allcustomEvents = ItemFound.GetItemFoundByLevel5(userinfo.ID, dbConnection);
                    //}
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = ItemFound.GetItemFoundByCId(userinfo.CustomerInfoId, dbConnection);
                }
                var grouped = allcustomEvents.GroupBy(item => item.Id);

                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();



                foreach (var item in allcustomEvents)
                {

                    var action = string.Empty;

                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        action = "<a href='#' data-target='#deletefoundItem'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Reference + "</td><td >" + item.CreatedDate.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.Brand + "</td><td>" + item.LocationFound + "</td><td>" + item.StorageLocation + "</td><td>" + item.Status + "</td><td><a href='#' data-target='#foundItemViewCard'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getFoundItemData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getEnquiryData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemInquiry>();
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = ItemInquiry.GetAllItemInquiry(dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager)
                    allcustomEvents = ItemInquiry.GetItemInquiryBySiteId(userinfo.SiteId, dbConnection);
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    //{
                    allcustomEvents = ItemInquiry.GetItemInquiryByLevel5(userinfo.ID, dbConnection);
                    //}
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = ItemInquiry.GetItemInquiryByCId(userinfo.CustomerInfoId, dbConnection);
                }
                var grouped = allcustomEvents.GroupBy(item => item.Id);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                //var usersList = CommonUtility.getUsersOfManagerDirector(userinfo);
                foreach (var item in allcustomEvents)
                {
                    var action = string.Empty;

                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        action = "<a href='#' data-target='#deleteinquiryItem'  data-toggle='modal' onclick='rowchoiceEnquiry(&apos;" + item.Id + "&apos;) '><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.ReferenceNo + "</td><td>" + item.Name + "</td><td >" + item.CreatedDate.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#itemEnquiryModal'  data-toggle='modal' onclick='rowchoiceEnquiry(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a>" + action + "</td></tr>";
                    listy.Add(returnstring);
                    //}
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getEnquiryData", err, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }

        [WebMethod]
        public static string getAttachmentDataTabFound(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                //listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Incident Location</p></div></div></div>";

                var attachments = ItemFound.GetItemFoundById(id, dbConnection);
                var getExtraAttachments = new List<LostAndFoundAttachments>();
                //if (attachments.Status != "Found")
                //{
                getExtraAttachments = LostAndFoundAttachments.GetAttachmentsByItemFoundId(attachments.Id, dbConnection);
                //}
                var i = 1;
                if (attachments.Status == "Found")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Item Image</p></div></div></div>";
                        listy += retstring;
                        i++;
                    }
                }
                else if (attachments.Status == "Return")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Item Image</p></div></div></div>";
                        listy += retstring;
                        i++;
                    }
                    var returnData = ItemOwner.GetItemOwnerByReference(attachments.Reference, dbConnection);
                    if (returnData != null)
                    {
                        if (!string.IsNullOrEmpty(returnData.IdCopy))
                        {
                            var retstring2 = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Return Image</p></div></div></div>";
                            listy += retstring2;
                            i++;
                        }
                        if (!string.IsNullOrEmpty(returnData.Signature))
                        {
                            var retstring2 = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Signature</p></div></div></div>";
                            listy += retstring2;
                            i++;
                        }
                    }
                }
                else if (attachments.Status == "Dispose" || attachments.Status == "Donate")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Item Image</p></div></div></div>";
                        listy += retstring;
                        i++;
                    }
                    var returnData = ItemDispose.GetItemDisposeByReference(attachments.Reference, dbConnection);
                    if (returnData != null)
                    {
                        if (!string.IsNullOrEmpty(returnData.ImagePath))
                        {
                            var retstring2 = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Dispose Image</p></div></div></div>";
                            listy += retstring2;
                            i++;
                        }
                    }
                }
                var extraI = 1;
                foreach (var attach in getExtraAttachments)
                {
                    //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    if (!string.IsNullOrEmpty(attach.ImagePath))
                    {
                        if (System.IO.Path.GetExtension(attach.ImagePath).ToUpperInvariant() == ".PDF")
                        {
                            //int index = attach.ImagePath.IndexOf("ItemFound");
                            //var requiredString = attach.ImagePath.Substring(index, (attach.ImagePath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;

                            var attachmentName = CommonUtility.getAttachmentDisplayName(attach.ImagePath, extraI);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + attach.ImagePath + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + attach.ImagePath + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoice(&apos;" + attach.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;
                            extraI++;
                        }
                        else
                        {

                            var attachmentName = CommonUtility.getAttachmentDisplayName(attach.ImagePath, extraI);

                            var retstringExtra = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-file-image-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoice(&apos;" + attach.Id + "&apos;)'></i></div></div></div>";
                            listy += retstringExtra;
                            i++;
                            extraI++;
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentDataTabFound", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentDataFound(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = ItemFound.GetItemFoundById(id, dbConnection);
                var getExtraAttachments = new List<LostAndFoundAttachments>();

                getExtraAttachments = LostAndFoundAttachments.GetAttachmentsByItemFoundId(attachments.Id, dbConnection);

                var i = 1;
                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                if (attachments.Status == "Found")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        //int index = attachments.ImagePath.IndexOf("ItemFound");
                        // var requiredString = attachments.ImagePath.Substring(index, (attachments.ImagePath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");
                        //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                        var retstring = "<img onclick='rotateMe(this);' src='" + attachments.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                        listy.Add(retstring);
                    }
                }
                else if (attachments.Status == "Return")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        //int index = attachments.ImagePath.IndexOf("ItemFound");
                        //var requiredString = attachments.ImagePath.Substring(index, (attachments.ImagePath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");
                        //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                        var retstring = "<img onclick='rotateMe(this);' src='" + attachments.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                        listy.Add(retstring);
                    }
                    var returnData = ItemOwner.GetItemOwnerByReference(attachments.Reference, dbConnection);
                    if (returnData != null)
                    {
                        if (!string.IsNullOrEmpty(returnData.IdCopy))
                        {
                            //int index = returnData.IdCopy.IndexOf("ItemFound");
                            //var requiredString = returnData.IdCopy.Substring(index, (returnData.IdCopy.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img onclick='rotateMe(this);' src='" + returnData.IdCopy + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstring);
                        }
                        if (!string.IsNullOrEmpty(returnData.Signature))
                        {
                            //int index = returnData.Signature.IndexOf("ItemFound");
                            //var requiredString = returnData.Signature.Substring(index, (returnData.Signature.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img onclick='rotateMe(this);' src='" + returnData.Signature + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstring);
                        }
                    }
                }
                else if (attachments.Status == "Dispose" || attachments.Status == "Donate")
                {
                    if (!string.IsNullOrEmpty(attachments.ImagePath))
                    {
                        //int index = attachments.ImagePath.IndexOf("ItemFound");
                        //var requiredString = attachments.ImagePath.Substring(index, (attachments.ImagePath.Length - index));
                        //requiredString = requiredString.Replace("\\", "/");
                        //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                        var retstring = "<img onclick='rotateMe(this);' src='" + attachments.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                        listy.Add(retstring);
                    }
                    var returnData = ItemDispose.GetItemDisposeByReference(attachments.Reference, dbConnection);
                    if (returnData != null)
                    {
                        if (!string.IsNullOrEmpty(returnData.ImagePath))
                        {
                            //int index = returnData.ImagePath.IndexOf("ItemFound");
                            //var requiredString = returnData.ImagePath.Substring(index, (returnData.ImagePath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstring = "<img onclick='rotateMe(this);' src='" + returnData.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstring);
                        }
                    }
                }
                foreach (var attach in getExtraAttachments)
                {
                    if (!string.IsNullOrEmpty(attach.ImagePath))
                    {
                        if (System.IO.Path.GetExtension(attach.ImagePath).ToUpperInvariant() != ".PDF")
                        {
                            //int index = attach.ImagePath.IndexOf("ItemFound");
                            //var requiredString = attach.ImagePath.Substring(index, (attach.ImagePath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            //var imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                            var retstringExtra = "<img onclick='rotateMe(this);' src='" + attach.ImagePath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                            listy.Add(retstringExtra);
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Lost", "getAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> saveFoundTimeData(string founddate, string receivedate, string foundT, string receiveT, string itemBrnd, string itemType, string recName, string finderName, string roomNumber
            , string itemColor, string itemStorage, string itemReference, string imgPath, string finderDepartment, string itemStatus
            , string locFound, string siteTransfer, string itemSubStorage, string itemSubType, string shelfLife, string uname)
        {
            var listy = new List<string>();
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var itemData = ItemFound.GetItemFoundByReference(itemReference, dbConnection);
                if (itemData == null)
                {

                    var customData = new ItemFound();
                    customData.Brand = itemBrnd;
                    customData.Colour = itemColor;
                    customData.CreatedBy = userinfo.Username;
                    customData.CreatedDate = CommonUtility.getDTNow();
                    var dtFound = DateTime.Parse(founddate + "," + foundT);
                    customData.DateFound = dtFound;
                    customData.FinderDepartment = finderDepartment;
                    customData.FinderName = finderName;
                    var newimgPath = imgPath.Replace('|', '\\');
                    customData.ImagePath = newimgPath;
                    customData.LocationFound = locFound;
                    var dtReceive = DateTime.Parse(receivedate + "," + receiveT);
                    customData.ReceiveDate = dtReceive.ToString();
                    customData.ReceiverName = recName;
                    customData.Reference = itemReference;
                    customData.RoomNumber = roomNumber;
                    customData.SiteId = userinfo.SiteId;
                    customData.Status = "Found";
                    customData.StorageLocation = itemStorage;
                    customData.Type = itemType;

                    customData.ShelfLife = shelfLife;
                    customData.SubStorageLocation = itemSubStorage;
                    customData.SubType = itemSubType;

                    customData.UpdatedBy = userinfo.Username;
                    customData.UpdatedDate = CommonUtility.getDTNow();
                    customData.TransferFromSiteId = Convert.ToInt32(siteTransfer);
                    customData.CustomerId = userinfo.CustomerInfoId;
                    var retId = ItemFound.InsertorUpdateItemFound(customData, dbConnection, dbConnectionAudit, true);
                    if (retId > 0)
                    {
                        retVal = "SUCCESS";
                        listy.Clear();
                        listy.Add(retVal);
                        listy.Add(retId.ToString());
                        listy.Add(itemBrnd);
                    }
                    else
                    {
                        retVal = "Error occured while trying to add item to system.";
                        listy.Clear();
                        listy.Add(retVal);
                    }
                }
                else
                {
                    retVal = "Item reference already in the system,generate new barcode.";
                    listy.Clear();
                    listy.Add(retVal);
                }

            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("saveFoundTimeData", "Lost.aspx", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add(retVal);
                retVal = ex.Message;
            }

            return listy;
        }

        [WebMethod]
        public static string saveReturnTimeData(string ownerNationality, string ownerNumber, string ownerName, string ownerAddress
            , string imgPath, string ownerTypeSelect, string ownerContact, string shipTracking, string ownerEmail
            , string ownerRoomNumber, string uname, string itemreference, string itemid, int eId, string siteTransfer)
        {
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var customData = new ItemOwner();
                customData.Address = ownerAddress;
                customData.Email = ownerEmail;
                customData.HandedBy = userinfo.Username;
                customData.MobileNo = ownerContact;
                customData.IdNumber = ownerNumber;
                customData.Name = ownerName;
                customData.Nationality = ownerNationality;
                customData.ReturnDate = CommonUtility.getDTNow();
                customData.RoomNumber = ownerRoomNumber;
                customData.ShippingNumber = shipTracking;
                customData.SiteId = userinfo.SiteId;
                customData.Type = ownerTypeSelect;

                if (!string.IsNullOrEmpty(siteTransfer))
                {
                    var site = Convert.ToInt32(siteTransfer);
                    if (site > 0)
                        customData.TransferSiteId = site;
                }

                customData.ItemFoundId = Convert.ToInt32(itemid);
                customData.ItemReference = itemreference;
                var newimgPath = imgPath.Replace('|', '\\');
                customData.IdCopy = newimgPath;

                customData.ImagePath = newimgPath;
                customData.UpdatedBy = userinfo.Username;
                customData.UpdatedDate = CommonUtility.getDTNow();
                customData.CreatedBy = userinfo.Username;
                customData.CreatedDate = CommonUtility.getDTNow();
                customData.CustomerId = userinfo.CustomerInfoId;



                var retId = ItemOwner.InsertorUpdateItemOwner(customData, dbConnection, dbConnectionAudit, true);
                if (retId > 0)
                {
                    var getFoundItem = ItemFound.GetItemFoundById(Convert.ToInt32(itemid), dbConnection);
                    getFoundItem.Status = "Return";
                    getFoundItem.UpdatedBy = userinfo.Username;
                    getFoundItem.UpdatedDate = CommonUtility.getDTNow();
                    ItemFound.InsertorUpdateItemFound(getFoundItem, dbConnection, dbConnectionAudit, true);

                    if (eId > 0)
                    {
                        var itemEnq = ItemInquiry.GetItemInquiryById(eId, dbConnection);
                        itemEnq.UpdatedBy = userinfo.Username;
                        itemEnq.UpdatedDate = CommonUtility.getDTNow();
                        itemEnq.ItemReference = itemreference;
                        itemEnq.ItemFoundId = Convert.ToInt32(itemid);
                        ItemInquiry.InsertorUpdateItemInquiry(itemEnq, dbConnection, dbConnectionAudit, true);
                    }

                    retVal = "SUCCESS";
                }
                else
                    retVal = "Error occured while trying to add item to system.";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("saveReturnTimeData", "Lost.aspx", ex, dbConnection, userinfo.SiteId);
                retVal = ex.Message;
            }
            return retVal;
        }

        [WebMethod]
        public static string saveDisposeItemData(string disposeSelect, string disposeName, string disposeCompany, string disposeLocation
            , string disposeContact, string disposeIDNumber, string sop, string disposeAfter, string disposeWitness
            , string imgPath, string uname, string itemreference, string itemid, int eId)
        {
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var customData = new ItemDispose();

                customData.DisposedAfter = disposeAfter;
                customData.DisposedBy = userinfo.Username;
                customData.DisposedDate = CommonUtility.getDTNow();
                customData.DisposedTo = disposeSelect;

                if (disposeSelect == "Destroyed")
                {
                    customData.Destroyed = sop;
                }
                if (imgPath != "undefined")
                {
                    var newimgPath = imgPath.Replace('|', '\\');
                    customData.ImagePath = newimgPath;
                }
                customData.ItemFoundId = Convert.ToInt32(itemid);
                customData.ItemReference = itemreference;
                customData.RecipientCompany = disposeCompany;
                customData.RecipientContactNo = disposeContact;
                customData.RecipientIdNo = disposeIDNumber;
                customData.RecipientLocation = disposeLocation;
                customData.RecipientName = disposeName;
                customData.Witness = disposeWitness;
                customData.SiteId = userinfo.SiteId;
                customData.UpdatedBy = userinfo.Username;
                customData.UpdatedDate = CommonUtility.getDTNow();
                customData.CreatedBy = userinfo.Username;
                customData.CreatedDate = CommonUtility.getDTNow();
                customData.CustomerId = userinfo.CustomerInfoId;


                var retId = ItemDispose.InsertorUpdateItemDispose(customData, dbConnection, dbConnectionAudit, true);
                if (retId > 0)
                {
                    var getFoundItem = ItemFound.GetItemFoundById(Convert.ToInt32(itemid), dbConnection);

                    if (disposeSelect == "Charity")
                    {
                        getFoundItem.Status = "Donate";
                    }
                    else
                        getFoundItem.Status = "Dispose";

                    getFoundItem.UpdatedBy = userinfo.Username;
                    getFoundItem.UpdatedDate = CommonUtility.getDTNow();
                    ItemFound.InsertorUpdateItemFound(getFoundItem, dbConnection, dbConnectionAudit, true);
                    if (eId > 0)
                    {
                        var itemEnq = ItemInquiry.GetItemInquiryById(eId, dbConnection);
                        itemEnq.UpdatedBy = userinfo.Username;
                        itemEnq.UpdatedDate = CommonUtility.getDTNow();
                        itemEnq.ItemReference = itemreference;
                        itemEnq.ItemFoundId = Convert.ToInt32(itemid);
                        ItemInquiry.InsertorUpdateItemInquiry(itemEnq, dbConnection, dbConnectionAudit, true);
                    }
                    retVal = "SUCCESS";
                }
                else
                    retVal = "Error occured while trying to add item to system.";


            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("saveReturnTimeData", "Lost.aspx", ex, dbConnection, userinfo.SiteId);
                retVal = ex.Message;
            }
            return retVal;
        }


        [WebMethod]
        public static string saveMultipleItemsGroup(string[] userIds, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var guid = Guid.NewGuid().ToString();
                    foreach (var ids in userIds)
                    {
                        var grpItem = new GroupItemFound();
                        grpItem.ItemFoundId = Convert.ToInt32(ids);
                        grpItem.ItemGroupId = guid;
                        grpItem.CreatedBy = userinfo.Username;
                        grpItem.CreatedDate = CommonUtility.getDTNow();
                        GroupItemFound.InsertorUpdateGroupItemFound(grpItem, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("LOST.aspx", "saveMultipleItemsGroup", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
            }
            else
            {
                return "Problem occured while trying to submit form";
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static List<string> getGroupItemsData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var itemGroups = ItemFound.GetAllItemFoundGroupByUserId(id, dbConnection);
                if (itemGroups.Count > 0)
                {
                    foreach (var item in itemGroups)
                    {
                        if (item.Id != id)
                            listy.Add(item.Id + "|" + item.Brand);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Lost", "getGroupItemsData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> saveItemEnquiryData(string itemBrnd, string locFound, string itemName, string itemColor
            , string itemType, string itemDesc, string itemAddress, string itemContact, string itemRoom, string subtype
            , string uname)
        {
            var listy = new List<string>();
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = new ItemInquiry();

                customData.Address = itemAddress;
                customData.Closed = false;
                customData.ContactNo = itemContact;
                customData.DateFound = CommonUtility.getDTNow();
                customData.Delivered = false;
                customData.DeliveredDate = CommonUtility.getDTNow();
                customData.Found = false;
                customData.ItemBrand = itemBrnd;
                customData.ItemColour = itemColor;
                customData.ItemDescription = itemDesc;
                customData.LocationLost = locFound;
                customData.Name = itemName;
                customData.RoomNumber = itemRoom;
                customData.Type = itemType;
                customData.SecurityTracking = userinfo.Username;
                customData.SiteId = userinfo.SiteId;
                customData.UpdatedBy = userinfo.Username;
                customData.UpdatedDate = CommonUtility.getDTNow();
                customData.CreatedBy = userinfo.Username;
                customData.CreatedDate = CommonUtility.getDTNow();
                if (!string.IsNullOrEmpty(subtype))
                {
                    if (!subtype.ToLower().Contains("please select"))
                    {
                        customData.SubType = subtype;
                    }
                }
                customData.CustomerId = userinfo.CustomerInfoId;
                var lastid = ItemInquiry.GetLastItemInquiryIdBySiteId(new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1), CommonUtility.getDTNow().AddHours(24), userinfo.SiteId, dbConnection);
                var getmonth = CommonUtility.getDTNow().Month;
                int getyear = CommonUtility.getDTNow().Year % 20;
                var retmonth = getmonth.ToString();
                if (getmonth < 10)
                    retmonth = "0" + getmonth.ToString();

                var retsiteid = userinfo.SiteId.ToString();
                if (userinfo.SiteId < 10)
                    retsiteid = "0" + userinfo.SiteId;
                var retstring = string.Empty;
                while (true)
                {
                    lastid = lastid + 1;
                    var retCount = (lastid).ToString();
                    var itemCount = (lastid);
                    if (itemCount.ToString().Length == 3)
                        retCount = "0" + itemCount.ToString();
                    else if (itemCount.ToString().Length == 2)
                        retCount = "00" + itemCount.ToString();
                    else if (itemCount.ToString().Length == 1)
                        retCount = "000" + itemCount.ToString();

                    retstring = "E" + retsiteid + retCount + retmonth + getyear.ToString();

                    var fItem = ItemInquiry.GetItemInquiryByReferenceNo(retstring, dbConnection);
                    if (fItem == null)
                    {
                        customData.ReferenceNo = retstring;
                        break;
                    }
                }
                var retId = ItemInquiry.InsertorUpdateItemInquiry(customData, dbConnection, dbConnectionAudit, true);
                if (retId > 0)
                {
                    retVal = retId.ToString();
                    listy.Add(retVal);
                    listy.Add(retstring);
                }
                else
                    listy.Clear();
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("saveReturnTimeData", "Lost.aspx", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowDataFound(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = ItemFound.GetItemFoundById(id, dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.Status.ToUpper() + " ITEM");
                    listy.Add(customData.FinderName);
                    listy.Add(customData.LocationFound);
                    listy.Add(customData.DateFound.ToString());
                    listy.Add(customData.Type);
                    listy.Add(customData.Brand);
                    listy.Add(customData.StorageLocation);
                    listy.Add(customData.Reference);
                    listy.Add(customData.FinderDepartment);
                    listy.Add(customData.ReceiverName);
                    listy.Add(customData.ReceiveDate);
                    listy.Add(customData.RoomNumber);
                    listy.Add(customData.Colour);

                    listy.Add(customData.SubType);
                    listy.Add(customData.SubStorageLocation);
                    listy.Add(customData.ShelfLife);

                    if (customData.Status.ToUpper() == "RETURN")
                    {
                        var returnData = ItemOwner.GetItemOwnerByReference(customData.Reference, dbConnection);
                        if (returnData != null)
                        {
                            listy.Add(returnData.ReturnDate.AddHours(userinfo.TimeZone).ToString());
                            listy.Add(returnData.Name);
                            listy.Add(returnData.Type);
                            listy.Add(returnData.RoomNumber);
                            listy.Add(returnData.Nationality);
                            listy.Add(returnData.Address);
                            listy.Add(returnData.MobileNo);
                            listy.Add(returnData.Email);
                            listy.Add(returnData.IdNumber);
                            listy.Add(returnData.ShippingNumber);
                            listy.Add(returnData.CustomerUName);//HandedBy
                            if (returnData.TransferSiteId > 0)
                            {
                                listy.Add("block");
                                listy.Add(returnData.TransferSiteName);
                            }
                            else
                            {
                                listy.Add("none");
                                listy.Add("N/A");
                            }

                        }
                    }
                    else if (customData.Status.ToUpper() == "DISPOSE" || customData.Status.ToUpper() == "DONATE")
                    {
                        var returnData = ItemDispose.GetItemDisposeByReference(customData.Reference, dbConnection);
                        if (returnData != null)
                        {
                            listy.Add(returnData.DisposedDate.AddHours(userinfo.TimeZone).ToString());
                            listy.Add(returnData.DisposedTo);
                            listy.Add(returnData.RecipientName);
                            listy.Add(returnData.RecipientCompany);
                            listy.Add(returnData.RecipientLocation);
                            listy.Add(returnData.RecipientContactNo);
                            listy.Add(returnData.RecipientIdNo);
                            listy.Add(returnData.Destroyed);
                            listy.Add(returnData.DisposedAfter);
                            listy.Add(returnData.CustomerUName); //DisposedBy
                            listy.Add(returnData.Witness);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTableRowDataFound", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowDataEnquiry(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = ItemInquiry.GetItemInquiryById(id, dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.ItemBrand);
                    listy.Add(customData.LocationLost);
                    listy.Add(customData.ItemColour);
                    listy.Add(customData.Type);
                    listy.Add(customData.ItemDescription);
                    listy.Add(customData.Name);
                    listy.Add(customData.Address);
                    listy.Add(customData.ContactNo);
                    listy.Add(customData.RoomNumber);
                    var foundCheck = false;
                    if (customData.Found)
                    {
                        foundCheck = true;
                    }

                    if (!string.IsNullOrEmpty(customData.ItemReference))
                    {
                        listy.Add(customData.ItemReference);
                        var itemRef = ItemFound.GetItemFoundByReference(customData.ItemReference, dbConnection);
                        listy.Add(itemRef.Status);

                        if (foundCheck)
                        {
                            listy.Add("<p>Search done and item has been processsed <a class='red-color' href='#' data-target='#foundItemViewCard'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + itemRef.Id + "&apos;) '> (" + itemRef.Reference + ")</a></p>");
                        }
                    }
                    else
                    {
                        listy.Add("N/A");
                        listy.Add("Not Found");
                        if (foundCheck)
                        {
                            listy.Add("<p>Search done but could not find item</p>");
                        }
                        else
                            listy.Add("<p>Search has not yet been done</p>");
                    }
                    listy.Add(customData.ReferenceNo);
                    listy.Add(customData.SubType);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getTableRowDataEnquiry", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getVehicleColorData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var alldata = new List<VehicleColor>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = VehicleColor.GetAllVehicleColorByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = VehicleColor.GetAllVehicleColorByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = VehicleColor.GetAllVehicleColor(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    alldata = VehicleColor.GetAllVehicleColorBySiteId(userinfo.SiteId, dbConnection);
                }


                foreach (var item in alldata)
                {
                    var action = "<a href='#' data-target='#editVehicleColorModal'  data-toggle='modal' onclick='rowchoiceVehicleColor(&apos;" + item.ColorCode + "&apos;,&apos;" + item.ColorDesc + "&apos;,&apos;" + item.ColorDesc_AR + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteVehColorModal'  data-toggle='modal' onclick='rowchoiceVehicleColor(&apos;" + item.ColorCode + "&apos;,&apos;" + item.ColorDesc + "&apos;,&apos;" + item.ColorDesc_AR + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.ColorDesc + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getVehicleColorData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string delVehColor(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {

                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    Arrowlabs.Business.Layer.VehicleColor.DeleteVehicleColorByColorCode(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", id.ToString(), id.ToString(), userinfo, "Delete Color" + id);
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "delVehColor", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
            }
            return listy;
        }

        [WebMethod]
        public static string delAssetCatSub(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.WarehouseAsset.DeleteWarehouseAssetbyId(id, dbConnection);

                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", id.ToString(), id.ToString(), userinfo, "Delete AssetSub" + id);
                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "delAssetCatSub", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteItemFound(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.ItemFound.DeleteItemFoundById(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "ItemFound-" + id, id.ToString(), "ItemFound", userinfo, "Lost and Found was deleted on_" + CommonUtility.getDTNow());


                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteItemFound", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteItemLost(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var itemLostData = ItemLost.GetItemLostById(id, dbConnection);
                    var msg = "Asset";

                    if (itemLostData.isKey)
                        msg = "Key";

                    var ret = Arrowlabs.Business.Layer.ItemLost.DeleteItemLostById(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, msg + "-" + id, id.ToString(), msg, userinfo, msg + " was deleted on_" + CommonUtility.getDTNow());
                        try
                        {
                            var atchments = Arrowlabs.Business.Layer.ItemLostAttachment.GetAllItemLostAttachments(dbConnection);
                            atchments = atchments.Where(i => i.ItemLostId == id).ToList();
                            foreach (var att in atchments)
                            {
                                CommonUtility.CloudDeleteFile(att.AttachmentPath);
                            }
                        }
                        catch { }
                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteItemLost", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteItemEnquiry(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.ItemInquiry.DeleteItemInquiryById(id, dbConnection);


                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "ItemInquiry-" + id, id.ToString(), "ItemInquiry", userinfo, "Enquiry was deleted on_" + CommonUtility.getDTNow());


                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteItemEnquiry", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteAttachmentDataAsset(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getRet = ItemLostAttachment.GetAllItemLostAttachments(dbConnection);
                    getRet = getRet.Where(i => i.Id == id).ToList();
                    if (getRet != null && getRet.Count > 0)
                    {
                        var ret = Arrowlabs.Business.Layer.ItemLostAttachment.DeleteItemLostAttachmentById(id, dbConnection);
                        if (ret)
                        {
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "ItemLostAttachment", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());

                            CommonUtility.CloudDeleteFile(getRet[0].AttachmentPath);

                            listy = "Successfully deleted entry";
                        }
                        else
                            listy = "Failed to delete entry";
                    }
                    else
                        listy = "Attachment already deleted";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteAttachmentDataAsset", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string deleteAttachmentData(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.LostAndFoundAttachments.DeleteLostAndFoundAttachmentsById(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "LostAndFoundAttachments", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());


                        listy = "Successfully deleted entry";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string DeleteAssetPlanById(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    AssetPlan.DeleteAssetPlanById(Convert.ToInt32(id), dbConnection);

                    SystemLogger.SaveSystemLog(dbConnectionAudit, "WarehouseP", id.ToString(), id.ToString(), userinfo, "Delete AssetPlan" + id);
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("WarehouseP", "DeleteAssetPlanById", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string delAssetCat(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var retCats = WarehouseAsset.GetAllWarehouseAsset(dbConnection);
                    foreach (var rets in retCats)
                    {
                        if (rets.AssetCategoryId == id)
                            Arrowlabs.Business.Layer.WarehouseAsset.DeleteWarehouseAssetbyId(rets.Id, dbConnection);
                    }
                    var ret = Arrowlabs.Business.Layer.WarehouseAssetCategory.DeleteWarehouseAssetCategorybyId(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", id.ToString(), id.ToString(), userinfo, "Delete AssetCat" + id);
                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "delAssetCat", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        

                    [WebMethod]
        public static string updateAssetItemData(string name, string comment, string asset
            , string location, string imgpath, string masset, string Loc, string barcode, string serialno
            , string uname, bool isKey, string accountid, string projectid, string assetmake, string assetmodel
            , string lvd, string assetcontractor, string aremarks, string assetid)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (!string.IsNullOrEmpty(assetid) && CommonUtility.isNumeric(assetid))
                    { 
                        var newItemLost = ItemLost.GetItemLostById(Convert.ToInt32(assetid), dbConnection);
                        if (newItemLost != null)
                        { 
                            newItemLost.Name = name;
                            newItemLost.Comments = comment; 
                            newItemLost.SiteId = userinfo.SiteId;
                            newItemLost.UpdatedBy = userinfo.Username;
                            newItemLost.UpdatedDate = CommonUtility.getDTNow();
                            newItemLost.isKey = isKey;
                            newItemLost.Barcode = barcode;
                            newItemLost.SerialNo = serialno;

                            //new fields
                            newItemLost.Contractor = assetcontractor;
                            newItemLost.AssetMake = assetmake;
                            newItemLost.AssetModel = assetmodel;
                            newItemLost.Remarks = aremarks;
                            try
                            {
                                newItemLost.LastVisitDate = Convert.ToDateTime(lvd);
                            }
                            catch (Exception ec)
                            {
                                MIMSLog.MIMSLogSave("WarehouseP", "newItemLost.LastVisitDate", ec, dbConnection, userinfo.SiteId);
                            }
                            if (!string.IsNullOrEmpty(accountid) && CommonUtility.isNumeric(accountid))
                                newItemLost.AccountId = Convert.ToInt32(accountid);

                            if (!string.IsNullOrEmpty(projectid) && CommonUtility.isNumeric(projectid))
                                newItemLost.ProjectId = Convert.ToInt32(projectid);
                            //new fields

                            var ploc = Location.GetAllLocationByLocationDescAndCId(Loc, userinfo.CustomerInfoId, dbConnection);
                            ploc = ploc.Where(i => i.ParentLocationId == 0).ToList();
                            if (ploc.Count > 0)
                            {
                                newItemLost.MainLocationId = ploc[0].ID;
                                newItemLost.Longitude = ploc[0].Longitude;
                                newItemLost.Latitude = ploc[0].Latitude;
                            }

                            var loc = Location.GetAllLocationByLocationDescAndCId(location, userinfo.CustomerInfoId, dbConnection);
                            loc = loc.Where(i => i.ParentLocationId > 0).ToList();
                            if (loc.Count > 0)
                            {
                                newItemLost.LocationId = loc[0].ID;
                                newItemLost.Longitude = loc[0].Longitude;
                                newItemLost.Latitude = loc[0].Latitude;
                            }

                            if (!string.IsNullOrEmpty(asset))
                                newItemLost.AssetCatId = Convert.ToInt32(asset);

                            newItemLost.CustomerId = userinfo.CustomerInfoId;
                            if (!string.IsNullOrEmpty(asset))
                                newItemLost.MainAssetCatId = Convert.ToInt32(masset);

                            newItemLost.CustomerId = userinfo.CustomerInfoId;

                            var retid = ItemLost.InsertOrUpdateItemLost(newItemLost, dbConnection, dbConnectionAudit, true);
 
                            listy = retid.ToString();//"SUCCESS";

                        }
                        else
                        {
                            listy = "Kindly change barcode it is already in use.";
                        }
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("WarehouseP", "updateAssetItemData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string saveAssetItemData(string name, string comment, string asset
            , string location, string imgpath, string masset, string Loc, string barcode,string serialno
            , string uname, bool isKey, string accountid, string projectid, string assetmake, string assetmodel
            , string lvd, string assetcontractor, string aremarks)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }
                    var iteml = ItemLost.GetItemLostByBarcode(barcode, dbConnection);
                    if (iteml == null || string.IsNullOrEmpty(barcode))
                    {
                        var newItemLost = new ItemLost();
                        newItemLost.Name = name;
                        newItemLost.Comments = comment;
                        newItemLost.CreatedBy = userinfo.Username;
                        newItemLost.CreatedDate = CommonUtility.getDTNow();
                        newItemLost.SiteId = userinfo.SiteId;
                        newItemLost.UpdatedBy = userinfo.Username;
                        newItemLost.UpdatedDate = CommonUtility.getDTNow();
                        newItemLost.isKey = isKey;
                        newItemLost.Barcode = barcode;
                        newItemLost.SerialNo = serialno;

                        //new fields
                        newItemLost.Contractor = assetcontractor;
                        newItemLost.AssetMake = assetmake;
                        newItemLost.AssetModel = assetmodel;
                        newItemLost.Remarks = aremarks;
                        try
                        {
                            newItemLost.LastVisitDate = Convert.ToDateTime(lvd);
                        }
                        catch (Exception ec)
                        {
                            MIMSLog.MIMSLogSave("WarehouseP", "newItemLost.LastVisitDate", ec, dbConnection, userinfo.SiteId);
                        }
                        if (!string.IsNullOrEmpty(accountid) && CommonUtility.isNumeric(accountid))
                            newItemLost.AccountId = Convert.ToInt32(accountid);

                        if (!string.IsNullOrEmpty(projectid) && CommonUtility.isNumeric(projectid))
                            newItemLost.ProjectId = Convert.ToInt32(projectid);
                        //new fields

                        var ploc = Location.GetAllLocationByLocationDescAndCId(Loc, userinfo.CustomerInfoId, dbConnection);
                        ploc = ploc.Where(i => i.ParentLocationId == 0).ToList();
                        if (ploc.Count > 0)
                        {
                            newItemLost.MainLocationId = ploc[0].ID;
                            newItemLost.Longitude = ploc[0].Longitude;
                            newItemLost.Latitude = ploc[0].Latitude;
                        }

                        var loc = Location.GetAllLocationByLocationDescAndCId(location, userinfo.CustomerInfoId, dbConnection);
                        loc = loc.Where(i => i.ParentLocationId > 0).ToList();
                        if (loc.Count > 0)
                        {
                            newItemLost.LocationId = loc[0].ID;
                            newItemLost.Longitude = loc[0].Longitude;
                            newItemLost.Latitude = loc[0].Latitude;
                        }

                        if (!string.IsNullOrEmpty(asset))
                            newItemLost.AssetCatId = Convert.ToInt32(asset);

                        newItemLost.CustomerId = userinfo.CustomerInfoId;
                        if (!string.IsNullOrEmpty(asset))
                            newItemLost.MainAssetCatId = Convert.ToInt32(masset);

                        newItemLost.CustomerId = userinfo.CustomerInfoId;

                        var retid = ItemLost.InsertOrUpdateItemLost(newItemLost, dbConnection, dbConnectionAudit, true);
                        if (imgpath.Contains('|'))
                        {
                            var itemLostA = new ItemLostAttachment();

                            var newimgPath = imgpath.Replace('|', '\\');

                            if (File.Exists(newimgPath))
                            {
                                Stream fs = File.OpenRead(newimgPath);
                                var getFileName = Path.GetFileName(newimgPath); 
                                getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                                var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                                if (savestring != "FAIL")
                                { 
                                    itemLostA.AttachmentPath = savestring; 
                                    itemLostA.ItemLostId = retid;
                                    itemLostA.CreatedBy = userinfo.Username;
                                    itemLostA.CreatedDate = CommonUtility.getDTNow();
                                    itemLostA.SiteId = userinfo.SiteId;
                                    itemLostA.CustomerId = userinfo.CustomerInfoId;

                                    itemLostA.UpdatedBy = userinfo.Username;
                                    itemLostA.UpdatedDate = CommonUtility.getDTNow(); 
                                    ItemLostAttachment.InsertOrUpdateItemLostAttachment(itemLostA, dbConnection, dbConnectionAudit, true);

                                }
                                else
                                {
                                    MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                                    listy = "Failed to upload file to cloud.";
                                }
                                if (fs != null)
                                {
                                    fs.Close();
                                }
                                if (File.Exists(newimgPath))
                                    File.Delete(newimgPath);
                            }
                            else
                            {
                                MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                                listy = "File trying to upload doesn't exist.";
                            }
                        }
                        listy = retid.ToString();//"SUCCESS";
                    }
                    else
                    {
                        listy = "Kindly change barcode it is already in use.";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "saveAssetItemData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string savePlanSchedule(string assetid, string planid, string[] sched, string schedtime, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var iplan = AssetPlan.GetAssetPlanById(Convert.ToInt32(planid), dbConnection);
                if (iplan != null)
                {

                    var aps = AssetPlanSchedule.GetAllAssetPlanScheduleByPlanId(iplan.Id, dbConnection);
                    aps = aps.Where(i => !i.isSent).ToList();
                    foreach (var s in aps)
                    {
                        AssetPlanSchedule.DeleteAssetPlanScheduleById(s.Id, dbConnection);
                    }

                    if (iplan.isRecurring)
                    {
                        List<string> calculatedDates = null;

                        if (iplan.StartDate.Value.Date < CommonUtility.getDTNow().Date)
                            iplan.StartDate = CommonUtility.getDTNow().Date;

                        calculatedDates = CommonUtility.GetDatesForRecurringTaskSchedule(iplan.Recurring, iplan.StartDate.Value,iplan.EndDate.Value);
                        foreach (var item in calculatedDates)
                        {
                            var selectedTaskStartDateTime = DateTime.Parse(item + "," + schedtime);
                            var scheduleitem = new AssetPlanSchedule();

                            if (!string.IsNullOrEmpty(assetid) && CommonUtility.isNumeric(assetid))
                                scheduleitem.AssetId = Convert.ToInt32(assetid);

                            scheduleitem.CreatedBy = userinfo.Username;
                            scheduleitem.CreatedDate = CommonUtility.getDTNow();
                            scheduleitem.CustomerId = userinfo.CustomerInfoId;
                            scheduleitem.PlanDateTime = selectedTaskStartDateTime;
                            scheduleitem.PlanId = Convert.ToInt32(planid);
                            scheduleitem.SiteId = userinfo.SiteId;
                            var retid = AssetPlanSchedule.InsertOrUpdateAssetPlanSchedule(scheduleitem, dbConnection, dbConnectionAudit, true);
                        }
                    }
                    else
                    {
                        foreach (var item in sched)
                        {
                            var dt = DateTime.Parse(item);

                            if (dt.Date >= CommonUtility.getDTNow().Date)
                            { 
                                var selectedTaskStartDateTime = DateTime.Parse(item + "," + schedtime);
                                var scheduleitem = new AssetPlanSchedule();

                                if (!string.IsNullOrEmpty(assetid) && CommonUtility.isNumeric(assetid))
                                    scheduleitem.AssetId = Convert.ToInt32(assetid);

                                scheduleitem.CreatedBy = userinfo.Username;
                                scheduleitem.CreatedDate = CommonUtility.getDTNow();
                                scheduleitem.CustomerId = userinfo.CustomerInfoId;
                                scheduleitem.PlanDateTime = selectedTaskStartDateTime;
                                scheduleitem.PlanId = Convert.ToInt32(planid);
                                scheduleitem.SiteId = userinfo.SiteId;
                                var retid = AssetPlanSchedule.InsertOrUpdateAssetPlanSchedule(scheduleitem, dbConnection, dbConnectionAudit, true);
                            }
                        }
                    }
                }
                json = "SUCCESS";
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("UsersDB", "saveDutyWizard", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }
 

        [WebMethod]
        public static string saveAssetSparePart(string name, string comment, string asset
    , string imgpath, string masset, string serialno
    , string uname, string assetmake, string assetmodel,string bcodes
    , string aremarks,string assetid)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    } 
                    var newItemLost = new ItemLost();
                    newItemLost.Name = name;
                    newItemLost.Comments = comment;
                    newItemLost.CreatedBy = userinfo.Username;
                    newItemLost.CreatedDate = CommonUtility.getDTNow();
                    newItemLost.SiteId = userinfo.SiteId;
                    newItemLost.UpdatedBy = userinfo.Username;
                    newItemLost.UpdatedDate = CommonUtility.getDTNow(); 
                    newItemLost.SerialNo = serialno;
                    newItemLost.Barcode = bcodes;
                    newItemLost.AssetMake = assetmake;
                    newItemLost.AssetModel = assetmodel;
                    //this is now quantity
                    newItemLost.Remarks = aremarks; 

                    if (!string.IsNullOrEmpty(asset))
                        newItemLost.AssetCatId = Convert.ToInt32(asset);

                    newItemLost.CustomerId = userinfo.CustomerInfoId;
                    if (!string.IsNullOrEmpty(asset))
                        newItemLost.MainAssetCatId = Convert.ToInt32(masset);

                    newItemLost.CustomerId = userinfo.CustomerInfoId;
                    if (!string.IsNullOrEmpty(assetid) && CommonUtility.isNumeric(assetid))
                    {
                        newItemLost.isSparePart = true;

                        newItemLost.SparePartParentId = Convert.ToInt32(assetid);

                        var parent = ItemLost.GetItemLostById(newItemLost.SparePartParentId, dbConnection);
                        newItemLost.LocationId = parent.LocationId;
                        newItemLost.MainLocationId = parent.MainLocationId;

                        newItemLost.AccountId = parent.AccountId;
                        newItemLost.ProjectId = parent.ProjectId;

                        newItemLost.Longitude = parent.Longitude;
                        newItemLost.Latitude = parent.Latitude;
                    }
                    var retid = ItemLost.InsertOrUpdateItemLost(newItemLost, dbConnection, dbConnectionAudit, true);
                    if (imgpath.Contains('|'))
                    {
                        var itemLostA = new ItemLostAttachment();

                        var newimgPath = imgpath.Replace('|', '\\');

                        if (File.Exists(newimgPath))
                        {
                            Stream fs = File.OpenRead(newimgPath);
                            var getFileName = Path.GetFileName(newimgPath);
                            getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                            var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                            if (savestring != "FAIL")
                            {
                                itemLostA.AttachmentPath = savestring;
                                itemLostA.ItemLostId = retid;
                                itemLostA.CreatedBy = userinfo.Username;
                                itemLostA.CreatedDate = CommonUtility.getDTNow();
                                itemLostA.SiteId = userinfo.SiteId;
                                itemLostA.CustomerId = userinfo.CustomerInfoId;

                                itemLostA.UpdatedBy = userinfo.Username;
                                itemLostA.UpdatedDate = CommonUtility.getDTNow();
                                ItemLostAttachment.InsertOrUpdateItemLostAttachment(itemLostA, dbConnection, dbConnectionAudit, true);

                            }
                            else
                            {
                                MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                                listy = "Failed to upload file to cloud.";
                            }
                            if (fs != null)
                            {
                                fs.Close();
                            }
                            if (File.Exists(newimgPath))
                                File.Delete(newimgPath);
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                            listy = "File trying to upload doesn't exist.";
                        }
                    }
                    listy = retid.ToString();//"SUCCESS";
                                             //}
                                             // else
                                             // {
                                             //     listy = "Kindly change barcode it is already in use.";
                                             //}
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "saveAssetSparePart", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string updateAssetSparePart(string name, string comment, string asset
, string imgpath, string masset, string serialno
, string uname, string assetmake, string assetmodel, string bcodes
, string aremarks, string assetid, string partid)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }
                    var newItemLost = ItemLost.GetItemLostById(Convert.ToInt32(partid),dbConnection);
                    if (newItemLost != null)
                    {
                        newItemLost.Name = name;
                        newItemLost.Comments = comment;
                        newItemLost.CreatedBy = userinfo.Username;
                        newItemLost.CreatedDate = CommonUtility.getDTNow();
                        newItemLost.SiteId = userinfo.SiteId;
                        newItemLost.UpdatedBy = userinfo.Username;
                        newItemLost.UpdatedDate = CommonUtility.getDTNow();
                        newItemLost.SerialNo = serialno;
                        newItemLost.Barcode = bcodes;
                        newItemLost.AssetMake = assetmake;
                        newItemLost.AssetModel = assetmodel;
                        //this is now quantity
                        newItemLost.Remarks = aremarks;

                        if (!string.IsNullOrEmpty(asset))
                            newItemLost.AssetCatId = Convert.ToInt32(asset);

                        newItemLost.CustomerId = userinfo.CustomerInfoId;
                        if (!string.IsNullOrEmpty(asset))
                            newItemLost.MainAssetCatId = Convert.ToInt32(masset);

                        newItemLost.CustomerId = userinfo.CustomerInfoId;
                        if (!string.IsNullOrEmpty(assetid) && CommonUtility.isNumeric(assetid))
                        {
                            newItemLost.isSparePart = true;

                            newItemLost.SparePartParentId = Convert.ToInt32(assetid);

                            var parent = ItemLost.GetItemLostById(newItemLost.SparePartParentId, dbConnection);
                            newItemLost.LocationId = parent.LocationId;
                            newItemLost.MainLocationId = parent.MainLocationId;

                            newItemLost.AccountId = parent.AccountId;
                            newItemLost.ProjectId = parent.ProjectId;

                            newItemLost.Longitude = parent.Longitude;
                            newItemLost.Latitude = parent.Latitude;
                        }
                        var retid = ItemLost.InsertOrUpdateItemLost(newItemLost, dbConnection, dbConnectionAudit, true);
                        if (imgpath.Contains('|'))
                        {
                            var itemLostA = new ItemLostAttachment();

                            var newimgPath = imgpath.Replace('|', '\\');

                            if (File.Exists(newimgPath))
                            {
                                Stream fs = File.OpenRead(newimgPath);
                                var getFileName = Path.GetFileName(newimgPath);
                                getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                                var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                                if (savestring != "FAIL")
                                {
                                    itemLostA.AttachmentPath = savestring;
                                    itemLostA.ItemLostId = retid;
                                    itemLostA.CreatedBy = userinfo.Username;
                                    itemLostA.CreatedDate = CommonUtility.getDTNow();
                                    itemLostA.SiteId = userinfo.SiteId;
                                    itemLostA.CustomerId = userinfo.CustomerInfoId;

                                    itemLostA.UpdatedBy = userinfo.Username;
                                    itemLostA.UpdatedDate = CommonUtility.getDTNow();
                                    ItemLostAttachment.InsertOrUpdateItemLostAttachment(itemLostA, dbConnection, dbConnectionAudit, true);

                                }
                                else
                                {
                                    MIMSLog.MIMSLogSave("Lost", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                                    listy = "Failed to upload file to cloud.";
                                }
                                if (fs != null)
                                {
                                    fs.Close();
                                }
                                if (File.Exists(newimgPath))
                                    File.Delete(newimgPath);
                            }
                            else
                            {
                                MIMSLog.MIMSLogSave("Lost", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                                listy = "File trying to upload doesn't exist.";
                            }
                        }
                        listy = retid.ToString();//"SUCCESS";
                                                 //}
                                                 // else
                                                 // {
                                                 //     listy = "Kindly change barcode it is already in use.";
                                                 //}
                    }
                    else
                    {
                        return "Problem occured trying to update";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("WarehouseP", "updateAssetSparePart", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string saveRemarksWP(int id, string notes, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var projinfo = ItemLost.GetItemLostById(id, dbConnection);
                if (projinfo != null)
                {
                    var newRemarks = new ItemFoundRemarks();
                    newRemarks.CreatedBy = userinfo.Username;
                    newRemarks.CreatedDate = CommonUtility.getDTNow();
                    newRemarks.ItemLostId = id;
                    newRemarks.Remarks = notes;
                    newRemarks.UpdatedBy = userinfo.Username;
                    newRemarks.UpdatedDate = CommonUtility.getDTNow(); 
                    newRemarks.CustomerId = userinfo.CustomerInfoId;
                    newRemarks.SiteId = userinfo.SiteId;
                    var retV = 0;
                    if (!string.IsNullOrEmpty(notes))
                    {
                        retV = ItemFoundRemarks.InsertOrUpdateItemFoundRemarks(newRemarks, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        retV = 1;
                    }
                    if (retV > 0)
                    {
                        json = "SUCCESS";
                    }
                    else
                    {
                        json = "Problem faced trying to save remarks";
                    }
                }
                else
                {
                    json = "Problem faced trying to get item and save remarks";
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "saveRemarksWP", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }


        [WebMethod]
        public static List<string> getAssetRemarksData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = ItemLost.GetItemLostById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = ItemFoundRemarks.GetItemFoundRemarksByItemLostId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Remarks) && !projinfo.isSparePart)
                    {
                        var nTR = new ItemFoundRemarks();
                        nTR.CustomerUName = projinfo.CustomerUName;
                        nTR.Remarks = projinfo.Remarks;
                        nTR.CreatedDate = projinfo.UpdatedDate;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        //var count = 0;
                        foreach (var task in remarksList)
                        {
                            var OGremarks = task.Remarks;
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                            
                            //if (count == 3)
                            //{
                            //    break;
                            //}
                            //count++;
                            //var OGremarks = task.Remarks;
                            //if (!string.IsNullOrEmpty(task.Remarks) && task.Remarks.Length > 50)
                            //{
                            //    task.Remarks = task.Remarks.Remove(50);
                            //    task.Remarks = task.Remarks + "...";
                            //}
                            //var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            //listy.Add(acceptedData);
                        }
                        //if (remarksList.Count > 3)
                        //{
                        //    var seeall = "<h5><span  class='line-center' onmouseover='style=&apos;cursor: pointer;' onclick='showAllRemarks(&apos;" + id + "&apos;)'>SEE ALL</span></h5>";
                        //    listy.Add(seeall);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "getAssetRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string processAssetItem(int id, string process, string chkname, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var lostItem = ItemLost.GetItemLostById(id, dbConnection);
                    if (lostItem != null)
                    {
                        var oldName = lostItem.OwnerName;
                        if (string.IsNullOrEmpty(oldName))
                            oldName = lostItem.UpdatedBy;

                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Asset-" + id, chkname, oldName, userinfo, "Checked " + process + "_" + CommonUtility.getDTNow());

                        if (process == "in")
                        {
                            lostItem.Lost = 0;
                            lostItem.CheckedinBy = chkname;
                        }
                        else if (process == "out")
                        {
                            lostItem.Lost = 1;
                        }
                        lostItem.OwnerName = chkname;
                        lostItem.UpdatedDate = CommonUtility.getDTNow();
                        lostItem.UpdatedBy = userinfo.Username;
                        lostItem.CustomerId = userinfo.CustomerInfoId;
                        ItemLost.InsertOrUpdateItemLost(lostItem, dbConnection, dbConnectionAudit, true);

                        listy = "SUCCESS";
                    }
                    else
                    {
                        listy = "Not able to find data entry of item";
                    }

                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "processAssetItem", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string editVehColor(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = VehicleColor.GetVehicleColorById(id, dbConnection);
                    if (off != null)
                    {
                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (off.ColorDesc != name)
                        {
                            var getData2 = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                oldValue = off.ColorDesc;
                                newValue = name;
                                off.ColorDesc = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "VEHICLE COLOR ColorDesc-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        if (off.ColorDesc_AR != nameAR)
                        {
                            oldValue = off.ColorDesc_AR;
                            newValue = nameAR;
                            off.ColorDesc_AR = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "VEHICLE COLOR ColorDesc_AR-" + id);
                        }
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        VehicleColor.InsertorUpdateVehicleColor(off, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Warehouse", "editVehColor", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string addVehColor(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }

                    if (getData == null)
                    {

                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = VehicleColor.GetAllVehicleColorByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.ColorDesc.ToLower() == name.ToLower())
                                        VehicleColor.DeleteVehicleColorByColorCode(getcat.ColorCode, dbConnection);
                                }
                            }
                        }

                        var off = new VehicleColor();
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.ColorDesc = name;
                        off.ColorDesc_AR = nameAR;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CustomerId = userinfo.CustomerInfoId;
                        off.SiteId = userinfo.SiteId;
                        VehicleColor.InsertorUpdateVehicleColor(off, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Warehouse", "addVehColor", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static List<string> searchForItemsTable(string searchItemTypeSelect, string dateFromCalendar, string FromTime, string dateToCalendar, string ToTime, string tbSearchBrand, string searchLocationSelect, string searchColorSelect, string searchItemSubSelect, string searchLocationStoreSelect, string searchSubStoreSelect, string searchShelfLifeSelect, string description, string uname, int eId)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<ItemFound>();
                var dtFrom = CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0));
                var dtTo = CommonUtility.getDTNow();

                if (!string.IsNullOrEmpty(dateFromCalendar) && !string.IsNullOrEmpty(FromTime) && !string.IsNullOrEmpty(dateToCalendar) && !string.IsNullOrEmpty(ToTime))
                {
                    dtFrom = DateTime.Parse(dateFromCalendar + "," + FromTime);
                    dtTo = DateTime.Parse(dateToCalendar).Add(new TimeSpan(23, 59, 0));
                }
                if (!string.IsNullOrEmpty(searchLocationSelect))
                {
                    if (searchLocationSelect.ToLower().Contains("please select"))
                        searchLocationSelect = "";
                }

                if (!string.IsNullOrEmpty(searchColorSelect))
                {
                    if (searchColorSelect.ToLower().Contains("please select"))
                        searchColorSelect = "";
                }

                if (!string.IsNullOrEmpty(searchItemSubSelect))
                {
                    if (searchItemSubSelect.ToLower().Contains("please select"))
                        searchItemSubSelect = "";
                }
                //string searchLocationStoreSelect, string searchSubStoreSelect, string searchShelfLifeSelect, 
                if (!string.IsNullOrEmpty(searchItemTypeSelect))
                {
                    if (searchItemTypeSelect.ToLower().Contains("please select"))
                        searchItemTypeSelect = "";
                }
                if (!string.IsNullOrEmpty(searchLocationStoreSelect))
                {
                    if (searchLocationStoreSelect.ToLower().Contains("please select"))
                        searchLocationStoreSelect = "";
                }
                if (!string.IsNullOrEmpty(searchSubStoreSelect))
                {
                    if (searchSubStoreSelect.ToLower().Contains("please select"))
                        searchSubStoreSelect = "";
                }
                if (!string.IsNullOrEmpty(searchShelfLifeSelect))
                {
                    if (searchShelfLifeSelect.ToLower().Contains("please select"))
                        searchShelfLifeSelect = "";
                }
                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allcustomEvents = ItemFound.SearchItemFoundSuperAdmin(searchItemTypeSelect, dtFrom.ToString(), dtTo.ToString(), tbSearchBrand, searchLocationSelect, searchColorSelect, searchItemSubSelect, searchLocationStoreSelect, searchSubStoreSelect, searchShelfLifeSelect, description, dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = ItemFound.SearchItemFoundSuperAdmin(searchItemTypeSelect, dtFrom.ToString(), dtTo.ToString(), tbSearchBrand, searchLocationSelect, searchColorSelect, searchItemSubSelect, searchLocationStoreSelect, searchSubStoreSelect, searchShelfLifeSelect, description, dbConnection);
                    allcustomEvents = allcustomEvents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                }
                else
                {
                    allcustomEvents = ItemFound.SearchItemFoundSuperAdmin(searchItemTypeSelect, dtFrom.ToString(), dtTo.ToString(), tbSearchBrand, searchLocationSelect, searchColorSelect, searchItemSubSelect, searchLocationStoreSelect, searchSubStoreSelect, searchShelfLifeSelect, description, dbConnection);
                    allcustomEvents = allcustomEvents.Where(i => (i.SiteId == userinfo.SiteId) || (i.CustomerId == userinfo.CustomerInfoId && i.SiteId == 0)).ToList();
                    //   allcustomEvents = ItemFound.SearchItemFound(searchItemTypeSelect, dtFrom.ToString(), dtTo.ToString(), tbSearchBrand, searchLocationSelect, searchColorSelect, searchItemSubSelect, searchLocationStoreSelect, searchSubStoreSelect, searchShelfLifeSelect, description, userinfo.SiteId, dbConnection);
                }
                //var usersList = CommonUtility.getUsersOfManagerDirector(userinfo);
                foreach (var item in allcustomEvents)
                {
                    //if (usersList.Count > 0)
                    //{
                    //    if (usersList.Contains(item.CreatedBy) || item.CreatedBy == userinfo.Username)
                    //    {
                    //        var returnstring = "<tr role='row' class='odd'><td >" + item.CreatedDate.ToString() + "</td><td>" + item.Reference + "</td><td>" + item.Status + "</td><td><a href='#' data-target='#foundItemViewCard'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                    //        listy.Add(returnstring);
                    //    }
                    //}
                    //else
                    //{
                    var returnstring = "<tr role='row' class='odd'><td >" + item.CreatedDate.ToString() + "</td><td>" + item.Reference + "</td><td>" + item.Status + "</td><td><a href='#' data-target='#foundItemViewCard'  data-toggle='modal' onclick='rowchoiceFound(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                    listy.Add(returnstring);
                    //}
                }
                if (eId > 0)
                {
                    var itemEnq = ItemInquiry.GetItemInquiryById(eId, dbConnection);
                    itemEnq.UpdatedBy = userinfo.Username;
                    itemEnq.UpdatedDate = CommonUtility.getDTNow();
                    //if (allcustomEvents.Count > 0)
                    itemEnq.Found = true;
                    // else
                    //  itemEnq.Found = false;

                    ItemInquiry.InsertorUpdateItemInquiry(itemEnq, dbConnection, dbConnectionAudit, true);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "searchForItemsTable", err, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }

        [WebMethod]
        public static string getGenerateAssetBarcode(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                listy = "FAIL";
                try
                {
                    //if(userinfo.RoleId == (int)Role.Manager ||userinfo.RoleId == (int)Role.Admin ||userinfo.RoleId == (int)Role.Admin )
                    //{
                    var lastid = ItemLost.GetLastItemLostIdBySiteId(new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1), CommonUtility.getDTNow().AddHours(24), userinfo.SiteId, dbConnection);
                    if (userinfo.SiteId == 0)
                    {
                        lastid = ItemLost.GetLastItemLostIdByCustomerId(new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1), CommonUtility.getDTNow().AddHours(24), userinfo.CustomerInfoId, dbConnection);
                    }
                    var getmonth = CommonUtility.getDTNow().Month;
                    int getyear = CommonUtility.getDTNow().Year % 20;
                    var retmonth = getmonth.ToString();

                    if (getmonth < 10)
                        retmonth = "0" + getmonth.ToString();

                    var prefix = "S";

                    var retsiteid = userinfo.SiteId.ToString();
                    if (userinfo.SiteId > 0)
                    {
                        if (userinfo.SiteId.ToString().Length == 1)
                            retsiteid = "00" + userinfo.SiteId;
                        else if (userinfo.SiteId.ToString().Length == 2)
                            retsiteid = "0" + userinfo.SiteId;
                    }
                    else
                    {
                        prefix = "C";
                        retsiteid = userinfo.CustomerInfoId.ToString();
                        if (userinfo.CustomerInfoId.ToString().Length == 1)
                            retsiteid = "00" + userinfo.CustomerInfoId;
                        else if (userinfo.CustomerInfoId.ToString().Length == 2)
                            retsiteid = "0" + userinfo.CustomerInfoId;
                    }
                    while (true)
                    {
                        lastid = lastid + 1;
                        var retCount = (lastid).ToString();

                        var itemCount = (lastid);

                        if (itemCount.ToString().Length == 4)
                            retCount = "0" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 3)
                            retCount = "00" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 2)
                            retCount = "000" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 1)
                            retCount = "0000" + itemCount.ToString();

                        var retstring = prefix + retsiteid + retCount + retmonth + getyear.ToString()+"A";
                        var fItem = ItemLost.GetItemLostByBarcode(retstring, dbConnection);
                        if (fItem == null)
                        {
                            listy = retstring;
                            break;
                        }
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "getGenerateAssetBarcode", err, dbConnection, userinfo.SiteId);
                    listy = "FAIL";
                }
                return listy;
            }
        }

        [WebMethod]
        public static string getGenerateSpareBarcode(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                listy = "FAIL";
                try
                {
                    //if(userinfo.RoleId == (int)Role.Manager ||userinfo.RoleId == (int)Role.Admin ||userinfo.RoleId == (int)Role.Admin )
                    //{
                    var lastid = ItemLost.GetLastItemLostIdBySiteId(new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1), CommonUtility.getDTNow().AddHours(24), userinfo.SiteId, dbConnection);
                    if (userinfo.SiteId == 0)
                    {
                        lastid = ItemLost.GetLastItemLostIdByCustomerId(new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1), CommonUtility.getDTNow().AddHours(24), userinfo.CustomerInfoId, dbConnection);
                    }
                    var getmonth = CommonUtility.getDTNow().Month;
                    int getyear = CommonUtility.getDTNow().Year % 20;
                    var retmonth = getmonth.ToString();

                    if (getmonth < 10)
                        retmonth = "0" + getmonth.ToString();

                    var prefix = "S";

                    var retsiteid = userinfo.SiteId.ToString();
                    if (userinfo.SiteId > 0)
                    {
                        if (userinfo.SiteId.ToString().Length == 1)
                            retsiteid = "00" + userinfo.SiteId;
                        else if (userinfo.SiteId.ToString().Length == 2)
                            retsiteid = "0" + userinfo.SiteId;
                    }
                    else
                    {
                        prefix = "C";
                        retsiteid = userinfo.CustomerInfoId.ToString();
                        if (userinfo.CustomerInfoId.ToString().Length == 1)
                            retsiteid = "00" + userinfo.CustomerInfoId;
                        else if (userinfo.CustomerInfoId.ToString().Length == 2)
                            retsiteid = "0" + userinfo.CustomerInfoId;
                    }
                    while (true)
                    {
                        lastid = lastid + 1;
                        var retCount = (lastid).ToString();

                        var itemCount = (lastid);

                        if (itemCount.ToString().Length == 4)
                            retCount = "0" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 3)
                            retCount = "00" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 2)
                            retCount = "000" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 1)
                            retCount = "0000" + itemCount.ToString();

                        var retstring = prefix + retsiteid + retCount + retmonth + getyear.ToString()+"S";
                        var fItem = ItemLost.GetItemLostByBarcode(retstring, dbConnection);
                        if (fItem == null)
                        {
                            listy = retstring;
                            break;
                        }
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "getGenerateSpareBarcode", err, dbConnection, userinfo.SiteId);
                    listy = "FAIL";
                }
                return listy;
            }
        }
        

        [WebMethod]
        public static string getGenerateBarcode(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                listy = "FAIL";
                try
                {
                    //if(userinfo.RoleId == (int)Role.Manager ||userinfo.RoleId == (int)Role.Admin ||userinfo.RoleId == (int)Role.Admin )
                    //{
                    var lastid = ItemFound.GetLastItemFoundIdBySiteId(new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1), CommonUtility.getDTNow().AddHours(24), userinfo.SiteId, dbConnection);

                    var getmonth = CommonUtility.getDTNow().Month;
                    int getyear = CommonUtility.getDTNow().Year % 20;
                    var retmonth = getmonth.ToString();
                    if (getmonth < 10)
                        retmonth = "0" + getmonth.ToString();

                    var retsiteid = userinfo.SiteId.ToString();
                    if (userinfo.SiteId < 10)
                        retsiteid = "0" + userinfo.SiteId;

                    while (true)
                    {
                        lastid = lastid + 1;
                        var retCount = (lastid).ToString();

                        var itemCount = (lastid);

                        if (itemCount.ToString().Length == 3)
                            retCount = "0" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 2)
                            retCount = "00" + itemCount.ToString();
                        else if (itemCount.ToString().Length == 1)
                            retCount = "000" + itemCount.ToString();

                        var retstring = retsiteid + retCount + retmonth + getyear.ToString();
                        var fItem = ItemFound.GetItemFoundByReference(retstring, dbConnection);
                        if (fItem == null)
                        {
                            listy = retstring;
                            break;
                        }
                    }
                    //}
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "getGenerateBarcode", err, dbConnection, userinfo.SiteId);
                    listy = "FAIL";
                }
                return listy;
            }
        }

        [WebMethod]
        public static List<string> searchBarcode(string id, string uname)
        {
            var listy = new List<string>();
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
                return listy;
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var customData = ItemFound.GetItemFoundByReference(id, dbConnection);

                    if (customData != null)
                    {
                        listy.Add(customData.FinderName);
                        listy.Add(customData.ReceiverName);
                        listy.Add(customData.Brand);
                        listy.Add(customData.RoomNumber);
                        listy.Add(customData.FinderDepartment);
                        listy.Add(customData.Type);
                        listy.Add(customData.LocationFound);
                        listy.Add(customData.Colour);
                        listy.Add(customData.StorageLocation);
                        listy.Add(customData.DateFound.ToString());
                        listy.Add(customData.ReceiveDate);
                        listy.Add(customData.ImagePath);

                        listy.Add(customData.SubType);
                        listy.Add(customData.SubStorageLocation);
                        listy.Add(customData.ShelfLife);
                    }
                    else
                    {
                        listy.Add("ERROR");
                        return listy;
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "searchBarcode", err, dbConnection, userinfo.SiteId);
                    listy.Add("FAIL");
                    return listy;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string tranferImages(string id, int newid, string uname)
        {
            var listy = new List<string>();
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
                return listy[0];
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var customData = ItemFound.GetItemFoundByReference(id, dbConnection);

                    if (customData != null)
                    {
                        var getExtraAttachments = new List<LostAndFoundAttachments>();

                        getExtraAttachments = LostAndFoundAttachments.GetAttachmentsByItemFoundId(customData.Id, dbConnection);

                        var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        if (customData.Status == "Return")
                        {
                            var returnData = ItemOwner.GetItemOwnerByReference(customData.Reference, dbConnection);
                            if (returnData != null)
                            {
                                var newObj = new LostAndFoundAttachments();
                                newObj.CreatedBy = userinfo.Username;
                                newObj.CreatedDate = CommonUtility.getDTNow();
                                newObj.ImagePath = returnData.IdCopy;
                                newObj.ItemFoundId = newid;
                                newObj.SiteId = userinfo.SiteId;
                                newObj.CustomerId = userinfo.CustomerInfoId;
                                newObj.UpdatedBy = userinfo.Username;
                                newObj.UpdatedDate = CommonUtility.getDTNow();
                                LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj, dbConnection);

                                var newObj2 = new LostAndFoundAttachments();
                                newObj2.CreatedBy = userinfo.Username;
                                newObj2.CreatedDate = CommonUtility.getDTNow();
                                newObj2.ImagePath = returnData.Signature;
                                newObj2.ItemFoundId = newid;
                                newObj2.SiteId = userinfo.SiteId;
                                newObj2.CustomerId = userinfo.CustomerInfoId;
                                newObj2.UpdatedBy = userinfo.Username;
                                newObj2.UpdatedDate = CommonUtility.getDTNow();
                                LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj2, dbConnection);
                            }
                        }
                        else if (customData.Status == "Dispose" || customData.Status == "Donate")
                        {
                            var returnData = ItemDispose.GetItemDisposeByReference(customData.Reference, dbConnection);
                            if (returnData != null)
                            {
                                var newObj = new LostAndFoundAttachments();
                                newObj.CreatedBy = userinfo.Username;
                                newObj.CreatedDate = CommonUtility.getDTNow();
                                newObj.ImagePath = returnData.ImagePath;
                                newObj.ItemFoundId = newid;
                                newObj.SiteId = userinfo.SiteId;
                                newObj.CustomerId = userinfo.CustomerInfoId;
                                newObj.UpdatedBy = userinfo.Username;
                                newObj.UpdatedDate = CommonUtility.getDTNow();
                                LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj, dbConnection);
                            }
                        }
                        foreach (var attach in getExtraAttachments)
                        {
                            if (!string.IsNullOrEmpty(attach.ImagePath))
                            {
                                if (System.IO.Path.GetExtension(attach.ImagePath).ToUpperInvariant() != ".PDF")
                                {
                                    var newObj = new LostAndFoundAttachments();
                                    newObj.CreatedBy = userinfo.Username;
                                    newObj.CreatedDate = CommonUtility.getDTNow();
                                    newObj.ImagePath = attach.ImagePath;
                                    newObj.ItemFoundId = newid;
                                    newObj.SiteId = userinfo.SiteId;
                                    newObj.CustomerId = userinfo.CustomerInfoId;
                                    newObj.UpdatedBy = userinfo.Username;
                                    newObj.UpdatedDate = CommonUtility.getDTNow();
                                    LostAndFoundAttachments.InsertorUpdateLostAndFoundAttachments(newObj, dbConnection);
                                }
                            }
                        }
                    }
                    else
                    {
                        listy.Add("ERROR");
                        return listy[0];
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "tranferImages", err, dbConnection, userinfo.SiteId);
                    listy.Add("FAIL");
                    return listy[0];
                }
                return listy[0];
            }
        }

        //Finder department

        [WebMethod]
        public static List<string> getFinderDepData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<FinderDepartment>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = FinderDepartment.GetAllFinderDepartmentByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents = FinderDepartment.GetAllFinderDepartmentByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = FinderDepartment.GetAllFinderDepartment(dbConnection);
                }
                else
                {
                    allcustomEvents = FinderDepartment.GetAllFinderDepartmentBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in allcustomEvents)
                {
                    var action = "<a href='#' data-target='#editFinderDepModal'  data-toggle='modal' onclick='rowchoiceFinderDep(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteFinderModal'  data-toggle='modal' onclick='rowchoiceFinderDep(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }
                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "getFinderDepData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string editFinderDepModal(int id, string name, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }


                    var off = FinderDepartment.GetFinderDepartmentById(id, dbConnection);
                    if (off != null)
                    {
                        if (off.Name != name)
                        {
                            var getData2 = FinderDepartment.GetFinderDepartmentByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = FinderDepartment.GetFinderDepartmentByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                var oldValue = off.Name;
                                var newValue = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", newValue, oldValue, userinfo, "Finder Department Settings change for id-" + id);
                                off.Name = name;
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        off.UpdatedDate = CommonUtility.getDTNow();
                        FinderDepartment.InsertOrUpdateFinderDepartment(off, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "editFinderDepModal", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
        public static string addFinderDep(int id, string name, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = FinderDepartment.GetFinderDepartmentByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = FinderDepartment.GetFinderDepartmentByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }
                    if (getData == null)
                    {
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = FinderDepartment.GetAllFinderDepartmentByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.Name.ToLower() == name.ToLower())
                                        FinderDepartment.DeleteFinderDepartmentById(getcat.Id, dbConnection);
                                }
                            }
                        }

                        var off = new FinderDepartment();
                        off.Name = name;
                        off.SiteId = userinfo.SiteId;
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CustomerId = userinfo.CustomerInfoId;
                        FinderDepartment.InsertOrUpdateFinderDepartment(off, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                    listy = "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "addFinderDep", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string delFinderDep(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.FinderDepartment.DeleteFinderDepartmentById(id, dbConnection);
                    if (ret)
                    {
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Lost", id.ToString(), id.ToString(), userinfo, "Delete FinderDepartment" + id);
                        listy = "SUCCESS";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Lost", "delFinderDep", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("WarehouseP.aspx", "saveTZ", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }

        [WebMethod]
        public static List<Project> getProjectListByCustomerId(int id, string uname)
        {
            var fullcollection = new List<Project>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    if (id > 0)
                    {

                        fullcollection.AddRange(Project.GetProjectByCustomerId(id, dbConnection));


                        //if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                        //{
                        //    fullcollection = fullcollection.Where(i => i.SiteId == userinfo.SiteId || i.CustomerInfoId == userinfo.CustomerInfoId).ToList();
                        //}]
                        //if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager)
                        // {
                        //fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();


                        if (userinfo.RoleId == (int)Role.Regional)
                        {
                            var guser = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            guser = guser.Where(i => i.RoleId == (int)Role.CustomerSuperadmin).ToList();
                            if (guser.Count > 0)
                            {
                                var sincidenttypes = fullcollection.Where(i => i.CreatedBy != guser[0].Username).ToList();

                                var tincidenttypes = fullcollection.Where(i => i.UserId == userinfo.ID).ToList();

                                tincidenttypes.AddRange(sincidenttypes);

                                fullcollection = tincidenttypes;
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username || i.SiteId == userinfo.SiteId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var slist = new List<Project>();
                            slist = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                            var manglist = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var mangid in manglist)
                            {
                                var mlist = new List<Project>();
                                mlist = fullcollection.Where(i => i.CreatedBy == mangid.Username).ToList();
                                slist.AddRange(mlist);
                            }
                            var grouped = slist.GroupBy(item => item.Id);
                            slist = grouped.Select(grp => grp.OrderBy(item => item.Name).First()).ToList();
                            slist = slist.OrderBy(i => i.Name).ToList();
                            fullcollection = slist;


                        }
                        else if (userinfo.RoleId == (int)Role.Manager)
                        {
                            fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                        }


                        fullcollection = fullcollection.Where(i => i.EndDate.Value.Date >= CommonUtility.getDTNow().Date).ToList();
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            fullcollection = new List<Project>();
                        }
                        // }

                    }
                    else
                    {

                        //fullcollection = Project.GetProjectByCustomerId(id, dbConnection);
                        if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                        {
                            fullcollection.AddRange(Project.GetAllProjectsBySiteId(userinfo.SiteId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            //  var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                            //  foreach (var site in sites)
                            //  {
                            fullcollection.AddRange(Project.GetAllProjectsByLevel5(userinfo.ID, dbConnection));
                            //  }
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            fullcollection.AddRange(Project.GetProjectByCustomerId(userinfo.CustomerLinkId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            fullcollection.AddRange(Project.GetAllProjectByCustomerInfoId(userinfo.CustomerInfoId, dbConnection));
                        }
                        else
                        {
                            fullcollection.AddRange(Project.GetAllProject(dbConnection));
                        }

                        if (userinfo.RoleId == (int)Role.Regional)
                        {
                            var guser = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            guser = guser.Where(i => i.RoleId == (int)Role.CustomerSuperadmin).ToList();
                            if (guser.Count > 0)
                            {
                                var sincidenttypes = fullcollection.Where(i => i.CreatedBy != guser[0].Username).ToList();

                                var tincidenttypes = fullcollection.Where(i => i.UserId == userinfo.ID).ToList();

                                tincidenttypes.AddRange(sincidenttypes);

                                fullcollection = tincidenttypes;
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username || i.SiteId == userinfo.SiteId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var slist = new List<Project>();
                            slist = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                            var manglist = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var mangid in manglist)
                            {
                                var mlist = new List<Project>();
                                mlist = fullcollection.Where(i => i.CreatedBy == mangid.Username).ToList();
                                slist.AddRange(mlist);
                            }
                            var grouped = slist.GroupBy(item => item.Id);
                            slist = grouped.Select(grp => grp.OrderBy(item => item.Name).First()).ToList();
                            slist = slist.OrderBy(i => i.Name).ToList();
                            fullcollection = slist;


                        }
                        else if (userinfo.RoleId == (int)Role.Manager)
                        {
                            fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                        }



                        var grouped2 = fullcollection.GroupBy(item => item.Id);
                        fullcollection = grouped2.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        fullcollection = fullcollection.OrderBy(i => i.Name).ToList();


                        //fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                        fullcollection = fullcollection.Where(i => i.EndDate.Value.Date >= CommonUtility.getDTNow().Date).ToList();
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            fullcollection = new List<Project>();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getProjectListByCustomerId", ex, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return fullcollection;
        }



        [WebMethod]
        public static List<Customer> getCustomerByProjectId(int id, string uname)
        {
            var fullcollection = new List<Customer>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    var selectAccountPlaceholder = "Select Account";
                    if (userinfo.ID == 479)
                    {
                        selectAccountPlaceholder = "هذا مثال على ذلك";
                    }
                    if (id > 0)
                    {

                        var cust = new Customer();
                        cust.Id = 0; cust.Id = 0;
                        cust.ClientName = selectAccountPlaceholder;//"Select Account";
                        cust.CreatedDate = CommonUtility.getDTNow();
                        cust.CreatedBy = userinfo.Username;
                        fullcollection.Add(cust);
                        var pproj = Project.GetProjectById(id, dbConnection);
                        if (pproj != null)
                        {
                            var custt = Customer.GetCustomerById(pproj.CustomerId, dbConnection);
                            if (custt != null)
                            {
                                if (custt.Status)
                                    fullcollection.Add(custt);
                            }
                            else
                            {
                                //var cust = new Customer();
                                //cust.Id = 0; cust.Id = 0;
                                //cust.ClientName = "Select Customer";
                                //cust.CreatedDate = DateTime.Now;
                                //cust.CreatedBy = userinfo.Username;
                                //fullcollection.Add(cust); 
                            }
                        }
                        else
                        {
                            //var cust = new Customer();
                            //cust.Id = 0; cust.Id = 0;
                            //cust.ClientName = "Select Customer";
                            //cust.CreatedDate = DateTime.Now;
                            //cust.CreatedBy = userinfo.Username;
                            //fullcollection.Add(cust); 
                        }
                    }
                    else
                    {
                        var cust = new Customer();
                        cust.Id = 0; cust.Id = 0;
                        cust.ClientName = selectAccountPlaceholder;//"Select Account";
                        cust.CreatedDate = CommonUtility.getDTNow();
                        cust.CreatedBy = userinfo.Username;
                        //fullcollection.Add(cust); 
                        var ffullcollection = new List<Customer>();
                        if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomersBySiteId(userinfo.SiteId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomersByLevel5(userinfo.ID, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomersByCustomerInfoId(userinfo.CustomerInfoId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            ffullcollection.Add(Customer.GetCustomerById(userinfo.CustomerLinkId, dbConnection));
                        }
                        else
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomers(dbConnection));
                        }

                        ffullcollection = ffullcollection.Where(i => i.Status == true).ToList();
                        fullcollection.Add(cust);
                        fullcollection.AddRange(ffullcollection);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getProjectListByCustomerId", ex, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return fullcollection;
        }



        [WebMethod]
        public static List<string> getCalendarDaysDuty(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var prevmonth = 0;
                var prevyear = 0;
                var dtyear = CommonUtility.getDTNow().Year;
                prevyear = CommonUtility.getDTNow().Year;
                if (id == 99)
                    id = CommonUtility.getDTNow().Month;
                else if (id > 12)
                {
                    id = 1;
                    prevyear = CommonUtility.getDTNow().AddYears(1).Year;
                    dtyear = CommonUtility.getDTNow().AddYears(1).Year;
                }
                else if (id == 0)
                    id = 12;

                prevmonth = id - 1;
                if (prevmonth == 0)
                {
                    prevmonth = 12;
                    prevyear = prevyear - 1;
                }

                var firstDayOfMonth = new DateTime(dtyear, id, 1);
                var previousMonth = DateTime.DaysInMonth(prevyear, prevmonth);
                var days = DateTime.DaysInMonth(dtyear, id);
                var startday = firstDayOfMonth.ToString("dddd");
                var adddays = 0;
                if (startday == "Monday")
                {
                    adddays = 1;
                }
                else if (startday == "Tuesday")
                {
                    adddays = 2;
                }
                else if (startday == "Wednesday")
                {
                    adddays = 3;
                }
                else if (startday == "Thursday")
                {
                    adddays = 4;
                }
                else if (startday == "Friday")
                {
                    adddays = 5;
                }
                else if (startday == "Saturday")
                {
                    adddays = 6;
                }

                var retString = string.Empty;
                var firstprev = false;
                for (var i = 1; i < days + 1 + adddays; i++)
                {
                    var IsEvent = "";
                    var today = CommonUtility.getDTNow();
                    var day = "";
                    if ((i - adddays) > 0)
                    {
                        day = (i - adddays).ToString();
                        if ((i - adddays) <= days)
                        {
                            var DayOfMonth = new DateTime(dtyear, id, (i - adddays));
                            //var reminder = Reminders.GetAllRemindersByDateAndCreator(DayOfMonth, userinfo.Username, dbConnection);
                            //if (reminder.Count > 0)
                            //    IsEvent = "event";
                        }
                        //if (today.Date == new DateTime(dtyear, id, (i - adddays)))
                        //    retString += "<td class='day " + IsEvent + "'><div class='day-contents' id='"+ id +"_"+ day +"' onclick='getDutyTableData(&apos;" + new DateTime(dtyear, id, (i - adddays)) + "&apos;,this)'>" + day + "</div></td>";
                        //else
                        retString += "<td class='day " + IsEvent + "'><div class='day-contents' id='" + id + "_" + day + "' onclick='getDutyTableData(&apos;" + new DateTime(dtyear, id, (i - adddays)) + "&apos;,this)'>" + day + "</div></td>";
                    }
                    else
                    {

                        if (!firstprev)
                        {
                            previousMonth = previousMonth - adddays + 1;
                            firstprev = true;
                        }
                        else { previousMonth = previousMonth + 1; }
                        var DayOfMonth = new DateTime(prevyear, prevmonth, previousMonth);
                        //var reminder = Reminders.GetAllRemindersByDateAndCreator(DayOfMonth, userinfo.Username, dbConnection);
                        //if (reminder.Count > 0)
                        //    IsEvent = "event";
                        retString += "<td class='day past adjacent-month last-month " + IsEvent + "'><div class='day-contents' id='" + id + "_" + day + "' onclick='getDutyTableData(&apos;" + DayOfMonth + "&apos;,this)'>" + previousMonth + "</div></td>";


                    }
                    if (i == 7 || i == 14 || i == 21 || i == 28 || (i == (days + adddays) || i == 35))
                    {
                        if (i == (days + adddays))
                        {
                            var endDayOfMonth = new DateTime(dtyear, id, days);
                            var endday = endDayOfMonth.ToString("dddd");
                            if (endday != "Saturday")
                                retString += "<td class='day '><div class='day-contents'></div></td>";

                        }
                        listy.Add(retString);
                        retString = string.Empty;
                    }
                    if (i == 35)
                        break;
                }
                listy.Add(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(id) + " " + dtyear.ToString());
                listy.Add(id.ToString());
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "getCalendarDays", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string insertUpdatePlan(string name, string tname, string desc, string assignType, string assignId
, string assigneeName, string startdate, string enddate
, string priority, string checklistid, string recurring, string lbrecurring, string uname, string tasktype
, string startT, string issig, string planid, string assetid, string myself
)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var newTask = AssetPlan.GetAssetPlanById(Convert.ToInt32(planid), dbConnection);
                    if (newTask != null)
                    {
                        newTask.PlanName = name;
                        newTask.TaskName = tname;
                        newTask.TaskDescription = desc;
                        var mselft = false;

                        if (!string.IsNullOrEmpty(myself))
                        {
                            mselft = Convert.ToBoolean(myself);
                        }
                        if (mselft)
                        {
                            newTask.AssigneeType = (int)TaskAssigneeType.User;
                            newTask.AssigneeId = userinfo.ID;
                        }
                        else
                        {
                            if (assignType == TaskAssigneeType.User.ToString())
                            {
                                newTask.AssigneeType = (int)TaskAssigneeType.User;
                                newTask.AssigneeId = Convert.ToInt32(assignId);
                                var guser = Users.GetUserById(newTask.AssigneeId, dbConnection);
                                if (guser != null)
                                    newTask.AssigneeName = guser.Username;
                            }
                            else if (assignType == TaskAssigneeType.Group.ToString())
                            {
                                newTask.AssigneeType = (int)TaskAssigneeType.Group;
                                newTask.AssigneeId = Convert.ToInt32(assignId);

                                var guser = Group.GetGroupById(newTask.AssigneeId, dbConnection);
                                if (guser != null)
                                    newTask.AssigneeName = guser.Name;
                            }
                        }
                        newTask.PlanTime = startT;
                        try
                        {
                            if (!string.IsNullOrEmpty(recurring))
                            {
                                if (Convert.ToBoolean(recurring))
                                {
                                    newTask.isRecurring = true;
                                    newTask.Recurring = lbrecurring;
                                }
                            }

                            newTask.StartDate = Convert.ToDateTime(startdate);
                            newTask.EndDate = Convert.ToDateTime(enddate);

                            //newTask.isTemplate = Convert.ToBoolean(ttemplate);
                            newTask.ChecklistId = Convert.ToInt32(checklistid);

                            //if (!string.IsNullOrEmpty(assetid) && CommonUtility.isNumeric(assetid))
                            //{
                            //    newTask.AssetId = Convert.ToInt32(assetid);
                            //    var ni = ItemLost.GetItemLostById(newTask.AssetId, dbConnection);
                            //    if (ni != null)
                            //    {
                            //        newTask.ProjectId = ni.ProjectId;
                            //        newTask.CustId = ni.AccountId;

                            //        if (ni.LocationId > 0)
                            //        {
                            //            var loc = Location.GetLocationById(ni.LocationId, dbConnection);
                            //            if (loc != null)
                            //            {
                            //                newTask.Longitude = loc.Longitude;
                            //                newTask.Latitude = loc.Latitude;
                            //            }
                            //        }
                            //        else if (ni.MainLocationId > 0)
                            //        {
                            //            var loc = Location.GetLocationById(ni.MainLocationId, dbConnection);
                            //            if (loc != null)
                            //            {
                            //                newTask.Longitude = loc.Longitude;
                            //                newTask.Latitude = loc.Latitude;
                            //            }
                            //        }
                            //    }
                            //}
                            newTask.isSignatures = Convert.ToBoolean(issig);

                            newTask.TaskType = Convert.ToInt32(tasktype);
                        }
                        catch (Exception e)
                        {

                        }
                        newTask.ManagerId = userinfo.ID;
                        newTask.CustomerId = userinfo.CustomerInfoId;
                        newTask.SiteId = userinfo.SiteId;
                       // newTask.CreateDate = CommonUtility.getDTNow();
                        newTask.UpdatedDate = CommonUtility.getDTNow();
                      //  newTask.CreatedBy = userinfo.Username;


                        if (priority == TaskPiority.Severe.ToString())
                            newTask.Priority = (int)TaskPiority.Severe;
                        else if (priority == TaskPiority.High.ToString())
                            newTask.Priority = (int)TaskPiority.High;
                        else if (priority == TaskPiority.Medium.ToString())
                            newTask.Priority = (int)TaskPiority.Medium;
                        else if (priority == TaskPiority.Low.ToString())
                            newTask.Priority = (int)TaskPiority.Low;

                        var rid = AssetPlan.InsertorUpdateAssetPlan(newTask, dbConnection, dbConnectionAudit, true);
                        return newTask.AssetId.ToString();
                    }
                    else
                    {
                        return "Problem occured retrieving plan information for updatation";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks", "insertUpdatePlan", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }

        [WebMethod]
        public static string insertSavePlan(string name, string tname, string desc, string assignType, string assignId
    , string assigneeName,  string startdate,string enddate
    , string priority, string checklistid,string recurring,string lbrecurring, string uname, string tasktype
    , string startT, string issig, string ttemplate, string assetid, string myself
    ) 
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }
                     
                    var newTask = new AssetPlan();
                    newTask.PlanName = name;
                    newTask.TaskName = tname;
                    newTask.TaskDescription = desc;
                    var mselft = false;

                    if (!string.IsNullOrEmpty(myself))
                    {
                        mselft = Convert.ToBoolean(myself);
                    }
                    if (mselft)
                    {
                        newTask.AssigneeType = (int)TaskAssigneeType.User; 
                        newTask.AssigneeId = userinfo.ID;
                    }
                    else
                    {
                        if (assignType == TaskAssigneeType.User.ToString())
                        {
                            newTask.AssigneeType = (int)TaskAssigneeType.User;
                            newTask.AssigneeId = Convert.ToInt32(assignId);
                            var guser = Users.GetUserById(newTask.AssigneeId, dbConnection);
                            if (guser != null)
                                newTask.AssigneeName = guser.Username;
                        }
                        else if (assignType == TaskAssigneeType.Group.ToString())
                        {
                            newTask.AssigneeType = (int)TaskAssigneeType.Group;
                            newTask.AssigneeId = Convert.ToInt32(assignId);

                            var guser = Group.GetGroupById(newTask.AssigneeId, dbConnection);
                            if (guser != null)
                                newTask.AssigneeName = guser.Name;
                        }
                    }
                    newTask.PlanTime = startT;
                    try
                    {
                        if (!string.IsNullOrEmpty(recurring))
                        {
                            if (Convert.ToBoolean(recurring))
                            {
                                newTask.isRecurring = true;
                                newTask.Recurring = lbrecurring;
                            }
                        }

                        newTask.StartDate = Convert.ToDateTime(startdate);
                        newTask.EndDate = Convert.ToDateTime(enddate);

                        newTask.isTemplate = Convert.ToBoolean(ttemplate);
                        newTask.ChecklistId = Convert.ToInt32(checklistid);

                        if (!string.IsNullOrEmpty(assetid) && CommonUtility.isNumeric(assetid))
                        {
                            newTask.AssetId = Convert.ToInt32(assetid);
                            var ni = ItemLost.GetItemLostById(newTask.AssetId, dbConnection);
                            if (ni != null)
                            {
                                newTask.ProjectId = ni.ProjectId;
                                newTask.CustId = ni.AccountId;

                                if (ni.LocationId > 0)
                                {
                                    var loc = Location.GetLocationById(ni.LocationId, dbConnection);
                                    if (loc != null)
                                    {
                                        newTask.Longitude = loc.Longitude;
                                        newTask.Latitude = loc.Latitude;
                                    }
                                }
                                else if (ni.MainLocationId > 0)
                                {
                                    var loc = Location.GetLocationById(ni.MainLocationId, dbConnection);
                                    if (loc != null)
                                    {
                                        newTask.Longitude = loc.Longitude;
                                        newTask.Latitude = loc.Latitude;
                                    }
                                }
                            }
                        }
                        newTask.isSignatures = Convert.ToBoolean(issig);

                        newTask.TaskType = Convert.ToInt32(tasktype);
                    }
                    catch (Exception e)
                    {

                    }
                    newTask.ManagerId = userinfo.ID;
                    newTask.CustomerId = userinfo.CustomerInfoId;
                    newTask.SiteId = userinfo.SiteId; 
                    newTask.CreateDate = CommonUtility.getDTNow();
                    newTask.UpdatedDate = CommonUtility.getDTNow();
                    newTask.CreatedBy = userinfo.Username; 
              

                    if (priority == TaskPiority.Severe.ToString())
                        newTask.Priority = (int)TaskPiority.Severe;
                    else if (priority == TaskPiority.High.ToString())
                        newTask.Priority = (int)TaskPiority.High;
                    else if (priority == TaskPiority.Medium.ToString())
                        newTask.Priority = (int)TaskPiority.Medium;
                    else if (priority == TaskPiority.Low.ToString())
                        newTask.Priority = (int)TaskPiority.Low;

                    var rid = AssetPlan.InsertorUpdateAssetPlan(newTask, dbConnection, dbConnectionAudit, true);
                    return rid.ToString();
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks", "insertSavePlan", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }


        [WebMethod]
        public static List<string> plantaskViewTable(string id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var fullcollection = new List<UserTask>();
                if (userinfo != null)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (CommonUtility.isNumeric(id))
                        {
                            fullcollection = UserTask.GetAllTaskByPlanId(Convert.ToInt32(id), dbConnection);

                            if (userinfo.RoleId == (int)Role.CustomerUser)
                            {
                                if (userinfo.ContractLinkId > 0)
                                {
                                    fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                                }
                            }

                            var grouped = fullcollection.GroupBy(item => item.Id);
                            fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                            var datetodisplay = string.Empty;
                            var imageclass = string.Empty;
                            foreach (var item in fullcollection)
                            {
                                if (!item.IsTaskTemplate)
                                {

                                    if (userinfo.RoleId != (int)Role.SuperAdmin)//userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                                    {

                                    }

                                    if (item.IsRecurring && item.RecurringParentId == 0)
                                    {

                                    }
                                    else
                                    {
                                        if (item.StartDate.Value.AddHours(userinfo.TimeZone).Date <= CommonUtility.getDTNow().Date)
                                        {
                                            // datetodisplay = item.StartDate.Value.AddHours(userinfo.TimeZone).ToString();

                                            if (item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                                            {
                                                if (item.Status == (int)TaskStatus.RejectedSaved)
                                                    item.StatusDescription = "Rejected";

                                                //if (item.Status == (int)TaskStatus.InProgress)
                                                //{
                                                //    if (item.ActualStartDate != null)
                                                //    {
                                                //        datetodisplay = item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                //    }
                                                //}
                                                //else if (item.Status == (int)TaskStatus.Completed)
                                                //{
                                                //    if (item.ActualEndDate != null)
                                                //    {
                                                //        datetodisplay = item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                //    }
                                                //}

                                                if (item.StatusDescription == "Completed" && item.FollowUp)
                                                    item.StatusDescription = "Follow Up";

                                                imageclass = CommonUtility.getImgStatusPriority(item.Priority);
                                                var returnstring = "<tr role='row' class='odd'><td>" + item.NewCustomerTaskId + "</td><td>" + item.Name + "</td><td>" + item.StatusDescription + "</td><td>" + item.TaskTypeName + "</td><td><a href='#' data-toggle='modal' data-target='#taskDocument'  data-dismiss='modal' onclick='showTaskDocument(&apos;" + item.Id + "&apos;)' ><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                                listy.Add(returnstring);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UsersDB", "cusViewTable", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> plantaskViewTableByDate(string id, string date, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var fullcollection = new List<UserTask>();
                if (userinfo != null)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (CommonUtility.isNumeric(id) && !string.IsNullOrEmpty(date))
                        {
                            fullcollection = UserTask.GetAllTaskByPlanId(Convert.ToInt32(id), dbConnection);

                            if (userinfo.RoleId == (int)Role.CustomerUser)
                            {
                                if (userinfo.ContractLinkId > 0)
                                {
                                    fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                                }
                            }

                            var dtfilter = Convert.ToDateTime(date);

                            fullcollection = fullcollection.Where(i => i.StartDate.Value.AddHours(userinfo.TimeZone).Date == dtfilter.Date).ToList();

                            var grouped = fullcollection.GroupBy(item => item.Id);
                            fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                            var datetodisplay = string.Empty;
                            var imageclass = string.Empty;
                            foreach (var item in fullcollection)
                            {
                                if (!item.IsTaskTemplate)
                                {

                                    if (userinfo.RoleId != (int)Role.SuperAdmin)//userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                                    {

                                    }

                                    if (item.IsRecurring && item.RecurringParentId == 0)
                                    {

                                    }
                                    else
                                    {
                                        if (item.StartDate.Value.AddHours(userinfo.TimeZone).Date == dtfilter.Date)
                                        {
                                            if (item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                                            {
                                                if (item.Status == (int)TaskStatus.RejectedSaved)
                                                    item.StatusDescription = "Rejected";
                                                
                                                if (item.StatusDescription == "Completed" && item.FollowUp)
                                                    item.StatusDescription = "Follow Up";

                                                imageclass = CommonUtility.getImgStatusPriority(item.Priority);
                                                var returnstring = "<tr role='row' class='odd'><td>" + item.NewCustomerTaskId + "</td><td>" + item.Name + "</td><td>" + item.StatusDescription + "</td><td>" + item.TaskTypeName + "</td><td><a href='#' data-toggle='modal' data-target='#taskDocument'  data-dismiss='modal' onclick='showTaskDocument(&apos;" + item.Id + "&apos;)' ><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                                listy.Add(returnstring);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "cusViewTableByDate", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> plantaskViewTable2(string id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var fullcollection = new List<UserTask>();
                if (userinfo != null)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (CommonUtility.isNumeric(id))
                        {
                            fullcollection = UserTask.GetAllTaskByPlanId(Convert.ToInt32(id), dbConnection);

                            if (userinfo.RoleId == (int)Role.CustomerUser)
                            {
                                if (userinfo.ContractLinkId > 0)
                                {
                                    fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                                }
                            }

                            var grouped = fullcollection.GroupBy(item => item.Id);
                            fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                            var datetodisplay = string.Empty;
                            var imageclass = string.Empty;
                            foreach (var item in fullcollection)
                            {
                                if (!item.IsTaskTemplate)
                                {

                                    if (userinfo.RoleId != (int)Role.SuperAdmin)//userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                                    {

                                    }

                                    if (item.IsRecurring && item.RecurringParentId == 0)
                                    {

                                    }
                                    else
                                    {
                                        if (item.StartDate.Value.AddHours(userinfo.TimeZone).Date <= CommonUtility.getDTNow().Date)
                                        {
                                            // datetodisplay = item.StartDate.Value.AddHours(userinfo.TimeZone).ToString();

                                            if (item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                                            {
                                                if (item.Status == (int)TaskStatus.RejectedSaved)
                                                    item.StatusDescription = "Rejected";

                                                //if (item.Status == (int)TaskStatus.InProgress)
                                                //{
                                                //    if (item.ActualStartDate != null)
                                                //    {
                                                //        datetodisplay = item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                //    }
                                                //}
                                                //else if (item.Status == (int)TaskStatus.Completed)
                                                //{
                                                //    if (item.ActualEndDate != null)
                                                //    {
                                                //        datetodisplay = item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                //    }
                                                //}

                                                if (item.StatusDescription == "Completed" && item.FollowUp)
                                                    item.StatusDescription = "Follow Up";

                                                imageclass = CommonUtility.getImgStatusPriority(item.Priority);
                                                var returnstring = "<tr role='row' class='odd'><td>" + item.NewCustomerTaskId + "</td><td>" + item.Name + "</td><td>" + item.StatusDescription + "</td><td>" + item.TaskTypeName + "</td><td><a href='#' data-toggle='modal' data-target='#taskDocument'  data-dismiss='modal' onclick='showTaskDocument2(&apos;" + item.Id + "&apos;)' ><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                                listy.Add(returnstring);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UsersDB", "plantaskViewTable2", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> plantaskViewTableByDate2(string id, string date, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var fullcollection = new List<UserTask>();
                if (userinfo != null)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (CommonUtility.isNumeric(id) && !string.IsNullOrEmpty(date))
                        {
                            fullcollection = UserTask.GetAllTaskByPlanId(Convert.ToInt32(id), dbConnection);

                            if (userinfo.RoleId == (int)Role.CustomerUser)
                            {
                                if (userinfo.ContractLinkId > 0)
                                {
                                    fullcollection = fullcollection.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                                }
                            }

                            var dtfilter = Convert.ToDateTime(date);

                            fullcollection = fullcollection.Where(i => i.StartDate.Value.AddHours(userinfo.TimeZone).Date == dtfilter.Date).ToList();

                            var grouped = fullcollection.GroupBy(item => item.Id);
                            fullcollection = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                            var datetodisplay = string.Empty;
                            var imageclass = string.Empty;
                            foreach (var item in fullcollection)
                            {
                                if (!item.IsTaskTemplate)
                                {

                                    if (userinfo.RoleId != (int)Role.SuperAdmin)//userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                                    {

                                    }

                                    if (item.IsRecurring && item.RecurringParentId == 0)
                                    {

                                    }
                                    else
                                    {
                                        if (item.StartDate.Value.AddHours(userinfo.TimeZone).Date == dtfilter.Date)
                                        {
                                            if (item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                                            {
                                                if (item.Status == (int)TaskStatus.RejectedSaved)
                                                    item.StatusDescription = "Rejected";

                                                if (item.StatusDescription == "Completed" && item.FollowUp)
                                                    item.StatusDescription = "Follow Up";

                                                imageclass = CommonUtility.getImgStatusPriority(item.Priority);
                                                var returnstring = "<tr role='row' class='odd'><td>" + item.NewCustomerTaskId + "</td><td>" + item.Name + "</td><td>" + item.StatusDescription + "</td><td>" + item.TaskTypeName + "</td><td><a href='#' data-toggle='modal' data-target='#taskDocument'  data-dismiss='modal' onclick='showTaskDocument2(&apos;" + item.Id + "&apos;)' ><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                                listy.Add(returnstring);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "plantaskViewTableByDate2", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }




        //TASKS
        [WebMethod]
        public static List<string> getTableRowDataTask(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.CustomerUName);
                    listy.Add(customData.CreateDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.ACustomerUName);
                    listy.Add(customData.StatusDescription);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geolocation);
                    listy.Add(customData.Name);
                    listy.Add(customData.Name);
                    listy.Add(customData.Name);
                    listy.Add(customData.Description);
                    listy.Add(customData.Notes);
                    listy.Add(customData.Name);
                    listy.Add(customData.StartDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.CheckListNotes);
                    listy.Add("circle-point " + CommonUtility.getImgStatus(customData.StatusDescription));
                    var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(customData.TemplateCheckListId.ToString(), dbConnection);
                    if (parentTasks != null)
                        listy.Add(parentTasks.Name);
                    else
                        listy.Add("None");

                    listy.Add(customData.TaskTypeName);

                    if (customData.IncidentId > 0)
                    {
                        var getCus = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                        if (getCus != null)
                        {
                            if (getCus.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus.Identifier, dbConnection);
                                if (mobEv != null)
                                {
                                    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                    {

                                    }
                                    else
                                        getCus.Name = mobEv.EventTypeName;
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.HotEvent || getCus.EventType == CustomEvent.EventTypes.Request)
                            {
                                var reqEv = HotEvent.GetHotEventById(getCus.Identifier, dbConnection);
                                if (reqEv != null)
                                {
                                    if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                        getCus.UserName = CommonUtility.getPCName(reqEv);

                                    if (getCus.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var splitName = reqEv.Name.Split('^');
                                        if (splitName.Length > 1)
                                            getCus.Name = splitName[1];
                                    }
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.DriverOffence)
                            {
                                var doffence = DriverOffence.GetDriverOffenceById(getCus.Identifier, dbConnection);
                                if (doffence != null)
                                {
                                    getCus.Name = doffence.OffenceCategory;
                                }
                            }
                            listy.Add(getCus.Name + "|" + customData.IncidentId);


                        }
                        else
                        {
                            listy.Add("None");
                        }
                    }
                    else
                    {
                        if (customData.TaskLinkId > 0)
                        {
                            var gt = UserTask.GetTaskById(customData.TaskLinkId, dbConnection);
                            if (gt != null)
                            {
                                if (gt.IncidentId > 0)
                                {
                                    var getCus2 = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                                    if (getCus2 != null)
                                    {
                                        if (getCus2.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus2.Identifier, dbConnection);
                                            if (mobEv != null)
                                            {
                                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                                {

                                                }
                                                else
                                                    getCus2.Name = mobEv.EventTypeName;
                                            }
                                        }
                                        else if (getCus2.EventType == CustomEvent.EventTypes.HotEvent || getCus2.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(getCus2.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                                    getCus2.UserName = CommonUtility.getPCName(reqEv);

                                                if (getCus2.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    var splitName = reqEv.Name.Split('^');
                                                    if (splitName.Length > 1)
                                                        getCus2.Name = splitName[1];
                                                }
                                            }
                                        }
                                        else if (getCus2.EventType == CustomEvent.EventTypes.DriverOffence)
                                        {
                                            var doffence = DriverOffence.GetDriverOffenceById(getCus2.Identifier, dbConnection);
                                            if (doffence != null)
                                            {
                                                getCus2.Name = doffence.OffenceCategory;
                                            }
                                        }
                                        listy.Add(getCus2.Name + "|" + customData.IncidentId);
                                    }
                                    else
                                    {
                                        listy.Add("None");
                                    }
                                }
                                else
                                {
                                    listy.Add("None");
                                }
                            }
                            else
                            {
                                listy.Add("None");
                            }
                        }
                        else
                            listy.Add("None");
                    }
                    listy.Add(string.IsNullOrEmpty(customData.ClientName) ? "N/A" : customData.ClientName);
                    listy.Add(string.IsNullOrEmpty(customData.ProjectName) ? "N/A" : customData.ProjectName);
                    listy.Add(string.IsNullOrEmpty(customData.ContractName) ? "N/A" : customData.ContractName);

                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("WarehouseP", "getTableRowDataTask", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTaskHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var eventData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);//EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(eventData.Id, dbConnection);

                var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(id), dbConnection);
                if (tasklinks.Count > 0)
                {
                    foreach (var link in tasklinks)
                    {
                        var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                        foreach (var forl in forlink)
                        {
                            forl.isSubTask = true;
                            taskeventhistory.Add(forl);
                        }
                    }
                    taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                }

                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                foreach (var task in taskeventhistory)
                {
                    var taskn = "Task";
                    if (task.isSubTask)
                    {
                        taskn = "Sub-task";
                    }
                    if (eventData.AssigneeType == (int)TaskAssigneeType.Group)
                    {
                        if (task.Action == (int)TaskAction.Update)
                        {
                            acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                                + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> updated<span class='red-color'>" + taskn
                                + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                            listy.Add(acceptedData);
                        }
                    }

                    if (task.Action == (int)TaskAction.Complete)
                    {
                        completedBy = task.CustomerUName;
                        var compLocation = string.Empty;
                        var geoLoc = ReverseGeocode.RetrieveFormatedAddress(eventData.EndLatitude.ToString(), eventData.EndLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc))
                            compLocation = " at " + geoLoc;
                        var compInfo = "completed";
                        var compstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + completedBy + "</span>" + compInfo + "<span class='red-color'>" + taskn
                            + "</span>" + compLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(compstring);
                    }
                    else if (task.Action == (int)TaskAction.InProgress)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "started";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.OnRoute)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "on route";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Pause)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "paused";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Resume)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "resumed";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Accepted)
                    {
                        acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> accepted<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(acceptedData);
                    }
                    else if (task.Action == (int)TaskAction.Rejected)
                    {
                        rejectedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> rejected<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(rejectedData);
                    }
                    else if (task.Action == (int)TaskAction.Assigned)
                    {
                        assignedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> assigned<span class='red-color'>" + taskn
                            + "</span> to " + task.ACustomerUName + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(assignedData);
                    }
                    else if (task.Action == (int)TaskAction.Pending)
                    {
                        var incidentLocation = string.Empty;
                        var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(eventData.Latitude.ToString(), eventData.Longitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc3))
                            incidentLocation = " at " + geoLoc3;
                        var actioninfo = "created";

                        var gUser = Users.GetUserByName(eventData.CreatedBy, dbConnection);

                        var dataString = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + gUser.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + taskn
                            + "</span>for " + task.ACustomerUName + " " + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(dataString);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "getTaskHistoryData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataIcons(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (i == 4)
                            {
                                var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#taskattachments-tab' onclick='hideTaskplay()'/>";
                                listy += retstring;
                                i++;
                                break;
                            }
                            else
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                {
                                    if (item.ChecklistId == 0)
                                    {
                                        //var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)'/>";

                                        listy += retstring;
                                        i++;
                                    }
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                    //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                    var retstring = "<img src='" + item.DocumentPath + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "taskgetAttachmentDataIcons", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> taskgetAttachmentData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<video id='Video" + i + " width='100%' height='380px' muted controls ><source src='" + item.DocumentPath + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                //if (item.ChecklistId == 0)
                                //{
                                //    var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source src='" + item.DocumentPath + "' type='audio/mpeg' /></audio>";
                                //    listy.Add(retstring);
                                //    i++;
                                //}
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img onclick='rotateMe(this);' src='" + item.DocumentPath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("WarehouseP", "taskgetAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideTaskplay()' data-target='#tasklocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Task Location</p></div></div></div>";
                var i = 1;
                var iTab = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideTaskplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                if (item.ChecklistId == 0)
                                {
                                    //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                    listy += retstring;
                                    //iTab++;
                                }
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                                var imgstring = String.Format(item.DocumentPath);

                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideTaskplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;

                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstring = "<div class='row static-height-with-border clickable-row' onclick='hideTaskplay()' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "getAttachmentDataTab", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> taskgetChecklistData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var chklisty = new List<string>();
                var isAllChecked = true;
                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);


                var groupedlist = new List<TaskCheckList>();
                var groupedItems = sessions.GroupBy(x => x.Id);
                var groupedlistItems = groupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                foreach (var child in groupedlistItems)
                {
                    var attachfiles = sessions.Where(i => i.Id == child.Id).ToList();
                    var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                    if (mp3files.Count > 0)
                    {
                        //Yes
                        groupedlist.Add(mp3files[0]);
                    }
                    else
                    {
                        groupedlist.Add(attachfiles[0]);
                    }
                }

                foreach (var item in groupedlist)
                {
                    var stringCheck = string.Empty;
                    if (item.IsChecked)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                        isAllChecked = false;
                    }
                    var imgsrc = string.Empty;
                    if (!string.IsNullOrEmpty(item.DocumentPath) && System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                    {

                        imgsrc = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                    }

                    if (item.CheckListItemType == 4 || item.CheckListItemType == 5)
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|True|" + imgsrc);
                    }
                    else if (item.CheckListItemType == 3)
                    {
                        if (!string.IsNullOrEmpty(item.TemplateCheckListItemNote))
                            chklisty.Add(item.Name + "|" + stringCheck + "|3|" + item.TemplateCheckListItemNote + "|" + imgsrc);
                        else
                            chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    else
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    if (item.ChildCheckList != null)
                    {
                        //var audiofiles = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                        //var glist = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() != ".MP3").ToList();

                        var childgroupedlist = new List<TaskCheckList>();
                        var childgroupedItems = item.ChildCheckList.GroupBy(x => x.Id);
                        var childgroupedlistItems = childgroupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                        foreach (var child in childgroupedlistItems)
                        {
                            var attachfiles = item.ChildCheckList.Where(i => i.Id == child.Id).ToList();
                            var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                            if (mp3files.Count > 0)
                            {
                                //Yes
                                childgroupedlist.Add(mp3files[0]);
                            }
                            else
                            {
                                childgroupedlist.Add(attachfiles[0]);
                            }
                        }

                        foreach (var child in childgroupedlist)
                        {
                            var imgsrc2 = string.Empty;
                            if (!string.IsNullOrEmpty(child.DocumentPath) && System.IO.Path.GetExtension(child.DocumentPath).ToUpperInvariant() == ".MP3")
                            {

                                imgsrc2 = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + child.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                            }

                            if (child.IsChecked)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                                isAllChecked = false;
                            }
                            chklisty.Add(child.Name + "|" + stringCheck + "|False|" + imgsrc2);
                        }
                    }
                }
                listy.Add(isAllChecked.ToString());
                listy.AddRange(chklisty);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "getChecklistData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getChecklistNotesData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                foreach (var item in sessions)
                {
                    if (item.CheckListItemType == 5)
                    {
                        if (item.ChildCheckList != null)
                        {
                            foreach (var child in item.ChildCheckList)
                            {
                                if (!child.IsChecked)
                                    listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "getChecklistNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getCanvasNotesData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var sessions = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var count = 1;
                foreach (var item in sessions)
                {
                    if (!string.IsNullOrEmpty(item.DocumentPath))
                    {
                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                        {
                            listy.Add("Attachment " + count + "|" + item.ImageNote);
                        }
                        count++;    //listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "getCanvasNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getTaskLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            if (item.StatusDescription == "Pending")
                            {
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                            {
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "OnRoute")
                            {
                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || item.StatusDescription == "Rejected")
                            {
                                if (item.EndLatitude > 0 && item.EndLongitude > 0)
                                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "getTaskLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static List<string> getTaskRemarksData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = UserTask.GetTaskById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = TaskRemarks.GetTaskRemarksByTaskId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Notes))
                    {
                        var nTR = new TaskRemarks();
                        nTR.CustomerUName = projinfo.ACustomerUName;
                        nTR.Remarks = projinfo.Notes;
                        nTR.CreatedDate = projinfo.UpdatedDate.Value;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        var count = 0;
                        foreach (var task in remarksList)
                        {
                            if (count == 3)
                            {
                                break;
                            }
                            count++;
                            var OGremarks = task.Remarks;
                            if (!string.IsNullOrEmpty(task.Remarks) && task.Remarks.Length > 50)
                            {
                                task.Remarks = task.Remarks.Remove(50);
                                task.Remarks = task.Remarks + "...";
                            }
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                        if (remarksList.Count > 3)
                        {
                            var seeall = "<h5><span  class='line-center' onmouseover='style=&apos;cursor: pointer;' onclick='showAllRemarks(&apos;" + id + "&apos;)'>SEE ALL</span></h5>";
                            listy.Add(seeall);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTaskRemarksData2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = UserTask.GetTaskById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = TaskRemarks.GetTaskRemarksByTaskId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Notes))
                    {
                        var nTR = new TaskRemarks();
                        nTR.CustomerUName = projinfo.ACustomerUName;
                        nTR.Remarks = projinfo.Notes;
                        nTR.CreatedDate = projinfo.UpdatedDate.Value;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        foreach (var task in remarksList)
                        {
                            var OGremarks = task.Remarks;
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getTracebackLocationDataByUser(int id, int duration, string ttype, int[] userIds, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    json += "[";
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);

                        var geteventhistorys = TaskEventHistory.GetTaskEventHistoryByTaskId(Convert.ToInt32(id), dbConnection);
                        var rejecteds = geteventhistorys.Where(i => i.Action == (int)TaskAction.Rejected).ToList();

                        var gotrejected = false;

                        var inprohistory = new TaskEventHistory();
                        var onroutehistory = new TaskEventHistory();

                        if (rejecteds.Count > 0)
                        {
                            gotrejected = true;
                        }
                        //if (!gotrejected)
                        {
                            if (item.StatusDescription == "Pending")
                            {
                                if (item.Longitude > 0 && item.Latitude > 0)
                                    json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                            {
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.Longitude > 0 && item.Latitude > 0)
                                    json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "OnRoute")
                            {
                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                            {
                                if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                    json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.Longitude > 0 && item.Latitude > 0)
                                    json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }

                        var curTime = CommonUtility.getDTNow();
                        var firstTime = false;
                        if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            if (traceback.Count > 0)
                            {
                                if (item.OnRouteLatitude > 0.0 && item.OnRouteLongitude > 0.0 && item.ActualOnRouteDate != null)
                                {
                                    json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + "https://testportalcdn.azureedge.net/Images/whitegreenball.png" + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + "https://testportalcdn.azureedge.net/Images/whiteyellowball.png" + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                    if (!firstTime)
                                    {
                                        curTime = tb.CreatedDate.Value;
                                        firstTime = true;
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + tb.Marker + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";

                                    }
                                    else
                                    {
                                        if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                        {
                                            json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + tb.Marker + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                            curTime = tb.CreatedDate.Value;
                                        }
                                    }
                                }

                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + "https://testportalcdn.azureedge.net/Images/bluesmall.png" + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    else
                    {
                        var item = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (item.EventType == CustomEvent.EventTypes.Incident)
                        {
                            var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                            if (geofence.Count > 0)
                            {
                                foreach (var geo in geofence)
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                        }
                        else
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        var colorCount = 0;
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        var notiuserlist = new List<string>();
                        foreach (var noti in notification)
                        {
                            if (noti.Prefix == "ARL" && noti.IsHandled)
                            {
                                if (!userIds.Contains(noti.AssigneeId))//(noti.AssigneeId != userIds[0])
                                {
                                    if (!notiuserlist.Contains(noti.AssigneeName))
                                    {
                                        notiuserlist.Add(noti.AssigneeName);
                                        var tbColor = string.Empty;
                                        if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                                        {

                                        }
                                        else
                                        {
                                            tbColor = CommonUtility.getHexColor(colorCount);
                                            assigneeList.Add(noti.AssigneeId);
                                            colorCount++;
                                        }

                                        var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                                        var curTime = CommonUtility.getDTNow();
                                        var firstTime = false;
                                        var evHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection);
                                        var engLongi = string.Empty;
                                        var engLati = string.Empty;
                                        var engDate = string.Empty;
                                        var compLongi = string.Empty;
                                        var compLati = string.Empty;
                                        var compDate = string.Empty;
                                        if (traceback.Count > 0)
                                        {
                                            //json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                            var engstart = false;

                                            foreach (var tb in traceback)
                                            {
                                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                                if (!engstart)
                                                {
                                                    engstart = true;
                                                    var engfound = false;
                                                    var compfound = false;
                                                    foreach (var ev in evHistory)
                                                    {
                                                        if (ev.UserName == getUser.Username)
                                                        {
                                                            if (!engfound)
                                                            {
                                                                if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                                {
                                                                    engLongi = ev.Longtitude;
                                                                    engLati = ev.Latitude;
                                                                    engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                                    json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                                    json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                                    engfound = true;

                                                                }
                                                            }
                                                            if (!compfound)
                                                            {
                                                                if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                                {
                                                                    compLongi = ev.Longtitude;
                                                                    compLati = ev.Latitude;
                                                                    compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                                    compfound = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (!firstTime)
                                                {
                                                    curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                                    firstTime = true;
                                                    json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";

                                                }
                                                else
                                                {
                                                    if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                                    {
                                                        json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                                    }
                                                }
                                            }
                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                            //json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                        }
                                        else
                                        {
                                            var getEventHistory = EventHistory.GetEventHistoryByEventId(id, dbConnection);
                                            var engfound = false;
                                            var compfound = false;
                                            //var engLongi = string.Empty;
                                            //var engLati = string.Empty;
                                            //var engDate = string.Empty;
                                            //var compLongi = string.Empty;
                                            //var compLati = string.Empty;
                                            //var compDate = string.Empty;
                                            foreach (var ev in getEventHistory)
                                            {
                                                if (ev.UserName == noti.AssigneeName)
                                                {
                                                    if (!engfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                        {
                                                            engLongi = ev.Longtitude;
                                                            engLati = ev.Latitude;
                                                            engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                            engfound = true;

                                                        }
                                                    }
                                                    if (!compfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                        {
                                                            compLongi = ev.Longtitude;
                                                            compLati = ev.Latitude;
                                                            compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                                            compfound = true;

                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("UsersDB", "getTracebackLocationDataByUser", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string getTracebackLocationData(int id, string ttype, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var assigneeList = new List<int>();
                if (id > 0)
                {
                    json += "[";
                    if (ttype == "task")
                    {
                        var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                        var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);

                        var geteventhistorys = TaskEventHistory.GetTaskEventHistoryByTaskId(Convert.ToInt32(id), dbConnection);
                        var rejecteds = geteventhistorys.Where(i => i.Action == (int)TaskAction.Rejected).ToList();

                        var gotrejected = false;

                        if (rejecteds.Count > 0)
                        {
                            gotrejected = true;
                        }

                        var inprohistory = new TaskEventHistory();
                        var onroutehistory = new TaskEventHistory();

                        //if (!gotrejected)
                        //{
                        if (item.StatusDescription == "Pending")
                        {
                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        {
                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.StartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        //}
                        //else
                        //{
                        //    if (item.Longitude > 0 && item.Latitude > 0)
                        //        json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        //    foreach (var ev in geteventhistorys)
                        //    {
                        //        if (ev.Action == (int)TaskAction.OnRoute)
                        //        {
                        //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                        //                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                        //            if (onroutehistory.Id == 0)
                        //            {
                        //                if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                        //                {
                        //                    onroutehistory = ev;
                        //                }
                        //            } 
                        //        }
                        //        else if (ev.Action == (int)TaskAction.InProgress)
                        //        {
                        //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                        //                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                        //            if (inprohistory.Id == 0)
                        //            {
                        //                if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                        //                {
                        //                    inprohistory = ev;
                        //                }
                        //            }
                        //        }
                        //        else if (ev.Action == (int)TaskAction.Complete)
                        //        {
                        //            if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                        //            {
                        //                if (ev.CreatedDate.Value < item.ActualEndDate.Value)
                        //                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"X\",\"Logs\" : \"Retrieve\"},";
                        //                else
                        //                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                        //            }
                        //        }
                        //    }
                        //}
                        if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || gotrejected)
                        {
                            if (traceback.Count > 0)
                            {
                                //if (gotrejected)
                                //{

                                //    if (onroutehistory.Id == 0)
                                //    {
                                //        if (!string.IsNullOrEmpty(onroutehistory.Longtitude) && !string.IsNullOrEmpty(onroutehistory.Latitude))
                                //            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + onroutehistory.Longtitude.ToString() + "\",\"Lat\" : \"" + onroutehistory.Latitude.ToString() + "\",\"LastLog\" :  \"" + onroutehistory.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                //    }
                                //    else
                                //    {
                                //        if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                //            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                //    }

                                //    if (inprohistory.Id == 0)
                                //    {
                                //        if (!string.IsNullOrEmpty(inprohistory.Longtitude) && !string.IsNullOrEmpty(inprohistory.Latitude))
                                //            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + inprohistory.Longtitude.ToString() + "\",\"Lat\" : \"" + inprohistory.Latitude.ToString() + "\",\"LastLog\" :  \"" + inprohistory.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                //    }
                                //    else
                                //    {
                                //        if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                //            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                //    }
                                //}
                                //else
                                //{
                                //    if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                //        json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualOnRouteDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";


                                //    if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                //        json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";                           
                                //}
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                    if (tb.Longitude > 0 && tb.Latitude > 0)
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"Marker\" :  \"" + tb.Marker + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                                //if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                //    json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    else
                    {
                        var item = CustomEvent.GetCustomEventById(id, dbConnection);
                        var getEventHistory = EventHistory.GetEventHistoryByEventId(id, dbConnection);
                        var isCompleted = false;
                        //if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject || item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                        //{

                        foreach (var getEv in getEventHistory)
                        {
                            if (getEv.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                            {
                                isCompleted = true;
                                break;
                            }
                        }
                        //}
                        if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete || isCompleted)
                        {
                            if (item.EventType == CustomEvent.EventTypes.Incident)
                            {
                                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                                if (geofence.Count > 0)
                                {
                                    foreach (var geo in geofence)
                                    {
                                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                    }
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            var colorCount = 0;
                            var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                            var notiuserlist = new List<string>();
                            foreach (var noti in notification)
                            {
                                if (noti.Prefix == "ARL" && noti.IsHandled)
                                {

                                    if (!notiuserlist.Contains(noti.AssigneeName))
                                    {
                                        notiuserlist.Add(noti.AssigneeName);
                                        var tbColor = string.Empty;
                                        if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                                        {

                                        }
                                        else
                                        {
                                            tbColor = CommonUtility.getHexColor(colorCount);
                                            assigneeList.Add(noti.AssigneeId);
                                            colorCount++;
                                        }
                                        var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                                        if (traceback.Count > 0)
                                        {

                                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                            foreach (var tb in traceback)
                                            {
                                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                                json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                            }
                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                        }
                                        else
                                        {

                                            var engfound = false;
                                            var compfound = false;
                                            var engLongi = string.Empty;
                                            var engLati = string.Empty;
                                            var engDate = string.Empty;
                                            var compLongi = string.Empty;
                                            var compLati = string.Empty;
                                            var compDate = string.Empty;
                                            foreach (var ev in getEventHistory)
                                            {
                                                if (ev.UserName == noti.AssigneeName)
                                                {
                                                    if (!engfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                                                        {
                                                            engLongi = ev.Longtitude;
                                                            engLati = ev.Latitude;
                                                            engDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + engLongi + "\",\"Lat\" : \"" + engLati + "\",\"LastLog\" :  \"" + engDate + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                                            engfound = true;

                                                        }
                                                    }
                                                    if (!compfound)
                                                    {
                                                        if (ev.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                                                        {
                                                            compLongi = ev.Longtitude;
                                                            compLati = ev.Latitude;
                                                            compDate = ev.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + compLongi + "\",\"Lat\" : \"" + compLati + "\",\"LastLog\" :  \"" + compDate + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                                                            compfound = true;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UsersDB.aspx", "getTracebackLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string addNewItemRemarks(int id, string notes, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var projinfo = ItemLost.GetItemLostById(id, dbConnection);
                if (projinfo != null)
                {
                    var newRemarks = new ItemFoundRemarks();
                    newRemarks.CreatedBy = userinfo.Username;
                    newRemarks.CreatedDate = CommonUtility.getDTNow();
                    newRemarks.ItemLostId = id;
                    newRemarks.Remarks = notes;
                    newRemarks.UpdatedBy = userinfo.Username;
                    newRemarks.UpdatedDate = CommonUtility.getDTNow(); 
                    newRemarks.CustomerId = userinfo.CustomerInfoId;
                    newRemarks.SiteId = userinfo.SiteId;
                    var retV = 0;
                    if (!string.IsNullOrEmpty(notes))
                    {
                        retV = ItemFoundRemarks.InsertOrUpdateItemFoundRemarks(newRemarks, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        retV = 1;
                    }
                    if (retV > 0)
                    {
                        json = "SUCCESS";
                    }
                    else
                    {
                        json = "Problem faced trying to save notes";
                    }
                }
                else
                {
                    json = "Problem faced trying to get lost item and save remarks";
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("WarehouseP", "addNewItemRemarks", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }
    }
}