﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArrowLabs.Licence.Portal.Helpers
{
    public class MimsReport
    {
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }

        public string ColumnStatus { get; set; }
    }
}