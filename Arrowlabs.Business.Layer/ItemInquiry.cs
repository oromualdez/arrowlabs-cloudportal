﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
  public  class ItemInquiry
    {
        public int Id { get; set; }
        public DateTime DateFound { get; set; }
        public string LocationLost { get; set; }
        public string Type { get; set; }
        public string ItemBrand { get; set; }
        public string ItemColour { get; set; }
        public string ItemDescription { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string RoomNumber { get; set; }
        public string SecurityTracking { get; set; }
        public bool Found { get; set; }
        public bool Delivered { get; set; }
        public bool Closed { get; set; }
        public DateTime DeliveredDate { get; set; }
        public int ItemFoundId { get; set; }
        public string ItemReference { get; set; }
        public int SiteId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
      
        public string ReferenceNo { get; set; }
        public string SubType { get; set; }

        public int CustomerId { get; set; }
        public string CustomerUName { get; set; }
        private static ItemInquiry GetItemInquiryFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var itemInquiry = new ItemInquiry();
            if (reader["Id"] != DBNull.Value)
                itemInquiry.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["DateFound"] != DBNull.Value)
                itemInquiry.DateFound = Convert.ToDateTime(reader["DateFound"].ToString());
            if (reader["LocationLost"] != DBNull.Value)
                itemInquiry.LocationLost = reader["LocationLost"].ToString();
            if (reader["Type"] != DBNull.Value)
                itemInquiry.Type = reader["Type"].ToString();
            if (reader["ItemBrand"] != DBNull.Value)
                itemInquiry.ItemBrand = reader["ItemBrand"].ToString();
            if (reader["ItemColour"] != DBNull.Value)
                itemInquiry.ItemColour = reader["ItemColour"].ToString();
            if (reader["ItemDescription"] != DBNull.Value)
                itemInquiry.ItemDescription = reader["ItemDescription"].ToString();
            if (reader["Name"] != DBNull.Value)
                itemInquiry.Name = reader["Name"].ToString();
            if (reader["Address"] != DBNull.Value)
                itemInquiry.Address = reader["Address"].ToString();
            if (reader["ContactNo"] != DBNull.Value)
                itemInquiry.ContactNo = reader["ContactNo"].ToString();
            if (reader["RoomNumber"] != DBNull.Value)
                itemInquiry.RoomNumber = reader["RoomNumber"].ToString();
            if (reader["SecurityTracking"] != DBNull.Value)
                itemInquiry.SecurityTracking = reader["SecurityTracking"].ToString();
            if (reader["DeliveredDate"] != DBNull.Value)
                itemInquiry.DeliveredDate = Convert.ToDateTime(reader["DeliveredDate"].ToString());
        
            if (reader["Found"] != DBNull.Value)
                itemInquiry.Found = Convert.ToBoolean(reader["Found"].ToString());
            if (reader["Delivered"] != DBNull.Value)
                itemInquiry.Delivered = Convert.ToBoolean(reader["Delivered"].ToString());
            if (reader["Closed"] != DBNull.Value)
                itemInquiry.Closed = Convert.ToBoolean(reader["Closed"].ToString());
            if (reader["SiteId"] != DBNull.Value)
                itemInquiry.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["ItemFoundId"] != DBNull.Value)
                itemInquiry.ItemFoundId = Convert.ToInt32(reader["ItemFoundId"].ToString());
            if (reader["ItemReference"] != DBNull.Value)
                itemInquiry.ItemReference = reader["ItemReference"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                itemInquiry.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                itemInquiry.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                itemInquiry.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["UpdatedBy"] != DBNull.Value)
                itemInquiry.UpdatedBy = reader["UpdatedBy"].ToString();

            if (columns.Contains( "ReferenceNo") && reader["ReferenceNo"] != DBNull.Value)
                itemInquiry.ReferenceNo = reader["ReferenceNo"].ToString();

            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                itemInquiry.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains( "SubType") && reader["SubType"] != DBNull.Value)
                itemInquiry.SubType = reader["SubType"].ToString();

            if (columns.Contains( "CustomerUName"))
                itemInquiry.CustomerUName = reader["CustomerUName"].ToString();
            

            return itemInquiry;
        }
        public static ItemInquiry GetItemInquiryByReference(string reference, string dbConnection)
        {
            ItemInquiry itemInquiry = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemInquiryByReference", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Reference"].Value = reference;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        itemInquiry = GetItemInquiryFromSqlReader(reader);
                    }
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemInquiry", "GetItemInquiryByReference", exp, dbConnection);
            }
            return itemInquiry;
        }

        public static ItemInquiry GetItemInquiryByReferenceNo(string reference, string dbConnection)
        {
            ItemInquiry itemInquiry = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemInquiryByReferenceNo", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Reference"].Value = reference;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        itemInquiry = GetItemInquiryFromSqlReader(reader);
                    }
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemInquiry", "GetItemInquiryByReferenceNo", exp, dbConnection);
            }
            return itemInquiry;
        }
      

        public static List<ItemInquiry> GetAllItemInquiry(string dbConnection)
        {
            var itemInquiries = new List<ItemInquiry>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllItemInquiry", connection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemOwner = GetItemInquiryFromSqlReader(reader);
                        itemInquiries.Add(itemOwner);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemInquiry", "GetAllItemInquiry", exp, dbConnection);

            }
            return itemInquiries;
        }
        public static List<ItemInquiry> GetItemInquiryByLevel5(int siteId, string dbConnection)
        {
            var itemInquiries = new List<ItemInquiry>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemInquiryByLevel5", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemOwner = GetItemInquiryFromSqlReader(reader);
                        itemInquiries.Add(itemOwner);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemInquiry", "GetItemInquiryByLevel5", exp, dbConnection);

            }
            return itemInquiries;
        }
        public static List<ItemInquiry> GetItemInquiryBySiteId(int siteId, string dbConnection)
        {
            var itemInquiries = new List<ItemInquiry>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemInquiryBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemOwner = GetItemInquiryFromSqlReader(reader);
                        itemInquiries.Add(itemOwner);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemInquiry", "GetItemInquiryBySiteId", exp, dbConnection);

            }
            return itemInquiries;
        }

        public static List<ItemInquiry> GetItemInquiryByCId(int siteId, string dbConnection)
        {
            var itemInquiries = new List<ItemInquiry>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemInquiryByCId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemOwner = GetItemInquiryFromSqlReader(reader);
                        itemInquiries.Add(itemOwner);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemInquiry", "GetItemInquiryByCId", exp, dbConnection);

            }
            return itemInquiries;
        }

        public static ItemInquiry GetItemInquiryById(int id, string dbConnection)
        {
            ItemInquiry itemInquiry = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemInquiryById", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        itemInquiry = GetItemInquiryFromSqlReader(reader);

                    }
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemInquiry", "GetItemInquiryById", exp, dbConnection);
            }
            return itemInquiry;
        }

        public static int InsertorUpdateItemInquiry(ItemInquiry itemInquiry, string dbConnection,
          string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            if (itemInquiry != null)
            {
                id = itemInquiry.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateItemInquiry", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = itemInquiry.Id;
                    cmd.Parameters.Add(new SqlParameter("@DateFound", SqlDbType.DateTime));
                    cmd.Parameters["@DateFound"].Value = itemInquiry.DateFound;
                    cmd.Parameters.Add(new SqlParameter("@LocationLost", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationLost"].Value = itemInquiry.LocationLost;
                    cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.NVarChar));
                    cmd.Parameters["@Type"].Value = itemInquiry.Type;
                    cmd.Parameters.Add(new SqlParameter("@ItemBrand", SqlDbType.NVarChar));
                    cmd.Parameters["@ItemBrand"].Value = itemInquiry.ItemBrand;
                    cmd.Parameters.Add(new SqlParameter("@ItemColour", SqlDbType.NVarChar));
                    cmd.Parameters["@ItemColour"].Value = itemInquiry.ItemColour;
                    cmd.Parameters.Add(new SqlParameter("@ItemDescription", SqlDbType.NVarChar));
                    cmd.Parameters["@ItemDescription"].Value = itemInquiry.ItemDescription;
                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = itemInquiry.Name;
                    cmd.Parameters.Add(new SqlParameter("@Address", SqlDbType.NVarChar));
                    cmd.Parameters["@Address"].Value = itemInquiry.Address;
                    cmd.Parameters.Add(new SqlParameter("@ContactNo", SqlDbType.NVarChar));
                    cmd.Parameters["@ContactNo"].Value = itemInquiry.ContactNo;
                    cmd.Parameters.Add(new SqlParameter("@RoomNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@RoomNumber"].Value = itemInquiry.RoomNumber;
                    cmd.Parameters.Add(new SqlParameter("@SecurityTracking", SqlDbType.NVarChar));
                    cmd.Parameters["@SecurityTracking"].Value = itemInquiry.SecurityTracking;
                    cmd.Parameters.Add(new SqlParameter("@Found", SqlDbType.Bit));
                    cmd.Parameters["@Found"].Value = itemInquiry.Found;
                    cmd.Parameters.Add(new SqlParameter("@Delivered", SqlDbType.Bit));
                    cmd.Parameters["@Delivered"].Value = itemInquiry.Delivered;
                    cmd.Parameters.Add(new SqlParameter("@Closed", SqlDbType.Bit));
                    cmd.Parameters["@Closed"].Value = itemInquiry.Closed;
                    cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    cmd.Parameters["@ItemFoundId"].Value = itemInquiry.ItemFoundId;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = itemInquiry.SiteId;
                    cmd.Parameters.Add(new SqlParameter("@ItemReference", SqlDbType.NVarChar));
                    cmd.Parameters["@ItemReference"].Value = itemInquiry.ItemReference;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = itemInquiry.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = itemInquiry.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = itemInquiry.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = itemInquiry.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ReferenceNo", SqlDbType.NVarChar));
                    cmd.Parameters["@ReferenceNo"].Value = itemInquiry.ReferenceNo;

                    cmd.Parameters.Add(new SqlParameter("@SubType", SqlDbType.NVarChar));
                    cmd.Parameters["@SubType"].Value = itemInquiry.SubType;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = itemInquiry.CustomerId;
                    

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            itemInquiry.Id = id;
                            var audit = new ItemInquiryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, itemInquiry.UpdatedBy);
                            Reflection.CopyProperties(itemInquiry, audit);
                            ItemInquiryAudit.InsertorUpdateItemInquiryAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateItemFound", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static bool DeleteItemInquiryById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteItemInquiryById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static int GetLastItemInquiryIdBySiteId(DateTime from, DateTime todate, int siteid, string dbConnection)
        {
            int lastUserId = 0;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLastItemInquiryBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.Parameters.Add(new SqlParameter("@fromdate", SqlDbType.DateTime));
                sqlCommand.Parameters["@fromdate"].Value = from;
                sqlCommand.Parameters.Add(new SqlParameter("@todate", SqlDbType.DateTime));
                sqlCommand.Parameters["@todate"].Value = todate;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    lastUserId = Convert.ToInt32(reader["ID"].ToString());
                    break;
                }
                connection.Close();
                reader.Close();
            }
            return lastUserId;
        }
    }
}
