﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class WhiteList
    {
        public int Id { get; set; }
        public string MACAddress { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public static List<WhiteList> GetAllWhiteList(string dbConnection)
        {
            var whiteList = new List<WhiteList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllWhiteList", connection);

                sqlCommand.CommandText = "GetAllWhiteList";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var whiteClass = new WhiteList();
                    whiteClass.MACAddress = reader["MACAddress"].ToString();
                    whiteClass.CreatedBy = reader["CreatedBy"].ToString();
                    whiteClass.UpdatedBy = reader["UpdatedBy"].ToString();
                    whiteClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    whiteClass.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    whiteList.Add(whiteClass);
                }
                connection.Close();
                reader.Close();
            }
            return whiteList;
        }
        public static bool InsertOrUpdateWhiteList(WhiteList whiteClass, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (whiteClass != null)
            {
                int id = whiteClass.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateWhiteList", connection);

                    cmd.Parameters.Add(new SqlParameter("@MACAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MACAddress"].Value = whiteClass.MACAddress;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = whiteClass.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = whiteClass.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = whiteClass.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = whiteClass.UpdatedDate;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateWhiteList";

                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            whiteClass.Id = id;
                            var audit = new WhiteListAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, whiteClass.UpdatedBy);
                            Reflection.CopyProperties(whiteClass, audit);
                            WhiteListAudit.InsertWhiteListAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateWhiteList", ex, auditDbConnection);
                    }
                }
            }
            return true;
        }
        public static bool DeleteWhiteByMacAddress(string macAddress, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteWhiteByMacAddress", connection);

                cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                cmd.Parameters["@MacAddress"].Value = macAddress;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteWhiteByMacAddress";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
