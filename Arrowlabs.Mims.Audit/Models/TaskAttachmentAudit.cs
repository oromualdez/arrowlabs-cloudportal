﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public enum UserTaskDocumentType
    {
        None,
        TaskImage,
        TaskDocument,
        Signature
    }
    public class TaskAttachmentAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int UserId { get; set; }
        public bool IsDeleted { get; set; }
        public string DocumentType { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public string ImageNote { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int ChecklistId { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        private static TaskAttachmentAudit GetTaskAttachmentAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var taskAttachment = new TaskAttachmentAudit();
            taskAttachment.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                taskAttachment.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("TaskId") && reader["TaskId"] != DBNull.Value)
                taskAttachment.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
            if (columns.Contains("DocumentType") && reader["DocumentType"] != DBNull.Value)
                taskAttachment.DocumentType = reader["DocumentType"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                taskAttachment.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                taskAttachment.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("UserId") && reader["UserId"] != DBNull.Value)
                taskAttachment.UserId = Convert.ToInt32(reader["UserId"].ToString());
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                taskAttachment.IsDeleted = (String.IsNullOrEmpty(reader["IsDeleted"].ToString())) ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (columns.Contains("DocumentPath") && reader["DocumentPath"] != DBNull.Value)
                taskAttachment.DocumentPath = reader["DocumentPath"].ToString();
            if (columns.Contains("DocumentName") && reader["DocumentName"] != DBNull.Value)
                taskAttachment.DocumentName = reader["DocumentName"].ToString();
            if (columns.Contains("ImageNote") && reader["ImageNote"] != DBNull.Value)
                taskAttachment.ImageNote = reader["ImageNote"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                taskAttachment.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return taskAttachment;
        }
        public static List<TaskAttachmentAudit> GetAllTaskAttachmentAudit(string dbConnection)
        {
            var taskAttachments = new List<TaskAttachmentAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllTaskAttachmentAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var taskAttachment = GetTaskAttachmentAuditFromSqlReader(reader);
                    taskAttachments.Insert(0, taskAttachment);
                }
                connection.Close();
                reader.Close();
                return taskAttachments;
            }

        }
        public static int InsertTaskAttachmentAudit(TaskAttachmentAudit attachment, string dbConnection)
        {
            int id = 0;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("InsertTaskAttachmentAudit", connection);

                var baseAuditId = BaseAudit.InsertBaseAudit(attachment.BaseAudit, dbConnection);
                cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = attachment.Id;

                cmd.Parameters.Add("@TaskId", SqlDbType.Int).Value = attachment.TaskId;

                cmd.Parameters.Add("@DocumentType", SqlDbType.NVarChar).Value = attachment.DocumentType;

                cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar).Value = attachment.DocumentName;

                cmd.Parameters.Add("@DocumentPath", SqlDbType.NVarChar).Value = attachment.DocumentPath;

                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = attachment.CreatedDate;

                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = attachment.CreatedBy;

                cmd.Parameters.Add("@ImageNote", SqlDbType.NVarChar).Value = attachment.ImageNote;

                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = attachment.UserId;
                
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = attachment.IsDeleted;

                cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = attachment.SiteId;

                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = attachment.CustomerId;

                cmd.Parameters.Add(new SqlParameter("@ChecklistId", SqlDbType.Int));
                cmd.Parameters["@ChecklistId"].Value = attachment.ChecklistId;
                
                SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

                id = (int)returnParameter.Value;
            }
            return id;
        }
    }
}
