﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class CustomerModuleMapAudit
    {
        public BaseAudit BaseAudit { get; set; }

        public int CustomerId { get; set; }
        public string MBC { get; set; }

        public string INC { get; set; }

        public string TAC { get; set; }

        public string TIC { get; set; }

        public string PPC { get; set; }

        public string DRC { get; set; }

        public string LFC { get; set; }

        public string COC { get; set; }

        public string REC { get; set; }

        public string SUC { get; set; }

        public string ACC { get; set; }
        public string CHC { get; set; }
        public string WAC { get; set; }

        public string DIC { get; set; }

        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public static bool InsertCustomerModuleMapAudit(CustomerModuleMapAudit clas, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(clas.BaseAudit, dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("InsertCustomerModuleMap", connection);

                cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                cmd.Parameters["@CustomerId"].Value = clas.CustomerId;

                cmd.Parameters.Add(new SqlParameter("@ACC", SqlDbType.NVarChar));
                cmd.Parameters["@ACC"].Value = Encrypt.EncryptData(clas.ACC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@CHC", SqlDbType.NVarChar));
                cmd.Parameters["@CHC"].Value = Encrypt.EncryptData(clas.CHC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@COC", SqlDbType.NVarChar));
                cmd.Parameters["@COC"].Value = Encrypt.EncryptData(clas.COC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                cmd.Parameters["@CreatedBy"].Value = Encrypt.EncryptData(clas.CreatedBy, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                cmd.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(clas.CreatedDate, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@DIC", SqlDbType.NVarChar));
                cmd.Parameters["@DIC"].Value = Encrypt.EncryptData(clas.DIC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@DRC", SqlDbType.NVarChar));
                cmd.Parameters["@DRC"].Value = Encrypt.EncryptData(clas.DRC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@INC", SqlDbType.NVarChar));
                cmd.Parameters["@INC"].Value = Encrypt.EncryptData(clas.INC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@LFC", SqlDbType.NVarChar));
                cmd.Parameters["@LFC"].Value = Encrypt.EncryptData(clas.LFC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@MBC", SqlDbType.NVarChar));
                cmd.Parameters["@MBC"].Value = Encrypt.EncryptData(clas.MBC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@PPC", SqlDbType.NVarChar));
                cmd.Parameters["@PPC"].Value = Encrypt.EncryptData(clas.PPC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@REC", SqlDbType.NVarChar));
                cmd.Parameters["@REC"].Value = Encrypt.EncryptData(clas.REC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@SUC", SqlDbType.NVarChar));
                cmd.Parameters["@SUC"].Value = Encrypt.EncryptData(clas.SUC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@TAC", SqlDbType.NVarChar));
                cmd.Parameters["@TAC"].Value = Encrypt.EncryptData(clas.TAC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@TIC", SqlDbType.NVarChar));
                cmd.Parameters["@TIC"].Value = Encrypt.EncryptData(clas.TIC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                cmd.Parameters["@UpdatedBy"].Value = Encrypt.EncryptData(clas.UpdatedBy, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.NVarChar));
                cmd.Parameters["@UpdatedDate"].Value = Encrypt.EncryptData(clas.UpdatedDate, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@WAC", SqlDbType.NVarChar));
                cmd.Parameters["@WAC"].Value = Encrypt.EncryptData(clas.WAC, true, string.Empty);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "InsertCustomerModuleMapAudit";

                cmd.ExecuteNonQuery();

            }
            return true;
        }
    }
}
