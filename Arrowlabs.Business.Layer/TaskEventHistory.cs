﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public enum TaskAction
    {
        Update = 0,
        InProgress = 1,
        Complete = 2,
        Pending = 3,
        Rejected = 4,
        Accepted = 5,
        Assigned = 6,
        Cancelled = 7,
        OnRoute = 8,
        Pause = 9,
        Resume = 10
    }
    public class TaskEventHistory
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int Action { get; set; }
        public string CreatedBy { get; set; }
        public string Remarks { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Latitude { get; set; }
        public string Longtitude { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        public string action { get; set; }

        public bool isSubTask { get; set; }
        public string TaskName { get; set; }
        public string CustomerUName { get; set; }
        public string ACustomerUName { get; set; }

        public string CustomerFullName { get; set; }
        public string ACustomerFullName { get; set; }

        private static TaskEventHistory GetTaskEventHistoryFromSqlReader(SqlDataReader reader)
        {
            var taskEventHistory = new TaskEventHistory();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                taskEventHistory.Id = Convert.ToInt32(reader["Id"].ToString());

 
 
            

            if (reader["TaskId"] != DBNull.Value)
                taskEventHistory.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
            if (reader["Action"] != DBNull.Value)
                taskEventHistory.Action = Convert.ToInt32(reader["Action"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                taskEventHistory.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["Remarks"] != DBNull.Value)
                taskEventHistory.Remarks = reader["Remarks"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                taskEventHistory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                taskEventHistory.SiteId = Convert.ToInt32(reader["SiteId"]);
			
			            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                taskEventHistory.CustomerId = Convert.ToInt32(reader["CustomerId"]);


                        taskEventHistory.Latitude = string.Empty;
                        taskEventHistory.Longtitude = string.Empty;

            if (columns.Contains( "Latitude") && reader["Latitude"] != DBNull.Value)
                taskEventHistory.Latitude = reader["Latitude"].ToString();

            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                taskEventHistory.Longtitude = reader["Longitude"].ToString();

            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                taskEventHistory.CustomerUName = reader["CustomerUName"].ToString();
            if (columns.Contains( "ACustomerUName") && reader["ACustomerUName"] != DBNull.Value)
                taskEventHistory.ACustomerUName = reader["ACustomerUName"].ToString();

            if (columns.Contains("CustomerFullName") && reader["CustomerFullName"] != DBNull.Value)
                taskEventHistory.CustomerFullName = reader["CustomerFullName"].ToString();

            if (columns.Contains("ACustomerFullName") && reader["ACustomerFullName"] != DBNull.Value)
                taskEventHistory.ACustomerFullName = reader["ACustomerFullName"].ToString();

            if (columns.Contains("TaskName") && reader["TaskName"] != DBNull.Value)
                taskEventHistory.TaskName = reader["TaskName"].ToString();

            

            if (taskEventHistory.ACustomerUName == " ..")
            {
                taskEventHistory.ACustomerUName = taskEventHistory.Remarks;
            }

            return taskEventHistory;
        }

        public static bool InsertTaskEventHistory(TaskEventHistory eventHistory, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (eventHistory.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (eventHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskEventHistory", connection);

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = eventHistory.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@Action", SqlDbType.Int));
                    cmd.Parameters["@Action"].Value = eventHistory.Action;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = eventHistory.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = eventHistory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = eventHistory.CustomerId;


                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = eventHistory.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = eventHistory.Latitude;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new Arrowlabs.Mims.Audit.Models.TaskEventHistory();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, eventHistory.CreatedBy);
                            Reflection.CopyProperties(eventHistory, audit);
                            Arrowlabs.Mims.Audit.Models.TaskEventHistory.InsertTaskEventHistory(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertTaskEventHistoryTrue", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static List<TaskEventHistory> GetTaskEventHistoryByTaskId(int eventId, string dbConnection)
        {
            var eventHistories = new List<TaskEventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTaskEventHistoryByTaskId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = eventId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTaskEventHistoryByTaskId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetTaskEventHistoryFromSqlReader(reader);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static List<TaskEventHistory> GetTaskEventHistoryByUser(string user, string dbConnection)
        {
            var eventHistories = new List<TaskEventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTaskEventHistoryByUser", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@UName"].Value = user;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTaskEventHistoryByUser";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetTaskEventHistoryFromSqlReader(reader);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }
    }
}
