﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class  CustomEventAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string Name { get; set; }
        public int EventType { get; set; }
        public Guid Identifier { get; set; }
        public int EventId { get; set; }
        public DateTime RecevieTime { get; set; }
        public bool IsHotEvent { get; set; }
        public bool IsDriverOffence { get; set; }
        public string Status { get; set; }
        public string View { get; set; }
        public string UserName { get; set; }
        public string HandledBy { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int SiteId { get; set; }
        public static void InsertCustomEvent(CustomEventAudit customEvent, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(customEvent.BaseAudit, dbConnection);

            if (customEvent != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomEventAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Identifier", SqlDbType.UniqueIdentifier).Value = customEvent.Identifier;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = customEvent.Name;
                    cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = customEvent.UserName;
                    cmd.Parameters.Add("@HandledBy", SqlDbType.NVarChar).Value = customEvent.HandledBy;
                    cmd.Parameters.Add("@Longtitude", SqlDbType.Float).Value = customEvent.Longitude;
                    cmd.Parameters.Add("@Latitude", SqlDbType.Float).Value = customEvent.Latitude;
                    cmd.Parameters.Add("@EventType", SqlDbType.Int).Value = (int)customEvent.EventType;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = customEvent.SiteId;
                    cmd.Parameters.Add("@RecevieTime", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = customEvent.EventId;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        private static CustomEventAudit GetCustomEventAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var customEventAudit = new CustomEventAudit();
            customEventAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);


            if (columns.Contains("Identifier") && reader["Identifier"] != DBNull.Value)
                customEventAudit.Identifier = Guid.Parse((reader["Identifier"].ToString()));

            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                customEventAudit.Name = reader["Name"].ToString();

            if (columns.Contains("EventId") && reader["EventId"] != DBNull.Value)
                customEventAudit.EventId = Convert.ToInt16(reader["EventId"]);

            if (columns.Contains("RecevieTime") && reader["RecevieTime"] != DBNull.Value)
                customEventAudit.RecevieTime = Convert.ToDateTime(reader["RecevieTime"]);

            if (columns.Contains("EventType") && reader["EventType"] != DBNull.Value)
                customEventAudit.EventType = Convert.ToInt32(reader["EventType"].ToString());

            if (columns.Contains("Username") && reader["Username"] != DBNull.Value)
                customEventAudit.UserName = reader["Username"].ToString();
          
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                customEventAudit.SiteId =Convert.ToInt32(reader["SiteId"].ToString());


            return customEventAudit;
        }
        public static List<CustomEventAudit> GetAllCustomEvents(string dbConnection)
        {
            var customEvents = new List<CustomEventAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomEvents";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetCustomEventAuditFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }
    }
}
