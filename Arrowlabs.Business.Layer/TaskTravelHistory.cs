﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
        public class TaskTravelHistory
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int Action { get; set; }
       
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public static double GetTotalTravelTimebyTaskId(int taskid,string dbcon)
        {
            var total = Arrowlabs.Business.Layer.TaskTravelHistory.GetTaskTravelHistoryByTaskId(taskid, dbcon);
            DateTime start = DateTime.MinValue;
            DateTime end = DateTime.MinValue;
            double tot = 0;
            var count = 0;
            foreach (var t in total)
            {
                if (count == 0 && t.Action == 0)
                {
                    count++;
                    continue;
                }
                //count++;
                if (t.Action == 0)
                    end = t.CreatedDate.Value;
                else if (t.Action == 1)
                {
                    start = t.CreatedDate.Value;

                    count++;
                }
                if (start != DateTime.MinValue && end != DateTime.MinValue)
                {
                    var tot1 = end.Subtract(start).TotalMinutes;
                    tot = tot + tot1;
                    start = DateTime.MinValue;
                    end = DateTime.MinValue;

                }
            }
            return tot;
        }
        public static List<TaskTravelHistory> GetTaskTravelHistoryByTaskId(int taskId, string dbConnection)
        {
            var eventHistories = new List<TaskTravelHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTaskTravelHistoryByTaskId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTaskTravelHistoryByTaskId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetTaskTravelHistoryFromSqlReader(reader);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }
        private static TaskTravelHistory GetTaskTravelHistoryFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var taskEventHistory = new TaskTravelHistory();

            if (reader["Id"] != DBNull.Value)
                taskEventHistory.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["TaskId"] != DBNull.Value)
                taskEventHistory.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
            if (reader["Action"] != DBNull.Value)
                taskEventHistory.Action = Convert.ToInt32(reader["Action"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                taskEventHistory.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                taskEventHistory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                taskEventHistory.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (reader["CreatedDate"] != DBNull.Value)
                taskEventHistory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                taskEventHistory.CustomerId = Convert.ToInt32(reader["CustomerId"]);

            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                taskEventHistory.Latitude = Convert.ToSingle( reader["Latitude"].ToString());

            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                taskEventHistory.Longitude=Convert.ToSingle( reader["Longitude"].ToString());

            return taskEventHistory;
        }

        public static bool InsertTaskTravelHistory(TaskTravelHistory eventHistory, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (eventHistory.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (eventHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskTravelHistory", connection);

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = eventHistory.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@Action", SqlDbType.Int));
                    cmd.Parameters["@Action"].Value = eventHistory.Action;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;

                    
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = eventHistory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = eventHistory.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = eventHistory.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = eventHistory.Latitude;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new Arrowlabs.Mims.Audit.Models.TaskTravelHistoryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, eventHistory.CreatedBy);
                            Reflection.CopyProperties(eventHistory, audit);
                            Arrowlabs.Mims.Audit.Models.TaskTravelHistoryAudit.InsertTaskTravelHistoryAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertTaskTravelHistory", ex, dbConnection);
                    }
                }
            }
            return true;
        }
    }
}
