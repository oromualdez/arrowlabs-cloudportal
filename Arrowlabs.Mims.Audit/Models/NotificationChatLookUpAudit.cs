﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class NotificationChatLookUpAudit
    {
        public BaseAudit BaseAudit { get; set; }        
        public int Id { get; set; }        
        public int NotificationId { get; set; }   
        public int UserId { get; set; }        
        public bool IsDeleted { get; set; }        
        public bool IsRead { get; set; }        
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }
        public static bool InsertNotificationChatLookUpAudit(NotificationChatLookUpAudit notificationChatLookUp, string dbConnection)
        {
            if (notificationChatLookUp != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertNotificationChatLookUpAudit", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    var baseAuditId = BaseAudit.InsertBaseAudit(notificationChatLookUp.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = notificationChatLookUp.Id;
                    cmd.Parameters.Add("@NotificationId", SqlDbType.Int).Value = notificationChatLookUp.NotificationId;
                    cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = notificationChatLookUp.UserId;
                    cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = notificationChatLookUp.IsDeleted;
                    cmd.Parameters.Add("@IsRead", SqlDbType.Bit).Value = notificationChatLookUp.IsRead;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = notificationChatLookUp.CreatedDate;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = notificationChatLookUp.SiteId;

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            return false;
        }
    }
}
