﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class HappinessLevels
    {
        public int Id { get; set; } 
        public int SiteId { get; set; }
        public int CustomerId { get; set; } 
        public int SmileCount { get; set; }
        public int FrownCount { get; set; } 
        public int MehCount { get; set; } 
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        private static HappinessLevels GetHappinessLevelsFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();


            var notification = new HappinessLevels();

            if (reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["SmileCount"] != DBNull.Value)
                notification.SmileCount = Convert.ToInt32(reader["SmileCount"].ToString());

            if (reader["MehCount"] != DBNull.Value)
                notification.MehCount = Convert.ToInt32(reader["MehCount"].ToString());

            if (reader["FrownCount"] != DBNull.Value)
                notification.FrownCount = Convert.ToInt32(reader["FrownCount"].ToString()); 

            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                notification.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedBy"] != DBNull.Value)
                notification.UpdatedBy = reader["UpdatedBy"].ToString();  


            if (reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                notification.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
             
            return notification;
        }

        public static List<HappinessLevels> GetAllHappinessLevels(string dbConnection)
        {
            var assetList = new List<HappinessLevels>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllHappinessLevel", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var asset = GetHappinessLevelsFromSqlReader(reader);
                        assetList.Add(asset);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("HappinessLevels", "GetAllHappinessLevels", exp, dbConnection);

            }
            return assetList;
        }

        public static HappinessLevels GetHappinessLevelsById(int id, string dbConnection)
        {
            HappinessLevels asset = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetHappinessLevelById", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        asset = GetHappinessLevelsFromSqlReader(reader);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("HappinessLevels", "GetHappinessLevelsById", exp, dbConnection);

            }
            return asset;
        }

        public static List<HappinessLevels> GetAllHappinessLevelsBySiteId(int siteId, string dbConnection)
        {
            var itemFounds = new List<HappinessLevels>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllHappinessLevelBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetHappinessLevelsFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("HappinessLevels", "GetAllHappinessLevelsBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<HappinessLevels> GetAllHappinessLevelsByLevel5(int siteId, string dbConnection)
        {
            var itemFounds = new List<HappinessLevels>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllHappinessLevelByLevel5", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetHappinessLevelsFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("HappinessLevels", "GetAllHappinessLevelsByLevel5", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<HappinessLevels> GetAllHappinessLevelsByCustomerId(int siteId, string dbConnection)
        {
            var itemFounds = new List<HappinessLevels>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllHappinessLevelByCustomerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetHappinessLevelsFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("HappinessLevels", "GetAllHappinessLevelsByCustomerId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static int InsertorUpdateHappinessLevels(HappinessLevels notification, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateHappinessLevel", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@MehCount", SqlDbType.Int));
                    cmd.Parameters["@MehCount"].Value = notification.MehCount;

                    cmd.Parameters.Add(new SqlParameter("@FrownCount", SqlDbType.Int));
                    cmd.Parameters["@FrownCount"].Value = notification.FrownCount;

                    cmd.Parameters.Add(new SqlParameter("@SmileCount", SqlDbType.Int));
                    cmd.Parameters["@SmileCount"].Value = notification.SmileCount;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = notification.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = notification.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId; 

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateHappinessLevel";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            notification.Id = id;
                            var audit = new HappinessLevelAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, notification.CreatedBy);
                            Reflection.CopyProperties(notification, audit);
                            HappinessLevelAudit.InsertorUpdateHappinessLevelAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateHappinessLevels", ex, dbConnection);
                    }
                }
            }
            return id;
        }
    }
}
