﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{

    [Serializable]
    public class Accounts
    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Name { get; set; }
        public string ConnectionString { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CompanyName { get; set; }
        public string UpdateBy { get; set; }
        public string CreatedBy { get; set; }
        public int State { get; set; }
        //Module Mapping Reference
        //protected string isServerNotification;	--(int)Accounts.ModuleTypes.Notification
        //protected string isServerIncidents;   --(int)Accounts.ModuleTypes.Alarms
        //protected string isServerTasks;			--(int)Accounts.ModuleTypes.Tasks
        //protected string isServerTicketing; --(int)Accounts.ModuleTypes.Database
        //protected string isServerPostOrder;	--(int)Accounts.ModuleTypes.PostOrder
        //protected string isServerVerifier;	--(int)Accounts.ModuleTypes.Verifier
        //protected string isServerChat;	--(int)Accounts.ModuleTypes.Chat
        //protected string isServerDutyRoster;	--(int)Accounts.ModuleTypes.DutyRoster
        //protected string isServerLostAndFound;	--(int)Accounts.ModuleTypes.Warehouse
        //protected string isServerSurveillance;	--(int)Accounts.ModuleTypes.Surveillance
        //protected string isServerRequests;	--(int)Accounts.ModuleTypes.Request
        //protected string isServerCollaboration;	--(int)Accounts.ModuleTypes.Collaboration
        //protected string isServerDispatch;	--(int)Accounts.ModuleTypes.Dispatch
        //protected string isServerDashboard; --(int)Accounts.ModuleTypes.Dash
        //protected string isServerDevices;	--(int)Accounts.ModuleTypes.Devices
        //protected string isServerReports;	--(int)Accounts.ModuleTypes.Reports
        //protected string isServerCuser;	--(int)Accounts.ModuleTypes.System

        //protected string isMobileNotification; --(int)Accounts.ModuleTypes.MobileNotification
        //protected string isMobileIncidents; --(int)Accounts.ModuleTypes.MobileAlarms
        //protected string isMobileTasks; 	--(int)Accounts.ModuleTypes.MobileTasks
        //protected string isMobileTicketing;	--(int)Accounts.ModuleTypes.MobileTicketing
        //protected string isMobilePostOrder; --(int)Accounts.ModuleTypes.MobilePostOrder
        //protected string isMobileVerifier;	--(int)Accounts.ModuleTypes.MobileVerifier
        //protected string isMobileChat; --(int)Accounts.ModuleTypes.MobileConversation
        //protected string isMobileDutyRoster; --(int)Accounts.ModuleTypes.MobileDutyRoster
        //protected string isMobileLostAndFound;	--(int)Accounts.ModuleTypes.MobileAsset
        //protected string isMobileSurveillance;	--(int)Accounts.ModuleTypes.MobileSurveillance
        //protected string isMobileRequests;	--(int)Accounts.ModuleTypes.MobileRequest
        //protected string isMobileCollaboration;	--(int)Accounts.ModuleTypes.MobileCollaboration
        //protected string isMobileHotEvents; --(int)Accounts.ModuleTypes.MobileHotevent
        //protected string isMobileLocation;	--(int)Accounts.ModuleTypes.MobileLocation

        public enum ModuleTypes : int
        {
            Unknown = -1,
            None = 0,
            Dash = 1,
            Alarms = 2,
            Database = 3,
            Tasks = 4,
            Reports = 5,
            Notification = 6,
            Verifier = 7,
            Devices = 8,
            System = 9,
            MobileNotification = 10,
            MobileAlarms = 11,
            MobileTasks = 12,
            MobileHotevent = 13,
            MobileVerifier = 14,
            MobilePostOrder = 15,
            MobileDutyRoster = 16,
            MobileConversation = 17,
            MobileTicketing = 18,
            MobileAsset = 19,
            Surveillance = 20,
            Location = 21,
            Ticketing = 22,
            Incident = 23,
            Dispatch = 24,
            Warehouse = 25,
            Chat = 26,
            Collaboration = 27,
            Request = 28,
            LostandFound = 29,
            DutyRoster = 30,
            PostOrder = 31,
            MobileRequest = 32,
            MobileSurveillance = 33,
            MobileLocation = 34,
            MobileLostAndFound = 35,
            MobileCollaboration = 36,
            MobileKey = 37,
            ServerKey = 38,
            Activity = 39,
            MobileActivity = 40,
            Customer = 41,
            MessageBoard = 42
        }
        public static List<string> GetRoleList()
        {
            return Enum.GetNames(typeof(Role)).ToList();
        }
        private static Accounts GetAccountFromSqlReader(SqlDataReader reader)
        {
            var asset = new Accounts();
            if (reader["Id"] != DBNull.Value)
                asset.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                asset.Name = reader["Name"].ToString();
            if (reader["ConnectionString"] != DBNull.Value)
                asset.ConnectionString = reader["ConnectionString"].ToString();
            if (reader["CompanyName"] != DBNull.Value)
                asset.CompanyName = reader["CompanyName"].ToString();
            if (reader["Phone"] != DBNull.Value)
                asset.Phone = reader["Phone"].ToString();
            if (reader["Email"] != DBNull.Value)
                asset.Email = reader["Email"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                asset.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                asset.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                asset.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdateBy"] != DBNull.Value)
                asset.UpdateBy = reader["UpdateBy"].ToString();
            if (reader["State"] != DBNull.Value)
                asset.State = Convert.ToInt32(reader["State"].ToString());
            return asset;
        }

        public static Accounts GetAccountByNameAnyState(string name, string dbConnection)
        {
            var account = new Accounts();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAccountByNameAndState", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAccountByNameAndState";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    account = GetAccountFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return account;
        }
        public static Accounts GetAccountByName(string name,string dbConnection)
        {
            Accounts account =null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAccountByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAccountByName";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    account = GetAccountFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return account;
        }
        public static List<Accounts> GetAllAccountsByState(int id, string dbConnection)
        {
            var accounts = new List<Accounts>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAccountsByState", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@State", SqlDbType.Int));
                sqlCommand.Parameters["@State"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllAccountsByState";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var account = new Accounts();
                    account = GetAccountFromSqlReader(reader);
                    accounts.Add(account);
                }
                connection.Close();
                reader.Close();
            }
            return accounts;
        }
        public static List<Accounts> GetAllAccounts( string dbConnection)
        {
            var accounts = new List<Accounts>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAccounts", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllAccounts";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var account = new Accounts();
                    account = GetAccountFromSqlReader(reader);
                    accounts.Add(account);
                }
                connection.Close();
                reader.Close();
            }
            return accounts;
        }
        public static bool InsertOrUpdateAccounts(Accounts item, string dbConnection)
        {
            if (item != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateAccounts", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = item.Id;

                    cmd.Parameters.Add(new SqlParameter("@State", SqlDbType.Int));
                    cmd.Parameters["@State"].Value = item.State;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = item.Name;

                    cmd.Parameters.Add(new SqlParameter("@ConnectionString", SqlDbType.NVarChar));
                    cmd.Parameters["@ConnectionString"].Value = item.ConnectionString;

                    cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                    cmd.Parameters["@Email"].Value = item.Email;

                    cmd.Parameters.Add(new SqlParameter("@Phone", SqlDbType.NVarChar));
                    cmd.Parameters["@Phone"].Value = item.Phone;

                    cmd.Parameters.Add(new SqlParameter("@CompanyName", SqlDbType.NVarChar));
                    cmd.Parameters["@CompanyName"].Value = item.CompanyName;

                    cmd.Parameters.Add(new SqlParameter("@UpdateBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdateBy"].Value = item.UpdateBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = item.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = item.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = item.CreatedBy;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static bool DeleteAccountByName(string name, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteAccountByName", connection);

                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                cmd.Parameters["@Name"].Value = name;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteAccountByName";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
 

        //Users
        public static Users GetFullAccountUserFromSqlReader(SqlDataReader reader, string dbConn)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var userClass = new Users();

            if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                userClass.ID = Convert.ToInt32(reader["ID"].ToString());
            userClass.Username = reader["Username"].ToString();
            userClass.Password = reader["Password"].ToString();
            userClass.FirstName = reader["FirstName"].ToString();
            userClass.LastName = reader["LastName"].ToString();
            userClass.Email = reader["Email"].ToString();
            if (!string.IsNullOrEmpty(reader["RoleId"].ToString()))
                userClass.RoleId = Convert.ToInt32(reader["RoleId"].ToString());
            if (!string.IsNullOrEmpty(reader["Latitude"].ToString()))
                userClass.Latitude = Convert.ToSingle(reader["Latitude"].ToString());
            if (!string.IsNullOrEmpty(reader["Longitude"].ToString()))
                userClass.Longitude = Convert.ToSingle(reader["Longitude"].ToString());


            if (!string.IsNullOrEmpty(reader["LastLogin"].ToString()))
                userClass.LastLogin = Convert.ToDateTime(reader["LastLogin"].ToString());
            if (!string.IsNullOrEmpty(reader["CreatedBy"].ToString()))
                userClass.CreatedBy = reader["CreatedBy"].ToString();
            if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                userClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (!string.IsNullOrEmpty(reader["UpdatedDate"].ToString()))
                userClass.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (!string.IsNullOrEmpty(reader["UpdatedBy"].ToString()))
                userClass.UpdatedBy = reader["UpdatedBy"].ToString();

            // To check if it is mobile/tablet user.
            if (columns.Contains("DeviceType") && reader["DeviceType"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["DeviceType"].ToString()))
                    userClass.DeviceType = Convert.ToInt32(reader["DeviceType"].ToString());
            }
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["SiteId"].ToString()))
                    userClass.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            }
            userClass.Register = reader["Register"].ToString();

            if (userClass.Register == ((int)Arrowlabs.Business.Layer.Users.UserState.Disable).ToString())
                userClass.State = Arrowlabs.Business.Layer.Users.UserState.Disable.ToString();
            else if (userClass.Register == ((int)Arrowlabs.Business.Layer.Users.UserState.Enable).ToString())
                userClass.State = Arrowlabs.Business.Layer.Users.UserState.Enable.ToString();
            else if (userClass.Register == ((int)Arrowlabs.Business.Layer.Users.UserState.UnRegistered).ToString())
                userClass.State = Arrowlabs.Business.Layer.Users.UserState.UnRegistered.ToString();
            else if (userClass.Register == ((int)Arrowlabs.Business.Layer.Users.UserState.Curfew).ToString())
                userClass.State = Arrowlabs.Business.Layer.Users.UserState.Curfew.ToString();
            else
                userClass.State = Arrowlabs.Business.Layer.Users.UserState.Unknown.ToString();

            if (columns.Contains( "Engaged"))
                if (!string.IsNullOrEmpty(reader["Engaged"].ToString()))
                    userClass.Engaged = Convert.ToBoolean(reader["Engaged"]);

            if (columns.Contains( "EngagedIn"))
                userClass.EngagedIn = reader["EngagedIn"].ToString();

            if (columns.Contains( "AccountId"))
                userClass.AccountId = Convert.ToInt32(reader["AccountId"]);

            if (columns.Contains( "AccountName"))
                userClass.AccountName = reader["AccountName"].ToString();

            if (columns.Contains( "CustomerUName"))
                userClass.CustomerUName = reader["CustomerUName"].ToString();


            return userClass;
        }

        public static int InsertOrUpdateAccountUsers(Users usersClass, string dbConnection)
        {
            int id = 0;
            try
            {
                
                if (usersClass != null)
                {

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateUser", connection);

                        //cmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.NVarChar));
                        //cmd.Parameters["@UserID"].Value = usersClass.UserID;

                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = usersClass.ID;

                        cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                        cmd.Parameters["@Username"].Value = usersClass.Username;

                        cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                        cmd.Parameters["@Password"].Value = usersClass.Password;

                        cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                        cmd.Parameters["@Email"].Value = usersClass.Email;

                        cmd.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                        cmd.Parameters["@RoleId"].Value = usersClass.RoleId;

                        cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                        cmd.Parameters["@Latitude"].Value = usersClass.Latitude;

                        cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                        cmd.Parameters["@Longitude"].Value = usersClass.Longitude;

                        cmd.Parameters.Add(new SqlParameter("@LastLogin", SqlDbType.DateTime));
                        cmd.Parameters["@LastLogin"].Value = usersClass.LastLogin;

                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreatedDate"].Value = usersClass.CreatedDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = usersClass.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = usersClass.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@UpdatedBy"].Value = usersClass.UpdatedBy;

                        cmd.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                        cmd.Parameters["@FirstName"].Value = usersClass.FirstName;

                        cmd.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                        cmd.Parameters["@LastName"].Value = usersClass.LastName;

                        cmd.Parameters.Add(new SqlParameter("@Register", SqlDbType.NVarChar));
                        cmd.Parameters["@Register"].Value = usersClass.Register;

                        cmd.Parameters.Add(new SqlParameter("@DeviceType", SqlDbType.Int));
                        cmd.Parameters["@DeviceType"].Value = usersClass.DeviceType;

                        cmd.Parameters.Add(new SqlParameter("@Engaged", SqlDbType.Bit));
                        cmd.Parameters["@Engaged"].Value = usersClass.Engaged;

                        cmd.Parameters.Add(new SqlParameter("@EngagedIn", SqlDbType.NVarChar));
                        cmd.Parameters["@EngagedIn"].Value = usersClass.EngagedIn;

                        cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                        cmd.Parameters["@AccountId"].Value = usersClass.AccountId;

                        cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                        cmd.Parameters["@AccountName"].Value = usersClass.AccountName;

                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();

                        id = (int)returnParameter.Value;
                    }
                }
                return id;
            }
            catch (Exception ex)
            {
                return id;
            }
        }

        public static bool InsertUserModuleMap(int UserId,int ModuleId, string dbConnection,string username,string acctname)
        {
            if (UserId >0 && ModuleId >0)
            {
                

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertUserModuleMap", connection);

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = UserId;

                    cmd.Parameters.Add(new SqlParameter("@ModuleId", SqlDbType.Int));
                    cmd.Parameters["@ModuleId"].Value = ModuleId;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = acctname;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = username;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertUserModuleMap";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static List<Users> GetAllUserByRoleAndAccountName(int roleId,string acctname, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserByRoleAndAccountName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = acctname;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullAccountUserFromSqlReader(reader, dbConnection);
                                        userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        

        public static List<Users> GetAllMimsMobileOnlineUsers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOnlineUsers", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullAccountUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllMimsMobileOfflineUsers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOfflineUsers", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullAccountUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static void DeleteUserByUserName(string name,string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteUserByUserName", connection);

                cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                cmd.Parameters["@UserName"].Value = name;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteUserByUserName";

                cmd.ExecuteNonQuery();
            }
        }
        //Image
        public static bool InsertUserImage(int UserId, string ImagePath, byte[] ImageFile, string dbConnection,string acctname)
        {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertUserImage", connection);

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = UserId;

                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = ImagePath;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = acctname;

                    if (ImageFile != null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ImageFile", SqlDbType.VarBinary));
                        cmd.Parameters["@ImageFile"].Value = ImageFile;
                    }

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertUserImage";

                    cmd.ExecuteNonQuery();
                }
            
            return true;
        }

        //Insert Checklist
        public static bool InsertorUpdateTemplateCheckList(TemplateCheckList checkListItem, string dbConnection)
        {
            try
            {
                if (checkListItem != null)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateTemplateCheckList", connection);

                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = checkListItem.Id;

                        cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                        cmd.Parameters["@Name"].Value = checkListItem.Name;

                        cmd.Parameters.Add(new SqlParameter("@MainLocationId", SqlDbType.Int));
                        cmd.Parameters["@MainLocationId"].Value = checkListItem.MainLocationId;

                        cmd.Parameters.Add(new SqlParameter("@SubLocationId", SqlDbType.Int));
                        cmd.Parameters["@SubLocationId"].Value = checkListItem.SubLocationId;

                        cmd.Parameters.Add(new SqlParameter("@CheckListType", SqlDbType.Int));
                        cmd.Parameters["@CheckListType"].Value = checkListItem.CheckListType;

                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreatedDate"].Value = checkListItem.CreatedDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = checkListItem.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = checkListItem.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                        cmd.Parameters["@IsDeleted"].Value = checkListItem.IsDeleted;

                        cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                        cmd.Parameters["@AccountId"].Value = checkListItem.AccountId;

                        cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                        cmd.Parameters["@AccountName"].Value = checkListItem.AccountName;

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.ExecuteNonQuery();

                    }
                    return true;
                }
                
            }
            catch(Exception ex)
            {
                return false;
            }
            return false;
        }

        //Insert Locations
        public static bool InsertorUpdateLocationType(LocationType locationType, string dbConnection)
        {
            if (locationType != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateLocationType", connection);

                    cmd.Parameters.Add(new SqlParameter("@LocationTypeId", SqlDbType.Int));
                    cmd.Parameters["@LocationTypeId"].Value = locationType.LocationTypeId;

                    cmd.Parameters.Add(new SqlParameter("@LocationTypeDesc", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationTypeDesc"].Value = locationType.LocationTypeDesc;

                    cmd.Parameters.Add(new SqlParameter("@LocationTypeDesc_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationTypeDesc_AR"].Value = locationType.LocationTypeDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = locationType.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = locationType.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = locationType.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                    cmd.Parameters["@AccountId"].Value = locationType.AccountId;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = locationType.AccountName;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateLocationType";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static bool InsertorUpdateLocation(Location location, string dbConnection)
        {
            if (location != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateLocation", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = location.ID;

                    cmd.Parameters.Add(new SqlParameter("@LocationDesc", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationDesc"].Value = location.LocationDesc;

                    cmd.Parameters.Add(new SqlParameter("@LocationDesc_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationDesc_AR"].Value = location.LocationDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@LocationTypeId", SqlDbType.Int));
                    cmd.Parameters["@LocationTypeId"].Value = location.LocationTypeId;

                    cmd.Parameters.Add(new SqlParameter("@FullLocationDesc_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@FullLocationDesc_AR"].Value = location.FullLocationDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreateDate"].Value = location.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = location.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = location.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ParentLocationId", SqlDbType.Int));
                    cmd.Parameters["@ParentLocationId"].Value = location.ParentLocationId;

                    cmd.Parameters.Add(new SqlParameter("@ParentLocationName", SqlDbType.NVarChar));
                    cmd.Parameters["@ParentLocationName"].Value = location.ParentLocationName;

                    cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                    cmd.Parameters["@AccountId"].Value = location.AccountId;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = location.AccountName;

                    try
                    {
                        if (!string.IsNullOrEmpty(location.Latitude.ToString()))
                        {
                            cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                            cmd.Parameters["@Latitude"].Value = location.Latitude;
                        }
                        if (!string.IsNullOrEmpty(location.Longitude.ToString()))
                        {
                            cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                            cmd.Parameters["@Longitude"].Value = location.Longitude;
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateLocation";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public static bool InsertorUpdateOffence(Offence offence, string dbConnection)
        {

            if (offence != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateOffence", connection);

                    cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int));
                    cmd.Parameters["@ID"].Value = offence.ID;

                    cmd.Parameters.Add(new SqlParameter("@OffenceDesc", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceDesc"].Value = offence.OffenceDesc;

                    cmd.Parameters.Add(new SqlParameter("@OffenceDesc_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@OffenceDesc_AR"].Value = offence.OffenceDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@OffenceTypeID", SqlDbType.Int));
                    cmd.Parameters["@OffenceTypeID"].Value = offence.OffenceTypeID;

                    cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreateDate"].Value = offence.CreateDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = offence.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = offence.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                    cmd.Parameters["@AccountId"].Value = offence.AccountId;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = offence.AccountName;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateOffence";

                    cmd.ExecuteNonQuery();

                    
                }
                return true;
            }
            return false;
        }

        public static bool InsertorUpdateOffenceType(OffenceType offenceType, string dbConnection)
        {
            if (offenceType != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateOffenceType", connection);

                    cmd.Parameters.Add(new SqlParameter("@OFFENCETYPEID", SqlDbType.Int));
                    cmd.Parameters["@OFFENCETYPEID"].Value = offenceType.OffenceTypeId;

                    cmd.Parameters.Add(new SqlParameter("@OFFENCETYPEDESC", SqlDbType.NVarChar));
                    cmd.Parameters["@OFFENCETYPEDESC"].Value = offenceType.OffenceTypeDesc;

                    cmd.Parameters.Add(new SqlParameter("@OFFENCETYPEDESC_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@OFFENCETYPEDESC_AR"].Value = offenceType.OffenceTypeDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = offenceType.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = offenceType.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = offenceType.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                    cmd.Parameters["@AccountId"].Value = offenceType.AccountId;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = offenceType.AccountName;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateOffenceType";

                    cmd.ExecuteNonQuery();


                }
                return true;
            }
            return false;
        }

        public static bool InsertorUpdateGroup(Group group, string dbConnection)
        {
            if (group != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateGroup", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = group.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = group.Name;

                    cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreateDate"].Value = group.CreateDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = group.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = group.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                    cmd.Parameters["@AccountId"].Value = group.AccountId;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = group.AccountName;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertorUpdateGroup";

                    cmd.ExecuteNonQuery();



                }
            }
            return true;
        }

        public static int InsertorUpdateVehicleMake(VehicleMake vehicleMake, string dbConnection)
        {
            var id = 0;
            if (vehicleMake != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVehicleMake", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = vehicleMake.ID;

                    cmd.Parameters.Add(new SqlParameter("@VEHICLEMAKEDESC", SqlDbType.NVarChar));
                    cmd.Parameters["@VEHICLEMAKEDESC"].Value = vehicleMake.VehicleMakeDesc;

                    cmd.Parameters.Add(new SqlParameter("@VEHICLEMAKEDESC_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@VEHICLEMAKEDESC_AR"].Value = vehicleMake.VehicleMakeDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@VEHICLEMAKECODE", SqlDbType.NVarChar));
                    cmd.Parameters["@VEHICLEMAKECODE"].Value = vehicleMake.VehicleMakeCode;


                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = vehicleMake.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = vehicleMake.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = vehicleMake.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                    cmd.Parameters["@AccountId"].Value = vehicleMake.AccountId;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = vehicleMake.AccountName;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateVehicleMake";

                    cmd.ExecuteNonQuery();
                }
            }
            return id;
        }
        public static bool InsertorUpdateVehicleColor(VehicleColor vehicleColor, string dbConnection)
        {
            
            if (vehicleColor != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVehicleColor", connection);

                    cmd.Parameters.Add(new SqlParameter("@COLORCODE", SqlDbType.Int));
                    cmd.Parameters["@COLORCODE"].Value = vehicleColor.ColorCode;

                    cmd.Parameters.Add(new SqlParameter("@COLORDESC", SqlDbType.NVarChar));
                    cmd.Parameters["@COLORDESC"].Value = vehicleColor.ColorDesc;

                    cmd.Parameters.Add(new SqlParameter("@COLORDESC_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@COLORDESC_AR"].Value = vehicleColor.ColorDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = vehicleColor.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = vehicleColor.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = vehicleColor.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                    cmd.Parameters["@AccountId"].Value = vehicleColor.AccountId;

                    cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                    cmd.Parameters["@AccountName"].Value = vehicleColor.AccountName;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateVehicleColor";

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            return false;
        }
        public static bool DeleteLocationTypeByIdAndAccountName(int locationId, string acct, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteLocationTypeByIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;
                cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                cmd.Parameters["@AccountName"].Value = acct;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool DeleteLocationByIdAndAccountName(int locationId, string acct, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteLocationByIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;
                cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                cmd.Parameters["@AccountName"].Value = acct;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteOffenceByIdAndAccountName(int locationId, string acct, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteOffenceByIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;
                cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                cmd.Parameters["@AccountName"].Value = acct;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteOffenceTypeByIdAndAccountName(int locationId, string acct, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteOffenceTypeByIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;
                cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                cmd.Parameters["@AccountName"].Value = acct;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteVehicleMakeByIdAndAccountName(int locationId, string acct, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteVehicleMakeByIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;
                cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                cmd.Parameters["@AccountName"].Value = acct;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteVehicleColorByIdAndAccountName(int locationId, string acct, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteVehicleColorByIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;
                cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                cmd.Parameters["@AccountName"].Value = acct;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteTaskByTaskIdAndAccountName(int locationId, string acct, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteTaskByTaskIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                cmd.Parameters["@TaskId"].Value = locationId;
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                cmd.Parameters["@Name"].Value = acct;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool InsertUserImage(int UserId, string ImagePath, byte[] ImageFile, string dbConnection, string acctname, string createdBy)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("InsertUserImage", connection);

                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = UserId;

                cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                cmd.Parameters["@ImagePath"].Value = ImagePath;

                cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                cmd.Parameters["@AccountName"].Value = acctname;

                cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                cmd.Parameters["@CreatedBy"].Value = createdBy;

                cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                cmd.Parameters["@CreatedDate"].Value = DateTime.Now;

                if (ImageFile != null)
                {
                    cmd.Parameters.Add(new SqlParameter("@ImageFile", SqlDbType.VarBinary));
                    cmd.Parameters["@ImageFile"].Value = ImageFile;
                }

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "InsertUserImage";

                cmd.ExecuteNonQuery();
            }

            return true;
        }
        public static UserImage GetUserImageByUserIdAndAccountName(int userid, string acctname, string dbConnection)
        {
            UserImage userimg = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetUserImageByUserIdAndAccountName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = userid;
                sqlCommand.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@AccountName"].Value = acctname;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetUserImageByUserIdAndAccountName";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    userimg = new UserImage();
                    if (reader["UserId"] != DBNull.Value)
                        userimg.UserId = Convert.ToInt32(reader["UserId"].ToString());
                    if (reader["ImagePath"] != DBNull.Value)
                        userimg.ImagePath = reader["ImagePath"].ToString();
                    if (reader["AccountName"] != DBNull.Value)
                        userimg.AccountName = reader["AccountName"].ToString();
                    if (reader["CreatedBy"] != DBNull.Value)
                        userimg.CreatedBy = reader["CreatedBy"].ToString();
                    if (reader["ImageFile"] != DBNull.Value)
                        userimg.ImageFile = (byte[])reader["ImageFile"];
                    if (reader["CreatedDate"] != DBNull.Value)
                        userimg.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

                }
                connection.Close();
                reader.Close();
            }
            return userimg;
        }
        public static Users GetDirectorByManagerName(string name, string dbConnection)
        {
            Users userClass = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetDirectorByManagerName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ManagerName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullAccountUserFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }
        public static bool DeleteGroupByGroupIdAndAccountName(int locationId, string acct, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteGroupByGroupIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                cmd.Parameters["@GroupId"].Value = locationId;
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                cmd.Parameters["@Name"].Value = acct;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteRecurringTaskByTaskIdAndDateAndAccountname(int taskId, DateTime dt,string acct ,string dbConnection)
        {
            //if (GroupDeviceTask.DeleteGroupDeviceTaskByTaskId(taskId, dbConnection))
            //{
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteRecurringTaskByTaskIdAndDateAndAccountname", connection);

                cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                cmd.Parameters["@TaskId"].Value = taskId;
                cmd.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                cmd.Parameters["@Date"].Value = dt;
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                cmd.Parameters["@Name"].Value = acct;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteRecurringTaskByTaskIdAndDateAndAccountname";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        //Delete

       // public static bool 
    }
}
