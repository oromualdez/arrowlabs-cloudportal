﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ServiceStatus
    {
       public string AlarmService { get; set; }
       public string AlarmStatus { get; set; }

       public string MessageService { get; set; }
       public string MessageStatus { get; set; }

       public string RTSPService { get; set; }
       public string RTSPStatus { get; set; }

       public string NotificationService { get; set; }
       public string NotificationStatus { get; set; }        

    }
}
