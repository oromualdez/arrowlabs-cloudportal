﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class VerifierService
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public string TypeName { get; set; }

        public enum VerifierServiceTypes
        {
            FRS = 0,
            ANPR = 1
        }
        private static VerifierService GetVerifierServiceFromSqlReader(SqlDataReader reader)
        {
            var verifierService = new VerifierService();
            if (reader["Id"] != DBNull.Value)
                verifierService.Id = Convert.ToInt16(reader["Id"].ToString());
            if (reader["Url"] != DBNull.Value)
                verifierService.Url = reader["Url"].ToString();
            if (reader["Type"] != DBNull.Value)
                verifierService.Type = Convert.ToInt16(reader["Type"].ToString());

            if (verifierService.Type == (int)VerifierServiceTypes.ANPR)
                verifierService.TypeName = "ANPR";
            else
                verifierService.TypeName = "FRS";
          
         
            if (reader["CreatedBy"] != DBNull.Value)
                verifierService.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedBy"] != DBNull.Value)
                verifierService.UpdatedBy = reader["UpdatedBy"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                verifierService.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                verifierService.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
       
            return verifierService;
        }
        public static bool InsertOrUpdateVerifierService(VerifierService verifierService, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (verifierService != null)
            {
                int id = verifierService.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVerifierService", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = verifierService.Id;

                    cmd.Parameters.Add(new SqlParameter("@Url", SqlDbType.NVarChar));
                    cmd.Parameters["@Url"].Value = verifierService.Url;
                  
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = verifierService.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = verifierService.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = verifierService.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = verifierService.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                    cmd.Parameters["@Type"].Value = verifierService.Type;


                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateVerifierService";

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            verifierService.Id = id;
                            var audit = new VerifierServiceAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, verifierService.UpdatedBy);
                            Reflection.CopyProperties(verifierService, audit);
                            VerifierServiceAudit.InsertVerifierServiceAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateVerifierService", ex, auditDbConnection);
                    }
                }
            }
            return true;
        }
        public static List<VerifierService> GetAllVerifierService(string dbConnection)
        {
            var verifierServices = new List<VerifierService>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllVerifierService";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifierService = GetVerifierServiceFromSqlReader(reader);
                    verifierServices.Add(verifierService);
                }
                connection.Close();
                reader.Close();
            }
            return verifierServices;
        }
    }
}
