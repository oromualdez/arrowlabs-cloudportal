﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
   public class RosterViewAudit
   {
       public BaseAudit BaseAudit { get; set; }  
       public int Id { get; set; }
       public int EmployeeId { get; set; }
       public string TotalDayLieu { get; set; }
       public string TotalDayOff { get; set; }
       public string CurrentMonth { get; set; }
       public string CurrentDayOff { get; set; }
       public string CurrentDayLieu { get; set; }
       public bool ActiveRoster { get; set; }
       public int SiteId { get; set; }
       public int Amendment { get; set; }
       
       public DateTime CreatedDate { get; set; }
       public DateTime UpdatedDate { get; set; }
       public string CreatedBy { get; set; }
       public string UpdatedBy { get; set; }

       public string Notes { get; set; }

       public static bool InsertOrUpdateRosterViewAudit(RosterViewAudit rosterViewAudit, string dbConnection)
       {
           var baseAuditId = BaseAudit.InsertBaseAudit(rosterViewAudit.BaseAudit, dbConnection);

           if (rosterViewAudit != null)
           {
               using (var connection = new SqlConnection(dbConnection))
               {
                   connection.Open();
                   var cmd = new SqlCommand("InsertOrUpdateRosterViewAudit", connection);

                   cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                   cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                   cmd.Parameters["@Id"].Value = rosterViewAudit.Id;
                   cmd.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.Int));
                   cmd.Parameters["@EmployeeId"].Value = rosterViewAudit.EmployeeId;
                   cmd.Parameters.Add(new SqlParameter("@TotalDayLieu", SqlDbType.NVarChar));
                   cmd.Parameters["@TotalDayLieu"].Value = rosterViewAudit.TotalDayLieu;
                   cmd.Parameters.Add(new SqlParameter("@TotalDayOff", SqlDbType.NVarChar));
                   cmd.Parameters["@TotalDayOff"].Value = rosterViewAudit.TotalDayOff;
                   cmd.Parameters.Add(new SqlParameter("@CurrentMonth", SqlDbType.NVarChar));
                   cmd.Parameters["@CurrentMonth"].Value = rosterViewAudit.CurrentMonth;
                   cmd.Parameters.Add(new SqlParameter("@CurrentDayOff", SqlDbType.NVarChar));
                   cmd.Parameters["@CurrentDayOff"].Value = rosterViewAudit.CurrentDayOff;
                   cmd.Parameters.Add(new SqlParameter("@CurrentDayLieu", SqlDbType.NVarChar));
                   cmd.Parameters["@CurrentDayLieu"].Value = rosterViewAudit.CurrentDayLieu;
                   cmd.Parameters.Add(new SqlParameter("@ActiveRoster", SqlDbType.Bit));
                   cmd.Parameters["@ActiveRoster"].Value = rosterViewAudit.ActiveRoster;
                   cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                   cmd.Parameters["@SiteId"].Value = rosterViewAudit.SiteId;
                   cmd.Parameters.Add(new SqlParameter("@Amendment", SqlDbType.Int));
                   cmd.Parameters["@Amendment"].Value = rosterViewAudit.Amendment; 
                   cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                   cmd.Parameters["@CreatedDate"].Value = rosterViewAudit.CreatedDate;
                   cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                   cmd.Parameters["@UpdatedDate"].Value = rosterViewAudit.UpdatedDate;
                   cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                   cmd.Parameters["@CreatedBy"].Value = rosterViewAudit.CreatedBy;

                   cmd.Parameters.Add(new SqlParameter("@Notes", SqlDbType.NVarChar));
                   cmd.Parameters["@Notes"].Value = rosterViewAudit.Notes;
                    
                   cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                   cmd.Parameters["@UpdatedBy"].Value = rosterViewAudit.UpdatedBy;
                   cmd.CommandType = CommandType.StoredProcedure;
                   cmd.ExecuteNonQuery();

               }
               return true;
           }
           return false;
       }
    }
}
