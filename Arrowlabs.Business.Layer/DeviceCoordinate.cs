﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class DeviceCoordinate
    {
       public string MacAddress { get; set; }
       public float Longitude { get; set; }
       public float Latitude { get; set; }
       public string PcName { get; set; }

       public string UpdatedBy { get; set; }
    }
}
