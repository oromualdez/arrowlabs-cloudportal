﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class Asset
    {
        [DisplayName("Variant")]
        public int Id { get; set; }
        [DisplayName("Variant")]
        public string Name { get; set; }
        public int Quantity { get; set; }
        [DisplayName("Details")]
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        [DisplayName("Asset")]
        public int AssetCategoryId { get; set; }
        public int ParentAssetCategoryId { get; set; }
        public int ParentLocationId { get; set; }
        public string AssetCategoryName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public List<ItemLost> AssetsList { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public string StoreLoc { get; set; }
        public string SubStoreLoc { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }

        public string CustomerUName { get; set; }

        public string SearchName { get; set; }

        private static Asset GetAssetFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var asset = new Asset();
            if (reader["Id"] != DBNull.Value)
                asset.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                asset.Name = reader["Name"].ToString();
            if (reader["Quantity"] != DBNull.Value)
                asset.Quantity = Convert.ToInt32(reader["Quantity"].ToString());
            if (reader["LocationId"] != DBNull.Value)
                asset.LocationId = Convert.ToInt32(reader["LocationId"].ToString());
            if (reader["AssetCategoryId"] != DBNull.Value)
                asset.AssetCategoryId = Convert.ToInt32(reader["AssetCategoryId"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                asset.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                asset.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                asset.CreatedBy = Convert.ToInt32(reader["CreatedBy"].ToString());

            if (reader["LocationName"] != DBNull.Value)
                asset.LocationName = reader["LocationName"].ToString();
            if (reader["AssetCategoryName"] != DBNull.Value)
                asset.AssetCategoryName = reader["AssetCategoryName"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                asset.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                asset.CustomerId = Convert.ToInt32(reader["CustomerId"]);

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                asset.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("SiteId2") && reader["SiteId2"] != DBNull.Value)
            {
                if (asset.SiteId == 0)
                    asset.SiteId = Convert.ToInt32(reader["SiteId2"]);
            }

            return asset;
        }

        /// <summary>
        /// It will return all assets in stock
        /// </summary>
        /// <param name="dbConnection">connection string to database</param>
        /// <returns>list of available assets</returns>
        public static Asset GetAssetByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            Asset asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAssetByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    asset = GetAssetFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return asset;
        }
        public static List<Asset> GetAllAsset(string dbConnection)
        {
            var assetList = new List<Asset>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAsset", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = GetAssetFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }
        public static List<Asset> GetAllAssetByCustomerId(int id, string dbConnection)
        {
            var assetList = new List<Asset>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAssetByCustomerId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = GetAssetFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        public static List<Asset> GetAllAssetByAssetCategoryId(int id, string dbConnection)
        {
            var assetList = new List<Asset>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAssetByAssetCategoryId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = GetAssetFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        public static List<Asset> GetAllAssetByLevel5(int id, string dbConnection)
        {
            var assetList = new List<Asset>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAssetByLevel5", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@UserId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = GetAssetFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        public static List<Asset> GetAllAssetBySiteId(int id, string dbConnection)
        {
            var assetList = new List<Asset>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAssetBySiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@SiteId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var asset = GetAssetFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        /// <summary>
        /// It will return assets which match with provided name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<Asset> GetAssetByName(string name, string dbConnection)
        {
            var assetList = new List<Asset>();
            Asset asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAssetByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetAssetFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        /// <summary>
        /// It will return asset which matches with provided name, location and category
        /// </summary>
        /// <param name="name"></param>
        /// <param name="locationId"></param>
        /// <param name="assetCategoryId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<Asset> GetAssetByNameLocationCategory(string name, int locationId, int assetCategoryId, string dbConnection)
        {
            var assetList = new List<Asset>();
            Asset asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAssetByNameLocationCategory", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                sqlCommand.Parameters["@LocationId"].Value = locationId;

                sqlCommand.Parameters.Add(new SqlParameter("@AssetCategoryId", SqlDbType.Int));
                sqlCommand.Parameters["@AssetCategoryId"].Value = assetCategoryId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetAssetFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        /// <summary>
        /// It will return asset which matches with provided name and category
        /// </summary>
        /// <param name="name"></param>
        /// <param name="assetCategoryId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<Asset> GetAssetByNameCategory(string name, int assetCategoryId, string dbConnection)
        {
            var assetList = new List<Asset>();
            Asset asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {

                connection.Open();
                var sqlCommand = new SqlCommand("GetAssetByNameCategory", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@AssetCategoryId", SqlDbType.Int));
                sqlCommand.Parameters["@AssetCategoryId"].Value = assetCategoryId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetAssetFromSqlReader(reader);
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        /// <summary>
        /// It will return asset based on its unique Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static Asset GetAssetById(int id, string dbConnection)
        {
            Asset asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAssetById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetAssetFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return asset;
        }

        public static bool DeleteAssetbyId(int id, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true, string auditBy = "")
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteAssetbyId", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

                try
                {
                    if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection) && !string.IsNullOrEmpty(auditBy))
                    {
                        var bAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.BeforeDelete, auditBy, "DeleteAssetbyId");
                        BaseAudit.InsertBaseAudit(bAudit, auditDbConnection);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Business Layer", "DeleteAssetbyId", ex, dbConnection);
                }


            }
            return true;
        }

        public static List<Asset> GetAllAssetByAssetIdAndAssetSubIdAndLocation(int locationId, int assetCategoryId, string dbConnection, bool isKey, Users getUser)
        {
            var assetList = new List<Asset>();
            Asset asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllAssetByAssetIdAndAssetSubIdAndLocation", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                sqlCommand.Parameters["@LocationId"].Value = locationId;

                sqlCommand.Parameters.Add(new SqlParameter("@AssetCategoryId", SqlDbType.Int));
                sqlCommand.Parameters["@AssetCategoryId"].Value = assetCategoryId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetAssetFromSqlReader(reader);
                    var assetlist = ItemLost.GetAllItemLostByLocationIdAndAssetCatId(locationId, assetCategoryId, dbConnection);
                    if (isKey)
                    {
                        assetlist = assetlist.Where(i => i.isKey == true).ToList();
                    }
                    else
                    {
                        assetlist = assetlist.Where(i => i.isKey == false).ToList();
                    }
                    if (assetlist.Count > 0)
                    {
                        if (getUser.RoleId == (int)Role.Manager || getUser.RoleId == (int)Role.Admin || getUser.RoleId == (int)Role.Director || getUser.RoleId == (int)Role.Operator || getUser.RoleId == (int)Role.UnassignedOperator)
                            assetlist = assetlist.Where(i => i.SiteId == getUser.SiteId).ToList();
                        else if (getUser.RoleId == (int)Role.Regional)
                        {
                            var alist = assetlist;
                            var newList = new List<ItemLost>();
                            var sites = UserSiteMap.GetAllUserSiteMapByUsername(getUser.Username, dbConnection);
                            foreach (var site in sites)
                            {
                                var dlist = alist.Where(i => i.SiteId == site.SiteId).ToList();
                                newList.AddRange(dlist);
                            }
                            var clist = alist.Where(i => i.SiteId == 0).ToList();
                            newList.AddRange(clist);
                            assetlist = newList;
                        }

                        asset.AssetsList = assetlist;

                        asset.Quantity = asset.AssetsList.Count;
                    }
                }
                if (asset != null)
                {
                    assetList.Add(asset);
                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }

        public static List<Asset> GetAllAssetByLocationId(int locationId, string dbConnection, bool isKey, Users getUser)
        {
            var assetList = new List<Asset>();
            Asset asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllAssetByLocationId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                sqlCommand.Parameters["@LocationId"].Value = locationId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetAssetFromSqlReader(reader);
                    //asset.AssetsList = ItemLost.GetAllItemLostByLocationIdAndAssetCatId(locationId, asset.Id, dbConnection);
                    var assetlist = ItemLost.GetAllItemLostByLocationIdAndAssetCatId(locationId, asset.Id, dbConnection);
                    if (isKey)
                    {
                        assetlist = assetlist.Where(i => i.isKey == true).ToList();
                    }
                    else
                    {
                        assetlist = assetlist.Where(i => i.isKey == false).ToList();
                    }
                    //asset.AssetsList = assetlist;
                    if (assetlist.Count > 0)
                    {
                        if (getUser.RoleId == (int)Role.Manager || getUser.RoleId == (int)Role.Admin || getUser.RoleId == (int)Role.Director || getUser.RoleId == (int)Role.Operator || getUser.RoleId == (int)Role.UnassignedOperator)
                            assetlist = assetlist.Where(i => i.SiteId == getUser.SiteId).ToList();
                        else if (getUser.RoleId == (int)Role.Regional)
                        {
                            var alist = assetlist;
                            var newList = new List<ItemLost>();
                            var sites = UserSiteMap.GetAllUserSiteMapByUsername(getUser.Username, dbConnection);
                            foreach (var site in sites)
                            {
                                var dlist = alist.Where(i => i.SiteId == site.SiteId).ToList();
                                newList.AddRange(dlist);
                            }
                            var clist = alist.Where(i => i.SiteId == 0).ToList();
                            newList.AddRange(clist);
                            assetlist = newList;
                        }

                        asset.AssetsList = assetlist;
                    }
                    if (assetlist.Count > 0)
                        assetList.Add(asset);

                }
                connection.Close();
                reader.Close();
            }
            return assetList;
        }
        public static bool InsertOrUpdateAsset(Asset assetCategory, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (assetCategory.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (assetCategory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateAsset", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = assetCategory.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = assetCategory.Name;

                    cmd.Parameters.Add(new SqlParameter("@Quantity", SqlDbType.Int));
                    cmd.Parameters["@Quantity"].Value = assetCategory.Quantity;

                    cmd.Parameters.Add(new SqlParameter("@AssetCategoryId", SqlDbType.Int));
                    cmd.Parameters["@AssetCategoryId"].Value = assetCategory.AssetCategoryId;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = assetCategory.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = assetCategory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = assetCategory.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.Int));
                    cmd.Parameters["@CreatedBy"].Value = assetCategory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = assetCategory.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = assetCategory.CustomerId;


                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    //try
                    //{
                    //    if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                    //    {
                    //        var audit = new AssetCategoryAudit();
                    //        audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, assetCategory.UpdatedBy);
                    //        Reflection.CopyProperties(assetCategory, audit);
                    //        AssetCategoryAudit.InsertAssetCategoryAudit(audit, auditDbConnection);
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateAssetCategory", ex, dbConnection);
                    //}
                }
            }
            return true;
        }
    }
}
