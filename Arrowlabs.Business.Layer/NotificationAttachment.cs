﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class NotificationAttachment
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public string AttachmentPath { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        private static NotificationAttachment GetNotificationAttachmentFromSqlReader(SqlDataReader reader)
        {
            var notificationAttachment = new NotificationAttachment();

            if (reader["Id"] != DBNull.Value)
                notificationAttachment.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["NotificationId"] != DBNull.Value)
                notificationAttachment.NotificationId = Convert.ToInt32(reader["NotificationId"].ToString());
            if (reader["AttachmentPath"] != DBNull.Value)
                notificationAttachment.AttachmentPath = reader["AttachmentPath"].ToString();
            if (reader["IsActive"] != DBNull.Value)
                notificationAttachment.IsActive = (String.IsNullOrEmpty(reader["IsActive"].ToString())) ? false : Convert.ToBoolean(reader["IsActive"].ToString());
            if (reader["IsDeleted"] != DBNull.Value)
                notificationAttachment.IsDeleted = (String.IsNullOrEmpty(reader["IsDeleted"].ToString())) ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                notificationAttachment.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                notificationAttachment.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notificationAttachment.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedBy"] != DBNull.Value)
                notificationAttachment.UpdatedBy = reader["UpdatedBy"].ToString();
           
            return notificationAttachment;
        }

        public static List<NotificationAttachment> GetNotificationAttachmentsByNotificationId(int notificationId, string dbConnection)
        {
            var notificationAttachments = new List<NotificationAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetNotificationAttachmentsByNotificationId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.Int)).Value = notificationId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    notificationAttachments.Add(GetNotificationAttachmentFromSqlReader(resultSet));
                }
                resultSet.Close();
                connection.Close();
                
                return notificationAttachments;
            }

        }
    }
}
