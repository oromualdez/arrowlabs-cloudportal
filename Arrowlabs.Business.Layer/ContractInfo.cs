﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ContractInfo
    {
        public int Id { get; set; }
        public int SystemTypeId { get; set; }
        public int CustomerId { get; set; }
        public string ContractRef { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int PPMVisitPerYear { get; set; }
        public int PPMTotalHour { get; set; }
        public int TotalPPMHour { get; set; }
        public int SRVisitPerYear { get; set; }
        public int SRHourVisitPer { get; set; }
        public int SRTotalHours { get; set; }
        public int SRTotal { get; set; }
        public string ScopeOfWorks { get; set; }
        public string SLA { get; set; }
        public string AnnualContractValue { get; set; }
        public string TotalContractValue { get; set; }
        public string MonthlyContractValue { get; set; }
        public string Remarks { get; set; }
        public string PaymentTerms { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int SiteId { get; set; }
        public int ContractType { get; set; }
        public bool Active { get; set; }

        public bool IsEmailed { get; set; }

        public int CustomerInfoId { get; set; }
        public string CustomerUName { get; set; }

        public string CustomerFullName { get; set; }

        public enum ContractTypes
        {
            SR = 0, //Level 7
            PPM = 1
        }

        public static int InsertOrUpdateContractInfo(ContractInfo contractInfo, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = contractInfo.Id;

            var action = (contractInfo.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (contractInfo != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateContractInfo", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = contractInfo.Id;


                    cmd.Parameters.Add(new SqlParameter("@SystemTypeId", SqlDbType.Int));
                    cmd.Parameters["@SystemTypeId"].Value = contractInfo.SystemTypeId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = contractInfo.CustomerInfoId;
                    
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = contractInfo.CustomerId;


                    cmd.Parameters.Add(new SqlParameter("@ContractRef", SqlDbType.NVarChar));
                    cmd.Parameters["@ContractRef"].Value = contractInfo.ContractRef;

                    cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                    cmd.Parameters["@StartDate"].Value = contractInfo.StartDate;

                    cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                    cmd.Parameters["@EndDate"].Value = contractInfo.EndDate;

                    cmd.Parameters.Add(new SqlParameter("@PPMVisitPerYear", SqlDbType.Int));
                    cmd.Parameters["@PPMVisitPerYear"].Value = contractInfo.PPMVisitPerYear;

                    cmd.Parameters.Add(new SqlParameter("@PPMTotalHour", SqlDbType.Int));
                    cmd.Parameters["@PPMTotalHour"].Value = contractInfo.PPMTotalHour;

                    cmd.Parameters.Add(new SqlParameter("@TotalPPMHour", SqlDbType.Int));
                    cmd.Parameters["@TotalPPMHour"].Value = contractInfo.TotalPPMHour;

                    cmd.Parameters.Add(new SqlParameter("@SRVisitPerYear", SqlDbType.Int));
                    cmd.Parameters["@SRVisitPerYear"].Value = contractInfo.SRVisitPerYear;

                    cmd.Parameters.Add(new SqlParameter("@SRHourVisitPer", SqlDbType.Int));
                    cmd.Parameters["@SRHourVisitPer"].Value = contractInfo.SRHourVisitPer;

                    cmd.Parameters.Add(new SqlParameter("@SRTotalHours", SqlDbType.Int));
                    cmd.Parameters["@SRTotalHours"].Value = contractInfo.SRTotalHours;

                    cmd.Parameters.Add(new SqlParameter("@SRTotal", SqlDbType.Int));
                    cmd.Parameters["@SRTotal"].Value = contractInfo.SRTotal;

                    cmd.Parameters.Add(new SqlParameter("@ScopeOfWorks", SqlDbType.NVarChar));
                    cmd.Parameters["@ScopeOfWorks"].Value = contractInfo.ScopeOfWorks;

                    cmd.Parameters.Add(new SqlParameter("@SLA", SqlDbType.NVarChar));
                    cmd.Parameters["@SLA"].Value = contractInfo.SLA;

                    cmd.Parameters.Add(new SqlParameter("@AnnualContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@AnnualContractValue"].Value = contractInfo.AnnualContractValue;

                    cmd.Parameters.Add(new SqlParameter("@TotalContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@TotalContractValue"].Value = contractInfo.TotalContractValue;

                    cmd.Parameters.Add(new SqlParameter("@MonthlyContractValue", SqlDbType.NVarChar));
                    cmd.Parameters["@MonthlyContractValue"].Value = contractInfo.MonthlyContractValue;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = contractInfo.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@PaymentTerms", SqlDbType.NVarChar));
                    cmd.Parameters["@PaymentTerms"].Value = contractInfo.PaymentTerms;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = contractInfo.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = contractInfo.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = contractInfo.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = contractInfo.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = contractInfo.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@ContractType", SqlDbType.NVarChar));
                    cmd.Parameters["@ContractType"].Value = contractInfo.ContractType;
                    cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit));
                    cmd.Parameters["@Active"].Value = contractInfo.Active;
                    
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateContractInfo";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            contractInfo.Id = id;
                            var audit = new ContractInfoAduit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, contractInfo.UpdatedBy);
                            Reflection.CopyProperties(contractInfo, audit);
                            ContractInfoAduit.InsertContractInfoAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertContractInfoAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static ContractInfo GetContractInfoFromSqlReader(SqlDataReader reader)
        {
            var contractInfo = new ContractInfo();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                contractInfo.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["SystemTypeId"] != DBNull.Value)
                contractInfo.SystemTypeId = Convert.ToInt32(reader["SystemTypeId"].ToString());
           
            if (reader["CustomerId"] != DBNull.Value)
                contractInfo.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
            
            if (reader["ContractRef"] != DBNull.Value)
                contractInfo.ContractRef = reader["ContractRef"].ToString();
            if (reader["StartDate"] != DBNull.Value)
                contractInfo.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());

            if (columns.Contains( "EndDate") && reader["EndDate"] != DBNull.Value)
                contractInfo.EndDate = Convert.ToDateTime(reader["EndDate"]);

            if (columns.Contains( "CustomerInfoId") && reader["CustomerInfoId"] != DBNull.Value)
            {
                contractInfo.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"]);
            }
            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                contractInfo.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("CustomerFullName") && reader["CustomerFullName"] != DBNull.Value)
                contractInfo.CustomerFullName = reader["CustomerFullName"].ToString();
            

            if (columns.Contains( "PPMVisitPerYear") && reader["PPMVisitPerYear"] != DBNull.Value)
            {
                contractInfo.PPMVisitPerYear = Convert.ToInt32(reader["PPMVisitPerYear"]);
            }
            if (columns.Contains( "TotalContractValue") && reader["TotalContractValue"] != DBNull.Value)
            {
                contractInfo.TotalContractValue = reader["TotalContractValue"].ToString();
            }

            if (columns.Contains("IsEmailed") && reader["IsEmailed"] != DBNull.Value)
            {
                contractInfo.IsEmailed = Convert.ToBoolean(reader["IsEmailed"].ToString());
            } 
             
            if (reader["SRTotalHours"] != DBNull.Value)
                contractInfo.SRTotalHours = Convert.ToInt32(reader["SRTotalHours"].ToString());
          
            if (reader["Active"] != DBNull.Value)
                contractInfo.Active = Convert.ToBoolean(reader["Active"].ToString());
            

            if (reader["PPMTotalHour"] != DBNull.Value)
                contractInfo.PPMTotalHour = Convert.ToInt32(reader["PPMTotalHour"].ToString());
            if (reader["TotalPPMHour"] != DBNull.Value)
                contractInfo.TotalPPMHour = Convert.ToInt32(reader["TotalPPMHour"].ToString());

            if (reader["SRVisitPerYear"] != DBNull.Value)
                contractInfo.SRVisitPerYear = Convert.ToInt32(reader["SRVisitPerYear"].ToString());

            if (reader["SRHourVisitPer"] != DBNull.Value)
                contractInfo.SRHourVisitPer = Convert.ToInt32(reader["SRHourVisitPer"].ToString());

            if (reader["ScopeOfWorks"] != DBNull.Value)
                contractInfo.ScopeOfWorks = reader["ScopeOfWorks"].ToString();

            if (reader["SRTotal"] != DBNull.Value)
                contractInfo.SRTotal = Convert.ToInt32(reader["SRTotal"].ToString());

            if (reader["SLA"] != DBNull.Value)
                contractInfo.SLA = reader["SLA"].ToString();

            if (reader["AnnualContractValue"] != DBNull.Value)
                contractInfo.AnnualContractValue = reader["AnnualContractValue"].ToString();

            if (reader["MonthlyContractValue"] != DBNull.Value)
                contractInfo.MonthlyContractValue = reader["MonthlyContractValue"].ToString();

            if (reader["Remarks"] != DBNull.Value)
                contractInfo.Remarks = reader["Remarks"].ToString();

            if (reader["PaymentTerms"] != DBNull.Value)
                contractInfo.PaymentTerms = reader["PaymentTerms"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                contractInfo.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["UpdatedDate"] != DBNull.Value)
                contractInfo.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                contractInfo.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedBy"] != DBNull.Value)
                contractInfo.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["SiteId"] != DBNull.Value)
                contractInfo.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["ContractType"] != DBNull.Value)
                contractInfo.ContractType = Convert.ToInt32(reader["ContractType"].ToString());

            return contractInfo;
        }
        public static List<ContractInfo> GetAllContractInfo(string dbConnection)
        {
            var contractInfos = new List<ContractInfo>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllContractInfo";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customerSystemType = GetContractInfoFromSqlReader(reader);
                    contractInfos.Add(customerSystemType);
                }
                connection.Close();
                reader.Close();
            }
            return contractInfos;
        }

        public static void DeleteContractInfoById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteContractInfoById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteContractInfoById";

                cmd.ExecuteNonQuery();
            }
        }
        public static List<ContractInfo> GetAllContractInfoBySiteId(int siteId, string dbConnection)
        {
            var customers = new List<ContractInfo>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllContractInfoBySiteId";
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customer = GetContractInfoFromSqlReader(reader);
                    customers.Add(customer);
                }
                connection.Close();
                reader.Close();
            }
            return customers;
        }
        public static ContractInfo GetContractInfoById(int id, string dbConnection)
        {
            ContractInfo customer = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetContractInfoById";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customer = GetContractInfoFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return customer;
        }
        public static List<ContractInfo> GetContractInfosByCustomerId(int customerId, string dbConnection)
        {
            var customers = new List<ContractInfo>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetContractInfosByCustomerId";
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CustomerId"].Value = customerId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customer = GetContractInfoFromSqlReader(reader);
                    customers.Add(customer);
                }
                connection.Close();
                reader.Close();
            }
            return customers;
        }

        public static bool UpdateContractIsEmailed(int eventId, bool status, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateContractIsEmailed", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = eventId;

                cmd.Parameters.Add(new SqlParameter("@IsEmailed", SqlDbType.Bit));
                cmd.Parameters["@IsEmailed"].Value = status;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }

}

