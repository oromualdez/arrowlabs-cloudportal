﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class Location
    {
        public int ID { get; set; }
        public string LocationDesc { get; set; }
        public string LocationDesc_AR { get; set; }
        public int LocationTypeId { get; set; }
        public string LocationTypeDesc { get; set; }
        public string UpdatedBy { get; set; }
        public string FullLocationDesc_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public List<LocationType> LocationTypeList { get; set; }
        public List<Location> MainLocationsList { get; set; }
        public int ParentLocationId { get; set; }
        public string ParentLocationName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string SiteName { get; set; }
        public int SiteId { get; set; }

        public string CustomerUName { get; set; }
        public int CustomerId { get; set; }

        public static List<LocationType> LocationTypes(string dbConnection)
        {
            return LocationType.GetAllLocationType(dbConnection);
        }

        public static List<Location> MainLocations(string dbConnection)
        {
            return Location.GetAllLocation(dbConnection);
        }

        private static Location GetLocationFromSqlReader(SqlDataReader resultSet,string dbcon)
        {
            var loc = new Location();
            var columns = Enumerable.Range(0, resultSet.FieldCount).Select(resultSet.GetName).ToList();
            if (resultSet["Id"] != DBNull.Value)
                loc.ID = Convert.ToInt32(resultSet["Id"].ToString());
            if (resultSet["LOCATIONDESC"] != DBNull.Value)
                loc.LocationDesc = resultSet["LOCATIONDESC"].ToString();
            if (resultSet["LOCATIONDESC_AR"] != DBNull.Value)
                loc.LocationDesc_AR = resultSet["LOCATIONDESC_AR"].ToString();
            if (resultSet["FULLLOCATIONDESC_AR"] != DBNull.Value)
                loc.FullLocationDesc_AR = resultSet["FULLLOCATIONDESC_AR"].ToString();
            if (resultSet["LOCATIONTYPEID"] != DBNull.Value)
                loc.LocationTypeId = Convert.ToInt32(resultSet["LOCATIONTYPEID"].ToString());
            if (resultSet["CreateDate"] != DBNull.Value)
                loc.CreatedDate = Convert.ToDateTime(resultSet["CreateDate"].ToString());
            if (resultSet["Updateddate"] != DBNull.Value)
                loc.UpdatedDate = Convert.ToDateTime(resultSet["Updateddate"].ToString());
            if (resultSet["CreatedBy"] != DBNull.Value)
                loc.CreatedBy = resultSet["CreatedBy"].ToString();
            if (resultSet["LOCATIONTYPEDESC"] != DBNull.Value)
                loc.LocationTypeDesc = resultSet["LOCATIONTYPEDESC"].ToString();
            if (resultSet["ParentLocationId"] != DBNull.Value)
                loc.ParentLocationId = (String.IsNullOrEmpty(resultSet["ParentLocationId"].ToString())) ? 0 : Convert.ToInt32(resultSet["ParentLocationId"].ToString());
            
            if (columns.Contains("ParentLocationName") && resultSet["ParentLocationName"] != DBNull.Value)
                loc.ParentLocationName = resultSet["ParentLocationName"].ToString();

            //if (loc.ParentLocationId > 0)
            //{
            //    var gLoc = Location.GetLocationById(loc.ParentLocationId, dbcon);
            //    if (gLoc != null)
            //        loc.ParentLocationName = gLoc.LocationDesc;
            //}

            try
            {
                if (columns.Contains( "SiteId") && resultSet["SiteId"] != DBNull.Value)
                    loc.SiteId = Convert.ToInt32(resultSet["SiteId"]);

                if (columns.Contains( "CustomerId") && resultSet["CustomerId"] != DBNull.Value)
                    loc.CustomerId = Convert.ToInt32(resultSet["CustomerId"]);

                if (columns.Contains( "AccountId"))
                    loc.AccountId = Convert.ToInt32(resultSet["AccountId"]);

                if (columns.Contains( "AccountName"))
                    loc.AccountName = resultSet["AccountName"].ToString();

                if (columns.Contains( "SiteName") && resultSet["SiteName"] != DBNull.Value)
                    loc.SiteName = resultSet["SiteName"].ToString();

                if (columns.Contains( "CustomerUName") && resultSet["CustomerUName"] != DBNull.Value)
                    loc.CustomerUName = resultSet["CustomerUName"].ToString();


                if (resultSet["Longitude"] != DBNull.Value)
                    loc.Longitude = (!String.IsNullOrEmpty(resultSet["Longitude"].ToString())) ? Convert.ToSingle(resultSet["Longitude"].ToString()) : 0f;
                if (resultSet["Latitude"] != DBNull.Value)
                    loc.Latitude = (!String.IsNullOrEmpty(resultSet["Latitude"].ToString())) ? Convert.ToSingle(resultSet["Latitude"].ToString()) : 0f;
            }
            catch (Exception ex)
            {
            }

            return loc;
        }

        public static Location GetLocationById(int locationId, string dbConnection)
        {
            Location location = null;
        //    var locationTypes = LocationTypes(dbConnection);
         //   var allMainLocations = Location.GetAllLocation(dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLocationById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = locationId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.ExecuteNonQuery();
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    location = GetLocationFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return location;
        }

        public static List<Location> GetAllMainLocationBySiteId(int id, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMainLocationBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = id;

                sqlCommand.CommandText = "GetAllMainLocationBySiteId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }


        public static List<Location> GetAllLocationBySiteId(int id, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = id;

                sqlCommand.CommandText = "GetAllLocationBySiteId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    if (loc.ParentLocationId == 0)
                        locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }
        public static List<Location> GetAllLocation(string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocation", connection);


                sqlCommand.CommandText = "GetAllLocation";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static List<Location> GetAllLocationByAccountName(string name , string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationByAccountName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.CommandText = "GetAllLocationByAccountName";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static List<Location> GetAllSubLocation(string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            var allMainLocations = Location.GetAllLocation(dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSubLocation", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    loc.MainLocationsList = allMainLocations;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static List<Location> GetAllSubLocationBySiteId(int SiteId, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            var allMainLocations = Location.GetAllLocation(dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSubLocationBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = SiteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    loc.MainLocationsList = allMainLocations;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static List<Location> GetAllLocationByCustomerId(int id, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationByCustomerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandText = "GetAllLocationByCustomerId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    if (loc.ParentLocationId == 0)
                        locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }
        public static List<Location> GetAllMainLocationByCustomerId(int id, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMainLocationByCustomerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandText = "GetAllMainLocationByCustomerId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }
        public static List<Location> GetAllSubLocationByCustomerId(int id, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            var allMainLocations = Location.GetAllLocation(dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSubLocationByCustomerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandText = "GetAllSubLocationByCustomerId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    loc.MainLocationsList = allMainLocations;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static List<Location> GetAllSubLocationByMainId(int locationId, string dbConnection)
        {
            var locations = new List<Location>();
        //    var locationTypes = LocationTypes(dbConnection);
         //   var allMainLocations = Location.GetAllLocation(dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSubLocationByMainId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = locationId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.ExecuteNonQuery();
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                 //   loc.LocationTypeList = locationTypes;
                 //   loc.MainLocationsList = allMainLocations;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static List<Location> GetAllLocationByDate(DateTime updatedDate, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationByDate", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;

                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static int InsertorUpdateLocation(Location location, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = location.ID;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (location != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateLocation", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = location.ID;

                    cmd.Parameters.Add(new SqlParameter("@LocationDesc", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationDesc"].Value = location.LocationDesc;

                    cmd.Parameters.Add(new SqlParameter("@LocationDesc_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationDesc_AR"].Value = location.LocationDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@LocationTypeId", SqlDbType.Int));
                    cmd.Parameters["@LocationTypeId"].Value = location.LocationTypeId;

                    cmd.Parameters.Add(new SqlParameter("@FullLocationDesc_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@FullLocationDesc_AR"].Value = location.FullLocationDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreateDate"].Value = location.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = location.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = location.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ParentLocationId", SqlDbType.Int));
                    cmd.Parameters["@ParentLocationId"].Value = location.ParentLocationId;

                    cmd.Parameters.Add(new SqlParameter("@ParentLocationName", SqlDbType.NVarChar));
                    cmd.Parameters["@ParentLocationName"].Value = location.ParentLocationName;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = location.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = location.CustomerId;


                    try
                    {
                        if (!string.IsNullOrEmpty(location.Latitude.ToString()))
                        {
                            cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                            cmd.Parameters["@Latitude"].Value = location.Latitude;
                        }
                        if (!string.IsNullOrEmpty(location.Longitude.ToString()))
                        {
                            cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                            cmd.Parameters["@Longitude"].Value = location.Longitude;
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer Lati+Long", "InsertorUpdateLocation", ex, dbConnection);
                    }

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateLocation";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            location.ID = id;
                            var audit = new LocationAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, location.UpdatedBy);
                            Reflection.CopyProperties(location, audit);
                            LocationAudit.InsertLocationAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateLocation", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static bool DeleteLocationById(int locationId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteLocationById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static List<Location> GetAllLocationByLocationDesc(string search, string dbConnection)
        {
            var locations = new List<Location>();
            //    var locationTypes = LocationTypes(dbConnection);
            //   var allMainLocations = Location.GetAllLocation(dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationByLocationDesc", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Search", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Search"].Value = search;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.ExecuteNonQuery();
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    //   loc.LocationTypeList = locationTypes;
                    //   loc.MainLocationsList = allMainLocations;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }
        public static List<Location> GetAllLocationByLocationDescAndCId(string search,int id ,string dbConnection)
        {
            var locations = new List<Location>();
            //    var locationTypes = LocationTypes(dbConnection);
            //   var allMainLocations = Location.GetAllLocation(dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationByLocationDescAndCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Search", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Search"].Value = search;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.ExecuteNonQuery();
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    //   loc.LocationTypeList = locationTypes;
                    //   loc.MainLocationsList = allMainLocations;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }


        public static List<Location> GetAllSubLocationByLevel5(int SiteId, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            var allMainLocations = Location.GetAllLocation(dbConnection);

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSubLocationByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = SiteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    loc.MainLocationsList = allMainLocations;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }
        public static List<Location> GetAllMainLocationByLevel5(int id, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMainLocationByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = id;

                sqlCommand.CommandText = "GetAllMainLocationByLevel5";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }
        public static List<Location> GetAllLocationByLevel5(int id, string dbConnection)
        {
            var locations = new List<Location>();
            var locationTypes = LocationTypes(dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = id;

                sqlCommand.CommandText = "GetAllLocationByLevel5";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationFromSqlReader(reader, dbConnection);
                    loc.LocationTypeList = locationTypes;
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }
  }
}
