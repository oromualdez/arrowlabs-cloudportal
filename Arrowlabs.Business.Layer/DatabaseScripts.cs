﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace Arrowlabs.Business.Layer
{
 public  class DatabaseScripts
    {

     public static bool UpdateDatabase( string connection)
     {
         string script = string.Empty;
         string[] separator = new string[] { "GO", "go" };
         var scripts = script.Split(separator, StringSplitOptions.RemoveEmptyEntries);
         foreach (var src in scripts)
         {
             using (var sqlConnection = new SqlConnection(connection))
             {
                 sqlConnection.Open();
             
                 Server server = new Server(new ServerConnection(sqlConnection));
                 server.ConnectionContext.ExecuteNonQuery(src);
                 sqlConnection.Close();
             }
         }

         return true;

     }
    }
}
