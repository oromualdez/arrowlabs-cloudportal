﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class MessageBoard
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public string HeaderTitle { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; } 
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DisplayDate { get; set; }
        public bool isFlashAlert { get; set; }
        public string CreatedBy { get; set; }
        public string UrlLink { get; set; }
        public string CustomerUName { get; set; }

        public string UpdatedBy { get; set; }

        public static bool DeleteMessageBoardById(int empid, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteMessageBoardById", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = empid;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteMessageBoardById";

                    cmd.ExecuteNonQuery();
                }
                return true;

            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Activity", "DeleteMessageBoardById", ex, dbConnection);
            }
            return false;
        }

        private static MessageBoard GetMessageBoardFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();


            var notification = new MessageBoard();

            if (reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["HeaderTitle"] != DBNull.Value)
                notification.HeaderTitle = reader["HeaderTitle"].ToString();
            if (reader["Description"] != DBNull.Value)
                notification.Description = reader["Description"].ToString();
            if (reader["ImagePath"] != DBNull.Value)
                notification.ImagePath = reader["ImagePath"].ToString();
            if (reader["isFlashAlert"] != DBNull.Value)
                notification.isFlashAlert = Convert.ToBoolean(reader["isFlashAlert"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                notification.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["DisplayDate"] != DBNull.Value)
                notification.DisplayDate = Convert.ToDateTime(reader["DisplayDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedBy"] != DBNull.Value)
                notification.UpdatedBy = reader["UpdatedBy"].ToString(); 
            if (reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                notification.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                notification.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("URLLink") && reader["URLLink"] != DBNull.Value)
                notification.UrlLink = reader["URLLink"].ToString();
            
            return notification;
        }

        public static List<MessageBoard> GetAllMessageBoard(string dbConnection)
        {
            var assetList = new List<MessageBoard>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllMessageBoard", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var asset = GetMessageBoardFromSqlReader(reader);
                        assetList.Add(asset);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MessageBoard", "GetAllMessageBoard", exp, dbConnection);

            }
            return assetList;
        }

        public static MessageBoard GetMessageBoardById(int id, string dbConnection)
        {
            MessageBoard asset = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetMessageBoardById", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        asset = GetMessageBoardFromSqlReader(reader);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MessageBoard", "GetMessageBoardById", exp, dbConnection);

            }
            return asset;
        }

        public static List<MessageBoard> GetAllMessageBoardBySiteId(int siteId, string dbConnection)
        {
            var itemFounds = new List<MessageBoard>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllMessageBoardBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetMessageBoardFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MessageBoard", "GetAllMessageBoardBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<MessageBoard> GetAllMessageBoardByLevel5(int siteId, string dbConnection)
        {
            var itemFounds = new List<MessageBoard>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllMessageBoardByLevel5", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetMessageBoardFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MessageBoard", "GetAllMessageBoardByLevel5", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<MessageBoard> GetAllMessageBoardByCustomerId(int siteId, string dbConnection)
        {
            var itemFounds = new List<MessageBoard>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllMessageBoardByCustomerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetMessageBoardFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("MessageBoard", "GetAllMessageBoardByCustomerId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static int InsertorUpdateMessageBoard(MessageBoard notification, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateMessageBoard", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@HeaderTitle", SqlDbType.NVarChar));
                    cmd.Parameters["@HeaderTitle"].Value = notification.HeaderTitle;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = notification.Description;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@DisplayDate", SqlDbType.DateTime));
                    cmd.Parameters["@DisplayDate"].Value = notification.DisplayDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = notification.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = notification.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = notification.ImagePath; 

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@isFlashAlert", SqlDbType.Bit));
                    cmd.Parameters["@isFlashAlert"].Value = notification.isFlashAlert;

                    cmd.Parameters.Add(new SqlParameter("@URLLink", SqlDbType.NVarChar));
                    cmd.Parameters["@URLLink"].Value = notification.UrlLink; 
                    
                     
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertorUpdateMessageBoard";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            notification.Id = id;
                            var audit = new MessageBoardAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, notification.CreatedBy);
                            Reflection.CopyProperties(notification, audit);
                            MessageBoardAudit.InsertorUpdateMessageBoardAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertMessageBoardAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }
    }
}
