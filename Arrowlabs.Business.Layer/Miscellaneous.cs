﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
  public class Miscellaneous
  {
      static bool optimalAsymmetricEncryptionPadding =true;
      static string result = string.Empty;

      public static string portionEncrypted0 = "BDfMQRtcoK34aHCOMF1Txxs43CjZjkiX2ZKXeRCcaD/K3Wv2d9jjVpewVKnib+mSuF2wwgXbjZGOOZ9VDoZuSg==";
      public static string portionEncrypted1 = "PFJTQUtleVZhbHVlPjxNb2R1bHVzPnlSN1pTV1Rvc3krMzJsR0k2KzV0SmVyeTEvc2VNRkFkaWE5MTFpNkNuMHVIN0ZTM3FlbzVsUU00OE1wL2FQdHhrbXV1V2dWbFJTMjljblVmMExrWGlRPT08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjxQPi8wWlNKTUxyU29sTzRpWFNDL0daSTNHZlZsNDRYSjlWbSsrNzdWMkdZdGM9PC9QPjxRPnliRWpSVkYvSXFWcWg2WXJXcWlCVVZseVdXdE9nNUJsM2ZIejdRa0piSjg9PC9RPjxEUD5CM3VITXQ2bnlQS0habWVYWlpRNldlaWR0QWxrKzRvTUYxc0tsaVdVcDljPTwvRFA+PERRPklxc1pGZlF4TjBKWnNtRmt1RnVZaHVRcUhLYXVRejBSMVhtZVpSTVZsSjg9PC9EUT48SW52ZXJzZVE+NndHejZFdE5paklKTW4yVjBETTJzR21VU0p0SWpVQzFzcTE2MERCQTJ4Zz08L0ludmVyc2VRPjxEPm9hdkVxM1VNZ2c0WDBzMjhMVHZxVFpMQ1I5QTdna2JFbDh1T0EvQTdYR1JodG1KUzhaeGI2TlhvU0FMK3NENjN0R0NwZStuWWZWYnU3VTlHNzRIanlRPT08L0Q+PC9SU0FLZXlWYWx1ZT4=";
          
      public static string DecryptText(string text, int keySize, string publicAndPrivateKeyXml)
      {
          var decrypted = Decrypt(Convert.FromBase64String(text), keySize, publicAndPrivateKeyXml);
          return Encoding.UTF8.GetString(decrypted);
      }
      public static byte[] Decrypt(byte[] data, int keySize, string publicAndPrivateKeyXml)
      {
         using (var provider = new RSACryptoServiceProvider(keySize))
          {
              provider.FromXmlString(publicAndPrivateKeyXml);
              return provider.Decrypt(data, optimalAsymmetricEncryptionPadding);
          }
      }
      public static string FinalResult(string dbConnection)
      {
          result = "ArrowlabsFZCLLCSecuritySolutions2013";
          if (string.IsNullOrEmpty(result))
          {
              var test = string.Empty;
              try
              {
                  var second = ClientLicence.GetClientSerialNo("YikrYTm1yg4=", dbConnection);
                  var other2 = (second == null) ? null : second.SerialNo.Split('}');////125
                  if (other2 != null & other2.Length > 1)
                  {
                      test += DecryptText(portionEncrypted0, 512, System.Text.Encoding.Default.GetString(Convert.FromBase64String(portionEncrypted1)));
                      test += DecryptText(other2[1], 512, System.Text.Encoding.Default.GetString(Convert.FromBase64String(other2[2])));
                      var regName1 = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("temp"));
                      var registry1 = "Value1";
                      var readkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(regName1);
                      if (readkey != null)
                      {

                          var value = readkey.GetValue(registry1).ToString();
                          if (!String.IsNullOrEmpty(value))
                          {
                              var values = value.Split('}');
                              test += DecryptText(values[0], 512, System.Text.Encoding.Default.GetString(Convert.FromBase64String(values[1])));
                          }

                      }
                      else
                      {
                          var readkey1 = Microsoft.Win32.Registry.LocalMachine;
                          if (readkey1 != null)
                          {
                              readkey1 = readkey1.OpenSubKey(@"Software\" + regName1);
                              if (readkey1 != null)
                              {
                                  var value = readkey1.GetValue(registry1).ToString();
                                  if (!String.IsNullOrEmpty(value))
                                  {
                                      var values = value.Split('}');
                                      test += DecryptText(values[0], 512, System.Text.Encoding.Default.GetString(Convert.FromBase64String(values[1])));
                                  }
                              }
                          }
                      }

                      result = test;
                  }
              }
              catch (Exception exp)
              {
                  //MIMSLog.MIMSLogSave("ArrowlabsBusinessLayer", "FinalKey", exp, dbConnection);
              }
          }
          return result;
      }
    }
}
