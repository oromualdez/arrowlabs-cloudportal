﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class CnlPostOrder
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public string Orders { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public string Remove { get; set; }
        public string UpdatedBy { get; set; }

        private static CnlPostOrder GetCnlPostOrderFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var postOrder = new CnlPostOrder();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                postOrder.Id = Convert.ToInt32(reader["ID"].ToString());
            if (columns.Contains("Location") && reader["Location"] != DBNull.Value)
                postOrder.Location = reader["Location"].ToString();
            if (columns.Contains("Orders") && reader["Orders"] != DBNull.Value)
                postOrder.Orders = reader["Orders"].ToString();
            if (columns.Contains("DateCreated") && reader["DateCreated"] != DBNull.Value)
                postOrder.CreatedDate = Convert.ToDateTime(reader["DateCreated"].ToString());
            if (columns.Contains("Remove") && reader["Remove"] != DBNull.Value)
                postOrder.Remove = reader["Remove"].ToString();

            return postOrder;
        }

        /// <summary>
        /// It returns all post orders
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<CnlPostOrder> GetAllCnlPostOrders(string dbConnection)
        {
            var postOrders = new List<CnlPostOrder>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPostOrders", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var postOrder = GetCnlPostOrderFromSqlReader(reader);
                    postOrders.Add(postOrder);
                }
                connection.Close();
                reader.Close();
            }
            return postOrders;
        }
    }
}
