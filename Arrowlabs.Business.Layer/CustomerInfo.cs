﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class CustomerInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }

        public bool SendEmail { get; set; }

        public string Email { get; set; }
        public int TotalUser { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Modules { get; set; }

        public string Lati { get; set; }
        public string Long { get; set; }

         public int MaxUploadSize { get; set; }

        public int MaxNumberAttachments { get; set; }

        public int MIMSMobileGPSInterval { get; set; }

        public int AverageHourlyCost { get; set; }

        public int WorkingDaysWeek { get; set; }

        public int WorkingHoursWeek{ get; set; }

        public string LastName { get; set; }

        public string RssFeedUrl { get; set; }

        public string MessageBoardVideoPath { get; set; } 

        public string DutyWorkWeek { get; set; }

        public Double TimeZone { get; set; }


        public int MessageBoardCount { get; set; }

        public int IncidentCount { get; set; }

        public int TaskCount { get; set; }

        public int TicketCount { get; set; }

        public int PostOrderCount { get; set; }

        public int DutyRosterCount { get; set; }

        public int LostFoundCount { get; set; }

        public int ContractCount { get; set; }

        public int RequestCount { get; set; }

        public int SurveillanceCount { get; set; }

        public int ActivityCount { get; set; }

        public int ChatCount { get; set; }

        public int WarehouseCount { get; set; }

        public int DispatchCount { get; set; }


        private static CustomerInfo GetChatUserFromSqlReader(SqlDataReader reader, string dbConnection)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var customerInfo = new CustomerInfo();
            if (reader["Id"] != DBNull.Value)
                customerInfo.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                customerInfo.Name = Decrypt.DecryptData(reader["Name"].ToString(), true, dbConnection);
            if (reader["Address"] != DBNull.Value)
                customerInfo.Address = Decrypt.DecryptData(reader["Address"].ToString(), true, dbConnection);
            if (reader["Country"] != DBNull.Value)
                customerInfo.Country = Decrypt.DecryptData(reader["Country"].ToString(), true, dbConnection);
            if (reader["PhoneNo"] != DBNull.Value)
                customerInfo.PhoneNo = Decrypt.DecryptData(reader["PhoneNo"].ToString(), true, dbConnection);
            if (reader["CreatedDate"] != DBNull.Value)
                customerInfo.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                customerInfo.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["Active"] != DBNull.Value)
                customerInfo.Active = Convert.ToBoolean(Decrypt.DecryptData(reader["Active"].ToString(), true, dbConnection));
            if (reader["Email"] != DBNull.Value)
                customerInfo.Email = Decrypt.DecryptData(reader["Email"].ToString(), true, dbConnection);
            if (columns.Contains("TotalUser") && reader["TotalUser"] != DBNull.Value)
                customerInfo.TotalUser = Convert.ToInt32(Decrypt.DecryptData(reader["TotalUser"].ToString(), true, dbConnection));
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                customerInfo.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("UpdatedBy") && reader["UpdatedBy"] != DBNull.Value)
                customerInfo.UpdatedBy = reader["UpdatedBy"].ToString();

            if (columns.Contains("RssFeedUrl") && reader["RssFeedUrl"] != DBNull.Value)
                customerInfo.RssFeedUrl = reader["RssFeedUrl"].ToString();

            if (columns.Contains("MessageBoardVideoPath") && reader["MessageBoardVideoPath"] != DBNull.Value)
                customerInfo.MessageBoardVideoPath = reader["MessageBoardVideoPath"].ToString();

            if (columns.Contains("DutyWorkWeek") && reader["DutyWorkWeek"] != DBNull.Value)
                customerInfo.DutyWorkWeek = reader["DutyWorkWeek"].ToString();

            if (columns.Contains("SendEmail") && reader["SendEmail"] != DBNull.Value)
                customerInfo.SendEmail = Convert.ToBoolean(reader["SendEmail"].ToString());

            if (columns.Contains("Modules") && reader["Modules"] != DBNull.Value)
                customerInfo.Modules = Decrypt.DecryptData(reader["Modules"].ToString(), true, dbConnection);

            if (columns.Contains("MaxUploadSize") && reader["MaxUploadSize"] != DBNull.Value)
                customerInfo.MaxUploadSize = Convert.ToInt32(reader["MaxUploadSize"].ToString());

            if (columns.Contains("MIMSMobileGPSInterval") && reader["MIMSMobileGPSInterval"] != DBNull.Value)
                customerInfo.MIMSMobileGPSInterval = Convert.ToInt32(reader["MIMSMobileGPSInterval"].ToString());

            if (columns.Contains("MaxNumberAttachments") && reader["MaxNumberAttachments"] != DBNull.Value)
                customerInfo.MaxNumberAttachments = Convert.ToInt32(reader["MaxNumberAttachments"].ToString());

            customerInfo.WorkingHoursWeek = 5;

            if (columns.Contains("WorkingHoursWeek") && reader["WorkingHoursWeek"] != DBNull.Value)
                customerInfo.WorkingHoursWeek = Convert.ToInt32(reader["WorkingHoursWeek"].ToString());

            customerInfo.WorkingDaysWeek = 40;

            if (columns.Contains("WorkingDaysWeek") && reader["WorkingDaysWeek"] != DBNull.Value)
                customerInfo.WorkingDaysWeek = Convert.ToInt32(reader["WorkingDaysWeek"].ToString());

            if (columns.Contains("AverageHourlyCost") && reader["AverageHourlyCost"] != DBNull.Value)
                customerInfo.AverageHourlyCost = Convert.ToInt32(reader["AverageHourlyCost"].ToString());


            if (columns.Contains("LastName") && reader["LastName"] != DBNull.Value)
                customerInfo.LastName = Decrypt.DecryptData(reader["LastName"].ToString(), true, dbConnection);

            if (columns.Contains("Lati") && reader["Lati"] != DBNull.Value)
                customerInfo.Lati = reader["Lati"].ToString();

            if (columns.Contains("Long") && reader["Long"] != DBNull.Value)
                customerInfo.Long = reader["Long"].ToString();

            if (columns.Contains("TimeZone") && reader["TimeZone"] != DBNull.Value)
                customerInfo.TimeZone = Convert.ToDouble(reader["TimeZone"].ToString());


            if (columns.Contains("MBC") && reader["MBC"] != DBNull.Value && !string.IsNullOrEmpty(reader["MBC"].ToString()))
                customerInfo.MessageBoardCount = Convert.ToInt32(Decrypt.DecryptData(reader["MBC"].ToString(), true, string.Empty));

            if (columns.Contains("INC") && reader["INC"] != DBNull.Value && !string.IsNullOrEmpty(reader["INC"].ToString()))
                customerInfo.IncidentCount = Convert.ToInt32(Decrypt.DecryptData(reader["INC"].ToString(), true, string.Empty));

            if (columns.Contains("TAC") && reader["TAC"] != DBNull.Value && !string.IsNullOrEmpty(reader["TAC"].ToString()))
                customerInfo.TaskCount = Convert.ToInt32(Decrypt.DecryptData(reader["TAC"].ToString(), true, string.Empty));

            if (columns.Contains("TIC") && reader["TIC"] != DBNull.Value && !string.IsNullOrEmpty(reader["TIC"].ToString()))
                customerInfo.TicketCount = Convert.ToInt32(Decrypt.DecryptData(reader["TIC"].ToString(), true, string.Empty));

            if (columns.Contains("PPC") && reader["PPC"] != DBNull.Value && !string.IsNullOrEmpty(reader["PPC"].ToString()))
                customerInfo.PostOrderCount = Convert.ToInt32(Decrypt.DecryptData(reader["PPC"].ToString(), true, string.Empty));

            if (columns.Contains("DRC") && reader["DRC"] != DBNull.Value && !string.IsNullOrEmpty(reader["DRC"].ToString()))
                customerInfo.DutyRosterCount = Convert.ToInt32(Decrypt.DecryptData(reader["DRC"].ToString(), true, string.Empty));

            if (columns.Contains("LFC") && reader["LFC"] != DBNull.Value && !string.IsNullOrEmpty(reader["LFC"].ToString()))
                customerInfo.LostFoundCount = Convert.ToInt32(Decrypt.DecryptData(reader["LFC"].ToString(), true, string.Empty));

            if (columns.Contains("COC") && reader["COC"] != DBNull.Value && !string.IsNullOrEmpty(reader["COC"].ToString()))
                customerInfo.ContractCount = Convert.ToInt32(Decrypt.DecryptData(reader["COC"].ToString(), true, string.Empty));

            if (columns.Contains("REC") && reader["REC"] != DBNull.Value && !string.IsNullOrEmpty(reader["REC"].ToString()))
                customerInfo.RequestCount = Convert.ToInt32(Decrypt.DecryptData(reader["REC"].ToString(), true, string.Empty));

            if (columns.Contains("SUC") && reader["SUC"] != DBNull.Value && !string.IsNullOrEmpty(reader["SUC"].ToString()))
                customerInfo.SurveillanceCount = Convert.ToInt32(Decrypt.DecryptData(reader["SUC"].ToString(), true, string.Empty));

            if (columns.Contains("ACC") && reader["ACC"] != DBNull.Value && !string.IsNullOrEmpty(reader["ACC"].ToString()))
                customerInfo.ActivityCount = Convert.ToInt32(Decrypt.DecryptData(reader["ACC"].ToString(), true, string.Empty));

            if (columns.Contains("CHC") && reader["CHC"] != DBNull.Value && !string.IsNullOrEmpty(reader["CHC"].ToString()))
                customerInfo.ChatCount = Convert.ToInt32(Decrypt.DecryptData(reader["CHC"].ToString(), true, string.Empty));

            if (columns.Contains("WAC") && reader["WAC"] != DBNull.Value && !string.IsNullOrEmpty(reader["WAC"].ToString()))
                customerInfo.WarehouseCount = Convert.ToInt32(Decrypt.DecryptData(reader["WAC"].ToString(), true, string.Empty));

            if (columns.Contains("DIC") && reader["DIC"] != DBNull.Value && !string.IsNullOrEmpty(reader["DIC"].ToString()))
                customerInfo.DispatchCount = Convert.ToInt32(Decrypt.DecryptData(reader["DIC"].ToString(), true, string.Empty));

            return customerInfo;
        }
        public static List<CustomerInfo> GetAllCustomerInfo(string dbConnection)
        {
            var CustomerInfoList = new List<CustomerInfo>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllCustomerInfo", connection);
              
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var customerInfo = GetChatUserFromSqlReader(reader,dbConnection);
                    CustomerInfoList.Add(customerInfo);
                }
                connection.Close();
                reader.Close();
            }
            return CustomerInfoList;
        }

        public static CustomerInfo GetCustomerInfoById(int id,string dbConnection)
        {
            CustomerInfo customerInfo = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetCustomerInfoById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    customerInfo = GetChatUserFromSqlReader(reader,dbConnection);
                   
                }
                connection.Close();
                reader.Close();
            }
            return customerInfo;
        }

        public static CustomerInfo GetCustomerInfoByEmail(string email, string dbConnection)
        {
            CustomerInfo customerInfo = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetCustomerInfoByEmail", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Email"].Value =Encrypt.EncryptData(email,true,dbConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    customerInfo = GetChatUserFromSqlReader(reader,dbConnection);

                }
                connection.Close();
                reader.Close();
            }
            return customerInfo;
        }
        public static int InsertorUpdateCustomerInfo(CustomerInfo customerInf, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (customerInf.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (customerInf != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateCustomerInfo", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customerInf.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value =Encrypt.EncryptData( customerInf.Name,true,dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@Address", SqlDbType.NVarChar));
                    cmd.Parameters["@Address"].Value =Encrypt.EncryptData( customerInf.Address,true,dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@Country", SqlDbType.NVarChar));
                    cmd.Parameters["@Country"].Value =Encrypt.EncryptData( customerInf.Country,true,dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@PhoneNo", SqlDbType.NVarChar));
                    cmd.Parameters["@PhoneNo"].Value =Encrypt.EncryptData( customerInf.PhoneNo,true,dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customerInf.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = customerInf.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.NVarChar));
                    cmd.Parameters["@Active"].Value = Encrypt.EncryptData(customerInf.Active.ToString(),true,dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                    cmd.Parameters["@Email"].Value =Encrypt.EncryptData( customerInf.Email,true,dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@TotalUser", SqlDbType.NVarChar));
                    cmd.Parameters["@TotalUser"].Value = Encrypt.EncryptData(customerInf.TotalUser.ToString(),true,dbConnection);
                    
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customerInf.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Modules", SqlDbType.NVarChar));
                    cmd.Parameters["@Modules"].Value = Encrypt.EncryptData(customerInf.Modules, true,dbConnection);
                    
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customerInf.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@RssFeedUrl", SqlDbType.NVarChar));
                    cmd.Parameters["@RssFeedUrl"].Value = customerInf.RssFeedUrl;

                    cmd.Parameters.Add(new SqlParameter("@DutyWorkWeek", SqlDbType.NVarChar));
                    cmd.Parameters["@DutyWorkWeek"].Value = customerInf.DutyWorkWeek;

                    cmd.Parameters.Add(new SqlParameter("@MessageBoardVideoPath", SqlDbType.NVarChar));
                    cmd.Parameters["@MessageBoardVideoPath"].Value = customerInf.MessageBoardVideoPath;

                    cmd.Parameters.Add(new SqlParameter("@Long", SqlDbType.NVarChar));
                    cmd.Parameters["@Long"].Value = customerInf.Long;

                    cmd.Parameters.Add(new SqlParameter("@Lati", SqlDbType.NVarChar));
                    cmd.Parameters["@Lati"].Value = customerInf.Lati;

                    cmd.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                    cmd.Parameters["@LastName"].Value = Encrypt.EncryptData(customerInf.LastName, true, dbConnection);
                    
                    cmd.Parameters.Add(new SqlParameter("@MaxNumberAttachments", SqlDbType.Int));
                    cmd.Parameters["@MaxNumberAttachments"].Value = customerInf.MaxNumberAttachments;

                    cmd.Parameters.Add(new SqlParameter("@MaxUploadSize", SqlDbType.Int));
                    cmd.Parameters["@MaxUploadSize"].Value = customerInf.MaxUploadSize;

                    cmd.Parameters.Add(new SqlParameter("@MIMSMobileGPSInterval", SqlDbType.Int));
                    cmd.Parameters["@MIMSMobileGPSInterval"].Value = customerInf.MIMSMobileGPSInterval;

                    cmd.Parameters.Add(new SqlParameter("@AverageHourlyCost", SqlDbType.Int));
                    cmd.Parameters["@AverageHourlyCost"].Value = customerInf.AverageHourlyCost;

                    cmd.Parameters.Add(new SqlParameter("@WorkingDaysWeek", SqlDbType.Int));
                    cmd.Parameters["@WorkingDaysWeek"].Value = customerInf.WorkingDaysWeek;

                    cmd.Parameters.Add(new SqlParameter("@WorkingHoursWeek", SqlDbType.Int));
                    cmd.Parameters["@WorkingHoursWeek"].Value = customerInf.WorkingHoursWeek;

                    cmd.Parameters.Add(new SqlParameter("@SendEmail", SqlDbType.Bit));
                    cmd.Parameters["@SendEmail"].Value = customerInf.SendEmail;
                     
                    cmd.Parameters.Add(new SqlParameter("@TimeZone", SqlDbType.Float));
                    cmd.Parameters["@TimeZone"].Value = customerInf.TimeZone;
                    
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    if ((int)returnParameter.Value > 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new CustomerInfoAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customerInf.UpdatedBy);
                            Reflection.CopyProperties(customerInf, audit);
                            CustomerInfoAudit.InsertCustomerInfoAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateCustomerInfo", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static bool UpdateCustomerInfoStatus(int id, bool active, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateCustomerInfoStatus", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.NVarChar));
                cmd.Parameters["@Active"].Value = Encrypt.EncryptData(active.ToString(),true,dbConnection);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }

            return true;
        }

        public static bool DeleteCustomerInfoById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteCustomerInfoById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteCustomerInfoById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
