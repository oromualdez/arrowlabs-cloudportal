﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class WatchInfo
    {

        public int Id { get; set; }
        public string PinCode { get; set; }
        public string SecurityCode { get; set; }
        public string DeviceTocken { get; set; }
        public string UserName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Updatedby { get; set; }
        public DateTime? ExpiryDate { get; set; }

        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        private static WatchInfo GetWatchInfoFromSqlReader(SqlDataReader reader, string dbcon)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var watchInfo = new WatchInfo();
            if (reader["Id"] != DBNull.Value)
                watchInfo.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["SiteId"] != DBNull.Value)
                watchInfo.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                watchInfo.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
          

            if (reader["PinCode"] != DBNull.Value)
                watchInfo.PinCode = Decrypt.DecryptData(reader["PinCode"].ToString(), true, dbcon);
            if (reader["SecurityCode"] != DBNull.Value)
                watchInfo.SecurityCode = Decrypt.DecryptData(reader["SecurityCode"].ToString(), true, dbcon);
            if (reader["DeviceTocken"] != DBNull.Value)
                watchInfo.DeviceTocken = reader["DeviceTocken"].ToString();
            if (reader["UserName"] != DBNull.Value)
                watchInfo.UserName = reader["UserName"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                watchInfo.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                watchInfo.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["ExpiryDate"] != DBNull.Value)
                watchInfo.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"].ToString());
            if (reader["Updatedby"] != DBNull.Value)
                watchInfo.Updatedby = reader["Updatedby"].ToString();
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                watchInfo.CreatedBy = reader["CreatedBy"].ToString();

            return watchInfo;
        }
        public static bool InsertorUpdateWatchInfo(WatchInfo watchInfo, string dbConnection,
    string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (watchInfo.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (watchInfo != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateWatchInfo", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = watchInfo.Id;

                                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = watchInfo.SiteId;

                                                            cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = watchInfo.CustomerId;
      

                    cmd.Parameters.Add(new SqlParameter("@PinCode", SqlDbType.NVarChar));
                    cmd.Parameters["@PinCode"].Value = watchInfo.PinCode;

                    cmd.Parameters.Add(new SqlParameter("@SecurityCode", SqlDbType.NVarChar));
                    cmd.Parameters["@SecurityCode"].Value = Encrypt.EncryptData(watchInfo.SecurityCode,true,dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@DeviceTocken", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceTocken"].Value = watchInfo.DeviceTocken;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = watchInfo.UserName;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = watchInfo.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = watchInfo.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.DateTime));
                    cmd.Parameters["@ExpiryDate"].Value = watchInfo.ExpiryDate;

                    cmd.Parameters.Add(new SqlParameter("@Updatedby", SqlDbType.NVarChar));
                    cmd.Parameters["@Updatedby"].Value = watchInfo.Updatedby;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = watchInfo.CreatedBy;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new WatchInfoAudit();

                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, watchInfo.Updatedby);
                            Reflection.CopyProperties(watchInfo, audit);
                            WatchInfoAudit.InsertorUpdateWatchInfo(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateWatchInfo", ex, dbConnection);
                    }
                }
            }
            return true;
        }
    }
}
