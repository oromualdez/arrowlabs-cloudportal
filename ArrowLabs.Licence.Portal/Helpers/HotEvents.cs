﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrowLabs.Licence.Portal.Helpers
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class HotEvents
    {

        private HotEventsHotEvent hotEventField;

        /// <remarks/>
        public HotEventsHotEvent HotEvent
        {
            get
            {
                return this.hotEventField;
            }
            set
            {
                this.hotEventField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class HotEventsHotEvent
    {

        private string cameraIndexField;

        private string cameraNameField;

        private string longtitudeField;

        private string latitudeField;

        private string hotEventIDField;

        private string pcNameField;

        private string ipField;

        private string startDateField;

        private string endDateField;

        /// <remarks/>
        public string CameraIndex
        {
            get
            {
                return this.cameraIndexField;
            }
            set
            {
                this.cameraIndexField = value;
            }
        }

        /// <remarks/>
        public string CameraName
        {
            get
            {
                return this.cameraNameField;
            }
            set
            {
                this.cameraNameField = value;
            }
        }

        /// <remarks/>
        public string Longtitude
        {
            get
            {
                return this.longtitudeField;
            }
            set
            {
                this.longtitudeField = value;
            }
        }

        /// <remarks/>
        public string Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public string HotEventID
        {
            get
            {
                return this.hotEventIDField;
            }
            set
            {
                this.hotEventIDField = value;
            }
        }

        /// <remarks/>
        public string PcName
        {
            get
            {
                return this.pcNameField;
            }
            set
            {
                this.pcNameField = value;
            }
        }

        /// <remarks/>
        public string IP
        {
            get
            {
                return this.ipField;
            }
            set
            {
                this.ipField = value;
            }
        }

        /// <remarks/>
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public string EndDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }
    }

}