﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class EventHistoryAudit
    {
        public BaseAudit BaseAudit { get; set; }  
        public int Id { get; set; }
        public int EventId { get; set; }
        public string UserName { get; set; }
        public string CreatedBy { get; set; }
        public int IncidentAction { get; set; }
        public string IncidentStatus { get; set; }
        public string IncidentDescription { get; set; }
        public string Remarks { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Latitude { get; set; }
        public string Longtitude { get; set; }

        public int SiteId { get; set; }

                public int CustomerId { get; set; }
        

        public static bool InsertEventHistory(EventHistoryAudit eventHistory, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(eventHistory.BaseAudit, dbConnection);

            if (eventHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertEventHistory", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                    cmd.Parameters["@EventId"].Value = eventHistory.EventId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = eventHistory.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Action", SqlDbType.Int));
                    cmd.Parameters["@Action"].Value = eventHistory.IncidentAction;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longitude"].Value = eventHistory.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = eventHistory.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = eventHistory.Remarks;


                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;


                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = eventHistory.CustomerId;

                    cmd.CommandText = "InsertEventHistory";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
