﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ItemFoundRemarks
    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int CustomerId { get; set; }
        public string Remarks { get; set; }
        public int SiteId { get; set; } 
        public string FName { get; set; }
        public string LName { get; set; } 
        public string CustomerUName { get; set; }
        public string Reference { get; set; }
        public int ItemFoundId { get; set; } 
        public int ItemInquiryId { get; set; } 
        public int ItemDisposeId { get; set; } 
        public int ItemLostId { get; set; } 
        public int ItemOwnerId { get; set; }
        public static List<ItemFoundRemarks> GetItemFoundRemarksByItemOwnerId(int taskId, string dbConnection)
        {
            var taskRemarks = new List<ItemFoundRemarks>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetItemFoundRemarksByItemOwnerId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var task = GetItemFoundRemarksFromSqlReader(reader);
                    taskRemarks.Add(task);
                }
                connection.Close();
                reader.Close();
            }
            return taskRemarks;
        }
        public static List<ItemFoundRemarks> GetItemFoundRemarksByItemLostId(int taskId, string dbConnection)
        {
            var taskRemarks = new List<ItemFoundRemarks>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetItemFoundRemarksByItemLostId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var task = GetItemFoundRemarksFromSqlReader(reader);
                    taskRemarks.Add(task);
                }
                connection.Close();
                reader.Close();
            }
            return taskRemarks;
        }
        public static List<ItemFoundRemarks> GetItemFoundRemarksByItemDisposeId(int taskId, string dbConnection)
        {
            var taskRemarks = new List<ItemFoundRemarks>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetItemFoundRemarksByItemDisposeId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var task = GetItemFoundRemarksFromSqlReader(reader);
                    taskRemarks.Add(task);
                }
                connection.Close();
                reader.Close();
            }
            return taskRemarks;
        }
        public static List<ItemFoundRemarks> GetItemFoundRemarksByItemInquiryId(int taskId, string dbConnection)
        {
            var taskRemarks = new List<ItemFoundRemarks>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetItemFoundRemarksByItemInquiryId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var task = GetItemFoundRemarksFromSqlReader(reader);
                    taskRemarks.Add(task);
                }
                connection.Close();
                reader.Close();
            }
            return taskRemarks;
        }
        public static int InsertOrUpdateItemFoundRemarks(ItemFoundRemarks taskRemarks, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = taskRemarks.Id;

            var action = (taskRemarks.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (taskRemarks != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateItemFoundRemarks", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskRemarks.Id;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = taskRemarks.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = taskRemarks.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = taskRemarks.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = taskRemarks.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = taskRemarks.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    cmd.Parameters["@Reference"].Value = taskRemarks.Reference;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = taskRemarks.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    cmd.Parameters["@ItemFoundId"].Value = taskRemarks.ItemFoundId;

                    cmd.Parameters.Add(new SqlParameter("@ItemInquiryId", SqlDbType.Int));
                    cmd.Parameters["@ItemInquiryId"].Value = taskRemarks.ItemInquiryId;

                    cmd.Parameters.Add(new SqlParameter("@ItemOwnerId", SqlDbType.Int));
                    cmd.Parameters["@ItemOwnerId"].Value = taskRemarks.ItemOwnerId;

                    cmd.Parameters.Add(new SqlParameter("@ItemLostId", SqlDbType.Int));
                    cmd.Parameters["@ItemLostId"].Value = taskRemarks.ItemLostId;

                    cmd.Parameters.Add(new SqlParameter("@ItemDisposeId", SqlDbType.Int));
                    cmd.Parameters["@ItemDisposeId"].Value = taskRemarks.ItemDisposeId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = taskRemarks.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateItemFoundRemarks";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            taskRemarks.Id = id;
                            var audit = new ItemFoundRemarksAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, taskRemarks.UpdatedBy);
                            Reflection.CopyProperties(taskRemarks, audit);
                            ItemFoundRemarksAudit.InsertItemFoundRemarksAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertItemFoundRemarksAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static ItemFoundRemarks GetItemFoundRemarksFromSqlReader(SqlDataReader reader)
        {
            var taskRemarks = new ItemFoundRemarks();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                taskRemarks.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["Remarks"] != DBNull.Value)
                taskRemarks.Remarks = reader["Remarks"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                taskRemarks.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["UpdatedDate"] != DBNull.Value)
                taskRemarks.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                taskRemarks.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedBy"] != DBNull.Value)
                taskRemarks.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["CustomerId"] != DBNull.Value)
                taskRemarks.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
             
            if (columns.Contains("CustomerUName"))
                taskRemarks.CustomerUName = reader["CustomerUName"].ToString();


            if (reader["SiteId"] != DBNull.Value)
                taskRemarks.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains("FName") && reader["FName"] != DBNull.Value)
                taskRemarks.FName = reader["FName"].ToString();

            if (columns.Contains("LName") && reader["LName"] != DBNull.Value)
                taskRemarks.LName = reader["LName"].ToString();
             
            if (columns.Contains("Reference") && reader["Reference"] != DBNull.Value)
                taskRemarks.Reference = reader["Reference"].ToString(); 
            if (columns.Contains("ItemFoundId") & reader["ItemFoundId"] != DBNull.Value)
                taskRemarks.ItemFoundId = Convert.ToInt32(reader["ItemFoundId"].ToString());
            if (columns.Contains("ItemInquiryId") & reader["ItemInquiryId"] != DBNull.Value)
                taskRemarks.ItemInquiryId = Convert.ToInt32(reader["ItemInquiryId"].ToString());
            if (columns.Contains("ItemDisposeId") & reader["ItemDisposeId"] != DBNull.Value)
                taskRemarks.ItemDisposeId = Convert.ToInt32(reader["ItemDisposeId"].ToString()); 
            if (columns.Contains("ItemOwnerId") & reader["ItemOwnerId"] != DBNull.Value)
                taskRemarks.ItemOwnerId = Convert.ToInt32(reader["ItemOwnerId"].ToString());
            if (columns.Contains("ItemLostId") & reader["ItemLostId"] != DBNull.Value)
                taskRemarks.ItemLostId = Convert.ToInt32(reader["ItemLostId"].ToString());
  
            return taskRemarks;
        }
        public static List<ItemFoundRemarks> GetItemFoundRemarksByItemFoundId(int taskId, string dbConnection)
        {
            var taskRemarks = new List<ItemFoundRemarks>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetItemFoundRemarksByItemFoundId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var task = GetItemFoundRemarksFromSqlReader(reader);
                    taskRemarks.Add(task);
                }
                connection.Close();
                reader.Close();
            }
            return taskRemarks;
        } 
        public static ItemFoundRemarks GetItemFoundRemarksById(int id, string dbConnection)
        {
            ItemFoundRemarks task = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetItemFoundRemarksById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    task = GetItemFoundRemarksFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return task;
        }
        public static void DeleteItemFoundRemarksById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteItemFoundRemarksById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteItemFoundRemarksById";

                cmd.ExecuteNonQuery();
            }
        }

    }
}
