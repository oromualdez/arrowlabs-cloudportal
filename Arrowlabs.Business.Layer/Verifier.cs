﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Drawing.Imaging;
using Arrowlabs.Mims.Audit.Models;
using System.Net;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class Verifier
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Type { get; set; }
        public string UpdatedBy { get; set; }
        [DataMember]
        public float Longitude { get; set; }
        [DataMember]
        public float Latitude { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CaseId { get; set; }
        [DataMember]
        public byte[] SentImageFile { get; set; }
        [DataMember]
        public int Request { get; set; }
        [DataMember]
        public string Reason { get; set; }
        [DataMember]
        public string TransactionID { get; set; }
        [DataMember]
        public string TypeName { get; set; }
        [DataMember]
        public byte[] ResultImage { get; set; }
        [DataMember]
        public string Score { get; set; }
        [DataMember]
        public string Rank { get; set; }
        [DataMember]
        public Enrollment EnrollmentResult { get; set; }
        [DataMember]
        public List<VerifierResults> results = new List<VerifierResults>();

        [DataMember]
        public int SiteId { get; set; }
       
        public enum VerifierTypes
        {
            FRS = 0,
            ANPR = 1
        }
        public enum RequestTypes
        {
            Enroll = 0,
            Verify = 1,
            Enrolled = 2,
            Rejected = 3
        }
        public static List<string> GetVerifierTypes()
        {
            return Enum.GetNames(typeof(VerifierTypes)).ToList();
        }

        private static Verifier GetVerifierFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var verifier = new Verifier();
            if (reader["Id"] != DBNull.Value)
                verifier.Id = Convert.ToInt16(reader["Id"].ToString());
            if (reader["Type"] != DBNull.Value)
                verifier.Type = Convert.ToInt16(reader["Type"].ToString());

            if (verifier.Type == (int)VerifierTypes.ANPR)
                verifier.TypeName = "ANPR";
            else
                verifier.TypeName = "FRS";
            if (reader["CaseId"] != DBNull.Value)
                verifier.CaseId = Convert.ToInt16(reader["CaseId"].ToString());
            if (reader["SentImageFile"] != DBNull.Value)
                verifier.SentImageFile = (byte[])reader["SentImageFile"];
            if (reader["TransactionID"] != DBNull.Value)
                verifier.TransactionID = reader["TransactionID"].ToString();
            if (reader["Reason"] != DBNull.Value)
                verifier.Reason = reader["Reason"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                verifier.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                verifier.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["Longitude"] != DBNull.Value)
                verifier.Longitude = Convert.ToSingle(reader["Longitude"].ToString());
            if (reader["Latitude"] != DBNull.Value)
                verifier.Latitude = Convert.ToSingle(reader["Latitude"].ToString());
            if (reader["Request"] != DBNull.Value)
                verifier.Request = Convert.ToInt16(reader["Request"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                verifier.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            return verifier;
        }

        private static Verifier GetVerifierNoBytesFromSqlReader(SqlDataReader reader)
        {
            var verifier = new Verifier();
            if (reader["Id"] != DBNull.Value)
                verifier.Id = Convert.ToInt16(reader["Id"].ToString());
            if (reader["Type"] != DBNull.Value)
                verifier.Type = Convert.ToInt16(reader["Type"].ToString());

            if (verifier.Type == (int)VerifierTypes.ANPR)
                verifier.TypeName = "ANPR";
            else
                verifier.TypeName = "FRS";
            if (reader["CaseId"] != DBNull.Value)
                verifier.CaseId = Convert.ToInt16(reader["CaseId"].ToString());
            if (reader["TransactionID"] != DBNull.Value)
                verifier.TransactionID = reader["TransactionID"].ToString();
            if (reader["Reason"] != DBNull.Value)
                verifier.Reason = reader["Reason"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                verifier.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                verifier.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["Longitude"] != DBNull.Value)
                verifier.Longitude = Convert.ToSingle(reader["Longitude"].ToString());
            if (reader["Latitude"] != DBNull.Value)
                verifier.Latitude = Convert.ToSingle(reader["Latitude"].ToString());
            if (reader["Request"] != DBNull.Value)
                verifier.Request = Convert.ToInt16(reader["Request"].ToString());

            return verifier;
        }

        public static int InsertOrUpdateVerifier(Verifier verifier, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            if (verifier != null)
            {
                id = verifier.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVerifier", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = verifier.Id;

                    cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                    cmd.Parameters["@Type"].Value = verifier.Type;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = verifier.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = verifier.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Reason", SqlDbType.NVarChar));
                    cmd.Parameters["@Reason"].Value = verifier.Reason;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = verifier.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedDate"].Value = verifier.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CaseId", SqlDbType.Int));
                    cmd.Parameters["@CaseId"].Value = verifier.CaseId;

                    cmd.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                    cmd.Parameters["@Request"].Value = verifier.Request;

                    cmd.Parameters.Add(new SqlParameter("@TransactionID", SqlDbType.NVarChar));
                    cmd.Parameters["@TransactionID"].Value = verifier.TransactionID;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = verifier.SiteId;
                    //if (verifier.SentImageFile != null)
                    //{
                    //    cmd.Parameters.Add(new SqlParameter("@SentImageFile", SqlDbType.VarBinary));
                    //    cmd.Parameters["@SentImageFile"].Value = Decompress(verifier.SentImageFile);
                    //}

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.CommandText = "InsertOrUpdateVerifier";

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            verifier.Id = id;
                            var audit = new VerifierAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, verifier.UpdatedBy);
                            Reflection.CopyProperties(verifier, audit);
                            VerifierAudit.InsertVerifierAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateVerifier", ex, auditDbConnection);
                    }
                }
            }
            return id;
        }

        public static List<Verifier> GetAllVerifier(string dbConnection)
        {
            var verifiers = new List<Verifier>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllVerifier";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);
                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierByType(int Type, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByType", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByType";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierByRequest(int Request, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByRequest", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = Request;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByRequest";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierNoBytesByRequest(int Request, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierNoBytesByRequest", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = Request;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierNoBytesByRequest";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierNoBytesFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }
        public static List<Verifier> GetAllVerifierNoBytesByRequestAndSite(int Request,int siteid ,string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierNoBytesByRequestAndSite", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = Request;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierNoBytesByRequestAndSite";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierNoBytesFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }
        public static List<Verifier> GetAllVerifierByCreatedBy(string name, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByCreatedBy", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByCreatedBy";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static Verifier GetAllVerifierByCaseId(int caseId, string dbConnection)
        {
            Verifier verifiers = null;

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByCaseId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@CaseId", SqlDbType.Int));
                sqlCommand.Parameters["@CaseId"].Value = caseId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByCaseId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    verifiers = GetVerifierFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierByManagerIdAndRequest(int id, int request, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByManagerIdAndRequest", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = request;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByManagerIdAndRequest";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierNoBytesByManagerIdAndRequest(int id, int request, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierNoBytesByManagerIdAndRequest", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = request;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierNoBytesByManagerIdAndRequest";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierNoBytesFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierByManagerIdAndRequestAndType(int id, int request, int type, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByManagerIdAndRequestAndType", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = request;
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = type;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByManagerIdAndRequestAndType";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierByManagerId(string id, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByManagerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByManagerId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static Verifier GetVerifierById(int caseId, string dbConnection)
        {
            Verifier verifiers = null;

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVerifierById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = caseId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetVerifierById";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    verifiers = GetVerifierFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierByTypeAndCreatedBy(string name, int Type, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByTypeAndCreatedBy", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByTypeAndCreatedBy";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }

        public static List<Verifier> GetAllVerifierByTypeAndManagerId(int id, int Type, string dbConnection)
        {
            var verifiers = new List<Verifier>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifierByTypeAndManagerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = Type;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVerifierByTypeAndManagerId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierFromSqlReader(reader);



                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }
        public static Verifier VerifierResult(Verifier verifier, string dbConnection,
    string auditDbConnection = "", bool isAuditEnabled = true)
        {
            try
            {
                verifier.results = new List<VerifierResults>();

                verifier.Id = InsertOrUpdateVerifier(verifier, dbConnection);
                
                var fvdbScan = new FVDBScan();
                var ValidService = false;
                foreach (var vs in VerifierService.GetAllVerifierService(dbConnection))
                {
                    try
                    {

                        fvdbScan.Url = vs.Url;
                        fvdbScan.alive();
                        ValidService = true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        {

                            file.WriteLine(ex.Message + "|GetAllVerifierService");
                        }
                    }
                }

                if (ValidService)
                {
                    try
                    {

                        var image = new Image();
                        image.binaryImg = verifier.SentImageFile;

                        var idResult = fvdbScan.identification(image, verifier.CreatedBy, 10000, 0.0f, 0);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        {

                            file.WriteLine(dbConnection + "|idResult");
                        }
                        var count = 0;
                        var mocsetting = Arrowlabs.Business.Layer.MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        mocsetting.FilePath = mocsetting.FilePath.Split(new string[] { "Video", "video" }, StringSplitOptions.RemoveEmptyEntries)[0] + @"\FaceRecognition\" + verifier.Id.ToString();
                        if (!Directory.Exists(mocsetting.FilePath))
                        {
                            Directory.CreateDirectory(mocsetting.FilePath);
                        }
                        foreach (var match in idResult.matches)
                        {

                            verifier.CaseId = Convert.ToInt16(match.caseID);
                            verifier.Rank = match.rank.ToString();
                            verifier.Score = match.score.ToString();
                            verifier.TransactionID = idResult.transactionId;
                            verifier.EnrollmentResult = Enrollment.GetAllEnrollmentByCaseId(verifier.CaseId, dbConnection);

                            var veriResult = new VerifierResults();
                            veriResult.CaseId = verifier.CaseId;
                            veriResult.Percent = match.score;
                            veriResult.CreatedDate = DateTime.Now;
                            veriResult.VerifierID = verifier.Id;
                            veriResult.FilePath = mocsetting.FilePath + @"\" + match.score.ToString() + ".jpeg";
                            var resultImage1 = fvdbScan.fvdbgetReferenceImage(match.caseID.ToString(), 250);
                            File.WriteAllBytes(veriResult.FilePath, resultImage1.img.binaryImg);

                            try
                            {
                                if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                                {
                                    VerifierResults.InsertOrUpdateVerifierResult(veriResult, dbConnection,auditDbConnection,true);
                                }
                                else
                                    VerifierResults.InsertOrUpdateVerifierResult(veriResult, dbConnection);
                            }
                            catch (Exception ex)
                            {
                                MIMSLog.MIMSLogSave("Business Layer Audit", "VerifierResult", ex, dbConnection);
                            }

                            
                            verifier.results.Add(veriResult);
                            if (count == 4)
                                break;
                            count++;
                        }
                        var resultImage = fvdbScan.fvdbgetReferenceImage(verifier.CaseId.ToString(), 250);
                        verifier.ResultImage = resultImage.img.binaryImg;
                        //verifier.SentImageFile = Decompress(verifier.SentImageFile);

                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {
                                InsertOrUpdateVerifier(verifier, dbConnection, auditDbConnection, true);
                            }
                            else
                                InsertOrUpdateVerifier(verifier, dbConnection);
                        }
                        catch (Exception ex)
                        {
                            MIMSLog.MIMSLogSave("Business Layer Audit", "VerifierResult", ex, dbConnection);
                        }


                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        {
                            file.WriteLine(ex.Message + "|VerifierResult");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                {

                    file.WriteLine(ex.Message + "|VerifierResult");
                }
            }
            return verifier;
        }
        public static Verifier VerifierResult(Verifier verifier, string dbConnection)
        {
            try
            {
                var retverifier = new Verifier();
                verifier.results = new List<VerifierResults>();
                verifier.Id = InsertOrUpdateVerifier(verifier, dbConnection);

                var fvdbScan = new FVDBScan();
                var ValidService = false;
                foreach (var vs in VerifierService.GetAllVerifierService(dbConnection))
                {
                    try
                    {

                        fvdbScan.Url = vs.Url;
                        fvdbScan.alive();
                        ValidService = true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        {
                            
                            file.WriteLine(ex.Message + "|GetAllVerifierService");
                        }
                    }
                }

                if (ValidService)
                {
                    try
                    {

                        var image = new Image();
                        image.binaryImg = verifier.SentImageFile;

                        var idResult = fvdbScan.identification(image, verifier.CreatedBy, 10000, 0.0f, 0);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        {

                            file.WriteLine(dbConnection + "|idResult");
                        }
                        var count = 0;
                     var mocsetting = Arrowlabs.Business.Layer.MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    mocsetting.FilePath = mocsetting.FilePath.Split(new string[] { "Video", "video" }, StringSplitOptions.RemoveEmptyEntries)[0] + @"\FaceRecognition\"+verifier.Id.ToString();
                    if (!Directory.Exists(mocsetting.FilePath))
                    {
                        Directory.CreateDirectory(mocsetting.FilePath);
                    }
                        foreach (var match in idResult.matches)
                        {

                            verifier.CaseId = Convert.ToInt16(match.caseID);
                            verifier.Rank = match.rank.ToString();
                            verifier.Score = match.score.ToString();
                            verifier.TransactionID = idResult.transactionId;
                            verifier.EnrollmentResult = Enrollment.GetAllEnrollmentByCaseId(verifier.CaseId, dbConnection);

                            var veriResult = new VerifierResults();
                            veriResult.CaseId = verifier.CaseId;
                            veriResult.Percent = match.score;
                            veriResult.CreatedDate = DateTime.Now;
                            veriResult.VerifierID = verifier.Id;
                            veriResult.FilePath = mocsetting.FilePath + @"\" + match.score.ToString() + ".jpeg";
                            var resultImage1 = fvdbScan.fvdbgetReferenceImage(match.caseID.ToString(), 250);
                            File.WriteAllBytes(veriResult.FilePath, resultImage1.img.binaryImg);
                            VerifierResults.InsertOrUpdateVerifierResult(veriResult, dbConnection);
                            verifier.results.Add(veriResult);
                            if (count == 4)
                                break;
                            count++;
                        }
                        var resultImage = fvdbScan.fvdbgetReferenceImage(verifier.CaseId.ToString(), 250);
                        verifier.ResultImage = resultImage.img.binaryImg;
                        //verifier.SentImageFile = Decompress(verifier.SentImageFile);

                        InsertOrUpdateVerifier(verifier, dbConnection);
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        {
                            file.WriteLine(ex.Message + "|VerifierResult");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                {

                    file.WriteLine(ex.Message + "|VerifierResult");
                }
            }
            return verifier;
        }
        public static List<Verifier> VerifierMultipleResult(Verifier verifier, string dbConnection)
        {
            var verifierList = new List<Verifier>();
            try
            {
                
                //verifier.Id = InsertOrUpdateVerifier(verifier, dbConnection);

                var fvdbScan = new FVDBScan();
                var ValidService = false;
                foreach (var vs in VerifierService.GetAllVerifierService(dbConnection))
                {
                    try
                    {

                        fvdbScan.Url = vs.Url;
                        fvdbScan.alive();
                        ValidService = true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                        //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        //{

                        //    file.WriteLine(ex.Message + "|GetAllVerifierService");
                        //}
                        
                    }
                }

                if (ValidService)
                {
                    try
                    {
                        var confSettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        var url =  confSettings.MIMSMobileAddress + "/Uploads/FaceRecognition/"+verifier.Id+".jpeg";
                        using (var wc = new WebClient())
                        {
                            verifier.SentImageFile = wc.DownloadData(url);
                        }
                        //MemoryStream ms = new MemoryStream(bytes);
                        //System.Drawing.Image image = System.Drawing.Image.FromStream(ms);

                        var image = new Image();
                        image.binaryImg = verifier.SentImageFile;

                        var idResult = fvdbScan.identification(image, verifier.CreatedBy, 10000, 0.0f, 0);
                        //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        //{

                        //    file.WriteLine(dbConnection + "|idResult");
                        //}
                        var index = 0;
                        foreach (var match in idResult.matches)
                        {
                            var newVerifier = new Verifier();
                            newVerifier.CaseId = Convert.ToInt16(match.caseID);
                            newVerifier.Rank = match.rank.ToString();
                            newVerifier.Score = match.score.ToString();
                            newVerifier.TransactionID = idResult.transactionId;
                            newVerifier.EnrollmentResult = Enrollment.GetAllEnrollmentByCaseId(newVerifier.CaseId, dbConnection);
                            index++;
                            var resultImage = fvdbScan.fvdbgetReferenceImage(newVerifier.CaseId.ToString(), 250);
                            newVerifier.ResultImage = resultImage.img.binaryImg;
                            newVerifier.SentImageFile = Decompress(verifier.SentImageFile);
                            verifierList.Add(newVerifier);
                            if (index == 5)
                            {
                                return verifierList;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                        {
                            
                            file.WriteLine(ex.Message + "|VerifierResult");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\WriteLines2.txt", true))
                {
                    
                    file.WriteLine(ex.Message + "|VerifierResult");
                }
            }
            return verifierList;
        }
        public static byte[] Decompress(byte[] inputData)
        {
            Byte[] outputBytes;
            var jpegQuality = 50;
            System.Drawing.Image image;
            using (var inputStream = new MemoryStream(inputData))
            {
                image = System.Drawing.Image.FromStream(inputStream);
                var jpegEncoder = ImageCodecInfo.GetImageDecoders()
                  .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, jpegQuality);

                using (var outputStream = new MemoryStream())
                {
                    image.Save(outputStream, jpegEncoder, encoderParameters);
                    outputBytes = outputStream.ToArray();
                }
            }
            return outputBytes;
        }
        public static bool DeleteVerfierById(int caseId, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("DeleteVerfierById", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = caseId;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteVerfierById";
                    sqlCommand.ExecuteNonQuery();
                    connection.Close();
                   
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static byte[] GetOriginalImageByCase(int caseId, string dbConnection)
        {
            var fvdbScan = new FVDBScan();
            var ValidService = false;
            foreach (var vs in VerifierService.GetAllVerifierService(dbConnection))
            {
                try
                {
                    fvdbScan.Url = vs.Url;
                    fvdbScan.alive();
                    ValidService = true;
                    break;
                }
                catch { }
            }

            if (ValidService)
            {
                var resultImage = fvdbScan.fvdbgetReferenceImage(caseId.ToString(), 250);
                if (resultImage.img != null)
                    return resultImage.img.binaryImg;

            }
            return null;
        }
        public static string GetCaseInformationByCaseId(int caseId, string dbConnection)
        {
            var fvdbScan = new FVDBScan();
            var ValidService = false;
            foreach (var vs in VerifierService.GetAllVerifierService(dbConnection))
            {
                try
                {
                    fvdbScan.Url = vs.Url;
                    fvdbScan.alive();
                    ValidService = true;
                    break;
                }
                catch { }
            }

            if (ValidService)
            {
                var resultImage = fvdbScan.fvdbgetCaseProperties(caseId.ToString());//fvdbgetReferenceImage(caseId.ToString(), 250);
                if (resultImage.Length > 0)
                    return resultImage[1].value;
                //if (resultImage.img != null)
                //    return resultImage.img.binaryImg;

            }
            return null;
        }
    }
}
