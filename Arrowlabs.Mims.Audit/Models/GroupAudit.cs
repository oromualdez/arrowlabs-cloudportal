﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class  GroupAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }

        private static GroupAudit GetGroupAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var group = new GroupAudit();
            group.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                group.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                group.Name = reader["Name"].ToString();
            if (columns.Contains("CreateDate") && reader["CreateDate"] != DBNull.Value)
                group.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                group.CreatedBy = reader["CreatedBy"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                group.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return group;
        }
        public static List<GroupAudit> GetAllGroupAudit(string dbConnection)
        {
            var groupAudits = new List<GroupAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllGroupAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var groupAudit = GetGroupAuditFromSqlReader(reader);
                    groupAudits.Add(groupAudit);
                }
                connection.Close();
                reader.Close();
            }
            return groupAudits;
        }
        public static bool InsertGroupAudit(GroupAudit groupAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(groupAudit.BaseAudit, dbConnection);

            if (groupAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertGroupAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = groupAudit.Id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = groupAudit.Name;
                    cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = groupAudit.CreateDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = groupAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = groupAudit.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = groupAudit.CustomerId; 
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
