﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
namespace Arrowlabs.Mims.Audit.Models
{
    public class JobOTHoursAudit
    {
        public BaseAudit BaseAudit { get; set; }

        public int Id { get; set; }
        public DateTime? OTStart { get; set; }
        public DateTime? OTEnd { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int TaskId { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string CreatedBy { get; set; }
        public string Username { get; set; }

        public static int InsertJobOTHoursAudit(JobOTHoursAudit customer, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(customer.BaseAudit, dbConnection);

            int id = 0;
            if (customer != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertJobOTHoursAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customer.Id;

                    cmd.Parameters.Add(new SqlParameter("@OTStart", SqlDbType.DateTime));
                    cmd.Parameters["@OTStart"].Value = customer.OTStart;
                    cmd.Parameters.Add(new SqlParameter("@OTEnd", SqlDbType.DateTime));
                    cmd.Parameters["@OTEnd"].Value = customer.OTEnd;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customer.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = customer.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customer.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = customer.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = customer.UserId;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customer.Username;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customer.CreatedBy;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertJobOTHoursAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
