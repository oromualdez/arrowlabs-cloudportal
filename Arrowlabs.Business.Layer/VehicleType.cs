﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
   public class VehicleType
   {
       public int ID { get; set; }
       public string VehicleTypeDesc { get; set; }
       public string VehicleTypeDesc_AR { get; set; }
       public DateTime? CreatedDate { get; set; }
       public string UpdatedBy { get; set; }
       public DateTime? UpdatedDate { get; set; }
       public string CreatedBy { get; set; }

       public static List<VehicleType> GetAllVehicleType(string dbConnection)
       {
           var locations = new List<VehicleType>();
           using (var connection = new SqlConnection(dbConnection))
           {
               connection.Open();
               var sqlCommand = new SqlCommand("GetAllVehicleType", connection);


               sqlCommand.CommandText = "GetAllVehicleType";

               var reader = sqlCommand.ExecuteReader();
               while (reader.Read())
               {
                   var vehicleType = new VehicleType();

                   vehicleType.ID = Convert.ToInt32(reader["Id"].ToString());
                   vehicleType.VehicleTypeDesc = reader["VEHICLETYPEDESC"].ToString();
                   vehicleType.VehicleTypeDesc_AR = reader["VEHICLETYPEDESC_AR"].ToString();

                   vehicleType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                   vehicleType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                   vehicleType.CreatedBy = reader["CreatedBy"].ToString();
                   locations.Add(vehicleType);
               }
               connection.Close();
               reader.Close();
           }
           return locations;
       }

       public static List<VehicleType> GetAllVehicleTypeByDate(DateTime updatedDate, string dbConnection)
       {
           var locations = new List<VehicleType>();
           using (var connection = new SqlConnection(dbConnection))
           {
               connection.Open();
               var sqlCommand = new SqlCommand("GetAllVehicleTypeByDate", connection);
               sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
               sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
               sqlCommand.CommandType = CommandType.StoredProcedure;

               sqlCommand.CommandText = "GetAllVehicleTypeByDate";

               var reader = sqlCommand.ExecuteReader();
               while (reader.Read())
               {
                   var vehicleType = new VehicleType();

                   vehicleType.ID = Convert.ToInt32(reader["Id"].ToString());
                   vehicleType.VehicleTypeDesc = reader["VEHICLETYPEDESC"].ToString();
                   vehicleType.VehicleTypeDesc_AR = reader["VEHICLETYPEDESC_AR"].ToString();

                   vehicleType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                   vehicleType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                   vehicleType.CreatedBy = reader["CreatedBy"].ToString();
                   locations.Add(vehicleType);
               }
               connection.Close();
               reader.Close();
           }
           return locations;
       }

       public static bool InsertorUpdateVehicleType(VehicleType vehicleType, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
       {
           if (vehicleType != null)
           {
               int id = vehicleType.ID;
               var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

               using (var connection = new SqlConnection(dbConnection))
               {
                   connection.Open();
                   var cmd = new SqlCommand("InsertOrUpdateVehicleType", connection);

                   cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                   cmd.Parameters["@Id"].Value = vehicleType.ID;

                   cmd.Parameters.Add(new SqlParameter("@VEHICLETYPEDESC", SqlDbType.NVarChar));
                   cmd.Parameters["@VEHICLETYPEDESC"].Value = vehicleType.VehicleTypeDesc;

                   cmd.Parameters.Add(new SqlParameter("@VEHICLETYPEDESC_AR", SqlDbType.NVarChar));
                   cmd.Parameters["@VEHICLETYPEDESC_AR"].Value = vehicleType.VehicleTypeDesc_AR;

                   cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                   cmd.Parameters["@CreatedDate"].Value = vehicleType.CreatedDate;

                   cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                   cmd.Parameters["@UpdatedDate"].Value = vehicleType.UpdatedDate;

                   cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                   cmd.Parameters["@CreatedBy"].Value = vehicleType.CreatedBy;

                   var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                   returnParameter.Direction = ParameterDirection.ReturnValue;

                   cmd.CommandType = CommandType.StoredProcedure;

                   cmd.CommandText = "InsertOrUpdateVehicleType";

                   cmd.ExecuteNonQuery();
                   if (id == 0)
                       id = (int)returnParameter.Value;

                   try
                   {
                       if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                       {
                           vehicleType.ID = id;
                           var audit = new VehicleTypeAudit();
                           audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, vehicleType.UpdatedBy);
                           Reflection.CopyProperties(vehicleType, audit);
                           VehicleTypeAudit.InsertVehicleTypeAudit(audit, auditDbConnection);
                       }
                   }
                   catch (Exception ex)
                   {
                       MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateVehicleType", ex, auditDbConnection);
                   }
               }
           }
           return true;
       }

    }
}
