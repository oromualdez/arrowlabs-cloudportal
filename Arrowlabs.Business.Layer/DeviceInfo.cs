﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
  public class DeviceInfo
  {
      public string DeviceName { get; set; }
      public float Longitude { get; set; }
      public float Latitude { get; set; }
      public string CameraIP { get; set; }
    }
}
