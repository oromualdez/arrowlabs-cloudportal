﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ItemFound
    {
        public int Id { get; set; }
        public DateTime DateFound { get; set; }
        public string LocationFound { get; set; }
        public string RoomNumber { get; set; }
        public string Type { get; set; }
        public string Brand { get; set; }
        public string Colour { get; set; }
        public string Status { get; set; }
        public string FinderName { get; set; }
        public string FinderDepartment { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiveDate { get; set; }
        public string StorageLocation { get; set; }
        public string Reference { get; set; }
        public string ImagePath { get; set; }

        public string Remarks { get; set; }
        public string CustomerUName { get; set; }
        public int SiteId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string ShelfLife { get; set; }
        public string SubType { get; set; }
        public string SubStorageLocation { get; set; }

        public string TransferFromSiteName { get; set; }

        public int TransferFromSiteId { get; set; }

        public int CustomerId { get; set; }

        private static ItemFound GetItemFoundFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var itemFound = new ItemFound();
            if (reader["Id"] != DBNull.Value)
                itemFound.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["DateFound"] != DBNull.Value)
                itemFound.DateFound = Convert.ToDateTime(reader["DateFound"].ToString());
            if (reader["LocationFound"] != DBNull.Value)
                itemFound.LocationFound = reader["LocationFound"].ToString();
            if (reader["RoomNumber"] != DBNull.Value)
                itemFound.RoomNumber = reader["RoomNumber"].ToString();
            if (reader["Type"] != DBNull.Value)
                itemFound.Type = reader["Type"].ToString();
            if (reader["Brand"] != DBNull.Value)
                itemFound.Brand = reader["Brand"].ToString();
            if (reader["Colour"] != DBNull.Value)
                itemFound.Colour = reader["Colour"].ToString();
            if (reader["Status"] != DBNull.Value)
                itemFound.Status = reader["Status"].ToString();
            if (reader["FinderName"] != DBNull.Value)
                itemFound.FinderName = reader["FinderName"].ToString();
            if (reader["FinderDepartment"] != DBNull.Value)
                itemFound.FinderDepartment = reader["FinderDepartment"].ToString();
            if (reader["ReceiverName"] != DBNull.Value)
                itemFound.ReceiverName = reader["ReceiverName"].ToString();
            if (reader["ReceiveDate"] != DBNull.Value)
                itemFound.ReceiveDate = reader["ReceiveDate"].ToString();
            if (reader["StorageLocation"] != DBNull.Value)
                itemFound.StorageLocation = reader["StorageLocation"].ToString();
            if (reader["Reference"] != DBNull.Value)
                itemFound.Reference = reader["Reference"].ToString();
            if (reader["ImagePath"] != DBNull.Value)
                itemFound.ImagePath = reader["ImagePath"].ToString();

            if (reader["SiteId"] != DBNull.Value)
                itemFound.SiteId = Convert.ToInt32(reader["SiteId"]);
            if (reader["CreatedDate"] != DBNull.Value)
                itemFound.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                itemFound.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                itemFound.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["UpdatedBy"] != DBNull.Value)
                itemFound.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["TransferFromSiteId"] != DBNull.Value)
                itemFound.TransferFromSiteId = Convert.ToInt32(reader["TransferFromSiteId"].ToString());

            if (columns.Contains( "TransferFromSiteId") && reader["TransferFromSiteId"] != DBNull.Value)
                itemFound.TransferFromSiteName = reader["TransferFromSiteId"].ToString();

            if (columns.Contains( "SubStorageLocation") && reader["SubStorageLocation"] != DBNull.Value)
                itemFound.SubStorageLocation = reader["SubStorageLocation"].ToString();

            if (columns.Contains( "SubType") && reader["SubType"] != DBNull.Value)
                itemFound.SubType = reader["SubType"].ToString();

            if (columns.Contains( "ShelfLife") && reader["ShelfLife"] != DBNull.Value)
                itemFound.ShelfLife = reader["ShelfLife"].ToString();

            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                itemFound.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains("Remarks") && reader["Remarks"] != DBNull.Value)
                itemFound.Remarks = reader["Remarks"].ToString();

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                itemFound.CustomerUName = reader["CustomerUName"].ToString();
            
            return itemFound;
        }

        public static List<ItemFound> GetAllItemFound(string dbConnection)
        { 
            var itemFounds = new List<ItemFound>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllItemFound", connection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetItemFoundFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }
               
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetAllItemFound", exp, dbConnection);
                
            } 
            return itemFounds;
        }

        public static List<ItemFound> GetItemFoundByLevel5(int siteId, string dbConnection)
        {
            var itemFounds = new List<ItemFound>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemFoundByLevel5", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetItemFoundFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetItemFoundBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }
        public static List<ItemFound> GetItemFoundBySiteId(int siteId,string dbConnection)
        {
            var itemFounds = new List<ItemFound>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemFoundBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetItemFoundFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetItemFoundBySiteId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<ItemFound> GetItemFoundByCId(int siteId, string dbConnection)
        {
            var itemFounds = new List<ItemFound>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemFoundByCId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetItemFoundFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetItemFoundByCId", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<ItemFound> SearchItemFound(string itemType, string fromDate, string toDate, string brand, string location, string colour, string searchItemSubSelect, string searchLocationStoreSelect, string searchSubStoreSelect, string searchShelfLifeSelect,string description, int siteId, string dbConnection)
        {
            var itemFounds = new List<ItemFound>();

                      

            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("SearchItemFound", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@ItemType", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ItemType"].Value = String.IsNullOrEmpty(itemType) ? null : itemType;
                    sqlCommand.Parameters.Add(new SqlParameter("@FromDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@FromDate"].Value = fromDate;
                    sqlCommand.Parameters.Add(new SqlParameter("@ToDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@ToDate"].Value = toDate;
                    sqlCommand.Parameters.Add(new SqlParameter("@Brand", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Brand"].Value = String.IsNullOrEmpty(brand) ? null : brand;
                    sqlCommand.Parameters.Add(new SqlParameter("@Location", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Location"].Value = String.IsNullOrEmpty(location) ? null : location;
                    sqlCommand.Parameters.Add(new SqlParameter("@Colour", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Colour"].Value = String.IsNullOrEmpty(colour) ? null : colour;

                    sqlCommand.Parameters.Add(new SqlParameter("@StorageLocation", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@StorageLocation"].Value = String.IsNullOrEmpty(searchLocationStoreSelect) ? null : searchLocationStoreSelect;
                    sqlCommand.Parameters.Add(new SqlParameter("@SubStorageLocation", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SubStorageLocation"].Value = String.IsNullOrEmpty(searchSubStoreSelect) ? null : searchSubStoreSelect; ;

                    sqlCommand.Parameters.Add(new SqlParameter("@SubType", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SubType"].Value = String.IsNullOrEmpty(searchItemSubSelect) ? null : searchItemSubSelect;

                    sqlCommand.Parameters.Add(new SqlParameter("@ShelfLife", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ShelfLife"].Value = String.IsNullOrEmpty(searchShelfLifeSelect) ? null : searchShelfLifeSelect;

                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;

                    sqlCommand.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Description"].Value = String.IsNullOrEmpty(description) ? null : description;
                    


                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetItemFoundFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "SearchItemFound", exp, dbConnection);

            }
           
            return itemFounds;
        }

        public static List<ItemFound> SearchItemFoundSuperAdmin(string itemType, string fromDate, string toDate, string brand, string location, string colour, string searchItemSubSelect, string searchLocationStoreSelect, string searchSubStoreSelect, string searchShelfLifeSelect, string description, string dbConnection)
        {
            var itemFounds = new List<ItemFound>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("SearchItemFoundSuperAdmin", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@ItemType", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ItemType"].Value =  String.IsNullOrEmpty(itemType)?null:itemType;
                    sqlCommand.Parameters.Add(new SqlParameter("@FromDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@FromDate"].Value = fromDate;
                    sqlCommand.Parameters.Add(new SqlParameter("@ToDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@ToDate"].Value = toDate;
                    sqlCommand.Parameters.Add(new SqlParameter("@Brand", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Brand"].Value = String.IsNullOrEmpty(brand) ? null : brand;
                    sqlCommand.Parameters.Add(new SqlParameter("@Location", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Location"].Value = String.IsNullOrEmpty(location) ? null : location; 
                    sqlCommand.Parameters.Add(new SqlParameter("@Colour", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Colour"].Value = String.IsNullOrEmpty(colour) ? null : colour; 

                    sqlCommand.Parameters.Add(new SqlParameter("@StorageLocation", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@StorageLocation"].Value = String.IsNullOrEmpty(searchLocationStoreSelect) ? null : searchLocationStoreSelect;
                    sqlCommand.Parameters.Add(new SqlParameter("@SubStorageLocation", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SubStorageLocation"].Value = String.IsNullOrEmpty(searchSubStoreSelect) ? null : searchSubStoreSelect; ;

                    sqlCommand.Parameters.Add(new SqlParameter("@SubType", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@SubType"].Value = String.IsNullOrEmpty(searchItemSubSelect) ? null : searchItemSubSelect; 

                    sqlCommand.Parameters.Add(new SqlParameter("@ShelfLife", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ShelfLife"].Value = String.IsNullOrEmpty(searchShelfLifeSelect) ? null : searchShelfLifeSelect;

                    sqlCommand.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Description"].Value = String.IsNullOrEmpty(description) ? null : description;


                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetItemFoundFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "SearchItemFound", exp, dbConnection);

            }
            return itemFounds;
        }

        public static List<ItemFound> GetAllItemFoundByDateRange(string fromdate,string todate,string dbConnection)
        {
            var itemFounds = new List<ItemFound>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllItemFound", connection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetItemFoundFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetAllItemFound", exp, dbConnection);

            }
            return itemFounds;
        }

        public static int GetLastItemFoundId(string dbConnection)
        {
            int lastUserId = 0;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLastItemFoundId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    lastUserId = Convert.ToInt32(reader["ID"].ToString());
                    break;
                }
                connection.Close();
                reader.Close();
            }
            return lastUserId;
        }

        public static int GetLastItemFoundIdBySiteId(DateTime from, DateTime todate, int siteid, string dbConnection)
        {
            int lastUserId = 0;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLastItemFoundBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.Parameters.Add(new SqlParameter("@fromdate", SqlDbType.DateTime));
                sqlCommand.Parameters["@fromdate"].Value = from;
                sqlCommand.Parameters.Add(new SqlParameter("@todate", SqlDbType.DateTime));
                sqlCommand.Parameters["@todate"].Value = todate;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    lastUserId = Convert.ToInt32(reader["ID"].ToString());
                    break;
                }
                connection.Close();
                reader.Close();
            }
            return lastUserId;
        }

        public static ItemFound GetItemFoundById(int id, string dbConnection)
        {
            ItemFound itemFound = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemFoundById", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        itemFound = GetItemFoundFromSqlReader(reader);

                    }
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetItemFoundById", exp, dbConnection);
            } 
            return itemFound;
        }
        public static ItemFound GetItemFoundByReference(string reference, string dbConnection)
        {
            ItemFound itemFound = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemFoundByReference", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Reference"].Value = reference;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        itemFound = GetItemFoundFromSqlReader(reader);

                    }
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetItemFoundByReference", exp, dbConnection);
            }
            return itemFound;
        }

        public static int InsertorUpdateItemFound(ItemFound itemFound, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            if (itemFound != null)
            {
                id = itemFound.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateItemFound", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = itemFound.Id;
                    cmd.Parameters.Add(new SqlParameter("@DateFound", SqlDbType.DateTime));
                    cmd.Parameters["@DateFound"].Value = itemFound.DateFound;
                    cmd.Parameters.Add(new SqlParameter("@LocationFound", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationFound"].Value = itemFound.LocationFound;
                    cmd.Parameters.Add(new SqlParameter("@RoomNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@RoomNumber"].Value = itemFound.RoomNumber;
                    cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.NVarChar));
                    cmd.Parameters["@Type"].Value = itemFound.Type;
                    cmd.Parameters.Add(new SqlParameter("@Colour", SqlDbType.NVarChar));
                    cmd.Parameters["@Colour"].Value = itemFound.Colour;
                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.NVarChar));
                    cmd.Parameters["@Status"].Value = itemFound.Status;
                    cmd.Parameters.Add(new SqlParameter("@FinderName", SqlDbType.NVarChar));
                    cmd.Parameters["@FinderName"].Value = itemFound.FinderName;
                    cmd.Parameters.Add(new SqlParameter("@FinderDepartment", SqlDbType.NVarChar));
                    cmd.Parameters["@FinderDepartment"].Value = itemFound.FinderDepartment;
                    cmd.Parameters.Add(new SqlParameter("@ReceiverName", SqlDbType.NVarChar));
                    cmd.Parameters["@ReceiverName"].Value = itemFound.ReceiverName;
                    cmd.Parameters.Add(new SqlParameter("@ReceiveDate", SqlDbType.DateTime));
                    cmd.Parameters["@ReceiveDate"].Value = itemFound.ReceiveDate;
                    cmd.Parameters.Add(new SqlParameter("@StorageLocation", SqlDbType.NVarChar));
                    cmd.Parameters["@StorageLocation"].Value = itemFound.StorageLocation;
                    cmd.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    cmd.Parameters["@Reference"].Value = itemFound.Reference;
                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = itemFound.ImagePath;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = itemFound.SiteId;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = itemFound.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = itemFound.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = itemFound.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = itemFound.UpdatedBy;
                    cmd.Parameters.Add(new SqlParameter("@Brand", SqlDbType.NVarChar));
                    cmd.Parameters["@Brand"].Value = itemFound.Brand;

                    cmd.Parameters.Add(new SqlParameter("@SubStorageLocation", SqlDbType.NVarChar));
                    cmd.Parameters["@SubStorageLocation"].Value = itemFound.SubStorageLocation;

                    cmd.Parameters.Add(new SqlParameter("@SubType", SqlDbType.NVarChar));
                    cmd.Parameters["@SubType"].Value = itemFound.SubType;

                    cmd.Parameters.Add(new SqlParameter("@ShelfLife", SqlDbType.NVarChar));
                    cmd.Parameters["@ShelfLife"].Value = itemFound.ShelfLife;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = itemFound.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = itemFound.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@TransferFromSiteId", SqlDbType.Int));
                    cmd.Parameters["@TransferFromSiteId"].Value = itemFound.TransferFromSiteId;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            itemFound.Id = id;
                            var audit = new ItemFoundAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, itemFound.UpdatedBy);
                            Reflection.CopyProperties(itemFound, audit);
                            ItemFoundAudit.InsertorUpdateItemFound(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateItemFound", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static bool DeleteItemFoundById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteItemFoundById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static List<ItemFound> GetAllItemFoundGroupByUserId(int userid, string dbConnection)
        {
            //renamed for easier understanding originally called  GetAllItemFoundByGroupId
            var itemFounds = new List<ItemFound>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllItemFoundByGroupId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                    sqlCommand.Parameters["@GroupId"].Value = userid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemFound = GetItemFoundFromSqlReader(reader);
                        itemFounds.Add(itemFound);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetAllItemFoundByGroupId", exp, dbConnection);

            }
            return itemFounds;
        }
    }
}
