﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class TaskCanvasCategoryAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        private static TaskCanvasCategoryAudit GetTaskCanvasCategoryAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var hotEventCategory = new TaskCanvasCategoryAudit();
            hotEventCategory.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                hotEventCategory.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("CategoryId") && reader["CategoryId"] != DBNull.Value)
                hotEventCategory.CategoryId = Convert.ToInt32(reader["CategoryId"].ToString());
            if (columns.Contains("Description") && reader["Description"] != DBNull.Value)
                hotEventCategory.Description = reader["Description"].ToString();
            if (columns.Contains("Color") && reader["Color"] != DBNull.Value)
                hotEventCategory.Color = reader["Color"].ToString();
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                hotEventCategory.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                hotEventCategory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (columns.Contains("IsActive") && reader["IsActive"] != DBNull.Value)
                hotEventCategory.IsActive = Convert.ToBoolean(reader["IsActive"]);
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                hotEventCategory.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                hotEventCategory.SiteId = Convert.ToInt32(reader["SiteId"]);
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                hotEventCategory.CustomerId = Convert.ToInt32(reader["CustomerId"]);


            return hotEventCategory;
        }
        public static List<TaskCanvasCategoryAudit> GetAllTaskCanvasCategoryAudit(string dbConnection)
        {
            var taskCanvasCategories = new List<TaskCanvasCategoryAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("TaskCanvasCategoryAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var taskCanvasCategory = GetTaskCanvasCategoryAuditFromSqlReader(reader);
                    taskCanvasCategories.Add(taskCanvasCategory);
                }
                connection.Close();
                reader.Close();
            }
            return taskCanvasCategories;
        }
        public static bool InsertTaskCanvasCategoryAudit(TaskCanvasCategoryAudit taskCanvasCategoryAudit, string dbConnection)
        {
            if (taskCanvasCategoryAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskCanvasCategoryAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(taskCanvasCategoryAudit.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = taskCanvasCategoryAudit.Id;
                    cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = taskCanvasCategoryAudit.CategoryId;
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = taskCanvasCategoryAudit.Description;
                    cmd.Parameters.Add("@Color", SqlDbType.NVarChar).Value = taskCanvasCategoryAudit.Color;
                    cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = taskCanvasCategoryAudit.IsDeleted;
                    cmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = taskCanvasCategoryAudit.IsActive;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = taskCanvasCategoryAudit.CreatedBy;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = taskCanvasCategoryAudit.CreatedDate;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = taskCanvasCategoryAudit.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = taskCanvasCategoryAudit.CustomerId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
