﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
   public class ItemOwnerAudit
   {
       public BaseAudit BaseAudit { get; set; }
       public int Id { get; set; }
       public DateTime ReturnDate { get; set; }
       public string Name { get; set; }
       public string Type { get; set; }
       public string RoomNumber { get; set; }
       public string Nationality { get; set; }
       public string Address { get; set; }
       public string MobileNo { get; set; }
       public string Email { get; set; }
       public string IdNumber { get; set; }
       public string IdCopy { get; set; }
       public string ShippingNumber { get; set; }
       public string HandedBy { get; set; }
       public int ItemFoundId { get; set; }
       public int SiteId { get; set; }
       public string ItemReference { get; set; }
       public DateTime CreatedDate { get; set; }
       public DateTime UpdatedDate { get; set; }
       public string CreatedBy { get; set; }
       public string UpdatedBy { get; set; }

       public int TransferSiteId { get; set; }
       public int CustomerId { get; set; }
       
       private static ItemOwnerAudit GetItemFoundFromSqlReader(SqlDataReader reader)
       {
           var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
           var itemOwner = new ItemOwnerAudit();
           if (reader["Id"] != DBNull.Value)
               itemOwner.Id = Convert.ToInt32(reader["Id"].ToString());
           if (reader["ReturnDate"] != DBNull.Value)
               itemOwner.ReturnDate = Convert.ToDateTime(reader["ReturnDate"].ToString());
           if (reader["Name"] != DBNull.Value)
               itemOwner.Name = reader["Name"].ToString();
           if (reader["Type"] != DBNull.Value)
               itemOwner.Type = reader["Type"].ToString();
           if (reader["RoomNumber"] != DBNull.Value)
               itemOwner.RoomNumber = reader["RoomNumber"].ToString();
           if (reader["Nationality"] != DBNull.Value)
               itemOwner.Nationality = reader["Nationality"].ToString();
           if (reader["Address"] != DBNull.Value)
               itemOwner.Address = reader["Address"].ToString();
           if (reader["MobileNo"] != DBNull.Value)
               itemOwner.MobileNo = reader["MobileNo"].ToString();
           if (reader["Email"] != DBNull.Value)
               itemOwner.Email = reader["Email"].ToString();
           if (reader["IdNumber"] != DBNull.Value)
               itemOwner.IdNumber = reader["IdNumber"].ToString();
           if (reader["IdCopy"] != DBNull.Value)
               itemOwner.IdCopy = reader["IdCopy"].ToString();
           if (reader["ShippingNumber"] != DBNull.Value)
               itemOwner.ShippingNumber = reader["ShippingNumber"].ToString();
           if (reader["HandedBy"] != DBNull.Value)
               itemOwner.HandedBy = reader["HandedBy"].ToString();
           if (reader["ItemFoundId"] != DBNull.Value)
               itemOwner.ItemFoundId =  Convert.ToInt32(reader["ItemFoundId"].ToString());
           if (reader["SiteId"] != DBNull.Value)
               itemOwner.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
           if (reader["ItemReference"] != DBNull.Value)
               itemOwner.ItemReference = reader["ItemReference"].ToString();
           if (reader["CreatedDate"] != DBNull.Value)
               itemOwner.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
           if (reader["CreatedBy"] != DBNull.Value)
               itemOwner.CreatedBy = reader["CreatedBy"].ToString();
           if (reader["UpdatedDate"] != DBNull.Value)
               itemOwner.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
           if (reader["UpdatedBy"] != DBNull.Value)
               itemOwner.UpdatedBy = reader["UpdatedBy"].ToString();

           if (reader["TransferSiteId"] != DBNull.Value)
               itemOwner.TransferSiteId = Convert.ToInt32(reader["TransferSiteId"].ToString());

           return itemOwner;
       }

       public static void InsertorUpdateItemOwnerAudit(ItemOwnerAudit itemOwner, string auditDbConnection)
       { 
            if (itemOwner != null)
            {
                var baseAuditId = BaseAudit.InsertBaseAudit(itemOwner.BaseAudit, auditDbConnection);

                using (var connection = new SqlConnection(auditDbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateItemOwner", connection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = itemOwner.Id;
                    cmd.Parameters.Add(new SqlParameter("@ReturnDate", SqlDbType.DateTime));
                    cmd.Parameters["@ReturnDate"].Value = itemOwner.ReturnDate;
                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = itemOwner.Name;
                    cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.NVarChar));
                    cmd.Parameters["@Type"].Value = itemOwner.Type;
                    cmd.Parameters.Add(new SqlParameter("@RoomNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@RoomNumber"].Value = itemOwner.RoomNumber;
                    cmd.Parameters.Add(new SqlParameter("@Nationality", SqlDbType.NVarChar));
                    cmd.Parameters["@Nationality"].Value = itemOwner.Nationality;
                    cmd.Parameters.Add(new SqlParameter("@Address", SqlDbType.NVarChar));
                    cmd.Parameters["@Address"].Value = itemOwner.Address;
                    cmd.Parameters.Add(new SqlParameter("@MobileNo", SqlDbType.NVarChar));
                    cmd.Parameters["@MobileNo"].Value = itemOwner.MobileNo;
                    cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                    cmd.Parameters["@Email"].Value = itemOwner.Email;
                    cmd.Parameters.Add(new SqlParameter("@IdNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@IdNumber"].Value = itemOwner.IdNumber;
                    cmd.Parameters.Add(new SqlParameter("@IdCopy", SqlDbType.NVarChar));
                    cmd.Parameters["@IdCopy"].Value = itemOwner.IdCopy;
                    cmd.Parameters.Add(new SqlParameter("@ShippingNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@ShippingNumber"].Value = itemOwner.ShippingNumber;
                    cmd.Parameters.Add(new SqlParameter("@HandedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@HandedBy"].Value = itemOwner.HandedBy;
                    cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    cmd.Parameters["@ItemFoundId"].Value = itemOwner.ItemFoundId;
                    cmd.Parameters.Add(new SqlParameter("@ItemReference", SqlDbType.NVarChar));
                    cmd.Parameters["@ItemReference"].Value = itemOwner.ItemReference;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = itemOwner.SiteId;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = itemOwner.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = itemOwner.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = itemOwner.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = itemOwner.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                                            cmd.Parameters["@CustomerId"].Value = itemOwner.CustomerId;
                    cmd.Parameters.Add(new SqlParameter("@TransferSiteId", SqlDbType.Int));
                    cmd.Parameters["@TransferSiteId"].Value = itemOwner.TransferSiteId;
                 
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                   
                }
            }
            
       }
   }
}
