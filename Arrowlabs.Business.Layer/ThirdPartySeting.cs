﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ThirdPartySeting
    {
        public int Id { get; set; }

        public string UserName { get; set; }
        public string IPAddress { get; set; }
        public string Password { get; set; }  
        public string DeviceMacAddress { get; set; }
        public string ServerIP { get; set; }
        public int Port { get; set; }
        public ThirdPartySettingType VMSType { get; set; }
        public int ServerPort { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Name { get; set; }

        public static List<ThirdPartySeting> GetAllThirdPartySettingByType(string dbConnection, int type)
        {
            var thirdPartySetingList = new List<ThirdPartySeting>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllThirdPartySettingByType", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = type;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var thirdParty = GetThirdPartySetingFromSqlReader(reader,dbConnection);

                    thirdPartySetingList.Add(thirdParty);
                }
                connection.Close();
                reader.Close();
            }
            return thirdPartySetingList;
        }

        public static bool InsertorUpdateThirdPartySeting(ThirdPartySeting thirdPartySeting, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (thirdPartySeting.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;


            if (thirdPartySeting != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateThirdPartySetting", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = thirdPartySeting.Id;

                    cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                    cmd.Parameters["@Type"].Value = thirdPartySeting.VMSType.Id;

                    cmd.Parameters.Add(new SqlParameter("@IPAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@IPAddress"].Value = thirdPartySeting.IPAddress;

                    cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                    cmd.Parameters["@Password"].Value = Encrypt.EncryptData(thirdPartySeting.Password, true, dbConnection);

                    cmd.Parameters.Add(new SqlParameter("@DeviceMacAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@DeviceMacAddress"].Value = thirdPartySeting.DeviceMacAddress;

                    cmd.Parameters.Add(new SqlParameter("@ServerIP", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerIP"].Value = thirdPartySeting.ServerIP;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = thirdPartySeting.Port;

                    cmd.Parameters.Add(new SqlParameter("@ServerPort", SqlDbType.Int));
                    cmd.Parameters["@ServerPort"].Value = thirdPartySeting.ServerPort;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = thirdPartySeting.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = thirdPartySeting.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = thirdPartySeting.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = thirdPartySeting.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = thirdPartySeting.Name;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = thirdPartySeting.UserName;


                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            thirdPartySeting.Id = id;
                            var audit = new ThirdPartySettingAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, thirdPartySeting.UpdatedBy);
                            Reflection.CopyProperties(thirdPartySeting, audit);
                            ThirdPartySettingAudit.InsertorUpdateThirdPartySeting(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateThirdPartySetting", ex, dbConnection);
                    }
                }
            }
            return true;
        }


        private static ThirdPartySeting GetThirdPartySetingFromSqlReader(SqlDataReader reader,string dbCon)
        {
            var thirdPartySeting = new ThirdPartySeting();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                thirdPartySeting.Id = Convert.ToInt32(reader["Id"].ToString());

            if (columns.Contains("Type") && reader["Type"] != DBNull.Value)
            {
                var thirdpartySettingsType = ThirdPartySettingType.GetThirdPartySettingTypeById(Convert.ToInt32(reader["Type"].ToString()), dbCon);
                thirdPartySeting.VMSType = thirdpartySettingsType;
            }
            if (columns.Contains("IPAddress") && reader["IPAddress"] != DBNull.Value)
                thirdPartySeting.IPAddress = reader["IPAddress"].ToString();

            if (columns.Contains("ServerIP") && reader["ServerIP"] != DBNull.Value)
                thirdPartySeting.ServerIP = reader["ServerIP"].ToString();

            if (columns.Contains("Password") && reader["Password"] != DBNull.Value)
                thirdPartySeting.Password = Decrypt.DecryptData(reader["Password"].ToString(),true,dbCon);

            if (columns.Contains("DeviceMacAddress") && reader["DeviceMacAddress"] != DBNull.Value)
                thirdPartySeting.DeviceMacAddress = reader["DeviceMacAddress"].ToString();

            if (columns.Contains("Port") && reader["Port"] != DBNull.Value)
                thirdPartySeting.Port = Convert.ToInt32(reader["Port"].ToString());

            if (columns.Contains("ServerPort") && reader["ServerPort"] != DBNull.Value)
                thirdPartySeting.ServerPort = Convert.ToInt32(reader["ServerPort"].ToString());

            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                thirdPartySeting.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("UpdatedDate") && reader["UpdatedDate"] != DBNull.Value)
                thirdPartySeting.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                thirdPartySeting.CreatedBy = reader["CreatedBy"].ToString();

            if (columns.Contains("UpdatedBy") && reader["UpdatedBy"] != DBNull.Value)
                thirdPartySeting.UpdatedBy = reader["UpdatedBy"].ToString();

            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                thirdPartySeting.Name = reader["Name"].ToString();


            if (columns.Contains("UserName") && reader["UserName"] != DBNull.Value)
                thirdPartySeting.UserName = reader["UserName"].ToString();

            if (columns.Contains("PCName") && reader["PCName"] != DBNull.Value)
                thirdPartySeting.Name = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbCon);
          
            if (columns.Contains("IP") && reader["IP"] != DBNull.Value)
                thirdPartySeting.IPAddress = reader["IP"].ToString();

            return thirdPartySeting;
        }
        public static List<ThirdPartySeting> GetAllThirdPartySeting(string dbConnection)
        {
            var thirdPartySetings = new List<ThirdPartySeting>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllThirdPartSetting", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var thirdParty = GetThirdPartySetingFromSqlReader(reader,dbConnection);
                    thirdPartySetings.Add(thirdParty);
                }
                connection.Close();
                reader.Close();
            }
            return thirdPartySetings;
        }
        public static List<ThirdPartySeting> GetAllHeitelDevices(string dbConnection)
        {
            var thirdPartySetings = new List<ThirdPartySeting>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllHeitelDevices", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var thirdParty = GetThirdPartySetingFromSqlReader(reader,dbConnection);
                    thirdPartySetings.Add(thirdParty);
                }
                connection.Close();
                reader.Close();
            }
            return thirdPartySetings;
        }
        public static bool DeleteThirdPartSettingById(int Id, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteThirdPartSettingById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = Id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteThirdPartSettingById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static ThirdPartySeting GetThirdPartSettingById(int id, string dbConnection)
        {
            ThirdPartySeting thirdParty = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetThirdPartySettingById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    thirdParty = GetThirdPartySetingFromSqlReader(reader,dbConnection);
                    
                }
                connection.Close();
                reader.Close();
            }
            return thirdParty;
        }
    }
	
}
