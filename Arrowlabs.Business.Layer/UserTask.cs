﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public enum TaskStatus
    {
        // Swapped value of completed and accepted
        Pending = 0,
        InProgress = 1 ,
        Completed = 2, 
        Accepted = 3 , 
        Rejected = 4,
        RejectedSaved = 5,
        Cancelled = 6,
        OnRoute = 7
    }
    public enum TaskPiority
    {
        High = 0,
        Medium = 1,
        Low = 2,
        Severe = 3
    }
    public enum TaskAssigneeType
    {
        User = 0,
        Location = 1,
        Group = 2,
        Device = 3
    }
    public class UserTask : Object
    {
        public int AccountId { get; set; }
        public int ProjectId { get; set; }
        public int CustId { get; set; }
        public int Systype { get; set; }
         
                public string taskapplatitude { get; set; }
        
                public string taskapplongitude { get; set; }
         

        public int IncidentId { get; set; }
        public string AccountName { get; set; }
        public int Id { get; set; }
        [DisplayName("Task")]
        public string Name { get; set; }
        [DisplayName("Created By")]
        public string CreatedBy { get; set; }
        [DisplayName("Updated Date")]
        public DateTime? UpdatedDate { get; set; }
        [DisplayName("Created Date")]
        public DateTime? CreateDate { get; set; }
        [DisplayName("Manager")]
        public int ManagerId { get; set; }
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }
        public string UpdatedBy { get; set; }
        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }

        public int SubmittedBy { get; set; }

        public string completedTask { get; set; }

        public string Notes { get; set; }
        public int Status { get; set; }
        [DisplayName("Manager")]
        public string ManagerName { get; set; }
        public string prefixname { get; set; }
        public string Description { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int Priority { get; set; }
        public string StatusDescription { get; set; }
        public bool IsTaskTemplate { get; set; }
        public bool IsTaskFavorite { get; set; }
        public bool IsDeleted { get; set; }
        public int AssigneeType { get; set; }
        public int AssigneeId { get; set; }
        public int UserId { get; set; }

        public List<UserTask> ChildrenTaskNames { get; set; }

        public string AssigneeName { get; set; }
        public int TemplateCheckListId { get; set; }
        [DataMember]
        public string CheckListNotes { get; set; }

        [DataMember]
        public string RejectionNotes { get; set; }
        public string IncidentName { get; set; }
        public float StartLongitude { get; set; }
        public float StartLatitude { get; set; }
        public float EndLongitude { get; set; }
        public float EndLatitude { get; set; }

        public float OnRouteLongitude { get; set; } 
        public float OnRouteLatitude { get; set; }
        public DateTime? ActualOnRouteDate { get; set; }
        public int JobStatus { get; set; }

        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public string imageURL { get; set; }
        public int TaskTypeId { get; set; }

        public string TaskTypeName { get; set; } 
        public int TaskLinkId { get; set; }
        public string ClientName { get; set; }
        public string SysTypeName { get; set; }
        public string ProjectName { get; set; }
        public string NewCustomerTaskId { get; set; }
        public string ContractName { get; set; }
        public int ContractId { get; set; }

        public bool IsSignature { get; set; }

        public bool FollowUp { get; set; }

        public int RecurringParentId { get; set; }
        public bool IsRecurring { get; set; }
        public string ServerNotes { get; set; }
        public string NewAssigneeName { get; set; }

        public string CustomerUName { get; set; }
        public string ACustomerUName { get; set; }

        public string ParentTaskName { get; set; }

        public int CustomerId { get; set; }
        public string CustomerFullName { get; set; }
        public string ACustomerFullName { get; set; }

        [DataMember]
        public List<TaskCheckList> TaskCheckListItems { get; set; }

        public int SiteId { get; set; }

        public static List<string> GetStatusList()
        {
            return Enum.GetNames(typeof(TaskStatus)).ToList();
        }

        public static List<string> GetPriorityList()
        {
            return Enum.GetNames(typeof(TaskPiority)).ToList();
        }

        public static List<string> GetPriorityImagesList()
        {
            return new List<string>() { 
                "<img height='8px' style='padding-right:5px;' width='8px' src='Images/red_circle.png'/>", 
                "<img height='8px' style='padding-right:5px;' width='8px' src='Images/orange_circle.png'/>",
                "<img height='8px' style='padding-right:5px;' width='8px' src='Images/green_circle.png'/>" };
        }

        public static List<string> GetAssigneeTypeList()
        {
            return Enum.GetNames(typeof(TaskAssigneeType)).ToList();
        }

        public static UserTask GetUserTaskFromSqlReader(SqlDataReader resultSet)
        {
            var userTask = new UserTask();
            var columns = Enumerable.Range(0, resultSet.FieldCount).Select(resultSet.GetName).ToList();
            if (resultSet["Id"] != DBNull.Value)
                userTask.Id = Convert.ToInt32(resultSet["Id"].ToString());
            if (resultSet["Name"] != DBNull.Value)
                userTask.Name = resultSet["Name"].ToString();
            if (resultSet["CreateDate"] != DBNull.Value)
                userTask.CreateDate = Convert.ToDateTime(resultSet["CreateDate"].ToString());
            if (resultSet["Updateddate"] != DBNull.Value)
                userTask.UpdatedDate = Convert.ToDateTime(resultSet["Updateddate"].ToString());
            if (resultSet["CreatedBy"] != DBNull.Value)
                userTask.CreatedBy = resultSet["CreatedBy"].ToString();
            if (resultSet["ManagerId"] != DBNull.Value)
                userTask.ManagerId = Convert.ToInt32(resultSet["ManagerId"].ToString());
            if (resultSet["StartDate"] != DBNull.Value)
                userTask.StartDate = Convert.ToDateTime(resultSet["StartDate"].ToString());
            if (resultSet["EndDate"] != DBNull.Value)
                userTask.EndDate = Convert.ToDateTime(resultSet["EndDate"].ToString());
            if (resultSet["Notes"] != DBNull.Value)
                userTask.Notes = resultSet["Notes"].ToString();
            if (resultSet["Description"] != DBNull.Value)
                userTask.Description = resultSet["Description"].ToString();
            if (resultSet["Status"] != DBNull.Value)
                userTask.Status = Convert.ToInt32(resultSet["Status"].ToString());
            if (resultSet["AssigneeType"] != DBNull.Value)
                userTask.AssigneeType = Convert.ToInt32(resultSet["AssigneeType"].ToString());
            if (resultSet["AssigneeId"] != DBNull.Value)
                userTask.AssigneeId = Convert.ToInt32(resultSet["AssigneeId"].ToString());
            if (resultSet["Longitude"] != DBNull.Value)
                userTask.Longitude = Convert.ToDouble(resultSet["Longitude"].ToString());
            if (resultSet["Latitude"] != DBNull.Value)
                userTask.Latitude = Convert.ToDouble(resultSet["Latitude"].ToString());
            if (resultSet["Priority"] != DBNull.Value)
                userTask.Priority = Convert.ToInt32(resultSet["Priority"].ToString());
            if (resultSet["CheckListNotes"] != DBNull.Value)
                userTask.CheckListNotes = resultSet["CheckListNotes"].ToString();
            if (resultSet["TemplateCheckListId"] != DBNull.Value)
                userTask.TemplateCheckListId = (!String.IsNullOrEmpty(resultSet["TemplateCheckListId"].ToString())) ? Convert.ToInt32(resultSet["TemplateCheckListId"].ToString()) : 0;
            if (resultSet["IsTaskTemplate"] != DBNull.Value)
                userTask.IsTaskTemplate = Convert.ToBoolean(resultSet["IsTaskTemplate"].ToString() == "1" ? "true" : "false");
            if (resultSet["IsDeleted"] != DBNull.Value)
                userTask.IsDeleted = Convert.ToBoolean(resultSet["IsDeleted"].ToString() == "1" ? "true" : "false");
            if (resultSet["SubmittedBy"] != DBNull.Value)
                userTask.SubmittedBy = (!String.IsNullOrEmpty(resultSet["SubmittedBy"].ToString())) ? Convert.ToInt32(resultSet["SubmittedBy"].ToString()) : 0;
            if (resultSet["StartLatitude"] != DBNull.Value)
                userTask.StartLatitude = (!String.IsNullOrEmpty(resultSet["StartLatitude"].ToString())) ? Convert.ToSingle(resultSet["StartLatitude"].ToString()) : 0f;
            if (resultSet["StartLongitude"] != DBNull.Value)
                userTask.StartLongitude = (!String.IsNullOrEmpty(resultSet["StartLongitude"].ToString())) ? Convert.ToSingle(resultSet["StartLongitude"].ToString()) : 0f;
            if (resultSet["EndLatitude"] != DBNull.Value)
                userTask.EndLatitude = (!String.IsNullOrEmpty(resultSet["EndLatitude"].ToString())) ? Convert.ToSingle(resultSet["EndLatitude"].ToString()) : 0f;
            if (resultSet["EndLongitude"] != DBNull.Value)
                userTask.EndLongitude = (!String.IsNullOrEmpty(resultSet["EndLongitude"].ToString())) ? Convert.ToSingle(resultSet["EndLongitude"].ToString()) : 0f;
            if (resultSet["ActualStartDate"] != DBNull.Value)
                userTask.ActualStartDate = Convert.ToDateTime(resultSet["ActualStartDate"].ToString());
            if (resultSet["ActualEndDate"] != DBNull.Value)
                userTask.ActualEndDate = Convert.ToDateTime(resultSet["ActualEndDate"].ToString());
            if (resultSet["RejectionNotes"] != DBNull.Value)
                userTask.RejectionNotes = resultSet["RejectionNotes"].ToString();
             
            userTask.StatusDescription = GetStatusList()[userTask.Status];
            if (columns.Contains( "ManagerName") && resultSet["ManagerName"] != DBNull.Value)
                userTask.ManagerName = resultSet["ManagerName"].ToString();
            if (columns.Contains( "AssigneeName") && resultSet["AssigneeName"] != DBNull.Value)
                userTask.AssigneeName = resultSet["AssigneeName"].ToString();

            if (columns.Contains("ParentTaskName") && resultSet["ParentTaskName"] != DBNull.Value)
                userTask.ParentTaskName = resultSet["ParentTaskName"].ToString();
            

            if (columns.Contains( "CustomerUName") && resultSet["CustomerUName"] != DBNull.Value)
                userTask.CustomerUName = resultSet["CustomerUName"].ToString();
            if (columns.Contains( "ACustomerUName") && resultSet["ACustomerUName"] != DBNull.Value)
            {
                userTask.ACustomerUName = resultSet["ACustomerUName"].ToString();

                if (userTask.AssigneeType == (int)TaskAssigneeType.Group)
                {
                    userTask.ACustomerUName = userTask.AssigneeName;
                }
                else
                {
                    if (columns.Contains("ACustomerFullName") && resultSet["ACustomerFullName"] != DBNull.Value)
                        userTask.ACustomerFullName = resultSet["ACustomerFullName"].ToString();
                }
            }

            if (columns.Contains("CustomerFullName") && resultSet["CustomerFullName"] != DBNull.Value)
                userTask.CustomerFullName = resultSet["CustomerFullName"].ToString();
             
            if (columns.Contains( "SiteId") && resultSet["SiteId"] != DBNull.Value)
                userTask.SiteId = Convert.ToInt32(resultSet["SiteId"]);
             
            
            if (columns.Contains( "CustomerId") && resultSet["CustomerId"] != DBNull.Value)
                userTask.CustomerId = Convert.ToInt32(resultSet["CustomerId"]);

            if (columns.Contains( "Projectid") && resultSet["Projectid"] != DBNull.Value)
                userTask.ProjectId = Convert.ToInt32(resultSet["Projectid"]);

            if (columns.Contains( "CustId") && resultSet["CustId"] != DBNull.Value)
                userTask.CustId = Convert.ToInt32(resultSet["CustId"]);

            if (columns.Contains( "ContractId") && resultSet["ContractId"] != DBNull.Value)
                userTask.ContractId = Convert.ToInt32(resultSet["ContractId"]);



            if (columns.Contains( "AccountId"))
            {
                if (resultSet["AccountId"] != DBNull.Value)
                    userTask.AccountId = Convert.ToInt32(resultSet["AccountId"]);

            }
            if (columns.Contains( "TaskTypeId") && resultSet["TaskTypeId"] != DBNull.Value)
                userTask.TaskTypeId = Convert.ToInt32(resultSet["TaskTypeId"]);

            if (columns.Contains( "TaskLinkId") && resultSet["TaskLinkId"] != DBNull.Value)
                userTask.TaskLinkId = Convert.ToInt32(resultSet["TaskLinkId"]);


            if (string.IsNullOrEmpty(userTask.AssigneeName))
            {
                if (columns.Contains( "NewAssigneeName"))
                {
                    if (resultSet["NewAssigneeName"] != DBNull.Value)
                        userTask.AssigneeName = resultSet["NewAssigneeName"].ToString();
                }
            }

            if (columns.Contains( "TaskTypeName"))
            {
                if (resultSet["TaskTypeName"] != DBNull.Value)
                    userTask.TaskTypeName = resultSet["TaskTypeName"].ToString();
            }

            if (string.IsNullOrEmpty(userTask.TaskTypeName))
                userTask.TaskTypeName = "None";

            if (columns.Contains( "AccountName"))
            {
                if (resultSet["AccountName"] != DBNull.Value)
                    userTask.AccountName = resultSet["AccountName"].ToString();
            }

            if (columns.Contains( "RecurringParentId"))
            {
                if (resultSet["RecurringParentId"] != DBNull.Value)
                    userTask.RecurringParentId = Convert.ToInt32(resultSet["RecurringParentId"]);
            }
            if (columns.Contains( "IsRecurring"))
            {
                if (resultSet["IsRecurring"] != DBNull.Value)
                    userTask.IsRecurring = Convert.ToBoolean(resultSet["IsRecurring"].ToString() == "1" ? "true" : "false");
            }
            if (columns.Contains( "IncidentId"))
            {
                if (resultSet["IncidentId"] != DBNull.Value)
                    userTask.IncidentId = Convert.ToInt32(resultSet["IncidentId"]);
            }

            if (columns.Contains( "IsSignature") && resultSet["IsSignature"] != DBNull.Value)
                userTask.IsSignature = Convert.ToBoolean(resultSet["IsSignature"].ToString());

            if (columns.Contains( "FollowUp") && resultSet["FollowUp"] != DBNull.Value)
                userTask.FollowUp = Convert.ToBoolean(resultSet["FollowUp"].ToString());


            if (columns.Contains( "ClientName") && resultSet["ClientName"] != DBNull.Value)
                userTask.ClientName = resultSet["ClientName"].ToString();

            if (columns.Contains( "SysTypeName") && resultSet["SysTypeName"] != DBNull.Value)
                userTask.SysTypeName = resultSet["SysTypeName"].ToString();

            if (columns.Contains( "ProjectName") && resultSet["ProjectName"] != DBNull.Value)
                userTask.ProjectName = resultSet["ProjectName"].ToString();

            if (columns.Contains( "ContractName") && resultSet["ContractName"] != DBNull.Value)
                userTask.ContractName = resultSet["ContractName"].ToString();


            if (columns.Contains( "CustomerTaskId") && resultSet["CustomerTaskId"] != DBNull.Value)
                userTask.NewCustomerTaskId = resultSet["CustomerTaskId"].ToString();

            if (columns.Contains("ActualOnRouteDate") && resultSet["ActualOnRouteDate"] != DBNull.Value)
                userTask.ActualOnRouteDate = Convert.ToDateTime(resultSet["ActualOnRouteDate"].ToString());
            if (columns.Contains("OnRouteLatitude") && resultSet["OnRouteLatitude"] != DBNull.Value)
                userTask.OnRouteLatitude = (!String.IsNullOrEmpty(resultSet["OnRouteLatitude"].ToString())) ? Convert.ToSingle(resultSet["OnRouteLatitude"].ToString()) : 0f;
            if (columns.Contains("OnRouteLongitude") && resultSet["OnRouteLongitude"] != DBNull.Value)
                userTask.OnRouteLongitude = (!String.IsNullOrEmpty(resultSet["OnRouteLongitude"].ToString())) ? Convert.ToSingle(resultSet["OnRouteLongitude"].ToString()) : 0f;
            if (columns.Contains("JobStatus") && resultSet["JobStatus"] != DBNull.Value)
                userTask.JobStatus = Convert.ToInt32(resultSet["JobStatus"]); 

            if (userTask.Status == (int)TaskStatus.InProgress && userTask.JobStatus == (int)TaskJobAction.Pause)
            {
                userTask.StatusDescription = "Pause";
            }

            if (userTask.StatusDescription == "Pending")
            {
                userTask.imageURL = @"../Images/yellow1.png";
            }
            else if (userTask.StatusDescription == "InProgress" || userTask.StatusDescription == "Pause")
            {
                userTask.imageURL = @"../Images/orange.png";
            }
            else if (userTask.StatusDescription == "Completed")
            {
                userTask.imageURL = @"../Images/green.png";
            }
            else if (userTask.StatusDescription == "Accepted")
            {
                userTask.imageURL = @"../Images/blue.png";
            }
            else if (userTask.StatusDescription == "Rejected")
            {
                userTask.imageURL = @"../Images/red.png";
            }
            else
            {
                userTask.imageURL = @"../Images/red.png";
            }

            if (string.IsNullOrEmpty(userTask.TaskTypeName))
                userTask.TaskTypeName = "None";

            userTask.prefixname = "-    " + userTask.Name;
              
            return userTask;
        }

        public static UserTask GetUserTaskTemplateFromSqlReader(SqlDataReader resultSet)
        {
            var userTaskTemplate = new UserTask();
            var columns = Enumerable.Range(0, resultSet.FieldCount).Select(resultSet.GetName).ToList();
            userTaskTemplate.Id = Convert.ToInt32(resultSet["Id"].ToString());
            userTaskTemplate.Name = resultSet["Name"].ToString();
            userTaskTemplate.CreateDate = Convert.ToDateTime(resultSet["CreateDate"].ToString());
            userTaskTemplate.UpdatedDate = Convert.ToDateTime(resultSet["Updateddate"].ToString());
            userTaskTemplate.CreatedBy = resultSet["CreatedBy"].ToString();
            userTaskTemplate.StartDate = Convert.ToDateTime(resultSet["StartDate"].ToString());
            userTaskTemplate.EndDate = Convert.ToDateTime(resultSet["EndDate"].ToString());
            userTaskTemplate.Description = resultSet["Description"].ToString();
            userTaskTemplate.Status = Convert.ToInt32(resultSet["Status"].ToString());
            userTaskTemplate.Longitude = Convert.ToDouble(resultSet["Longitude"].ToString());
            userTaskTemplate.Latitude = Convert.ToDouble(resultSet["Latitude"].ToString());
            userTaskTemplate.Priority = Convert.ToInt32(resultSet["Priority"].ToString());

            userTaskTemplate.prefixname = "-  " + userTaskTemplate.Name;

            var obje = resultSet["TemplateCheckListId"];

            userTaskTemplate.TemplateCheckListId = (!String.IsNullOrEmpty(resultSet["TemplateCheckListId"].ToString())) ? Convert.ToInt32(resultSet["TemplateCheckListId"].ToString()) : 0;
            userTaskTemplate.StatusDescription = GetStatusList()[userTaskTemplate.Status];
            userTaskTemplate.IsTaskTemplate = Convert.ToBoolean(resultSet["IsTaskTemplate"].ToString() == "1" ? "true" : "false");
            userTaskTemplate.IsDeleted = Convert.ToBoolean(resultSet["IsDeleted"].ToString() == "1" ? "true" : "false");
            userTaskTemplate.IsTaskFavorite = Convert.ToBoolean(resultSet["IsTaskFavorite"].ToString() == "1" ? "true" : "false");

            if (columns.Contains( "AccountId"))
                userTaskTemplate.AccountId = Convert.ToInt32(resultSet["AccountId"]);

            if (columns.Contains( "TaskTypeId") && resultSet["TaskTypeId"] != DBNull.Value)
                userTaskTemplate.TaskTypeId = Convert.ToInt32(resultSet["TaskTypeId"]);

            if (columns.Contains( "AccountName"))
                userTaskTemplate.AccountName = resultSet["AccountName"].ToString();

            if (columns.Contains( "CustomerUName") && resultSet["CustomerUName"] != DBNull.Value)
                userTaskTemplate.CustomerUName = resultSet["CustomerUName"].ToString();

            if (columns.Contains( "SiteId") && resultSet["SiteId"] != DBNull.Value)
                userTaskTemplate.SiteId = Convert.ToInt32(resultSet["SiteId"]);

            if (columns.Contains( "CustomerId") && resultSet["CustomerId"] != DBNull.Value)
                userTaskTemplate.CustomerId = Convert.ToInt32(resultSet["CustomerId"]);

            if (columns.Contains( "ACustomerUName") && resultSet["ACustomerUName"] != DBNull.Value)
                userTaskTemplate.ACustomerUName = resultSet["ACustomerUName"].ToString();

            return userTaskTemplate;
        }

        public static string GetStatusById(int statusId, int priority)
        {
            var statuses = GetStatusList();
            var priorities = GetPriorityImagesList();
            switch (statusId)
            {
                //<a href="@Url.Action("Details", null ,new { id=item.Id })" data-role="button" data-icon="grid" data-mini="true">Details</a>
                case 0:
                    return string.Format("<div align='left'>" + priorities[priority] + " {0}</div>", statuses[0]);
                //return string.Format("<div style='color: #FFFF00'><img src='Images/1413462457_Circle_Yellow.png'/> {0}</div> ", statuses[0]);
                case 1:
                    return string.Format("<div align='left'>" + priorities[priority] + " {0}</div>", Helper.AddSpacesToSentence(statuses[1]));
                //return string.Format("<div style='color: #FFA500'><img src='Images/1413463880_Circle_Orange.png'/> {0}</div> ", statuses[1]);
                case 2:
                    return string.Format("<div align='left'>" + priorities[priority] + " {0}</div>", Helper.AddSpacesToSentence(statuses[2]));
                //   return string.Format("<button data-role='button' data-mini='true' style='color: #00ff00'><div align='left'>" + priorities[priority] + " {0}</div></button> ", statuses[2]);
                // return string.Format("<div style='color: #00ff00'><img src='Images/1413463946_Circle_Green.png'/> {0}</div>", statuses[2]);
                case 3:
                    return string.Format("<div style='color: #007f00'><img src='Images/1413463946_Circle_Green.png'/> {0}</div>", statuses[3]);
                case 4:
                    return string.Format("<div style='color: #FF0000'><img src='Images/1413463946_Circle_Green.png'/> {0}</div>", statuses[4]);
                default:
                    return "<b style='color: #FFFFFF'>Unknown</b>";
            }
        }

        public static List<UserTask> GetTop5CompleteTasks(string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTopFiveTaskCompletedUsers", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = new UserTask();
                        if (resultSet["Username"] != DBNull.Value)
                            userTask.AssigneeName = resultSet["Username"].ToString();
                        if (resultSet["CompletedTasks"] != DBNull.Value)
                            userTask.completedTask = resultSet["CompletedTasks"].ToString();
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetTop5CompleteTasks", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetTop5CompleteTasksByManagerIdAndDate(int id, DateTime startDate, DateTime endDate, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetTop5CompleteTasksByManagerIdAndDate", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));
                    sqlCommand.Parameters["@id"].Value = id;
                    sqlCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@startDate"].Value = startDate;
                    sqlCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@endDate"].Value = endDate;
                    sqlCommand.CommandText = "GetTop5CompleteTasksByManagerIdAndDate";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = new UserTask();
                        if (resultSet["Username"] != DBNull.Value)
                            userTask.AssigneeName = resultSet["Username"].ToString();
                        if (resultSet["CompletedTasks"] != DBNull.Value)
                            userTask.completedTask = resultSet["CompletedTasks"].ToString();
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetTop5CompleteTasksByManagerIdAndDate", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetTop5CompleteTasksByDate(DateTime startDate, DateTime endDate, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetTop5CompleteTasksByDate", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@startDate"].Value = startDate;
                    sqlCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@endDate"].Value = endDate;
                    sqlCommand.CommandText = "GetTop5CompleteTasksByDate";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = new UserTask();
                        if (resultSet["Username"] != DBNull.Value)
                            userTask.AssigneeName = resultSet["Username"].ToString();
                        if (resultSet["CompletedTasks"] != DBNull.Value)
                            userTask.completedTask = resultSet["CompletedTasks"].ToString();
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetTop5CompleteTasksByDate", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetAllTask(string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTask", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTask", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskBySiteId(int siteid,string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskBySiteId", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByLevel5(int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByLevel5", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskBySiteId", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByCId(int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByCId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByCId", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByCustId(int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByCustId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByCustId", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetAllTaskByPlanId(int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByPlanId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByPlanId", ex, dbConnection);
            }
            return userTasks;
        }
        public static UserTask GetTaskById(int Id, string dbConnection)
        {
            UserTask userTask = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetTaskById", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandText = "GetTaskById";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetTaskById", ex, dbConnection);
            }
            return userTask;
        }

        public static List<UserTask> GetAllTasksByIncidentId(int incidentId, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("@IncidentId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                    sqlCommand.Parameters["@IncidentId"].Value = incidentId;
                    sqlCommand.CommandText = "GetAllTasksByIncidentId";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTasksByIncidentId", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetTaskByName(string name, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetTaskByName", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@name"].Value = name;
                    sqlCommand.CommandText = "GetTaskByName";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetTaskByName", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetAllTaskByDate(DateTime dt, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByDate", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.CommandText = "GetAllTaskByDate";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByDate", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetAllTaskByDateExcludingRecurringTaskParent(DateTime dt, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByDateExcludingRecurringTaskParent", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.CommandText = "GetAllTaskByDateExcludingRecurringTaskParent";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByDateExcludingRecurringTaskParent", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(DateTime dt, int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByDateExcludingRecurringTaskParentAndSiteId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteid;
                    sqlCommand.CommandText = "GetAllTaskByDateExcludingRecurringTaskParentAndSiteId";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByDateExcludingRecurringTaskParent", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(DateTime dt, int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByDateExcludingRecurringTaskParentAndLevel5", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = siteid;
                    sqlCommand.CommandText = "GetAllTaskByDateExcludingRecurringTaskParentAndLevel5";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByDateExcludingRecurringTaskParent", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByDateExcludingRecurringTaskParentAndCId(DateTime dt, int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByDateExcludingRecurringTaskParentAndCId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteid;
                    sqlCommand.CommandText = "GetAllTaskByDateExcludingRecurringTaskParentAndCId";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByDateExcludingRecurringTaskParentAndCId", ex, dbConnection);
            }
            return userTasks;
        }
        public static int InsertorUpdateTask(UserTask userTask, string dbConnection, string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = userTask.Id;
            try
            {
                var action = (userTask.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                if (userTask != null)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateTask", connection);

                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = userTask.Id;

                        cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                        cmd.Parameters["@Name"].Value = userTask.Name;

                        cmd.Parameters.Add(new SqlParameter("@ProjectId", SqlDbType.Int));
                        cmd.Parameters["@ProjectId"].Value = userTask.ProjectId;

                        cmd.Parameters.Add(new SqlParameter("@ContractId", SqlDbType.Int));
                        cmd.Parameters["@ContractId"].Value = userTask.ContractId;

                        cmd.Parameters.Add(new SqlParameter("@CustId", SqlDbType.Int));
                        cmd.Parameters["@CustId"].Value = userTask.CustId;
                        cmd.Parameters.Add(new SqlParameter("@Systype", SqlDbType.Int));
                        cmd.Parameters["@Systype"].Value = userTask.Systype;

                        cmd.Parameters.Add(new SqlParameter("@CreateDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreateDate"].Value = userTask.CreateDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = userTask.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = userTask.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                        cmd.Parameters["@ManagerId"].Value = userTask.ManagerId;

                        cmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                        cmd.Parameters["@StartDate"].Value = userTask.StartDate;

                        cmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                        cmd.Parameters["@EndDate"].Value = userTask.EndDate;

                        cmd.Parameters.Add(new SqlParameter("@Notes", SqlDbType.NVarChar));
                        cmd.Parameters["@Notes"].Value = userTask.Notes;

                        cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                        cmd.Parameters["@Description"].Value = userTask.Description;

                        cmd.Parameters.Add(new SqlParameter("@CheckListNotes", SqlDbType.NVarChar));
                        cmd.Parameters["@CheckListNotes"].Value = userTask.CheckListNotes;

                        cmd.Parameters.Add(new SqlParameter("@TemplateCheckListId", SqlDbType.Int));
                        cmd.Parameters["@TemplateCheckListId"].Value = userTask.TemplateCheckListId;

                        cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                        cmd.Parameters["@Status"].Value = userTask.Status;

                        cmd.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                        cmd.Parameters["@AssigneeType"].Value = userTask.AssigneeType;

                        cmd.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                        cmd.Parameters["@AssigneeId"].Value = userTask.AssigneeId;

                        cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                        cmd.Parameters["@Longitude"].Value = userTask.Longitude;

                        cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                        cmd.Parameters["@Latitude"].Value = userTask.Latitude;

                        if (userTask.Id == 0 && !userTask.IsTaskTemplate)
                        {
                            userTask.NewCustomerTaskId = CustomerTaskId(userTask.CustomerId, userTask.ContractId, dbConnection,auditDbConnection);
                            cmd.Parameters.Add(new SqlParameter("@CustomerTaskId", SqlDbType.NVarChar));
                            cmd.Parameters["@CustomerTaskId"].Value = userTask.NewCustomerTaskId;
                        }

                        cmd.Parameters.Add(new SqlParameter("@Priority", SqlDbType.Int));
                        cmd.Parameters["@Priority"].Value = userTask.Priority;

                        cmd.Parameters.Add(new SqlParameter("@IsTaskTemplate", SqlDbType.TinyInt));
                        cmd.Parameters["@IsTaskTemplate"].Value = userTask.IsTaskTemplate;

                        cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.TinyInt));
                        cmd.Parameters["@IsDeleted"].Value = userTask.IsDeleted;

                        cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                        cmd.Parameters["@IncidentId"].Value = userTask.IncidentId;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = userTask.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@TaskTypeId", SqlDbType.Int));
                        cmd.Parameters["@TaskTypeId"].Value = userTask.TaskTypeId;

                        cmd.Parameters.Add(new SqlParameter("@NewAssigneeName", SqlDbType.NVarChar));
                        cmd.Parameters["@NewAssigneeName"].Value = userTask.AssigneeName;

                        if (userTask.IsTaskFavorite != null)
                        {
                            cmd.Parameters.Add(new SqlParameter("@IsTaskFavorite", SqlDbType.TinyInt));
                            cmd.Parameters["@IsTaskFavorite"].Value = userTask.IsTaskFavorite;
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@IsTaskFavorite", SqlDbType.TinyInt));
                            cmd.Parameters["@IsTaskFavorite"].Value = false;
                        }
                        cmd.Parameters.Add(new SqlParameter("@FollowUp", SqlDbType.TinyInt));
                        cmd.Parameters["@FollowUp"].Value = userTask.FollowUp;


                        cmd.Parameters.Add(new SqlParameter("@StartLongitude", SqlDbType.Float));
                        cmd.Parameters["@StartLongitude"].Value = userTask.StartLongitude;

                        cmd.Parameters.Add(new SqlParameter("@StartLatitude", SqlDbType.Float));
                        cmd.Parameters["@StartLatitude"].Value = userTask.StartLatitude;

                        cmd.Parameters.Add(new SqlParameter("@EndLongitude", SqlDbType.Float));
                        cmd.Parameters["@EndLongitude"].Value = userTask.EndLongitude;

                        cmd.Parameters.Add(new SqlParameter("@EndLatitude", SqlDbType.Float));
                        cmd.Parameters["@EndLatitude"].Value = userTask.EndLatitude;

                        cmd.Parameters.Add(new SqlParameter("@RejectionNotes", SqlDbType.NVarChar));
                        cmd.Parameters["@RejectionNotes"].Value = userTask.RejectionNotes;

                        cmd.Parameters.Add(new SqlParameter("@ActualEndDate", SqlDbType.DateTime));
                        cmd.Parameters["@ActualEndDate"].Value = userTask.ActualEndDate;

                        cmd.Parameters.Add(new SqlParameter("@ActualStartDate", SqlDbType.DateTime));
                        cmd.Parameters["@ActualStartDate"].Value = userTask.ActualStartDate;

                        cmd.Parameters.Add(new SqlParameter("@ActualOnRouteDate", SqlDbType.DateTime));
                        cmd.Parameters["@ActualOnRouteDate"].Value = userTask.ActualOnRouteDate;

                        cmd.Parameters.Add(new SqlParameter("@OnRouteLatitude", SqlDbType.Float));
                        cmd.Parameters["@OnRouteLatitude"].Value = userTask.OnRouteLatitude;

                        cmd.Parameters.Add(new SqlParameter("@OnRouteLongitude", SqlDbType.Float));
                        cmd.Parameters["@OnRouteLongitude"].Value = userTask.OnRouteLongitude;

                        cmd.Parameters.Add(new SqlParameter("@SubmittedBy", SqlDbType.Int));
                        cmd.Parameters["@SubmittedBy"].Value = userTask.SubmittedBy;

                        if (!string.IsNullOrEmpty(userTask.AccountName))
                        {
                            cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                            cmd.Parameters["@AccountName"].Value = userTask.AccountName;
                        }

                        cmd.Parameters.Add(new SqlParameter("@TaskLinkId", SqlDbType.Int));
                        cmd.Parameters["@TaskLinkId"].Value = userTask.TaskLinkId;

                        cmd.Parameters.Add(new SqlParameter("@RecurringParentId", SqlDbType.Int));
                        cmd.Parameters["@RecurringParentId"].Value = userTask.RecurringParentId;

                        cmd.Parameters.Add(new SqlParameter("@IsRecurring", SqlDbType.TinyInt));
                        cmd.Parameters["@IsRecurring"].Value = userTask.IsRecurring; 

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = userTask.CustomerId; 
                         
                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.CommandText = "InsertOrUpdateTask";

                        cmd.ExecuteNonQuery();

                        if (id == 0)
                            id = (int)returnParameter.Value;

                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {

                                userTask.Id = id;
                                var audit = new Arrowlabs.Mims.Audit.Models.TaskAudit();
                                audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, userTask.UpdatedBy);
                                Reflection.CopyProperties(userTask, audit);
                                Arrowlabs.Mims.Audit.Models.TaskAudit.InsertTaskAudit(audit, auditDbConnection);
                            }
                        }
                        catch (Exception ex)
                        {
                            MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateTask", ex, dbConnection);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "InsertorUpdateTask", ex, dbConnection);
            }
            return id;
        }

        public static bool SaveUserTask(UserTask task, UserTaskAttachment taskImage, List<UserTaskAttachment> taskAttachments,
            List<TaskCheckList> taskCheckListItems, string dbConnection,string dbconAudit)
        {
            try
            {
                InsertorUpdateTask(task, dbConnection,dbconAudit,true);

                foreach (var taskCheckListItem in taskCheckListItems)
                {
                    TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckListItem, dbConnection,dbconAudit,true);
                    if (taskCheckListItem.ChildCheckList != null && taskCheckListItem.ChildCheckList.Count > 0)
                    {
                        foreach (var taskCheckListItemChild in taskCheckListItem.ChildCheckList)
                        {
                            TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckListItemChild, dbConnection,dbconAudit,true);
                        }
                    }
                }

                if (taskImage != null && !string.IsNullOrEmpty(taskImage.DocumentPath))
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateTaskAttachment", connection);

                        cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                        cmd.Parameters["@TaskId"].Value = task.Id;

                        cmd.Parameters.Add(new SqlParameter("@DocumentType", SqlDbType.NVarChar));
                        cmd.Parameters["@DocumentType"].Value = taskImage.DocumentType;

                        cmd.Parameters.Add(new SqlParameter("@DocumentName", SqlDbType.NVarChar));
                        cmd.Parameters["@DocumentName"].Value = taskImage.DocumentName;

                        cmd.Parameters.Add(new SqlParameter("@DocumentPath", SqlDbType.NVarChar));
                        cmd.Parameters["@DocumentPath"].Value = taskImage.DocumentPath;

                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreatedDate"].Value = taskImage.CreatedDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = taskImage.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = taskImage.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@ImageNote", SqlDbType.NVarChar));
                        cmd.Parameters["@ImageNote"].Value = taskImage.ImageNote;

                        cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                        cmd.Parameters["@IsDeleted"].Value = taskImage.IsDeleted;

                        cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                        cmd.Parameters["@UserId"].Value = taskImage.UserId;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = taskImage.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = taskImage.CustomerId;
                         

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.CommandText = "InsertOrUpdateTaskAttachment";

                        cmd.ExecuteNonQuery();
                    }
                }
                if (taskAttachments != null)
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();

                        foreach (var attachment in taskAttachments)
                        {
                            InsertOrUpdateTaskAttachment(attachment, dbConnection,dbconAudit,true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "SaveUserTask", ex, dbConnection);
            }
            return true;
        }

        public static int InsertOrUpdateTaskAttachment(UserTaskAttachment attachment, string dbConnection)
        {
            int id = 0;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("InsertOrUpdateTaskAttachment", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = attachment.Id;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = attachment.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@DocumentType", SqlDbType.NVarChar));
                    cmd.Parameters["@DocumentType"].Value = attachment.DocumentType;

                    cmd.Parameters.Add(new SqlParameter("@DocumentName", SqlDbType.NVarChar));
                    cmd.Parameters["@DocumentName"].Value = attachment.DocumentName;

                    cmd.Parameters.Add(new SqlParameter("@DocumentPath", SqlDbType.NVarChar));
                    cmd.Parameters["@DocumentPath"].Value = attachment.DocumentPath;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = attachment.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));

                    if (attachment.UpdatedDate == null)
                        cmd.Parameters["@UpdatedDate"].Value = DateTime.Now;
                    else
                        cmd.Parameters["@UpdatedDate"].Value = attachment.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = attachment.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ImageNote", SqlDbType.NVarChar));
                    cmd.Parameters["@ImageNote"].Value = attachment.ImageNote;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = attachment.UserId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = attachment.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = attachment.CustomerId;
                    /*
                                    cmd.Parameters.Add(new SqlParameter("@IsInProgress", SqlDbType.Bit));
                                    cmd.Parameters["@IsInProgress"].Value = attachment.IsInProgress;
                    */
                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = attachment.IsDeleted;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "InsertOrUpdateTaskAttachment", ex, dbConnection);
            }
            return id;
        }

        public static int InsertOrUpdateTaskAttachment(UserTaskAttachment attachment, string dbConnection,
      string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("InsertOrUpdateTaskAttachment", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = attachment.Id;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = attachment.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@DocumentType", SqlDbType.NVarChar));
                    cmd.Parameters["@DocumentType"].Value = attachment.DocumentType;

                    cmd.Parameters.Add(new SqlParameter("@DocumentName", SqlDbType.NVarChar));
                    cmd.Parameters["@DocumentName"].Value = attachment.DocumentName;

                    cmd.Parameters.Add(new SqlParameter("@DocumentPath", SqlDbType.NVarChar));
                    cmd.Parameters["@DocumentPath"].Value = attachment.DocumentPath;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = attachment.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));

                    if (attachment.UpdatedDate == null)
                        cmd.Parameters["@UpdatedDate"].Value = DateTime.Now;
                    else
                        cmd.Parameters["@UpdatedDate"].Value = attachment.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = attachment.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ImageNote", SqlDbType.NVarChar));
                    cmd.Parameters["@ImageNote"].Value = attachment.ImageNote;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = attachment.UserId;
                    /*
                                    cmd.Parameters.Add(new SqlParameter("@IsInProgress", SqlDbType.Bit));
                                    cmd.Parameters["@IsInProgress"].Value = attachment.IsInProgress;
                    */
                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = attachment.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@ChecklistId", SqlDbType.Int));
                    cmd.Parameters["@ChecklistId"].Value = attachment.ChecklistId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = attachment.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = attachment.CustomerId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            attachment.Id = id;
                            var audit = new TaskAttachmentAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.Create, attachment.UpdatedBy);
                            Reflection.CopyProperties(attachment, audit);
                            TaskAttachmentAudit.InsertTaskAttachmentAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateTaskAttachment", ex, dbConnection);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "InsertOrUpdateTaskAttachment", ex, dbConnection);
            }
            return id;
        }

        public static bool DeleteTaskByTaskId(int taskId, string dbConnection)
        {
            try
            {
                if (GroupDeviceTask.DeleteGroupDeviceTaskByTaskId(taskId, dbConnection))
                {
                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();

                        var cmd = new SqlCommand("DeleteTaskByTaskId", connection);

                        cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                        cmd.Parameters["@TaskId"].Value = taskId;

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.CommandText = "DeleteTaskByTaskId";

                        cmd.ExecuteNonQuery();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "DeleteTaskByTaskId", ex, dbConnection);
            }
            return false;
        }

        public static bool DeleteTasksByIncidentId(int taskId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteTasksByIncidentId", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteTasksByIncidentId";

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "DeleteTasksByIncidentId", ex, dbConnection);
            }

            return false;
        }

        public static List<UserTask> GetAllTaskTemplates(string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            
            var task = new UserTask();
            task.Name = "Please Select Task";
            task.Id = -1;
            userTaskTemplates.Add(task);
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskTemplates", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskTemplateFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskTemplates", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllTaskTemplatesBySiteId(int siteid,string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();

            var task = new UserTask();
            task.Name = "Please Select Task";
            task.Id = -1;
            userTaskTemplates.Add(task);
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskTemplatesBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskTemplateFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskTemplatesBySiteId", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllTaskTemplatesDefaultTask(string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskTemplates", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskTemplateFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskTemplatesDefaultTask", ex, dbConnection);
            }
            return userTaskTemplates;
        }
        public static UserTask GetTaskTemplateById(int Id, string dbConnection)
        {
            UserTask userTask = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                  
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetTaskTemplateById", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskTemplateFromSqlReader(resultSet);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetTaskTemplateById", ex, dbConnection);
            }
            return userTask;
        }

        public static List<UserTask> GetAllTaskByManagerName(string name, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByManagerName", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = name;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByManagerName", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByManagerNameAndByStatus(string name, int status, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByManagerNameAndBy", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = name;
                    sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    sqlCommand.Parameters["@Status"].Value = status;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByManagerNameAndBy", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByManagerNameAndDate(string name,DateTime dt ,string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByManagerNameAndDate", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = name;
                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByManagerNameAndDate", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByManagerNameAndByStatusAndDate(string name, int status,DateTime dt, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByManagerNameAndByStatusAndDate", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = name;
                    sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    sqlCommand.Parameters["@Status"].Value = status;
                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByManagerNameAndByStatusAndDate", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByStatus(int status, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByStatus", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    sqlCommand.Parameters["@Status"].Value = status;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByStatus", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetAllTaskByStatusAndDate(int status, DateTime dt, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByStatusAndDate", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    sqlCommand.Parameters["@Status"].Value = status;
                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByStatus", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetTaskTemplateByName(string name, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetTaskTemplateByName", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@name"].Value = name;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskTemplateFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetTaskTemplateByName", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus(int assigneeType, int assigneeId, int status,
              string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeId"].Value = assigneeId;

                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeType"].Value = assigneeType;

                    sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    sqlCommand.Parameters["@Status"].Value = status;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        if (userTask.IsRecurring && userTask.RecurringParentId == 0)
                        {

                        }
                        else
                        {
                            userTasks.Add(userTask);
                        }
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllInCompleteTasksByAssigneeTypeAndAssigneeId(int assigneeType, int assigneeId,
      string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllInCompleteTasksByAssigneeTypeAndAssigneeId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeId"].Value = assigneeId;

                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeType"].Value = assigneeType;

                    sqlCommand.Parameters.Add(new SqlParameter("@PendingStatus", SqlDbType.Int));
                    sqlCommand.Parameters["@PendingStatus"].Value = (int)TaskStatus.Pending;

                    sqlCommand.Parameters.Add(new SqlParameter("@InProgressStatus", SqlDbType.Int));
                    sqlCommand.Parameters["@InProgressStatus"].Value = (int)TaskStatus.InProgress;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        if (userTask.IsRecurring && userTask.RecurringParentId == 0)
                        {

                        }
                        else
                        {
                            userTasks.Add(userTask);
                        }
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllInCompleteTasksByAssigneeTypeAndAssigneeId", ex, dbConnection);
            }
            return userTasks;
        }



        public static List<UserTask> GetAllInCompleteIncidentTasksByAssigneeTypeAndAssigneeId(int assigneeType, int assigneeId,
      string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllInCompleteIncidentTasksByAssigneeTypeAndAssigneeId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeId"].Value = assigneeId;

                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeType"].Value = assigneeType;

                    sqlCommand.Parameters.Add(new SqlParameter("@PendingStatus", SqlDbType.Int));
                    sqlCommand.Parameters["@PendingStatus"].Value = (int)TaskStatus.Pending;

                    sqlCommand.Parameters.Add(new SqlParameter("@InProgressStatus", SqlDbType.Int));
                    sqlCommand.Parameters["@InProgressStatus"].Value = (int)TaskStatus.InProgress;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllInCompleteIncidentTasksByAssigneeTypeAndAssigneeId", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllFavourites(string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllFavorites", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskTemplateFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllFavourites", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllFavouriteTasksByManagerId(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllFavouriteTasksByManagerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskTemplateFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllFavouriteTasksByManagerId", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllTemplateTasksByManagerId(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            
            var task = new UserTask();
            task.Name = "Please Select Task";
            task.Id = -1;
            userTaskTemplates.Add(task);
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTemplateTasksByManagerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskTemplateFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTemplateTasksByManagerId", ex, dbConnection);
            }
            return userTaskTemplates;
        }
        public static List<UserTask> GetAllTemplateTasksByManagerIdDefaultTask(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            var task = new UserTask();
            task.Name = "Please Select Task";
            task.Id = -1;
            userTaskTemplates.Add(task);
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTemplateTasksByManagerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskTemplateFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTemplateTasksByManagerIdDefaultTask", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllTasksByManagerId(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTasksByManagerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTasksByManagerId", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllTaskByIncidentId(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByIncidentId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByIncidentId", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllTaskByIncidentIdAndAssigneeId(int Id,int assigneeId ,string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByIncidentIdAndAssigneeId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeId"].Value = assigneeId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByIncidentIdAndAssigneeId", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        

        public static List<UserTask> GetAllTasksByManagerIdNotIncludeHandled(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTasksByManagerIdNotIncludeHandled", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTasksByManagerIdNotIncludeHandled", ex, dbConnection);
            }
            return userTaskTemplates;
        }
        public static List<UserTask> GetAllRecurringTasks(string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllRecurring", connection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllRecurringTasks", ex, dbConnection);
            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllRecurringTasksBySite(int id,string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllRecurringBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllRecurringTasksBySite", ex, dbConnection);
            }
            return userTaskTemplates;
        }
        public static List<UserTask> GetAllRecurringTasksByCId(int id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllRecurringTasksByCId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllRecurringTasksBySite", ex, dbConnection);
            }
            return userTaskTemplates;
        }
        public static List<UserTask> GetAllRecurringByManagerIdNotIncludeHandled(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllRecurringByManagerIdNotIncludeHandled", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllRecurringByManagerIdNotIncludeHandled", ex, dbConnection);
            }
            return userTaskTemplates;
        }
        public static bool DeleteRecurringTaskByTaskId(int taskId, string dbConnection)
        {
            //if (GroupDeviceTask.DeleteGroupDeviceTaskByTaskId(taskId, dbConnection))
            //{
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteRecurringTaskByTaskId", connection);

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = taskId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteRecurringTaskByTaskId";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "DeleteRecurringTaskByTaskId", ex, dbConnection);
                return false;
            }
            return true;
        }

        public static bool DeleteRecurringTaskByTaskIdAndDate(int taskId,DateTime dt ,string dbConnection)
        {
            //if (GroupDeviceTask.DeleteGroupDeviceTaskByTaskId(taskId, dbConnection))
            //{
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteRecurringTaskByTaskIdAndDate", connection);

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = taskId;
                    cmd.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    cmd.Parameters["@Date"].Value = dt;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteRecurringTaskByTaskIdAndDate";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "DeleteRecurringTaskByTaskIdAndDate", ex, dbConnection);
                return false;
            }
            return true;
        }

        public static List<UserTask> GetAllTasksOfTeamByManagerIdNotIncludingRecurringParent(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTasksOfTeamByManagerIdNotIncludingRecurringParent", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTasksOfTeamByManagerIdNotIncludingRecurringParent", ex, dbConnection);
                
            }
            return userTaskTemplates;
        }
        public static List<UserTask> GetAllTasksOfTeamByManagerId(int Id, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTasksOfTeamByManagerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTasksOfTeamByManagerId", ex, dbConnection);

            }
            return userTaskTemplates;
        }
        public static List<UserTask> GetAllTasksOfTeamByManagerIdAndStatus(int Id,int status ,string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTasksOfTeamByManagerIdAndStatus", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    sqlCommand.Parameters["@Status"].Value = status;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTasksOfTeamByManagerIdAndStatus", ex, dbConnection);

            }
            return userTaskTemplates;
        }
        public static List<UserTask> GetAllTasksOfTeamByManagerIdAndStatusAndDate(int Id, int status, DateTime dt,string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTasksOfTeamByManagerIdAndStatus", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = Id;
                    sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    sqlCommand.Parameters["@Status"].Value = status;
                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTasksOfTeamByManagerIdAndStatus", ex, dbConnection);

            }
            return userTaskTemplates;
        }

        public static List<UserTask> GetAllTaskAssignedToAndAssignedByAndStatusAndDate(string managername, int assigneetype, int Id, int status, DateTime dt, string dbConnection)
        {
            var userTaskTemplates = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskAssignedToAndAssignedByAndStatusAndDate", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeId"].Value = Id;
                    sqlCommand.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                    sqlCommand.Parameters["@AssigneeType"].Value = assigneetype;
                    sqlCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    sqlCommand.Parameters["@Status"].Value = status;
                    sqlCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
                    sqlCommand.Parameters["@Date"].Value = dt;
                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = managername;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTaskTemplate = GetUserTaskFromSqlReader(resultSet);
                        userTaskTemplates.Add(userTaskTemplate);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskAssignedToAndAssignedByAndStatusAndDate", ex, dbConnection);

            }
            return userTaskTemplates;
        }



        public List<UserTaskAttachment> UserTaskAttachmentList { get; set; }

        public static string CustomerTaskId(int siteId, int contractid, string dbConnection, string auditDbConnection)
        {
            var custTaskId = "";
            var count = 0;
            try
            {
                using (var connection = new SqlConnection(auditDbConnection))
                {

                    connection.Open();
                    var sqlCommand = new SqlCommand("CustomerTaskId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandText = "CustomerTaskId";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        if (resultSet["CustomerTaskId"] != DBNull.Value)
                            count = Convert.ToInt32(resultSet["CustomerTaskId"].ToString());

                    }
                    connection.Close();
                    resultSet.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "CustomerTaskId", ex, dbConnection);
            }
            var result = 10000000 + (count + 1);
            var no="TN";
            
            if (contractid > 0)
            {
                var gContract = ContractInfo.GetContractInfoById(contractid, dbConnection);
                if (gContract != null)
                {
                    if (gContract.ContractType == (int)ContractInfo.ContractTypes.PPM)
                    {
                        if (gContract.SRTotal > 0)
                            no = "PSR";
                        else
                            no = "PPM";
                    }
                    else if (gContract.ContractType == (int)ContractInfo.ContractTypes.SR)
                        no = "SR";
                } 
            } 
                custTaskId = string.Concat(no, result.ToString().Substring(1, result.ToString().Length - 1));
            //"TN000000003"

            return custTaskId;
        }

        public static List<UserTask> GetAllTaskByContractId(int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByContractId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@ContractId", SqlDbType.Int));
                    sqlCommand.Parameters["@ContractId"].Value = siteid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByContractId", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetTaskByAssigneeIdAndActualStartDate(int aid,int type ,DateTime from ,DateTime to,string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetTaskByAssigneeIdAndActualStartDate", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@AId", SqlDbType.Int));
                    sqlCommand.Parameters["@AId"].Value = aid;
                    sqlCommand.Parameters.Add(new SqlParameter("@Atype", SqlDbType.Int));
                    sqlCommand.Parameters["@Atype"].Value = type;
                    sqlCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@startDate"].Value = from;
                    sqlCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@endDate"].Value = to;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetTaskByAssigneeIdAndActualStartDate", ex, dbConnection);
            }
            return userTasks;
        }

        public static List<UserTask> GetAllTaskByProjectId(int siteid, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllTaskByProjectId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = siteid;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        var userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTaskByProjectId", ex, dbConnection);
            }
            return userTasks;
        }
        public static List<UserTask> GetAllTasksByTaskLinkId(int incidentId, string dbConnection)
        {
            var userTasks = new List<UserTask>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    UserTask userTask = null;
                    connection.Open();
                    var sqlCommand = new SqlCommand("@TaskLinkId", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TaskLinkId", SqlDbType.Int));
                    sqlCommand.Parameters["@TaskLinkId"].Value = incidentId;
                    sqlCommand.CommandText = "GetAllTasksByTaskLinkId";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var resultSet = sqlCommand.ExecuteReader();
                    while (resultSet.Read())
                    {
                        userTask = GetUserTaskFromSqlReader(resultSet);
                        userTasks.Add(userTask);
                    }
                    connection.Close();
                    resultSet.Close();

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "GetAllTasksByTaskLinkId", ex, dbConnection);
            }
            return userTasks;
        }

        public static bool UpdateTaskSignature(int taskId, bool dt, string dbConnection)
        {
            //if (GroupDeviceTask.DeleteGroupDeviceTaskByTaskId(taskId, dbConnection))
            //{
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("UpdateTaskSignature", connection);

                    cmd.Parameters.Add(new SqlParameter("@taskId", SqlDbType.Int));
                    cmd.Parameters["@taskId"].Value = taskId;
                    cmd.Parameters.Add(new SqlParameter("@signature", SqlDbType.TinyInt));
                    cmd.Parameters["@signature"].Value = dt;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "UpdateTaskSignature";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "UpdateTaskSignature", ex, dbConnection);
                return false;
            }
            return true;
        }

        public static bool UpdateJobStatusTask(int taskId, int dt, string dbConnection)
        {
            //if (GroupDeviceTask.DeleteGroupDeviceTaskByTaskId(taskId, dbConnection))
            //{
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("UpdateJobStatusTask", connection);

                    cmd.Parameters.Add(new SqlParameter("@taskId", SqlDbType.Int));
                    cmd.Parameters["@taskId"].Value = taskId;
                    cmd.Parameters.Add(new SqlParameter("@status", SqlDbType.Int));
                    cmd.Parameters["@status"].Value = dt;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "UpdateJobStatusTask";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTask", "UpdateJobStatusTask", ex, dbConnection);
                return false;
            }
            return true;
        }
    }
}
