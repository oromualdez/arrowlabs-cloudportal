﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class TaskCategoryAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        
        private static TaskCategoryAudit GetTaskCategoryAuditFromSqlReader(SqlDataReader reader)
        {
            var hotEventCategory = new TaskCategoryAudit();

            hotEventCategory.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                hotEventCategory.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("CategoryId") && reader["CategoryId"] != DBNull.Value)
                hotEventCategory.CategoryId = Convert.ToInt32(reader["CategoryId"].ToString());
            if (columns.Contains("Description") && reader["Description"] != DBNull.Value)
                hotEventCategory.Description = reader["Description"].ToString();
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                hotEventCategory.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                hotEventCategory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (columns.Contains("IsActive") && reader["IsActive"] != DBNull.Value)
                hotEventCategory.IsActive = Convert.ToBoolean(reader["IsActive"]);
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                hotEventCategory.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                hotEventCategory.SiteId = Convert.ToInt32(reader["SiteId"]);
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                hotEventCategory.CustomerId = Convert.ToInt32(reader["CustomerId"]);
            
            return hotEventCategory;
        }

        public static List<TaskCategoryAudit> GetAllTaskCategoryAudit(string dbConnection)
        {
            var hotEventCategories = new List<TaskCategoryAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllTaskCategoryAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetTaskCategoryAuditFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }

        public static bool InsertTaskCategoryAudit(TaskCategoryAudit mobileHotEventCategoryAudit, string dbConnection)
        {


            if (mobileHotEventCategoryAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskCategoryAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(mobileHotEventCategoryAudit.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = mobileHotEventCategoryAudit.Id;
                    cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = mobileHotEventCategoryAudit.CategoryId;
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = mobileHotEventCategoryAudit.Description;
                    cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = mobileHotEventCategoryAudit.IsDeleted;
                    cmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = mobileHotEventCategoryAudit.IsActive;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = mobileHotEventCategoryAudit.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = mobileHotEventCategoryAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = mobileHotEventCategoryAudit.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = mobileHotEventCategoryAudit.CustomerId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}