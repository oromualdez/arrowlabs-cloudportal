﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
   public  class TaskRemarks
    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int CustomerId { get; set; }
        public string Remarks { get; set; }
        public int TaskId { get; set; }
        public int SiteId { get; set; }
        public int CustomerInfoId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }

        public string CustomerUName { get; set; }

        public static int InsertOrUpdateTaskRemarks(TaskRemarks taskRemarks, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = taskRemarks.Id;

            var action = (taskRemarks.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (taskRemarks != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateTaskRemarks", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskRemarks.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = taskRemarks.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = taskRemarks.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = taskRemarks.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = taskRemarks.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = taskRemarks.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = taskRemarks.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = taskRemarks.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = taskRemarks.TaskId;


                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = taskRemarks.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateTaskRemarks";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            taskRemarks.Id = id;
                            var audit = new TaskRemarksAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, taskRemarks.UpdatedBy);
                            Reflection.CopyProperties(taskRemarks, audit);
                            TaskRemarksAudit.InsertTaskRemarksAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertTaskRemarksAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static TaskRemarks GetTaskRemarksFromSqlReader(SqlDataReader reader)
        {
            var taskRemarks = new TaskRemarks();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                taskRemarks.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["Remarks"] != DBNull.Value)
                taskRemarks.Remarks = reader["Remarks"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                taskRemarks.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["UpdatedDate"] != DBNull.Value)
                taskRemarks.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                taskRemarks.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedBy"] != DBNull.Value)
                taskRemarks.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["CustomerId"] != DBNull.Value)
                taskRemarks.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (reader["TaskId"] != DBNull.Value)
                taskRemarks.TaskId = Convert.ToInt32(reader["TaskId"].ToString());

            if (columns.Contains("CustomerUName"))
                taskRemarks.CustomerUName = reader["CustomerUName"].ToString();


            if (reader["SiteId"] != DBNull.Value)
                taskRemarks.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains( "FName") && reader["FName"] != DBNull.Value)
                taskRemarks.FName = reader["FName"].ToString();

            if (columns.Contains( "LName") && reader["LName"] != DBNull.Value)
                taskRemarks.LName = reader["LName"].ToString();

            if (columns.Contains( "CustomerInfoId") & reader["CustomerInfoId"] != DBNull.Value)
                taskRemarks.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"].ToString());

            return taskRemarks;
        }
        public static List<TaskRemarks> GetTaskRemarksByTaskId(int taskId, string dbConnection)
        {
            var taskRemarks = new List<TaskRemarks>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetTaskRemarksByTaskId";
                sqlCommand.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                sqlCommand.Parameters["@TaskId"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var task = GetTaskRemarksFromSqlReader(reader);
                    taskRemarks.Add(task);
                }
                connection.Close();
                reader.Close();
            }
            return taskRemarks;
        }

        public static TaskRemarks GetTaskRemarksById(int id, string dbConnection)
        {
            TaskRemarks task = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTaskRemarksById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    task = GetTaskRemarksFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return task;
        }
        public static void DeleteTaskRemarksById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTaskRemarksById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTaskRemarksById";

                cmd.ExecuteNonQuery();
            }
        }

    }
}
