﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class HappinessLevelAudit
    {
        public BaseAudit BaseAudit { get; set; }    
        public int Id { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public int SmileCount { get; set; }
        public int FrownCount { get; set; }
        public int MehCount { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public static int InsertorUpdateHappinessLevelAudit(HappinessLevelAudit notification, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(notification.BaseAudit, dbConnection);

            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertHappinessLevelAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@MehCount", SqlDbType.Int));
                    cmd.Parameters["@MehCount"].Value = notification.MehCount;

                    cmd.Parameters.Add(new SqlParameter("@FrownCount", SqlDbType.Int));
                    cmd.Parameters["@FrownCount"].Value = notification.FrownCount;

                    cmd.Parameters.Add(new SqlParameter("@SmileCount", SqlDbType.Int));
                    cmd.Parameters["@SmileCount"].Value = notification.SmileCount; 

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;
                     
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = notification.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = notification.UpdatedBy;
                     
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId; 

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertHappinessLevelAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return 0;
        }
    }
}
