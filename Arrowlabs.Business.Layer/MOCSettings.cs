﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class MOCSettings
    {
        public enum MOCSettingTypes
        {
            None = 0,
            Offence = 1,
            GPS = 2,
            HotEvent = 3,
            MobileHotEvent = 4,
            MobileOffence = 5,
            MileCamera = 6,
            HeitelCamera = 7
        }
        public int Id { get; set; }
        public int grid2x2Tile1 { get; set; }
        public int grid2x2Tile2 { get; set; }
        public int grid2x2Tile3 { get; set; }
        public int grid2x2Tile4 { get; set; }

        public int grid1x3Tile1 { get; set; }
        public int grid1x3Tile2 { get; set; }
        public int grid1x3Tile3 { get; set; }
        public int grid1x3Tile4 { get; set; }


        public int grid2x3Tile1 { get; set; }
        public int grid2x3Tile2 { get; set; }
        public int grid2x3Tile3 { get; set; }
        public int grid2x3Tile4 { get; set; }
        public int grid2x3Tile5 { get; set; }
        public int grid2x3Tile6 { get; set; }

        public int grid3x3Tile1 { get; set; }
        public int grid3x3Tile2 { get; set; }
        public int grid3x3Tile3 { get; set; }
        public int grid3x3Tile4 { get; set; }
        public int grid3x3Tile5 { get; set; }
        public int grid3x3Tile6 { get; set; }
        public int grid3x3Tile7 { get; set; }
        public int grid3x3Tile8 { get; set; }
        public int grid3x3Tile9 { get; set; }

        public bool DispatchAlarm { get; set; }

        public bool DispatchNotification { get; set; }

        public bool CreateTask { get; set; }

        public bool TaskList { get; set; }

        public bool CameraList { get; set; }

        public bool Users { get; set; }

        public bool Groups { get; set; }

        public bool Devices { get; set; }

        public bool Settings { get; set; }

        public string CreatedBy { get; set; }
        public int LoadLayout { get; set; }
        public int grid1x1Tile1 { get; set; }

        public static MOCSettings GetMOCSettingsFromSqlReader(SqlDataReader resultSet)
        {
            var mocSettings = new MOCSettings();


            if (resultSet["Id"] != DBNull.Value)
                mocSettings.Id = Convert.ToInt32(resultSet["Id"].ToString());

            if (resultSet["grid2x2Tile1"] != DBNull.Value)
                mocSettings.grid2x2Tile1 = Convert.ToInt32(resultSet["grid2x2Tile1"].ToString());
            if (resultSet["grid2x2Tile2"] != DBNull.Value)
                mocSettings.grid2x2Tile2 = Convert.ToInt32(resultSet["grid2x2Tile2"].ToString());
            if (resultSet["grid2x2Tile3"] != DBNull.Value)
                mocSettings.grid2x2Tile3 = Convert.ToInt32(resultSet["grid2x2Tile3"].ToString());
            if (resultSet["grid2x2Tile4"] != DBNull.Value)
                mocSettings.grid2x2Tile4 = Convert.ToInt32(resultSet["grid2x2Tile4"].ToString());
            
            if (resultSet["grid1x3Tile1"] != DBNull.Value)
                mocSettings.grid1x3Tile1 = Convert.ToInt32(resultSet["grid1x3Tile1"].ToString());
            if (resultSet["grid1x3Tile2"] != DBNull.Value)
                mocSettings.grid1x3Tile2 = Convert.ToInt32(resultSet["grid1x3Tile2"].ToString());
            if (resultSet["grid1x3Tile3"] != DBNull.Value)
                mocSettings.grid1x3Tile3 = Convert.ToInt32(resultSet["grid1x3Tile3"].ToString());
            if (resultSet["grid1x3Tile4"] != DBNull.Value)
                mocSettings.grid1x3Tile4 = Convert.ToInt32(resultSet["grid1x3Tile4"].ToString());

            if (resultSet["grid2x3Tile1"] != DBNull.Value)
                mocSettings.grid2x3Tile1 = Convert.ToInt32(resultSet["grid2x3Tile1"].ToString());
            if (resultSet["grid2x3Tile2"] != DBNull.Value)
                mocSettings.grid2x3Tile2 = Convert.ToInt32(resultSet["grid2x3Tile2"].ToString());
            if (resultSet["grid2x3Tile3"] != DBNull.Value)
                mocSettings.grid2x3Tile3 = Convert.ToInt32(resultSet["grid2x3Tile3"].ToString());
            if (resultSet["grid2x3Tile4"] != DBNull.Value)
                mocSettings.grid2x3Tile4 = Convert.ToInt32(resultSet["grid2x3Tile4"].ToString());
            if (resultSet["grid2x3Tile5"] != DBNull.Value)
                mocSettings.grid2x3Tile5 = Convert.ToInt32(resultSet["grid2x3Tile5"].ToString());
            if (resultSet["grid2x3Tile6"] != DBNull.Value)
                mocSettings.grid2x3Tile6 = Convert.ToInt32(resultSet["grid2x3Tile6"].ToString());

            if (resultSet["grid3x3Tile1"] != DBNull.Value)
                mocSettings.grid3x3Tile1 = Convert.ToInt32(resultSet["grid3x3Tile1"].ToString());
            if (resultSet["grid3x3Tile2"] != DBNull.Value)
                mocSettings.grid3x3Tile2 = Convert.ToInt32(resultSet["grid3x3Tile2"].ToString());
            if (resultSet["grid3x3Tile3"] != DBNull.Value)
                mocSettings.grid3x3Tile3 = Convert.ToInt32(resultSet["grid3x3Tile3"].ToString());
            if (resultSet["grid3x3Tile4"] != DBNull.Value)
                mocSettings.grid3x3Tile4 = Convert.ToInt32(resultSet["grid3x3Tile4"].ToString());
            if (resultSet["grid3x3Tile5"] != DBNull.Value)
                mocSettings.grid3x3Tile5 = Convert.ToInt32(resultSet["grid3x3Tile5"].ToString());
            if (resultSet["grid3x3Tile6"] != DBNull.Value)
                mocSettings.grid3x3Tile6 = Convert.ToInt32(resultSet["grid3x3Tile6"].ToString());
            if (resultSet["grid3x3Tile7"] != DBNull.Value)
                mocSettings.grid3x3Tile7 = Convert.ToInt32(resultSet["grid3x3Tile7"].ToString());
            if (resultSet["grid3x3Tile8"] != DBNull.Value)
                mocSettings.grid3x3Tile8 = Convert.ToInt32(resultSet["grid3x3Tile8"].ToString());
            if (resultSet["grid3x3Tile9"] != DBNull.Value)
                mocSettings.grid3x3Tile9 = Convert.ToInt32(resultSet["grid3x3Tile9"].ToString());

            if (resultSet["DispatchAlarm"] != DBNull.Value)
                mocSettings.DispatchAlarm = Convert.ToBoolean(resultSet["DispatchAlarm"].ToString());

            if (resultSet["DispatchNotification"] != DBNull.Value)
                mocSettings.DispatchNotification = Convert.ToBoolean(resultSet["DispatchNotification"].ToString());

            if (resultSet["CreateTask"] != DBNull.Value)
                mocSettings.CreateTask = Convert.ToBoolean(resultSet["CreateTask"].ToString());

            if (resultSet["TaskList"] != DBNull.Value)
                mocSettings.TaskList = Convert.ToBoolean(resultSet["TaskList"].ToString());

            if (resultSet["CameraList"] != DBNull.Value)
                mocSettings.CameraList = Convert.ToBoolean(resultSet["CameraList"].ToString());

            if (resultSet["Users"] != DBNull.Value)
                mocSettings.Users = Convert.ToBoolean(resultSet["Users"].ToString());

            if (resultSet["Groups"] != DBNull.Value)
                mocSettings.Groups = Convert.ToBoolean(resultSet["Groups"].ToString());

            if (resultSet["Devices"] != DBNull.Value)
                mocSettings.Devices = Convert.ToBoolean(resultSet["Devices"].ToString());

            if (resultSet["Settings"] != DBNull.Value)
                mocSettings.Settings = Convert.ToBoolean(resultSet["Settings"].ToString());

            if (resultSet["CreatedBy"] != DBNull.Value)
                mocSettings.CreatedBy = resultSet["CreatedBy"].ToString();

            if (resultSet["LoadLayout"] != DBNull.Value)
                mocSettings.LoadLayout = Convert.ToInt16(resultSet["LoadLayout"].ToString());
            if (resultSet["grid1x1Tile1"] != DBNull.Value)
                mocSettings.grid1x1Tile1 = Convert.ToInt16(resultSet["grid1x1Tile1"].ToString());
            


            return mocSettings;
        }

        public static MOCSettings GetMOCSettingsByCreatedBy(string name, string dbConnection)
        {
            MOCSettings setClass = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetMOCSettingsByCreatedBy", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    setClass = GetMOCSettingsFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return setClass;
        }

        public static MOCSettings GetMOCSettings(string dbConnection)
        {
            var setClass = new MOCSettings();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetMOCSettings", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    setClass = GetMOCSettingsFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return setClass;
        }
        public static bool InsertOrUpdateMOCSettings(MOCSettings settingClass, string dbConnection)
        {
            if (settingClass != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMOCSettings", connection);


                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = settingClass.Id;

                    cmd.Parameters.Add(new SqlParameter("@grid2x2Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid2x2Tile1"].Value = settingClass.grid2x2Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid2x2Tile2", SqlDbType.Int));
                    cmd.Parameters["@grid2x2Tile2"].Value = settingClass.grid2x2Tile2;

                    cmd.Parameters.Add(new SqlParameter("@grid2x2Tile3", SqlDbType.Int));
                    cmd.Parameters["@grid2x2Tile3"].Value = settingClass.grid2x2Tile3;

                    cmd.Parameters.Add(new SqlParameter("@grid2x2Tile4", SqlDbType.Int));
                    cmd.Parameters["@grid2x2Tile4"].Value = settingClass.grid2x2Tile4;


                    cmd.Parameters.Add(new SqlParameter("@grid1x3Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid1x3Tile1"].Value = settingClass.grid1x3Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid1x3Tile2", SqlDbType.Int));
                    cmd.Parameters["@grid1x3Tile2"].Value = settingClass.grid1x3Tile2;

                    cmd.Parameters.Add(new SqlParameter("@grid1x3Tile3", SqlDbType.Int));
                    cmd.Parameters["@grid1x3Tile3"].Value = settingClass.grid1x3Tile3;

                    cmd.Parameters.Add(new SqlParameter("@grid1x3Tile4", SqlDbType.Int));
                    cmd.Parameters["@grid1x3Tile4"].Value = settingClass.grid1x3Tile4;


                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile1"].Value = settingClass.grid2x3Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile2", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile2"].Value = settingClass.grid2x3Tile2;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile3", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile3"].Value = settingClass.grid2x3Tile3;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile4", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile4"].Value = settingClass.grid2x3Tile4;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile5", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile5"].Value = settingClass.grid2x3Tile5;

                    cmd.Parameters.Add(new SqlParameter("@grid2x3Tile6", SqlDbType.Int));
                    cmd.Parameters["@grid2x3Tile6"].Value = settingClass.grid2x3Tile6;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile1"].Value = settingClass.grid3x3Tile1;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile2", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile2"].Value = settingClass.grid3x3Tile2;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile3", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile3"].Value = settingClass.grid3x3Tile3;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile4", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile4"].Value = settingClass.grid3x3Tile4;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile5", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile5"].Value = settingClass.grid3x3Tile5;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile6", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile6"].Value = settingClass.grid3x3Tile6;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile7", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile7"].Value = settingClass.grid3x3Tile7;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile8", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile8"].Value = settingClass.grid3x3Tile8;

                    cmd.Parameters.Add(new SqlParameter("@grid3x3Tile9", SqlDbType.Int));
                    cmd.Parameters["@grid3x3Tile9"].Value = settingClass.grid3x3Tile9;


                    cmd.Parameters.Add(new SqlParameter("@DispatchAlarm", SqlDbType.Bit));
                    cmd.Parameters["@DispatchAlarm"].Value = settingClass.DispatchAlarm;

                    cmd.Parameters.Add(new SqlParameter("@DispatchNotification", SqlDbType.Bit));
                    cmd.Parameters["@DispatchNotification"].Value = settingClass.DispatchNotification;

                    cmd.Parameters.Add(new SqlParameter("@CreateTask", SqlDbType.Bit));
                    cmd.Parameters["@CreateTask"].Value = settingClass.CreateTask;

                    cmd.Parameters.Add(new SqlParameter("@TaskList", SqlDbType.Bit));
                    cmd.Parameters["@TaskList"].Value = settingClass.TaskList;

                    cmd.Parameters.Add(new SqlParameter("@CameraList", SqlDbType.Bit));
                    cmd.Parameters["@CameraList"].Value = settingClass.CameraList;

                    cmd.Parameters.Add(new SqlParameter("@Users", SqlDbType.Bit));
                    cmd.Parameters["@Users"].Value = settingClass.Users;

                    cmd.Parameters.Add(new SqlParameter("@Groups", SqlDbType.Bit));
                    cmd.Parameters["@Groups"].Value = settingClass.Groups;

                    cmd.Parameters.Add(new SqlParameter("@Devices", SqlDbType.Bit));
                    cmd.Parameters["@Devices"].Value = settingClass.Devices;

                    cmd.Parameters.Add(new SqlParameter("@Settings", SqlDbType.Bit));
                    cmd.Parameters["@Settings"].Value = settingClass.Settings;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = settingClass.CreatedBy;


                    cmd.Parameters.Add(new SqlParameter("@LoadLayout", SqlDbType.Int));
                    cmd.Parameters["@LoadLayout"].Value = settingClass.LoadLayout;

                    cmd.Parameters.Add(new SqlParameter("@grid1x1Tile1", SqlDbType.Int));
                    cmd.Parameters["@grid1x1Tile1"].Value = settingClass.grid1x1Tile1;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateMOCSettings";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
