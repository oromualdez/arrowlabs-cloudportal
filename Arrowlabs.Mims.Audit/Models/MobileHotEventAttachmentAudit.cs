﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class MobileHotEventAttachmentAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }         
        public int HotEventId { get; set; }         
        public byte[] Attachment { get; set; }         
        public string AttachmentPath { get; set; }
        public bool IsPortal { get; set; }

        public string AttachmentName { get; set; }    

        public string CreatedBy { get; set; }         
        public string AccountName { get; set; }         
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        private static MobileHotEventAttachmentAudit GetMobileHotEventAttachmentAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var hotEventAttachmentAudit = new MobileHotEventAttachmentAudit();
            hotEventAttachmentAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                hotEventAttachmentAudit.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("HotEventId") && reader["HotEventId"] != DBNull.Value)
                hotEventAttachmentAudit.HotEventId = Convert.ToInt32(reader["HotEventId"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                hotEventAttachmentAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                hotEventAttachmentAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (columns.Contains("Attachment") && reader["Attachment"] != DBNull.Value)
                hotEventAttachmentAudit.Attachment = (byte[])reader["Attachment"];
            if (columns.Contains("AttachmentPath") && reader["AttachmentPath"] != DBNull.Value)
                hotEventAttachmentAudit.AttachmentPath = reader["AttachmentPath"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                 hotEventAttachmentAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString()); 

            return hotEventAttachmentAudit;
        }

        public static List<MobileHotEventAttachmentAudit> GetAllMobileHotEventAttachmentAudit(string dbConnection)
        {
            var hotEvents = new List<MobileHotEventAttachmentAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMobileHotEventAttachmentAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventAttachmentAuditFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }

        public static bool InsertMobileHotEventAttachmentAudit(MobileHotEventAttachmentAudit hotEventAttachmentAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(hotEventAttachmentAudit.BaseAudit, dbConnection);

            if (hotEventAttachmentAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMobileHotEventAttachmentAudit", connection);


                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = hotEventAttachmentAudit.Id;

                    cmd.Parameters.Add("@HotEventId", SqlDbType.Int).Value = hotEventAttachmentAudit.HotEventId;

                    cmd.Parameters.Add("@Attachment", SqlDbType.VarBinary).Value = hotEventAttachmentAudit.Attachment;

                    cmd.Parameters.Add("@AttachmentPath", SqlDbType.NVarChar).Value = hotEventAttachmentAudit.AttachmentPath;

                    cmd.Parameters.Add("@AttachmentName", SqlDbType.NVarChar).Value = hotEventAttachmentAudit.AttachmentName;

                    cmd.Parameters.Add("@IsPortal", SqlDbType.Bit).Value = hotEventAttachmentAudit.IsPortal;
                    

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = hotEventAttachmentAudit.CreatedBy;

                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = hotEventAttachmentAudit.SiteId;

                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = hotEventAttachmentAudit.CustomerId;  
                     
                    if (hotEventAttachmentAudit.Id == 0)
                    {
                        cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    }
                    else
                    {
                        cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = hotEventAttachmentAudit.CreatedDate;
                    }

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
