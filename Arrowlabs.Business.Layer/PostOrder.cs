﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class PostOrder
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Description { get; set; }
        public string UpdatedBy { get; set; }
        [DataMember]
        public int LocationId { get; set; }
        [DataMember]
        public int SubLocationId { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }

        public string LocationName { get; set; }

        public string SubLocationName { get; set; }

        public string PdfPath { get; set; }

        public string SiteName { get; set; }

        public int CustomerId { get; set; }

        public string CustomerUName { get; set; }

        [DataMember]
        public int SiteId { get; set; }
        public static bool DeletePostOrderById(int colorCode, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("DeletePostOrderById", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = colorCode;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeletePostOrderById";

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("PostOrder", "Delete", ex, dbConnection);
                return false;
            }
        }
        
        private static PostOrder GetPostOrderFromSqlReader(SqlDataReader reader)
        {
            var postOrder = new PostOrder();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                postOrder.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Description"] != DBNull.Value)
                postOrder.Description = reader["Description"].ToString();
            if (reader["LocationId"] != DBNull.Value)
                postOrder.LocationId = Convert.ToInt32(reader["LocationId"].ToString());
            if (reader["SubLocationId"] != DBNull.Value)
                postOrder.SubLocationId = Convert.ToInt32(reader["SubLocationId"].ToString());
            if (reader["IsDeleted"] != DBNull.Value)
                postOrder.IsDeleted = (String.IsNullOrEmpty(reader["IsDeleted"].ToString())) ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                postOrder.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                postOrder.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                postOrder.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["LocationName"] != DBNull.Value)
                postOrder.LocationName = (String.IsNullOrEmpty(reader["LocationName"].ToString())) ? "All" : reader["LocationName"].ToString();
            if (reader["SubLocationName"] != DBNull.Value)
                postOrder.SubLocationName = (String.IsNullOrEmpty(reader["SubLocationName"].ToString())) ? "All" : reader["SubLocationName"].ToString();

            if (columns.Contains( "PdfPath") && reader["PdfPath"] != DBNull.Value)
                postOrder.PdfPath = reader["PdfPath"].ToString();

            if (columns.Contains( "SiteName") && reader["SiteName"] != DBNull.Value)
                postOrder.SiteName = reader["SiteName"].ToString();

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                postOrder.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                postOrder.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                postOrder.CustomerUName = reader["CustomerUName"].ToString();

            
            
            return postOrder;
        }

        private static PostOrder GetCNLPostOrderFromSqlReader(SqlDataReader reader)
        {
            var postOrder = new PostOrder();

            if (reader["Id"] != DBNull.Value)
                postOrder.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Location"] != DBNull.Value)
                postOrder.LocationName = reader["Location"].ToString();
            if (reader["Orders"] != DBNull.Value)
                postOrder.Description = reader["Orders"].ToString();
            return postOrder;
        }

        /// <summary>
        /// It returns all post orders
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<PostOrder> GetAllPostOrders(string dbConnection)
        {
            var postOrders = new List<PostOrder>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPostOrders", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var postOrder = GetPostOrderFromSqlReader(reader);
                    postOrders.Add(postOrder);
                }
                connection.Close();
                reader.Close();
            }
            return postOrders;
        }

        public static List<PostOrder> GetAllCNLPostOrders(string dbConnection)
        {
            var postOrders = new List<PostOrder>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPostOrders", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var postOrder = GetCNLPostOrderFromSqlReader(reader);
                    postOrders.Add(postOrder);
                }
                connection.Close();
                reader.Close();
            }
            return postOrders;
        }

        public static List<PostOrder> GetAllPostOrdersBySiteId(int siteId, string dbConnection)
        {

            List<PostOrder> CusEvent = new List<PostOrder>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPostOrdersBySiteID", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllPostOrdersBySiteID";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    CusEvent.Add(GetPostOrderFromSqlReader(reader));
                }
                connection.Close();
                reader.Close();
            }
            return CusEvent;
        }

        public static List<PostOrder> GetAllPostOrdersByLevel5(int siteId, string dbConnection)
        {

            List<PostOrder> CusEvent = new List<PostOrder>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPostOrdersByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllPostOrdersByLevel5";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    CusEvent.Add(GetPostOrderFromSqlReader(reader));
                }
                connection.Close();
                reader.Close();
            }
            return CusEvent;
        }

        public static List<PostOrder> GetAllPostOrdersByCustomerId(int siteId, string dbConnection)
        {

            List<PostOrder> CusEvent = new List<PostOrder>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPostOrdersByCustomerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllPostOrdersByCustomerId";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    CusEvent.Add(GetPostOrderFromSqlReader(reader));
                }
                connection.Close();
                reader.Close();
            }
            return CusEvent;
        }

        /// <summary>
        /// It returns all post orders for specific location
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<PostOrder> GetAllPostOrdersByMainLocationIdAndSubLocationId(string locationname,string dbConnection)
        {

            var allPostOrders = GetAllCNLPostOrders(dbConnection);
            var returnPostOrders = new List<PostOrder>();
            foreach(var postorder in allPostOrders)
            {
                if(postorder.LocationName == locationname)
                    returnPostOrders.Add(postorder);
            }
            return returnPostOrders;
        }

        /// <summary>
        /// It will insert or update post order
        /// </summary>
        /// <param name="postOrder"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static bool InsertorUpdatePostOrder(PostOrder postOrder, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (postOrder != null)
            {
                int id = postOrder.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdatePostOrder", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = postOrder.Id;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = postOrder.Description;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = postOrder.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@SubLocationId", SqlDbType.Int));
                    cmd.Parameters["@SubLocationId"].Value = postOrder.SubLocationId;

                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = postOrder.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = postOrder.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = postOrder.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = postOrder.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@PdfPath", SqlDbType.NVarChar));
                    cmd.Parameters["@PdfPath"].Value = postOrder.PdfPath;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = postOrder.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = postOrder.CustomerId;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            postOrder.Id = id;
                            var audit = new PostOrderAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, postOrder.UpdatedBy);
                            Reflection.CopyProperties(postOrder, audit);
                            PostOrderAudit.InsertPostOrderAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdatePostOrder", ex, auditDbConnection);
                    }
                }
            }
            return true;
        }
    }
}