﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class EventHistory
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int EventId { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int IncidentAction { get; set; }
        [DataMember]
        public string IncidentStatus { get; set; }
        [DataMember]
        public string IncidentDescription { get; set; }
        [DataMember]
        public string Remarks { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longtitude { get; set; }

        [DataMember]
        public int SiteId { get; set; }

        [DataMember]
        public int CustomerId { get; set; }

        public string CustomerUName { get; set; }
        public string ACustomerUName { get; set; }

        public string CustomerFullName { get; set; }
        public string ACustomerFullName { get; set; }

        

        public string TaskName { get; set; }

        public bool isTask { get; set; }

        public bool isSubTask { get; set; }

        private static EventHistory GetEventHistoryFromSqlReader(SqlDataReader reader, string dbConnection)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var eventHistory = new EventHistory();

            if (reader["EventId"] != DBNull.Value)
                eventHistory.EventId = Convert.ToInt32(reader["EventId"].ToString());
            if (reader["Action"] != DBNull.Value)
                eventHistory.IncidentAction = Convert.ToInt32(reader["Action"].ToString());
            if (reader["Username"] != DBNull.Value)
                eventHistory.UserName = reader["Username"].ToString();
            if (reader["ID"] != DBNull.Value)
                eventHistory.Id = Convert.ToInt32(reader["ID"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                eventHistory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                eventHistory.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["Remarks"] != DBNull.Value)
                eventHistory.Remarks = reader["Remarks"].ToString();

            if (reader["Longitude"] != DBNull.Value)
                eventHistory.Longtitude = reader["Longitude"].ToString();

            if (reader["Latitude"] != DBNull.Value)
                eventHistory.Latitude = reader["Latitude"].ToString();

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                eventHistory.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                eventHistory.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains( "ACustomerUName") && reader["ACustomerUName"] != DBNull.Value)
                eventHistory.ACustomerUName = reader["ACustomerUName"].ToString();
            if (columns.Contains("CustomerFullName") && reader["CustomerFullName"] != DBNull.Value)
                eventHistory.CustomerFullName = reader["CustomerFullName"].ToString();

            if (columns.Contains("ACustomerFullName") && reader["ACustomerFullName"] != DBNull.Value)
                eventHistory.ACustomerFullName = reader["ACustomerFullName"].ToString();


            


            if (eventHistory.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
            {
                eventHistory.IncidentDescription = CustomEvent.IncidentActionStatus.Dispatch.ToString() + " To " + eventHistory.UserName;
                eventHistory.IncidentStatus = CustomEvent.IncidentActionStatus.Dispatch.ToString();
            }
            else if (eventHistory.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
            {
                eventHistory.IncidentDescription = "Created By " + eventHistory.CreatedBy;
                eventHistory.IncidentStatus = CustomEvent.IncidentActionStatus.Pending.ToString();
            }
            else if (eventHistory.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
            {
                eventHistory.IncidentDescription = "Park By " + eventHistory.UserName;
                eventHistory.IncidentStatus = CustomEvent.IncidentActionStatus.Park.ToString();
            }
            else if (eventHistory.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
            {
                eventHistory.IncidentDescription = "Engage By " + eventHistory.UserName;
                eventHistory.IncidentStatus = CustomEvent.IncidentActionStatus.Engage.ToString();
            }
            else if (eventHistory.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
            {
                eventHistory.IncidentDescription = "Release By " + eventHistory.UserName;
                eventHistory.IncidentStatus = CustomEvent.IncidentActionStatus.Release.ToString();
            }
            else if (eventHistory.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
            {
                eventHistory.IncidentDescription = "Complete By " + eventHistory.UserName;
                eventHistory.IncidentStatus = CustomEvent.IncidentActionStatus.Complete.ToString();
            }
            else if (eventHistory.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
            {
                eventHistory.IncidentDescription = "Resolve By " + eventHistory.UserName;
                eventHistory.IncidentStatus = CustomEvent.IncidentActionStatus.Resolve.ToString();
            }
            else if (eventHistory.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
            {
                eventHistory.IncidentDescription = "Reject By " + eventHistory.UserName;
                eventHistory.IncidentStatus = CustomEvent.IncidentActionStatus.Reject.ToString();
            }

            return eventHistory;
        }
        public static List<EventHistory> GetEventHistoryByEventId(int eventId, string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetEventHistoryByEventId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = eventId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetEventHistoryByEventId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetEventHistoryFromSqlReader(reader, dbConnection);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static List<EventHistory> GetEventHistoryByUser(string name, string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetEventHistoryByUser", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetEventHistoryByUser";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetEventHistoryFromSqlReader(reader, dbConnection);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static List<EventHistory> GetEventHistoryByUsername(string name, string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetEventHistoryByUsername", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetEventHistoryByUsername";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetEventHistoryFromSqlReader(reader, dbConnection);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static bool InsertEventHistory(EventHistory eventHistory, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (eventHistory.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (eventHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertEventHistory", connection);

                    cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                    cmd.Parameters["@EventId"].Value = eventHistory.EventId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = eventHistory.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Action", SqlDbType.Int));
                    cmd.Parameters["@Action"].Value = eventHistory.IncidentAction;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longitude"].Value = eventHistory.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = eventHistory.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = eventHistory.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = eventHistory.CustomerId;

                    

                    cmd.CommandText = "InsertEventHistory";
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new Arrowlabs.Mims.Audit.Models.EventHistoryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, eventHistory.CreatedBy);
                            Reflection.CopyProperties(eventHistory, audit);
                            EventHistoryAudit.InsertEventHistory(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertEventHistory", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static bool InsertEventHistory(EventHistory eventHistory, string dbConnection)
        {
            int id = 0;

           
            if (eventHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertEventHistory", connection);

                    cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                    cmd.Parameters["@EventId"].Value = eventHistory.EventId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = eventHistory.UserName;

                    cmd.Parameters.Add(new SqlParameter("@Action", SqlDbType.Int));
                    cmd.Parameters["@Action"].Value = eventHistory.IncidentAction;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longitude"].Value = eventHistory.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = eventHistory.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = eventHistory.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;

                    cmd.CommandText = "InsertEventHistory";
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                }
            }
            return true;
        }

        public static List<EventHistory> GetTopEventHistoryBySiteId(int siteId,string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTopEventHistoryBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTopEventHistoryBySiteId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetEventHistoryFromSqlReader(reader, dbConnection);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }


        public static List<EventHistory> GetTopEventHistoryByLevel5(int siteId, string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTopEventHistoryByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTopEventHistoryByLevel5";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetEventHistoryFromSqlReader(reader, dbConnection);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static List<EventHistory> GetTopEventHistoryByCId(int siteId, string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTopEventHistoryByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTopEventHistoryByCId";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetEventHistoryFromSqlReader(reader, dbConnection);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static List<EventHistory> GetTopEventHistory(string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTopEventHistory", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetTopEventHistory";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var eventHistory = GetEventHistoryFromSqlReader(reader, dbConnection);
                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static List<EventHistory> Top10RecentActivityofVerifierAndCustomEvent(int eventtype, int requesttype, string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("RecentActivityofVerifierAndCustomEvent", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                sqlCommand.Parameters["@EventType"].Value = eventtype;
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = requesttype;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "RecentActivityofVerifierAndCustomEvent";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList(); 
                while (reader.Read())
                {
                    var eventHistory = new EventHistory();

                    if (reader["EventId"] != DBNull.Value)
                        eventHistory.EventId = Convert.ToInt32(reader["EventId"].ToString());
                    if (reader["Username"] != DBNull.Value)
                        eventHistory.UserName = reader["Username"].ToString();
                    if (reader["RecevieTime"] != DBNull.Value)
                        eventHistory.CreatedDate = Convert.ToDateTime(reader["RecevieTime"].ToString());
                    if (reader["Longtitude"] != DBNull.Value)
                        eventHistory.Longtitude = reader["Longtitude"].ToString();
                    if (reader["Latitude"] != DBNull.Value)
                        eventHistory.Latitude = reader["Latitude"].ToString();
                    if (reader["EventType"] != DBNull.Value)
                        eventHistory.Id = Convert.ToInt32(reader["EventType"].ToString());

                    if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        eventHistory.CustomerUName = reader["CustomerUName"].ToString();

                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }
        public static List<EventHistory> Top10RecentActivityofVerifierAndCustomEventByLevel5(int eventtype, int requesttype, int siteid, string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("Top10RecentActivityofVerifierAndCustomEventByLevel5", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                sqlCommand.Parameters["@EventType"].Value = eventtype;
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = requesttype;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Top10RecentActivityofVerifierAndCustomEventByLevel5";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var eventHistory = new EventHistory();

                    if (reader["EventId"] != DBNull.Value)
                        eventHistory.EventId = Convert.ToInt32(reader["EventId"].ToString());
                    if (reader["Username"] != DBNull.Value)
                        eventHistory.UserName = reader["Username"].ToString();
                    if (reader["RecevieTime"] != DBNull.Value)
                        eventHistory.CreatedDate = Convert.ToDateTime(reader["RecevieTime"].ToString());
                    if (reader["Longtitude"] != DBNull.Value)
                        eventHistory.Longtitude = reader["Longtitude"].ToString();
                    if (reader["Latitude"] != DBNull.Value)
                        eventHistory.Latitude = reader["Latitude"].ToString();
                    if (reader["EventType"] != DBNull.Value)
                        eventHistory.Id = Convert.ToInt32(reader["EventType"].ToString());

                    if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        eventHistory.CustomerUName = reader["CustomerUName"].ToString();

                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static List<EventHistory> Top10RecentActivityofVerifierAndCustomEventBySiteId(int eventtype, int requesttype,int siteid ,string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("Top10RecentActivityofVerifierAndCustomEventBySiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                sqlCommand.Parameters["@EventType"].Value = eventtype;
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = requesttype;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Top10RecentActivityofVerifierAndCustomEventBySiteId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var eventHistory = new EventHistory();

                    if (reader["EventId"] != DBNull.Value)
                        eventHistory.EventId = Convert.ToInt32(reader["EventId"].ToString());
                    if (reader["Username"] != DBNull.Value)
                        eventHistory.UserName = reader["Username"].ToString();
                    if (reader["RecevieTime"] != DBNull.Value)
                        eventHistory.CreatedDate = Convert.ToDateTime(reader["RecevieTime"].ToString());
                    if (reader["Longtitude"] != DBNull.Value)
                        eventHistory.Longtitude = reader["Longtitude"].ToString();
                    if (reader["Latitude"] != DBNull.Value)
                        eventHistory.Latitude = reader["Latitude"].ToString();
                    if (reader["EventType"] != DBNull.Value)
                        eventHistory.Id = Convert.ToInt32(reader["EventType"].ToString());

                    if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        eventHistory.CustomerUName = reader["CustomerUName"].ToString();

                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }

        public static List<EventHistory> Top10RecentActivityofVerifierAndCustomEventByCId(int eventtype, int requesttype, int siteid, string dbConnection)
        {
            var eventHistories = new List<EventHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("Top10RecentActivityofVerifierAndCustomEventByCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@EventType", SqlDbType.Int));
                sqlCommand.Parameters["@EventType"].Value = eventtype;
                sqlCommand.Parameters.Add(new SqlParameter("@Request", SqlDbType.Int));
                sqlCommand.Parameters["@Request"].Value = requesttype;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Top10RecentActivityofVerifierAndCustomEventByCId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var eventHistory = new EventHistory();

                    if (reader["EventId"] != DBNull.Value)
                        eventHistory.EventId = Convert.ToInt32(reader["EventId"].ToString());
                    if (reader["Username"] != DBNull.Value)
                        eventHistory.UserName = reader["Username"].ToString();
                    if (reader["RecevieTime"] != DBNull.Value)
                        eventHistory.CreatedDate = Convert.ToDateTime(reader["RecevieTime"].ToString());
                    if (reader["Longtitude"] != DBNull.Value)
                        eventHistory.Longtitude = reader["Longtitude"].ToString();
                    if (reader["Latitude"] != DBNull.Value)
                        eventHistory.Latitude = reader["Latitude"].ToString();
                    if (reader["EventType"] != DBNull.Value)
                        eventHistory.Id = Convert.ToInt32(reader["EventType"].ToString());

                    if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                        eventHistory.CustomerUName = reader["CustomerUName"].ToString();

                    eventHistories.Add(eventHistory);
                }
                connection.Close();
                reader.Close();
            }
            return eventHistories;
        }
    }
}
