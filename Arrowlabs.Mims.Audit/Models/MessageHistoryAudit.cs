﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class MessageHistoryAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public bool IsRead { get; set; }
        public int CustomerId { get; set; }
        public int NotificationId { get; set; }
        public int SiteId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public static bool InsertMessageHistoryAudit(MessageHistoryAudit messageHistoryAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(messageHistoryAudit.BaseAudit, dbConnection);

            if (messageHistoryAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMessageHistoryAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = messageHistoryAudit.Id;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = messageHistoryAudit.CreatedDate;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = messageHistoryAudit.CustomerId;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = messageHistoryAudit.SiteId;
                    cmd.Parameters.Add("@NotificationId", SqlDbType.Int).Value = messageHistoryAudit.NotificationId;
                    cmd.Parameters.Add("@IsRead", SqlDbType.Bit).Value = messageHistoryAudit.IsRead;
                  
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = messageHistoryAudit.CreatedBy;
           
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                }
                return true;
            }
            return false;
        }
 

    }
}
