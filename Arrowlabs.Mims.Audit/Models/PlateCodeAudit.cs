﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class PlateCodeAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int ID { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
        public string Description_AR { get; set; }
        public string PlateSource { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        
        private static PlateCodeAudit GetPlateCodeAuditFromSqlReader(SqlDataReader reader)
        {
            var plateCode = new PlateCodeAudit();
            plateCode.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                plateCode.ID = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("CODE") && reader["CODE"] != DBNull.Value)
                plateCode.Code = Convert.ToInt32(reader["CODE"].ToString());
            if (columns.Contains("DESCRIPTION") && reader["DESCRIPTION"] != DBNull.Value)
                plateCode.Description = reader["DESCRIPTION"].ToString();
            if (columns.Contains("DESCRIPTION_AR") && reader["DESCRIPTION_AR"] != DBNull.Value)
                plateCode.Description_AR = reader["DESCRIPTION_AR"].ToString();
            if (columns.Contains("PLATESOURCE") && reader["PLATESOURCE"] != DBNull.Value)
                plateCode.PlateSource = reader["PLATESOURCE"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                plateCode.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                plateCode.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return plateCode;
        }
        public static List<PlateCodeAudit> GetAllPlateCodeAudit(string dbConnection)
        {
            var plateCodes = new List<PlateCodeAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllPlateCodeAudit", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var plateCode = GetPlateCodeAuditFromSqlReader(reader);
                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }
        public static bool InsertPlateCodeAudit(PlateCodeAudit plateCode, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(plateCode.BaseAudit, dbConnection);

            if (plateCode != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertPlateCodeAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = plateCode.ID;

                    cmd.Parameters.Add("@CODE", SqlDbType.Int).Value = plateCode.Code;

                    cmd.Parameters.Add("@PLATESOURCE", SqlDbType.NVarChar).Value = plateCode.PlateSource;

                    cmd.Parameters.Add("@DESCRIPTION", SqlDbType.NVarChar).Value = plateCode.Description;

                    cmd.Parameters.Add("@DESCRIPTION_AR", SqlDbType.NVarChar).Value = plateCode.Description_AR;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = plateCode.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = plateCode.CreatedBy;

                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = plateCode.SiteId;

                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = plateCode.CustomerId; 

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
