﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
   public class ItemInquiryAudit
   {
       public BaseAudit BaseAudit { get; set; }
       public int Id { get; set; }
       public DateTime DateFound { get; set; }
       public string LocationLost { get; set; }
       public string Type { get; set; }
       public string ItemBrand { get; set; }
       public string ItemColour { get; set; }
       public string ItemDescription { get; set; }
       public string Name { get; set; }
       public string Address { get; set; }
       public string ContactNo { get; set; }
       public string RoomNumber { get; set; }
       public string SecurityTracking { get; set; }
       public bool Found { get; set; }
       public bool Delivered { get; set; }
       public bool Closed { get; set; }
       public DateTime DeliveredDate { get; set; }
       public int ItemFoundId { get; set; }
       public string ItemReference { get; set; }
       public int SiteId { get; set; }
       public DateTime CreatedDate { get; set; }
       public DateTime UpdatedDate { get; set; }
       public string CreatedBy { get; set; }
       public string UpdatedBy { get; set; }

       public string ReferenceNo { get; set; }
       public string SubType { get; set; }

       public int CustomerId { get; set; }

       public static void InsertorUpdateItemInquiryAudit(ItemInquiryAudit itemInquiry, string auditDbConnection)
       {
           if (itemInquiry == null)
               return;
           var baseAuditId = BaseAudit.InsertBaseAudit(itemInquiry.BaseAudit, auditDbConnection);
           using (var connection = new SqlConnection(auditDbConnection))
           {
               connection.Open();
               var cmd = new SqlCommand("InsertorUpdateItemInquiry", connection);
               cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
               cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
               cmd.Parameters["@Id"].Value = itemInquiry.Id;
               cmd.Parameters.Add(new SqlParameter("@DateFound", SqlDbType.DateTime));
               cmd.Parameters["@DateFound"].Value = itemInquiry.DateFound;
               cmd.Parameters.Add(new SqlParameter("@LocationLost", SqlDbType.NVarChar));
               cmd.Parameters["@LocationLost"].Value = itemInquiry.LocationLost;
               cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.NVarChar));
               cmd.Parameters["@Type"].Value = itemInquiry.Type;
               cmd.Parameters.Add(new SqlParameter("@ItemBrand", SqlDbType.NVarChar));
               cmd.Parameters["@ItemBrand"].Value = itemInquiry.ItemBrand;
               cmd.Parameters.Add(new SqlParameter("@ItemColour", SqlDbType.NVarChar));
               cmd.Parameters["@ItemColour"].Value = itemInquiry.ItemColour;
               cmd.Parameters.Add(new SqlParameter("@ItemDescription", SqlDbType.NVarChar));
               cmd.Parameters["@ItemDescription"].Value = itemInquiry.ItemDescription;
               cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
               cmd.Parameters["@Name"].Value = itemInquiry.Name;
               cmd.Parameters.Add(new SqlParameter("@Address", SqlDbType.NVarChar));
               cmd.Parameters["@Address"].Value = itemInquiry.Address;
               cmd.Parameters.Add(new SqlParameter("@ContactNo", SqlDbType.NVarChar));
               cmd.Parameters["@ContactNo"].Value = itemInquiry.ContactNo;
               cmd.Parameters.Add(new SqlParameter("@RoomNumber", SqlDbType.NVarChar));
               cmd.Parameters["@RoomNumber"].Value = itemInquiry.RoomNumber;
               cmd.Parameters.Add(new SqlParameter("@SecurityTracking", SqlDbType.NVarChar));
               cmd.Parameters["@SecurityTracking"].Value = itemInquiry.SecurityTracking;
               cmd.Parameters.Add(new SqlParameter("@Found", SqlDbType.Bit));
               cmd.Parameters["@Found"].Value = itemInquiry.Found;
               cmd.Parameters.Add(new SqlParameter("@Delivered", SqlDbType.Bit));
               cmd.Parameters["@Delivered"].Value = itemInquiry.Delivered;
               cmd.Parameters.Add(new SqlParameter("@Closed", SqlDbType.Bit));
               cmd.Parameters["@Closed"].Value = itemInquiry.Closed;
               cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
               cmd.Parameters["@ItemFoundId"].Value = itemInquiry.ItemFoundId;
               cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
               cmd.Parameters["@SiteId"].Value = itemInquiry.SiteId;
               cmd.Parameters.Add(new SqlParameter("@ItemReference", SqlDbType.NVarChar));
               cmd.Parameters["@ItemReference"].Value = itemInquiry.ItemReference;
               cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
               cmd.Parameters["@CreatedDate"].Value = itemInquiry.CreatedDate;
               cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
               cmd.Parameters["@UpdatedDate"].Value = itemInquiry.UpdatedDate;
               cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
               cmd.Parameters["@CreatedBy"].Value = itemInquiry.CreatedBy;
               cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
               cmd.Parameters["@UpdatedBy"].Value = itemInquiry.UpdatedBy;

               cmd.Parameters.Add(new SqlParameter("@ReferenceNo", SqlDbType.NVarChar));
               cmd.Parameters["@ReferenceNo"].Value = itemInquiry.ReferenceNo;

               cmd.Parameters.Add(new SqlParameter("@SubType", SqlDbType.NVarChar));
               cmd.Parameters["@SubType"].Value = itemInquiry.SubType;

               cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
               cmd.Parameters["@CustomerId"].Value = itemInquiry.CustomerId;

               cmd.CommandType = CommandType.StoredProcedure;

               cmd.ExecuteNonQuery();
           }
       }
   }
}
