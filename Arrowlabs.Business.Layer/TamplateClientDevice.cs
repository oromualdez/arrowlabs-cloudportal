﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class TamplateClientDevice
    {
        public string MacAddress { get; set; }
        public string PcName { get; set; }
        public string RtspURL { get; set; }
    }
}
