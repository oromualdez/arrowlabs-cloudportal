﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Arrowlabs.Business.Layer;
using System.IO;
using System.Text;
using System.Configuration;

namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for EnrollmentHandler
    /// </summary>
    public class EnrollmentHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Expires = -1;

            foreach (string file in context.Request.Files)
            {
                HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
                string FileName = string.Empty;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                {
                    string[] files = hpf.FileName.Split(new char[] { '\\' });
                    FileName = files[files.Length - 1];
                }
                else
                {
                    FileName = hpf.FileName;
                }
                if (hpf.ContentLength == 0)
                    continue;

                string savedFileName = context.Server.MapPath("~/Uploads/" + FileName);
                hpf.SaveAs(savedFileName);

                context.Response.Write(savedFileName);
                context.Response.StatusCode = 200;
            }
        }
        string dbConnection = ConfigurationManager.ConnectionStrings["ArrowLabsPortal"].ConnectionString;
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}