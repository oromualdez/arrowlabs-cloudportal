﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class TamplateHotEvent
    {
        public Guid Identifier { get; set; }
        public string CameraName { get; set; }
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
        public string UserName { get; set; }
        public string PcName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int EventType { get; set; }
        public int SiteId { get; set; }

    }
}

