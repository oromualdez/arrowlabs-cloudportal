﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Arrowlabs.Mims.Audit.Models
{
    public enum TaskStatus
    {
        // Swapped value of completed and accepted
        Pending = 0,
        InProgress = 1,
        Completed = 2,
        Accepted = 3,
        Rejected = 4,
        RejectedSaved = 5,
        Cancelled = 6,
        OnRoute = 7
    }
    public enum TaskPiority
    {
        High = 0,
        Medium = 1,
        Low = 2
    }
    public enum TaskAssigneeType
    {
        User = 0,
        Location = 1,
        Group = 2
    }
    public class TaskAudit
    { 
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int ManagerId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int SubmittedBy { get; set; }
        public string CompletedTask { get; set; }
        public string Notes { get; set; }
        public int Status { get; set; }
        public string ManagerName { get; set; }
        public string prefixname { get; set; }
        public string Description { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int Priority { get; set; }
        public string StatusDescription { get; set; }
        public bool IsTaskTemplate { get; set; }
        public bool IsTaskFavorite { get; set; }
        public bool IsDeleted { get; set; }
        public int AssigneeType { get; set; }
        public int AssigneeId { get; set; }
        public int UserId { get; set; }
        public string AssigneeName { get; set; }
        public int TemplateCheckListId { get; set; }
        public string CheckListNotes { get; set; }
        public string RejectionNotes { get; set; }
        public float StartLongitude { get; set; }
        public float StartLatitude { get; set; }
        public float EndLongitude { get; set; }
        public float EndLatitude { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public string imageURL { get; set; }
        public List<TaskCheckListAudit> TaskCheckListItems { get; set; }
        public int SiteId { get; set; }
        public int SysType { get; set; }
        public int CustId { get; set; }
        public int ContractId { get; set; }
        public int TaskLinkId { get; set; }
        public int ProjectId { get; set; }
        public string NewCustomerTaskId { get; set; }

        public bool FollowUp { get; set; }
        public bool IsSignature { get; set; }

        public int CustomerId { get; set; }


        public float OnRouteLongitude { get; set; }
        public float OnRouteLatitude { get; set; }
        public DateTime? ActualOnRouteDate { get; set; }

        public static List<string> GetStatusList()
        {
            return Enum.GetNames(typeof(TaskStatus)).ToList();
        }

        public static List<string> GetPriorityList()
        {
            return Enum.GetNames(typeof(TaskPiority)).ToList();
        }

        public static List<string> GetPriorityImagesList()
        {
            return new List<string>() { 
                "<img height='8px' style='padding-right:5px;' width='8px' src='Images/red_circle.png'/>", 
                "<img height='8px' style='padding-right:5px;' width='8px' src='Images/1413463880_Circle_Orange.png'/>",
                "<img height='8px' style='padding-right:5px;' width='8px' src='Images/1413463946_Circle_Green.png'/>" };
        }

        public static List<string> GetAssigneeTypeList()
        {
            return Enum.GetNames(typeof(TaskAssigneeType)).ToList();
        }

        public static TaskAudit GetTaskAuditFromSqlReader(SqlDataReader reader)
        {
            var taskAudit = new TaskAudit();
            taskAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                taskAudit.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                taskAudit.Name = reader["Name"].ToString();
            if (columns.Contains("CreateDate") && reader["CreateDate"] != DBNull.Value)
                taskAudit.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                taskAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("ManagerId") && reader["ManagerId"] != DBNull.Value)
                taskAudit.ManagerId = Convert.ToInt32(reader["ManagerId"].ToString());
            if (columns.Contains("StartDate") && reader["StartDate"] != DBNull.Value)
                taskAudit.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());
            if (columns.Contains("EndDate") && reader["EndDate"] != DBNull.Value)
                taskAudit.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());
            if (columns.Contains("Notes") && reader["Notes"] != DBNull.Value)
                taskAudit.Notes = reader["Notes"].ToString();
            if (columns.Contains("Description") && reader["Description"] != DBNull.Value)
                taskAudit.Description = reader["Description"].ToString();
            if (columns.Contains("Status") && reader["Status"] != DBNull.Value)
                taskAudit.Status = Convert.ToInt32(reader["Status"].ToString());
            if (columns.Contains("AssigneeType") && reader["AssigneeType"] != DBNull.Value)
                taskAudit.AssigneeType = Convert.ToInt32(reader["AssigneeType"].ToString());
            if (columns.Contains("AssigneeId") && reader["AssigneeId"] != DBNull.Value)
                taskAudit.AssigneeId = Convert.ToInt32(reader["AssigneeId"].ToString());
            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                taskAudit.Longitude = Convert.ToDouble(reader["Longitude"].ToString());
            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                taskAudit.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
            if (columns.Contains("Priority") && reader["Priority"] != DBNull.Value)
                taskAudit.Priority = Convert.ToInt32(reader["Priority"].ToString());
            if (columns.Contains("CheckListNotes") && reader["CheckListNotes"] != DBNull.Value)
                taskAudit.CheckListNotes = reader["CheckListNotes"].ToString();
            if (columns.Contains("TemplateCheckListId") && reader["TemplateCheckListId"] != DBNull.Value)
                taskAudit.TemplateCheckListId = (!String.IsNullOrEmpty(reader["TemplateCheckListId"].ToString())) ? Convert.ToInt32(reader["TemplateCheckListId"].ToString()) : 0;
            if (columns.Contains("IsTaskTemplate") && reader["IsTaskTemplate"] != DBNull.Value)
                taskAudit.IsTaskTemplate = Convert.ToBoolean(reader["IsTaskTemplate"].ToString() == "1" ? "true" : "false");
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                taskAudit.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString() == "1" ? "true" : "false");
            if (columns.Contains("SubmittedBy") && reader["SubmittedBy"] != DBNull.Value)
                taskAudit.SubmittedBy = (!String.IsNullOrEmpty(reader["SubmittedBy"].ToString())) ? Convert.ToInt32(reader["SubmittedBy"].ToString()) : 0;
            if (columns.Contains("StartLatitude") && reader["StartLatitude"] != DBNull.Value)
                taskAudit.StartLatitude = (!String.IsNullOrEmpty(reader["StartLatitude"].ToString())) ? Convert.ToSingle(reader["StartLatitude"].ToString()) : 0f;
            if (columns.Contains("StartLongitude") && reader["StartLongitude"] != DBNull.Value)
                taskAudit.StartLongitude = (!String.IsNullOrEmpty(reader["StartLongitude"].ToString())) ? Convert.ToSingle(reader["StartLongitude"].ToString()) : 0f;
            if (columns.Contains("EndLatitude") && reader["EndLatitude"] != DBNull.Value)
                taskAudit.EndLatitude = (!String.IsNullOrEmpty(reader["EndLatitude"].ToString())) ? Convert.ToSingle(reader["EndLatitude"].ToString()) : 0f;
            if (columns.Contains("EndLongitude") && reader["EndLongitude"] != DBNull.Value)
                taskAudit.EndLongitude = (!String.IsNullOrEmpty(reader["EndLongitude"].ToString())) ? Convert.ToSingle(reader["EndLongitude"].ToString()) : 0f;
            if (columns.Contains("ActualStartDate") && reader["ActualStartDate"] != DBNull.Value)
                taskAudit.ActualStartDate = Convert.ToDateTime(reader["ActualStartDate"].ToString());
            if (columns.Contains("ActualEndDate") && reader["ActualEndDate"] != DBNull.Value)
                taskAudit.ActualEndDate = Convert.ToDateTime(reader["ActualEndDate"].ToString());
            if (columns.Contains("RejectionNotes") && reader["RejectionNotes"] != DBNull.Value)
                taskAudit.RejectionNotes = reader["RejectionNotes"].ToString();
            taskAudit.StatusDescription = GetStatusList()[taskAudit.Status];
            if (columns.Contains("ManagerName") && reader["ManagerName"] != DBNull.Value)
                taskAudit.ManagerName = reader["ManagerName"].ToString();
            if (columns.Contains("AssigneeName") && reader["AssigneeName"] != DBNull.Value)
                taskAudit.AssigneeName = reader["AssigneeName"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                taskAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                taskAudit.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
            
            return taskAudit;
        }
        public static List<TaskAudit> GetAllTaskAudit(string dbConnection)
        {
            var userTasks = new List<TaskAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllTaskAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    var userTask = GetTaskAuditFromSqlReader(resultSet);
                    userTasks.Add(userTask);
                }
                connection.Close();
                resultSet.Close();
            }
            return userTasks;
        }
        public static int InsertTaskAudit(TaskAudit taskAudit, string dbConnection)
        {
            int id = 0;
            if (taskAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(taskAudit.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = taskAudit.Id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = taskAudit.Name;
                    cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = taskAudit.CreateDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = taskAudit.CreatedBy;
                    cmd.Parameters.Add("@ManagerId", SqlDbType.Int).Value = taskAudit.ManagerId;
                    cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = taskAudit.StartDate;
                    cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = taskAudit.EndDate;
                    cmd.Parameters.Add("@Notes", SqlDbType.NVarChar).Value = taskAudit.Notes;
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = taskAudit.Description;
                    cmd.Parameters.Add("@CheckListNotes", SqlDbType.NVarChar).Value = taskAudit.CheckListNotes;
                    cmd.Parameters.Add("@TemplateCheckListId", SqlDbType.Int).Value = taskAudit.TemplateCheckListId;
                    cmd.Parameters.Add("@Status", SqlDbType.Int).Value = taskAudit.Status;
                    cmd.Parameters.Add("@AssigneeType", SqlDbType.Int).Value = taskAudit.AssigneeType;
                    cmd.Parameters.Add("@AssigneeId", SqlDbType.Int).Value = taskAudit.AssigneeId;
                    cmd.Parameters.Add("@Longitude", SqlDbType.Float).Value = taskAudit.Longitude;
                    cmd.Parameters.Add("@Latitude", SqlDbType.Float).Value = taskAudit.Latitude;
                    cmd.Parameters.Add("@Priority", SqlDbType.Int).Value = taskAudit.Priority;
                    cmd.Parameters.Add("@IsTaskTemplate", SqlDbType.TinyInt).Value = taskAudit.IsTaskTemplate;
                    cmd.Parameters.Add("@IsDeleted", SqlDbType.TinyInt).Value = taskAudit.IsDeleted;
                    cmd.Parameters.Add("@IsSignature", SqlDbType.TinyInt).Value = taskAudit.IsSignature;
                    cmd.Parameters.Add("@FollowUp", SqlDbType.TinyInt).Value = taskAudit.FollowUp;

                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = taskAudit.CustomerId;
                    if (taskAudit.IsTaskFavorite != null)
                    {
                        cmd.Parameters.Add("@IsTaskFavorite", SqlDbType.TinyInt).Value = taskAudit.IsTaskFavorite;
                    }
                    else
                    {
                        cmd.Parameters.Add("@IsTaskFavorite", SqlDbType.TinyInt).Value = false;
                    }

                    cmd.Parameters.Add("@StartLongitude", SqlDbType.Float).Value = taskAudit.StartLongitude;
                    cmd.Parameters.Add("@StartLatitude", SqlDbType.Float).Value = taskAudit.StartLatitude;
                    cmd.Parameters.Add("@EndLongitude", SqlDbType.Float).Value = taskAudit.EndLongitude;
                    cmd.Parameters.Add("@EndLatitude", SqlDbType.Float).Value = taskAudit.EndLatitude;
                    cmd.Parameters.Add("@RejectionNotes", SqlDbType.NVarChar).Value = taskAudit.RejectionNotes;

                    cmd.Parameters.Add(new SqlParameter("@ActualOnRouteDate", SqlDbType.DateTime));
                    cmd.Parameters["@ActualOnRouteDate"].Value = taskAudit.ActualOnRouteDate;

                    cmd.Parameters.Add(new SqlParameter("@OnRouteLatitude", SqlDbType.Float));
                    cmd.Parameters["@OnRouteLatitude"].Value = taskAudit.OnRouteLatitude;

                    cmd.Parameters.Add(new SqlParameter("@OnRouteLongitude", SqlDbType.Float));
                    cmd.Parameters["@OnRouteLongitude"].Value = taskAudit.OnRouteLongitude;


                    cmd.Parameters.Add("@ActualEndDate", SqlDbType.DateTime).Value = taskAudit.ActualEndDate;
                    cmd.Parameters.Add("@ActualStartDate", SqlDbType.DateTime).Value = taskAudit.ActualStartDate;
                    cmd.Parameters.Add("@SubmittedBy", SqlDbType.Int).Value = taskAudit.SubmittedBy;
                    cmd.Parameters.Add("@CustId", SqlDbType.Int).Value = taskAudit.CustId;
                    cmd.Parameters.Add("@SysType", SqlDbType.Int).Value = taskAudit.SysType;
                    cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = taskAudit.ProjectId;
                    cmd.Parameters.Add("@CustomerTaskId", SqlDbType.NVarChar).Value = taskAudit.NewCustomerTaskId;
                    cmd.Parameters.Add("@TaskLinkId", SqlDbType.Int).Value = taskAudit.TaskLinkId; 

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }

     

    }
}
