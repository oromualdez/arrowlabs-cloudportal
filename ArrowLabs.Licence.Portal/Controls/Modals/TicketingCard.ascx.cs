﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArrowLabs.Licence.Portal.Controls.Modals
{
    public partial class TicketingCard : System.Web.UI.UserControl
    {
        protected string tskOptionView;
        protected string isCustomerUser;
        protected void Page_Load(object sender, EventArgs e)
        {
            tskOptionView = "none";
            isCustomerUser = "style='display:none;'";
            var dbConnection = CommonUtility.dbConnection;
            var user = HttpContext.Current.User.Identity;
            if (user != null)
            {
                var userinfo = Users.GetUserByName(user.Name, dbConnection);
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    {
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks)
                        {
                            tskOptionView = "block";
                        }
                    }
                    if(userinfo.RoleId != (int)Role.CustomerUser)
                    {
                        isCustomerUser = string.Empty;
                    }
                    else
                    {
                        tskOptionView = "none";
                    }
                }
            }

        } 
    }
}