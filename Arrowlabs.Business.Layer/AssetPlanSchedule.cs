﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class AssetPlanSchedule
    {
        public int Id { get; set; }

        public int TaskId { get; set; }

        public int PlanId { get; set; }

        public bool isSent { get; set; }

        public DateTime? PlanDateTime { get; set; }

        public DateTime? SentDateTime { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }

        public int AssetId { get; set; }
        
        public static int InsertOrUpdateAssetPlanSchedule(AssetPlanSchedule taskRemarks, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = taskRemarks.Id;

            var action = (taskRemarks.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (taskRemarks != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateAssetPlanSchedule", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskRemarks.Id;

                    cmd.Parameters.Add(new SqlParameter("@AssetId", SqlDbType.Int));
                    cmd.Parameters["@AssetId"].Value = taskRemarks.AssetId;

                    cmd.Parameters.Add(new SqlParameter("@PlanId", SqlDbType.Int));
                    cmd.Parameters["@PlanId"].Value = taskRemarks.PlanId;

                    cmd.Parameters.Add(new SqlParameter("@IsSent", SqlDbType.Bit));
                    cmd.Parameters["@IsSent"].Value = taskRemarks.isSent;

                    cmd.Parameters.Add(new SqlParameter("@PlanDateTime", SqlDbType.DateTime));
                    cmd.Parameters["@PlanDateTime"].Value = taskRemarks.PlanDateTime;

                    cmd.Parameters.Add(new SqlParameter("@SentDateTime", SqlDbType.DateTime));
                    cmd.Parameters["@SentDateTime"].Value = taskRemarks.SentDateTime;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = taskRemarks.CreatedDate;
                     
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = taskRemarks.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = taskRemarks.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = taskRemarks.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateAssetPlanSchedule";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            taskRemarks.Id = id;
                            var audit = new AssetPlanScheduleAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, taskRemarks.CreatedBy);
                            Reflection.CopyProperties(taskRemarks, audit);
                            AssetPlanScheduleAudit.InsertAssetPlanScheduleAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertAssetPlanScheduleAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static AssetPlanSchedule GetAssetPlanScheduleFromSqlReader(SqlDataReader resultSet)
        {
            var userTask = new AssetPlanSchedule();
            var columns = Enumerable.Range(0, resultSet.FieldCount).Select(resultSet.GetName).ToList();
            if (resultSet["Id"] != DBNull.Value)
                userTask.Id = Convert.ToInt32(resultSet["Id"].ToString());
             
            if (resultSet["CreatedBy"] != DBNull.Value)
                userTask.CreatedBy = resultSet["CreatedBy"].ToString();

            if (resultSet["CreatedDate"] != DBNull.Value)
                userTask.CreatedDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());

            if (resultSet["SentDateTime"] != DBNull.Value)
                userTask.SentDateTime = Convert.ToDateTime(resultSet["SentDateTime"].ToString());

            if (resultSet["PlanDateTime"] != DBNull.Value)
                userTask.PlanDateTime = Convert.ToDateTime(resultSet["PlanDateTime"].ToString());

            if (resultSet["isSent"] != DBNull.Value)
                userTask.isSent = Convert.ToBoolean(resultSet["isSent"].ToString());
            if (resultSet["PlanId"] != DBNull.Value)
                userTask.PlanId = Convert.ToInt32(resultSet["PlanId"].ToString());
            if (resultSet["TaskId"] != DBNull.Value)
                userTask.TaskId = Convert.ToInt32(resultSet["TaskId"].ToString());
            if (resultSet["AssetId"] != DBNull.Value)
                userTask.AssetId = Convert.ToInt32(resultSet["AssetId"].ToString());

            if (resultSet["SiteId"] != DBNull.Value)
                userTask.SiteId = Convert.ToInt32(resultSet["SiteId"].ToString());

            if (resultSet["CustomerId"] != DBNull.Value)
                userTask.CustomerId = Convert.ToInt32(resultSet["CustomerId"].ToString());

            return userTask;
        }

        //GetAssetPlanScheduleById
        //GetAllAssetPlanScheduleBySiteId
        //GetAllAssetPlanScheduleByCId
        //GetAllAssetPlanScheduleByLevel5
        //GetAllAssetPlanScheduleByTaskId
        //GetAllAssetPlanScheduleByPlanId
        //GetAllAssetPlanSchedule
        //DeleteAssetPlanScheduleById

        public static List<AssetPlanSchedule> GetAllAssetPlanScheduleBySiteId(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlanSchedule>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanScheduleBySiteId";
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanScheduleFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlanSchedule> GetAllAssetPlanScheduleByCId(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlanSchedule>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanScheduleByCId";
                sqlCommand.Parameters.Add(new SqlParameter("@CId", SqlDbType.Int));
                sqlCommand.Parameters["@CId"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanScheduleFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlanSchedule> GetAllAssetPlanScheduleByLevel5(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlanSchedule>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanScheduleByLevel5";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanScheduleFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlanSchedule> GetAllAssetPlanScheduleByTaskId(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlanSchedule>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanScheduleByTaskId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanScheduleFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static List<AssetPlanSchedule> GetAllAssetPlanScheduleByPlanId(int siteId, string dbConnection)
        {
            var customEvents = new List<AssetPlanSchedule>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanScheduleByPlanId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanScheduleFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }


        public static AssetPlanSchedule GetAssetPlanScheduleById(int siteId, string dbConnection)
        {
            var customEvent = new AssetPlanSchedule();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAssetPlanScheduleById";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                     customEvent = GetAssetPlanScheduleFromSqlReader(reader);
          
                }
                connection.Close();
                reader.Close();
            }
            return customEvent;
        }
         
        public static List<AssetPlanSchedule> GetAllAssetPlanSchedule(string dbConnection)
        {
            var customEvents = new List<AssetPlanSchedule>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllAssetPlanSchedule";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customEvent = GetAssetPlanScheduleFromSqlReader(reader);
                    customEvents.Add(customEvent);
                }
                connection.Close();
                reader.Close();
            }
            return customEvents;
        }

        public static void DeleteAssetPlanScheduleById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteAssetPlanScheduleById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteAssetPlanScheduleById";

                cmd.ExecuteNonQuery();
            }
        }

    }
}
