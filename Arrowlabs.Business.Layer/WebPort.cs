﻿// -----------------------------------------------------------------------
// <copyright file="WebPort.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data;
    using System.Data.SqlClient;
    using Arrowlabs.Mims.Audit.Models;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class WebPort
    {
        public int WebPortID { get; set; }
        public int Port { get; set; }
        public bool Active { get; set; }

        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string CreatedBy { get; set; }


        private static WebPort GetWebPortFromSqlReader(SqlDataReader reader)
        {
            var webPort = new WebPort();

            webPort.WebPortID = Convert.ToInt16(reader["WebPortID"].ToString());
            webPort.Port = (String.IsNullOrEmpty(reader["Port"].ToString())) ? 0 : Convert.ToInt32(reader["Port"].ToString());
            webPort.Active = (String.IsNullOrEmpty(reader["Active"].ToString())) ? false : Convert.ToBoolean(reader["Active"].ToString());
            webPort.UpdatedBy = reader["UpdatedBy"].ToString();
            webPort.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            webPort.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            webPort.CreatedBy = reader["CreatedBy"].ToString();

            return webPort;
        }

        /// <summary>
        /// It returns all Web Ports
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<WebPort> GetAllWebPort(string dbConnection)
        {
            var webPorts = new List<WebPort>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllWebPort", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var webPort = GetWebPortFromSqlReader(reader);
                    webPorts.Add(webPort);
                }
                connection.Close();
                reader.Close();
            }
            return webPorts;
        }


        /// <summary>
        /// It returns all available Web Ports
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<WebPort> GetAllAvailableWebPorts(string dbConnection)
        {
            var webPorts = new List<WebPort>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAvailableWebPorts", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;              

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var webPort = GetWebPortFromSqlReader(reader);
                    webPorts.Add(webPort);
                }
                connection.Close();
                reader.Close();
            }
            return webPorts;
        }
        /// <summary>
        /// It inserts or updates WebPort
        /// </summary>
        /// <param name="webPort"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static bool InsertorUpdateWebPort(WebPort webPort, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (webPort != null)
            {
                int id = webPort.WebPortID;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;
                
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateWebPort", connection);

                    cmd.Parameters.Add(new SqlParameter("@WebPortID", SqlDbType.Int));
                    cmd.Parameters["@WebPortID"].Value = webPort.WebPortID;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.NVarChar));
                    cmd.Parameters["@Port"].Value = webPort.Port;

                    cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit));
                    cmd.Parameters["@Active"].Value = webPort.Active;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = webPort.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = webPort.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = webPort.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = webPort.CreatedBy;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new WebPortAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, webPort.UpdatedBy);
                            Reflection.CopyProperties(webPort, audit);
                            WebPortAudit.InsertWebPortAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateWebPort", ex, auditDbConnection);
                    }
                }
            }
            return true;
        }

        public static bool UpdateWebPort(int port,bool active, string dbConnection)
        {
          
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("UpdateWebPort", connection);

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.NVarChar));
                    cmd.Parameters["@Port"].Value = port;

                    cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit));
                    cmd.Parameters["@Active"].Value = active;                

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();               
            }
            return true;
        }
        public static bool DeleteWebPortWebPortID(int WebPortId, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteWebPortByWebPortId", connection);

                cmd.Parameters.Add(new SqlParameter("@WebPortId", SqlDbType.Int));
                cmd.Parameters["@WebPortId"].Value = WebPortId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteWebPortByWebPortId";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }

}

