﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Arrowlabs.Business.Layer;
using System.Web.Configuration;
using ArrowLabs.Licence.Portal.Helpers;
namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class Devices : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        static ClientLicence getClientLic;

        protected string senderName;
        protected string senderName2;
        protected string ipaddress;
        protected string dispatchSurv;
        protected string userinfoDisplay;

        protected string ASSETSDisplay;
        protected string USERSDisplay;
        protected string ALARMSDisplay;
        protected string TASKSDisplay;
        protected string VERIFIERDisplay;
        protected string REMINDERSDisplay;
        protected string GROUPSDisplay;
        protected string LOCATIONSDisplay;
        protected string CHECKLISTSDisplay;
        protected string ATTACHMENTDisplay;
        protected string ERRORDisplay;
        protected string NOTIFICATIONDisplay;
        protected string TICKETINGDisplay;
        protected string TRANSACTIONDisplay;
        protected string DUTYDisplay;
        protected string POSTDisplay;
        protected string WAREHOUSEDisplay;
        protected string senderName3;
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;

            senderName2 = Encrypt.EncryptData(User.Identity.Name, true, dbConnection);
            senderName = User.Identity.Name;
            userinfoDisplay = "block";
            var userinfo = Users.GetUserByName(User.Identity.Name, dbConnection);
            try
            {

                //general
                ATTACHMENTDisplay = "block";
                ERRORDisplay = "block";
                
                REMINDERSDisplay = "block";
                USERSDisplay = "block";

                //Incident
                ALARMSDisplay = "none";
                TRANSACTIONDisplay = "none";

                //task
                TASKSDisplay = "none";
                CHECKLISTSDisplay = "none";
                //verifier
                VERIFIERDisplay = "none";
             
                //colab
                GROUPSDisplay = "none";
                //location
                LOCATIONSDisplay = "none";

                //notichat
                NOTIFICATIONDisplay = "none";
                //ticketing
                TICKETINGDisplay = "none";
              
                //Duty
                DUTYDisplay = "none";
                //Post
                POSTDisplay = "none";
                //Warehouse
                WAREHOUSEDisplay = "none";
                ASSETSDisplay = "none";

                var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
                ipaddress = configtSettings.ChatServerUrl + "/signalr";
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                senderName3 = userinfo.CustomerUName;
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                    //if (pushDev != null)
                    //{
                    //    if (!string.IsNullOrEmpty(pushDev.Username))
                    //    {
                    //        pushDev.IsServerPortal = true;
                    //        pushDev.SiteId = userinfo.SiteId;
                    //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection, dbConnectionAudit, true);
                    //    }
                    //}
                    PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);
                    userinfoDisplay = "none";
                }
                if (getClientLic != null)
                {
                    if (getClientLic.isCollaboration)
                    {
                        GROUPSDisplay = "block";
                    }
                    if (getClientLic.isLocation)
                    {
                        LOCATIONSDisplay = "block";
                    }
                    if (getClientLic.isTicketing)
                    {
                        TICKETINGDisplay = "block";
                    }
                    if (getClientLic.isPostOrder)
                    {
                        POSTDisplay = "block";
                    }
                    if (getClientLic.isDutyRoster)
                    {
                        DUTYDisplay = "block";
                    }
                    if (getClientLic.isIncident)
                    {
                        ALARMSDisplay = "block";
                        TRANSACTIONDisplay = "block";
                    }
                    if(getClientLic.isTask)
                    {
                        CHECKLISTSDisplay = "block";
                        TASKSDisplay = "block";
                    }
                    if (getClientLic.isVerification)
                    {
                        VERIFIERDisplay = "block";
                    }
                    if (getClientLic.isNotification || getClientLic.isChat)
                    {
                        NOTIFICATIONDisplay = "block";
                    }
                    if (getClientLic.isWarehouse)
                    {
                        WAREHOUSEDisplay = "block";
                        ASSETSDisplay = "block";
                    }
                    if (getClientLic.isSurveillance)
                    {
                        var allmainlocs = MilestoneCamera.GetAllMilestoneCamera(dbConnection);
                        var allmainloc = new List<MilestoneCamera>();
                        var assignedCams = ClientCamera.GetAllClientCameras(dbConnection);
                        var camList = new List<string>();
                        var assignList = new List<string>();
                        foreach (var cams in assignedCams)
                        {
                            assignList.Add(cams.CameraName);
                        }
                        foreach (var cams in allmainlocs)
                        {
                            if (!assignList.Contains(cams.CameraName))
                                allmainloc.Add(cams);
                        }
                        milestoneCamera.DataTextField = "CameraName";
                        milestoneCamera.DataValueField = "CameraName";
                        milestoneCamera.DataSource = allmainloc;
                        milestoneCamera.DataBind();
                    }
                    if (!getClientLic.isSurveillance)
                        dispatchSurv = "style='display:none;'";
                }

                var assignmanagers = Users.GetAllManagers(dbConnection);
                assignManager.DataTextField = "Username";
                assignManager.DataValueField = "ID";
                assignManager.DataSource = assignmanagers;
                assignManager.DataBind();

                var ttypes = ThirdPartySettingType.GetAllThirdPartySettingType(dbConnection);
                thirdPartytypeSelect.DataTextField = "Name";
                thirdPartytypeSelect.DataValueField = "Id";
                thirdPartytypeSelect.DataSource = ttypes;
                thirdPartytypeSelect.DataBind();
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Devices", "Page_Load", er, dbConnection, userinfo.SiteId);
            }
        }
        [WebMethod]
        public static List<string> getTableDataDevices(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    var sessions = Device.GetAllDevice(dbConnection);
                    var returnstring = string.Empty;
                    foreach (var item in sessions)
                    {
                        var healthcheckdev = Arrowlabs.Business.Layer.HealthCheck.GetDeviceHealthCheckbyMacAddress(item.MACAddress, dbConnection);
                        var devState = string.Empty;
                        var mangid = 0;
                        var getManager = ClientManager.GetAllClientManagerByMacAddress(item.MACAddress, dbConnection);
                        if (getManager.Count > 0)
                        {
                            mangid = getManager[0].UserId;
                        }
                        if (healthcheckdev != null)
                        {
                            if (Decrypt.DecryptData(item.Register, true, dbConnection) == "1")
                                devState = "<a  class='green-color mr-2x' onclick='devChangeStatus(&apos;" + item.MACAddress + "&apos;,&apos;" + Decrypt.DecryptData(item.Register, true, dbConnection) + "&apos;)'><i class='fa fa-check-circle'></i> Activate</a><a href='#' data-target='#deleteDevModal'  data-toggle='modal' onclick='rowchoiceDevice(&apos;" + item.MACAddress + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                            else
                                devState = "<a  class='red-color mr-2x' onclick='devChangeStatus(&apos;" + item.MACAddress + "&apos;,&apos;" + Decrypt.DecryptData(item.Register, true, dbConnection) + "&apos;)'><i class='fa fa-minus-circle'></i> Suspend</a><a href='#' data-target='#deleteDevModal'  data-toggle='modal' onclick='rowchoiceDevice(&apos;" + item.MACAddress + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                            returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + healthcheckdev.StatusInfo + "</td><td><a href='#' data-target='#deviceProfileModal'  data-toggle='modal'   class='mr-2x' onclick='assigneDeviceInfo(&apos;" + item.MACAddress + "&apos;,&apos;" + mangid + "&apos;)'>" + item.PCName + "</a></td><td>" + item.MACAddress + "</td><td>" + item.LastLoginDate.ToString() + "</td><td id='" + item.MACAddress + "_td'>" + devState + "</td></tr>";
                            listy.Add(returnstring);
                        }
                        else
                        {
                            devState = "<a  class='green-color mr-2x' onclick='devChangeStatus(&apos;" + item.MACAddress + "&apos;,&apos;" + Decrypt.DecryptData(item.Register, true, dbConnection) + "&apos;)'><i class='fa fa-check-circle'></i> Activate</a><a href='#' data-target='#deleteDevModal'  data-toggle='modal' onclick='rowchoiceDevice(&apos;" + item.MACAddress + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                            returnstring = "<tr role='row' class='odd'><td class='sorting_1'>Offline</td><td><a href='#' data-target='#deviceProfileModal'  data-toggle='modal'   class='mr-2x' onclick='assigneDeviceInfo(&apos;" + item.MACAddress + "&apos;,&apos;" + mangid + "&apos;)'>" + item.PCName + "</a></td><td>" + item.MACAddress + "</td><td>" + item.LastLoginDate.ToString() + "</td><td id='" + item.MACAddress + "_td'>" + devState + "</td></tr>";
                            listy.Add(returnstring);
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getTableDataDevices", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                var devinuse = 0;
                foreach (var dev in getalldevs)
                {
                    if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                        devinuse++;
                }
                var users = Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);

                if (licenseInfo != null)
                {
                    var remaining = (Convert.ToInt32(licenseInfo.TotalMobileUsers) - users).ToString();
                    listy.Add(licenseInfo.Address);
                    listy.Add(licenseInfo.ClientId);
                    listy.Add("2.3");
                    listy.Add(licenseInfo.Name);
                    listy.Add(licenseInfo.PhoneNo);
                    listy.Add(licenseInfo.SerialNo);
                    listy.Add((Convert.ToInt32(licenseInfo.TotalDevices) - devinuse).ToString()); //Remaining Client
                    listy.Add(licenseInfo.TotalDevices); //Total Client
                    listy.Add(devinuse.ToString()); //Used Client
                    listy.Add(remaining); // Remaining Mobile
                    listy.Add(licenseInfo.TotalMobileUsers); //Total Mobile
                    listy.Add(users.ToString()); // Used
                    licenseInfo.RemainingMobileUsers = remaining;
                    licenseInfo.UsedMobileUsers = users.ToString();

                    listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                    listy.Add(licenseInfo.isNotification.ToString().ToLower());
                    listy.Add(licenseInfo.isLocation.ToString().ToLower());
                    listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                    listy.Add(licenseInfo.isTask.ToString().ToLower());
                    listy.Add(licenseInfo.isIncident.ToString().ToLower());
                    listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                    listy.Add(licenseInfo.isChat.ToString().ToLower());
                    listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                    listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                    listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                    listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                    listy.Add(licenseInfo.isVerification.ToString().ToLower());
                    listy.Add(licenseInfo.isRequest.ToString().ToLower());
                    listy.Add(licenseInfo.isDispatch.ToString().ToLower());

                    ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> changeUserState(int id, string status,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    { 
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var customData = Users.GetUserById(id, dbConnection);

                    if (customData != null)
                    {
                        var statusElem = string.Empty;
                        if (status == "1")
                        {
                            customData.Register = "2";
							
							                            var action = "<a href='#' data-target='#deleteUserModal'  data-toggle='modal' onclick='rowchoiceUser(&apos;" + customData.ID + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                            if (userinfo.RoleId != (int)Role.SuperAdmin)
                                action = string.Empty;
							
                            statusElem = "<a  class='red-color mr-2x' onclick='userChangeStatus(&apos;" + customData.ID + "&apos;,&apos;" + customData.Register + "&apos;)'><i class='fa fa-minus-circle'></i> Suspend</a>"+action;
                            LoginSession.DeleteLoginByUsername(customData.Username, dbConnection);
                            listy.Add(customData.Username + "_td");
                            listy.Add(statusElem);
                        }
                        else
                        {
                            customData.Register = "1";
							
							                            var action = "<a href='#' data-target='#deleteUserModal'  data-toggle='modal' onclick='rowchoiceUser(&apos;" + customData.ID + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                            if (userinfo.RoleId != (int)Role.SuperAdmin)
                                action = string.Empty;
							
                            statusElem = "<a  class='green-color mr-2x' onclick='userChangeStatus(&apos;" + customData.ID + "&apos;,&apos;" + customData.Register + "&apos;)'><i class='fa fa-check-circle'></i> Activate</a>"+action;
                            listy.Add(customData.Username + "_td");
                            listy.Add(statusElem);
                        }
                        Users.InsertOrUpdateUsers(customData, dbConnection, dbConnectionAudit, true);

                    }
                }
                catch (Exception er)
                {
                    listy.Clear();
                    MIMSLog.MIMSLogSave("Devices", "changeUserState", er, dbConnection, userinfo.SiteId);
                }
              
            }
            return listy;
        }
        [WebMethod]
        public static List<string> changeDevState(string id, string status,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    { 
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var customData = Device.GetClientDeviceByMacAddress(id, dbConnection);

                    if (customData != null)
                    {
                        var statusElem = string.Empty;
                        if (status == "1")
                        {
                            customData.Register = Encrypt.EncryptData("2", true, dbConnection);
                            var action = "<a href='#' data-target='#deleteDevModal'  data-toggle='modal' onclick='rowchoiceDevice(&apos;" + customData.MACAddress + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                            if (userinfo.RoleId != (int)Role.SuperAdmin)
                                action = string.Empty;
                            statusElem = "<a  class='red-color mr-2x' onclick='devChangeStatus(&apos;" + customData.MACAddress + "&apos;,&apos;" + Decrypt.DecryptData(customData.Register, true, dbConnection) + "&apos;)'><i class='fa fa-minus-circle'></i> Suspend</a>"+action;

                            listy.Add(customData.MACAddress + "_td");
                            listy.Add(statusElem);
                        }
                        else
                        {
                            customData.Register = Encrypt.EncryptData("1", true, dbConnection);
                            var action = "<a href='#' data-target='#deleteDevModal'  data-toggle='modal' onclick='rowchoiceDevice(&apos;" + customData.MACAddress + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                            if (userinfo.RoleId != (int)Role.SuperAdmin)
                                action = string.Empty;
                            statusElem = "<a  class='green-color mr-2x' onclick='devChangeStatus(&apos;" + customData.MACAddress + "&apos;,&apos;" + Decrypt.DecryptData(customData.Register, true, dbConnection) + "&apos;)'><i class='fa fa-check-circle'></i> Activate</a>"+action;
                            listy.Add(customData.MACAddress + "_td");
                            listy.Add(statusElem);
                        }
                        Device.InsertClientDevice(customData, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception er)
                {
                    listy.Clear();
                    MIMSLog.MIMSLogSave("Devices", "changeDevState", er, dbConnection, userinfo.SiteId);
                }
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableDataUsers(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = new List<Users>();
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        sessions = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var session = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var usr in session)
                        {
                            sessions.Add(usr);
                            var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                            foreach (var subsubuser in getallUsers)
                            {
                                sessions.Add(subsubuser);
                            }
                        }
                        var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                        unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                        foreach (var subsubuser in unassigned)
                        {
                            sessions.Add(subsubuser);
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        sessions = Users.GetAllUsersBySiteId(userinfo.SiteId,dbConnection);

                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        sessions = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                        sessions = sessions.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        foreach (var site in sites)
                        {
                            sessions.AddRange(Users.GetAllUsersBySiteId(site.SiteId, dbConnection));
                        }
                    }
                    else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        sessions = Users.GetAllUsers(dbConnection);
                        sessions = sessions.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        sessions = Users.GetAllUsers(dbConnection);
                    }

                    var grouped = sessions.GroupBy(item => item.ID);
                    sessions = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                    foreach (var item in sessions)
                    {
                        var fontstyle = string.Empty;
                        if (item.Username != userinfo.Username)
                        {
                            if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                            {
                                fontstyle = "style='color:lime;'";
                            }
                         //   var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(item.Username, dbConnection);
                            var monitor = string.Empty;
                            if (item.RoleId == (int)Role.Admin || item.RoleId == (int)Role.Manager || item.RoleId == (int)Role.Director || item.RoleId == (int)Role.Regional || item.RoleId == (int)Role.ChiefOfficer)
                            {
                            //    if (pushDev != null)
                             //   {
                            //        if (!string.IsNullOrEmpty(pushDev.Username))
                             //       {
                                        if (item.IsServerPortal)
                                        {
                                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                                            fontstyle = "";
                                        }
                                        else
                                        {
                                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                                        }
                                 //   }
                                //    else
                                 //   {
                                 //       monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                                 //       fontstyle = "";
                                  //  }
                               // }
                            }

                            var statusElem = string.Empty;
                            var deleteUser = "<a href='#' data-target='#deleteUserModal'  data-toggle='modal' onclick='rowchoiceUser(&apos;" + item.ID + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                            if (userinfo.RoleId != (int)Role.SuperAdmin)
                                deleteUser = string.Empty;
                            if (item.Register == "1")
                                statusElem = "<a  class='green-color mr-2x' onclick='userChangeStatus(&apos;" + item.ID + "&apos;,&apos;" + item.Register + "&apos;)'><i class='fa fa-check-circle'></i> Activate</a>" + deleteUser;
                            else
                                statusElem = "<a  class='red-color mr-2x' onclick='userChangeStatus(&apos;" + item.ID + "&apos;,&apos;" + item.Register + "&apos;)'><i class='fa fa-minus-circle'></i> Suspend</a>" + deleteUser;

                            if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                                statusElem = string.Empty;

                            var chatUser = ChatUser.GetChatUserByName(item.Username, dbConnection);
                            var lastlogintime = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString();
                            if (chatUser != null)
                            {
                                if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                                    lastlogintime = chatUser.LoginDate.Value.ToString();
                                else
                                    lastlogintime = chatUser.LogoutDate.Value.ToString();
                            }

                            var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + CommonUtility.getUserStatus(item.Active) + "</td><td>" + item.Username + "</td><td>" + CommonUtility.getUserDeviceType(item.DeviceType, fontstyle, monitor) + "</td><td>" + lastlogintime + "</td><td id='" + item.Username + "_td'>" + statusElem + "</td></tr>";
                            listy.Add(returnstring);
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getTableDataUsers", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableDataWhitelist(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = Arrowlabs.Business.Layer.WhiteList.GetAllWhiteList(dbConnection);
                foreach (var item in sessions)
                {
                    var action = "<a href='#' data-target='#newEditWhiteListModal'  data-toggle='modal' onclick='whitelistrowchoice(&apos;" + item.MACAddress + "&apos;)'><i class='fa fa-check-circle mr-1x'></i>Edit</a><a href='#' data-target='#deleteModal'  data-toggle='modal' onclick='whitelistrowchoice(&apos;" + item.MACAddress + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                        action = string.Empty;
                    var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.MACAddress + "</td><td>" + item.CreatedBy + "</td><td>" + item.CreatedDate.ToString() + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getTableDataWhitelist", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableDataThirdParty(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = ThirdPartySeting.GetAllThirdPartySeting(dbConnection);
                foreach (var item in sessions)
                {
                    var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.Name + "</td><td>" + item.IPAddress + "</td><td>" + item.Port + "</td><td>" + item.UserName + "</td><td><a href='#' data-target='#new3rdPartySystemModal'  data-toggle='modal' onclick='thirdrowchoice(&apos;" + item.Id + "&apos;)'><i class='fa fa-check-circle mr-1x'></i>Edit</a><a href='#' data-target='#deleteThirdModal'  data-toggle='modal' onclick='thirdrowchoice(&apos;" + item.Id + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getTableDataThirdParty", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getSettingsData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var mainsettings = ConfigSettings.GetConfigSettings(dbConnection);
                var mobilesettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var client = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                var houekeeping = HouseKeeping.GetHouseKeeping(dbConnection);
                var auditsettings = AuditSettings.GetAuditSettings(dbConnection);
                var verifierinfo = VerifierService.GetAllVerifierService(dbConnection);
                if (mainsettings != null)
                {
                    //MIMS SERVER
                    listy.Add(mainsettings.ServerIP);
                    listy.Add(mainsettings.Port.ToString());
                    listy.Add(mainsettings.ServerConnection.ToString());
                    listy.Add(mainsettings.RTSPStreamPort.ToString());
                    listy.Add(mainsettings.VideoPort.ToString());
                    listy.Add(mainsettings.ChatServerUrl.ToString());
                    listy.Add(mainsettings.MessagePort.ToString());
                    //MIMS CLIENT
                    listy.Add(mainsettings.VideoLength.ToString());
                    listy.Add(mainsettings.VideoLengthState.ToString());
                    listy.Add(Decrypt.DecryptData(mainsettings.SettingsPassword, true, dbConnection));
                    listy.Add(mainsettings.SettingPassState.ToString());
                    listy.Add(mainsettings.NoImages.ToString());
                    listy.Add(mainsettings.NumberOfImageState.ToString());
                    listy.Add(mainsettings.PSDPortal.ToString());
                    listy.Add(mainsettings.CSVPath);
                    listy.Add(mainsettings.CSVPathState.ToString());
                    listy.Add(mainsettings.GPSPath);
                    listy.Add(mainsettings.GPSURL);
                    listy.Add(mainsettings.GPSInterval.ToString());
                    listy.Add(mainsettings.GPSIntervalState.ToString());
                    listy.Add(mainsettings.Interval.ToString());
                    listy.Add(mainsettings.IntervalState.ToString());
                }
                if (mobilesettings != null)
                {
                    //MIMS MOBILE
                    listy.Add(mobilesettings.MIMSMobileGPSInterval.ToString());
                    listy.Add(mobilesettings.MIMSMobileAddress.ToString());
                    listy.Add(mobilesettings.MaxNumberAttachments.ToString());
                    listy.Add(mobilesettings.MaxUploadSize.ToString());
                }
                if (client != null)
                {
                    //MOC
                    listy.Add(client.Port.ToString());
                    listy.Add(mainsettings.HealthCheckInterval.ToString());
                    listy.Add(mainsettings.HealthCheckState.ToString());
                }
                if (houekeeping != null)
                {
                    //HOUSE KEEPING
                    listy.Add(houekeeping.Interval.ToString());

                    listy.Add(houekeeping.Asset.ToString());
                    listy.Add(houekeeping.CustomEvent.ToString());
                    listy.Add(houekeeping.TaskAttachment.ToString());
                    listy.Add(houekeeping.TemplateCheckList.ToString());
                    listy.Add(houekeeping.DutyRoaster.ToString());
                    listy.Add(houekeeping.Group.ToString());
                    listy.Add(houekeeping.Location.ToString());
                    listy.Add(houekeeping.MIMSLog.ToString());
                    listy.Add(houekeeping.Notification.ToString());
                    listy.Add(houekeeping.PostOrder.ToString());
                    listy.Add(houekeeping.Reminder.ToString());
                    listy.Add(houekeeping.Task.ToString());
                    listy.Add(houekeeping.Offence.ToString());
                    listy.Add(houekeeping.TransactionLog.ToString());
                    listy.Add(houekeeping.User.ToString());
                    listy.Add(houekeeping.Verifier.ToString());

                    listy.Add(houekeeping.IsAsset.ToString());
                    listy.Add(houekeeping.IsAlarm.ToString());
                    listy.Add(houekeeping.IsAttachmentFiles.ToString());
                    listy.Add(houekeeping.IsCheckList.ToString());
                    listy.Add(houekeeping.IsDutyRostersLogs.ToString());
                    listy.Add(houekeeping.IsGroups.ToString());
                    listy.Add(houekeeping.IsLocation.ToString());
                    listy.Add(houekeeping.IsMIMSLogs.ToString());
                    listy.Add(houekeeping.IsNotification.ToString());
                    listy.Add(houekeeping.IsPostOrders.ToString());
                    listy.Add(houekeeping.IsReminders.ToString());
                    listy.Add(houekeeping.IsTask.ToString());
                    listy.Add(houekeeping.IsTicketing.ToString());
                    listy.Add(houekeeping.IsTransactionLogs.ToString());
                    listy.Add(houekeeping.IsUser.ToString());
                    listy.Add(houekeeping.IsVerifier.ToString());

                    //Audit
                    listy.Add(auditsettings.Asset.ToString());
                    listy.Add(auditsettings.CustomEvent.ToString());
                    listy.Add(auditsettings.TaskAttachment.ToString());
                    listy.Add(auditsettings.TemplateCheckList.ToString());
                    listy.Add(auditsettings.DutyRoaster.ToString());
                    listy.Add(auditsettings.Group.ToString());
                    listy.Add(auditsettings.Location.ToString());
                    listy.Add(auditsettings.MIMSLog.ToString());
                    listy.Add(auditsettings.Notification.ToString());
                    listy.Add(auditsettings.PostOrder.ToString());
                    listy.Add(auditsettings.Reminder.ToString());
                    listy.Add(auditsettings.Task.ToString());
                    listy.Add(auditsettings.Offence.ToString());
                    listy.Add(auditsettings.TransactionLog.ToString());
                    listy.Add(auditsettings.User.ToString());
                    listy.Add(auditsettings.Verifier.ToString());

                    listy.Add(auditsettings.IsAsset.ToString());
                    listy.Add(auditsettings.IsAlarm.ToString());
                    listy.Add(auditsettings.IsAttachmentFiles.ToString());
                    listy.Add(auditsettings.IsCheckList.ToString());
                    listy.Add(auditsettings.IsDutyRostersLogs.ToString());
                    listy.Add(auditsettings.IsGroups.ToString());
                    listy.Add(auditsettings.IsLocation.ToString());
                    listy.Add(auditsettings.IsMIMSLogs.ToString());
                    listy.Add(auditsettings.IsNotification.ToString());
                    listy.Add(auditsettings.IsPostOrders.ToString());
                    listy.Add(auditsettings.IsReminders.ToString());
                    listy.Add(auditsettings.IsTask.ToString());
                    listy.Add(auditsettings.IsTicketing.ToString());
                    listy.Add(auditsettings.IsTransactionLogs.ToString());
                    listy.Add(auditsettings.IsUser.ToString());
                    listy.Add(auditsettings.IsVerifier.ToString());
                }
                //VERIFIER
                var anpr = string.Empty;
                var frs = string.Empty;
                foreach (var ver in verifierinfo)
                {
                    if (ver.Type == 0)
                    {
                        frs = ver.Url;
                    }
                    else if (ver.Type == 1)
                    {
                        anpr = ver.Url;
                    }
                }
                listy.Add(frs);
                listy.Add(anpr);
                if (mainsettings != null)
                {
                    if (mainsettings.Protocol)
                        listy.Add("true");
                    else
                        listy.Add("false");
                }
                else
                    listy.Add("false");

                listy.Add(houekeeping.Warehouse.ToString());
                listy.Add(houekeeping.IsWarehouse.ToString());
                listy.Add(auditsettings.Warehouse.ToString());
                listy.Add(auditsettings.IsWarehouse.ToString());
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getSettingsData", er, dbConnection);
            }
            return listy;
        }

        [WebMethod]
        public static string saveWhitelist(string id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var newOff = new Arrowlabs.Business.Layer.WhiteList();
                    newOff.MACAddress = id;
                    newOff.UpdatedDate = CommonUtility.getDTNow();
                    newOff.CreatedDate = CommonUtility.getDTNow();
                    newOff.CreatedBy = userinfo.Username;
                    newOff.UpdatedBy = userinfo.Username;
                    Arrowlabs.Business.Layer.WhiteList.InsertOrUpdateWhiteList(newOff, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception er)
                {
                    
                    MIMSLog.MIMSLogSave("Devices", "saveWhitelist", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string editWhitelist(string id, string oldMac, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {

                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var newOff = new Arrowlabs.Business.Layer.WhiteList();
                    Arrowlabs.Business.Layer.WhiteList.DeleteWhiteByMacAddress(oldMac, dbConnection);
                    newOff.MACAddress = id;
                    newOff.UpdatedDate = CommonUtility.getDTNow();
                    newOff.UpdatedBy = userinfo.Username;
                    newOff.CreatedBy = userinfo.Username;
                    newOff.CreatedDate = CommonUtility.getDTNow();
                    Arrowlabs.Business.Layer.WhiteList.InsertOrUpdateWhiteList(newOff, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception er)
                {
                    
                    MIMSLog.MIMSLogSave("Devices", "editWhitelist", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string deleteWhitelist(string id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    Arrowlabs.Business.Layer.WhiteList.DeleteWhiteByMacAddress(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", id, id, userinfo, "Delete WhiteList" + id);
                }
                catch (Exception er)
                {

                    MIMSLog.MIMSLogSave("Devices", "deleteWhitelist", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string deleteDevice(string id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT.";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    Device.DeleteDeviceByMacAddress(Encrypt.EncryptData(id, true, dbConnection), dbConnection);
                    HealthCheck.DeleteHealthCheckDeviceByMacAddress(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", id, id, userinfo, "Delete Device" + id);
                }
                catch (Exception er)
                {

                    MIMSLog.MIMSLogSave("Devices", "deleteDevice", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string deleteUser(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var delU = Users.GetUserById(id, dbConnection);
					if(delU != null && !string.IsNullOrEmpty(delU.Username)){
						Users.DeleteUserByIdAndUsername(id, delU.Username, dbConnection);
						SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", delU.Username, id.ToString(), userinfo, "Delete User" + delU.Username);
					}
					else{
						return "User has already been deleted";
					}
                }
                catch (Exception er)
                {

                    MIMSLog.MIMSLogSave("Devices", "deleteUser", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string deleteThirdParty(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    ThirdPartySeting.DeleteThirdPartSettingById(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", id.ToString(), id.ToString(), userinfo, "Delete ThirdPartySeting" + id.ToString());
                }
                catch (Exception er)
                {
                    MIMSLog.MIMSLogSave("Devices", "deleteThirdParty", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
                return "SUCCESS";
            }
        }

        [WebMethod]
        public static string saveServerSettings(string port, string ip, string con, string rtsp, string video, string signalR, string mPort, string protocol,string uname)
        {            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }


                    var confset = ConfigSettings.GetConfigSettings(dbConnection);
                    var oldValue = string.Empty;
                    var newValue = string.Empty;
                    if (confset.Port != Convert.ToInt32(port))
                    {
                        oldValue = confset.Port.ToString();
                        newValue = port;
                        confset.Port = Convert.ToInt32(port);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings Server Port");
                    }
                    if (confset.ServerIP != ip)
                    {
                        oldValue = confset.ServerIP;
                        newValue = ip;
                        confset.ServerIP = ip;
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings Server IP");
                    }
                    if (confset.ConnectionString != con)
                    {
                        oldValue = confset.ConnectionString;
                        newValue = con;
                        confset.ConnectionString = con;
                        confset.ServerConnection = con;
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings MIMS API CONNECTION");
                    }
                    if (confset.RTSPStreamPort != Convert.ToInt32(rtsp))
                    {
                        oldValue = confset.RTSPStreamPort.ToString();
                        newValue = rtsp;
                        confset.RTSPStreamPort = Convert.ToInt32(rtsp);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings RTSP PORT");
                    }
                    if (confset.VideoPort != Convert.ToInt32(video))
                    {
                        oldValue = confset.VideoPort.ToString();
                        newValue = video;
                        confset.VideoPort = Convert.ToInt32(video);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings VIDEO STREAM PORT");
                    }
                    if (confset.ChatServerUrl != signalR)
                    {
                        oldValue = confset.ChatServerUrl;
                        newValue = signalR;
                        confset.ChatServerUrl = signalR;
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings MESSAGING SERVICE");
                    }
                    if (confset.MessagePort != Convert.ToInt32(mPort))
                    {
                        oldValue = confset.MessagePort.ToString();
                        newValue = mPort;
                        confset.MessagePort = Convert.ToInt32(mPort);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings MOC PORT");
                    }
                    if (confset.Protocol != Convert.ToBoolean(protocol))
                    {
                        oldValue = confset.Protocol.ToString();
                        newValue = protocol;
                        confset.Protocol = Convert.ToBoolean(protocol);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings Protocol");
                    }

                    ConfigSettings.InsertOrUpdateConfigSettings(confset, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "saveServerSettings", ex, dbConnection);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string saveClientSettings(string vidclip, string vidState, string pass, string passState,
            string images, string imagesState, string portal, string dbpath, string dbpathState, string gps, string gpsURL            ,
            string gpsI, string gpsIState, string interval, string intervalState,string uname
            )
        {

            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var confset = ConfigSettings.GetConfigSettings(dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var oldValue = string.Empty;
                    var newValue = string.Empty;

                    if (confset.VideoLength != Convert.ToInt32(vidclip))
                    {
                        oldValue = confset.VideoLength.ToString();
                        newValue = vidclip;
                        confset.VideoLength = Convert.ToInt32(vidclip);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings VIDEO CLIP LENGTH");
                    }

                    if (confset.VideoLengthState != Convert.ToBoolean(vidState))
                    {
                        oldValue = confset.VideoLengthState.ToString();
                        newValue = vidState;
                        confset.VideoLengthState = Convert.ToBoolean(vidState);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings VIDEO CLIP LENGTH SWITCH");
                    }

                    if (confset.SettingsPassword != Encrypt.EncryptData(pass, true, dbConnection))
                    {
                        oldValue = confset.SettingsPassword.ToString();
                        newValue = Encrypt.EncryptData(pass, true, dbConnection);
                        confset.SettingsPassword = Encrypt.EncryptData(pass, true, dbConnection);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings SETTINGS PASSWORD");
                    }

                    if (confset.SettingPassState != Convert.ToBoolean(passState))
                    {
                        oldValue = confset.SettingPassState.ToString();
                        newValue = passState;
                        confset.SettingPassState = Convert.ToBoolean(passState);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings SETTINGS PASSWORD SWITCH");
                    }

                    if (confset.NoImages != Convert.ToInt32(images))
                    {
                        oldValue = confset.NoImages.ToString();
                        newValue = images;
                        confset.NoImages = Convert.ToInt32(images);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings RECORDING IMAGES");
                    }
                    if (confset.NumberOfImageState != Convert.ToBoolean(imagesState))
                    {
                        oldValue = confset.NumberOfImageState.ToString();
                        newValue = imagesState;
                        confset.NumberOfImageState = Convert.ToBoolean(imagesState);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings RECORDING IMAGES SWITCH");
                    }
                    if (confset.PSDPortal != portal)
                    {
                        oldValue = confset.PSDPortal;
                        newValue = portal;
                        confset.PSDPortal = portal;
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings 3RD PARTY WEBPORTAL");
                    }
                    if (confset.CSVPath != dbpath)
                    {
                        oldValue = confset.CSVPath;
                        newValue = dbpath;
                        confset.CSVPath = dbpath;
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings 3RD PARTY DATABASE");
                    }
                    if (confset.CSVPathState != Convert.ToBoolean(dbpathState))
                    {
                        oldValue = confset.CSVPathState.ToString();
                        newValue = dbpathState;
                        confset.CSVPathState = Convert.ToBoolean(dbpathState);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings 3RD PARTY DATABASE SWITCH");
                    }

                    if (confset.GPSPath != gps)
                    {
                        oldValue = confset.GPSPath;
                        newValue = gps;
                        confset.GPSPath = gps;
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings 3RD PARTY GPS");
                    }

                    if (confset.GPSURL != gpsURL)
                    {
                        oldValue = confset.GPSURL;
                        newValue = gpsURL;
                        confset.GPSURL = gpsURL;
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings 3RD APP LAUNCH");
                    }

                    if (confset.GPSInterval != Convert.ToInt32(gpsI))
                    {
                        oldValue = confset.GPSInterval.ToString();
                        newValue = gpsI;
                        confset.GPSInterval = Convert.ToInt32(gpsI);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings GPS UPDATE INTERVAL");
                    }

                    if (confset.GPSIntervalState != Convert.ToBoolean(gpsIState))
                    {
                        oldValue = confset.GPSIntervalState.ToString();
                        newValue = gpsIState;
                        confset.GPSIntervalState = Convert.ToBoolean(gpsIState);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings GPS UPDATE INTERVAL SWITCH");
                    }

                    if (confset.Interval != Convert.ToInt32(interval))
                    {
                        oldValue = confset.Interval.ToString();
                        newValue = interval;
                        confset.Interval = Convert.ToInt32(interval);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings FILE TRANSFER INTERVAL");
                    }

                    if (confset.IntervalState != Convert.ToBoolean(intervalState))
                    {
                        oldValue = confset.IntervalState.ToString();
                        newValue = intervalState;
                        confset.IntervalState = Convert.ToBoolean(intervalState);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Client Settings FILE TRANSFER INTERVAL SWITCH");
                    }

                    ConfigSettings.InsertOrUpdateConfigSettings(confset, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "saveClientSettings", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }

        [WebMethod]
        public static string saveMobileSettings(string mobileGPS, string mobileAddress, string maxA, string maxU,string uname
            )
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var confset = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var oldValue = string.Empty;
                    var newValue = string.Empty;
                    if (!string.IsNullOrEmpty(mobileGPS))
                    {
                        if (confset.MIMSMobileGPSInterval != Convert.ToSingle(mobileGPS))
                        {
                            oldValue = confset.MIMSMobileGPSInterval.ToString();
                            newValue = mobileGPS;
                            confset.MIMSMobileGPSInterval = Convert.ToSingle(mobileGPS);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Mobile Settings GPS UPDATE INTERVAL");
                        }
                    }

                    if (confset.MIMSMobileAddress != mobileAddress)
                    {
                        oldValue = confset.MIMSMobileAddress;
                        newValue = mobileAddress;
                        confset.MIMSMobileAddress = mobileAddress;
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Mobile Settings URL LINK");
                    }
                    if (!string.IsNullOrEmpty(mobileAddress))
                    {
                        var split = mobileAddress.Split('/');
                        //confset.FilePath = @"C:\inetpub\wwwroot\" + split[split.Length - 1] + @"\Uploads\Video";
                        //confset.UploadServiceUrl = split[split.Length - 2] + "/lupload/";
                    }
                    if (!string.IsNullOrEmpty(maxA))
                    {
                        if (confset.MaxNumberAttachments != Convert.ToInt32(maxA)) 
                        { 
                            oldValue = confset.MaxNumberAttachments.ToString();
                            newValue = maxA;
                            confset.MaxNumberAttachments = Convert.ToInt32(maxA);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings MAX ATTACHMENTS");
                        }
                    }
                    if (!string.IsNullOrEmpty(maxU))
                    {
                        if (confset.MaxUploadSize != Convert.ToInt32(maxU))
                        {
                            oldValue = confset.MaxUploadSize.ToString();
                            newValue = maxU;
                            confset.MaxUploadSize = Convert.ToInt32(maxU);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Server Settings MAX UPLOAD SIZE");
                        }
                    }
                    MIMSConfigSetting.InsertorUpdateMIMSConfigSetting(confset, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "saveMobileSettings", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string saveMOCSettings(string mocport, string hk, string hkState, string uname
            )
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var confset = ConfigSettings.GetConfigSettings(dbConnection);
            var client = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var oldValue = string.Empty;
                    var newValue = string.Empty;
                    if (confset.HealthCheckInterval != Convert.ToInt32(hk))
                    {
                        oldValue = confset.HealthCheckInterval.ToString();
                        newValue = hk;
                        confset.HealthCheckInterval = Convert.ToInt32(hk);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "MOC Settings HEALTH CHECK INTERVAL");
                    }
                    if (confset.HealthCheckState != Convert.ToBoolean(hkState))
                    {
                        oldValue = confset.HealthCheckState.ToString();
                        newValue = hkState;
                        confset.HealthCheckState = Convert.ToBoolean(hkState);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "MOC Settings HEALTH CHECK INTERVAL SWITCH");
                    }
                    client.Port = Convert.ToInt32(mocport);

                    ConfigSettings.InsertOrUpdateConfigSettings(confset, dbConnection, dbConnectionAudit, true);
                    ClientLicence.InsertClientLicence(client, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "saveMOCSettings", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string saveHouseKeepingSettings(string interval, string asset, string cusev, string taskA, string checklist,
            string dRoster, string group, string location, string mlog, string noti, string pOrder, string rem, string task, string offence,
            string transLog, string user, string verifier, string Isinterval, string Isasset, string Iscusev, string IstaskA, string Ischecklist,
            string IsdRoster, string Isgroup, string Islocation, string Ismlog, string Isnoti, string IspOrder, string Isrem, string Istask, string Isoffence,
            string IstransLog, string Isuser, string Isverifier,string uname,string Iswarehouse,string warehouse
            )
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var saveData = HouseKeeping.GetHouseKeeping(dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var oldValue = string.Empty;
                    var newValue = string.Empty;
                    if (saveData.Interval != Convert.ToInt32(interval))
                    {
                        oldValue = saveData.Interval.ToString();
                        newValue = interval;
                        saveData.Interval = Convert.ToInt32(interval);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING INTERVAL");
                    }

                    if (saveData.Asset != Convert.ToInt32(asset))
                    {
                        oldValue = saveData.Asset.ToString();
                        newValue = asset;
                        saveData.Asset = Convert.ToInt32(asset);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING Asset");
                    }

                    if (saveData.CustomEvent != Convert.ToInt32(cusev))
                    {
                        oldValue = saveData.CustomEvent.ToString();
                        newValue = cusev;
                        saveData.CustomEvent = Convert.ToInt32(cusev);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING CustomEvent");
                    }

                    if (saveData.TaskAttachment != Convert.ToInt32(taskA))
                    {
                        oldValue = saveData.TaskAttachment.ToString();
                        newValue = taskA;
                        saveData.TaskAttachment = Convert.ToInt32(taskA);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING TaskAttachment");
                    }

                    if (saveData.TemplateCheckList != Convert.ToInt32(checklist))
                    {
                        oldValue = saveData.TemplateCheckList.ToString();
                        newValue = checklist;
                        saveData.TemplateCheckList = Convert.ToInt32(checklist);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING TemplateCheckList");
                    }

                    if (saveData.DutyRoaster != Convert.ToInt32(dRoster))
                    {
                        oldValue = saveData.DutyRoaster.ToString();
                        newValue = dRoster;
                        saveData.DutyRoaster = Convert.ToInt32(dRoster);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING DutyRoster");
                    }

                    if (saveData.Group != Convert.ToInt32(group))
                    {
                        oldValue = saveData.Group.ToString();
                        newValue = group;
                        saveData.Group = Convert.ToInt32(group);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING Group");
                    }

                    if (saveData.Location != Convert.ToInt32(location))
                    {
                        oldValue = saveData.Location.ToString();
                        newValue = location;
                        saveData.Location = Convert.ToInt32(location);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING Location");
                    }

                    if (saveData.MIMSLog != Convert.ToInt32(mlog))
                    {
                        oldValue = saveData.MIMSLog.ToString();
                        newValue = mlog;
                        saveData.MIMSLog = Convert.ToInt32(mlog);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING MIMSLog");
                    }

                    if (saveData.Notification != Convert.ToInt32(noti))
                    {
                        oldValue = saveData.Notification.ToString();
                        newValue = noti;
                        saveData.Notification = Convert.ToInt32(noti);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING Notification");
                    }

                    if (saveData.PostOrder != Convert.ToInt32(pOrder))
                    {
                        oldValue = saveData.PostOrder.ToString();
                        newValue = pOrder;
                        saveData.PostOrder = Convert.ToInt32(pOrder);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING PostOrder");
                    }

                    if (saveData.Reminder != Convert.ToInt32(rem))
                    {
                        oldValue = saveData.Reminder.ToString();
                        newValue = rem;
                        saveData.Reminder = Convert.ToInt32(rem);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING Reminder");
                    }

                    if (saveData.Task != Convert.ToInt32(task))
                    {
                        oldValue = saveData.Task.ToString();
                        newValue = task;
                        saveData.Task = Convert.ToInt32(task);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING Task");
                    }
                    if (saveData.Warehouse != Convert.ToInt32(warehouse))
                    {
                        oldValue = saveData.Warehouse.ToString();
                        newValue = warehouse;
                        saveData.Warehouse = Convert.ToInt32(warehouse);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING Warehouse");
                    }
                    if (saveData.Offence != Convert.ToInt32(offence))
                    {
                        oldValue = saveData.Offence.ToString();
                        newValue = offence;
                        saveData.Offence = Convert.ToInt32(offence);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING Offence");
                    }

                    if (saveData.TransactionLog != Convert.ToInt32(transLog))
                    {
                        oldValue = saveData.TransactionLog.ToString();
                        newValue = transLog;
                        saveData.TransactionLog = Convert.ToInt32(transLog);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING TransactionLog");
                    }

                    if (saveData.User != Convert.ToInt32(user))
                    {
                        oldValue = saveData.User.ToString();
                        newValue = user;
                        saveData.User = Convert.ToInt32(user);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING User");
                    }

                    if (saveData.Verifier != Convert.ToInt32(verifier))
                    {
                        oldValue = saveData.Verifier.ToString();
                        newValue = verifier;
                        saveData.Verifier = Convert.ToInt32(verifier);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE SETTING User");
                    }

                    if (saveData.IsAsset != Convert.ToBoolean(Isasset))
                    {
                        oldValue = saveData.IsAsset.ToString();
                        newValue = Isasset;
                        saveData.IsAsset = Convert.ToBoolean(Isasset);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsAsset");
                    }


                    if (saveData.IsAlarm != Convert.ToBoolean(Iscusev))
                    {
                        oldValue = saveData.IsAlarm.ToString();
                        newValue = Iscusev;
                        saveData.IsAlarm = Convert.ToBoolean(Iscusev);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsAlarm");
                    }

                    if (saveData.IsAttachmentFiles != Convert.ToBoolean(IstaskA))
                    {
                        oldValue = saveData.IsAttachmentFiles.ToString();
                        newValue = IstaskA;
                        saveData.IsAttachmentFiles = Convert.ToBoolean(IstaskA);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsAttachmentFiles");
                    }

                    if (saveData.IsCheckList != Convert.ToBoolean(Ischecklist))
                    {
                        oldValue = saveData.IsCheckList.ToString();
                        newValue = Ischecklist;
                        saveData.IsCheckList = Convert.ToBoolean(Ischecklist);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsCheckList");
                    }

                    if (saveData.IsDutyRostersLogs != Convert.ToBoolean(IsdRoster))
                    {
                        oldValue = saveData.IsDutyRostersLogs.ToString();
                        newValue = IsdRoster;
                        saveData.IsDutyRostersLogs = Convert.ToBoolean(IsdRoster);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsDutyRostersLogs");
                    }

                    if (saveData.IsGroups != Convert.ToBoolean(Isgroup))
                    {
                        oldValue = saveData.IsGroups.ToString();
                        newValue = Isgroup;
                        saveData.IsGroups = Convert.ToBoolean(Isgroup);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsGroups");
                    }

                    if (saveData.IsLocation != Convert.ToBoolean(Islocation))
                    {
                        oldValue = saveData.IsLocation.ToString();
                        newValue = Islocation;
                        saveData.IsLocation = Convert.ToBoolean(Islocation);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsLocation");
                    }

                    if (saveData.IsMIMSLogs != Convert.ToBoolean(Ismlog))
                    {
                        oldValue = saveData.IsMIMSLogs.ToString();
                        newValue = Ismlog;
                        saveData.IsMIMSLogs = Convert.ToBoolean(Ismlog);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsMIMSLogs");
                    }

                    if (saveData.IsNotification != Convert.ToBoolean(Isnoti))
                    {
                        oldValue = saveData.IsNotification.ToString();
                        newValue = Isnoti;
                        saveData.IsNotification = Convert.ToBoolean(Isnoti);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsNotification");
                    }

                    if (saveData.IsPostOrders != Convert.ToBoolean(IspOrder))
                    {
                        oldValue = saveData.IsPostOrders.ToString();
                        newValue = IspOrder;
                        saveData.IsPostOrders = Convert.ToBoolean(IspOrder);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsPostOrders");
                    }

                    if (saveData.IsReminders != Convert.ToBoolean(Isrem))
                    {
                        oldValue = saveData.IsReminders.ToString();
                        newValue = Isrem;
                        saveData.IsReminders = Convert.ToBoolean(Isrem);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsReminders");
                    }

                    if (saveData.IsTask != Convert.ToBoolean(Istask))
                    {
                        oldValue = saveData.IsTask.ToString();
                        newValue = Istask;
                        saveData.IsTask = Convert.ToBoolean(Istask);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsTask");
                    }

                    if (saveData.IsTicketing != Convert.ToBoolean(Isoffence))
                    {
                        oldValue = saveData.IsTicketing.ToString();
                        newValue = Isoffence;
                        saveData.IsTicketing = Convert.ToBoolean(Isoffence);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsTicketing");
                    }

                    if (saveData.IsTransactionLogs != Convert.ToBoolean(IstransLog))
                    {
                        oldValue = saveData.IsTransactionLogs.ToString();
                        newValue = IstransLog;
                        saveData.IsTransactionLogs = Convert.ToBoolean(IstransLog);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsTransactionLogs");
                    }

                    if (saveData.IsUser != Convert.ToBoolean(Isuser))
                    {
                        oldValue = saveData.IsUser.ToString();
                        newValue = Isuser;
                        saveData.IsUser = Convert.ToBoolean(Isuser);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsUser");
                    }

                    if (saveData.IsVerifier != Convert.ToBoolean(Isverifier))
                    {
                        oldValue = saveData.IsVerifier.ToString();
                        newValue = Isverifier;
                        saveData.IsVerifier = Convert.ToBoolean(Isverifier);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsVerifier");
                    }

                    if (saveData.IsWarehouse != Convert.ToBoolean(Iswarehouse))
                    {
                        oldValue = saveData.IsWarehouse.ToString();
                        newValue = Iswarehouse;
                        saveData.IsWarehouse = Convert.ToBoolean(Iswarehouse);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "HOUSE Settings IsWarehouse");
                    }

                    HouseKeeping.InsertOrUpdateHouseKeeping(saveData, dbConnection);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "saveHouseKeepingSettings", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string saveAuditSettings(string interval, string asset, string cusev, string taskA, string checklist,
            string dRoster, string group, string location, string mlog, string noti, string pOrder, string rem, string task, string offence,
            string transLog, string user, string verifier, string Isinterval, string Isasset, string Iscusev, string IstaskA, string Ischecklist,
            string IsdRoster, string Isgroup, string Islocation, string Ismlog, string Isnoti, string IspOrder, string Isrem, string Istask, string Isoffence,
            string IstransLog, string Isuser, string Isverifier, string uname, string Iswarehouse, string warehouse
            )
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var saveData = AuditSettings.GetAuditSettings(dbConnection);
                    var oldValue = string.Empty;
                    var newValue = string.Empty;
                    if (saveData.Interval != Convert.ToInt32(interval))
                    {
                        oldValue = saveData.Interval.ToString();
                        newValue = interval;
                        saveData.Interval = Convert.ToInt32(interval);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING INTERVAL");
                    }

                    if (saveData.Asset != Convert.ToInt32(asset))
                    {
                        oldValue = saveData.Asset.ToString();
                        newValue = asset;
                        saveData.Asset = Convert.ToInt32(asset);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING Asset");
                    }

                    if (saveData.CustomEvent != Convert.ToInt32(cusev))
                    {
                        oldValue = saveData.CustomEvent.ToString();
                        newValue = cusev;
                        saveData.CustomEvent = Convert.ToInt32(cusev);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING CustomEvent");
                    }

                    if (saveData.TaskAttachment != Convert.ToInt32(taskA))
                    {
                        oldValue = saveData.TaskAttachment.ToString();
                        newValue = taskA;
                        saveData.TaskAttachment = Convert.ToInt32(taskA);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING TaskAttachment");
                    }

                    if (saveData.TemplateCheckList != Convert.ToInt32(checklist))
                    {
                        oldValue = saveData.TemplateCheckList.ToString();
                        newValue = checklist;
                        saveData.TemplateCheckList = Convert.ToInt32(checklist);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING TemplateCheckList");
                    }

                    if (saveData.DutyRoaster != Convert.ToInt32(dRoster))
                    {
                        oldValue = saveData.DutyRoaster.ToString();
                        newValue = dRoster;
                        saveData.DutyRoaster = Convert.ToInt32(dRoster);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING DutyRoster");
                    }

                    if (saveData.Group != Convert.ToInt32(group))
                    {
                        oldValue = saveData.Group.ToString();
                        newValue = group;
                        saveData.Group = Convert.ToInt32(group);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING Group");
                    }

                    if (saveData.Location != Convert.ToInt32(location))
                    {
                        oldValue = saveData.Location.ToString();
                        newValue = location;
                        saveData.Location = Convert.ToInt32(location);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING Location");
                    }

                    if (saveData.MIMSLog != Convert.ToInt32(mlog))
                    {
                        oldValue = saveData.MIMSLog.ToString();
                        newValue = mlog;
                        saveData.MIMSLog = Convert.ToInt32(mlog);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING MIMSLog");
                    }

                    if (saveData.Notification != Convert.ToInt32(noti))
                    {
                        oldValue = saveData.Notification.ToString();
                        newValue = noti;
                        saveData.Notification = Convert.ToInt32(noti);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING Notification");
                    }

                    if (saveData.PostOrder != Convert.ToInt32(pOrder))
                    {
                        oldValue = saveData.PostOrder.ToString();
                        newValue = pOrder;
                        saveData.PostOrder = Convert.ToInt32(pOrder);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING PostOrder");
                    }

                    if (saveData.Reminder != Convert.ToInt32(rem))
                    {
                        oldValue = saveData.Reminder.ToString();
                        newValue = rem;
                        saveData.Reminder = Convert.ToInt32(rem);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING Reminder");
                    }

                    if (saveData.Task != Convert.ToInt32(task))
                    {
                        oldValue = saveData.Task.ToString();
                        newValue = task;
                        saveData.Task = Convert.ToInt32(task);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING Task");
                    }

                    if (saveData.Offence != Convert.ToInt32(offence))
                    {
                        oldValue = saveData.Offence.ToString();
                        newValue = offence;
                        saveData.Offence = Convert.ToInt32(offence);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING Offence");
                    }

                    if (saveData.TransactionLog != Convert.ToInt32(transLog))
                    {
                        oldValue = saveData.TransactionLog.ToString();
                        newValue = transLog;
                        saveData.TransactionLog = Convert.ToInt32(transLog);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING TransactionLog");
                    }

                    if (saveData.User != Convert.ToInt32(user))
                    {
                        oldValue = saveData.User.ToString();
                        newValue = user;
                        saveData.User = Convert.ToInt32(user);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING User");
                    }

                    if (saveData.Warehouse != Convert.ToInt32(warehouse))
                    {
                        oldValue = saveData.Warehouse.ToString();
                        newValue = warehouse;
                        saveData.Warehouse = Convert.ToInt32(warehouse);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING Warehouse");
                    }

                    if (saveData.IsWarehouse != Convert.ToBoolean(Iswarehouse))
                    {
                        oldValue = saveData.IsWarehouse.ToString();
                        newValue = Iswarehouse;
                        saveData.IsWarehouse = Convert.ToBoolean(Iswarehouse);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsWarehouse");
                    }

                    if (saveData.Verifier != Convert.ToInt32(verifier))
                    {
                        oldValue = saveData.Verifier.ToString();
                        newValue = verifier;
                        saveData.Verifier = Convert.ToInt32(verifier);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT SETTING User");
                    }
                    if (saveData.IsAsset != Convert.ToBoolean(Isasset))
                    {
                        oldValue = saveData.IsAsset.ToString();
                        newValue = Isasset;
                        saveData.IsAsset = Convert.ToBoolean(Isasset);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsAsset");
                    }


                    if (saveData.IsAlarm != Convert.ToBoolean(Iscusev))
                    {
                        oldValue = saveData.IsAlarm.ToString();
                        newValue = Iscusev;
                        saveData.IsAlarm = Convert.ToBoolean(Iscusev);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsAlarm");
                    }

                    if (saveData.IsAttachmentFiles != Convert.ToBoolean(IstaskA))
                    {
                        oldValue = saveData.IsAttachmentFiles.ToString();
                        newValue = IstaskA;
                        saveData.IsAttachmentFiles = Convert.ToBoolean(IstaskA);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsAttachmentFiles");
                    }

                    if (saveData.IsCheckList != Convert.ToBoolean(Ischecklist))
                    {
                        oldValue = saveData.IsCheckList.ToString();
                        newValue = Ischecklist;
                        saveData.IsCheckList = Convert.ToBoolean(Ischecklist);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsCheckList");
                    }

                    if (saveData.IsDutyRostersLogs != Convert.ToBoolean(IsdRoster))
                    {
                        oldValue = saveData.IsDutyRostersLogs.ToString();
                        newValue = IsdRoster;
                        saveData.IsDutyRostersLogs = Convert.ToBoolean(IsdRoster);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsDutyRostersLogs");
                    }

                    if (saveData.IsGroups != Convert.ToBoolean(Isgroup))
                    {
                        oldValue = saveData.IsGroups.ToString();
                        newValue = Isgroup;
                        saveData.IsGroups = Convert.ToBoolean(Isgroup);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsGroups");
                    }

                    if (saveData.IsLocation != Convert.ToBoolean(Islocation))
                    {
                        oldValue = saveData.IsLocation.ToString();
                        newValue = Islocation;
                        saveData.IsLocation = Convert.ToBoolean(Islocation);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsLocation");
                    }

                    if (saveData.IsMIMSLogs != Convert.ToBoolean(Ismlog))
                    {
                        oldValue = saveData.IsMIMSLogs.ToString();
                        newValue = Ismlog;
                        saveData.IsMIMSLogs = Convert.ToBoolean(Ismlog);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsMIMSLogs");
                    }

                    if (saveData.IsNotification != Convert.ToBoolean(Isnoti))
                    {
                        oldValue = saveData.IsNotification.ToString();
                        newValue = Isnoti;
                        saveData.IsNotification = Convert.ToBoolean(Isnoti);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsNotification");
                    }

                    if (saveData.IsPostOrders != Convert.ToBoolean(IspOrder))
                    {
                        oldValue = saveData.IsPostOrders.ToString();
                        newValue = IspOrder;
                        saveData.IsPostOrders = Convert.ToBoolean(IspOrder);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsPostOrders");
                    }

                    if (saveData.IsReminders != Convert.ToBoolean(Isrem))
                    {
                        oldValue = saveData.IsReminders.ToString();
                        newValue = Isrem;
                        saveData.IsReminders = Convert.ToBoolean(Isrem);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsReminders");
                    }

                    if (saveData.IsTask != Convert.ToBoolean(Istask))
                    {
                        oldValue = saveData.IsTask.ToString();
                        newValue = Istask;
                        saveData.IsTask = Convert.ToBoolean(Istask);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsTask");
                    }

                    if (saveData.IsTicketing != Convert.ToBoolean(Isoffence))
                    {
                        oldValue = saveData.IsTicketing.ToString();
                        newValue = Isoffence;
                        saveData.IsTicketing = Convert.ToBoolean(Isoffence);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsTicketing");
                    }

                    if (saveData.IsTransactionLogs != Convert.ToBoolean(IstransLog))
                    {
                        oldValue = saveData.IsTransactionLogs.ToString();
                        newValue = IstransLog;
                        saveData.IsTransactionLogs = Convert.ToBoolean(IstransLog);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsTransactionLogs");
                    }

                    if (saveData.IsUser != Convert.ToBoolean(Isuser))
                    {
                        oldValue = saveData.IsUser.ToString();
                        newValue = Isuser;
                        saveData.IsUser = Convert.ToBoolean(Isuser);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsUser");
                    }

                    if (saveData.IsVerifier != Convert.ToBoolean(Isverifier))
                    {
                        oldValue = saveData.IsVerifier.ToString();
                        newValue = Isverifier;
                        saveData.IsVerifier = Convert.ToBoolean(Isverifier);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "AUDIT Settings IsVerifier");
                    }

                    AuditSettings.InsertOrUpdateAuditSettings(saveData, dbConnection);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "saveAuditSettings", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }

        [WebMethod]
        public static string save3rdPartyData(int id, string name, string ip, string port,string username,string password,string type,string uname
            )
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                var newthird = new ThirdPartySeting();
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    if (id > 0)
                    {
                        newthird = ThirdPartySeting.GetThirdPartSettingById(id, dbConnection);
                        var oldValue = string.Empty;
                        var newValue = string.Empty;

                        if (newthird.IPAddress != ip)
                        {
                            oldValue = newthird.IPAddress;
                            newValue = ip;
                            newthird.IPAddress = ip;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "3RD PARTY SYSTEM IP-" + id);
                        }

                        if (newthird.ServerIP != ip)
                        {
                            oldValue = newthird.ServerIP;
                            newValue = ip;
                            newthird.ServerIP = ip;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "3RD PARTY SYSTEM SERVER IP-" + id);
                        }

                        if (newthird.Name != name)
                        {
                            oldValue = newthird.Name;
                            newValue = name;
                            newthird.Name = name;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "3RD PARTY SYSTEM SERVER Name-" + id);
                        }

                        if (newthird.UserName != username)
                        {
                            oldValue = newthird.UserName;
                            newValue = username;
                            newthird.UserName = username;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "3RD PARTY SYSTEM SERVER UserName-" + id);
                        }

                        if (newthird.Password != password)
                        {
                            oldValue = newthird.Password;
                            newValue = password;
                            newthird.Password = password;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "3RD PARTY SYSTEM SERVER Password-" + id);
                        }
                        if (newthird.Port != Convert.ToInt32(port))
                        {
                            oldValue = newthird.Port.ToString();
                            newValue = port;
                            newthird.Port = Convert.ToInt32(port);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "3RD PARTY SYSTEM SERVER Port-" + id);
                        }
                        if (newthird.ServerPort != Convert.ToInt32(port))
                        {
                            oldValue = newthird.ServerPort.ToString();
                            newValue = port;
                            newthird.ServerPort = Convert.ToInt32(port);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "3RD PARTY SYSTEM SERVER ServerPort-" + id);
                        }

                        newthird.CreatedBy = userinfo.Username;
                        newthird.CreatedDate = CommonUtility.getDTNow();
                        var thirdSettingType = ThirdPartySettingType.GetThirdPartySettingTypeById(Convert.ToInt32(type), dbConnection);
                        newthird.VMSType = thirdSettingType;
                        ThirdPartySeting.InsertorUpdateThirdPartySeting(newthird, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        newthird.Name = name;
                        newthird.IPAddress = ip;
                        newthird.Port = Convert.ToInt32(port);
                        newthird.ServerIP = ip;
                        newthird.ServerPort = Convert.ToInt32(port);
                        newthird.UserName = username;
                        newthird.Password = password;
                        newthird.CreatedBy = userinfo.Username;
                        newthird.CreatedDate = CommonUtility.getDTNow();
                        var thirdSettingType = ThirdPartySettingType.GetThirdPartySettingTypeById(Convert.ToInt32(type), dbConnection);
                        newthird.VMSType = thirdSettingType;

                        ThirdPartySeting.InsertorUpdateThirdPartySeting(newthird, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "save3rdPartyData", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string saveNewSystemData(int id, string name,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var newthird = new ThirdPartySettingType();
                var retstring = string.Empty;
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    if (id > 0)
                    {

                    }
                    else
                    {
                        newthird.Name = name;
                        retstring = ThirdPartySettingType.InsertOrUpdateThirdPartySettingType(newthird, dbConnection, dbConnectionAudit, true).ToString();
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "saveNewSystemData", ex, dbConnection, userinfo.SiteId);
                    return "FAIL";
                }
                return retstring;
            }
        }
        [WebMethod]
        public static string saveVerifierSettings(string ANPR, string FRS,string uname
            )
        {

            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            { 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    var verServs = VerifierService.GetAllVerifierService(dbConnection);
                    foreach (var ver in verServs)
                    {
                        if (ver.Type == (int)VerifierService.VerifierServiceTypes.ANPR)
                        {
                            if (!string.IsNullOrEmpty(ANPR))
                            {
                                ver.UpdatedBy = userinfo.Username;
                                ver.UpdatedDate = CommonUtility.getDTNow();
                                ver.Url = ANPR;
                                VerifierService.InsertOrUpdateVerifierService(ver, dbConnection, dbConnectionAudit, true);
                            }
                        }
                        else if (ver.Type == (int)VerifierService.VerifierServiceTypes.FRS)
                        {
                            if (!string.IsNullOrEmpty(FRS))
                            {
                                ver.UpdatedBy = userinfo.Username;
                                ver.UpdatedDate = CommonUtility.getDTNow();
                                ver.Url = FRS;
                                VerifierService.InsertOrUpdateVerifierService(ver, dbConnection, dbConnectionAudit, true);
                            }
                        }
                    }
                    if (verServs.Count == 0)
                    {
                        var newVerANPR = new VerifierService();
                        var newVerFRS = new VerifierService();
                        if (!string.IsNullOrEmpty(ANPR))
                        {
                            newVerANPR.CreatedBy = userinfo.Username;
                            newVerANPR.CreatedDate = CommonUtility.getDTNow();
                            newVerANPR.UpdatedBy = userinfo.Username;
                            newVerANPR.UpdatedDate = CommonUtility.getDTNow();
                            newVerANPR.Url = ANPR;
                            newVerANPR.Type = (int)VerifierService.VerifierServiceTypes.ANPR;
                            VerifierService.InsertOrUpdateVerifierService(newVerANPR, dbConnection, dbConnectionAudit, true);
                        }
                        if (!string.IsNullOrEmpty(FRS))
                        {
                            newVerFRS.CreatedBy = userinfo.Username;
                            newVerFRS.CreatedDate = CommonUtility.getDTNow();
                            newVerFRS.UpdatedBy = userinfo.Username;
                            newVerFRS.UpdatedDate = CommonUtility.getDTNow();
                            newVerFRS.Url = FRS;
                            newVerFRS.Type = (int)VerifierService.VerifierServiceTypes.FRS;
                            VerifierService.InsertOrUpdateVerifierService(newVerFRS, dbConnection, dbConnectionAudit, true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices.aspx", "saveVerifierSettings", ex, dbConnection, userinfo.SiteId);
                    return "FAIL";
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static List<string> getThirdPartyDataInfo(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var newthird = ThirdPartySeting.GetThirdPartSettingById(id, dbConnection);
                if (newthird != null)
                {
                    listy.Add(newthird.Name);
                    listy.Add(newthird.IPAddress);
                    listy.Add(newthird.Port.ToString());
                    listy.Add(newthird.UserName);
                    listy.Add(newthird.Password);
                    listy.Add(newthird.VMSType.Id.ToString());
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getThirdPartyDataInfo", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        //User Profile
        [WebMethod]
        public static string changePW(int id, string password,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT"; 
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                return 0;
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return 0; 
                    }

                    if (userinfo.ID > 0)
                    {
                        var getuser = userinfo;
                        getuser.FirstName = firstname;
                        getuser.LastName = lastname;
                        getuser.Email = emailaddress;

                        if (getuser.RoleId != role)
                        {
                            getuser.RoleId = role;
                            if (role == (int)Role.Manager)
                            {
                                var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                                if (getMang != null)
                                    UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                            }
                            else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                            {
                                var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                                if (getdir != null)
                                    DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                            }
                            else
                            {
                                var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                                if (getdir != null)
                                    DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                                var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                                if (getMang != null)
                                    UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                            }
                        }
                        if (getuser.RoleId == (int)Role.Manager)
                        {

                            var dirUser = Users.GetUserById(supervisor, dbConnection);
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                            if (dirUser != null)
                            {
                                if (!string.IsNullOrEmpty(dirUser.Username))
                                {
                                    List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                           
                                            }};
                                    DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                                }
                            }
                        }
                        else if (getuser.RoleId == (int)Role.Operator)
                        {
                            if (supervisor > 0)
                            {
                                var manUser = Users.GetUserById(supervisor, dbConnection);

                                List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                                UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                            }
                        }
                        else if (getuser.RoleId == (int)Role.UnassignedOperator)
                        {
                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                        if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                        {
                            return userinfo.ID;
                        }
                        else
                            return 0;
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Devices", "addUserProfile", ex, dbConnection, userinfo.SiteId);
                }
                return userinfo.ID;
            }
        }

        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                // var test = Users.GetUserById(id, dbConnection);
                var customData = Users.GetUserById(userinfo.ID, dbConnection);
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                  //  var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                    {
                     //   if (pushDev != null)
                     //   {
                        //    if (!string.IsNullOrEmpty(pushDev.Username))
                       //     {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                         //   }
                          //  else
                         //   {
                         //       monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                         //       fontstyle = "";
                         //   }
                      //  }
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("VerifierPage", "getUserProfileData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static string addCam(string MAC, string CAM, string uname)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {

                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    ClientCamera.DeleteClientCamerabyCameraName(CAM, dbConnection);
                    var cameraObj = new ClientCamera();
                    cameraObj.MacAddress = MAC;
                    cameraObj.CreatedBy = userinfo.Username;
                    cameraObj.CreatedDate = CommonUtility.getDTNow();
                    cameraObj.UpdatedDate = CommonUtility.getDTNow();
                    cameraObj.CameraName = CAM;
                    ClientCamera.InsertorUpdateClientCamera(cameraObj, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception er)
                {
                    MIMSLog.MIMSLogSave("Devices", "addCam", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string assignMang(string MAC, string ID,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                        var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
 
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    ClientManager.DeleteClientManagerByMacAddress(MAC, dbConnection);
                    var clientMan = new ClientManager();
                    clientMan.CreatedBy = userinfo.Username;
                    clientMan.MacAddress = MAC;
                    clientMan.UpdatedDate = CommonUtility.getDTNow();
                    clientMan.UserId = Convert.ToInt32(ID);
                    clientMan.CreatedDate = CommonUtility.getDTNow();
                    ClientManager.InsertorUpdateClientManager(clientMan, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception er)
                {
                    MIMSLog.MIMSLogSave("Devices", "assignMang", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string removeCam(string MAC, string CAM,string uname)
        {
                                    var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
 
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT"; 
                    }

                    ClientCamera.DeleteClientCamerabyCameraName(CAM, dbConnection);
                }
                catch (Exception er)
                {
                    MIMSLog.MIMSLogSave("Devices", "removeCam", er, dbConnection, userinfo.SiteId);
                    return er.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static List<string> getCameras(string MAC, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                { 
                    listy.Add("LOGOUT");
                    return listy;
                }

                var cCams = ClientCamera.GetAllClientCamerasByMacAddress(MAC, dbConnection);
                foreach (var cams in cCams)
                {
                    listy.Add(cams.CameraName);
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Devices", "getCameras", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
    }
}