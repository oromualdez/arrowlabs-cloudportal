﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class  LocationAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int ID { get; set; }
        public string LocationDesc { get; set; }
        public string LocationDesc_AR { get; set; }
        public int LocationTypeId { get; set; }
        public string LocationTypeDesc { get; set; }
        public string FullLocationDesc_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int ParentLocationId { get; set; }
        public string ParentLocationName { get; set; }
        public int CustomerId { get; set; }
        public int SiteId { get; set; }

        private static LocationAudit GetLocationAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            
            var locationAudit = new LocationAudit();
            locationAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                locationAudit.ID = Convert.ToInt16(reader["Id"].ToString());
            if (columns.Contains("LOCATIONDESC") && reader["LOCATIONDESC"] != DBNull.Value)
                locationAudit.LocationDesc = reader["LOCATIONDESC"].ToString();
            if (columns.Contains("LOCATIONDESC_AR") && reader["LOCATIONDESC_AR"] != DBNull.Value)
                locationAudit.LocationDesc_AR = reader["LOCATIONDESC_AR"].ToString();
            if (columns.Contains("FULLLOCATIONDESC_AR") && reader["FULLLOCATIONDESC_AR"] != DBNull.Value)
                locationAudit.FullLocationDesc_AR = reader["FULLLOCATIONDESC_AR"].ToString();
            if (columns.Contains("LOCATIONTYPEID") && reader["LOCATIONTYPEID"] != DBNull.Value)
                locationAudit.LocationTypeId = Convert.ToInt16(reader["LOCATIONTYPEID"].ToString());
            if (columns.Contains("CreateDate") && reader["CreateDate"] != DBNull.Value)
                locationAudit.CreatedDate = Convert.ToDateTime(reader["CreateDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                locationAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("LOCATIONTYPEDESC") && reader["LOCATIONTYPEDESC"] != DBNull.Value)
                locationAudit.LocationTypeDesc = reader["LOCATIONTYPEDESC"].ToString();
            if (columns.Contains("ParentLocationAuditId") && reader["ParentLocationAuditId"] != DBNull.Value)
                locationAudit.ParentLocationId = (String.IsNullOrEmpty(reader["ParentLocationAuditId"].ToString())) ? 0 : Convert.ToInt16(reader["ParentLocationAuditId"].ToString());
            if (columns.Contains("ParentLocationAuditName") && reader["ParentLocationAuditName"] != DBNull.Value)
                locationAudit.ParentLocationName = reader["ParentLocationAuditName"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                locationAudit.SiteId = Convert.ToInt16(reader["SiteId"].ToString());
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                locationAudit.CustomerId = Convert.ToInt16(reader["CustomerId"].ToString());

            return locationAudit;
        }
        public static List<LocationAudit> GetAllLocationAudit(string dbConnection)
        {
            var locations = new List<LocationAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationAudit", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var loc = GetLocationAuditFromSqlReader(reader);
                    locations.Add(loc);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }
        public static bool InsertLocationAudit(LocationAudit location, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(location.BaseAudit, dbConnection);

            if (location != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertLocationAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = location.ID;
                    cmd.Parameters.Add("@LocationDesc", SqlDbType.NVarChar).Value = location.LocationDesc;
                    cmd.Parameters.Add("@LocationDesc_AR", SqlDbType.NVarChar).Value = location.LocationDesc_AR;
                    cmd.Parameters.Add("@LocationTypeId", SqlDbType.Int).Value = location.LocationTypeId;
                    cmd.Parameters.Add("@FullLocationDesc_AR", SqlDbType.NVarChar).Value = location.FullLocationDesc_AR;
                    cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = location.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = location.CreatedBy;
                    cmd.Parameters.Add("@ParentLocationId", SqlDbType.Int).Value = location.ParentLocationId;
                    cmd.Parameters.Add("@ParentLocationName", SqlDbType.NVarChar).Value = location.ParentLocationName;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = location.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = location.CustomerId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
