﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
  public  class SiteAudit
  {
      public BaseAudit BaseAudit { get; set; }
      public int Id { get; set; }
      public string Name { get; set; }
      public string CreatedBy { get; set; }
      public DateTime? UpdatedDate { get; set; }
      public DateTime? CreatedDate { get; set; }
      public string UpdatedBy { get; set; }
      public string Long { get; set; }
      public string Lati { get; set; }

      public string RssFeedUrl { get; set; }

      public string MessageBoardVideoPath { get; set; }
      public static bool InsertSiteAudit(SiteAudit assetAudit, string dbConnection)
      {
          var baseAuditId = BaseAudit.InsertBaseAudit(assetAudit.BaseAudit, dbConnection);

          if (assetAudit != null)
          {
              using (var connection = new SqlConnection(dbConnection))
              {
                  connection.Open();
                  var cmd = new SqlCommand("InsertSiteAudit", connection);

                  cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                  cmd.Parameters.Add("@Id", SqlDbType.Int).Value = assetAudit.Id;
                  cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = assetAudit.Name;
                  cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                  cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = assetAudit.CreatedBy;
                  cmd.Parameters.Add("@UpdatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                  cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar).Value = assetAudit.CreatedBy;
                  cmd.Parameters.Add(new SqlParameter("@Lati", SqlDbType.NVarChar));
                  cmd.Parameters["@Lati"].Value = assetAudit.Lati;

                  cmd.Parameters.Add(new SqlParameter("@Long", SqlDbType.NVarChar));
                  cmd.Parameters["@Long"].Value = assetAudit.Long;

                  cmd.Parameters.Add(new SqlParameter("@MessageBoardVideoPath", SqlDbType.NVarChar));
                  cmd.Parameters["@MessageBoardVideoPath"].Value = assetAudit.MessageBoardVideoPath;


                  cmd.Parameters.Add(new SqlParameter("@RssFeedUrl", SqlDbType.NVarChar));
                  cmd.Parameters["@RssFeedUrl"].Value = assetAudit.RssFeedUrl;


                  cmd.CommandType = CommandType.StoredProcedure;
                  cmd.ExecuteNonQuery();
              }
          }
          return true;
      }
    }
}
