﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class FinderDepartment
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }

        public int CustomerId { get; set; }

        public static List<FinderDepartment> GetAllFinderDepartment(string dbConnection)
        {
            var plateCodes = new List<FinderDepartment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFinderDepartment", connection);


                sqlCommand.CommandText = "GetAllFinderDepartment";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var plateCode = new FinderDepartment();

                    plateCode.Id = Convert.ToInt32(reader["Id"].ToString());
                    plateCode.Name = reader["Name"].ToString();
                    plateCode.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }

        public static FinderDepartment GetFinderDepartmentByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            FinderDepartment plateCode = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetFinderDepartmentByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    plateCode = new FinderDepartment();

                    plateCode.Id = Convert.ToInt32(reader["Id"].ToString());
                    plateCode.Name = reader["Name"].ToString();
                    plateCode.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                }
                connection.Close();
                reader.Close();
            }
            return plateCode;
        }

        public static FinderDepartment GetFinderDepartmentById(int id, string dbConnection)
        {
            FinderDepartment plateCode = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetFinderDepartmentById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandText = "GetFinderDepartmentById";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    plateCode = new FinderDepartment();

                    plateCode.Id = Convert.ToInt32(reader["Id"].ToString());
                    plateCode.Name = reader["Name"].ToString();
                    plateCode.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                }
                connection.Close();
                reader.Close();
            }
            return plateCode;
        }
        public static List<FinderDepartment> GetAllFinderDepartmentBySiteId(int siteid, string dbConnection)
        {
            var plateCodes = new List<FinderDepartment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFinderDepartmentBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.CommandText = "GetAllFinderDepartmentBySiteId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var plateCode = new FinderDepartment();

                    plateCode.Id = Convert.ToInt32(reader["Id"].ToString());
                    plateCode.Name = reader["Name"].ToString();
                    plateCode.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }
        public static List<FinderDepartment> GetAllFinderDepartmentByLevel5(int siteid, string dbConnection)
        {
            var plateCodes = new List<FinderDepartment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFinderDepartmentByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid;
                sqlCommand.CommandText = "GetAllFinderDepartmentByLevel5";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var plateCode = new FinderDepartment();

                    plateCode.Id = Convert.ToInt32(reader["Id"].ToString());
                    plateCode.Name = reader["Name"].ToString();
                    plateCode.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }
        public static List<FinderDepartment> GetAllFinderDepartmentByCId(int siteid, string dbConnection)
        {
            var plateCodes = new List<FinderDepartment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFinderDepartmentByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteid;
                sqlCommand.CommandText = "GetAllFinderDepartmentByCId";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var plateCode = new FinderDepartment();

                    plateCode.Id = Convert.ToInt32(reader["Id"].ToString());
                    plateCode.Name = reader["Name"].ToString();
                    plateCode.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
                    plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();

                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }

        public static int InsertOrUpdateFinderDepartment(FinderDepartment vehicleColor, string dbConnection,
    string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var id = 0;
            if (vehicleColor != null)
            {
                id = vehicleColor.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateFinderDepartment", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = vehicleColor.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = vehicleColor.Name;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = vehicleColor.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = vehicleColor.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = vehicleColor.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = vehicleColor.CreatedBy.ToLower();

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = vehicleColor.CustomerId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateFinderDepartment";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            vehicleColor.Id = id;
                            var audit = new FinderDepartmentAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, vehicleColor.CreatedBy);
                            Reflection.CopyProperties(vehicleColor, audit);
                            FinderDepartmentAudit.InsertFinderDepartmentAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertFinderDepartmentAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        public static bool DeleteFinderDepartmentById(int colorCode, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteFinderDepartmentById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = colorCode;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteFinderDepartmentById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
