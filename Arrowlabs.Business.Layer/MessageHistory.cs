﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
   public class MessageHistory
    {
            [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        public int CustomerId { get; set; }
        [DataMember]
        public int SiteId { get; set; }

        [DataMember]
        public int NotificationId { get; set; }

        [DataMember]
        public bool IsRead { get; set; }
       public string CustomerUName {get;set;}
        private static MessageHistory GetMessageHistoryFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var messageHistory = new MessageHistory();
            if (reader["Id"] != DBNull.Value)
                messageHistory.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                messageHistory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                messageHistory.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                messageHistory.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                messageHistory.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains("NotificationId") && reader["NotificationId"] != DBNull.Value)
                messageHistory.NotificationId = Convert.ToInt32(reader["NotificationId"].ToString());

            if (columns.Contains("IsRead") && reader["IsRead"] != DBNull.Value)
                messageHistory.IsRead = Convert.ToBoolean(reader["IsRead"].ToString());

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                messageHistory.CustomerUName = reader["CustomerUName"].ToString();
            
            
            return messageHistory;
        }
        public static bool InsertorUpdateMessageHistory(MessageHistory messageHistory, string dbConnection,
         string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (messageHistory.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (messageHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateMessageHistory", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = messageHistory.Id;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = messageHistory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = messageHistory.CustomerId;
                    
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = messageHistory.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = messageHistory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@IsRead", SqlDbType.Bit));
                    cmd.Parameters["@IsRead"].Value = messageHistory.IsRead;
                    
                    cmd.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.NVarChar));
                    cmd.Parameters["@NotificationId"].Value = messageHistory.NotificationId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                    id = (int)returnParameter.Value;
                    if (id > 0)
                    {
                        messageHistory.Id = id;
                    }
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new MessageHistoryAudit();

                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, messageHistory.CreatedBy);
                            Reflection.CopyProperties(messageHistory, audit);
                            MessageHistoryAudit.InsertMessageHistoryAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateWatchInfo", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static List<MessageHistory> GetMessageHistoryByNotificationId(int notificationId, string dbConnection)
        {
            var messageHistoryList = new List<MessageHistory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetMessageHistoryByNotificationId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.Int));
                sqlCommand.Parameters["@NotificationId"].Value = notificationId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetMessageHistoryFromSqlReader(reader);
                    messageHistoryList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return messageHistoryList;
        }

    }
}
