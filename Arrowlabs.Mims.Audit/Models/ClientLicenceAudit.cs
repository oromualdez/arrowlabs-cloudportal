﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ClientLicenceAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string ClientId { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        public string TotalDevices { get; set; }
        public string Curfew { get; set; }
        public string RemainingDevices { get; set; }
        public string UsedDevices { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string SerialNo { get; set; }
        public string ExpiryDate { get; set; }
        public bool LicenseInUse { get; set; }
        public bool DatabaseStatus { get; set; }
        

        public static void InsertClientLicenceAudit(ClientLicenceAudit clientLicence, string dbConnection)
        {
            

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("InsertLicenceClientDeviceAudit", connection);

                var baseAuditId = BaseAudit.InsertBaseAudit(clientLicence.BaseAudit, dbConnection);
                cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                cmd.Parameters.Add("@ClientID", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.ClientId, true);
                cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.Name, true);
                cmd.Parameters.Add("@PhoneNo", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.PhoneNo, true);
                cmd.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.Address, true);
                cmd.Parameters.Add("@TotalDevices", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.TotalDevices, true);
                cmd.Parameters.Add("@RemainingDevices", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.RemainingDevices, true);
                cmd.Parameters.Add("@CreatedDate", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.CreatedDate, true);
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.CreatedBy, true);
                cmd.Parameters.Add("@SerialNo", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.SerialNo, true);
                cmd.Parameters.Add("@ExpiryDate", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.ExpiryDate, true);
                cmd.Parameters.Add("@Curfew", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.Curfew, true);
                cmd.Parameters.Add("@UsedDevices", SqlDbType.NVarChar).Value = Encrypt.EncryptData(clientLicence.UsedDevices, true);
                
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
}
