﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class VerifierAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }         
        public int Type { get; set; }         
        public float Longitude { get; set; }         
        public float Latitude { get; set; }         
        public string CreatedBy { get; set; }         
        public DateTime CreatedDate { get; set; }         
        public int CaseId { get; set; }         
        public byte[] SentImageFile { get; set; }         
        public int Request { get; set; }         
        public string Reason { get; set; }         
        public string TransactionID { get; set; }         
        public string TypeName { get; set; }         
        public byte[] ResultImage { get; set; }         
        public string Score { get; set; }         
        public string Rank { get; set; }
        public int SiteId { get; set; }
        public enum VerifierTypes
        {
            FRS = 0,
            ANPR = 1
        }
        public enum RequestTypes
        {
            Enroll = 0,
            Verify = 1,
            Enrolled = 2,
            Rejected = 3
        }
        public static List<string> GetVerifierTypes()
        {
            return Enum.GetNames(typeof(VerifierTypes)).ToList();
        }

        private static VerifierAudit GetVerifierAuditFromSqlReader(SqlDataReader reader)
        {
            var verifier = new VerifierAudit();
            verifier.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                verifier.Id = Convert.ToInt16(reader["Id"].ToString());
            if (columns.Contains("CaseId") && reader["CaseId"] != DBNull.Value)
                verifier.CaseId = Convert.ToInt16(reader["CaseId"].ToString());
            if (columns.Contains("SentImageFile") && reader["SentImageFile"] != DBNull.Value)
                verifier.SentImageFile = (byte[])reader["SentImageFile"];
            if (columns.Contains("TransactionID") && reader["TransactionID"] != DBNull.Value)
                verifier.TransactionID = reader["TransactionID"].ToString();
            if (columns.Contains("Reason") && reader["Reason"] != DBNull.Value)
                verifier.Reason = reader["Reason"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                verifier.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                verifier.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                verifier.Longitude = Convert.ToSingle(reader["Longitude"].ToString());
            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                verifier.Latitude = Convert.ToSingle(reader["Latitude"].ToString());
            if (columns.Contains("Request") && reader["Request"] != DBNull.Value)
                verifier.Request = Convert.ToInt16(reader["Request"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                verifier.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (columns.Contains("Type") && reader["Type"] != DBNull.Value)
            {
                verifier.Type = Convert.ToInt16(reader["Type"].ToString());
                if (verifier.Type == (int)VerifierTypes.ANPR)
                    verifier.TypeName = "ANPR";
                else
                    verifier.TypeName = "FRS";
            }
            
            return verifier;
        }
        public static List<VerifierAudit> GetAllVerifierAudit(string dbConnection)
        {
            var verifiers = new List<VerifierAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVerifier", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var verifier = GetVerifierAuditFromSqlReader(reader);
                    verifiers.Add(verifier);
                }
                connection.Close();
                reader.Close();
            }
            return verifiers;
        }
        public static int InsertVerifierAudit(VerifierAudit verifier, string dbConnection)
        {
            int id = 0;
            if (verifier != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertVerifierAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(verifier.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = verifier.Id;

                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = verifier.Type;

                    cmd.Parameters.Add("@Longitude", SqlDbType.Float).Value = verifier.Longitude;

                    cmd.Parameters.Add("@Latitude", SqlDbType.Float).Value = verifier.Latitude;

                    cmd.Parameters.Add("@Reason", SqlDbType.NVarChar).Value = verifier.Reason;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = verifier.CreatedBy;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.NVarChar).Value = verifier.CreatedDate;

                    cmd.Parameters.Add("@CaseId", SqlDbType.Int).Value = verifier.CaseId;

                    cmd.Parameters.Add("@Request", SqlDbType.Int).Value = verifier.Request;

                    cmd.Parameters.Add("@TransactionID", SqlDbType.NVarChar).Value = verifier.TransactionID;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = verifier.SiteId;  
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
