﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArrowLabs.Licence.Portal
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var settings = ConfigSettings.GetConfigSettings(CommonUtility.dbConnection);
                var ipaddress = settings.ChatServerUrl + "/signalr";
                Page.ClientScript.RegisterClientScriptInclude("script", ipaddress + "/hubs");
                var user = HttpContext.Current.User.Identity;
                if (user != null)
                {
                    var getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, CommonUtility.dbConnection);
                    if (getClientLic != null)
                    {
                        inciDisplay = "none";
                        tickDisplay = "none";
                        taskDisplay = "none";
                        veriDisplay = "none";//"none";
                        wareDisplay = "none";
                        survDisplay = "none";
                        actDisplay = "none";
						assetDisplay = "none";
                        if(getClientLic.isSurveillance)
                        {
                            survDisplay = "block";
                        }
                        if (getClientLic.isIncident)
                        {
                            inciDisplay = "block";
                        }
                        if (getClientLic.isTicketing)
                        {
                            tickDisplay = "block";
                        }
                        if (getClientLic.isTask)
                        {
                            taskDisplay = "block";
                        }
                        if (getClientLic.isVerification)
                        {
                            veriDisplay = "block";
                        }
                        if (getClientLic.isLostandFound)
                        {
                            wareDisplay = "block";
                        }
						                            if (getClientLic.isWarehouse)
                            {
                                assetDisplay = "block";
                            } 
                    }
                    var uProfile = Users.GetUserByName(user.Name, CommonUtility.dbConnection);
                    if (uProfile != null)
                    {
                        deviceDisplay = "block";
                        site2Display = "block";
                        if (uProfile.RoleId == (int)Role.MessageBoardUser)
                        {
                            dashDisplay = "none";
                            inciDisplay = "none";
                            tickDisplay = "none";
                            taskDisplay = "none";
                            repoDisplay = "none";
                            messDisplay = "none";
                            veriDisplay = "none";
                            deviceDisplay = "none";
                            userDisplay = "none";
                            survDisplay = "none";
                            wareDisplay = "none";
                            assetDisplay = "none";
                            siteDisplay = "none";
                            site2Display = "none";
                        }
                        else if (uProfile.RoleId != (int)Role.SuperAdmin)
                        {
                            dashDisplay = "none";
                            inciDisplay = "none";
                            tickDisplay = "none";
                            taskDisplay = "none";
                            repoDisplay = "none";
                            messDisplay = "none";
                            veriDisplay = "none";
                            deviceDisplay = "none";
                            userDisplay = "block";
                            survDisplay = "none";
                            wareDisplay = "none";
							assetDisplay = "none";
                            //if (uProfile.RoleId == (int)Role.Admin)
                            //{
                                //userDisplay = "block";
                               // deviceDisplay = "block";
                            //}
                            siteDisplay = "none";
                            site2Display = "none";

                            if (uProfile.RoleId == (int)Role.CustomerSuperadmin)
                                site2Display = "block";

                            var uModules = UserModules.GetAllModulesByUsername(user.Name, CommonUtility.dbConnection);
                            foreach (var mods in uModules)
                            {
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Dash)
                                {
                                    var isIncident = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Alarms).ToList();
                                    if (isIncident.Count > 0)
                                    {
                                        dashDisplay = "block";
                                    }
                                    else
                                    {
                                        var isTask = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Tasks).ToList();
                                        if (isTask.Count > 0)
                                        {
                                            dashDisplay = "block";
                                        }
                                        else
                                        {
                                            var isOther = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Database).ToList();
                                            //var isVeri = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Verifier).ToList();
                                            if (isOther.Count > 0)//&& isVeri.Count > 0)
                                            {
                                                dashDisplay = "block";
                                            }
                                        }
                                    }
                                }
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Alarms)
                                {
                                    inciDisplay = "block";
                                }
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Database)
                                {
                                    tickDisplay = "block";
                                }
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks)
                                {
                                    taskDisplay = "block";
                                }
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Reports)
                                {
                                    repoDisplay = "block";
                                }
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Notification)
                                {
                                    messDisplay = "block";
                                }
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Verifier)
                                {
                                    veriDisplay = "block";
                                }
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Devices)
                                {
                                    deviceDisplay = "block";
                                }
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Activity)
                                {
                                    actDisplay = "block";
                                }
                                
                                //if (mods.ModuleId == (int)Accounts.ModuleTypes.System)
                                //{
                                //    userDisplay = "block";
                                //}
                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Surveillance)
                                {
                                    survDisplay = "block";
                                }
                                  if (mods.ModuleId == (int)Accounts.ModuleTypes.LostandFound)
                                {
                                    wareDisplay = "block";
                                }
								if (mods.ModuleId == (int)Accounts.ModuleTypes.Warehouse || mods.ModuleId == (int)Accounts.ModuleTypes.ServerKey)
                                {
                                    assetDisplay = "block";
                                }
                            }
                        }
                        else
                        {
                            actDisplay = "block";
                        }
                        if (!string.IsNullOrEmpty(user.Name))
                            getEventStatus(CommonUtility.dbConnection, user.Name);
                    }
                }
            }
            catch(Exception er)
            {
                MIMSLog.MIMSLogSave("Site.Master", "Page_Load", er, CommonUtility.dbConnection);
            }
        }
        protected string deviceDisplay;
        protected string siteDisplay;
        protected string site2Display;

        protected string assetDisplay;
         protected string userDisplay;
         protected string veriDisplay;
         protected string messDisplay;
         protected string repoDisplay;
         protected string tickDisplay;
         protected string inciDisplay;
         protected string dashDisplay;
         protected string taskDisplay;
         protected string wareDisplay;
         protected string survDisplay;
         protected string actDisplay;
        
        protected string pendingIncidents;
        protected string pendingTasks;
        protected string pendingVerifier;
        protected string pendingTicketing;
        protected string pendingMessages;
        public void getEventStatus(string dbConnection,string username)
        {
            var cusEvents = new List<CustomEvent>();
            var totaltaskslist = new List<UserTask>();
            var pendingIncidentCount = 0;
            var pendingTicketCount = 0;
            var pendingTasksCount = 0;
            //deviceDisplay = "block";
            try
            {
                var userinfo = Users.GetUserByName(username, dbConnection);
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                        totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                        totaltaskslist = totaltaskslist.Where(i => i.SiteId == userinfo.SiteId).ToList();
                        //deviceDisplay = "none";
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                        totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection));
                        var userGroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                        foreach (var group in userGroups)
                        {
                            totaltaskslist.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeId((int)TaskAssigneeType.Group, group.Id, dbConnection));
                        }
                        foreach (var manguser in managerusers)
                        {
                            cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                            totaltaskslist.AddRange(UserTask.GetAllTasksOfTeamByManagerId(manguser, dbConnection));
                        }
                        var grouped = totaltaskslist.GroupBy(item => item.Id);
                        totaltaskslist = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                        totaltaskslist = totaltaskslist.Where(i => i.SiteId == userinfo.SiteId).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId,dbConnection);
                        
                        totaltaskslist = UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndSiteId(DateTime.Now.Date, userinfo.SiteId,dbConnection);
                    } 
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                        //{
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));
                        totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndLevel5(DateTime.Now.Date, userinfo.ID, dbConnection));
                        //}

                        var groupedd = totaltaskslist.GroupBy(item => item.Id);
                        totaltaskslist = groupedd.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection));
                        totaltaskslist.AddRange(UserTask.GetAllTaskByDateExcludingRecurringTaskParentAndCId(CommonUtility.getDTNow().Date, userinfo.CustomerInfoId, dbConnection));
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                        totaltaskslist = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(DateTime.Now.Date, dbConnection);
                    }
                    foreach (var task in totaltaskslist)
                    {
                        if (task.Status == (int)TaskStatus.Pending)
                        {
                            if (task.IsRecurring && task.RecurringParentId == 0)
                            {

                            }
                            else
                            {
                                if (task.StartDate.Value.Date <= CommonUtility.getDTNow().Date)
                                    pendingTasksCount++;
                            }
                        }
                    }
                    var userList = new List<string>();
                    var offenceuserList = new List<string>();
                    if (cusEvents.Count > 0)
                    {
                        var grouped = cusEvents.GroupBy(item => item.EventId);
                        cusEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                        foreach (var evs in cusEvents)
                        {
                            if (evs.EventType != (int)CustomEvent.EventTypes.DriverOffence)
                            {
                                if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                {
                                    pendingIncidentCount++; //pending count
                                }
                            }
                            else
                            {
                                //if (!evs.Handled)//evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                //{
                                //    pendingTicketCount++; //pending count
                                //}
                                //var gTicket = DriverOffence.GetDriverOffenceById(evs.Identifier, dbConnection);
                                //if (gTicket != null)
                                //{
                                //    if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                                //    {
                                //        pendingTicketCount++;
                                //    } 
                                //}
                            }
                        }
                    }
                    var inprotasks = new List<Verifier>();

                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                        inprotasks = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Verify, dbConnection);
                    else
                        inprotasks = Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(userinfo.ID, (int)Verifier.RequestTypes.Verify, dbConnection);

                    var cusEvents2 = new List<CustomEvent>();

                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        cusEvents2 = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.DriverOffence, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        cusEvents2 = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);

                        cusEvents2 = cusEvents2.Where(i => i.UserName == userinfo.Username).ToList();
                    }
                    else
                    {
                        cusEvents2 = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);

                    }
                    if (cusEvents2.Count > 0)
                    {
                        cusEvents2 = cusEvents2.Where(i => !i.Handled).ToList();
                        var grouped = cusEvents2.GroupBy(item => item.EventId);
                        cusEvents2 = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                        foreach (var evs in cusEvents2)
                        { 
                            var gTicket = DriverOffence.GetDriverOffenceById(evs.Identifier, dbConnection);
                            if (gTicket != null)
                            {
                                if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                                {

                                }
                                else if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                {

                                }
                                else
                                {
                                    pendingTicketCount++;
                                }
                            } 
                        }
                    }

                    if (pendingIncidentCount > 0)
                        pendingIncidents = pendingIncidentCount.ToString();
                    if (pendingTicketCount > 0)
                        pendingTicketing = pendingTicketCount.ToString();
                    if (pendingTasksCount > 0)
                        pendingTasks = pendingTasksCount.ToString();
                    if (inprotasks.Count > 0)
                        pendingVerifier = inprotasks.Count.ToString();

                }
            }
            catch(Exception er)
            {
                MIMSLog.MIMSLogSave("Site.Master", "getEventStatus", er, CommonUtility.dbConnection);
            }
        }
    }
}
