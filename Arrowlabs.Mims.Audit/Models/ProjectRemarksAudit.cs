﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
  public  class ProjectRemarksAudit

    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int CustomerId { get; set; }
        public string Remarks { get; set; }
        public int ProjectId { get; set; }
        public int SiteId { get; set; }
        public BaseAudit BaseAudit { get; set; }
        public int CustomerInfoId { get; set; }
        public static int InsertProjectRemarksAudit(ProjectRemarksAudit project, string auditDbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(project.BaseAudit, auditDbConnection);

            int id = 0;

            if (project != null)
            {
                using (var connection = new SqlConnection(auditDbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertProjectRemarksAudit", connection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = project.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = project.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = project.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = project.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = project.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = project.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = project.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = project.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@ProjectId", SqlDbType.NVarChar));
                    cmd.Parameters["@ProjectId"].Value = project.ProjectId;


                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = project.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertProjectRemarksAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                }

            }
            return id;
        }
    }
}
