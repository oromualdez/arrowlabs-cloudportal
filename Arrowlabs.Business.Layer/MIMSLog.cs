﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class MIMSLog
    {
        public int ID { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public DateTime? CurrentDateTime { get; set; }
        public string UpdatedBy { get; set; }
        public string MacAddress { get; set; }
        public string CurrentDateTimeToString { get; set; }

        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public static int InsertMIMSLog(MIMSLog mimslog, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            if (mimslog != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMIMSLog", connection);

                    cmd.Parameters.Add(new SqlParameter("@Message", SqlDbType.NVarChar));
                    cmd.Parameters["@Message"].Value = mimslog.Message;

                    cmd.Parameters.Add(new SqlParameter("@Exception", SqlDbType.NVarChar));
                    cmd.Parameters["@Exception"].Value = mimslog.Exception;

                    cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MacAddress"].Value = mimslog.MacAddress;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = mimslog.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = mimslog.CustomerId;
                     
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertMIMSLog";

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new MimsLogAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.Create, mimslog.UpdatedBy);
                            Reflection.CopyProperties(mimslog, audit);
                            MimsLogAudit.InsertMimsLogAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertMIMSLog", ex, dbConnection,0);
                    }
                }
            }
            return id;
        }
        public static int InsertMIMSLog(MIMSLog mimslog, string dbConnection)
        {
            int id = 0;

            if (mimslog != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertMIMSLog", connection);

                    cmd.Parameters.Add(new SqlParameter("@Message", SqlDbType.NVarChar));
                    cmd.Parameters["@Message"].Value = mimslog.Message;

                    cmd.Parameters.Add(new SqlParameter("@Exception", SqlDbType.NVarChar));
                    cmd.Parameters["@Exception"].Value = mimslog.Exception;

                    cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MacAddress"].Value = mimslog.MacAddress;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = mimslog.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = mimslog.CustomerId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertMIMSLog";

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;


                }
            }
            return id;
        }
        public static List<MIMSLog> GetAllMIMSLog(DateTime fromDate, DateTime toDate, string dbConnection)
        {
            var mimslogs = new List<MIMSLog>();
            
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMIMSLogs", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@toDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@toDateTime"].Value = toDate;

                sqlCommand.Parameters.Add(new SqlParameter("@frmDateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@frmDateTime"].Value = fromDate;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllMIMSLogs";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList(); 
                while (reader.Read())
                {
                    var mimslog = new MIMSLog();

                    mimslog.ID = Convert.ToInt32(reader["ID"].ToString());
                    mimslog.Message = reader["Message"].ToString();
                    mimslog.Exception = reader["Exception"].ToString();
                    mimslog.CurrentDateTime = Convert.ToDateTime(reader["CurrentDateTime"].ToString());
                    mimslog.MacAddress = reader["MacAddress"].ToString();

                    if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                        mimslog.SiteId = Convert.ToInt32(reader["SiteId"]);
                    
                    mimslogs.Add(mimslog);
                }
                connection.Close();
                reader.Close();
            }
            return mimslogs;
        }
        public static string DateFormatter(DateTime date)
        {
            string formattedDate = date.ToString("MM/dd/yyyy hh:mm:sstt");
            return formattedDate;
        }
        public static List<MIMSLog> GetAllMIMSLogsByMacAddress(string macAddress,string dbConnection)
        {
            var mimslogs = new List<MIMSLog>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMIMSLogsByMacAddress", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@macAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@macAddress"].Value = macAddress;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllMIMSLogsByMacAddress";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var mimslog = new MIMSLog();

                    mimslog.ID = Convert.ToInt32(reader["ID"].ToString());
                    mimslog.Message = reader["Message"].ToString();
                    mimslog.Exception = reader["Exception"].ToString();
                    mimslog.CurrentDateTime = Convert.ToDateTime(reader["CurrentDateTime"].ToString());
                    mimslog.CurrentDateTimeToString = reader["CurrentDateTime"].ToString();
                    mimslog.MacAddress = reader["MacAddress"].ToString();

                    if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                        mimslog.SiteId = Convert.ToInt32(reader["SiteId"]);

                    mimslogs.Add(mimslog);
                }
                connection.Close();
                reader.Close();
            }
            return mimslogs;
        }

        public static string DeleteAllMIMSLogByMacAddress(string MacAddress, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteMIMSLogByMacAddress", connection);

                cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                cmd.Parameters["@MacAddress"].Value = MacAddress;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteMIMSLogByMacAddress";

                cmd.ExecuteNonQuery();
            }
            return "success";
        }
        public static DateTime StringToDateFormater(string dateTime)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            var dateString = dateTime;
            var format = "MM/dd/yyyy hh:mm:sstt";

            return DateTime.ParseExact(dateString, format, provider);
        }

        public static int MIMSLogSave(string moduleName, string methodName, Exception exp, string dbConn, int siteId = 0, int customerid = 0)
        {
            var mimslog = new MIMSLog();
            mimslog.MacAddress = moduleName;
            mimslog.Exception = exp.Source;
            mimslog.Message = methodName + " : " + exp.Message;
            mimslog.SiteId = siteId;
            mimslog.CustomerId = customerid;
          return  MIMSLog.InsertMIMSLog(mimslog, dbConn);
        }
    }
}
