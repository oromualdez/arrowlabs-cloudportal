﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
     [Serializable]
    public class DirectorManager
    {
        public int DirectorId { get; set; }
        public int ManagerId { get; set; }
        public string ManagerName { get; set; }
        public string ManagerAccountName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public static bool InsertDirectorManager(List<DirectorManager> directormanager, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true,int siteId = 0)
        {
            var action =  Arrowlabs.Mims.Audit.Enums.Action.Create;
            var auditList = new List<DirectorManagerAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                foreach (var item in directormanager)
                {
                    var sqlCommand = new SqlCommand("InsertDirectorManager", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                    sqlCommand.Parameters["@DirectorId"].Value = item.DirectorId;

                    sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                    sqlCommand.Parameters["@ManagerId"].Value = item.ManagerId;

                    sqlCommand.Parameters.Add(new SqlParameter("@ManagerName", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ManagerName"].Value = item.ManagerName;

                    sqlCommand.Parameters.Add(new SqlParameter("@ManagerAccountName", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ManagerAccountName"].Value = item.ManagerAccountName;

                    sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@CreatedDate"].Value = item.CreatedDate;

                    sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@UpdatedDate"].Value = item.UpdatedDate;

                    sqlCommand.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@CreatedBy"].Value = item.CreatedBy;

                    sqlCommand.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UpdatedBy"].Value = item.UpdatedBy;

                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = item.SiteId;

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    sqlCommand.Parameters["@CustomerId"].Value = item.CustomerId;
                    
                    
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteNonQuery();


                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new DirectorManagerAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, item.UpdatedBy);
                            Reflection.CopyProperties(item, audit);
                            auditList.Add(audit);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer Inner Loop", "InsertDirectorManager", ex, dbConnection);
                    }
                }
                try
                {
                    if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                    {
                        DirectorManagerAudit.InsertDirectorManager(auditList, auditDbConnection);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Business Layer", "InsertDirectorManager", ex, dbConnection);
                }
                connection.Close();
            }
            return true;
        }

        public static List<int> GetAllManagersByDirectorId(int managerId, string dbConnection)
        {
            var userIdsList = new List<int>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllManagersByDirectorId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = managerId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userIdsList.Add(Convert.ToInt32(reader["ManagerId"].ToString()));
                }
                connection.Close();
                reader.Close();
            }
            return userIdsList;
        }

        public static List<int> GetAllDirectorsByManagerId(int userId, string dbConnection)
        {
            var managerIdsList = new List<int>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllDirectorsByManagerId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    managerIdsList.Add(Convert.ToInt32(reader["DirectorId"].ToString()));
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static List<Users> GetAllManagersByDirectorIdAndRoleIdAndUsername(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllManagersByDirectorIdAndRoleIdAndUsername", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllManagersByDirectorIdAndRoleIdAndFirstName(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllManagersByDirectorIdAndRoleIdAndFirstName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@FirstName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllManagersByDirectorIdAndRoleIdAndLastName(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllManagersByDirectorIdAndRoleIdAndLastName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static List<Users> GetAllUsersByDirectorIdAndUsername(int userId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByDirectorIdAndUsername", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllUsersByDirectorIdAndFirstName(int userId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByDirectorIdAndFirstName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@FirstName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllUsersByDirectorIdAndLastName(int userId,string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByDirectorIdAndLastName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }


        public static List<Users> GetAllFullManagersByDirectorId(int userId, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFullManagersByDirectorId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static List<Users> GetAllAssignedManagersByDirectorId(int userId, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllAssignedManagersByDirectorId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static List<Users> GetAllManagersByDirectorIdAndAccount(int userId,string account, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllManagersByDirectorIdAndAccount", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                sqlCommand.Parameters["@DirectorId"].Value = userId;

                sqlCommand.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@AccountName"].Value = account;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = Users.GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }



        public static bool DeleteManagersByDirectorIdAndMangName(string mangname, int dirId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteManagersByDirectorIdAndMangId", connection);

                cmd.Parameters.Add(new SqlParameter("@MangId", SqlDbType.NVarChar));
                cmd.Parameters["@MangId"].Value = mangname;
                cmd.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                cmd.Parameters["@DirectorId"].Value = dirId;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
