﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class PostOrderAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }        
        public string Description { get; set; }        
        public int LocationId { get; set; }        
        public int SubLocationId { get; set; }        
        public bool IsDeleted { get; set; }        
        public DateTime? CreatedDate { get; set; }            
        public string CreatedBy { get; set; }
        public string LocationName { get; set; }
        public string SubLocationName { get; set; }
        public int SiteId { get; set; }

        private static PostOrderAudit GetPostOrderAuditFromSqlReader(SqlDataReader reader)
        {
            var postOrder = new PostOrderAudit();
            postOrder.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                postOrder.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Description") && reader["Description"] != DBNull.Value)
                postOrder.Description = reader["Description"].ToString();
            if (columns.Contains("LocationId") && reader["LocationId"] != DBNull.Value)
                postOrder.LocationId = Convert.ToInt32(reader["LocationId"].ToString());
            if (columns.Contains("SubLocationId") && reader["SubLocationId"] != DBNull.Value)
                postOrder.SubLocationId = Convert.ToInt32(reader["SubLocationId"].ToString());
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                postOrder.IsDeleted = (String.IsNullOrEmpty(reader["IsDeleted"].ToString())) ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                postOrder.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                postOrder.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("LocationName") && reader["LocationName"] != DBNull.Value)
                postOrder.LocationName = (String.IsNullOrEmpty(reader["LocationName"].ToString())) ? "All" : reader["LocationName"].ToString();
            if (columns.Contains("SubLocationName") && reader["SubLocationName"] != DBNull.Value)
                postOrder.SubLocationName = (String.IsNullOrEmpty(reader["SubLocationName"].ToString())) ? "All" : reader["SubLocationName"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                postOrder.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
           
            return postOrder;
        }

        /// <summary>
        /// It returns all post orders
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<PostOrderAudit> GetAllPostOrderAudit(string dbConnection)
        {
            var postOrders = new List<PostOrderAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllPostOrderAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var postOrder = GetPostOrderAuditFromSqlReader(reader);
                    postOrders.Add(postOrder);
                }
                connection.Close();
                reader.Close();
            }
            return postOrders;
        }
        public static bool InsertPostOrderAudit(PostOrderAudit postOrder, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(postOrder.BaseAudit, dbConnection);

            if (postOrder != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertPostOrderAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = postOrder.Id;
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = postOrder.Description;
                    cmd.Parameters.Add("@LocationId", SqlDbType.Int).Value = postOrder.LocationId;
                    cmd.Parameters.Add("@SubLocationId", SqlDbType.Int).Value = postOrder.SubLocationId;
                    cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = postOrder.IsDeleted;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = postOrder.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = postOrder.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = postOrder.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
