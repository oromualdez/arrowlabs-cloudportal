﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Arrowlabs.Business.Layer
{
    public class Helper
    {
        public const string GENERIC_EXCEPTION = "We are unable to process your request";
        public const string NO_ITEM_FOR_DELETE_EXCEPTION = "Please select alteast one item to delete";
        public const string DELETETION_SUCCESS = "Deleted successfully";
        
        public static bool IsStringValid(string input)
        {
            return (!String.IsNullOrEmpty(input) && !String.IsNullOrWhiteSpace(input));
        }
        public static bool IsNumericString(string input)
        {
            double n;
            return double.TryParse(input, out n);
        }
        public static string AddSpacesToSentence(string text)
        {
            return Regex.Replace(text, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
        }
    }
}
