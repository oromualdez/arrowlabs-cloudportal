﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class LiveVideoResponse
    {
        public string IP { get; set; }
        public int Port { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Message { get; set; }

    }
}
