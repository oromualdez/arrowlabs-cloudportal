﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewTicket.ascx.cs" Inherits="ArrowLabs.Licence.Portal.Controls.Modals.NewTicket" %>
<script>

</script>
			<div aria-hidden="true" aria-labelledby="newTicketDocument" role="dialog" tabindex="-1" id="newTicketDocument" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button  aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					    <h4 class="modal-title capitalize-text" >NEW TICKET</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5">
                               <div class="row" style="display:<%=customersDisplay%>;">
									<div class="col-md-12">
                                        <label class="select select-o">
                                           <select id="ticketcustomerslst" onchange="ticketcustomersOnChange(this);">
											</select>
										 </label>

									</div>
								</div>
                                <div class="row" style="display:<%=customersDisplay%>;">
									<div class="col-md-12">
                                        <label class="select select-o">
                                           <select id="ticketcontractslst" >
                                               <option value="0" >Select Contract</option>
											</select>
										 </label>
									</div>

								</div>
								<div class="row">
<%--									<div class="col-md-7">
										<input placeholder="Name" id="tbNewTicketName" class="form-control">
									</div>--%>
                                    <div class="col-md-12">
                                        <label class="select select-o">
                                           <select id="ticketCategorySelect" runat="server" onchange="ticketCategorySelectOnChange(this);">
											</select>
										 </label>
                                    </div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                       <label class="select select-o">
                                           <select id="ticketTypeSelect" onchange="ticketTypeSelectOnChange(this);">
                                                  <option value="0" >Select Type</option>
											</select>
										 </label>
									</div> 
								</div>
                                <div class="row">
                                    <div class="col-md-9">
                                       <label class="select select-o">
                                           <select id="ticketItemSelect" >
                                                  <option value="0" >Select Item</option>
											</select>
										 </label>
									</div>
                                    <div class="col-md-3 horizontal-navigation" style="margin-top:8px;">
                                                <div class="panel-control">
                                <ul class="nav nav-tabs">
                                    <li style="background-color:#3ebb64;border-color:#3ebb64;height: 30px;margin: 0px;padding: 0px;" class="active" ><a href="#" style="margin-top: -5px;"  onclick="addOffenceToList()">+ ADD</a> 
                                    </li>
                                    </ul>
                                                    </div> 
                                    </div>
                                </div>
                                		<div class="row" style="
    border: 1px solid lightgray;
    padding-top: 10px;
    margin-left: 5px;
    margin-right: 5px;
    height: 100px;"
>
                                               <ol id="ticketItemsList">

                                                    </ol> 
                                		</div>
								<div class="row">
									<div class="col-md-12">
                                        <textarea placeholder="Description" id="tbNewTicketComment" class="form-control" rows="5"></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="ticketlocationSelect" onchange="ticketlocationOnChange(this);" runat="server">
											</select>
										 </label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<input id="tbNewTicketLongitude" placeholder="Longitude"  class="form-control">
									</div>
									<div class="col-md-6">
										<input id="tbNewTicketLatitude" placeholder="Latitude"  class="form-control">
									</div>									
								</div>	
                                <div class="row">
<div style="height:50px;" enctype="multipart/form-data" id="dz-test" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                                        <div class="dz-message">
                                                          <h2 style="color:#a6acbc">Drag and Drop or Click to Browse</h2>
                                                            <h6 style="color:#b2163b">Attach supporting screen shots or documents.</h6>
                                                        </div>
                                                      </div>
                                </div> 						
							</div>
							<div class="col-md-7" id="map_canvasticketLocationDIV">
                                <input id="pac-inputticket" class="controls" type="text" placeholder="Search Location">
                                <div id="map_newticketcanvasLocation" style="width:100%;height:442px;"></div>
						    </div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li  ><a href="#" class="capitalize-text" data-dismiss="modal">CANCEL</a>
									</li>
                                    <li ><a href="#" class="capitalize-text" onclick="sendTicket();">CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>		