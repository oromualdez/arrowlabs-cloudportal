﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class GroupItemFound
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]

        public int ItemFoundId { get; set; }
        [DataMember]
        public string ItemGroupId { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }

        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        private static GroupItemFound GetGroupItemFoundFromSqlReader(SqlDataReader reader)
        {
            var groupItemFound = new GroupItemFound();
            if (reader["Id"] != DBNull.Value)
                groupItemFound.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["ItemFoundId"] != DBNull.Value)
                groupItemFound.ItemFoundId = Convert.ToInt32(reader["ItemFoundId"].ToString());
            if (reader["ItemGroupId"] != DBNull.Value)
                groupItemFound.ItemGroupId = reader["ItemGroupId"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                groupItemFound.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                groupItemFound.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["SiteId"] != DBNull.Value)
                groupItemFound.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (reader["CustomerId"] != DBNull.Value)
                groupItemFound.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            return groupItemFound;
        }
        public static bool InsertorUpdateGroupItemFound(GroupItemFound groupItemFound, string dbConnection, string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (groupItemFound.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (groupItemFound != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateGroupItemFound", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = groupItemFound.Id;

                    cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    cmd.Parameters["@ItemFoundId"].Value = groupItemFound.ItemFoundId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = groupItemFound.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = groupItemFound.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@ItemGroupId", SqlDbType.NVarChar));
                    cmd.Parameters["@ItemGroupId"].Value = groupItemFound.ItemGroupId;


                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = groupItemFound.CreatedDate;


                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = groupItemFound.CreatedBy;


                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            groupItemFound.Id = id;
                            var audit = new GroupItemFoundAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, groupItemFound.CreatedBy);
                            Reflection.CopyProperties(groupItemFound, audit);
                            GroupItemFoundAudit.InsertGroupItemFoundAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateAssetCategory", ex, dbConnection);
                    }
                }
            }
            return true;
        }
        public static bool DeleteGroupItemFound(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteGroupItemFound", connection);

                cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                cmd.Parameters["@ItemFoundId"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
