﻿namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data.SqlClient;
    using System.Data;
    using System.Runtime.Serialization;
    using Arrowlabs.Mims.Audit.Models;

    public enum CheckListItemType
    {
        None = 0,
        Type1 = 1,
        Type2 = 2,
        Type3 = 3,
        Type4 = 4,
        Type5 = 5
    }
    public class TemplateCheckList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MainLocationId { get; set; }
        public int SubLocationId { get; set; }
        public string UpdatedBy { get; set; }
        public int CheckListType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        //public List<Location> MainLocationsList { get; set; }
        //public List<Location> SubLocationsList { get; set; }
        public string MainLocationName { get; set; }
        public string SubLocationName { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        private static TemplateCheckList GetTemplateCheckListFromSqlReader(SqlDataReader reader)
        {
            var templateCheckList = new TemplateCheckList();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                templateCheckList.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Name"] != DBNull.Value)
                templateCheckList.Name = reader["Name"].ToString();
            if (reader["MainLocationId"] != DBNull.Value)
                templateCheckList.MainLocationId = Convert.ToInt32(reader["MainLocationId"].ToString());
            if (reader["SubLocationId"] != DBNull.Value)
                templateCheckList.SubLocationId = Convert.ToInt32(reader["SubLocationId"].ToString());
            if (reader["CheckListType"] != DBNull.Value)
                templateCheckList.CheckListType = Convert.ToInt32(reader["CheckListType"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                templateCheckList.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                templateCheckList.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                templateCheckList.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["IsDeleted"] != DBNull.Value)
                templateCheckList.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString() == "1" ? "true" : "false");

            if (columns.Contains( "AccountId"))
                templateCheckList.AccountId = Convert.ToInt32(reader["AccountId"]);

            if (columns.Contains( "AccountName"))
                templateCheckList.AccountName = reader["AccountName"].ToString();

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                templateCheckList.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                templateCheckList.CustomerId = Convert.ToInt32(reader["CustomerId"]);


            return templateCheckList;
        }

        private static List<TemplateCheckList> AssignLocations(List<TemplateCheckList> templateCheckLists, string dbConnection)
        {
            var mainLocations = Location.GetAllLocation(dbConnection);
            var subLocations = Location.GetAllSubLocation(dbConnection);

            foreach (var templateCheckList in templateCheckLists)
            {
               // templateCheckList.MainLocationsList = mainLocations;
                //templateCheckList.SubLocationsList = subLocations;
                var parentLocation = mainLocations.FirstOrDefault(x => x.ID == templateCheckList.MainLocationId);
                if (parentLocation != null)
                    templateCheckList.MainLocationName = parentLocation.LocationDesc;
                var subLocation = subLocations.FirstOrDefault(x => x.ID == templateCheckList.SubLocationId);
                if (subLocation != null)
                    templateCheckList.SubLocationName = subLocation.LocationDesc;
            }
            return templateCheckLists;
        }

        public static TemplateCheckList AssignLocation(TemplateCheckList templateCheckLists, string dbConnection)
        {
            var mainLocations = Location.GetAllLocation(dbConnection);
            var subLocations = Location.GetAllSubLocation(dbConnection);


            // templateCheckList.MainLocationsList = mainLocations;
            //templateCheckList.SubLocationsList = subLocations;
            var parentLocation = mainLocations.FirstOrDefault(x => x.ID == templateCheckLists.MainLocationId);
            if (parentLocation != null)
                templateCheckLists.MainLocationName = parentLocation.LocationDesc;
            var subLocation = subLocations.FirstOrDefault(x => x.ID == templateCheckLists.SubLocationId);
            if (subLocation != null)
                templateCheckLists.SubLocationName = subLocation.LocationDesc;

            return templateCheckLists;
        }

        /// <summary>
        /// It will insert or update check list item.
        /// </summary>
        /// <param name="checkListItem"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static int InsertorUpdateTemplateCheckList(TemplateCheckList checkListItem, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var id = 0;
            if (checkListItem != null)
            {
                id = checkListItem.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateTemplateCheckList", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = checkListItem.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = checkListItem.Name;

                    cmd.Parameters.Add(new SqlParameter("@MainLocationId", SqlDbType.Int));
                    cmd.Parameters["@MainLocationId"].Value = checkListItem.MainLocationId;

                    cmd.Parameters.Add(new SqlParameter("@SubLocationId", SqlDbType.Int));
                    cmd.Parameters["@SubLocationId"].Value = checkListItem.SubLocationId;

                    cmd.Parameters.Add(new SqlParameter("@CheckListType", SqlDbType.Int));
                    cmd.Parameters["@CheckListType"].Value = checkListItem.CheckListType;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = checkListItem.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = checkListItem.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = checkListItem.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = checkListItem.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = checkListItem.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = checkListItem.CustomerId;
                    
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateTemplateCheckList";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            checkListItem.Id = id;
                            var audit = new TemplateCheckListAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, checkListItem.UpdatedBy);
                            Reflection.CopyProperties(checkListItem, audit);
                            TemplateCheckListAudit.InsertTemplateCheckListAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateTemplateCheckList", ex, auditDbConnection);
                    }
                }
            }
            return id;
        }
        public static List<TemplateCheckList> GetAllTemplateCheckListByTypeId(int typeId, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();


        /// <summary>
        /// It will return all type of check lists.
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
       var sqlCommand = new SqlCommand("GetAllTemplateCheckListByTypeId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.Int));
                sqlCommand.Parameters["@TypeId"].Value = typeId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                templateCheckListItems = AssignLocations(templateCheckListItems, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }
        public static List<TemplateCheckList> GetAllTemplateCheckListByTypeIdAndSiteId(int typeId, int siteId, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();


                /// <summary>
                /// It will return all type of check lists.
                /// </summary>
                /// <param name="dbConnection"></param>
                /// <returns></returns>
                var sqlCommand = new SqlCommand("GetAllTemplateCheckListByTypeIdAndSiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.Int));
                sqlCommand.Parameters["@TypeId"].Value = typeId;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                templateCheckListItems = AssignLocations(templateCheckListItems, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        public static List<TemplateCheckList> GetAllTemplateCheckListByTypeIdAndLevel5(int typeId, int siteId, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();


                /// <summary>
                /// It will return all type of check lists.
                /// </summary>
                /// <param name="dbConnection"></param>
                /// <returns></returns>
                var sqlCommand = new SqlCommand("GetAllTemplateCheckListByTypeIdAndLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.Int));
                sqlCommand.Parameters["@TypeId"].Value = typeId;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                templateCheckListItems = AssignLocations(templateCheckListItems, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        public static List<TemplateCheckList> GetAllTemplateCheckListByTypeIdAndCId(int typeId, int siteId, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();


                /// <summary>
                /// It will return all type of check lists.
                /// </summary>
                /// <param name="dbConnection"></param>
                /// <returns></returns>
                var sqlCommand = new SqlCommand("GetAllTemplateCheckListByTypeIdAndCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.Int));
                sqlCommand.Parameters["@TypeId"].Value = typeId;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                templateCheckListItems = AssignLocations(templateCheckListItems, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        public static List<TemplateCheckList> GetAllTemplateCheckListBySiteId(int siteId, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();


                /// <summary>
                /// It will return all type of check lists.
                /// </summary>
                /// <param name="dbConnection"></param>
                /// <returns></returns>
                var sqlCommand = new SqlCommand("GetAllTemplateCheckListBySiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                templateCheckListItems = AssignLocations(templateCheckListItems, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        public static List<TemplateCheckList> GetAllTemplateCheckListByLevel5(int siteId, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();


                /// <summary>
                /// It will return all type of check lists.
                /// </summary>
                /// <param name="dbConnection"></param>
                /// <returns></returns>
                var sqlCommand = new SqlCommand("GetAllTemplateCheckListByLevel5", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                templateCheckListItems = AssignLocations(templateCheckListItems, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }
        public static List<TemplateCheckList> GetAllTemplateCheckListByCId(int siteId, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();


                /// <summary>
                /// It will return all type of check lists.
                /// </summary>
                /// <param name="dbConnection"></param>
                /// <returns></returns>
                var sqlCommand = new SqlCommand("GetAllTemplateCheckListByCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                templateCheckListItems = AssignLocations(templateCheckListItems, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }
        public static List<TemplateCheckList> GetAllTemplateCheckListByTypeIdAndCreator(int typeId, string Name, string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();


                /// <summary>
                /// It will return all type of check lists.
                /// </summary>
                /// <param name="dbConnection"></param>
                /// <returns></returns>
                var sqlCommand = new SqlCommand("GetAllTemplateCheckListByTypeIdAndCreator", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.Int));
                sqlCommand.Parameters["@TypeId"].Value = typeId;
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = Name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                templateCheckListItems = AssignLocations(templateCheckListItems, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

        public static TemplateCheckList GetAllTemplateCheckListById(string id, string dbConnection)
        {
            TemplateCheckList template = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllTemplateCheckListById";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    template = new TemplateCheckList();
                    template = GetTemplateCheckListFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return template;
        }

        public static List<TemplateCheckList> GetAllTemplateCheckListByCreator(string Name, string dbConnection)
        {
            var templateCheckLists = new List<TemplateCheckList>();
            var TemplateCheck = new TemplateCheckList();
            TemplateCheck.Name = "None";
            TemplateCheck.Id = -1;
            templateCheckLists.Add(TemplateCheck);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListByCreator", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = Name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllTemplateCheckListByCreator";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var templateCheckList = GetTemplateCheckListFromSqlReader(reader);
                    templateCheckLists.Add(templateCheckList);
                }
                templateCheckLists = AssignLocations(templateCheckLists, dbConnection);
                connection.Close();
                reader.Close();
            }
            return templateCheckLists;
        }

        public static TemplateCheckList GetAllTemplateCheckListByName(string Name, string dbConnection)
        {
            TemplateCheckList template = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckListByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = Name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllTemplateCheckListByName";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    template = new TemplateCheckList();
                    template = GetTemplateCheckListFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return template;
        }

        public static List<TemplateCheckList> GetAllTemplateCheckList(string dbConnection)
        {
            var templateCheckLists = new List<TemplateCheckList>();
            var TemplateCheck = new TemplateCheckList();
            TemplateCheck.Name = "None";
            TemplateCheck.Id = -1;
            templateCheckLists.Add(TemplateCheck);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTemplateCheckList", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var templateCheckList = GetTemplateCheckListFromSqlReader(reader);   
                    templateCheckLists.Add(templateCheckList);
                }
                templateCheckLists = AssignLocations(templateCheckLists, dbConnection);

                connection.Close();
                reader.Close();
            }
            return templateCheckLists;
        }

        public static bool DeleteTemplateCheckListById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTemplateCheckListById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTemplateCheckListById";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        
    }
}
