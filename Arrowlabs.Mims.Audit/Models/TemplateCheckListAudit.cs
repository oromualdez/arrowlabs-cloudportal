﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public enum CheckListItemType
    {
        None = 0,
        Type1 = 1,
        Type2 = 2,
        Type3 = 3,
        Type4 = 4
    }
    public class TemplateCheckListAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int MainLocationId { get; set; }
        public int SubLocationId { get; set; }
        public int CheckListType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string MainLocationName { get; set; }
        public string SubLocationName { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
        
        private static TemplateCheckListAudit GetTemplateCheckListAuditFromSqlReader(SqlDataReader reader)
        {
            var templateCheckList = new TemplateCheckListAudit();
            templateCheckList.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("ID") && reader["Id"] != DBNull.Value)
                templateCheckList.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                templateCheckList.Name = reader["Name"].ToString();
            if (columns.Contains("MainLocationId") && reader["MainLocationId"] != DBNull.Value)
                templateCheckList.MainLocationId = Convert.ToInt32(reader["MainLocationId"].ToString());
            if (columns.Contains("SubLocationId") && reader["SubLocationId"] != DBNull.Value)
                templateCheckList.SubLocationId = Convert.ToInt32(reader["SubLocationId"].ToString());
            if (columns.Contains("CheckListType") && reader["CheckListType"] != DBNull.Value)
                templateCheckList.CheckListType = Convert.ToInt32(reader["CheckListType"].ToString());
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                templateCheckList.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                templateCheckList.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("IsDeleted") && reader["IsDeleted"] != DBNull.Value)
                templateCheckList.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString() == "1" ? "true" : "false");
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                templateCheckList.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                templateCheckList.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
            
            return templateCheckList;
        }
        public static bool InsertTemplateCheckListAudit(TemplateCheckListAudit checkListItem, string dbConnection)
        {
            if (checkListItem != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTemplateCheckListAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(checkListItem.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = checkListItem.Id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = checkListItem.Name;
                    cmd.Parameters.Add("@MainLocationId", SqlDbType.Int).Value = checkListItem.MainLocationId;
                    cmd.Parameters.Add("@SubLocationId", SqlDbType.Int).Value = checkListItem.SubLocationId;
                    cmd.Parameters.Add("@CheckListType", SqlDbType.Int).Value = checkListItem.CheckListType;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = checkListItem.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = checkListItem.CreatedBy;
                    cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Value = checkListItem.IsDeleted;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = checkListItem.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = checkListItem.CustomerId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public static List<TemplateCheckListAudit> GetAllTemplateCheckListAudit(string dbConnection)
        {
            var templateCheckListItems = new List<TemplateCheckListAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllTemplateCheckListAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var templateCheckListItem = GetTemplateCheckListAuditFromSqlReader(reader);
                    templateCheckListItems.Add(templateCheckListItem);
                }
                connection.Close();
                reader.Close();
            }
            return templateCheckListItems;
        }

    }
}
