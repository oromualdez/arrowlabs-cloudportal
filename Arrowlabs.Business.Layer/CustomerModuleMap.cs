﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Arrowlabs.Business.Layer
{
    public class CustomerModuleMap
    {
        public int CustomerId { get; set; }
        public string MBC { get; set; }
         
        public string INC { get; set; }
         
        public string TAC { get; set; }
         
        public string TIC { get; set; }

        public string PPC { get; set; }

        public string DRC { get; set; }
         
        public string LFC { get; set; }
         
        public string COC { get; set; }
         
        public string REC { get; set; }
         
        public string SUC { get; set; }
         
        public string ACC { get; set; }
        public string CHC { get; set; }
        public string WAC { get; set; }
         
        public string DIC { get; set; }

        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public static CustomerModuleMap GetFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var Class = new CustomerModuleMap();

            if (!string.IsNullOrEmpty(reader["CustomerId"].ToString()))
                Class.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains("MBC") && reader["MBC"] != DBNull.Value && !string.IsNullOrEmpty(reader["MBC"].ToString()))
                Class.MBC = Decrypt.DecryptData(reader["MBC"].ToString(), true, string.Empty);

            if (columns.Contains("INC") && reader["INC"] != DBNull.Value && !string.IsNullOrEmpty(reader["INC"].ToString()))
                Class.INC = Decrypt.DecryptData(reader["INC"].ToString(), true, string.Empty);

            if (columns.Contains("TAC") && reader["TAC"] != DBNull.Value && !string.IsNullOrEmpty(reader["TAC"].ToString()))
                Class.TAC = Decrypt.DecryptData(reader["TAC"].ToString(), true, string.Empty);

            if (columns.Contains("TIC") && reader["TIC"] != DBNull.Value && !string.IsNullOrEmpty(reader["TIC"].ToString()))
                Class.TIC = Decrypt.DecryptData(reader["TIC"].ToString(), true, string.Empty);

            if (columns.Contains("PPC") && reader["PPC"] != DBNull.Value && !string.IsNullOrEmpty(reader["PPC"].ToString()))
                Class.PPC = Decrypt.DecryptData(reader["PPC"].ToString(), true, string.Empty);

            if (columns.Contains("DRC") && reader["DRC"] != DBNull.Value && !string.IsNullOrEmpty(reader["DRC"].ToString()))
                Class.DRC = Decrypt.DecryptData(reader["DRC"].ToString(), true, string.Empty);

            if (columns.Contains("LFC") && reader["LFC"] != DBNull.Value && !string.IsNullOrEmpty(reader["LFC"].ToString()))
                Class.LFC = Decrypt.DecryptData(reader["LFC"].ToString(), true, string.Empty);

            if (columns.Contains("COC") && reader["COC"] != DBNull.Value && !string.IsNullOrEmpty(reader["COC"].ToString()))
                Class.COC = Decrypt.DecryptData(reader["COC"].ToString(), true, string.Empty);

            if (columns.Contains("REC") && reader["REC"] != DBNull.Value && !string.IsNullOrEmpty(reader["REC"].ToString()))
                Class.REC = Decrypt.DecryptData(reader["REC"].ToString(), true, string.Empty);

            if (columns.Contains("SUC") && reader["SUC"] != DBNull.Value && !string.IsNullOrEmpty(reader["SUC"].ToString()))
                Class.SUC = Decrypt.DecryptData(reader["SUC"].ToString(), true, string.Empty);

            if (columns.Contains("ACC") && reader["ACC"] != DBNull.Value && !string.IsNullOrEmpty(reader["ACC"].ToString()))
                Class.ACC = Decrypt.DecryptData(reader["ACC"].ToString(), true, string.Empty);

            if (columns.Contains("CHC") && reader["CHC"] != DBNull.Value && !string.IsNullOrEmpty(reader["CHC"].ToString()))
                Class.CHC = Decrypt.DecryptData(reader["CHC"].ToString(), true, string.Empty);

            if (columns.Contains("WAC") && reader["WAC"] != DBNull.Value && !string.IsNullOrEmpty(reader["WAC"].ToString()))
                Class.WAC = Decrypt.DecryptData(reader["WAC"].ToString(), true, string.Empty);

            if (columns.Contains("DIC") && reader["DIC"] != DBNull.Value && !string.IsNullOrEmpty(reader["DIC"].ToString()))
                Class.DIC = Decrypt.DecryptData(reader["DIC"].ToString(), true, string.Empty);

            if (columns.Contains("UpdatedBy") && reader["UpdatedBy"] != DBNull.Value && !string.IsNullOrEmpty(reader["UpdatedBy"].ToString()))
                Class.UpdatedBy = Decrypt.DecryptData(reader["UpdatedBy"].ToString(), true, string.Empty);

            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value && !string.IsNullOrEmpty(reader["CreatedBy"].ToString()))
                Class.CreatedBy = Decrypt.DecryptData(reader["CreatedBy"].ToString(), true, string.Empty);

            if (columns.Contains("UpdatedDate") && reader["UpdatedDate"] != DBNull.Value && !string.IsNullOrEmpty(reader["UpdatedDate"].ToString()))
                Class.UpdatedDate = Decrypt.DecryptData(reader["UpdatedDate"].ToString(), true, string.Empty);

            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value && !string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                Class.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, string.Empty);


            return Class;
        }

        public static bool InsertCustomerModuleMap(CustomerModuleMap clas, string dbConnection,
          string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var id = 0;

            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("InsertCustomerModuleMap", connection);

                cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                cmd.Parameters["@CustomerId"].Value = clas.CustomerId;

                cmd.Parameters.Add(new SqlParameter("@ACC", SqlDbType.NVarChar));
                cmd.Parameters["@ACC"].Value = Encrypt.EncryptData(clas.ACC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@CHC", SqlDbType.NVarChar));
                cmd.Parameters["@CHC"].Value = Encrypt.EncryptData(clas.CHC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@COC", SqlDbType.NVarChar));
                cmd.Parameters["@COC"].Value = Encrypt.EncryptData(clas.COC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                cmd.Parameters["@CreatedBy"].Value = Encrypt.EncryptData(clas.CreatedBy, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                cmd.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(clas.CreatedDate, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@DIC", SqlDbType.NVarChar));
                cmd.Parameters["@DIC"].Value = Encrypt.EncryptData(clas.DIC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@DRC", SqlDbType.NVarChar));
                cmd.Parameters["@DRC"].Value = Encrypt.EncryptData(clas.DRC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@INC", SqlDbType.NVarChar));
                cmd.Parameters["@INC"].Value = Encrypt.EncryptData(clas.INC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@LFC", SqlDbType.NVarChar));
                cmd.Parameters["@LFC"].Value = Encrypt.EncryptData(clas.LFC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@MBC", SqlDbType.NVarChar));
                cmd.Parameters["@MBC"].Value = Encrypt.EncryptData(clas.MBC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@PPC", SqlDbType.NVarChar));
                cmd.Parameters["@PPC"].Value = Encrypt.EncryptData(clas.PPC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@REC", SqlDbType.NVarChar));
                cmd.Parameters["@REC"].Value = Encrypt.EncryptData(clas.REC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@SUC", SqlDbType.NVarChar));
                cmd.Parameters["@SUC"].Value = Encrypt.EncryptData(clas.SUC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@TAC", SqlDbType.NVarChar));
                cmd.Parameters["@TAC"].Value = Encrypt.EncryptData(clas.TAC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@TIC", SqlDbType.NVarChar));
                cmd.Parameters["@TIC"].Value = Encrypt.EncryptData(clas.TIC, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                cmd.Parameters["@UpdatedBy"].Value = Encrypt.EncryptData(clas.UpdatedBy, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.NVarChar));
                cmd.Parameters["@UpdatedDate"].Value = Encrypt.EncryptData(clas.UpdatedDate, true, string.Empty);

                cmd.Parameters.Add(new SqlParameter("@WAC", SqlDbType.NVarChar));
                cmd.Parameters["@WAC"].Value = Encrypt.EncryptData(clas.WAC, true, string.Empty);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "InsertCustomerModuleMap";

                var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;
                  
                cmd.ExecuteNonQuery();
                 
                if (id == 0)
                    id = (int)returnParameter.Value;


                try
                {
                    if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                    {
                        var audit = new CustomerModuleMapAudit();
                        audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, clas.UpdatedBy);
                        Reflection.CopyProperties(clas, audit);
                        CustomerModuleMapAudit.InsertCustomerModuleMapAudit(audit, auditDbConnection);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Business Layer", "InsertCustomerModuleMap", ex, dbConnection);
                }

            }
            return true;
        }
    }
}
