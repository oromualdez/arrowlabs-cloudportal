﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArrowLabs.Licence.Portal.Controls.Modals
{
    public partial class IncidentCard : System.Web.UI.UserControl
    {
        protected string escalatedView;
        protected string taskDisplay;
        protected string dispatchDisplay;
        protected void Page_Load(object sender, EventArgs e)
        {
            var dbConnection = CommonUtility.dbConnection;
            var user2 = HttpContext.Current.User.Identity;
            if (user2 != null)
            {
                var userinfo = Users.GetUserByName(user2.Name, dbConnection);
                try
                {
                    var senderName = userinfo.Username;
                    var CameraLists = MilestoneCamera.GetAllMilestoneCamera(dbConnection);

                    cameraSelect.DataTextField = "CameraName";
                    cameraSelect.DataValueField = "CamDetails";
                    cameraSelect.DataSource = CameraLists;
                    cameraSelect.DataBind();
                    var getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    dispatchDisplay = "none";
                    taskDisplay = "style='display:none'";
                    if (getClientLic != null)
                    {
                        if (!getClientLic.isDispatch)
                            dispatchDisplay = "none";
                        if (!getClientLic.isTask)
                            taskDisplay = "style='display:none'";
                    }

                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                        foreach (var mods in modules)
                        {
                            if (mods.ModuleId == (int)Accounts.ModuleTypes.Surveillance && getClientLic.isSurveillance)
                            {

                            }
                            else if (mods.ModuleId == (int)Accounts.ModuleTypes.Dispatch && getClientLic.isDispatch)
                            {
                                dispatchDisplay = "block";
                            }
                            else if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks && getClientLic.isTask)
                            {
                                taskDisplay = "style='display:block'";
                            }
                            else if (mods.ModuleId == (int)Accounts.ModuleTypes.Request)
                            {

                            }
                        }
                    }
                    else
                    {
                        if (getClientLic != null)
                        {
                            if (getClientLic.isDispatch)
                                dispatchDisplay = "block";
                            if (getClientLic.isTask)
                                taskDisplay = "style='display:block'";
                        }
                    }
                    if (!string.IsNullOrEmpty(senderName))
                    {

                        var allusers = new List<Users>();
                        var alldevices = new List<Device>();
                        var allgroups = new List<Group>();
                        var defUser = new Users();
                        defUser.Username = "Please Select";
                        defUser.CustomerUName = "Please Select";
                        defUser.ID = 0;
                        allusers.Add(defUser);
                        var defDev = new Device();
                        defDev.PCName = "Please Select";
                        defDev.MACAddress = "0";
                        alldevices.Add(defDev);
                        var defgrp = new Group();
                        defgrp.Name = "Please Select";
                        defgrp.Id = 0;
                        allgroups.Add(defgrp);

                        if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.Regional || userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            escalatedView = "style='display:none'";
                        }

                        if (userinfo.RoleId == (int)Role.SuperAdmin)
                        {
                            escalatedView = "style='display:none'";
                            var taskslist = UserTask.GetAllTaskTemplates(dbConnection);

                            selectTaskTemplate.DataTextField = "Name";
                            selectTaskTemplate.DataValueField = "Id";
                            selectTaskTemplate.DataSource = taskslist;
                            selectTaskTemplate.DataBind();


                        }
                        else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            escalatedView = "style='display:none'";
                            var taskslist = UserTask.GetAllTemplateTasksByManagerId(userinfo.ID, dbConnection);

                            selectTaskTemplate.DataTextField = "Name";
                            selectTaskTemplate.DataValueField = "Id";
                            selectTaskTemplate.DataSource = taskslist;
                            selectTaskTemplate.DataBind();

                        }
                        else
                        {
                            PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);

                            var tasktemplateList = UserTask.GetAllTemplateTasksByManagerId(userinfo.ID, dbConnection);

                            selectTaskTemplate.DataTextField = "Name";
                            selectTaskTemplate.DataValueField = "Id";
                            selectTaskTemplate.DataSource = tasktemplateList;
                            selectTaskTemplate.DataBind();


                        }
                    }
                }
                catch (Exception er)
                {
                    MIMSLog.MIMSLogSave("IncidentCard", "Page_Load", er, dbConnection, userinfo.SiteId);
                }
            }
        }
    }
}