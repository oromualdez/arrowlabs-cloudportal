﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ContractInfoRemarks
    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int CustomerId { get; set; }
        public string Remarks { get; set; }
        public int ContractInfoId { get; set; }
        public int SiteId { get; set; }

        public string FName { get; set; }
        public string LName { get; set; }

        public string CustomerUName { get; set; }

        public int CustomerInfoId { get; set; }

        public static int InsertOrUpdateContractInfoRemarks(ContractInfoRemarks contractInfoRemarks, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = contractInfoRemarks.Id;

            var action = (contractInfoRemarks.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (contractInfoRemarks != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateContractInfoRemarks", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = contractInfoRemarks.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                    cmd.Parameters["@CustomerInfoId"].Value = contractInfoRemarks.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = contractInfoRemarks.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = contractInfoRemarks.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = contractInfoRemarks.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = contractInfoRemarks.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = contractInfoRemarks.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = contractInfoRemarks.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@ContractInfoId", SqlDbType.Int));
                    cmd.Parameters["@ContractInfoId"].Value = contractInfoRemarks.ContractInfoId;


                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = contractInfoRemarks.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateContractInfoRemarks";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            contractInfoRemarks.Id = id;
                            var audit = new ContractInfoRemarksAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, contractInfoRemarks.UpdatedBy);
                            Reflection.CopyProperties(contractInfoRemarks, audit);
                            ContractInfoRemarksAudit.InsertContractInfoRemarksAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateContractInfoRemarksAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static ContractInfoRemarks GetContractInfoRemarksFromSqlReader(SqlDataReader reader)
        {
            var contractInfoRemarks = new ContractInfoRemarks();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                contractInfoRemarks.Id = Convert.ToInt32(reader["Id"].ToString());

            if (reader["Remarks"] != DBNull.Value)
                contractInfoRemarks.Remarks = reader["Remarks"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                contractInfoRemarks.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["UpdatedDate"] != DBNull.Value)
                contractInfoRemarks.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                contractInfoRemarks.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["UpdatedBy"] != DBNull.Value)
                contractInfoRemarks.UpdatedBy = reader["UpdatedBy"].ToString();

            if (reader["CustomerId"] != DBNull.Value)
                contractInfoRemarks.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (reader["ContractInfoId"] != DBNull.Value)
                contractInfoRemarks.ContractInfoId = Convert.ToInt32(reader["ContractInfoId"].ToString());

            if (columns.Contains("CustomerUName"))
                contractInfoRemarks.CustomerUName = reader["CustomerUName"].ToString();


            if (reader["SiteId"] != DBNull.Value)
                contractInfoRemarks.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains( "CustomerInfoId") && reader["CustomerInfoId"] != DBNull.Value)
                contractInfoRemarks.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"].ToString());

            if (columns.Contains( "FName") && reader["FName"] != DBNull.Value)
                contractInfoRemarks.FName = reader["FName"].ToString();

            if (columns.Contains( "LName") && reader["LName"] != DBNull.Value)
                contractInfoRemarks.LName = reader["LName"].ToString(); 

            return contractInfoRemarks;
        }
        public static List<ContractInfoRemarks> GetContractInfoRemarksByContractInfoId(int contractInfoId, string dbConnection)
        {
            var projects = new List<ContractInfoRemarks>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetContractInfoRemarksByContractInfoId";
                sqlCommand.Parameters.Add(new SqlParameter("@ContractInfoId", SqlDbType.Int));
                sqlCommand.Parameters["@ContractInfoId"].Value = contractInfoId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var project = GetContractInfoRemarksFromSqlReader(reader);
                    projects.Add(project);
                }
                connection.Close();
                reader.Close();
            }
            return projects;
        }

        public static ContractInfoRemarks GetContractInfoRemarksById(int id, string dbConnection)
        {
            ContractInfoRemarks project = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetContractInfoRemarksById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    project = GetContractInfoRemarksFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return project;
        }



        public static void DeleteContractInfoRemarksById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteContractInfoRemarksById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteContractInfoRemarksById";

                cmd.ExecuteNonQuery();
            }
        }
    }
}
