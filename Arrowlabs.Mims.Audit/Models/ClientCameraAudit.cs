﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ClientCameraAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }        
        public int SiteId { get; set; }
        public string MacAddress { get; set; }
        public string CameraName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }


        public static bool InsertorUpdateClientCamera(ClientCameraAudit clientCamera, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(clientCamera.BaseAudit, dbConnection);

            if (clientCamera != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateClientCamera", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = clientCamera.Id;

                    cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NChar));
                    cmd.Parameters["@MacAddress"].Value = clientCamera.MacAddress;

                    cmd.Parameters.Add(new SqlParameter("@CameraName", SqlDbType.NVarChar));
                    cmd.Parameters["@CameraName"].Value = clientCamera.CameraName;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = clientCamera.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = clientCamera.UpdatedDate;


                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = clientCamera.CreatedBy;
                    
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = clientCamera.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
