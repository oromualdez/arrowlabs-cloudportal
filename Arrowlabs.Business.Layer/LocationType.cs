﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{

   public class LocationType
    {
        public int LocationTypeId { get; set; }
        public string LocationTypeDesc { get; set; }
        public string LocationTypeDesc_AR { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }

        private static LocationType GetLocationFromSqlReader(SqlDataReader reader)
        {
            var locType = new LocationType();
           var columns= Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["LOCATIONTYPEID"] != DBNull.Value)
                locType.LocationTypeId = Convert.ToInt32(reader["LOCATIONTYPEID"].ToString());
            if (reader["LOCATIONTYPEDESC"] != DBNull.Value)
                locType.LocationTypeDesc = reader["LOCATIONTYPEDESC"].ToString();
            if (reader["LOCATIONTYPEDESC_AR"] != DBNull.Value)
                locType.LocationTypeDesc_AR = reader["LOCATIONTYPEDESC_AR"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                locType.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                locType.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                locType.CreatedBy = reader["CreatedBy"].ToString();

            if (columns.Contains( "AccountId"))
                locType.AccountId = Convert.ToInt32(reader["AccountId"]);

            if (columns.Contains( "AccountName"))
                locType.AccountName = reader["AccountName"].ToString();


            return locType;
        }

        public static List<LocationType> GetAllLocationType(string dbConnection)
        {
            var locations = new List<LocationType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationType", connection);


                sqlCommand.CommandText = "GetAllLocationType";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var locType = GetLocationFromSqlReader(reader);
                    locations.Add(locType);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static List<LocationType> GetAllLocationTypeByDate(DateTime updatedDate, string dbConnection)
        {
            var locations = new List<LocationType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllLocationTypeByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetAllLocationTypeByDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var locType = GetLocationFromSqlReader(reader);
                    locations.Add(locType);
                }
                connection.Close();
                reader.Close();
            }
            return locations;
        }

        public static int InsertorUpdateLocationType(LocationType locationType, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = locationType.LocationTypeId;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

            if (locationType != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateLocationType", connection);

                    cmd.Parameters.Add(new SqlParameter("@LocationTypeId", SqlDbType.Int));
                    cmd.Parameters["@LocationTypeId"].Value = locationType.LocationTypeId;

                    cmd.Parameters.Add(new SqlParameter("@LocationTypeDesc", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationTypeDesc"].Value = locationType.LocationTypeDesc;

                    cmd.Parameters.Add(new SqlParameter("@LocationTypeDesc_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@LocationTypeDesc_AR"].Value = locationType.LocationTypeDesc_AR;
         
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = locationType.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = locationType.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = locationType.CreatedBy;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateLocationType";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            locationType.LocationTypeId = id;
                            var audit = new LocationTypeAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, locationType.UpdatedBy);
                            Reflection.CopyProperties(locationType, audit);
                            LocationTypeAudit.InsertLocationTypeAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateLocationType", ex, dbConnection);
                    }
                }
            }
            return id;
        }
        public static bool DeleteLocationTypeById(int locationId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteLocationTypeById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
