﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ReminderAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string CreatedBy { get; set; }
        public int ID { get; set; }
        public string ReminderNote { get; set; }
        public string ReminderTopic { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ReminderDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        
        public static ReminderAudit GetReminderAuditFromSqlReader(SqlDataReader reader)
        {
            var reminder = new ReminderAudit();
            reminder.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("ID") && reader["ID"] != DBNull.Value)
                reminder.ID = Convert.ToInt16(reader["ID"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                reminder.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                reminder.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("ReminderNote") && reader["ReminderNote"] != DBNull.Value)
                reminder.ReminderNote = reader["ReminderNote"].ToString();
            if (columns.Contains("ReminderTopic") && reader["ReminderTopic"] != DBNull.Value)
                reminder.ReminderTopic = reader["ReminderTopic"].ToString();
            if (columns.Contains("ReminderDate") && reader["ReminderDate"] != DBNull.Value)
                reminder.ReminderDate = Convert.ToDateTime(reader["ReminderDate"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                reminder.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return reminder;
        }
        public static List<ReminderAudit> GetAllReminderAudit(string dbConnection)
        {
            var reminderAudits = new List<ReminderAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllReminderAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    var reminderAudit = GetReminderAuditFromSqlReader(resultSet);
                    reminderAudits.Add(reminderAudit);
                }
                connection.Close();
                resultSet.Close();
            }
            return reminderAudits;
        }
        public static int InsertReminderAudit(ReminderAudit reminder, string dbConnection)
        {
            int id = 0;
            if (reminder != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("InsertReminderAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(reminder.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@ID", SqlDbType.Int).Value = reminder.ID;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = reminder.CreatedBy;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = reminder.CreatedDate;

                    cmd.Parameters.Add("@ReminderNote", SqlDbType.NVarChar).Value = reminder.ReminderNote;

                    cmd.Parameters.Add("@ReminderTopic", SqlDbType.NVarChar).Value = reminder.ReminderTopic;

                    cmd.Parameters.Add("@ReminderDate", SqlDbType.DateTime).Value = reminder.ReminderDate;

                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = reminder.SiteId;

                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = reminder.CustomerId;
                    

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
