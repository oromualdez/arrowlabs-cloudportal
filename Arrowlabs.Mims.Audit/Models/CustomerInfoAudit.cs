﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class CustomerInfoAudit
    {
        public BaseAudit BaseAudit { get; set; }  
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }
        public bool SendEmail { get; set; }
         
        public string Email { get; set; }
        public int TotalUser { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Modules { get; set; }
        public string Lati { get; set; }
        public string Long { get; set; }

        public string RssFeedUrl { get; set; }

        public string MessageBoardVideoPath { get; set; }

        public string DutyWorkWeek { get; set; }

        public int MaxUploadSize { get; set; }

        public int MaxNumberAttachments { get; set; }

        public int MIMSMobileGPSInterval { get; set; }

        public Double TimeZone { get; set; }
        public static bool InsertCustomerInfoAudit(CustomerInfoAudit customerInf, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(customerInf.BaseAudit, dbConnection);

            if (customerInf != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertCustomerInfoAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customerInf.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = Encrypt.EncryptData(customerInf.Name, true);

                    cmd.Parameters.Add(new SqlParameter("@Address", SqlDbType.NVarChar));
                    cmd.Parameters["@Address"].Value = Encrypt.EncryptData(customerInf.Address, true);

                    cmd.Parameters.Add(new SqlParameter("@Country", SqlDbType.NVarChar));
                    cmd.Parameters["@Country"].Value = Encrypt.EncryptData(customerInf.Country, true);

                    cmd.Parameters.Add(new SqlParameter("@PhoneNo", SqlDbType.NVarChar));
                    cmd.Parameters["@PhoneNo"].Value = Encrypt.EncryptData(customerInf.PhoneNo, true);

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customerInf.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = customerInf.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.NVarChar));
                    cmd.Parameters["@Active"].Value = Encrypt.EncryptData(customerInf.Active.ToString(), true);

                    cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                    cmd.Parameters["@Email"].Value = Encrypt.EncryptData(customerInf.Email, true);

                    cmd.Parameters.Add(new SqlParameter("@TotalUser", SqlDbType.NVarChar));
                    cmd.Parameters["@TotalUser"].Value = Encrypt.EncryptData(customerInf.TotalUser.ToString(), true);

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customerInf.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Modules", SqlDbType.NVarChar));
                    cmd.Parameters["@Modules"].Value = Encrypt.EncryptData(customerInf.Modules, true);

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customerInf.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Lati", SqlDbType.NVarChar));
                    cmd.Parameters["@Lati"].Value = customerInf.Lati;

                    cmd.Parameters.Add(new SqlParameter("@Long", SqlDbType.NVarChar));
                    cmd.Parameters["@Long"].Value = customerInf.Long;

                    cmd.Parameters.Add(new SqlParameter("@MaxNumberAttachments", SqlDbType.Int));
                    cmd.Parameters["@MaxNumberAttachments"].Value = customerInf.MaxNumberAttachments;

                    cmd.Parameters.Add(new SqlParameter("@MaxUploadSize", SqlDbType.Int));
                    cmd.Parameters["@MaxUploadSize"].Value = customerInf.MaxUploadSize;

                    cmd.Parameters.Add(new SqlParameter("@MIMSMobileGPSInterval", SqlDbType.Int));
                    cmd.Parameters["@MIMSMobileGPSInterval"].Value = customerInf.MIMSMobileGPSInterval;

                    cmd.Parameters.Add(new SqlParameter("@RssFeedUrl", SqlDbType.NVarChar));
                    cmd.Parameters["@RssFeedUrl"].Value = customerInf.RssFeedUrl;

                    cmd.Parameters.Add(new SqlParameter("@DutyWorkWeek", SqlDbType.NVarChar));
                    cmd.Parameters["@DutyWorkWeek"].Value = customerInf.DutyWorkWeek;

                    cmd.Parameters.Add(new SqlParameter("@MessageBoardVideoPath", SqlDbType.NVarChar));
                    cmd.Parameters["@MessageBoardVideoPath"].Value = customerInf.MessageBoardVideoPath;

                    cmd.Parameters.Add(new SqlParameter("@TimeZone", SqlDbType.Float));
                    cmd.Parameters["@TimeZone"].Value = customerInf.TimeZone;

                    cmd.Parameters.Add(new SqlParameter("@SendEmail", SqlDbType.Bit));
                    cmd.Parameters["@SendEmail"].Value = customerInf.SendEmail;
                     
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                }
                return true;
            }
            return false;
        }
    }
}
