﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TicketingCard.ascx.cs" Inherits="ArrowLabs.Licence.Portal.Controls.Modals.TicketingCard" %>
	            <style>
                    .line-center{
    margin:0;padding:0 10px;
    background:#FFF;
    display:inline-block;
}
h5{

    text-align:center;
    position:relative;
    z-index:2;
    color:gray;
    margin-left:-20px;
}
h5:after{
    content:"";
    position:absolute;
    top:50%;
    left:0;
    right:0;
    border-top:solid 1px gray;
    z-index:-1;
}  		 
                             </style>
<script>
    function nextbackTick() {
        document.getElementById("ticketrotationDIV1").style.display = "block";
        document.getElementById("ticketrotationDIV2").style.display = "block";
    }
</script>
			<div aria-hidden="true" aria-labelledby="ticketingViewCard" role="dialog" tabindex="-1" id="ticketingViewCard" class="modal fade videoModal" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="headerImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="ticketNameHeader">TICKET</h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-4">
							<p>Created by: <span id="ticketusernameSpan"></span></p>
						</div>		
                        <div class="col-md-4">
							<p>Location: <span id="ticketlocSpan"></span></p>
						</div>	
						<div class="col-md-4">
							<p>Created on: <span id="tickettimeSpan"></span></p>
						</div>				
					  </div>
                        <div class="row">
                            <div class="col-md-4">
							<p>Status: <span id="ticketstatusSpan"></span></p>
						</div>
                            <div class="col-md-4">
							 
						</div>	
						<div class="col-md-4">
							 
						</div>		
                        </div>				
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5" style="border-right: 1px #bbbbbb solid;">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                            <li class="active" id="liTicketInfo"><a href="#ticketinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="liTicketAct"><a href="#ticketactivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                            </li>	
                                            <li id="liTicketNotes"><a href="#ticketnotes-tab" data-toggle="tab" class="capitalize-text">NOTES</a>
                                            </li>	
                                            <li id="liTicketTasks"><a href="#tickettasks-tab" data-toggle="tab" class="capitalize-text">TASKS</a>
                                            </li>
                                            <li id="liTicketAtta"><a href="#ticketattachments-tab" data-toggle="tab" class="capitalize-text">FILES</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
                                        <div class="tab-content">
										<div class="tab-pane fade active in" id="ticketinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
                                                <div class="col-md-12" id="dvticketAccountSpan">
													<p class="red-color"><b>Account:</b></p>
													<p id="ticketAccountSpan"></p>
												</div>
                                                <div class="col-md-12" id="dvticketContract">
													<p class="red-color"><b>Contract:</b></p>
													<p id="ticketContract"></p>
												</div>
                                                 <div class="col-md-12">
													<p class="red-color"><b>Ticket Description:</b></p>
													<p id="ticketCommentSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Ticket Category:</b></p>
													<p id="offcatSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Ticket Type:</b></p>
													<p id="offtypeSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Ticket Items: </b></p>
                                                    <ul id="offencesItemsList">

                                                    </ul>
												</div>
<%--													<div class="col-md-12">
													<p class="red-color"><b>Plate Number:</b></p>
													<p id="platenumberSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate Source:</b></p>
													<p id="platesourceSpan"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Plate Code:</b></p>
													<p id="plateCodeSpan"></p>
												</div>
                                                <div class="col-md-12">
                                            	<p class="red-color"><b>Vehicle Make:</b></p>
													<p id="vehicleMakeSpan"></p>
												</div>
                                                <div class="col-md-12">
                                        		<p class="red-color"><b>Vehicle Color:</b></p>
													<p id="vehicleColorSpan"></p>
												</div>--%>
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div>
                                            <div class="tab-pane fade" id="ticketactivity-tab">	
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                                    <div id="divTicketActivity">
                                                    </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>							
										</div>	
                                            <div class="tab-pane fade" id="ticketnotes-tab">	
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
 <div id="ticketRemarksList" >
												    </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>							
										</div>		
                                            <div class="tab-pane fade" id="tickettasks-tab">	
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:305px;">
                                                    <ul id="tickettaskItemsList" style="list-style-type:none">

                                                    </ul> 
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>	
                                                <div class="row" id="legendTicketTaskDIV" style="border-top:1px solid #bbbbbb">
                                                <div class="col-md-4" style="margin-top:5px;padding-bottom:0px;"><i class="fa fa-circle  red-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Pending</a></div>
                                                <div class="col-md-4" style="margin-top:5px;padding-bottom:0px;"><i class="fa fa-circle  yellow-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Inprogress</a></div>
                                                <div class="col-md-4" style="margin-top:5px;padding-bottom:0px;"><i class="fa fa-circle  green-color"></i><a style="margin-left:5px;" href="#"  class="capitalize-text">Complete</a></div>
                                            </div>						
										</div>	


										<div class="tab-pane fade" id="ticketattachments-tab" onclick="startRot();nextbackTick();">	
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="ticketattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>							
										</div>		
                                        </div>								
									</div>
								</div>
							</div>

                                                        <div class="col-md-1"  style="width:40px;">

                                <i id="ticketrotationDIV1" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb" class="fa fa-angle-double-left  fa-2x" onclick="ticketbackImg()"></i>

                            </div>

							<div class="col-md-5" id="ticketdivAttachmentHolder" style="width:440px;margin-top:4px;border-right: 1px #bbbbbb solid;border-left: 1px #bbbbbb solid;">
                                <div class="tab-pane fade" id="ticketremarks-tab">
                                                                            								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li><a href="#ticketlocation-tab" data-toggle="tab" onclick="hideAllTicketRemarks()" class="capitalize-text">BACK</a>
                                            </li> 										
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                                    <div id="ticketRemarksList2"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div> 
								<div class="tab-pane fade active in" id="ticketlocation-tab">
                                    <div id="map_canvasTicketLocation" style="width:100%;height:380px;"></div>
									<div id="ticketdivAttachment" onclick="startRot()" class="overlapping-map-image">
									</div>
								</div>			
							</div>

                                     <div class="col-md-1"  style="width:40px;">
                                 <i id="ticketrotationDIV2" class="fa fa-angle-double-right  fa-2x" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb;"  onclick="ticketnextImg()"></i>

                             </div>

						</div>
 
					</div>
					<div class="modal-footer">
                                              <div class="row" id="pdfloadingAcceptTick" style="padding-bottom:75px;display:none;">
                            <div class="col-md-7">

                            </div>
                            <div class="col-md-5" style="margin-bottom:-50px">
                                <p class="red-color text-center" style="font-size:12px"><b id="gnNoteTick">GENERATING REPORT</b></p>
                                                                            <div id="myProgress">
                                              <div id="myBarTick"></div>
                                            </div>
                            </div>
                        </div>
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs" id="ticketinitialOptionsDiv" style="display:none;">
									<li><a href="#" data-dismiss="modal">UNHANDLE</a>
									</li>
									<li><a href="#"  onclick="handledClickTicket()">HANDLE</a>
									</li>	
								</ul>
								<!-- /.nav -->
                                <ul class="nav nav-tabs" id="tickethandleOptionsDiv" style="display:none;">
                                    <li id="liTicketBack"><a href="#" class="capitalize-text" onclick="unhandledClickTicket()">BACK</a>
                                    </li>
                                    <li id="liTicketStart" >
                                        <a href="#" onclick="updateStatus('START')">START</a>
                                    </li>
                                    <li id="liTicketEnd" style="display:none;" <%=isCustomerUser%>>
                                        <a href="#" onclick="updateStatus('END')">END</a>
                                    </li>	
                                    <li id="liTicketTask" style="display:<%=tskOptionView%>"><a href="#"  data-target="#newDocument" data-toggle="modal" onclick="addnewtask()">ADD TASK</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="addNotesToTicket()">ADD NOTES</a>
                                    </li>
                                    <li><a href="#" class="capitalize-text" onclick="document.getElementById('dz-postticket').click()">ATTACH</a>
                                    </li> 
                                    <li id="liTicketOpen"  style="display:none;" <%=isCustomerUser%>>
                                        <a href="#" onclick="updateStatus('START')">RE-OPEN</a>
                                    </li>
                                    <li id="liTicketResolve" <%=isCustomerUser%>><a href="#" class="capitalize-text" onclick="handleTicket()">RESOLVE</a>
                                    </li>
                                    <li id="liTicketReport" class="pull-right"><a href="#" class="capitalize-text " onclick="generateTicketPDF()">PDF</a>
                                    </li> 
                                </ul>
                                <textarea placeholder="Add Notes" style="display:none;" id="addticketNotesTA" class="form-control" rows="3"></textarea>
                                
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>

                