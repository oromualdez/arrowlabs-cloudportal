﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Arrowlabs.Business.Layer;
using System.Web.Configuration;
using ArrowLabs.Licence.Portal.Helpers;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Web.Services.Description;

namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class VerifierPage : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        protected string senderName;
        protected string senderName2;
        protected string pendingPercent;
        protected string inprogressPercent;
        protected string completedPercent;
        int completedCount;
        int inprogressCount;
        int pendingCount;
        int total;
        protected string ipaddress;
        static ClientLicence getClientLic;
        protected string userinfoDisplay;
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            senderName = User.Identity.Name;
            senderName2 = Encrypt.EncryptData(User.Identity.Name, true, dbConnection);
            var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
            ipaddress = configtSettings.ChatServerUrl + "/signalr";
            var userinfo = Users.GetUserByName(senderName, dbConnection);
            try
            {
                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                    //if (pushDev != null)
                    //{
                    //    if (!string.IsNullOrEmpty(pushDev.Username))
                    //    {
                    //        pushDev.IsServerPortal = true;
                    //        pushDev.SiteId = userinfo.SiteId;
                    //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection, dbConnectionAudit,true);
                    //    }
                    //}
                    PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);
                }
                if (userinfo != null)
                {
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        userinfoDisplay = "none";

                    }
                }
                getEventStatus(userinfo);
                anprPlateCategoryEnrollment.DataSource = Enum.GetNames(typeof(ANPR.enumPlateCategory));
                anprPlateCategoryEnrollment.DataBind();

                //var candidCodes = CandidStatusCode.GetAllCandidStatusCodes(dbConnection);//new List<CandidStatusCode>();
  
                //candidCodes.AddRange(CandidStatusCode.GetAllCandidStatusCodes(dbConnection));
                //var newcandidcode = new CandidStatusCode();
                //newcandidcode.Description = "Not found in the system";
                //newcandidcode.StatusCode = "";
                //candidCodes.Add(newcandidcode);
                //anprPlateStatusCodeEnrollment.DataTextField = "Description";
                //anprPlateStatusCodeEnrollment.DataValueField = "StatusCode";
                //anprPlateStatusCodeEnrollment.DataSource = candidCodes;
                //anprPlateStatusCodeEnrollment.DataBind();
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "Page_Load", err, dbConnection, userinfo.SiteId);
            }

        }
        [WebMethod]
        public static List<string> getTableData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var sessions = new List<Verifier>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    foreach (var site in sites)
                        sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, site.SiteId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    sessions = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Verify, dbConnection);
                }
                //if (userinfo.RoleId == (int)Role.SuperAdmin)
                //    sessions = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Verify, dbConnection);
                //else
                //    sessions = Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(userinfo.ID, (int)Verifier.RequestTypes.Verify, dbConnection);

                var grouped = sessions.GroupBy(item => item.Id);
                sessions = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                foreach (var item in sessions)
                {
                    var imageclass = "circle-point-orange";
                    var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.Id + "</td><td>" + item.CreatedBy + "</td><td>" + item.TypeName + "</td><td>" + item.CreatedDate.ToString() + "</td><td><a href='#' data-target='#viewDocument1'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;,&apos;VERI&apos;)'><i class='fa fa-eye mr-1x'></i>View</a><a href='#' data-target='#deleteVer'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;,&apos;" + item.TypeName + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getTableData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableData2(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var sessions = new List<Verifier>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Enroll, userinfo.SiteId, dbConnection));

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Enroll, userinfo.SiteId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Enroll, userinfo.SiteId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    foreach (var site in sites)
                        sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Enroll, site.SiteId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    sessions = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Enroll, dbConnection);
                }
                var grouped = sessions.GroupBy(item => item.Id);
                sessions = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                //if (userinfo.RoleId == (int)Role.SuperAdmin)
                //    sessions = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Enroll, dbConnection);
                //else
                //    sessions = Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(userinfo.ID, (int)Verifier.RequestTypes.Enroll, dbConnection);

                foreach (var item in sessions)
                {
                    var imageclass = "circle-point-orange";
                    var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.Id + "</td><td>" + item.CreatedBy + "</td><td>" + item.TypeName + "</td><td>" + item.CreatedDate.ToString() + "</td><td><a href='#' data-target='#viewEnrollment'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;,&apos;EN&apos;)'><i class='fa fa-eye mr-1x'></i>View</a><a href='#' data-target='#deleteVer'  data-toggle='modal' onclick='rowchoice(&apos;" + item.Id + "&apos;,&apos;" + item.TypeName + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getTableData2", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableData3(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var sessions = new List<Enrollment>();
                //if (userinfo.RoleId == (int)Role.Manager)
                //{
                //    sessions = Enrollment.GetAllEnrollmentBySite(userinfo.SiteId, dbConnection);
                //}
                //else if (userinfo.RoleId == (int)Role.Admin)
                //{
                //    sessions = Enrollment.GetAllEnrollmentBySite(userinfo.SiteId, dbConnection);
                //}
                //else if (userinfo.RoleId == (int)Role.Director)
                //{
                //    sessions = Enrollment.GetAllEnrollmentBySite(userinfo.SiteId, dbConnection);
                //}
                //else if (userinfo.RoleId == (int)Role.Regional)
                //{
                //    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                //    foreach (var site in sites)
                //        sessions.AddRange(Enrollment.GetAllEnrollmentBySite(site.SiteId, dbConnection));
                //}
                //else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                //{
                    sessions = Enrollment.GetAllEnrollments(dbConnection);
                //}
                var grouped = sessions.GroupBy(item => item.CaseId);
                sessions = grouped.Select(grp => grp.OrderBy(item => item.CaseId).First()).ToList();
                //if (userinfo.RoleId == (int)Role.SuperAdmin)
                //    sessions = Enrollment.GetAllEnrollments(dbConnection);
                //else
                //    sessions = Enrollment.GetAllEnrollmentBySite(userinfo.SiteId, dbConnection);


                foreach (var item in sessions)
                {
                    var delete = "<a href='#' data-target='#deleteEnroll'  data-toggle='modal' onclick='rowchoice(&apos;" + item.CaseId + "&apos;,&apos;END&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                        delete = string.Empty;

                    var imageclass = "circle-point-orange";
                    var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.CaseId + "</td><td>" + item.Name + "</td><td>" + item.TypeName + "</td><td>" + item.CreatedDate.ToString() + "</td><td><a href='#' data-target='#viewEnrollment'  data-toggle='modal' onclick='rowchoice(&apos;" + item.CaseId + "&apos;,&apos;END&apos;)'><i class='fa fa-eye mr-1x'></i>View</a>"+delete+"</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getTableData3", err, dbConnection, userinfo.SiteId);
            }

            return listy;
        }
        [WebMethod]
        public static List<string> getTableANPR(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var sessions = ANPR.GetAllANPR(dbConnection);
                //var usersList = CommonUtility.getUsersOfManagerDirector(userinfo);
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    sessions = sessions.Where(i=>i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    sessions = sessions.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    sessions = sessions.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    var sessionList = new List<ANPR>();
                    foreach (var site in sites)
                        sessionList.AddRange(sessions.Where(i => i.SiteId == site.SiteId).ToList());
                    //sessions.AddRange(Enrollment.GetAllEnrollmentBySite(site.SiteId, dbConnection));
                }
                foreach (var item in sessions)
                {
                    //if (usersList.Count > 0)
                    //{
                    //    if (usersList.Contains(item.CreatedBy))
                    //    {
                    //        if (!string.IsNullOrEmpty(item.PlateNumber))
                    //        {
                    //            if (item.Type != ANPR.Types.Enrolled && item.Type != ANPR.Types.EnrollmentRequest && item.Type != ANPR.Types.EnrollRejected)
                    //            {
                    //                var returnstring = "<tr role='row' class='odd'><td>" + item.TransactionID + "</td><td class='sorting_1'>" + item.PlateNumber + "</td><td>" + item.PlateCity + "</td><td>" + item.PlateCategory + "</td><td>" + item.TransactionTime.ToString() + "</td><td><a href='#' data-target='#anprViewCard'  data-toggle='modal' onclick='rowchoiceANPR(&apos;" + item.TransactionID + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a><a href='#' data-target='#deleteANPR'  data-toggle='modal' onclick='rowchoiceANPR(&apos;" + item.TransactionID + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a></td></tr>";
                    //                listy.Add(returnstring);
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                        if (!string.IsNullOrEmpty(item.PlateNumber))
                        {
                            if (item.Type != ANPR.Types.Enrolled && item.Type != ANPR.Types.EnrollmentRequest && item.Type != ANPR.Types.EnrollRejected)
                            {
                                var returnstring = "<tr role='row' class='odd'><td>" + item.TransactionID + "</td><td class='sorting_1'>" + item.PlateNumber + "</td><td>" + item.PlateCity + "</td><td>" + item.PlateCategory + "</td><td>" + item.TransactionTime.ToString() + "</td><td><a href='#' data-target='#anprViewCard'  data-toggle='modal' onclick='rowchoiceANPR(&apos;" + item.TransactionID + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a><a href='#' data-target='#deleteANPR'  data-toggle='modal' onclick='rowchoiceANPR(&apos;" + item.TransactionID + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a></td></tr>";
                                listy.Add(returnstring);
                            }
                        }
                    //}
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getTableANPR", err, dbConnection, userinfo.SiteId);
            }

            return listy;
        }
        [WebMethod]
        public static List<string> getTableANPREnrollment(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var sessions = new List<ANPR>();
                sessions = ANPR.GetAllANPRByType(ANPR.Types.EnrollmentRequest, dbConnection);
                var usersList = CommonUtility.getUsersOfManagerDirector(userinfo);
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                foreach (var item in sessions)
                {
                    
                    if (usersList.Count > 0)
                    {
                        if (usersList.Contains(item.CreatedBy))
                        {
                            var imageclass = "circle-point-orange";
                            var imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/" + item.Id.ToString() + ".jpeg";
                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.Id + "</td><td>" + item.CreatedBy + "</td><td>" + item.TransactionTime.ToString() + "</td><td><a href='#' data-target='#anprEnrollment' onclick='rowchoiceANPREnroll(&apos;" + item.Id + "&apos;,&apos;" + imgstring + "&apos;,&apos;" + item.CreatedBy + "&apos;)'  data-toggle='modal' ><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                            listy.Add(returnstring);
                        }
                    }
                    else
                    {
                        var imageclass = "circle-point-orange";
                        var imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/" + item.Id.ToString() + ".jpeg";
                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.Id + "</td><td>" + item.CreatedBy + "</td><td>" + item.TransactionTime.ToString() + "</td><td><a href='#' data-target='#anprEnrollment' onclick='rowchoiceANPREnroll(&apos;" + item.Id + "&apos;,&apos;" + imgstring + "&apos;,&apos;" + item.CreatedBy + "&apos;)'  data-toggle='modal' ><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getTableANPREnrollment", err, dbConnection, userinfo.SiteId);
            }

            return listy;
        }
        [WebMethod]
        public static List<string> getTableANPREnrolled(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var sessions = new List<ANPR>();
                sessions = ANPR.GetAllANPRByType(ANPR.Types.Enrolled, dbConnection);
                //var usersList = CommonUtility.getUsersOfManagerDirector(userinfo);
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    sessions = sessions.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    sessions = sessions.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    sessions = sessions.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    var sessionList = new List<ANPR>();
                    foreach (var site in sites)
                        sessionList.AddRange(sessions.Where(i => i.SiteId == site.SiteId).ToList());
                    //sessions.AddRange(Enrollment.GetAllEnrollmentBySite(site.SiteId, dbConnection));
                }
                foreach (var item in sessions)
                {
                    var imageclass = "circle-point-orange";
                    var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    var imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/" + item.Id.ToString() + ".jpeg";
                    var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.Id + "</td><td>" + item.CreatedBy + "</td><td>" + item.TransactionTime.ToString() + "</td><td><a href='#' data-target='#anprEnrollment' onclick='rowchoiceANPREnroll(&apos;" + item.Id + "&apos;,&apos;" + imgstring + "&apos;,&apos;" + item.CreatedBy + "&apos;)'  data-toggle='modal' ><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getTableANPREnrollment", err, dbConnection, userinfo.SiteId);
            }

            return listy;
        }
        public void getEventStatus(Users userinfo)
        {
            pendingCount = 0;
            inprogressCount = 0;
            completedCount = 0;
            try
            {
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        var pendingtasks = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Enroll, dbConnection);
                        var inprotasks = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Verify, dbConnection);
                        var completedtasks = Enrollment.GetAllEnrollments(dbConnection);

                        pendingCount = pendingtasks.Count;
                        inprogressCount = inprotasks.Count;
                        completedCount = completedtasks.Count;
                    }
                    else if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                    {
                        var pendingtasks = Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Enroll,userinfo.SiteId ,dbConnection);
                        var inprotasks = Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection);
                        var completedtasks = Enrollment.GetAllEnrollmentBySite(userinfo.SiteId, dbConnection);
                        pendingCount = pendingtasks.Count;
                        inprogressCount = inprotasks.Count;
                        completedCount = completedtasks.Count;
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        var pendingtasks = Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Enroll, userinfo.SiteId, dbConnection);
                        var inprotasks = Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection);
                        var completedtasks = Enrollment.GetAllEnrollmentBySite(userinfo.SiteId, dbConnection);
                        pendingCount = pendingtasks.Count;
                        inprogressCount = inprotasks.Count;
                        completedCount = completedtasks.Count;
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        var pendingtasks = new List<Verifier>();//.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Enroll, userinfo.SiteId, dbConnection);
                        var inprotasks = new List<Verifier>(); //Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection);
                        var completedtasks = new List<Enrollment>();// Enrollment.GetAllEnrollmentByCreatedBy(userinfo.Username, dbConnection);
                        
                        var usites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        foreach (var usite in usites)
                        {
                            pendingtasks.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Enroll, usite.SiteId, dbConnection));
                            inprotasks.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, usite.SiteId, dbConnection));
                            completedtasks.AddRange(Enrollment.GetAllEnrollmentBySite(usite.SiteId, dbConnection));
                        }

                        var grouped = pendingtasks.GroupBy(item => item.Id);
                        pendingtasks = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                        var grouped2 = inprotasks.GroupBy(item => item.Id);
                        inprotasks = grouped2.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                        var grouped3 = completedtasks.GroupBy(item => item.CaseId);
                        completedtasks = grouped3.Select(grp => grp.OrderBy(item => item.CaseId).First()).ToList();

                        pendingCount = pendingtasks.Count;
                        inprogressCount = inprotasks.Count;
                        completedCount = completedtasks.Count;
                    }
                    total = pendingCount + inprogressCount + completedCount;

                    inprogressCount = total - (pendingCount + completedCount);
                    var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                    var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                    var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                    if (pendingCount > 0)
                        pendingPercent = pendingPercentage.ToString();
                    else
                        pendingPercent = "0";

                    if (inprogressCount > 0)
                        inprogressPercent = inprogressPercentage.ToString();
                    else
                        inprogressPercent = "0";

                    if (completedCount > 0)
                        completedPercent = completedPercentage.ToString();
                    else
                        completedPercent = "0";
                    lbCompleted.Text = completedCount.ToString();
                    lbCompletedpercent.Text = completedPercentage.ToString();
                    lbInprogress.Text = inprogressCount.ToString();
                    lbInprogresspercent.Text = inprogressPercentage.ToString();
                    lbPending.Text = pendingCount.ToString();
                    lbPendingpercent.Text = pendingPercentage.ToString();
                    lbTotalAlarms.Text = total.ToString();
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getEventStatus", err, dbConnection, userinfo.SiteId);
            }
        }
        [WebMethod]
        public static List<string> getVerifierRequestData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var customData = Verifier.GetVerifierById(id, dbConnection);
                var caseInfo = new Enrollment();
                var runVerify = VerifierResults.GetVerifierResultByVerifierId(id, dbConnection);
                if (runVerify.Count > 0)
                {

                    caseInfo = Enrollment.GetAllEnrollmentByCaseId(runVerify[0].CaseId, dbConnection);

                }
                var newCaseId = new List<int>();
             if (caseInfo != null)
                {
                    listy.Add(caseInfo.CreatedBy);
                }
                else
                    listy.Add(customData.CreatedBy);
	    
                listy.Add(customData.TypeName);
                listy.Add(customData.CaseId.ToString());
                listy.Add(customData.CreatedDate.ToString());
                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                listy.Add(geoLoc);

                if (caseInfo != null)
                {
                    if (!string.IsNullOrEmpty(caseInfo.Reason))
                        listy.Add(caseInfo.Reason);
                    else
                        listy.Add("N/A");
                    listy.Add(caseInfo.YearOfBirth);
                    listy.Add(caseInfo.Ethnicity);
                    listy.Add(caseInfo.PID);
                    listy.Add(caseInfo.Gender);
                    listy.Add(caseInfo.ListName);
                }
                else if (customData.EnrollmentResult != null)
                {
                    listy.Add(customData.EnrollmentResult.Reason);
                    listy.Add(customData.EnrollmentResult.YearOfBirth);
                    listy.Add(customData.EnrollmentResult.Ethnicity);
                    listy.Add(customData.EnrollmentResult.PID);
                    listy.Add(customData.EnrollmentResult.Gender);
                    listy.Add(customData.EnrollmentResult.ListName);
                }
                else
                {
                    listy.Add(customData.Reason);
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                }

                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + ".jpeg";
                listy.Add(imgstring);

                if (customData.Request == (int)Verifier.RequestTypes.Verify)
                {
                    var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
                    if (resultImg != null)
                    {
                        string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                        listy.Add("data:image/png;base64," + base64String1);
                    }
                    foreach (var verify in runVerify)
                    {
                        newCaseId.Add(verify.CaseId);
                        imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + "/" + verify.Percent + ".jpeg";
                        listy.Add(imgstring);

                        listy.Add(((int)Math.Round(Convert.ToSingle(verify.Percent) * (float)100)).ToString());
                    }
                }
                if (newCaseId[0] > 0)
                {
                    var newcaseInfo = Verifier.GetCaseInformationByCaseId(newCaseId[0], dbConnection);
                    if (!string.IsNullOrEmpty(newcaseInfo))
                    {
                        listy.Add(newcaseInfo);
                    }
                    else
                    {
                        listy.Add("N/A");
                    }
                    foreach (var caseid in newCaseId)
                    {
                        var casename = Verifier.GetCaseInformationByCaseId(caseid, dbConnection);

                        if (!string.IsNullOrEmpty(casename))
                        {
                            listy.Add(casename);
                            listy.Add(caseid.ToString());
                        }
                        else
                        {
                            listy.Add("N/A");
                            listy.Add(caseid.ToString());
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getVerifyLocationData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                json += "[";
                if (id > 0)
                {
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            var customData = Verifier.GetVerifierById(id, dbConnection);

                            if (customData != null)
                            {
                                json += "{ \"Username\" : \"" + customData.TypeName + "\",\"Id\" : \"" + customData.Id.ToString() + "\",\"Long\" : \"" + customData.Longitude.ToString() + "\",\"Lat\" : \"" + customData.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getVerifyLocationData", err, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getAttachmentDataIcons(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var cusEv = ANPR.GetANPRByTransactionID(id, dbConnection);
                if (cusEv != null)
                {
                    if (cusEv.Type == ANPR.Types.ANPRSystem )
                    {
                        var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        var imgstring = string.Empty;
                        var retstring2 = string.Empty;
                        if (!string.IsNullOrEmpty(cusEv.OverviewImage))
                        {
                            imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/Overview/" + id + ".jpg";
                            retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-1-tab'/>";
                            listy += retstring2;
                        }
                        if (!string.IsNullOrEmpty(cusEv.PlateImage))
                        {
                            imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/Plates/" + id + ".jpg";
                            retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-2-tab'/>";
                            listy += retstring2;
                        }
                    }
                    else if (cusEv.Type == ANPR.Types.MIMSMobile || cusEv.Type == ANPR.Types.MIMSClient)
                    {
                        var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        var imgstring = string.Empty;
                        var retstring2 = string.Empty;
                        if (!string.IsNullOrEmpty(cusEv.OverviewImage))
                        {
                            imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/" + cusEv.Id + ".jpg";
                            retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-1-tab'/>";
                            listy += retstring2;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentDataIcons", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTab(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = string.Empty;
            try
            {
                var cusEv = ANPR.GetANPRByTransactionID(id, dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#location-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Location</p></div></div></div>";
                if (cusEv != null)
                {
                    if (cusEv.Type == ANPR.Types.ANPRSystem )
                    {
                        listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-1-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Overview</p></div></div></div>";
                        listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-2-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Plate</p></div></div></div>";
                    }
                    else if (cusEv.Type == ANPR.Types.MIMSMobile || cusEv.Type == ANPR.Types.MIMSClient)
                    {
                        listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-1-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Overview</p></div></div></div>";
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("VerifierPage", "getAttachmentDataTab", err, dbConnection, userinfo.SiteId);
            }

            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var cusEv = ANPR.GetANPRByTransactionID(id, dbConnection);
                if (cusEv.Type == ANPR.Types.ANPRSystem )
                {
                    var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    var imgstring = string.Empty;
                    var retstring2 = string.Empty;
                    var retstring = "<video id='Video0' width='100%' height='378px' autoplay='autoplay' type='video/ogg; codecs=theo' muted controls ></video>";
                    //var retstring = "<video id='Video0' width='100%' height='378px' src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' type='video/ogg; codecs=theo' autoplay='autoplay' />";
                    listy.Add(retstring);
                    if (!string.IsNullOrEmpty(cusEv.OverviewImage))
                    {
                        imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/Overview/" + id + ".jpg";
                        retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                        listy.Add(retstring2);
                    }
                    if (!string.IsNullOrEmpty(cusEv.PlateImage))
                    {
                        imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/Plates/" + id + ".jpg";
                        retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                        listy.Add(retstring2);
                    }
                }
                else if (cusEv.Type == ANPR.Types.MIMSMobile || cusEv.Type == ANPR.Types.MIMSClient)
                {
                    var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                    var imgstring = string.Empty;
                    var retstring2 = string.Empty;
                    var retstring = "<video id='Video0' width='100%' height='378px' autoplay='autoplay' type='video/ogg; codecs=theo' muted controls ></video>";
                    //var retstring = "<video id='Video0' width='100%' height='378px' src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' type='video/ogg; codecs=theo' autoplay='autoplay' />";
                    listy.Add(retstring);
                    imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/" + cusEv.Id + ".jpg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getANPRData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var customData = ANPR.GetANPRByTransactionID(id, dbConnection);
                
                listy.Add(customData.PlateCategory);
                listy.Add(customData.PlateCity);
                listy.Add(customData.PlateColor);
                listy.Add(customData.PlateCountry);
                listy.Add(customData.PlateNumber);
                listy.Add(customData.PlateType);
                listy.Add(customData.TransactionTime.ToString());
                if (customData.Type == ANPR.Types.ANPRSystem)
                {
                    listy.Add(customData.DPUName);
                    listy.Add("N/A");    
                }
                else if (customData.Type == ANPR.Types.MIMSClient)
                {
                    listy.Add(customData.CreatedBy);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);  
                }
                else if (customData.Type == ANPR.Types.MIMSMobile)
                {
                    listy.Add(customData.CreatedBy);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                }           
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getEnrolledData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getIncidentLocationData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                json += "[";
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        var cusEv = ANPR.GetANPRByTransactionID(id, dbConnection);
                        if (cusEv != null)
                        {
                            
                            json += "{ \"Username\" : \"Offence\",\"Id\" : \"" + cusEv.TransactionID.ToString() + "\",\"Long\" : \"" + cusEv.Longitude.ToString() + "\",\"Lat\" : \"" + cusEv.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                           
                        }
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getIncidentLocationData", err, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static List<string> getEnrolledData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var customData = Enrollment.GetAllEnrollmentByCaseId(id, dbConnection);
                listy.Add(customData.CreatedBy);
                listy.Add(customData.TypeName);
                listy.Add(customData.CaseId.ToString());
                listy.Add(customData.CreatedDate.ToString());
                if (!string.IsNullOrEmpty(customData.Reason))
                    listy.Add(customData.Reason);
                else
                    listy.Add("N/A");
                listy.Add(customData.PID);
                listy.Add(customData.YearOfBirth);
                listy.Add(customData.Gender);
                listy.Add(customData.Ethnicity);
                listy.Add(customData.ListName);
                var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
                if (resultImg != null)
                {
                    string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                    listy.Add("data:image/png;base64," + base64String1);
                }
                else
                {
                    listy.Add("../images/icon-user-default.png");
                }
                listy.Add(customData.Name);
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "getEnrolledData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> delVerifierData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                Verifier.DeleteVerfierById(id, dbConnection);
                SystemLogger.SaveSystemLog(dbConnectionAudit, "Verifier", id.ToString(), id.ToString(), userinfo, "Delete Verifier" + id);
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "delVerifierData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> delEnrolledData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                Enrollment.DeleteEnrollmentById(id, dbConnection);
                SystemLogger.SaveSystemLog(dbConnectionAudit, "Verifier", id.ToString(), id.ToString(), userinfo, "Delete Enrollment" + id);
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "delEnrolledData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> rejectEnrollmentRequest(int id,string notes,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var getVerifier = Verifier.GetVerifierById(id, dbConnection);
                getVerifier.Reason = notes;
                getVerifier.Request = (int)Verifier.RequestTypes.Rejected;
                Verifier.InsertOrUpdateVerifier(getVerifier, dbConnection, dbConnectionAudit,true);
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "rejectEnrollmentRequest", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> runVerifyData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var customData = Verifier.GetVerifierById(id, dbConnection);
                var caseInfo = Enrollment.GetAllEnrollmentByCaseId(customData.CaseId, dbConnection);
                var newCaseId = new List<int>();
                listy.Add(customData.CreatedBy);
                listy.Add(customData.TypeName);
                listy.Add(customData.CaseId.ToString());
                listy.Add(customData.CreatedDate.ToString());
                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                listy.Add(geoLoc);

                if (caseInfo != null)
                {
                    listy.Add(caseInfo.Reason);
                    listy.Add(caseInfo.YearOfBirth);
                    listy.Add(caseInfo.Ethnicity);
                    listy.Add(caseInfo.PID);
                    listy.Add(caseInfo.Gender);
                    listy.Add(caseInfo.ListName);
                }
                else if (customData.EnrollmentResult != null)
                {
                    listy.Add(customData.EnrollmentResult.Reason);
                    listy.Add(customData.EnrollmentResult.YearOfBirth);
                    listy.Add(customData.EnrollmentResult.Ethnicity);
                    listy.Add(customData.EnrollmentResult.PID);
                    listy.Add(customData.EnrollmentResult.Gender);
                    listy.Add(customData.EnrollmentResult.ListName);
                }
                else
                {
                    listy.Add(customData.Reason);
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                }
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + ".jpeg";
                listy.Add(imgstring);

                if (customData.Request == (int)Verifier.RequestTypes.Verify)
                {
                    var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
                    if (resultImg != null)
                    {
                        string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                        listy.Add("data:image/png;base64," + base64String1);
                    }
                    var runVerify = Verifier.VerifierMultipleResult(customData, dbConnection);
                    foreach (var verify in runVerify)
                    {
                        newCaseId.Add(verify.CaseId);
                        string base64String1 = Convert.ToBase64String(verify.ResultImage, 0, verify.ResultImage.Length);
                        if (verify.ResultImage.Length > 333)
                            listy.Add("data:image/png;base64," + base64String1);
                        else
                            listy.Add("../images/icon-user-default.png");

                        listy.Add(((int)Math.Round(Convert.ToSingle(verify.Score) * (float)100)).ToString());
                    }
                }
                if (newCaseId[0] > 0)
                {
                    var newcaseInfo = Verifier.GetCaseInformationByCaseId(newCaseId[0], dbConnection);
                    if (!string.IsNullOrEmpty(newcaseInfo))
                    {
                        listy.Add(newcaseInfo);
                    }
                    else
                    {
                        listy.Add("N/A");
                    }
                    foreach (var caseid in newCaseId)
                    {
                        var casename = Verifier.GetCaseInformationByCaseId(caseid, dbConnection);

                        if (!string.IsNullOrEmpty(casename))
                        {
                            listy.Add(casename);
                            listy.Add(caseid.ToString());
                        }
                        else
                        {
                            listy.Add("N/A");
                            listy.Add(caseid.ToString());
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "runVerifyData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string newEnrollmentData(int id, string reason, string ethnicity, string gender, string category, string pid, string name, string yob, string enrolled,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                if (Convert.ToBoolean(enrolled))
                {
                    var getEnrolled = Enrollment.GetAllEnrollmentByCaseId(id, dbConnection);
                    getEnrolled.ConfirmReason = reason;
                    getEnrolled.Ethnicity = ethnicity;
                    getEnrolled.Gender = gender;
                    getEnrolled.ListCategory = Convert.ToInt32(category);
                    getEnrolled.PID = pid;
                    getEnrolled.Name = name;
                    getEnrolled.YearOfBirth = yob;
                    getEnrolled.UpdatedDate = CommonUtility.getDTNow();
                    getEnrolled.UpdatedBy = userinfo.Username;
                    var retId = Enrollment.InsertOrUpdateEnrolment(getEnrolled, dbConnection, true, dbConnectionAudit,true);

                    return "SUCCESS";
                }
                else
                {
                    var getVerifier = Verifier.GetVerifierById(id, dbConnection);
                    if (getVerifier != null)
                    {
                        var newEnroll = new Enrollment();
                        newEnroll.ConfirmReason = reason;
                        newEnroll.CreatedBy = userinfo.Username;
                        newEnroll.CreatedDate = CommonUtility.getDTNow();
                        newEnroll.Ethnicity = ethnicity;
                        newEnroll.Gender = gender;
                        newEnroll.ListCategory = Convert.ToInt32(category);
                        newEnroll.PID = pid;
                        newEnroll.Type = getVerifier.Type;
                        newEnroll.Name = name;
                        newEnroll.YearOfBirth = yob;
                        newEnroll.Reason = getVerifier.Reason;
                        var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                        var imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + id.ToString() + ".jpeg";
                        byte[] imageBytes;
                        using (var webClient = new System.Net.WebClient())
                        {
                            imageBytes = webClient.DownloadData(imgstring);
                        }
                        newEnroll.EnrollImage = imageBytes; //getVerifier.SentImageFile;
                        newEnroll.UpdatedDate = CommonUtility.getDTNow();
                        newEnroll.UpdatedBy = userinfo.Username;
                        var newCaseId = Enrollment.InsertOrUpdateEnrolment(newEnroll, dbConnection, true, dbConnectionAudit,true);
                        if (newCaseId > 0)
                        {
                            var successEnrollment = Enrollment.GetAllEnrollmentByCaseId(newCaseId, dbConnection);
                            if (!string.IsNullOrEmpty(successEnrollment.TransactionID))
                            {
                                getVerifier.Request = (int)Verifier.RequestTypes.Enrolled;
                                getVerifier.CaseId = newCaseId;
                                Verifier.InsertOrUpdateVerifier(getVerifier, dbConnection, dbConnectionAudit,true);
                                return "SUCCESS";
                            }
                            else
                            {
                                Enrollment.DeleteEnrollmentById(newCaseId, dbConnection);
                                return "Problem enrolling, try again later.";
                            }
                        }
                        else
                            return "Problem enrolling, try again later.";
                    }
                }
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("VerifierPage", "newEnrollmentData", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
            return "Problem with service,try again later.";
        }
        [WebMethod]
        public static string newEnrollment(string imgPath, string reason, string ethnicity, string gender, string category, string pid, string name, string yob, string type,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var newimgPath = imgPath.Replace('|', '\\');
                var bmap = new System.Drawing.Bitmap(newimgPath);
                var newEnroll = new Enrollment();
                newEnroll.ConfirmReason = reason;
                newEnroll.CreatedBy = userinfo.Username;
                newEnroll.CreatedDate = CommonUtility.getDTNow();
                newEnroll.Reason = reason;
                newEnroll.Ethnicity = ethnicity;
                newEnroll.Gender = gender;
                newEnroll.ListCategory = Convert.ToInt32(category);
                newEnroll.PID = pid;

                if (type == "ANPR")
                    newEnroll.Type = 1;
                else
                    newEnroll.Type = 0;

                newEnroll.Name = name;
                newEnroll.YearOfBirth = yob;
                newEnroll.EnrollImage = CommonUtility.ImageToByte2(bmap);
                newEnroll.UpdatedDate = CommonUtility.getDTNow();
                newEnroll.UpdatedBy = userinfo.Username;
                var newCaseId = Enrollment.InsertOrUpdateEnrolment(newEnroll, dbConnection, true, dbConnectionAudit,true);
                if (newCaseId > 0)
                {
                    var successEnrollment = Enrollment.GetAllEnrollmentByCaseId(newCaseId, dbConnection);
                    if (!string.IsNullOrEmpty(successEnrollment.TransactionID))
                    {
                        return "SUCCESS";
                    }
                    else
                    {
                        Enrollment.DeleteEnrollmentById(newCaseId, dbConnection);
                        return "Problem enrolling, try again later.";
                    }
                }
                else
                    return "Failed to enroll, try again later.";

                
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("VerifierPage", "newEnrollment", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
        }
        
         [WebMethod]
        public static string enrollANPR(int id, string plateno, string platelane, string platecity, string platecategory, string platecolor, string platetype, string platecode, string plateprefix,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                                     var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var retval = string.Empty;
                    ChannelFactory<ServiceSoap> channelFactory = null;
                    EndpointAddress ep = null;
                    var webports = VerifierService.GetAllVerifierService(dbConnection);
                    var serviceUrl = string.Empty;
                    var platecountry = string.Empty;
                    var platecodenotes = string.Empty;
                    var platecategorycode = (int)((ANPR.enumPlateCategory)Enum.Parse(typeof(ANPR.enumPlateCategory), platecategory));
                    if (platecity == "SAU" || platecity == "KUW" || platecity == "BHR" || platecity == "OMN" || platecity == "QAT")
                        platecountry = platecity;
                    else
                        platecountry = "UAE";
                    //var candidcodes = CandidStatusCode.GetAllCandidStatusCodes(dbConnection);
                    //platecodenotes = candidcodes.Single(a => a.StatusCode == platecode).Description;

                    foreach (var web in webports)
                    {
                        if (web.Type == (int)VerifierService.VerifierServiceTypes.ANPR)
                            serviceUrl = web.Url;
                    }
                    string strEPAdr = serviceUrl;
                    if (!string.IsNullOrEmpty(strEPAdr))
                    {
                        switch ("HTTP")
                        {
                            case "TCP":
                                NetTcpBinding tcpb = new NetTcpBinding();
                                channelFactory = new ChannelFactory<ServiceSoap>(tcpb);

                                // End Point Address
                                strEPAdr = "";
                                break;

                            case "HTTP":
                                BasicHttpBinding httpb = new BasicHttpBinding();
                                channelFactory = new ChannelFactory<ServiceSoap>(httpb);

                                // End Point Address
                                strEPAdr = serviceUrl;
                                break;
                        }

                        // Create End Point
                        ep = new EndpointAddress(strEPAdr);

                        // Create Channel
                        ServiceSoap mathSvcObj = channelFactory.CreateChannel(ep);

                        var returnval = mathSvcObj.SetVehicleAlert(1, "candid", "candid@123", "candid", plateprefix, plateno, platecity, platecountry, platecategorycode.ToString(), platecolor, platecode, platecodenotes);

                        if (!string.IsNullOrEmpty(returnval))
                            retval = returnval;
                        else
                        {
                            var anprentry = ANPR.GetANPRById(id, dbConnection);
                            anprentry.PlateNumber = plateprefix + plateno;
                            anprentry.PlateCity = platecity;
                            anprentry.PlateCountry = platecountry;
                            anprentry.PlateCategory = platecategory;
                            anprentry.PlateColor = platecolor;
                            anprentry.PlateType = platetype;
                            anprentry.PlatePrefix = plateprefix;
                            anprentry.AlarmStatusCode = platecode;
                            anprentry.AlarmStatusTitle = platecodenotes;
                            anprentry.PlateNumberOnly = plateno;
                            anprentry.TransactionTime = CommonUtility.getDTNow();
                            anprentry.Type = ANPR.Types.Enrolled;
                            ANPR.InsertOrUpdateANPR(anprentry, dbConnection);
                            retval = "Success";
                        }
                        channelFactory.Close();

                    }
                    return retval;
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("VerifierPage", "enrollANPR", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
            }
        }
       
         [WebMethod]
         public static string rejectANPR(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = "SUCCESS";
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var anprentry = ANPR.GetANPRById(id, dbConnection);
                    anprentry.TransactionTime = CommonUtility.getDTNow();
                    anprentry.Type = ANPR.Types.EnrollRejected;
                    ANPR.InsertOrUpdateANPR(anprentry, dbConnection);
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Verifier", "rejectANPR", err, dbConnection, userinfo.SiteId);
                    return "Error occured while trying to reject entry - " + err.Message;
                }
                return listy;
            }
        }
        [WebMethod]
         public static List<string> verifyANPR(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>(); ;
            try
            {
                ChannelFactory<ServiceSoap> channelFactory = null;
                EndpointAddress ep = null;
                var webports = VerifierService.GetAllVerifierService(dbConnection);
                var serviceUrl = string.Empty;

                foreach (var web in webports)
                {
                    if (web.Type == (int)VerifierService.VerifierServiceTypes.ANPR)
                        serviceUrl = web.Url;
                }
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var imgstring = settings.MIMSMobileAddress + "/Uploads/Anpr/" + id.ToString() + ".jpeg";
                byte[] imageBytes;
                using (var webClient = new System.Net.WebClient())
                {
                    imageBytes = webClient.DownloadData(imgstring);
                }
                string strEPAdr = serviceUrl;
                if (!string.IsNullOrEmpty(strEPAdr))
                {
                    switch ("HTTP")
                    {
                        case "TCP":
                            NetTcpBinding tcpb = new NetTcpBinding();
                            channelFactory = new ChannelFactory<ServiceSoap>(tcpb);

                            // End Point Address
                            strEPAdr = "";
                            break;

                        case "HTTP":
                            BasicHttpBinding httpb = new BasicHttpBinding();
                            channelFactory = new ChannelFactory<ServiceSoap>(httpb);

                            // End Point Address
                            strEPAdr = serviceUrl;
                            break;
                    }

                    // Create End Point
                    ep = new EndpointAddress(strEPAdr);

                    // Create Channel
                    ServiceSoap mathSvcObj = channelFactory.CreateChannel(ep);

                    mathSvcObj.VerifyImage(imageBytes, "MobileDevice1",0.0,0.0);
                    System.Threading.Thread.Sleep(3000);
                    var retANPR = mathSvcObj.FetchData("MobileDevice1");
                    if (retANPR == null)
                    {
                        System.Threading.Thread.Sleep(3000);
                        retANPR = mathSvcObj.FetchData("MobileDevice1");
                    }
                    else if (string.IsNullOrEmpty(retANPR.PlateNumber))
                    {
                        System.Threading.Thread.Sleep(3000);
                        retANPR = mathSvcObj.FetchData("MobileDevice1");
                    }
                    if(retANPR != null)
                    {
                        if(!string.IsNullOrEmpty(retANPR.PlateNumber))
                        {
                            var psplit = retANPR.PlateNumber.Split(' ');
                            if (psplit.Length > 1)
                            {
                                retANPR.PlateNumberOnly = psplit[1];
                                retANPR.PlatePrefix = psplit[0];
                            }
                            if (string.IsNullOrEmpty(retANPR.PlateNumberOnly))
                                retANPR.PlateNumberOnly = retANPR.PlateNumber;
                            if (retANPR.PlateCity == "Dubai")
                                retANPR.PlateCity = "DXB";
                            listy.Add(retANPR.PlateNumberOnly);
                            listy.Add(retANPR.LaneName);
                            listy.Add(retANPR.PlateCity);
                            listy.Add(retANPR.PlateCategory);
                            listy.Add(retANPR.PlateColor);
                            listy.Add(retANPR.PlateType);
                            listy.Add(retANPR.AlarmStatusCode);
                            listy.Add(retANPR.PlatePrefix);
                            if (string.IsNullOrEmpty(retANPR.AlarmStatusCode))
                                listy.Add("Not enrolled in the system");
                            else
                                listy.Add(retANPR.AlarmStatusTitle);

                            var mimcon = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            var consplit = mimcon.FilePath.Split('\\');
                            var newstring = string.Empty;
                            var count = 0;
                            foreach (var split in consplit)
                            {
                                if (count < 5)
                                {
                                    newstring += split + "\\";
                                    count++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            var anprinfo = ANPR.GetANPRByTransactionID(Convert.ToInt32(retANPR.TransactionID), dbConnection);
                            newstring = newstring + "Anpr\\" + anprinfo.Id + ".jpg";
                            System.IO.File.WriteAllBytes(newstring, imageBytes);
                        }
                        else
                        {
                            listy.Add("Unable to read plate number");
                        }
                    }
                    else
                    {
                        listy.Add("Unable to read plate image");
                    }
                    channelFactory.Close();
                }
            }
            catch (Exception err)
            {
                listy.Add("Error occured while trying to read image data");
                MIMSLog.MIMSLogSave("Verifier", "verifyANPR", err, dbConnection, userinfo.SiteId);                
            }
            return listy;
        }
        
        //User Profile
        [WebMethod]
        public static string changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {
                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            if (userinfo.ID > 0)
            {
                var getuser = userinfo;
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                           
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection,dbConnectionAudit,true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit,true))
                {
                    return userinfo.ID;
                }
                else
                    return 0;
            }
            return userinfo.ID;
        }
        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                // var test = Users.GetUserById(id, dbConnection);
                var customData = Users.GetUserById(userinfo.ID, dbConnection);
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.Username);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.Username);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                  //  var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                    {
                    //    if (pushDev != null)
                     //   {
                      //      if (!string.IsNullOrEmpty(pushDev.Username))
                      //      {
                        if (customData.IsServerPortal)
                                {
                                    monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                                    fontstyle = "";
                                }
                                else
                                {
                                    monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                                }
                      //      }
                     //       else
                      //      {
                      //          monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                       //         fontstyle = "";
                       //     }
                       // }
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("VerifierPage", "getUserProfileData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static string delANPRData(int id,string uname)
        {
            var listy = "SUCCESS";
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                ANPR.DeleteANPRByTransactionID(id, dbConnection);
                SystemLogger.SaveSystemLog(dbConnectionAudit, "Verifier", id.ToString(), id.ToString(), userinfo, "Delete ANPR" + id);
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Verifier", "delANPRData", err, dbConnection, userinfo.SiteId);
                return "Error occured while trying to delete - " + err.Message;
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }
    }
}