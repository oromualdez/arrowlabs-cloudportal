﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ThirdPartySettingType
    {
        public int Id { get; set; }
        public string Name { get; set; }


        private static ThirdPartySettingType GetThirdPartySettingTypeFromSqlReader(SqlDataReader reader)
        {
            var thirdPartySeting = new ThirdPartySettingType();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                thirdPartySeting.Id = Convert.ToInt32(reader["Id"].ToString());



            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                thirdPartySeting.Name = reader["Name"].ToString();

            return thirdPartySeting;
        }
        public static List<ThirdPartySettingType> GetAllThirdPartySettingType(string dbConnection)
        {
            var thirdPartySetings = new List<ThirdPartySettingType>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllThirdPartySettingType", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var thirdParty = GetThirdPartySettingTypeFromSqlReader(reader);
                    thirdPartySetings.Add(thirdParty);
                }
                connection.Close();
                reader.Close();
            }
            return thirdPartySetings;
        }
        public static ThirdPartySettingType GetThirdPartySettingTypeById(int id, string dbConnection)
        {
            ThirdPartySettingType thirdParty = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetThirdPartySettingTypeById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    thirdParty = GetThirdPartySettingTypeFromSqlReader(reader);
                    
                }
                connection.Close();
                reader.Close();
            }
            return thirdParty;
        }

        public static int InsertOrUpdateThirdPartySettingType(ThirdPartySettingType thirdPartySeting, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var id = 0;

            var action = (thirdPartySeting.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;


            if (thirdPartySeting != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateThirdPartySettingType", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = thirdPartySeting.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = thirdPartySeting.Name;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            thirdPartySeting.Id = id;
                            var audit = new ThirdPartySettingTypeAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, thirdPartySeting.Name);
                            Reflection.CopyProperties(thirdPartySeting, audit);
                            ThirdPartySettingTypeAudit.InsertOrUpdateThirdPartySettingType(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateThirdPartySetting", ex, dbConnection);
                    }
                }
            }
            return id;
        }
    }
}
